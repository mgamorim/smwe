﻿using System.Threading.Tasks;
using Abp.Configuration;

namespace SW10.SWMANAGER.Timing
{
    public interface ITimeZoneService
    {
        Task<string> GetDefaultTimezoneAsync(SettingScopes scope, int? tenantId);
    }
}
