﻿using SW10.SWMANAGER.ClassesAplicacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.MultiTenancy
{
    public class TenantImportConfig : CamposPadraoCRUD
    {
        public long? TenantId { get; set; }
        public string ConnectionStringNameSw { get; set; }
        public string ConnectionStringNameAsa { get; set; }
    }
}
