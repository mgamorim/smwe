﻿using Abp.Domain.Services;

namespace SW10.SWMANAGER
{
    public abstract class SWMANAGERDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected SWMANAGERDomainServiceBase()
        {
            LocalizationSourceName = SWMANAGERConsts.LocalizationSourceName;
        }
    }
}
