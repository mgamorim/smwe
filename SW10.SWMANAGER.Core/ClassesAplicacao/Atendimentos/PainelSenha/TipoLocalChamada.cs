﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha
{
    [Table("AteTipoLocalChamada")]
    public class TipoLocalChamada : CamposPadraoCRUD
    {
        public long? TipoLocalChamadaProximoId { get; set; }

        [ForeignKey("TipoLocalChamadaProximoId")]
        public TipoLocalChamada TipoLocalChamadaProximo { get; set; }

    }
}
