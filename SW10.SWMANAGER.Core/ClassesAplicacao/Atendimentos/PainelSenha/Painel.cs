﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha
{
    [Table("AtePainel")]
    public class Painel : CamposPadraoCRUD
    {
        public List<PainelTipoLocalChamada> PaineisTipoLocaisChamadas { get; set; }

    }
}
