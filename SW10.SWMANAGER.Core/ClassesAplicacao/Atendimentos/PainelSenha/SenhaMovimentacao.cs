﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha
{
    [Table("AteSenhaMov")]
    public class SenhaMovimentacao : CamposPadraoCRUD
    {
        public long SenhaId { get; set; }
        public long? LocalChamadaId { get; set; }
        public long? TipoLocalChamadaId { get; set; }
        public DateTime DataHora { get; set; }
        public DateTime? DataHoraInicial { get; set; }
        public DateTime? DataHoraFinal { get; set; }

        [ForeignKey("SenhaId")]
        public Senha Senha { get; set; }

        [ForeignKey("LocalChamadaId")]
        public LocalChamada LocalChamada { get; set; }

        [ForeignKey("TipoLocalChamadaId")]
        public TipoLocalChamada TipoLocalChamada { get; set; }

    }
}
