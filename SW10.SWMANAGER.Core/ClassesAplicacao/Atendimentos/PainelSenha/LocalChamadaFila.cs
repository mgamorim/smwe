﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha
{
    [Table("AteLocalChamadaFila")]
    public class LocalChamadaFila : CamposPadraoCRUD
    {
        public long? FilaId { get; set; }
        public long? LocalChamadaId { get; set; }

        [ForeignKey("FilaId")]
        public Fila Fila { get; set; }

        [ForeignKey("LocalChamadaId")]
        public LocalChamada LocalChamada { get; set; }
    }
}
