﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha
{
    [Table("AteLocalChamada")]
    public class LocalChamada : CamposPadraoCRUD
    {
        public long? TipoLocalChamadaId { get; set; }

        [ForeignKey("TipoLocalChamadaId")]
        public TipoLocalChamada TipoLocalChamada { get; set; }
    }
}
