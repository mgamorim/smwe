﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha
{
    [Table("AteSenha")]
    public class Senha : CamposPadraoCRUD
    {
        public DateTime DataHora { get; set; }
        public int Numero { get; set; }
        public long FilaId { get; set; }
        public long? AtendimentoId { get; set; }

        [ForeignKey("FilaId")]
        public Fila Fila { get; set; }

        [ForeignKey("AtendimentoId")]
        public Atendimento Atendimento { get; set; }
    }
}
