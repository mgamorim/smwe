﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.CentralAutorizacoes
{
    [Table("AteAutorizacaoProcedimento")]
    public class AutorizacaoProcedimento : CamposPadraoCRUD
    {
        public long? SolicitanteId { get; set; }
        public long? PacienteId { get; set; }
        public long ConvenioId { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public string Observacao { get; set; }
        public string NumeroGuia { get; set; }

        [ForeignKey("SolicitanteId")]
        public Medico Solicitante { get; set; }

        [ForeignKey("PacienteId")]
        public Paciente Paciente { get; set; }

        [ForeignKey("ConvenioId")]
        public Convenio Convenio { get; set; }

    }
}
