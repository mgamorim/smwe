﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Atendimentos.CentralAutorizacoes
{
    [Table("AteComentarioAutorizacaoProcedimento")]
    public class ComentarioAutorizacaoProcedimento : CamposPadraoCRUD
    {
        public long AutorizacaoProcedimentoId { get; set; }
        public DateTime DataRegistro { get; set; }
        public string Conteudo { get; set; }
        public long? UsuarioId { get; set; }

        [ForeignKey("AutorizacaoProcedimentoId")]
        public AutorizacaoProcedimento AutorizacaoProcedimento { get; set; }
    }       
}
