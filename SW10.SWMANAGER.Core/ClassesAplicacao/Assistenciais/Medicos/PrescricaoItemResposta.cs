﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.FormasAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Frequencias;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos
{
    [Table("AssPrescricaoItemResposta")]
    public class PrescricaoItemResposta : CamposPadraoCRUD
    {
        public decimal? Quantidade { get; set; }

        [ForeignKey("Unidade"), Column("EstUnidadeId")]
        public long? UnidadeId { get; set; }

        [ForeignKey("VelocidadeInfusao"), Column("AssVelocidadeInfusaoId")]
        public long? VelocidadeInfusaoId { get; set; }

        [ForeignKey("FormaAplicacao"), Column("AssFormaAplicacaoId")]
        public long? FormaAplicacaoId { get; set; }

        [ForeignKey("Frequencia"), Column("AssFrequenciaId")]
        public long? FrequenciaId { get; set; }

        public bool IsSeNecessario { get; set; }

        public bool IsUrgente { get; set; }

        public bool IsDias { get; set; }

        [ForeignKey("UnidadeOrganizacional"), Column("SisUnidadeOrganizacionalId")]
        public long? UnidadeOrganizacionalId { get; set; }

        [ForeignKey("Medico"), Column("SisMedicoId")]
        public long? MedicoId { get; set; }

        public DateTime? DataInicial { get; set; }

        public double DiaAtual { get { return DataInicial.HasValue ? DateTime.Now.Subtract(DataInicial.Value).Days : 0; } }

        public int? TotalDias { get; set; }

        public string Observacao { get; set; }

        public VelocidadeInfusao VelocidadeInfusao { get; set; }

        public FormaAplicacao FormaAplicacao { get; set; }

        public Frequencia Frequencia { get; set; }

        public UnidadeOrganizacional UnidadeOrganizacional { get; set; }

        public Medico Medico { get; set; }

        public Unidade Unidade { get; set; }

        [ForeignKey("PrescricaoItem"), Column("AssPrescricaoItemId")]
        public long? PrescricaoItemId { get; set; }

        public PrescricaoItem PrescricaoItem { get; set; }

        [ForeignKey("Divisao"), Column("AssDivisaoId")]
        public long? DivisaoId { get; set; }

        public Divisao Divisao { get; set; }

        [ForeignKey("PrescricaoMedica"), Column("AssPrescricaoMedicaId")]
        public long? PrescricaoMedicaId { get; set; }

        public PrescricaoMedica PrescricaoMedica { get; set; }

        [ForeignKey("PrescricaoItemStatus"), Column("AssPrescricaoItemStatusId")]
        public long? PrescricaoItemStatusId { get; set; }
        public PrescricaoItemStatus PrescricaoItemStatus { get; set; }
    }
}
