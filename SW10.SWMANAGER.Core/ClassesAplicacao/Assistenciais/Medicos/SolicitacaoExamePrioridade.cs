﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos
{
    [Table("AssSolicitacaoExamePrioridade")]
    public class SolicitacaoExamePrioridade : CamposPadraoCRUD
    {
        [StringLength(30)]
        public override string Descricao { get; set; }
    }
}
