﻿using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos
{
    [Table("AssSolicitacaoExameItem")]
    public class SolicitacaoExameItem : CamposPadraoCRUD
    {
        [ForeignKey("Solicitacao"), Column("AssSolicitacaoExameId")]
        public long SolicitacaoExameId { get; set; }

        public SolicitacaoExame Solicitacao { get; set; }

        [ForeignKey("FaturamentoItem"), Column("FatItemId")]
        public long? FaturamentoItemId { get; set; }

        public FaturamentoItem FaturamentoItem { get; set; }

        public string GuiaNumero { get; set; }

        public DateTime? DataValidade { get; set; }

        public string SenhaNumero { get; set; }

        [ForeignKey("Material"), Column("LabMaterialId")]
        public long? MaterialId { get; set; }

        public Material Material { get; set; }

        public string Justificativa { get; set; }

        [ForeignKey("KitExame"), Column("LabKitExameId")]
        public long? KitExameId { get; set; }

        public KitExame KitExame { get; set; }

        [ForeignKey("StatusSolicitacaoExameItem"), Column("StatusSolicitacaoExameItemId")]
        public long? StatusSolicitacaoExameItemId { get; set; }

        public StatusSolicitacaoExameItem StatusSolicitacaoExameItem { get; set; }
    }
}
