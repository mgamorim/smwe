﻿using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Prestadores;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos
{
    [Table("AssPrescricaoMedica")]
    public class PrescricaoMedica : CamposPadraoCRUD
    {
        public DateTime DataPrescricao { get; set; }

        [ForeignKey("UnidadeOrganizacional"), Column("SisUnidadeOrganizacionalId")]
        public long? UnidadeOrganizacionalId { get; set; }

        [ForeignKey("Atendimento"), Column("AteAtendimentoId")]
        public long? AtendimentoId { get; set; }

        [ForeignKey("Prestador"), Column("SisPrestadorId")]
        public long? PrestadorId { get; set; }

        public string Observacao { get; set; }

        public UnidadeOrganizacional UnidadeOrganizacional { get; set; }

        public Atendimento Atendimento { get; set; }

        public Prestador Prestador { get; set; }

        [ForeignKey("PrescricaoStatus"), Column("AssPrescricaoStatusId")]
        public long? PrescricaoStatusId { get; set; }
        public PrescricaoStatus PrescricaoStatus { get; set; }

    }
}
