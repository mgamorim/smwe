﻿using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao
{
    [Table("SisRegistroArquivo")]
    public class RegistroArquivo : CamposPadraoCRUD
    {
        public long RegistroId { get; set; }
        public long? RegistroTabelaId { get; set; }
        public string Campo { get; set; }
        public byte[] Arquivo { get; set; }

        [ForeignKey("RegistroTabelaId")]
        public RegistroTabela RegistroTabela { get; set; }

        public long? AtendimentoId { get; set; }

        [ForeignKey("AtendimentoId")]
        public Atendimento Atendimento { get; set; }
    }
}
