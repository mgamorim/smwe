﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus
{
    [Table("AssPrescricaoStatus")]
    public class PrescricaoStatus : CamposPadraoCRUD, IDescricao
    {
        public string Cor { get; set; }
    }
}
