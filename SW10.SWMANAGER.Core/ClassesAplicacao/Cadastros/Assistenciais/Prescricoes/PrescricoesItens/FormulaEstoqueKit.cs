﻿using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesItens
{
    [Table("AssFormulaEstoqueKit")]
    public class FormulaEstoqueKit : CamposPadraoCRUD
    {
        [ForeignKey("PrescricaoItem"), Column("AssPrescricaoItemId")]
        public long PrescricaoItemId { get; set; }

        public PrescricaoItem PrescricaoItem { get; set; }

        [ForeignKey("EstoqueKit"), Column("AssEstoqueKitId")]
        public long EstoqueKitId { get;set; }

        public EstoqueKit EstoqueKit { get; set; }



    }
}
