﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Feriados
{
    [Table("Feriado")]
    public class Feriado : CamposPadraoCRUD
    {
        public DateTime? DiaMesAno { get; set; }

    }
}
