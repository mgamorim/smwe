﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.CEP;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Cidades;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Paises;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposLogradouro;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.VersoesTiss;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios
{
    [Table("SisConvenio")]
    public class Convenio : CamposPadraoCRUD
    {
        public Convenio()
        {
            //if(this.SisPessoa==null)
            //{
            //    this.SisPessoa = new SisPessoa();
            //} 
        }

        public  string NomeFantasia { get { return (this.SisPessoa != null) ? this.SisPessoa.NomeFantasia : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.NomeFantasia = value; } }


        public string RazaoSocial { get { return (this.SisPessoa != null) ? this.SisPessoa.RazaoSocial : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.RazaoSocial = value; } }


        public string Cnpj { get { return (this.SisPessoa != null) ? this.SisPessoa.Cnpj : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.Cnpj = value; } }

        public string InscricaoEstadual { get { return (this.SisPessoa != null) ? this.SisPessoa.InscricaoEstadual : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.InscricaoEstadual = value; } }

        public string InscricaoMunicipal { get { return (this.SisPessoa != null) ? this.SisPessoa.InscricaoMunicipal : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.InscricaoMunicipal = value; } }



        #region propridades de Pessoa

        [StringLength(9)]
        public string Cep { get; set; }

        //ACERTAR REFERENCIA
        public long? TipoLogradouroId { get; set; }

        [ForeignKey("TipoLogradouroId")]
        public TipoLogradouro TipoLogradouro { get; set; }

        [StringLength(80)]
        public string Logradouro { get; set; }

        [StringLength(30)]
        public string Complemento { get; set; }

        [StringLength(20)]
        public string Numero { get; set; }

        [StringLength(40)]
        public string Bairro { get; set; }

        [ForeignKey("CidadeId")]
        public Cidade Cidade { get; set; }
        public long? CidadeId { get; set; }

        [ForeignKey("EstadoId")]
        public Estado Estado { get; set; }
        public long? EstadoId { get; set; }

        [ForeignKey("PaisId")]
        public Pais Pais { get; set; }
        public long? PaisId { get; set; }

        [StringLength(20)]
        public string Telefone1 { get { return (this.SisPessoa != null) ? this.SisPessoa.Telefone1 : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.Telefone1 = value; } }

        public int? TipoTelefone1 { get; set; }

        public int? DddTelefone1 { get; set; }

        [StringLength(20)]
        public string Telefone2 { get { return (this.SisPessoa != null) ? this.SisPessoa.Telefone2 : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.Telefone2 = value; } }

        public int? TipoTelefone2 { get; set; }

        public int? DddTelefone2 { get; set; }

        [StringLength(20)]
        public string Telefone3 { get { return (this.SisPessoa != null) ? this.SisPessoa.Telefone3 : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.Telefone3 = value; } }

        public int? TipoTelefone3 { get; set; }

        public int? DddTelefone3 { get; set; }

        [StringLength(20)]
        public string Telefone4 { get { return (this.SisPessoa != null) ? this.SisPessoa.Telefone4 : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.Telefone4 = value; } }

        public int? TipoTelefone4 { get; set; }

        public int? DddTelefone4 { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get { return (this.SisPessoa != null) ? this.SisPessoa.Email : string.Empty; } set { if (this.SisPessoa != null) this.SisPessoa.Email = value; } }

        public string Email2 { get; set; }

        public string Email3 { get; set; }

        public string Email4 { get; set; }


        #endregion



        //CONFIRMAR NOME DO CAMPO.
        public string Nome { get; set; }

        public bool IsAtivo { get; set; }

        public byte[] Logotipo { get; set; }

        public string LogotipoMimeType { get; set; }

        public bool IsFilotranpica { get; set; }

        [StringLength(255)]
        public string LogradouroCobranca { get; set; }

        [ForeignKey("CepCobranca"), Column("SisCepCobrancaId")]
        public long? CepCobrancaId { get; set; }

        public Cep CepCobranca { get; set; }

        [ForeignKey("TipoLogradouroCobranca"), Column("SisTipoLogradouroCobrancaId")]
        public long? TipoLogradouroCobrancaId { get; set; }

        public TipoLogradouro TipoLogradouroCobranca { get; set; }

        public string NumeroCobranca { get; set; }

        public string ComplementoCobranca { get; set; }

        public string BairroCobranca { get; set; }

        [ForeignKey("CidadeCobranca"), Column("SisCidadeCobrancaId")]
        public long? CidadeCobrancaId { get; set; }

        public Cidade CidadeCobranca { get; set; }

        [ForeignKey("EstadoCobranca"), Column("SisEstadoCobrancaId")]
        public long? EstadoCobrancaId { get; set; }

        public Estado EstadoCobranca { get; set; }

        public string Cargo { get; set; }

        public DateTime DataInicioContrato { get; set; }

        public int Vigencia { get; set; }

        public DateTime DataProximaRenovacaoContratual { get; set; }

        public DateTime DataInicialContrato { get; set; }

        public DateTime DataUltimaRenovacaoContrato { get; set; }

        public string RegistroANS { get; set; }

        // Novo modelo SisPessoa
        // Novo modelo SisPessoa
        [ForeignKey("SisPessoa"), Column("SisPessoaId")]
        public long? SisPessoaId { get; set; }
        public SisPessoa SisPessoa { get; set; }

        public long? VersaoTissId { get; set; }

        [ForeignKey("VersaoTissId")]
        public VersaoTiss VersaoTiss { get; set; }

        public List<IdentificacaoPrestadorNaOperadora> IdentificacoesPrestadoresNaOperadora { get; set; }
    }
}
