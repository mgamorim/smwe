﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Enumeradores
{
    public enum EnumStatusLaudo
    {
        Registrado=1,
        ComParecer=2,
        ComLaudo=3,
        LaudoRevisado=4
    }
}
