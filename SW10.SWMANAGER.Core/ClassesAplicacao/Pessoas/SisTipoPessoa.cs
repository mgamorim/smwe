﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Pessoas
{
    [Table("SisTipoPessoa")]
    public class SisTipoPessoa : CamposPadraoCRUD
    {
        public bool IsReceber { get; set; }
        public bool IsPagar { get; set; }
        public bool IsAtivo { get; set; }
    }
}
