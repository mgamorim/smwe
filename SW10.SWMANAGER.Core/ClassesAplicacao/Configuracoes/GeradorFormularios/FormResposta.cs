﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios
{
    [Table("SisFormResposta")]
    public class FormResposta : CamposPadraoCRUD
    {
        public DateTime DataResposta { get; set; }

        public long FormConfigId { get; set; }

        public string NomeClasse { get; set; }

        public string RegistroClasseId { get; set; }

        [ForeignKey("FormConfigId")]
        public FormConfig FormConfig { get; set; }

        public List<FormData> ColRespostas { get; set; }

    }
}