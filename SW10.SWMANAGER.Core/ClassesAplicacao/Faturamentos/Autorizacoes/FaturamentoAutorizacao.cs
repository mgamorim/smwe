﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.SubGrupos;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Autorizacoes
{
    [Table("FatAutorizacao")]
    public class FaturamentoAutorizacao : CamposPadraoCRUD
    {
        public string    Mensagem        { get; set; }
        public DateTime  DataInicial     { get; set; }
        public DateTime? DataFinal       { get; set; }
        public bool      IsAmbulatorio   { get; set; }
        public bool      IsInternacao    { get; set; }
        public bool      IsAutorizacao   { get; set; }
        public bool      IsLiberacao     { get; set; }
        public bool      IsJustificativa { get; set; }
        public bool      IsBloqueio      { get; set; }
    }
}


