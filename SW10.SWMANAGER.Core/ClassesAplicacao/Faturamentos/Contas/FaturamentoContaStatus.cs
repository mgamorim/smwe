﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas
{
	[Table("FatContaStatus")]
	public class FaturamentoContaStatus : CamposPadraoCRUD
	{
		[StringLength(10)]
		public override string Codigo { get; set; }

		[StringLength(255)]
		public override string Descricao { get; set; }

        [StringLength(7)]
        public string Cor { get; set; }
    }
}
