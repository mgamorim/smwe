﻿using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Faturamentos
{
    [Table("FatPacote")]
    public class FaturamentoPacote : CamposPadraoCRUD
    {
        public DateTime Inicio { get; set; }
        public DateTime Final { get; set; }

        public long? FaturamentoItemId { get; set; }

        [ForeignKey("FaturamentoItemId")]
        public FaturamentoItem FaturamentoItem { get; set; }

        public long? FaturamentoContaId { get; set; }

        [ForeignKey("FaturamentoContaId")]
        public FaturamentoConta FaturamentoConta { get; set; }
    }
}
