﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Planos;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasApresentacoes;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasItens;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasLaboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Kits;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.SubGrupos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens
{
    [Table("FatItemConfigGlobal")]
    public class FaturamentoItemConfigGlobal : CamposPadraoCRUD
    {
        [ForeignKey("Global"), Column("GlobalId")]
        public long? GlobalId { get; set; }
        public FaturamentoGlobal Global { get; set; }

        [ForeignKey("Item"), Column("ItemId")]
        public long? ItemId { get; set; }
        public FaturamentoItem Item { get; set; }
        
        [ForeignKey("ItemCobrar"), Column("ItemCobrarId")]
        public long? ItemCobrarId { get; set; }
        public FaturamentoItem ItemCobrar { get; set; }
    }

}


