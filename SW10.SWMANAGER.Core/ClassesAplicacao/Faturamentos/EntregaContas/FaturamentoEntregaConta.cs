﻿using SW10.SWMANAGER.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas
{
    [Table("FatEntregaConta")]
    public class FaturamentoEntregaConta : CamposPadraoCRUD
    {
        [ForeignKey("ContaMedica"), Column("FatContaMedicaId")]
        public long? ContaMedicaId { get; set; }
        public FaturamentoConta ContaMedica { get; set; }

        [ForeignKey("EntregaLote"), Column("FatEntregaLoteId")]
        public long? EntregaLoteId { get; set; }
        public FaturamentoEntregaLote EntregaLote { get; set; }

        public float ValorConta { get; set; }
        public float ValorTaxas { get; set; }
        public float ValorFranquia { get; set; }
        public float ValorProduzido { get; set; }
        public float ValorProduzidoTaxas { get; set; }
        public float ValorRecebido { get; set; }
        public float ValorRecebidoTemp { get; set; }

        public bool IsGlosa { get; set; }
        public bool IsRecebe { get; set; }
        public bool IsRecebeTudo { get; set; }
        public bool IsErroGuia { get; set; }

        //[ForeignKey("GlosaInterna"), Column("GlosaInternaId")]
        //public long? GlosaInternaId { get; set; }
        //public GlosaInterna GlosaInterna { get; set; }

        //[ForeignKey("Quitacao"), Column("QuitacaoId")]
        //public long? QuitacaoId { get; set; }
        //public Quitacao Quitacao { get; set; }
        
        [DataType(DataType.DateTime)]
        public DateTime? DataEntrega { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DataFinalEntrega { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DataUsuarioEntrega { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DataUsuarioTemp { get; set; }

        public long? UsuarioTempId { get; set; }

        public long? UsuarioEntregaId { get; set; }
        //[ForeignKey("UsuarioEntrega"), Column("FatUsuarioEntregaId")]
        //public long? UsuarioEntregaId { get; set; }
        //public User UsuarioEntrega { get; set; }
    }

}


