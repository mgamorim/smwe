﻿using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas
{
    [Table("FatEntregaLote")]
    public class FaturamentoEntregaLote : CamposPadraoCRUD
    {
        [ForeignKey("Empresa"), Column("SisEmpresaId")]
        public long? EmpresaId { get; set; }
        public Empresa Empresa { get; set; }

        [ForeignKey("Convenio"), Column("SisConvenioId")]
        public long? ConvenioId { get; set; }
        public Convenio Convenio { get; set; }

        public string CodEntregaLote { get; set; }
        public string NumeroProcesso { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DataInicial { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DataFinal { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? DataEntrega { get; set; }

        public float ValorFatura { get; set; }
        public float ValorTaxas { get; set; }

        public bool IsAmbulatorio { get; set; }
        public bool IsInternacao { get; set; }
        public bool Desativado { get; set; }

        [ForeignKey("UsuarioLote"), Column("FatUsuarioLoteId")]
        public long? UsuarioLoteId { get; set; }
        public User UsuarioLote { get; set; }
    }

}


