﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.VersoesTiss;

namespace SW10.SWMANAGER.ClassesAplicacao
{
	public abstract class VersaoTissRelacao : Entity<long>
	{
		public long VersaoTissId { get; set; }

		public bool Incluido { get; set; }

		public bool Excluido { get; set; }

		[ForeignKey("VersaoTissId")]
		public VersaoTiss VersaoTiss { get; set; }
	}
}
