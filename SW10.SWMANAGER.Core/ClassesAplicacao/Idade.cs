﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao
{
    public class Idade
    {
        public int Ano { get; set; }
        public int Mes { get; set; }
        public int Dia { get; set; }
        public string Mensagem { get; set; }
    }
}
