﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinCheque")]
    public class Cheque : CamposPadraoCRUD
    {
        public long TalaoChequeId { get; set; }

        [ForeignKey("TalaoChequeId")]
        public TalaoCheque TalaoCheque { get; set; }

        public long Numero { get; set; }
        public string Nominal { get; set; }
        public DateTime? Data { get; set; }

    }
}
