﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinTalaoCheque")]
    public class TalaoCheque : CamposPadraoCRUD
    {
        public long ContaCorrenteId { get; set; }

        [ForeignKey("ContaCorrenteId")]
        public ContaCorrente ContaCorrente { get; set; }

        public DateTime DataAbertura { get; set; }
        public int NumeroInicial { get; set; }
        public int NumeroFinal { get; set; }
    }
}
