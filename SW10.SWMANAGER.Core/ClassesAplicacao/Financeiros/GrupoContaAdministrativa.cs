﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinGrupoContaAdministrativa")]
    public class GrupoContaAdministrativa : CamposPadraoCRUD
    {
        public bool IsValorIncideResultadoOperacionalEmpresa { get; set; }

        public long? GrupoDREId { get; set; }

        [ForeignKey("GrupoDREId")]
        public GrupoDRE GrupoDRE { get; set; }

        public List<SubGrupoContaAdministrativa> SubGruposContasAdministrativa { get; set; }
    }
}
