﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinRateioCentroCusto")]
    public class RateioCentroCusto : CamposPadraoCRUD
    {
        public List<RateioCentroCustoItem> RateioCentroCustoItens { get; set; }
    }
}
