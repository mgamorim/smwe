﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinDocumento")]
    public class Documento : CamposPadraoCRUD
    {
        public long TipoDocumentoId { get; set; }

        [ForeignKey("TipoDocumentoId")]
        public TipoDocumento TipoDocumento { get; set; }

        public long EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        public Empresa Empresa { get; set; }

        public long? PessoaId { get; set; }

        [ForeignKey("PessoaId")]
        public SisPessoa Pessoa { get; set; }

        public string Numero { get; set; }
        public DateTime? DataEmissao { get; set; }
        public bool IsCredito { get; set; }
        public decimal? ValorDocumento { get; set; }
        public decimal? ValorAcrescimoDecrescimo { get; set; }
        public string Observacao { get; set; }
        public int QuantidadeParcelas { get; set; }

        public List<Lancamento> LancamentoDocumentos { get; set; }
        public List<DocumentoRateio> Rateios { get; set; }


    }
}
