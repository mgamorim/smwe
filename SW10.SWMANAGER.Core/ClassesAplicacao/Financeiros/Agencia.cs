﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinAgencia")]
    public class Agencia: CamposPadraoCRUD
    {
        public long BancoId { get; set; }

        [ForeignKey("BancoId")]
        public Banco Banco { get; set; }
    }
}
