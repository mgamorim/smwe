﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinLancamento")]
    public class Lancamento : CamposPadraoCRUD
    {
        public long? DocumentoId { get; set; }

        [ForeignKey("DocumentoId")]
        public Documento Documento { get; set; }

        public DateTime? DataVencimento { get; set; }
        public decimal? ValorLancamento { get; set; }
        public decimal? ValorAcrescimoDecrescimo { get; set; }
        public decimal? Juros { get; set; }
        public decimal? Multa { get; set; }

        public long SituacaoLancamentoId { get; set; }

        [ForeignKey("SituacaoLancamentoId")]
        public SituacaoLancamento SituacaoLancamento { get; set; }

        //public long? DocumentoRelacionadoId { get; set; }

        //[ForeignKey("DocumentoRelacionadoId")]
        //public Documento DocumentoRelacionado { get; set; }

        public int? MesCompetencia { get; set; }
        public int? AnoCompetencia { get; set; }
        public bool IsCredito { get; set; }
        public DateTime? DataLancamento { get; set; }
        public int Parcela { get; set; }

        public string NossoNumero { get; set; }
        public string CodigoBarras { get; set; }
        public string LinhaDigitavel { get; set; }

        public List<LancamentoQuitacao> LancamentosQuitacoes { get; set; }

    }
}
