﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Financeiros
{
    [Table("FinQuitacao")]
    public class Quitacao : CamposPadraoCRUD
    {
        public long ContaCorrenteId { get; set; }

        [ForeignKey("ContaCorrenteId")]
        public  ContaCorrente ContaCorrente { get; set; }

        public long MeioPagamentoId { get; set; }

        [ForeignKey("MeioPagamentoId")]
        public MeioPagamento MeioPagamento { get; set; }

        public string Numero { get; set; }
        public DateTime? DataMovimento { get; set; }
        public DateTime? DataCompensado { get; set; }
        public DateTime? DataConsolidado { get; set; }
        
        public string Observacao { get; set; }

        public long? ChequeId { get; set; }

        [ForeignKey("ChequeId")]
        public Cheque Cheque { get; set; }

        public List<LancamentoQuitacao> LancamentoQuitacoes { get; set; }
       
    }
}
