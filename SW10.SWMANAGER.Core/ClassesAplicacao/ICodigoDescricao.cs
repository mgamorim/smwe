﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao
{
    public interface ICodigoDescricao
    {
        string Codigo { get; set; }
        string Descricao { get; set; }
    }
}
