﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    [Table("ProdutoSaldoEmprestimo")]
    public class ProdutoSaldoEmprestimo : CamposPadraoCRUD
    {
        public long ProdutoId { get; set; }
        public long FornecedorId { get; set; }
        public decimal QuantidadeAtual { get; set; }

        [ForeignKey("ProdutoId")]
        public  Produto Produto { get; set; }

        [ForeignKey("FornecedorId")]
        public  Fornecedor Fornecedor { get; set; }
    }
}
