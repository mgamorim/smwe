﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    [Table("Est_Grupo")]
    public class Grupo : CamposPadraoCRUD
    {
        /// <summary>
        /// Coleção de classes que pertencem ao Grupo
        /// </summary>
        public ICollection<GrupoClasse> Classes { get; set; }
    }
}
