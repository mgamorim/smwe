﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    [Table("Est_GrupoSubClasse")]
    public class GrupoSubClasse : CamposPadraoCRUD
    {
        /// <summary>
        /// Classe
        /// </summary>
        public long GrupoClasseId { get; set; }
        [ForeignKey("GrupoClasseId")]
        public GrupoClasse Classe { get; set; }
    }
}
