﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    [Table("EstKitItem")]
    public class EstoqueKitItem : CamposPadraoCRUD
    {
        public long? EstoqueKitId { get; set; }

        [ForeignKey("EstoqueKitId")]
        public EstoqueKit EstoqueKit { get; set; }

        public int Quantidade { get; set; }

        public long ProdutoId { get; set; }

        [ForeignKey("ProdutoId")]
        public Produto Produto { get; set; }

        public long UnidadeId { get; set; }

        [ForeignKey("UnidadeId")]
        public Unidade Unidade { get; set; }
    }
}
