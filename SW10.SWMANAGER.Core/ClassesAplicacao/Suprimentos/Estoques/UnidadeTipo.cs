﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    [Table("Est_UnidadeTipo")]
    public class UnidadeTipo : CamposPadraoCRUD
    {
        /// <summary>
        /// Descrição do tipo de unidade
        /// (Referência, Gerencial, Compras, Entreda, Estoque)
        /// </summary>
    }
}
