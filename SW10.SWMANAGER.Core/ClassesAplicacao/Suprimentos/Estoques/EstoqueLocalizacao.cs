﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    [Table("Est_EstoqueLocalizacao")]
    public class EstoqueLocalizacao : CamposPadraoCRUD
    {
        /// <summary>
        /// Estoque
        /// </summary>
        public long EstoqueId { get; set; }
        [ForeignKey("EstoqueId")]
        public Estoque Estoque { get; set; }
    }
}
