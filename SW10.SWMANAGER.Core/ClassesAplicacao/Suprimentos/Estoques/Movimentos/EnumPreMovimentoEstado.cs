﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{
    public enum EnumPreMovimentoEstado
    {
        Autorizado = 1,
        Conferencia = 2,
        NaoAutorizado = 3,
        Pendente = 4,
        ParcialmenteAtendido = 5,
        TotalmenteAtendido = 6,
        ParcialmentoSuspenso = 7,
        TotalmenteSuspenso=8
    }
}
