﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{
    [Table("EstoquePreMovimentoLoteValidade")]
    public class EstoquePreMovimentoLoteValidade : CamposPadraoCRUD
    {

        public long EstoquePreMovimentoItemId { get; set; }

        public long LoteValidadeId { get; set; }

        public decimal Quantidade { get; set; }

        [ForeignKey("EstoquePreMovimentoItemId")]
        public  EstoquePreMovimentoItem EstoquePreMovimentoItem { get; set; }

        [ForeignKey("LoteValidadeId")]
        public  LoteValidade LoteValidade { get; set; }

    }
}
