﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Produtos;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{
    [Table("EstoquePreMovimentoItem")]
    public class EstoquePreMovimentoItem : CamposPadraoCRUD
    {
        public long PreMovimentoId { get; set; }
        public long ProdutoId { get; set; }
        public decimal Quantidade { get; set; }
        public string NumeroSerie { get; set; }
        public decimal CustoUnitario { get; set; }
        public string CodigoBarra { get; set; }
        public long? ProdutoUnidadeId { get; set; }
        public decimal PerIPI { get; set; }
        public long? PreMovimentoItemEstadoId { get; set; }


        [ForeignKey("ProdutoId")]
        public  Produto Produto { get; set; }

        [ForeignKey("PreMovimentoId")]
        public  EstoquePreMovimento EstoquePreMovimento { get; set; }

        [ForeignKey("ProdutoUnidadeId")]
        public  Unidade ProdutoUnidade { get; set; }

        [ForeignKey("PreMovimentoItemEstadoId")]
        public EstoquePreMovimentoEstado PreMovimentoItemEstado { get; set; }


    }
}
