﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{
    public enum EnumTipoMovimento
    {
        NotaFiscal_Entrada=1,
        Setor_Saida=2,
        Paciente_Saida=3,
        Perda_Saida=4,
        GastoSala_Saida=5,
        Emprestimo_Saida=6,
        Doacao_Entrada=7,
        Vale_Entrada=8,
        Emprestimo_Entrada=9,
        Consignado_Entrada=10
    }
}
