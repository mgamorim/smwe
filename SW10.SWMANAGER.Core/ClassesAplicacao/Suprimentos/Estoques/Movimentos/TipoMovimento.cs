﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{

    [Table("EstTipoMovimento")]
    public class TipoMovimento : CamposPadraoCRUD
    {
        public bool IsEntrada { get; set; }
    }
}
