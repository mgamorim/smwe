﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{
    [Table("LoteValidade")]
    public class LoteValidade : CamposPadraoCRUD
    {
        public string Lote { get; set; }
        public DateTime Validade { get; set; }

        public long ProdutoId { get; set; }

        [ForeignKey("ProdutoId")]
        public  Produto Produto { get; set; }

        [ForeignKey("EstoqueLaboratorio"), Column("EstEstoqueLaboratorioId")]
        public long? EstLaboratorioId { get; set; }
        public EstoqueLaboratorio EstoqueLaboratorio { get; set; }

    }
}
