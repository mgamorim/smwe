﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos
{
    public enum EnumTipoOperacao
    {
        Entrada=1,
        Transferencia=2,
        Saida=3,
        Devolucao=4,
        ProducaoKit=5,
        Solicitacao=6,
        Inventario=7
    }
}
