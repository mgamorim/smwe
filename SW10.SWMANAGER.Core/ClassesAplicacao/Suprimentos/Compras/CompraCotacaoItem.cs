﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Compras
{
    [Table("CmpCotacaoItem")]
    public class CompraCotacaoItem : CamposPadraoCRUD
    {
        [ForeignKey("RequisicaoItem"), Column("CmpRequisicaoItemId")]
        public long RequisicaoItemId { get; set; }
        public CompraRequisicaoItem RequisicaoItem { get; set; }

        /// <summary>
        /// Unidade relacionada ao produto
        /// </summary>
        [ForeignKey("Fornecedor"), Column("FornecedorId")]
        public long FornecedorId { get; set; }
        public Fornecedor Fornecedor { get; set; }

        /// <summary>
        /// Preco informado pelo fornecedor
        /// </summary>
        public decimal Preco { get; set; }

    }
}