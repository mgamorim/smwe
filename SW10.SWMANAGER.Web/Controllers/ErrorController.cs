﻿using System.Web.Mvc;
using Abp.Auditing;

namespace SW10.SWMANAGER.Web.Controllers
{
    public class ErrorController : SWMANAGERControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}