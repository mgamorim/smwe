﻿using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.UI;
using Abp.Web.Mvc.Controllers;
using Microsoft.AspNet.Identity;
using SW10.SWMANAGER.ClassesAplicacao.Manutencoes.BIs;
using SW10.SWMANAGER.ClassesAplicacao.Services.Manutencoes.BIs;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// Add your methods to this class common for all controllers.
    /// </summary>
    public abstract class SWMANAGERControllerBase : AbpController
    {
        protected SWMANAGERControllerBase()
        {
            LocalizationSourceName = SWMANAGERConsts.LocalizationSourceName;
        }

        protected void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                throw new UserFriendlyException(L("FormIsNotValidMessage"));
            }
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

    }
}