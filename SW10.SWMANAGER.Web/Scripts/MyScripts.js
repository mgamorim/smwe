﻿(function ($) {

    // Comandos executados automaticamente ao carregar paginas princiapais (layout global)
    //CamposRequeridos();
    //$('#exibir-bi-' + $('#bi-id').val()).on('click', function (e) {
    //    e.preventDefault();
    //    //console.log($('#frame-relatorio-despesas').attr('src'));
    //    if ($('#frame-bi-' + $('#bi-id').val()).attr('src') == undefined) {
    //        $('#bi-' + $('#bi-id').val()).removeClass('hidden');
    //        $('#frame-bi-' + $('#bi-id').val()).attr('src', $('#bi-url').val());
    //    }
    //    else {
    //        $('#bi-' + $('#bi-id').val()).addClass('hidden');
    //        $('#frame-bi-' + $('#bi-id').val()).attr('src', null);
    //    }
    //});

})(jQuery);

function SomenteNumero(campo, aDigits) {
    var digits = ["0123456789,", "0123456789ESAOesao", "0123456789", "0123456789:", "0123456789/", "0123456789-", "0123456789."];
    var campo_temp;
    var nomeCampo = '#' + campo.id;
    //for (var i = 0; i < campo.value.length; i++) {
    for (var i = 0; i < $(nomeCampo).val().length; i++) {
        campo_temp = $(nomeCampo).val().substring(i, i + 1);
        if (digits[aDigits].indexOf(campo_temp) == -1) {
            $(nomeCampo).val($(nomeCampo).val().substring(0, i));
            break;
        }
    }
}

function zeroEsquerda(num, length) {
    var str = "" + num
    var pad = ""
    for (var i = 0; i < length; i++) {
        pad += "0";
    }
    var ans = pad.substring(0, pad.length - str.length) + str
    return ans;
}

function ShowModal(url, title) {
    $('#SWManagerAlertModal .modal-body p').load(url, function () {
        $('#SWManagerAlertModal .modal-header h4').html(title);
        $('#SWManagerAlertModal').modal('show');
    });
}

function buscarCep(string) {
    $.ajax({
        data: {
            'cep': string
        },
        dataType: 'json',
        contentType: 'application/json',
        dataContext: 'application/json; charset=utf-8',
        url: '/mpa/Ceps/ConsultaCep',
        async: true,
        type: 'GET',
        cache: false,
        success: function (data) {
            var result = JSON.stringify(data.result);
            ////console.log(result);
            if (result.indexOf('ERRO') !== -1) {
                abp.notify.error(app.localize("Erro") + "<br />" + data.result.replace('ERRO:', ''));
                return false;
            }
            else {
                $('#cbo-paisid').append('<option value="' + data.result.paisId + '">' + data.result.estado.pais.nome + '</option>');
                $('#cbo-paisid').val(data.result.paisId);
                $('#cbo-paisid').trigger("change");

                $('#cbo-estadoid').append('<option value="' + data.result.estadoId + '">' + data.result.estado.nome + '</option>');
                $('#cbo-estadoid').val(data.result.estadoId);
                $('#cbo-estadoid').trigger("change");

                $('#cbo-cidadeid').append('<option value="' + data.result.cidadeId + '">' + data.result.cidade.nome + '</option>');
                $('#cbo-cidadeid').val(data.result.cidadeId);
                $('#cbo-cidadeid').trigger("change");

                $('#pais-search').val(data.result.estado.pais.nome).trigger("chosen:updated");
                $('#estado-search').val(data.result.estado.nome).trigger("chosen:updated");
                $('#cidade-search').val(data.result.cidade.nome).trigger("chosen:updated");

                $('#bairro').val(data.result.bairro).addClass("edited");
                $('#pais-id').val(data.result.paisId).trigger("chosen:updated");
                $('#estado-id').val(data.result.estadoId).trigger("chosen:updated");
                $('#cidade-id').val(data.result.cidadeId).trigger("chosen:updated");
                $('#logradouro').val(data.result.logradouro).addClass("edited");
                var _tipoLogradouroService = abp.services.app.tipoLogradouro;
                var _tipoLogradouro = _tipoLogradouroService.obter(data.result.tipoLogradouro.descricao)
                    .done(function (data) {
                        $('#tipo-logradouro-id')
                            .append('<option value="' + data.id + '" selected>' + data.descricao + '</option>')
                            .trigger("chosen:updated");
                    });
            }
        },
        beforeSend: function () {
            $('#btn-buscar-cep').buttonBusy(true);
        },
        complete: function () {
            $('#btn-buscar-cep').buttonBusy(false);

        }
    });
}

function IsDate(dateStr) {
    var matchArray = dateStr.match(datePat); // is the format ok?
    if (matchArray == null) {
        return false;
    }
    var locale = moment.locale();
    switch (locale) {
        case 'pt-br':
        case 'pt-BR':
        case 'PT-BR':
        case 'pt-Br':
            var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var matchArray = dateStr.match(datePat); // is the format ok?
            if (matchArray == null) {
                return false;
            }
            month = matchArray[3]; // p@rse date into variables
            day = matchArray[1];
            year = matchArray[5];
            break;
        case 'en':
        case 'En':
        case 'EN':
        case 'en-us':
        case 'En-US':
        case 'EN-US':
        case 'en-US':
        case 'en-Us':
            var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
            var matchArray = dateStr.match(datePat); // is the format ok?
            if (matchArray == null) {
                return false;
            }
            month = matchArray[1]; // p@rse date into variables
            day = matchArray[3];
            year = matchArray[5];
            break;
        default:
            var datePat = /^(\d{1,4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
            var matchArray = dateStr.match(datePat); // is the format ok?
            if (matchArray == null) {
                return false;
            }
            month = matchArray[5]; // p@rse date into variables
            day = matchArray[7];
            year = matchArray[1];
    }
    if (month < 1 || month > 12) { // check month range
        return false;
    }
    if (day < 1 || day > 31) {
        return false;
    }
    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
        return false;
    }
    if (month == 2) { // check for february 29th
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap)) {
            return false;
        }
    }
    return true; // date is valid
}

function barraData(n) {
    if (n.value.length == 2)
        c.value += '/';
    if (n.value.length == 5)
        c.value += '/';
}

function updateCriarOuEditarAgendamentoConsultaViewModel() {
    var myDate = $('input[name="DataAgendamento"]').val();
    var aDate = myDate.split('/');
    var oldMedicoId = $('#form-medico-id').length > 0 ? $('#form-medico-id') : 0;
    var oldMedicoEspecialidadeId = $('#form-medico-especialidade-id').length > 0 ? $('#form-medico-especialidade-id').val() : 0;
    var oldHoraAgendamento = $('#form-hora-agendamento').length > 0 ? $('#form-hora-agendamento').val() : '00:00';
    var oldAgendamentoConsultaMedicoDisponibilidadeId = $('#form-agendamento-consulta-medico-disponibilidade-id').length > 0 ? $('#form-agendamento-consulta-medico-disponibilidade-id').val() : 0;

    $('#div-medicos').load(
        '/mpa/AgendamentoConsultas/_MontarComboMedicos',
        {
            date: aDate[2] + '-' + aDate[1] + '-' + aDate[0]
        },
        function () {
            if (oldMedicoId > 0) {
                $('#form-medico-id').val(oldMedicoId);
            }
            $('.chosen-select').chosen({ no_results_text: app.localize("NotFound"), width: '100%' });
            $('#form-medico-id').on('change', function (e) {
                e.preventDefault();
                var id = $(this).val();
                var myDate = $('input[name="DataAgendamento"]').val();
                var aDate = myDate.split('/');
                $('#div-medico-especialidades').load('/mpa/AgendamentoConsultas/_MontarComboMedicoEspecialidades',
                    {
                        medicoId: id,
                        date: aDate[2] + '-' + aDate[1] + '-' + aDate[0]
                    },
                    function () {
                        if (oldMedicoEspecialidadeId > 0) {
                            $('#form-medico-especialidade-id').val(oldMedicoId);
                        }
                        $('.chosen-select').chosen({ no_results_text: app.localize("NotFound"), width: '100%' }).trigger("chosen:updated");
                        $('#form-medico-especialidade-id').on('change', function (e) {
                            e.preventDefault();
                            var medicoEspecialidadeId = $('#form-medico-especialidade-id').val();
                            var id = $('#id').length > 0 ? $('#id').val() : 0;
                            var myDate = $('input[name="DataAgendamento"]').val();
                            var aDate = myDate.split('/');
                            var medicoId = $('#form-medico-id').val();
                            $('#div-horarios').load('/mpa/AgendamentoConsultas/_MontarComboHorarios',
                                {
                                    date: aDate[2] + '-' + aDate[1] + '-' + aDate[0],
                                    medicoId: medicoId,
                                    medicoEspecialidadeId: medicoEspecialidadeId,
                                    id: id
                                },
                                function () {
                                    if (oldAgendamentoConsultaMedicoDisponibilidadeId > 0) {
                                        $('#agendamento-consulta-medico-disponibilidade-id')
                                            .val(
                                            $('#agendamento-consulta-medico-disponibilidade-id option')
                                                .filter(function () {
                                                    return $(this).html() === oldHoraAgendamento;
                                                })
                                                .val()
                                            );
                                    }
                                    $('.chosen-select').chosen({ no_results_text: app.localize("NotFound"), width: '100%' });
                                    $('#form-agendamento-consulta-medico-disponibilidade-id').on('change', function (e) {
                                        e.preventDefault();
                                        var hora = $('#form-agendamento-consulta-medico-disponibilidade-id option:selected').text();
                                        var data = $('#form-data-agendamento').val();
                                        $('#form-hora-agendamento').val(data + ' ' + hora);
                                    });
                                }
                            );

                        });
                    }
                );
            });
        }
    );

}

function lerAtendimentoAmbulatorioEmergencia(id, origem, tipoAtendimento) {
    $('li a').attr('aria-expanded', 'false');
    $('#abas-' + tipoAtendimento + ' li').removeClass('active')
    $('#conteudo-abas-' + tipoAtendimento + ' div.tab-pane').removeClass('active')
    var url = origem == "assistencial" ? '/mpa/assistenciais/_leratendimento/' : 'Mpa/Atendimentos/Index?abaId=' + id;
    $('#conteudo-atendimento-' + id).load(url + id, function () {
        $('#link-atendimento-' + id).attr('aria-expanded', 'true');
        $(this).addClass("active")
        $('#conteudo-atendimento-' + id).addClass('active');
        localStorage["AtendimentoId"] = id;
        localStorage["TargetAssistencial"] = '#conteudo-atendimento-' + id;
    });
}

$('#abas-amb').on('click', ' li a .fa-close', function () {
    //    $('#abas-amb li').removeClass('active').removeAttr('aria-expanded');
    //    $('#conteudo-abas-amb div.tab-pane').removeClass('active').removeAttr('aria-expanded');
    var tabId = $(this).parents('li').children('a').attr('href');
    $(this).parents('li').remove('li').remove('div.tab-pane');
    $(tabId).remove();
    $('#link-principal-amb').trigger('click'); //.tab('show');
    var aId = tabId.split('-');
    var id = aId[1];
    $('#conteudo-atendimento-' + id).remove();
    //    ////console.log('Removido');
    $('.jtable-row-selected').removeClass('jtable-row-selected');
    $(':checked').removeAttr('checked');
    localStorage.removeItem("AtendimentoId");
});

$('#abas-int').on('click', ' li a .fa-close', function () {
    //    $('#abas-int li').removeClass('active').removeAttr('aria-expanded');
    //    $('#conteudo-abas-int div.tab-pane').removeClass('active').removeAttr('aria-expanded');
    var tabId = $(this).parents('li').children('a').attr('href');
    $(this).parents('li').remove('li').remove('div.tab-pane');
    $(tabId).remove();
    $('#link-principal-int').trigger('click'); //.tab('show');
    var aId = tabId.split('-');
    var id = aId[1];
    $('#conteudo-atendimento-' + id).remove();
    //    ////console.log('Removido');
    $('.jtable-row-selected').removeClass('jtable-row-selected');
    $(':checked').removeAttr('checked');
    localStorage.removeItem("AtendimentoId");
});

function atualizarAtendimento(id) {
    localStorage["AtendimentoId"] = id;
    localStorage["TargetAssistencial"] = '#conteudo-atendimento-' + id;
}

function fecharAtendimentoAmbulatorioEmergencia(id) {
    $('#atendimento-' + id).detach();
    $('#conteudo-atendimento-' + id).detach();
    //alert($('#abas a:first').attr('id'));
    if ($('#abas-amb').length > 0) {
        $('#abas-amb a:first').tab('show');
    }
    else {
        $('#abas-int a:first').tab('show');
    }
    localStorage.removeItem("AtendimentoId");
}

$('.favorito').on('click', function (e) {
    e.preventDefault();

    var _icon = $(this).attr('data-icone');
    var _nome = $(this).find('#fav-nome').val();
    var _url = $(this).find('#fav-url').val();

    var _data = {
        "UserId": 0,
        "DisplayName": _nome,
        "Icon": _icon,
        "Name": _nome,
        "Url": _url
    }

    ////console.log(JSON.stringify(_data));

    $.ajax({
        type: "POST",
        url: '/Mpa/Layout/Favoritar',
        data: _data,
        success: function (result) {
            if (result == '1') {
                $('.favorito span').removeClass('fa-star-o').addClass('fa-star');
            }
            else if (result == '0') {
                $('.favorito span').removeClass('fa-star').addClass('fa-star-o');
            }
            else {

            }
        },
    });
});

// Imagens
function lerImagemForm(input, dataField, mimeTypeField, imageTag) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var dados = {};
            var base64 = e.target.result;
            dados.base64 = base64.substr(base64.indexOf(',') + 1, base64.length);
            var type = base64.substr(base64.indexOf(':') + 1, base64.indexOf(';') - 5);
            $('#' + dataField).val(dados.base64);
            $('#' + mimeTypeField).val(type);
            $('#' + imageTag).attr({
                'src': 'data:' + type + ';base64,' + dados.base64
            });
        }
        reader.readAsDataURL(input.files[0]);
    }
}

// Gerenciamento de abas
function LerParcial(id, url, parametro) {
    if ($('#' + id).data('reload') == '0') {
        $('#' + id).data('reload', '1');
        localStorage["TargetConteudo"] = id;
        $.ajaxSetup({ cache: true, async: true });
        $('#' + id).load(url);
    }
}

// Insere asterisco em todos os campos requeridos (Rodrigo - 06/07/17)
// Funciona para elmentos cuja label esteja imediatamente antes do form-control
function CamposRequeridos() {
    $('.form-control').each(function () {

        // convertendo para elemento javascript para usar funcao hassAttribute
        var elementoJS = $(this)[0];
        var requerido = elementoJS.hasAttribute("required");

        if (requerido) {
            // revertendo para elemento Jquery para usar funcao prev e after
            var elementoJquery = $(elementoJS);
            elementoJquery.prevAll('label').after("<b> *</b>");
        }
    });
}

function CriarAutoComplete(idSearch, idCampo, url, cadastro) {

    var search = '#' + idSearch;
    var campo = '#' + idCampo;

    $(search)
        .autocomplete({
            minLength: 3,
            delay: 0,
            source: function (request, response) {
                var term = $(search).val();
                // var url = ;
                var fullUrl = url + '/?term=' + term;
                $.getJSON(fullUrl, function (data) {
                    if (data.length == 0) {
                        $(campo).val(0);
                        $(search).focus();
                        abp.notify.info(app.localize("ListaVazia"));
                        return false;
                    };
                    response($.map(data, function (item) {
                        $(campo).val(0);
                        return {
                            label: item.Nome,
                            value: item.Nome,
                            realValue: item.Id,

                        };
                    }));
                });
            },
            select: function (event, ui) {
                $(campo).val(ui.item.realValue);
                $(search).val(ui.item.value);
                //$('.save-button').removeAttr('disabled');
                return false;
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    //$('.save-button').attr('disabled', 'disabled');
                    $(campo).val(0);
                    $(search).val('').focus();
                    abp.notify.info(app.localize("AutoCompleteInvalido").replace('$cadastro', cadastro));
                    return false;
                }
            },
        });
}

function posicionarDireita(str) {
    return "<div style=\"float: right;\">" + str + "</div>";

}

function newForm() {
    $('input[type=text]').val('');
    $('input[type=checkbox]').removeAttr('checked');
    $('input[type=radio]').removeAttr('checked');
    $('textarea').val('');
    var date = moment().format('L');
    var dateSingle = $('.date-single-picker');
    dateSingle.each(function (index) {
        var obj = $(this);

        obj.daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            //autoUpdateInput: false,
            //maxDate: new Date(),
            changeYear: true,
            //yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
            function (start, end, label) {
                $(this).val(moment(end).format('L'));
                obj.trigger('input');
                obj.trigger('change');
            });
    });
    var _selectedDateRange = {
        startDate: moment().add('-6', 'day').startOf('day'),
        endDate: moment().endOf('day')
    };

    var dateRange = $('.date-range-picker');
    dateRange.each(function (index) {
        var obj = $(this);

        obj.daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
                obj.val(start + ' - ' + end);
                obj.trigger('input');
                obj.trigger('change');
            }
        );
    });
}

function editForm() {
    var dateSingle = $('.date-single-picker');

    dateSingle.each(function (index) {
        var obj = $(this);
        ////console.log(obj.prop('id'));
        ////console.log(obj.val());
        obj.daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            //autoUpdateInput: false,
            //maxDate: new Date(),
            changeYear: true,
            //yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
            function (start, end, label) {
                obj.val(moment(end).format('L'));
                obj.trigger('input');
                obj.trigger('change');
            });
        //obj.on('apply.daterangepicker', function (ev, picker) {
        //    $(this).val(picker.endDate.format('L'));
        //    //console.log(obj.val());
        //});

        //obj.on('cancel.daterangepicker', function (ev, picker) {
        //    $(this).val('').change();
        //});

    });

    var _selectedDateRange = {
        startDate: moment().add('-6', 'day').startOf('day'),
        endDate: moment().endOf('day')
    };

    var dateRange = $('.date-range-picker');
    dateRange.each(function (index) {
        var obj = $(this);
        ////console.log(obj.prop('id'));
        ////console.log(obj.val());
        obj.daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
                obj.val(start + ' - ' + end);
                obj.trigger('input');
                obj.trigger('change');
            });
        //obj.on('apply.daterangepicker', function (ev, picker) {
        //    $(this).val(picker.startDate.format('L') + ' - ' + picker.endDate.format('L'));
        //    //console.log($(this).val()).change();
        //});

    });
}

function aplicarDateSingle() {
    var dateSingle = $('.date-single-picker');
    dateSingle.each(function (index) {
        var obj = $(this);

        obj.daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            //maxDate: new Date(),
            changeYear: true,
            //yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
            function (start, end, label) {
                obj.val(moment(end).format('L'));
                //obj.trigger('input');
                //obj.trigger('change');
            });
    });
}

function aplicarDateRange() {
    var _selectedDateRange = {
        startDate: moment().add('-6', 'day').startOf('day'),
        endDate: moment().endOf('day')
    };

    var dateRange = $('.date-range-picker');
    dateRange.each(function (index) {
        var obj = $(this);

        obj.daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
                obj.val(start + ' - ' + end);
                obj.trigger('input');
                obj.trigger('change');
            }
        );
    });
}

//LOAD PARA ABERTURAS DAS ABAS DE ATENDIMENTO (PABLO)
var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    // PADRÃO
    //var $dialog = $(
    //    '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
    //    '<div class="modal-dialog modal-m">' +
    //    '<div class="modal-content">' +
    //        '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
    //        '<div class="modal-body">' +
    //            '<div class="fa fa-spinner fa-spin fa-3x fa-fw"" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
    //        '</div>' +
    //    '</div></div></div>');

    //CUSTOMIZADO
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-body">' +
        '<div class="fa fa-spinner fa-pulse fa-5x fa-fw" style="margin-bottom:0;margin-left:40%;"><div class="progress-bar" style="width: 100%;"></div></div>' +
        '</div>' +
        '</div></div></div>');

    return {
        //'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>'
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Carregando...';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };







})(jQuery);

// SWMWE Helpers
function selectSW(classe, url, elementoFiltro) {

   

    $(classe).css('width', '100%');
    $.fn.modal.Constructor.prototype.enforceFocus = function () { };



    function filtrar() {

       

        if (elementoFiltro) {
            var retorno = null;
            if (elementoFiltro.valor != undefined) {
                retorno = elementoFiltro.valor;
            } else if ((elementoFiltro != undefined) && (elementoFiltro != null) && (elementoFiltro != 0) && (elementoFiltro != '0')) {
                if (elementoFiltro.val()) {
                    retorno = elementoFiltro.val();
                } else {
                    retorno = null;// elementoFiltro;
                }
            } else if (elementoFiltro.val()) {
                retorno = elementoFiltro.val();
            }
            return retorno;
        }
        else {
            return null;
        }
    }

    $(classe).select2({
        allowClear: true,
        placeholder: app.localize("SelecioneLista"),
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            method: 'Post',

            data: function (params) {
                //   //console.log('data: ', params, (params.page == undefined));
                if (params.page == undefined)
                    params.page = '1';
                //   //console.log('data: ', params);

                return {
                    search: params.term,
                    page: params.page,
                    totalPorPagina: 10,
                    filtro: filtrar()
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.result.items,
                    pagination: {
                        more: (params.page * 10) < data.result.totalCount
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0
    });

}

function select2MestreFor(mestreId, dependenteClasse, dependenteServicoMetodo) {
    $("#" + mestreId).on("change", function () {
        var mestreId = $(this).val();

        ////console.log('meestreId: ' + mestreId);

        var classeDep = '.' + dependenteClasse;
        var path = '/api/services/app/' + dependenteServicoMetodo;

        ////console.log('classeDependente: ' + classeDep);
        ////console.log('path: ' + path);
        ////console.log('mestreId: ' + mestreId);

        selectSW(classeDep, path, mestreId);
    });
}

// Select2 exclusivo para ContasMedicasPlano
function selectSWPlano(classe, url, elementoFiltro) {
    $(classe).css('width', '100%');
    $.fn.modal.Constructor.prototype.enforceFocus = function () { };
    function filtrar() {
        if (elementoFiltro) {
            var retorno = null;
            if (elementoFiltro.valor != undefined) {
                retorno = elementoFiltro.valor;
            } else if ((elementoFiltro != undefined) && (elementoFiltro != null) && (elementoFiltro != 0) && (elementoFiltro != '0')) {
                if (elementoFiltro.val()) {
                    retorno = elementoFiltro.val();
                } else {
                    retorno = null;
                }
            } else if (elementoFiltro.val()) {
                retorno = elementoFiltro.val();
            }
            return retorno;
        }
        else {
            return null;
        }
    }

    $(classe).select2({
        allowClear: true,
        placeholder: app.localize("SelecioneLista"),
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            method: 'Post',

            data: function (params) {
                //   //console.log('data: ', params, (params.page == undefined));
                if (params.page == undefined)
                    params.page = '1';
                //   //console.log('data: ', params);

                return {
                    search: params.term,
                    page: params.page,
                    totalPorPagina: 10,
                    filtro: filtrar()
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.result.items,
                    pagination: {
                        more: (params.page * 10) < data.result.totalCount
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0
    });

}

function select2MestreForPlano(mestreId, dependenteClasse, dependenteServicoMetodo) {
    $("#" + mestreId).on("change", function () {
        var mestreId = $(this).val();

        ////console.log('meestreId: ' + mestreId);

        var classeDep = '.' + dependenteClasse;
        var path = '/api/services/app/' + dependenteServicoMetodo;

        ////console.log('classeDependente: ' + classeDep);
        ////console.log('path: ' + path);
        ////console.log('mestreId: ' + mestreId);

        selectSWPlano(classeDep, path, mestreId);
    });
}
// Fim - select2 exclusivo

function aplicarSelect2Padrao() {
    $('.select2').each(function () {
        var url = '/api/services/app/' + $(this)
            .attr('name');
        url = url
            .replace('1', '')
            .replace('2', '')
            .replace('3', '')
            .replace('4', '');
        //+ '/listarDropdown';
        var url1 = url.substr(0, url.length - 2) + '/listarDropdown';
        $(this).select2({
            allowClear: true,
            placeholder: app.localize("SelecioneLista"),
            ajax: {
                url: url1,
                dataType: 'json',
                delay: 250,
                method: 'Post',
                data: function (params) {
                    if (params.page == undefined)
                        params.page = '1';
                    return {
                        search: params.term,
                        page: params.page,
                        totalPorPagina: 10
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.result.items,
                        pagination: {
                            more: (params.page * 10) < data.result.totalCount
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0
        })
    });
    $('.select2').css('width', '100%');
}

function selectSWMultiplosFiltros(classe, url, filtros, oi) {

    $(classe).css('width', '100%');
    $.fn.modal.Constructor.prototype.enforceFocus = function () { };


    function filtrar() {
        var filtroValores = [];
        for (i = 0; i < filtros.length; i++) {
            var campo = (filtros[i].valor != undefined) ? filtros[i].valor : $('#' + filtros[i]).val();
            filtroValores.push(campo);
        }
        return filtroValores;
    }

    var valoresFiltro;

    valoresFiltro = filtrar();

    $(classe).select2({
        allowClear: true,
        placeholder: app.localize("SelecioneLista"),
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            method: 'Post',

            data: function (params) {
                if (params.page == undefined)
                    params.page = '1';

                return {
                    search: params.term,
                    page: params.page,
                    totalPorPagina: 10,
                    filtros: valoresFiltro
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.result.items,
                    pagination: {
                        more: (params.page * 10) < data.result.totalCount
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0
    });

}

function setActivePage(pageName) {
    sessionStorage["ActivePage"] = pageName;
    $.ajaxSetup({ cache: false, async: false });
    $.ajax({
        url: '/mpa/operacoes/DefinirOperacaoAtual?name=' + pageName,
        dataType: 'json',
        success: function (data) {
            if (data && data != null) {
                sessionStorage["OperacaoId"] = data.Id;
            }
        },
    });
}

function formatarValor(valor) {

    if (valor != '' && valor != null) {
        var numero = parseFloat(valor).toFixed(2).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');

    }
    return '';

}

function formatarValor4(valor) {

    if (valor != '' && valor != null) {
        var numero = parseFloat(valor).toFixed(4).split('.');
        numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');

    }
    return '';

}

function retirarMascara(valor) {
    while (valor.indexOf('.') != -1) valor = valor.replace('.', '');
    while (valor.indexOf(' ') != -1) valor = valor.replace(' ', '');
    valor = valor.replace(',', '.');
    return valor;
}

// Print function (require the reportviewer client ID)
function printReport(report_ID) {
    var rv1 = $('#' + report_ID);
    var iDoc = rv1.parents('html');
    // Reading the report styles
    var styles = iDoc.find("head style[id$='ReportControl_styles']").html();
    if ((styles == undefined) || (styles == '')) {
        iDoc.find('head script').each(function () {
            var cnt = $(this).html();
            var p1 = cnt.indexOf('ReportStyles":"');
            if (p1 > 0) {
                p1 += 15;
                var p2 = cnt.indexOf('"', p1);
                styles = cnt.substr(p1, p2 - p1);
            }
        });
    }
    if (styles == '') { alert("Cannot generate styles, Displaying without styles.."); }
    styles = '<style type="text/css">' + styles + "</style>";

    // Reading the report html
    var table = rv1.find("div[id$='_oReportDiv']");
    if (table == undefined) {
        alert("Report source not found.");
        return;
    }

    // Generating a copy of the report in a new window
    var docType = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/loose.dtd">';
    var docCnt = styles + table.parent().html();
    var docHead = '<head><title>Printing ...</title><style>body{margin:5;padding:0;}</style></head>';
    var winAttr = "location=yes, statusbar=no, directories=no, menubar=no, titlebar=no, toolbar=no, dependent=no, width=720, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";;
    var newWin = window.open("", "_blank", winAttr);
    writeDoc = newWin.document;
    writeDoc.open();
    writeDoc.write(docType + '<html>' + docHead + '<body onload="window.print();">' + docCnt + '</body></html>');
    writeDoc.close();

    // The print event will fire as soon as the window loads
    newWin.focus();
    // uncomment to autoclose the preview window when printing is confirmed or canceled.
    // newWin.close();
};

function addPrintButton(ctl) {
    var innerTbody = '<tbody><tr><td><input type="image" style="border-width: 0px; padding: 2px; height: 16px; width: 16px;" alt="Print" src="/App/Reserved.ReportViewerWebControl.axd?OpType=Resource&Version=10.0.40219.1&Name=Microsoft.Reporting.WebForms.Icons.Print.gif" title="Print"></td></tr></tbody>';
    var innerTable = '<table title="Print" onclick="javascript:PrintFunc(\'' + ctl + '\'); return false;" id="do_print" style="cursor: default;">' + innerTbody + '</table>'
    var outerDiv = '<div style="display: inline-block; font-size: 8pt; height: 30px;" class=" "><table cellspacing="0" cellpadding="0" style="display: inline;"><tbody><tr><td height="28px">' + innerTable + '</td></tr></tbody></table></div>';
    $("#ctl00_cphMain_rvReportMain_ctl05 > div").append(outerDiv);
}

function preventWhiteSpace(str) {
    var notWhitespaceTestRegex = /[^\s]{1,}/;
    return String(str).search(notWhitespaceTestRegex) != -1;
}

function removerAcentos(str) {
    if (str != null && str != undefined) {
        var result = str.toUpperCase();
        result = result.replace('Á', 'A');
        result = result.replace('Â', 'A');
        result = result.replace('Ã', 'A');
        result = result.replace('À', 'A');
        result = result.replace('Ä', 'A');
        result = result.replace('É', 'E');
        result = result.replace('È', 'E');
        result = result.replace('Ê', 'E');
        result = result.replace('Ë', 'E');
        result = result.replace('Í', 'I');
        result = result.replace('Ì', 'I');
        result = result.replace('Î', 'I');
        result = result.replace('Ï', 'I');
        result = result.replace('Ó', 'O');
        result = result.replace('Ò', 'O');
        result = result.replace('Ô', 'O');
        result = result.replace('Õ', 'O');
        result = result.replace('Ö', 'O');
        result = result.replace('Ú', 'U');
        result = result.replace('Ù', 'U');
        result = result.replace('Û', 'U');
        result = result.replace('Ü', 'U');
        result = result.replace('Ç', 'C');
        result = result.replace('Ñ', 'N');
        return result;
    }
    else {
        return str;
    }
}

function minimizarMenu() {
    $('body').addClass('page-sidebar-closed');
    $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
}

function definirHorarios(intervalo, horaIni) {
    var str = '';
    data = new Date();
    var aHoraIni = horaIni.split(':');
    var i = parseInt(aHoraIni[0]);

    var loops = 24 / parseInt(intervalo);
    if (loops != Infinity) {
        while (loops > 0) {
            if (str.indexOf(zeroEsquerda(i, 2) + ':00') == -1) {
                str += zeroEsquerda(i, 2) + ':00' + ' ';
            }
            i += parseInt(intervalo);
            loops--;
            if (i >= 24) {
                i = (i - 24);
            }
        }
    }
    var len = str.length;
    str = str.substring(0, len - 1);
    return str;
}

function selecionarRegistroSelect2(campo, id, descricao) {
    $('#' + campo).append($("<option>").val(id)
                                       .text(descricao)
                         )
                 .val(id)
                 .trigger("change");
}

function alertSw(titulo, mensagem, icone) {
    swal({
        title: titulo,
        text: mensagem,
        type: icone
    });
}

window.alert = function (titulo, mensagem, icone) {
    swal({
        title: titulo,
        text: mensagem,
        type: icone
    });
}