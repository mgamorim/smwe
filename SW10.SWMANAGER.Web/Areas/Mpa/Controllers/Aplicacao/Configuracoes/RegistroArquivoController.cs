﻿using Abp.Auditing;
using Abp.Web.Mvc.Authorization;
using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Configuracoes
{
    public class RegistroArquivoController : SWMANAGERControllerBase
    {
        private readonly IRegistroArquivoAppService _registroArquivoAppService;

        public RegistroArquivoController(IRegistroArquivoAppService registroArquivoAppService)
        {
            _registroArquivoAppService = registroArquivoAppService;
        }

        [HttpPost]
        [ValidateInput(false)]
        public void GravarHTMLFormularioDinamico(string registroHTML)
        {
            var _registroHTML = JsonConvert.DeserializeObject<RegistroHTML>(registroHTML);
            _registroArquivoAppService.GravarHTMLFormularioDinamico(_registroHTML);
        }


        [HttpPost]
        [ValidateInput(false)]
        public void GravarImagemFormularioDinamico(string registroHTML)
        {
            var _registroHTML = JsonConvert.DeserializeObject<RegistroHTML>(registroHTML);
            _registroArquivoAppService.GravarImagemFormularioDinamico(_registroHTML);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult VisualizarPorId(long id)
        {
            var registroArquivo = _registroArquivoAppService.ObterPorId(id);

            try
            {
                Response.Headers.Add("Content-Disposition", "inline; filename=desctino.pdf");
                return File(registroArquivo.Arquivo, "application/pdf");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return null;

        }




        [AbpMvcAuthorize]
        [DisableAuditing]
        public ActionResult DownloadArvivoPorRegistro(long registroId, long registroTabelaId, string nomeArquivo, string tipoArquivo)
        {
            var registroArquivo = _registroArquivoAppService.ObterPorRegistro(registroId, registroTabelaId);

            if (registroArquivo != null)
            {
                string _nomeArquivo = !string.IsNullOrEmpty(nomeArquivo) ? nomeArquivo : registroArquivo.Descricao;

                return File(registroArquivo.Arquivo, tipoArquivo, _nomeArquivo);
            }
            byte[] bytes = new byte[0];
            return File(bytes, tipoArquivo, nomeArquivo);
        }

    }
}