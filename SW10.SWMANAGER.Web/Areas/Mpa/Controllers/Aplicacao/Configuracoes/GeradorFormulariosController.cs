﻿using Abp.AutoMapper;
using Abp.Threading;
using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Configuracoes
{
    public class GeradorFormulariosController : SWMANAGERControllerBase
    {

        private readonly IFormConfigAppService _formConfigAppService;
        private readonly IRowConfigAppService _rowConfigAppService;
        private readonly IColConfigAppService _colConfigAppService;
        private readonly IColMultiOptionAppService _colMultiOptionAppService;
        private readonly IFormDataAppService _formDataAppService;
        private readonly IFormRespostaAppService _formRespostaAppService;
        private readonly IFormConfigOperacaoAppService _formConfigOperacaoAppService;
        private readonly IFormConfigUnidadeOrganizacionalAppService _formConfigUnidadeOrganizacionalAppService;
        
        public GeradorFormulariosController (
            IFormConfigAppService formConfigAppService,
            IRowConfigAppService rowConfigAppService,
            IColConfigAppService colConfigAppService,
            IColMultiOptionAppService colMultiOptionAppService,
            IFormDataAppService formDataAppService,
            IFormRespostaAppService formRespostaAppService,
            IFormConfigOperacaoAppService formConfigOperacaoAppService,
            IFormConfigUnidadeOrganizacionalAppService formConfigUnidadeOrganizacionalAppService
            )
        {
            _formConfigAppService = formConfigAppService;
            _rowConfigAppService = rowConfigAppService;
            _colConfigAppService = colConfigAppService;
            _colMultiOptionAppService = colMultiOptionAppService;
            _formDataAppService = formDataAppService;
            _formRespostaAppService = formRespostaAppService;
            _formConfigOperacaoAppService = formConfigOperacaoAppService;
            _formConfigUnidadeOrganizacionalAppService = formConfigUnidadeOrganizacionalAppService;
        }

        // GET: FormularioDinamico
        public ActionResult Index ()
        {
            //var list = await _formConfigAppService.ListarTodos();
            var model = new GeradorFormulariosViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/Index.cshtml", model);
        }

        public ActionResult CriarFormulario ()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/CriarFormulario.cshtml");
        }

        public async Task<ActionResult> CamposReservados ()
        {
            long idReservado = await _formConfigAppService.ObterReservadoId();

            if (idReservado != 0)
            {
                ViewBag.CloneId = idReservado;
                return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/Reservados/Reservados.cshtml");
            }
            else
            {
                var id = await _formConfigAppService.CriarReservadoAndGetId();
                ViewBag.CloneId = id;
                return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/Reservados/Reservados.cshtml");
            }
        }

        public async Task<ContentResult> ObterFormConfigReservado ()
        {
            var result = new FormConfig();

            var reservadoId = await _formConfigAppService.ObterReservadoId();

            result = await _formConfigAppService.Obter(reservadoId);

            result.Linhas = result.Linhas.OrderBy(i => i.Ordem).ToList();

            var list = JsonConvert.SerializeObject(result,
                     Formatting.None,
                     new JsonSerializerSettings()
                     {
                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                     });

            //return Json(result, JsonRequestBehavior.AllowGet);
            return Content(list, "application/json");
        }

        public async Task<ContentResult> ObterColConfigReservado (string colDesejada)
        {
            var result = new FormConfig();

            var reservadoId = await _formConfigAppService.ObterReservadoId();

            result = await _formConfigAppService.Obter(reservadoId);

            result.Linhas = result.Linhas.OrderBy(i => i.Ordem).ToList();

            List<ColConfig> cols = new List<ColConfig>();

            foreach (var linha in result.Linhas)
            {
                foreach (var c in linha.ColConfigs)
                {
                    cols.Add(c);
                }
            }

            var col = cols.FirstOrDefault(c => c.Name == colDesejada);

            var list = JsonConvert.SerializeObject(col,
                     Formatting.None,
                     new JsonSerializerSettings()
                     {
                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                     });

            return Content(list, "application/json");
        }


        public ActionResult ClonarFormulario (long id)
        {
            ViewBag.CloneId = id;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/ClonarFormulario.cshtml");
        }

        public ActionResult EditarFormularioConfig (long id)
        {
            ViewBag.CloneId = id;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/EditarFormulario.cshtml");
        }

        public async Task<ActionResult> ListarDados (long id)
        {
            //var list = new List<FormResposta>();
            //var respostasList = await _formRespostaAppService.ListarTodos();
            //list = respostasList.Items.Where(m => m.FormConfig.Id == id).ToList();

            //ViewBag.Id = id;
            var model = new FormRespostaViewModel();
            model.FormId = id;
            var form = await _formConfigAppService.Obter(id);
            model.FormName = form.Nome;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/ListarDados.cshtml", model);
        }

        public async Task<PartialViewResult> _ListarDados (long id)
        {
            //var list = new List<FormResposta>();
            //var respostasList = await _formRespostaAppService.ListarTodos();
            //list = respostasList.Items.Where(m => m.FormConfig.Id == id).ToList();

            //ViewBag.Id = id;
            var model = new FormRespostaViewModel();
            model.FormId = id;
            var form = await _formConfigAppService.Obter(id);
            model.FormName = form.Nome;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/_ListarDados.cshtml", model);
        }

        //public JsonResult ObterFormConfig(long id = 3)
        public ContentResult ObterFormConfig (long id = 3)
        {
            var result = new FormConfig();

            result = AsyncHelper.RunSync(() => _formConfigAppService.Obter(id));  //await _formConfigAppService.Obter(id);


            result.Linhas = result.Linhas.OrderBy(i => i.Ordem).ToList();

            var list = JsonConvert.SerializeObject(result,
                     Formatting.None,
                     new JsonSerializerSettings()
                     {
                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                     });

            //return Json(result, JsonRequestBehavior.AllowGet);
            return Content(list, "application/json");
        }

        public ContentResult ObterCloneFormConfig (long id = 3)
        {
            var result = new FormConfig();

            result = AsyncHelper.RunSync(() => _formConfigAppService.Clonar(id));  //await _formConfigAppService.Obter(id);

            //var formConfig = new FormConfig
            //{
            //    DataAlteracao = result.DataAlteracao,
            //    Linhas = result.Linhas,
            //    Nome = result.Nome
            //};
            var list = JsonConvert.SerializeObject(result,
                     Formatting.None,
                     new JsonSerializerSettings()
                     {
                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                     });

            //return Json(result, JsonRequestBehavior.AllowGet);
            return Content(list, "application/json");
        }

        public ContentResult ObterDados (long id = 2)
        {

            var teste = Session["testeArquivo"];

            var result = new FormResposta();

            result = AsyncHelper.RunSync(() => _formRespostaAppService.Obter(id)); //await _formRespostaAppService.Obter(id);
                                                                                   //List<ColConfig> cols = result?.FormConfig?.Linhas?.Select(s => s.Col1)?.ToList();
                                                                                   //cols?.AddRange(result?.FormConfig?.Linhas?.Select(s => s.Col2)?.ToList());
            List<ColConfig> cols = result?.FormConfig?.Linhas?.SelectMany(s => s.ColConfigs).ToList();

            if (cols != null)
            {

                foreach (var item in cols)
                {
                    if (item != null)
                    {
                        if (item.Type != "checkbox")
                        {
                            //item.Value = item.Valores.Where(w=> w.Resposta.Id == id)?.FirstOrDefault()?.Valor;
                            item.Value = result.ColRespostas?.Where(w => w.Coluna.Id == item.Id)?.FirstOrDefault()?.Valor;
                        }
                        else
                        {
                            item.MultiOption?.ForEach(e =>
                            {
                                e.Selecionado = item.Valores != null ? item.Valores.Any(a => a.Valor == e.Opcao) : false;
                                //e.Selecionado = result.ColRespostas != null ? result.ColRespostas.Any(a => a.Coluna.Id == item.Id && a.Valor.Equals(e.Opcao)) : false;
                            });
                        }
                    }
                }

                //result.ColRespostas.Clear();

            }

            var list = JsonConvert.SerializeObject(result,
                     Formatting.None,
                     new JsonSerializerSettings()
                     {
                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                     });

            return Content(list, "application/json");
        }

        public ActionResult Preencher (long id)
        {
            ViewBag.formId = id;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/Preencher.cshtml");
        }

        public ViewResult _Preencher (string nomeClasse, long formConfigId, long registroClasseId, long? atendimentoId = null)
        {
            ViewBag.formId = formConfigId;
            ViewBag.nomeClasse = nomeClasse;
            ViewBag.registroClasseId = registroClasseId;
            ViewBag.atendimentoId = atendimentoId;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/_Preencher.cshtml");
        }

        public ActionResult EditarPreenchimento (long id = 2)
        {
            ViewBag.dadosRespostaId = id;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/EditarPreenchimento.cshtml");
        }

        public ActionResult EditarForm (string nomeClasse, long formRespostaId, long registroClasseId)
        {
            ViewBag.dadosRespostaId = formRespostaId;
            ViewBag.nomeClasse = nomeClasse;
            ViewBag.registroClasseId = registroClasseId;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/EditarPreenchimento.cshtml");
        }

        public ActionResult PreencherForm (string nomeClasse, long formConfigId, long registroClasseId)
        {
            ViewBag.formId = formConfigId;
            ViewBag.nomeClasse = nomeClasse;
            ViewBag.registroClasseId = registroClasseId;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/Preencher.cshtml");
        }

        public ViewResult _EditarPreenchimento (string nomeClasse, long formRespostaId, long registroClasseId, long? atendimentoId = null)
        {
            ViewBag.dadosRespostaId = formRespostaId;
            ViewBag.nomeClasse = nomeClasse;
            ViewBag.registroClasseId = registroClasseId;

            // Para Campos reservados - atendimento
            //var fr = AsyncHelper.RunSync(()=> _formRespostaAppService.Obter(formRespostaId));

            

            //var atd = _atdService.
            
            ViewBag.atendimentoId = atendimentoId;
            // Fim - campos reservados

            TempData["testeArquivo"]="sdfgsdfgsdfgsdfgsdgsdfgsdf";

            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/_EditarPreenchimento.cshtml");
        }

        public ActionResult DetalharPreenchimento (long id)
        {
            ViewBag.dadosRespostaId = id;
            return View("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/DetalharPreenchimento.cshtml");
        }

        [HttpPost]
        public async Task<JsonResult> GravarConfig (FormConfig form)
        {
            if (form == null)
            {
                throw new Exception();
            }

            form.DataAlteracao = DateTime.Now;
            await _formConfigAppService.CriarOuEditar(form);

            return Json(form);
        }

        //[HttpPost]
        //public async Task<JsonResult> GravarDados(FormConfig formDados, int IdDadosResposta)
        //{
        //    if (formDados == null || formDados.Id == 0 || formDados.Linhas == null)
        //    {
        //        throw new Exception();
        //    }

        //    var resposta = new FormResposta();
        //    var dados = new List<FormData>();
        //    var colunasId = formDados.Linhas.Select(s => s.Col1.Id).ToList();
        //    colunasId.AddRange(formDados.Linhas.Select(s => s.Col2.Id).ToList());
        //    colunasId = colunasId.Distinct().ToList();

        //    var idConfig = formDados.Id;
        //    resposta = new FormResposta
        //    {
        //        DataResposta = DateTime.Now,
        //        FormConfig = await _formConfigAppService.Obter(idConfig) //db.FormConfig.Where(w => w.Id == idConfig).FirstOrDefault()
        //    };
        //    var qColunasBase = await _colConfigAppService.ListarTodos();
        //    var colunasBase = qColunasBase.Items.Where(m => colunasId.Contains(m.Id)).ToList(); //db.ColConfig.Where(w => colunasId.Contains(w.Id)).ToList();

        //    formDados.Linhas.ForEach(f =>
        //    {
        //        if (f.Col1 != null && f.Col2 != null)
        //        {
        //            ProcessarValor(f.Col1, dados, colunasBase.FirstOrDefault(w => w.Id == f.Col1.Id), f.Col1.MultiOption?.Where(s => s.Selecionado == true).ToList());
        //            ProcessarValor(f.Col2, dados, colunasBase.FirstOrDefault(w => w.Id == f.Col2.Id), f.Col2.MultiOption?.Where(s => s.Selecionado == true).ToList());

        //        }
        //        else if (f.Col1 != null && f.Col2 == null)
        //        {
        //            ProcessarValor(f.Col1, dados, colunasBase.FirstOrDefault(w => w.Id == f.Col1.Id), f.Col1.MultiOption?.Where(s => s.Selecionado == true).ToList());
        //        }
        //    });

        //    //Inclusão
        //    if (IdDadosResposta == 0)
        //    {
        //        //await _formRespostaAppService.CriarOuEditar(resposta);
        //    }
        //    else // edição
        //    {
        //        var respostaBase = await _formRespostaAppService.Obter(IdDadosResposta);
        //        /*db.Respostas.Where(w => w.Id == IdDadosResposta)
        //        .Include(i => i.FormConfig)
        //        .Include(i => i.ColRespostas)
        //        .FirstOrDefault();*/
        //        foreach (var colResposta in respostaBase.ColRespostas)
        //        {
        //            await _formDataAppService.Excluir(IdDadosResposta);
        //        }
        //        /*db.FormData.RemoveRange(respostaBase.ColRespostas);
        //        await db.SaveChangesAsync();*/
        //        respostaBase.ColRespostas = dados;
        //        respostaBase.DataResposta = DateTime.Now;
        //        //await _formRespostaAppService.CriarOuEditar(respostaBase);
        //    }
        //    //await db.SaveChangesAsync();


        //    return Json(resposta.Id != 0);
        //}

        private static void ProcessarValor (ColConfig Col, List<FormData> dados, ColConfig coluna, List<ColMultiOption> multiSelected)
        {

            if (Col.Type == "checkbox" && multiSelected != null)
            {
                for (int i = 0; i < multiSelected.Count; i++)
                {
                    dados.Add(new FormData
                    {
                        Valor = multiSelected[i].Opcao + (i < (multiSelected.Count - 1) ? "," : ""),
                        Coluna = coluna
                    });
                }
            }
            else
            {
                dados.Add(new FormData
                {
                    Valor = Col.Value,
                    Coluna = coluna
                });
            }

        }

        //Adicionando actions para relacionar o formulário com as Operações e as Unidades Operacionais

        public async Task<PartialViewResult> _AssociarUnidadeOrganizacional (long formId)
        {
            var model = await Task.Run(() => new FormConfigUnidadeOrganizacionalDto());
            model.FormConfigId = formId;
            var form = await _formConfigAppService.Obter(formId);
            model.FormConfig = form;
            var viewModel = new AssociarUnidadeOrganizacionalViewModel(model);
            //if (model.Items.Count() > 0)
            //{
            //    TempData["AssociacoesUnidadeOrganizacionalAtivas"] = JsonConvert.SerializeObject(model.Items.ToList());
            //}
            //else
            //{
            //    TempData["AssociacoesUnidadeOrganizacionalAtivas"] = JsonConvert.SerializeObject(new List<FormConfigUnidadeOrganizacionalDto>());
            //}
            //viewModel.FormId = formId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/_AssociarUnidadeOrganizacional.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _AssociarOperacao (long formId)
        {
            var model = await Task.Run(() => new FormConfigOperacaoDto());
            model.FormConfigId = formId;
            var form = await _formConfigAppService.Obter(formId);
            model.FormConfig = form;
            var viewModel = new AssociarOperacaoViewModel(model);
            //if (model.Items.Count() > 0)
            //{
            //    TempData["AssociacoesOperacaoAtivas"] = JsonConvert.SerializeObject(model.Items.ToList());
            //}
            //else
            //{
            //    TempData["AssociacoesOperacaoAtivas"] = JsonConvert.SerializeObject(new List<FormConfigOperacaoDto>());
            //}
            //viewModel.FormId = formId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/_AssociarOperacao.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _AssociarEspecialidade (long formId)
        {
            var model = await Task.Run(() => new FormConfigEspecialidadeDto());
            model.FormConfigId = formId;
            var form = await _formConfigAppService.Obter(formId);
            model.FormConfig = form;
            var viewModel = new AssociarEspecialidadeViewModel(model);
            //if (model.Items.Count() > 0)
            //{
            //    TempData["AssociacoesOperacaoAtivas"] = JsonConvert.SerializeObject(model.Items.ToList());
            //}
            //else
            //{
            //    TempData["AssociacoesOperacaoAtivas"] = JsonConvert.SerializeObject(new List<FormConfigOperacaoDto>());
            //}
            //viewModel.FormId = formId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Configuracoes/GeradorFormularios/_AssociarEspecialidade.cshtml", viewModel);
        }


        [HttpPost]
        [ValidateInput(false)]
        public void GravarHtml(string html)
        {
            var registroHTML = JsonConvert.DeserializeObject<List<RegistroHTML>>(html);





        }

    }
}