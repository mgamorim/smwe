﻿using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaKits;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.ContasKits;
using SW10.SWMANAGER.Web.Controllers;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Faturamentos
{
    public class FaturamentoContaKitsController : SWMANAGERControllerBase
    {
        #region Cabecalho
        private readonly IFaturamentoContaKitAppService _faturamentoContaKitAppService;
        private readonly IContaAppService _contaAppService;

        public FaturamentoContaKitsController(
            IFaturamentoContaKitAppService faturamentoContaKitAppService
            ,
            IContaAppService contaAppService
            )
        {
            _faturamentoContaKitAppService = faturamentoContaKitAppService;
            _contaAppService = contaAppService;
        }
        #endregion cabecalho.

        public ActionResult Index()
        {
            var model = new ContaKitsViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Faturamentos/ContaKits/Index.cshtml", model);
        }
        
        // [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_ContaKit_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_ContaKit_Edit)]
        public async Task<ActionResult> CriarOuEditarModal(long? id, long? contaId = null)
        {
            CriarOuEditarContaKitModalViewModel viewModel;
            if (id.HasValue)
            {
                //var output = await _faturamentoContaKitAppService.Obter((long)id);
                //viewModel = new CriarOuEditarContaKitModalViewModel(output);
                //viewModel.FaturamentoContaId = contaId;

                //var conta = AsyncHelper.RunSync(() => _contaAppService.Obter((long)contaId));
                //viewModel.FaturamentoConta = conta;

                var output = await _faturamentoContaKitAppService.ObterViewModel((long)id);
                viewModel = new CriarOuEditarContaKitModalViewModel(output);
                viewModel.FaturamentoContaId = contaId;

             //   var conta = AsyncHelper.RunSync(() => _contaAppService.Obter((long)contaId));
                viewModel.FaturamentoContaId = contaId;
            }
            else
            {
                viewModel = new CriarOuEditarContaKitModalViewModel(new FaturamentoContaKitDto());
                viewModel.FaturamentoContaId = contaId;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Faturamentos/ContaKits/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<ActionResult> ContaKit(long? id)
        {
            CriarOuEditarContaKitModalViewModel viewModel;
            if (id.HasValue)
            {
                var output = await _faturamentoContaKitAppService.Obter((long)id);
                viewModel = new CriarOuEditarContaKitModalViewModel(output);
            }
            else
            {
                viewModel = new CriarOuEditarContaKitModalViewModel(new FaturamentoContaKitDto());
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Faturamentos/ContaKits/ContaKit/Index.cshtml", viewModel);
        }

    }
}