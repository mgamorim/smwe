﻿#region Usings
using Abp.Threading;
using Microsoft.Reporting.WebForms;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.ContasMedicas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.Relatorios;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.Relatorios.Guias;
using SW10.SWMANAGER.Web.Controllers;
using SW10.SWMANAGER.Web.Relatorios.Faturamento.Guias;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
#endregion usings.

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Faturamentos
{
    public class EntregaContasController : SWMANAGERControllerBase
    {
        #region Cabecalho
        private readonly IContaAppService _contaMedicaAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IFaturamentoContaItemAppService _contaItemAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private ISessionAppService _sessionAppService;
        private readonly IUserAppService _userAppService;
        private readonly IFaturamentoConfigConvenioAppService _configConvenioAppService;
        private readonly IFaturamentoContaStatusAppService _faturamentoContaStatusAppService;

        public EntregaContasController(
            IContaAppService contaMedicaAppService,
            IPacienteAppService pacienteAppService,
            IFaturamentoContaItemAppService contaItemAppService,
            IAtendimentoAppService atendimentoAppService,
            ISessionAppService sessionAppService,
            IUserAppService userAppService,
            IFaturamentoConfigConvenioAppService configConvenioAppService,
            IFaturamentoContaStatusAppService faturamentoContaStatusAppService
            )
        {
            _contaMedicaAppService = contaMedicaAppService;
            _pacienteAppService = pacienteAppService;
            _contaItemAppService = contaItemAppService;
            _atendimentoAppService = atendimentoAppService;
            _sessionAppService = sessionAppService;
            _userAppService = userAppService;
            _configConvenioAppService = configConvenioAppService;
            _faturamentoContaStatusAppService = faturamentoContaStatusAppService;

        }
        #endregion cabecalho.

        public ActionResult Index ()
        {
            var model = new ContasMedicasViewModel();

            model.ListaStatus = _faturamentoContaStatusAppService.ListarTodos();
            return View("~/Areas/Mpa/Views/Aplicacao/Faturamentos/EntregaContas/Index.cshtml", model);
        }

        public async Task<ActionResult> ConferenciaModal (long? id)
        {
            CriarOuEditarContaMedicaModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _contaMedicaAppService.ObterViewModel((long)id);
                viewModel = new CriarOuEditarContaMedicaModalViewModel(output);
                viewModel.Atendimento = await _atendimentoAppService.Obter((long)viewModel.AtendimentoId);// ta demorando este

                ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
                configConvenioInput.Filtro = output.ConvenioId.ToString();
                var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);

                // Filtrar por empresa
                var configsPorEmpresa = configsConvenio.Items
                    .Where(c => c.EmpresaId == output.EmpresaId);

                viewModel.configsPorEmpresa = configsPorEmpresa.ToArray();

                // Filtrar por plano
                var configsPorPlano = configsPorEmpresa
                    .Where(x => x.PlanoId != null)
                    .Where(c => c.PlanoId == output.PlanoId);

                viewModel.configsPorPlano = configsPorPlano.ToArray();
            }
            else
            {
                viewModel = new CriarOuEditarContaMedicaModalViewModel(new FaturamentoContaDto());
                viewModel.Atendimento = new ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto.AtendimentoDto();
                viewModel.EmpresaId = 0; // CORRIGIR PARA EMPRESA LOGADA
                viewModel.ConvenioId = 0; // CORRIGIR ?
                viewModel.PlanoId = 0; // CORRIGIR ?
            }

            viewModel.Conferencia = true;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Faturamentos/EntregaContas/ConferenciaModal.cshtml", viewModel);
        }



    }


}