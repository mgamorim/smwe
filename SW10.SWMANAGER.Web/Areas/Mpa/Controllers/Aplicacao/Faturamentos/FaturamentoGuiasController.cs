﻿#region Usings
using Microsoft.Reporting.WebForms;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Relatorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Relatorios.Models;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Faturamentos.GuiasClasses;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Faturamentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.ContasMedicas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.Relatorios.Guias;
using SW10.SWMANAGER.Web.Controllers;
using SW10.SWMANAGER.Web.Relatorios.Faturamento.Guias;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
#endregion usings.

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Faturamentos
{
    public class FaturamentoGuiasController : SWMANAGERControllerBase
    {
        #region Dependencias
        private readonly IContaAppService                     _contaMedicaAppService;
        private readonly IFaturamentoContaItemAppService      _contaItemAppService;
        private readonly IAtendimentoAppService               _atendimentoAppService;
        private readonly IFaturamentoConfigConvenioAppService _configConvenioAppService;
        private readonly IFaturamentoGuiaAppService           _guiaAppService;

        private readonly IRelatorioFaturamentoAppService _relFatAppService;

        // Dados Guia Spsadt
        List<FaturamentoContaItemDto> _itensGuiaPrincipal               = new List<FaturamentoContaItemDto>();
        List<FaturamentoContaItemDto> _itensGuiaPrincipalPrimeiraPagina = new List<FaturamentoContaItemDto>();
        List<FaturamentoContaItemDto> _itensGuiaOutrasDespesas          = new List<FaturamentoContaItemDto>();

        public FaturamentoGuiasController (
            IContaAppService                     contaMedicaAppService,
            IFaturamentoContaItemAppService      contaItemAppService,
            IAtendimentoAppService               atendimentoAppService,
            IFaturamentoConfigConvenioAppService configConvenioAppService,
            IFaturamentoGuiaAppService           guiaAppService,
            IRelatorioFaturamentoAppService      relFatAppService
            )
        {
            _contaMedicaAppService    = contaMedicaAppService;
            _contaItemAppService      = contaItemAppService;
            _atendimentoAppService    = atendimentoAppService;
            _configConvenioAppService = configConvenioAppService;
            _guiaAppService           = guiaAppService;
            _relFatAppService         = relFatAppService;
        }
        #endregion dependencias.

        public ActionResult Index ()
        {
            var model = new FaturamentoGuiasViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Faturamentos/Guias/Index.cshtml", model);
        }

        public async Task<ActionResult> CriarOuEditarModal (long? id)
        {
            var model = new FaturamentoGuiaDto();

            if (id.HasValue)
            {
                model = await _guiaAppService.Obter((long)id);
            }
            else
            {
                model.Id = 0;
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Faturamentos/Guias/_CriarOuEditarModal.cshtml", model);
        }

        public class VisualizarGuiaInput
        {
            public long? ContaId { get; set; }
            public long? AtendimentoId { get; set; }
            public long? GuiaId { get; set; }
        }

        public async Task<ActionResult> VisualizarGuia(VisualizarGuiaInput input)
        {
            var guia = await _guiaAppService.Obter((long)input.GuiaId);


            switch (guia.Codigo)
            {
                case "1":
                    // return VisualizarGuiaConsulta();
                    break;

                case "2":
                    var spsadtInput = new GuiaSpsadtInput
                    {
                        ContaId = input.ContaId,
                        AtendimentoId = input.AtendimentoId
                    };
                    return await VisualizarSpsadt(spsadtInput);
                   
                case "3":
                    return await GuiaResumoInternacaoPdf((long)input.AtendimentoId);
                default:
                    return null;
                    break;
            }

            return null;
        }

        // Guia Resumo Internacao
        private async Task<ActionResult> GuiaResumoInternacaoPdf(long atendimentoId)
        {
            try
            {
                var atendimento = await _atendimentoAppService.Obter((long)atendimentoId);
                var dados = GuiaResumoInternacaoModel.MapearFromAtendimento(atendimento);

                // Guia principal
                Web.Relatorios.Faturamento.Guias.InternacaoResumo.resumo_internacao_dataset resumo_internacao_dataset = new Web.Relatorios.Faturamento.Guias.InternacaoResumo.resumo_internacao_dataset();
                DataTable tabela = this.ConvertToDataTable(dados.Lista, resumo_internacao_dataset.Tables["resumo_internacao_table"]);
                DataRow row = tabela.NewRow();
                row["Logotipo"] = atendimento.Empresa.Logotipo;
                //   tabela.Rows[tabela.Rows.Count - 1].Delete();
                tabela.Rows.Add(row);

                ReportDataSource dataSource = new ReportDataSource("resumo_internacao_dataset", tabela);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.DataSources.Add(dataSource);
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);

                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Faturamento\Guias\InternacaoResumo\guia_internacao_resumo.rdlc");

                SetParametrosResumoInternacao(reportViewer, dados);

                // APARENTEMENTE FALTANDO SUB-RELATORIOS

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";

                string[] streamIds;
                Warning[] warnings;
                byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                reportViewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=PulseiraInternacao.pdf");
                return File(pdfBytes, "application/pdf");
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }
        }

        private void SetParametrosResumoInternacao(ReportViewer rv, GuiaResumoInternacaoModel dados)
        {
            ReportParameter NomePaciente = new ReportParameter("NomePaciente", dados.NomePaciente);
            ReportParameter Matricula = new ReportParameter("Matricula", dados.Matricula);
            ReportParameter RegistroANS = new ReportParameter("RegistroANS", dados.RegistroANS);
            ReportParameter ValidadeCarteira = new ReportParameter("ValidadeCarteira", dados.ValidadeCarteira);
            ReportParameter Senha = new ReportParameter("Senha", dados.Senha);
            ReportParameter CodCNES = new ReportParameter("CodCNES", dados.CodCNES);
            ReportParameter DataAutorizacao = new ReportParameter("DataAutorizacao", dados.DataAutorizacao);
            ReportParameter NomeContratado = new ReportParameter("NomeContratado", dados.NomeContratado);
            ReportParameter ValidadeSenha = new ReportParameter("ValidadeSenha", dados.ValidadeSenha);
            ReportParameter NumeroGuia = new ReportParameter("NumeroGuia", dados.NumeroGuia);
            ReportParameter Cid1 = new ReportParameter("Cid1", dados.Cid1);
            ReportParameter Cid2 = new ReportParameter("Cid2", dados.Cid2);
            ReportParameter Cid3 = new ReportParameter("Cid3", dados.Cid3);
            ReportParameter Cid4 = new ReportParameter("Cid4", dados.Cid4);
            ReportParameter CodOperadora = new ReportParameter("CodOperadora", dados.CodOperadora);
            ReportParameter CaraterAtendimento = new ReportParameter("CaraterAtendimento", dados.CaraterAtendimento);
            ReportParameter TipoFaturamento = new ReportParameter("TipoFaturamento", dados.TipoFaturamento);
            ReportParameter DataIniFaturamento = new ReportParameter("DataIniFaturamento", dados.DataIniFaturamento);
            ReportParameter DataFimFaturamento = new ReportParameter("DataFimFaturamento", dados.DataFimFaturamento);
            ReportParameter HoraIniFaturamento = new ReportParameter("HoraIniFaturamento", dados.HoraIniFaturamento);
            ReportParameter HoraFimFaturamento = new ReportParameter("HoraFimFaturamento", dados.HoraFimFaturamento);
            ReportParameter TipoInternacao = new ReportParameter("TipoInternacao", dados.TipoInternacao);
            ReportParameter RegimeInternacao = new ReportParameter("RegimeInternacao", dados.RegimeInternacao);
            ReportParameter TotalProcedimentos = new ReportParameter("TotalProcedimentos", dados.TotalProcedimentos);
            ReportParameter TotalDiaria = new ReportParameter("TotalDiaria", dados.TotalDiaria);
            ReportParameter TotalTaxasAlugueis = new ReportParameter("TotalTaxasAlugueis", dados.TotalTaxasAlugueis);
            ReportParameter TotalMateriais = new ReportParameter("TotalMateriais", dados.TotalMateriais);
            ReportParameter TotalOpme = new ReportParameter("TotalOpme", dados.TotalOpme);
            ReportParameter TotalMedicamentos = new ReportParameter("TotalMedicamentos", dados.TotalMedicamentos);
            ReportParameter TotalGasesMedicinais = new ReportParameter("TotalGasesMedicinais", dados.TotalGasesMedicinais);
            ReportParameter TotalGeral = new ReportParameter("TotalGeral", dados.TotalGeral);
            ReportParameter RN = new ReportParameter("RN", dados.RN ? "S" : "N");




            rv.LocalReport.SetParameters(new ReportParameter[] {
                NomePaciente            ,
                Matricula               ,
                RegistroANS             ,
                ValidadeCarteira        ,
                Senha                   ,
                CodCNES                 ,
                DataAutorizacao         ,
                NomeContratado          ,
                ValidadeSenha           ,
                NumeroGuia              ,
                Cid1                    ,
                Cid2                    ,
                Cid3                    ,
                Cid4                    ,
                CodOperadora            ,
                CaraterAtendimento      ,
                TipoFaturamento         ,
                DataIniFaturamento      ,
                DataFimFaturamento      ,
                HoraIniFaturamento      ,
                HoraFimFaturamento      ,
                TipoInternacao          ,
                RegimeInternacao        ,
                TotalProcedimentos      ,
                TotalDiaria             ,
                TotalTaxasAlugueis      ,
                TotalMateriais          ,
                TotalOpme               ,
                TotalMedicamentos       ,
                TotalGasesMedicinais    ,
                TotalGeral              ,
                RN
            });
        }

        // Fim - guia resumo internacao

        public async Task<ActionResult> VisualizarSpsadt (GuiaSpsadtInput input)
        {
            try
            {
                var dados = new GuiaSpsadtModel();
                var conta = await _contaMedicaAppService.ObterReportModel((long)input.ContaId);
                var atendimento = await _atendimentoAppService.Obter((long)input.AtendimentoId);
                dados.LerAtendimento(atendimento);

                // Itens da conta
                ListarFaturamentoContaItensInput listarItensInput = new ListarFaturamentoContaItensInput();
                listarItensInput.Filtro = conta.Id.ToString();
                var contaItensPaged = await _contaItemAppService.ListarPorConta(listarItensInput);
                var contaItens = contaItensPaged.Items as List<FaturamentoContaItemDto>;

                // Separando itens de acordo com grupo.IsOutraDespesa (guia principal ou 'outras despesas')
                // Spsadt
                List<FaturamentoContaItemDto> itensGuiaPrincipal = new List<FaturamentoContaItemDto>();
                List<FaturamentoContaItemDto> itensGuiaPrincipalPrimeiraPagina = new List<FaturamentoContaItemDto>();
                List<List<FaturamentoContaItemDto>> listasGuiaComplementar = new List<List<FaturamentoContaItemDto>>();
                // Outras Despesas
                List<FaturamentoContaItemDto> itensGuiaOutrasDespesas = new List<FaturamentoContaItemDto>();
                List<List<FaturamentoContaItemDto>> listasGuiaOutrasDespesas = new List<List<FaturamentoContaItemDto>>();

                itensGuiaPrincipal = contaItens.Where(x => !x.FaturamentoItem.Grupo.IsOutraDespesa).ToList();
                itensGuiaOutrasDespesas = contaItens.Where(x => x.FaturamentoItem.Grupo.IsOutraDespesa).ToList();


                //foreach (var item in contaItens)
                //{
                //    if (!item.IsValorItemManual)
                //    {
                //        item.ValorItem = await _contaItemAppService.CalcularValorUnitarioItem(conta.EmpresaId ?? 0, conta.ConvenioId ?? 0, conta.PlanoId ?? 0, item);
                //    }
                //}
                



                int totalItensGuiaPrincipal = itensGuiaPrincipal.Count;
                bool gerarGuiaComplementar = totalItensGuiaPrincipal > 5 ;

                // Verificando necessidade de guias complementares, gerando subListas de itens se necessario
                if (gerarGuiaComplementar)
                {
                    itensGuiaPrincipalPrimeiraPagina = itensGuiaPrincipal.GetRange(0, 5);
                    var lista = itensGuiaPrincipal.GetRange(5, totalItensGuiaPrincipal - 5);
                    // Cada guia complementar suporta ate 7 itens
                    var tamanho = 7;

                    // Gerando subListas de ate 7 itens
                    for (int i = 0; i < lista.Count; i += tamanho)
                    {
                        listasGuiaComplementar.Add(lista.GetRange(i, Math.Min(tamanho, lista.Count - i)));
                    }
                }
                else
                {
                    itensGuiaPrincipalPrimeiraPagina = itensGuiaPrincipal.GetRange(0, totalItensGuiaPrincipal);

                    // Complementar com itens vazios para ocupar espaco certo no relatorio
                    int count = itensGuiaPrincipalPrimeiraPagina.Count;

                    if (count < 5)
                    {
                        int diferenca = 5 - count;

                        for (int i = diferenca + 1; i < 5; i++)
                        {
                            var novoItem = new FaturamentoContaItemDto();
                            novoItem.FaturamentoItem = new FaturamentoItemDto();

                           

                            itensGuiaPrincipalPrimeiraPagina.Add(novoItem);
                        }
                    }
                }
        
                // Guia principal Spsadt
                Guias relDS = new Guias();
                DataTable tabela = this.ConvertToDataTable(dados.Contas, relDS.Tables["Spsadt"]);//precisa?
                ReportDataSource dataSource = new ReportDataSource("Spsadt", tabela);
                ReportViewer GuiaSpsadt = new ReportViewer();
                GuiaSpsadt.LocalReport.DataSources.Add(dataSource);
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(GuiaSpsadt);
                GuiaSpsadt.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"Relatorios\Faturamento\Guias\Spsadt\guia_spsadt.rdlc");
                SetParametrosSpsadt(GuiaSpsadt, dados);

                // Sub Relatorios, passando parametros
                _itensGuiaPrincipal = itensGuiaPrincipal;
                _itensGuiaPrincipalPrimeiraPagina = itensGuiaPrincipalPrimeiraPagina;

                // "Link" para carregamweento dos dados dos subRelatorios no relatorio principal
                GuiaSpsadt.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessingSpsadt);
                GuiaSpsadt.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessingSpsadtExames);
                GuiaSpsadt.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessingSpsadtEquipe);

                // Pdf (byte array)
                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";
                byte[] pdfBytes = GuiaSpsadt.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                GuiaSpsadt.LocalReport.Refresh();
                // Fim - Guia Principal

                // Spsadt: paginas complementares
                List<byte[]> pdfBytesComplementares = new List<byte[]>();

                // Um relatorio extra para cada subLista de itens
                foreach (var lista in listasGuiaComplementar)
                {
                    Guias DS = new Guias();
                    List<GuiaSpsadtDespesaItem> despesaItens = new List<GuiaSpsadtDespesaItem>();

                    foreach (var item in lista)
                    {
                        var despesaItem = new GuiaSpsadtDespesaItem();
                        despesaItem.Descricao = item.Descricao + item.FaturamentoItem.Descricao;
                        despesaItem.CodigoItem = item.FaturamentoItem.CodTuss;
                        despesaItem.Qtde = item.Qtde.ToString();
                        despesaItem.Tabela = item.FaturamentoConfigConvenioDto?.Codigo;
                        despesaItens.Add(despesaItem);
                    }

                    DataTable tabelaComplemetar = this.ConvertToDataTable(despesaItens, DS.Tables["SpsadtDespesasItens"]);
                    ReportDataSource dataSourceComplementar = new ReportDataSource("SpsadtDespesasItens", tabelaComplemetar);
                    ReportViewer GuiaComplementar = new ReportViewer();
                    GuiaComplementar.LocalReport.DataSources.Add(dataSourceComplementar);
                    ScriptManager scriptManagerComplementar = new ScriptManager();
                    scriptManagerComplementar.RegisterPostBackControl(GuiaComplementar);
                    GuiaComplementar.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"Relatorios\Faturamento\Guias\Spsadt\guia_spsadt_despesas.rdlc");

                    // PDFs de paginas complementares
                    Warning[] warningsComplementar;
                    string[] streamIdsComplementar;
                    string mimeTypeComplementar = string.Empty;
                    string encodingComplementar = string.Empty;
                    string extensionComplementar = "pdf";
                    byte[] pdfBytesComplementar = GuiaComplementar.LocalReport.Render("PDF", null, out mimeTypeComplementar, out encodingComplementar, out extensionComplementar, out streamIdsComplementar, out warningsComplementar);
                    pdfBytesComplementares.Add(pdfBytesComplementar);
                }
                // FIM - Guias complementares
                
                // Guias 'Outras Despesas'
                // Gerando sublistas (uma para cada pagina)
                var tamanhoOutrasDespesas = 7;

                for (int i = 0; i < itensGuiaOutrasDespesas.Count; i += tamanhoOutrasDespesas)
                {
                    listasGuiaOutrasDespesas.Add(itensGuiaOutrasDespesas.GetRange(i, Math.Min(tamanhoOutrasDespesas, itensGuiaOutrasDespesas.Count - i)));
                }

                List<byte[]> pdfBytesOutrasDespesas = new List<byte[]>();

                // Um relatorio extra para cada subLista de itens
                foreach (var lista in listasGuiaOutrasDespesas)
                {
                    Guias DS = new Guias();
                    List<GuiaSpsadtDespesaItem> despesaItens = new List<GuiaSpsadtDespesaItem>();

                    foreach (var item in lista)
                    {
                        var despesaItem = new GuiaSpsadtDespesaItem();
                        despesaItem.Descricao = item.Descricao + item.FaturamentoItem.Descricao;

                        despesaItem.CodigoItem = item.FaturamentoItem.CodTuss;
                        despesaItem.Qtde = item.Qtde.ToString();
                        despesaItem.Data = string.Format("{0:dd/MM/yyyy}", item.Data);
                        despesaItem.HoraInicial = string.Format("{0:HH:mm}", item.HoraIncio);
                        despesaItem.HoraFinal = string.Format("{0:HH:mm}", item.HoraFim);
                        despesaItem.ValorUnitario = string.Format("{0:#,##0.00}", item.ValorItem);
                        despesaItem.ValorTotal = string.Format("{0:#,##0.00}", (item.ValorItem * item.Qtde));
                        despesaItem.Tabela = item.FaturamentoConfigConvenioDto?.Codigo;

                        despesaItens.Add(despesaItem);
                    }

                    DataTable tabelaComplemetar = this.ConvertToDataTable(despesaItens, DS.Tables["SpsadtDespesasItens"]);
                    ReportDataSource dataSourceComplementar = new ReportDataSource("SpsadtDespesasItens", tabelaComplemetar);
                    ReportViewer GuiaComplementar = new ReportViewer();
                    GuiaComplementar.LocalReport.DataSources.Add(dataSourceComplementar);
                    ScriptManager scriptManagerComplementar = new ScriptManager();
                    scriptManagerComplementar.RegisterPostBackControl(GuiaComplementar);
                    GuiaComplementar.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"Relatorios\Faturamento\Guias\Spsadt\guia_spsadt_despesas.rdlc");

                    // PDF 'Outras Despesas'
                    Warning[] warningsComplementar;
                    string[] streamIdsComplementar;
                    string mimeTypeComplementar = string.Empty;
                    string encodingComplementar = string.Empty;
                    string extensionComplementar = "pdf";
                    byte[] pdfBytesComplementar = GuiaComplementar.LocalReport.Render("PDF", null, out mimeTypeComplementar, out encodingComplementar, out extensionComplementar, out streamIdsComplementar, out warningsComplementar);
                    pdfBytesComplementares.Add(pdfBytesComplementar);
                }

                // FIM - Guias 'Outras Despesas'

                // Anexando todos os relatorios
                GuiaSpsadt.LocalReport.Refresh();
                MemoryStream guiaPrincipalStream = new MemoryStream(pdfBytes); // guia principal
                PdfDocument guiaPrincipalPdf = PdfReader.Open(guiaPrincipalStream, PdfDocumentOpenMode.Import);
                PdfDocument pdfDefinitivo = new PdfDocument();

                // Gerando paginas da guia principal
                int principalPageCount = guiaPrincipalPdf.PageCount;
                for (int i = 0; i < principalPageCount; i++)
                {
                    PdfPage page = guiaPrincipalPdf.Pages[i];
                    page = pdfDefinitivo.AddPage(page);
                }

                // Guias complementares
                foreach (var pdfBs in pdfBytesComplementares)
                {
                    MemoryStream guiaComplementarStream = new MemoryStream(pdfBs); // guia outras despesas
                    PdfDocument guiaComplementarPdf = PdfReader.Open(guiaComplementarStream, PdfDocumentOpenMode.Import);
                    int compPageCount = guiaComplementarPdf.PageCount;
                    for (int i = 0; i < compPageCount; i++)
                    {
                        PdfPage page = guiaComplementarPdf.Pages[i];
                        page = pdfDefinitivo.AddPage(page);
                    }
                }

                // Gerando pdf byte array, relatorio definitivo
                byte[] definitivaBytes = null;

                using (MemoryStream stream = new MemoryStream())
                {
                    pdfDefinitivo.Save(stream, true);
                    definitivaBytes = stream.ToArray();
                }

                Response.Headers.Add("Content-Disposition", "inline; filename=guia_spsadt.pdf");
                return File(definitivaBytes, "application/pdf");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return null;
        }

        public void SetParametrosSpsadt (ReportViewer rv, GuiaSpsadtModel dados)
        {
            ReportParameter NumeroGuiaPrestador               = new ReportParameter("NumeroGuiaPrestador", string.IsNullOrEmpty(dados.NumeroGuiaPrestador) ? " " : dados.NumeroGuiaPrestador);
            ReportParameter RegistroAns                       = new ReportParameter("RegistroAns",         string.IsNullOrEmpty(dados.RegistroAns) ? " " : dados.RegistroAns);
            ReportParameter NumeroGuiaPrincipal               = new ReportParameter("NumeroGuiaPrincipal", string.IsNullOrEmpty(dados.NumeroGuiaPrincipal) ? " " : dados.NumeroGuiaPrincipal);
            ReportParameter DataAutorizacao                   = new ReportParameter("DataAutorizacao",     string.IsNullOrEmpty(dados.DataAutorizacao) ? " " : dados.DataAutorizacao);
            ReportParameter Senha                             = new ReportParameter("Senha",               string.IsNullOrEmpty(dados.Senha) ? " " : dados.Senha);
            ReportParameter DataValidadeSenha                 = new ReportParameter("DataValidadeSenha",   string.IsNullOrEmpty(dados.DataValidadeSenha) ? " " : dados.DataValidadeSenha);
            ReportParameter NumeroGuiaOperadora               = new ReportParameter("NumeroGuiaOperadora", string.IsNullOrEmpty(dados.NumeroGuiaOperadora) ? " " : dados.NumeroGuiaOperadora);
            ReportParameter NumeroCarteira                    = new ReportParameter("NumeroCarteira",      string.IsNullOrEmpty(dados.NumeroCarteira) ? " " : dados.NumeroCarteira);
            ReportParameter ValidadeCarteira                  = new ReportParameter("ValidadeCarteira",    string.IsNullOrEmpty(dados.ValidadeCarteira) ? " " : dados.ValidadeCarteira);
            ReportParameter NomePaciente                      = new ReportParameter("NomePaciente",        string.IsNullOrEmpty(dados.NomePaciente) ? " " : dados.NomePaciente);
            ReportParameter CartaoNacionalSaude               = new ReportParameter("CartaoNacionalSaude", string.IsNullOrEmpty(dados.NumeroCns) ? " " : dados.NumeroCns);
            ReportParameter AtendimentoRn                     = new ReportParameter("AtendimentoRn",       string.IsNullOrEmpty(dados.AtendimentoRn) ? " " : dados.AtendimentoRn);
            ReportParameter CodigoOperadora                   = new ReportParameter("CodigoOperadora",     string.IsNullOrEmpty(dados.CodigoOperadora) ? " " : dados.CodigoOperadora);
            ReportParameter NomeContratado                    = new ReportParameter("NomeContratado",      string.IsNullOrEmpty(dados.NomeContratado) ? " " : dados.NomeContratado);
            ReportParameter NomeProfissionalSolicitante       = new ReportParameter("NomeProfissionalSolicitante", string.IsNullOrEmpty(dados.NomeProfissionalSolicitante) ? " " : dados.NomeProfissionalSolicitante);
            ReportParameter ConselhoProfissional              = new ReportParameter("ConselhoProfissional", string.IsNullOrEmpty(dados.ConselhoProfissional) ? " " : dados.ConselhoProfissional);
            ReportParameter NumeroConselho                    = new ReportParameter("NumeroConselho", string.IsNullOrEmpty(dados.NumeroConselho) ? " " : dados.NumeroConselho);
            ReportParameter UF                                = new ReportParameter("UF", string.IsNullOrEmpty(dados.UF) ? " " : dados.UF);
            ReportParameter CodigoCbo                         = new ReportParameter("CodigoCbo", string.IsNullOrEmpty(dados.CodigoCbo) ? " " : dados.CodigoCbo);
            ReportParameter AssinaturaProfissionalSolicitante = new ReportParameter("AssinaturaProfissionalSolicitante", string.IsNullOrEmpty(dados.AssinaturaProfissionalSolicitante) ? " " : dados.AssinaturaProfissionalSolicitante);
            ReportParameter CaraterAtendimento                = new ReportParameter("CaraterAtendimento", string.IsNullOrEmpty(dados.CaraterAtendimento) ? " " : dados.CaraterAtendimento);
            ReportParameter DataSolicitacao                   = new ReportParameter("DataSolicitacao", string.IsNullOrEmpty(dados.DataSolicitacao) ? " " : dados.DataSolicitacao);
            ReportParameter IndicacaoClinica                  = new ReportParameter("IndicacaoClinica", string.IsNullOrEmpty(dados.IndicacaoClinica) ? " " : dados.IndicacaoClinica);
            ReportParameter CodigoCne                         = new ReportParameter("CodigoCne", string.IsNullOrEmpty(dados.CodigoCne) ? " " : dados.CodigoCne);
            ReportParameter TipoAtendimento                   = new ReportParameter("TipoAtendimento", string.IsNullOrEmpty(dados.TipoAtendimento) ? " " : dados.TipoAtendimento);
            ReportParameter IndicacaoAcidente                 = new ReportParameter("IndicacaoAcidente", string.IsNullOrEmpty(dados.IndicacaoAcidente) ? " " : dados.IndicacaoAcidente);
            ReportParameter TipoConsulta                      = new ReportParameter("TipoConsulta", string.IsNullOrEmpty(dados.TipoConsulta) ? " " : dados.TipoConsulta);
            ReportParameter MotivoEncerramentoAtendimento     = new ReportParameter("MotivoEncerramentoAtendimento", string.IsNullOrEmpty(dados.MotivoEncerramentoAtendimento) ? " " : dados.MotivoEncerramentoAtendimento);
            // Identificacao Equipe
            ReportParameter SequenciaRef1 = new ReportParameter("SequenciaRef1", string.IsNullOrEmpty(dados.SequenciaRef1) ? " " : dados.SequenciaRef1);
            ReportParameter SequenciaRef2 = new ReportParameter("SequenciaRef2", string.IsNullOrEmpty(dados.SequenciaRef2) ? " " : dados.SequenciaRef2);
            ReportParameter SequenciaRef3 = new ReportParameter("SequenciaRef3", string.IsNullOrEmpty(dados.SequenciaRef3) ? " " : dados.SequenciaRef3);
            ReportParameter SequenciaRef4 = new ReportParameter("SequenciaRef4", string.IsNullOrEmpty(dados.SequenciaRef4) ? " " : dados.SequenciaRef4);
            ReportParameter GrauPart1 = new ReportParameter("GrauPart1", string.IsNullOrEmpty(dados.GrauPart1) ? " " : dados.GrauPart1);
            ReportParameter GrauPart2 = new ReportParameter("GrauPart2", string.IsNullOrEmpty(dados.GrauPart2) ? " " : dados.GrauPart2);
            ReportParameter GrauPart3 = new ReportParameter("GrauPart3", string.IsNullOrEmpty(dados.GrauPart3) ? " " : dados.GrauPart3);
            ReportParameter GrauPart4 = new ReportParameter("GrauPart4", string.IsNullOrEmpty(dados.GrauPart4) ? " " : dados.GrauPart4);
            ReportParameter CodigoOperadoraCpf1 = new ReportParameter("CodigoOperadoraCpf1", string.IsNullOrEmpty(dados.CodigoOperadoraCpf1) ? " " : dados.CodigoOperadoraCpf1);
            ReportParameter CodigoOperadoraCpf2 = new ReportParameter("CodigoOperadoraCpf2", string.IsNullOrEmpty(dados.CodigoOperadoraCpf2) ? " " : dados.CodigoOperadoraCpf2);
            ReportParameter CodigoOperadoraCpf3 = new ReportParameter("CodigoOperadoraCpf3", string.IsNullOrEmpty(dados.CodigoOperadoraCpf3) ? " " : dados.CodigoOperadoraCpf3);
            ReportParameter CodigoOperadoraCpf4 = new ReportParameter("CodigoOperadoraCpf4", string.IsNullOrEmpty(dados.CodigoOperadoraCpf4) ? " " : dados.CodigoOperadoraCpf4);
            ReportParameter NomeProfissional1 = new ReportParameter("NomeProfissional1", string.IsNullOrEmpty(dados.NomeProfissional1) ? " " : dados.NomeProfissional1);
            ReportParameter NomeProfissional2 = new ReportParameter("NomeProfissional2", string.IsNullOrEmpty(dados.NomeProfissional2) ? " " : dados.NomeProfissional2);
            ReportParameter NomeProfissional3 = new ReportParameter("NomeProfissional3", string.IsNullOrEmpty(dados.NomeProfissional3) ? " " : dados.NomeProfissional3);
            ReportParameter NomeProfissional4 = new ReportParameter("NomeProfissional4", string.IsNullOrEmpty(dados.NomeProfissional4) ? " " : dados.NomeProfissional4);
            ReportParameter ConselhoProfissional1 = new ReportParameter("ConselhoProfissional1", string.IsNullOrEmpty(dados.ConselhoProfissional1) ? " " : dados.ConselhoProfissional1);
            ReportParameter ConselhoProfissional2 = new ReportParameter("ConselhoProfissional2", string.IsNullOrEmpty(dados.ConselhoProfissional2) ? " " : dados.ConselhoProfissional2);
            ReportParameter ConselhoProfissional3 = new ReportParameter("ConselhoProfissional3", string.IsNullOrEmpty(dados.ConselhoProfissional3) ? " " : dados.ConselhoProfissional3);
            ReportParameter ConselhoProfissional4 = new ReportParameter("ConselhoProfissional4", string.IsNullOrEmpty(dados.ConselhoProfissional4) ? " " : dados.ConselhoProfissional4);
            ReportParameter NumeroConselho1 = new ReportParameter("NumeroConselho1", string.IsNullOrEmpty(dados.NumeroConselho1) ? " " : dados.NumeroConselho1);
            ReportParameter NumeroConselho2 = new ReportParameter("NumeroConselho2", string.IsNullOrEmpty(dados.NumeroConselho2) ? " " : dados.NumeroConselho2);
            ReportParameter NumeroConselho3 = new ReportParameter("NumeroConselho3", string.IsNullOrEmpty(dados.NumeroConselho3) ? " " : dados.NumeroConselho3);
            ReportParameter NumeroConselho4 = new ReportParameter("NumeroConselho4", string.IsNullOrEmpty(dados.NumeroConselho4) ? " " : dados.NumeroConselho4);
            ReportParameter Uf1 = new ReportParameter("Uf1", string.IsNullOrEmpty(dados.Uf1) ? " " : dados.Uf1);
            ReportParameter Uf2 = new ReportParameter("Uf2", string.IsNullOrEmpty(dados.Uf2) ? " " : dados.Uf2);
            ReportParameter Uf3 = new ReportParameter("Uf3", string.IsNullOrEmpty(dados.Uf3) ? " " : dados.Uf3);
            ReportParameter Uf4 = new ReportParameter("Uf4", string.IsNullOrEmpty(dados.Uf4) ? " " : dados.Uf4);
            ReportParameter CodigoCbo1 = new ReportParameter("CodigoCbo1", string.IsNullOrEmpty(dados.CodigoCbo1) ? " " : dados.CodigoCbo1);
            ReportParameter CodigoCbo2 = new ReportParameter("CodigoCbo2", string.IsNullOrEmpty(dados.CodigoCbo2) ? " " : dados.CodigoCbo2);
            ReportParameter CodigoCbo3 = new ReportParameter("CodigoCbo3", string.IsNullOrEmpty(dados.CodigoCbo3) ? " " : dados.CodigoCbo3);
            ReportParameter CodigoCbo4 = new ReportParameter("CodigoCbo4", string.IsNullOrEmpty(dados.CodigoCbo4) ? " " : dados.CodigoCbo4);
            // Datas e Assinaturas (procedimentos em serie)
            ReportParameter DataRealizacaoProcedimentoSerie1 = new ReportParameter("DataRealizacaoProcedimentoSerie1", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie1) ? " " : dados.DataRealizacaoProcedimentoSerie1);
            ReportParameter DataRealizacaoProcedimentoSerie2 = new ReportParameter("DataRealizacaoProcedimentoSerie2", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie2) ? " " : dados.DataRealizacaoProcedimentoSerie2);
            ReportParameter DataRealizacaoProcedimentoSerie3 = new ReportParameter("DataRealizacaoProcedimentoSerie3", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie3) ? " " : dados.DataRealizacaoProcedimentoSerie3);
            ReportParameter DataRealizacaoProcedimentoSerie4 = new ReportParameter("DataRealizacaoProcedimentoSerie4", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie4) ? " " : dados.DataRealizacaoProcedimentoSerie4);
            ReportParameter DataRealizacaoProcedimentoSerie5 = new ReportParameter("DataRealizacaoProcedimentoSerie5", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie5) ? " " : dados.DataRealizacaoProcedimentoSerie5);
            ReportParameter DataRealizacaoProcedimentoSerie6 = new ReportParameter("DataRealizacaoProcedimentoSerie6", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie6) ? " " : dados.DataRealizacaoProcedimentoSerie6);
            ReportParameter DataRealizacaoProcedimentoSerie7 = new ReportParameter("DataRealizacaoProcedimentoSerie7", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie7) ? " " : dados.DataRealizacaoProcedimentoSerie7);
            ReportParameter DataRealizacaoProcedimentoSerie8 = new ReportParameter("DataRealizacaoProcedimentoSerie8", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie8) ? " " : dados.DataRealizacaoProcedimentoSerie8);
            ReportParameter DataRealizacaoProcedimentoSerie9 = new ReportParameter("DataRealizacaoProcedimentoSerie9", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie9) ? " " : dados.DataRealizacaoProcedimentoSerie9);
            ReportParameter DataRealizacaoProcedimentoSerie10 = new ReportParameter("DataRealizacaoProcedimentoSerie10", string.IsNullOrEmpty(dados.DataRealizacaoProcedimentoSerie10) ? " " : dados.DataRealizacaoProcedimentoSerie10);
            ReportParameter ObservacaoJustificativa = new ReportParameter("ObservacaoJustificativa", string.IsNullOrEmpty(dados.ObservacaoJustificativa) ? " " : dados.ObservacaoJustificativa);
            ReportParameter TotalProcedimentos = new ReportParameter("TotalProcedimentos", string.IsNullOrEmpty(dados.TotalProcedimentos) ? " " : dados.TotalProcedimentos);
            ReportParameter TotalTaxasAlugueis = new ReportParameter("TotalTaxasAlugueis", string.IsNullOrEmpty(dados.TotalTaxasAlugueis) ? " " : dados.TotalTaxasAlugueis);
            ReportParameter TotalMateriais = new ReportParameter("TotalMateriais", string.IsNullOrEmpty(dados.TotalMateriais) ? " " : dados.TotalMateriais);
            ReportParameter TotalOpme = new ReportParameter("TotalOpme", string.IsNullOrEmpty(dados.TotalOpme) ? " " : dados.TotalOpme);
            ReportParameter TotalMedicamentos = new ReportParameter("TotalMedicamentos", string.IsNullOrEmpty(dados.TotalMedicamentos) ? " " : dados.TotalMedicamentos);
            ReportParameter TotalGeral = new ReportParameter("TotalGeral", string.IsNullOrEmpty(dados.TotalGeral) ? " " : dados.TotalGeral);

            rv.LocalReport.SetParameters(new ReportParameter[] {
                          //   Titulo ,
                             NumeroGuiaPrestador ,
                             // Guia
                             RegistroAns ,
                             NumeroGuiaPrincipal ,
                             DataAutorizacao ,
                             Senha ,
                             DataValidadeSenha,
                             NumeroGuiaOperadora ,
                             NumeroCarteira ,
                             ValidadeCarteira ,
                             NomePaciente ,
                             CartaoNacionalSaude ,
                           //  AtendimentoRn ,
                             CodigoOperadora ,
                             NomeContratado ,
                             NomeProfissionalSolicitante ,
                             ConselhoProfissional ,
                             NumeroConselho ,
                             UF ,
                             CodigoCbo ,
                            // AtendimentoRn,
                         //    AssinaturaProfissionalSolicitante ,
                             CaraterAtendimento ,
                             DataSolicitacao ,
                             IndicacaoClinica ,
                        //     CodigoCne ,
                             TipoAtendimento ,
                             IndicacaoAcidente ,
                             TipoConsulta ,
                             MotivoEncerramentoAtendimento ,
                             // Identificacao Equipe
                             //SequenciaRef1 ,
                             //SequenciaRef2 ,
                             //SequenciaRef3 ,
                             //SequenciaRef4 ,
                             //GrauPart1 ,
                             //GrauPart2 ,
                             //GrauPart3 ,
                             //GrauPart4 ,
                             //CodigoOperadoraCpf1 ,
                             //CodigoOperadoraCpf2 ,
                             //CodigoOperadoraCpf3 ,
                             //CodigoOperadoraCpf4 ,
                             //NomeProfissional1 ,
                             //NomeProfissional2 ,
                             //NomeProfissional3 ,
                             //NomeProfissional4 ,
                             //ConselhoProfissional1 ,
                             //ConselhoProfissional2 ,
                             //ConselhoProfissional3 ,
                             //ConselhoProfissional4 ,
                             //NumeroConselho1 ,
                             //NumeroConselho2 ,
                             //NumeroConselho3 ,
                             //NumeroConselho4 ,
                             //Uf1 ,
                             //Uf2 ,
                             //Uf3 ,
                             //Uf4 ,
                             //CodigoCbo1 ,
                             //CodigoCbo2 ,
                             //CodigoCbo3 ,
                             //CodigoCbo4 ,
                             //// Datas e Assinaturas (procedimentos em serie)
                             //DataRealizacaoProcedimentoSerie1 ,
                             //DataRealizacaoProcedimentoSerie2 ,
                             //DataRealizacaoProcedimentoSerie3 ,
                             //DataRealizacaoProcedimentoSerie4 ,
                             //DataRealizacaoProcedimentoSerie5 ,
                             //DataRealizacaoProcedimentoSerie6 ,
                             //DataRealizacaoProcedimentoSerie7 ,
                             //DataRealizacaoProcedimentoSerie8 ,
                             //DataRealizacaoProcedimentoSerie9 ,
                             //DataRealizacaoProcedimentoSerie10 ,
                             //ObservacaoJustificativa ,
                             //TotalProcedimentos ,
                             //TotalTaxasAlugueis ,
                             //TotalMateriais ,
                             //TotalOpme ,
                             //TotalMedicamentos ,
                             //TotalGeral
                        });
        }

        public void LocalReport_SubreportProcessingSpsadt (object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                var dados = new GuiaSpsadtModel();
                dados.Contas = new List<ContaMedicaReportModel>();
                var x = new ContaMedicaReportModel();
                x.AtendimentoCodigo = "12313212";
                dados.Contas.Add(x);
                Guias relDS = new Guias();

                // Itens solicitados
                List<GuiaSpsadtItemSolic> itensSolics = new List<GuiaSpsadtItemSolic>();

                foreach (var item in _itensGuiaPrincipalPrimeiraPagina)
                {
                    var ex1 = new GuiaSpsadtItemSolic();
                    ex1.CodigoProcedimento = item.FaturamentoItem.CodTuss;
                    ex1.Descricao = item.Descricao + " " + item.FaturamentoItem.Descricao;
                    ex1.QtAutoriz = "";
                    ex1.QtSolic = item.Qtde > 0 ? item.Qtde.ToString() : "";
                    ex1.Tabela = item.FaturamentoConfigConvenioDto?.Codigo;

                    itensSolics.Add(ex1);
                }

                DataTable tabelaItensSolic = this.ConvertToDataTable(itensSolics, relDS.Tables["SpsadtItensSolic"]);
                ReportDataSource dataSourceItensSolic = new ReportDataSource("GuiaSpsadtItensSolic", tabelaItensSolic);
                e.DataSources.Add(dataSourceItensSolic);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void LocalReport_SubreportProcessingSpsadtExames (object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                var dados = new GuiaSpsadtModel();
                dados.Contas = new List<ContaMedicaReportModel>();
                var x = new ContaMedicaReportModel();
                x.AtendimentoCodigo = "1234";
                dados.Contas.Add(x);
                Guias relDS = new Guias();

                // Exames
                List<GuiaSpsadtExame> exames = new List<GuiaSpsadtExame>();

                // Itens solicitados
                List<GuiaSpsadtItemSolic> itensSolics = new List<GuiaSpsadtItemSolic>();

                foreach (var item in _itensGuiaPrincipalPrimeiraPagina)
                {
                    var valorTotal = (item.ValorItem * item.Qtde);

                    var ex1 = new GuiaSpsadtExame();
                    ex1.CodigoProcedimento = item.FaturamentoItem.CodTuss;
                    ex1.Descricao = item.Descricao + " " + item.FaturamentoItem.Descricao;
                    ex1.Tabela = item.TabelaUtilizada;
                    ex1.Data = item.Data?.ToString("dd/MM/yy");
                    ex1.HoraInicial = string.Format("{0:HH:mm}", item.HoraIncio);// item.HoraIncio?.ToString("mm:ss");
                    ex1.HoraFinal = string.Format("{0:HH:mm}", item.HoraFim);// item.HoraFim?.ToString("mm:ss tt");
                    ex1.Qtde = item.Qtde > 0 ? item.Qtde.ToString() : "";
                    ex1.ValorUnitario = item.ValorItem > 0 ? string.Format("{0:#,##0.00}", item.ValorItem): "";
                    ex1.ValorTotal = valorTotal > 0 ? string.Format("{0:#,##0.00}", valorTotal): "";
                    ex1.Tabela = item.FaturamentoConfigConvenioDto?.Codigo;
                    exames.Add(ex1);
                }

                DataTable tabelaExames = this.ConvertToDataTable(exames, relDS.Tables["SpsadtExames"]);
                ReportDataSource dataSourceExames = new ReportDataSource("GuiaSpsadtExames", tabelaExames);
                e.DataSources.Add(dataSourceExames);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void LocalReport_SubreportProcessingSpsadtEquipe (object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                var dados = new GuiaSpsadtModel();
                dados.Contas = new List<ContaMedicaReportModel>();
                var x = new ContaMedicaReportModel();
                x.AtendimentoCodigo = "123";
                dados.Contas.Add(x);
                Guias relDS = new Guias();

                // Equipe

                List<GuiaSpsadtEquipe> equipe = new List<GuiaSpsadtEquipe>();
                for (int i = 0; i < 4; i++)
                {
                    var ex1 = new GuiaSpsadtEquipe();
                    ex1.CodigoCbo = "";
                    ex1.CodigoOperadoraCpf = "";
                    ex1.ConselhoProfissional = "";
                    ex1.GrauPart = "";
                    ex1.NomeProfissional = "";
                    ex1.NumeroConselho = "";
                    ex1.SeqRef = "";
                    ex1.Uf = "";

                    equipe.Add(ex1);
                }

                DataTable tabelaEquipe = this.ConvertToDataTable(equipe, relDS.Tables["SpsadtEquipe"]);
                ReportDataSource dataSourceEquipe = new ReportDataSource("GuiaSpsadtEquipe", tabelaEquipe);
                e.DataSources.Add(dataSourceEquipe);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void LocalReport_SubreportProcessingSpsadtDespesas (object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                var dados = new GuiaSpsadtModel();
                dados.Contas = new List<ContaMedicaReportModel>();
                var x = new ContaMedicaReportModel();
                x.AtendimentoCodigo = "12313212";
                dados.Contas.Add(x);
                Guias relDS = new Guias();

                // Despesa Itens

                List<GuiaSpsadtDespesaItem> equipe = new List<GuiaSpsadtDespesaItem>();
                for (int i = 0; i < 7; i++)
                {
                    var ex1 = new GuiaSpsadtDespesaItem();
                    ex1.Descricao = "" + i.ToString();
                    ex1.Cd = "" + i.ToString();
                    equipe.Add(ex1);
                }

                DataTable tabelaEquipe = this.ConvertToDataTable(equipe, relDS.Tables["SpsadtDespesasItens"]);
                ReportDataSource dataSourceEquipe = new ReportDataSource("SpsadtDespesasItens", tabelaEquipe);
                e.DataSources.Add(dataSourceEquipe);

                // Despesa loop

                //List<teste> loop = new List<teste>();
                //for (int i = 0; i < 4; i++)
                //{
                //    var ex1 = new teste();
                //    ex1.Despesas = "despesas" + i.ToString();

                //    loop.Add(ex1);
                //}

                //DataTable tabelaEquipeloop = this.ConvertToDataTable(loop, relDS.Tables["SpsadtDespesas"]);
                //ReportDataSource dataSourceEquipeloop = new ReportDataSource("SpsadtDespesasLoopDataSet", tabelaEquipeloop);
                //e.DataSources.Add(dataSourceEquipeloop);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public DataTable ConvertToDataTable<T> (IList<T> data, DataTable table)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));

            if (data != null)
            {
                foreach (T item in data)
                {
                    try
                    {
                        DataRow row = table.NewRow();
                        foreach (PropertyDescriptor prop in properties)
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        table.Rows.Add(row);
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
            }

            return table;
        }

        //public static IEnumerable<List<T>> repartirLista<T> (List<T> lista, int tamanho)
        //{

        //        for (int i = 0; i < lista.Count; i+= tamanho)
        //        {
        //            yield return lista.GetRange(i, Math.Min(tamanho, lista.Count - i));
        //        }

        //}
    }

    public class GuiaSpsadtItemSolic
    {
        public string Tabela { get; set; }
        public string CodigoProcedimento { get; set; }
        public string Descricao { get; set; }
        public string QtSolic { get; set; }
        public string QtAutoriz { get; set; }
    }

    public class GuiaSpsadtExame
    {
        public string Data { get; set; }
        public string HoraInicial { get; set; }
        public string HoraFinal { get; set; }
        public string Tabela { get; set; }
        public string CodigoProcedimento { get; set; }
        public string Descricao { get; set; }
        public string Qtde { get; set; }
        public string Via { get; set; }
        public string Tec { get; set; }
        public string RedAcresc { get; set; }
        public string ValorUnitario { get; set; }
        public string ValorTotal { get; set; }
    }

    public class GuiaSpsadtEquipe
    {
        public string SeqRef { get; set; }
        public string GrauPart { get; set; }
        public string CodigoOperadoraCpf { get; set; }
        public string NomeProfissional { get; set; }
        public string ConselhoProfissional { get; set; }
        public string NumeroConselho { get; set; }
        public string Uf { get; set; }
        public string CodigoCbo { get; set; }
    }

    //public class GuiaSpsadtDespesaItem
    //{
    //    public string Cd { get; set; }
    //    public string Data { get; set; }
    //    public string HoraInicial { get; set; }
    //    public string HoraFinal { get; set; }
    //    public string Tabela { get; set; }
    //    public string CodigoItem { get; set; }
    //    public string Qtde { get; set; }
    //    public string UnidadeMedida { get; set; }
    //    public string RedAcres { get; set; }
    //    public string ValorUnitario { get; set; }
    //    public string ValorTotal { get; set; }
    //    public string RegistroAnvisa { get; set; }
    //    public string RefMaterialFabricante { get; set; }
    //    public string NumAutorizacaoFuncionamento { get; set; }
    //    public string Descricao { get; set; }

    //    public void LerContaItem(FaturamentoContaItem item)
    //    {
    //        Cd = "";
    //        Data = item.Data?.ToString("dd/MM/yyyy");
    //        HoraInicial = item.HoraIncio.ToString();
    //        HoraFinal = item.HoraFim.ToString();
    //        Tabela = "";
    //        CodigoItem = item.Codigo;
    //        Qtde = item.Qtde.ToString();
    //        UnidadeMedida = "";
    //        RedAcres = "";
    //        ValorUnitario = "";
    //        ValorTotal = "";
    //        RegistroAnvisa = "";
    //        RefMaterialFabricante = "";
    //        NumAutorizacaoFuncionamento = "";
    //        Descricao = item.Descricao;
    //    }
    //}

}