﻿using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Faturamentos.CentralAutorizacaoGuias;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Faturamentos
{
    public class CentralAutorizacaoController : SWMANAGERControllerBase
    {
        // GET: Mpa/Agendamentos
        public ActionResult Index()
        {
            var viewModel = new CentralAutorizacaoGuiasViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Faturamentos/CentralAutorizacaoGuias/Index.cshtml", viewModel);
        }
    }
}