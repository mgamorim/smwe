﻿using Abp.Application.Services.Dto;
using Abp.Threading;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Taxas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Taxas.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Faturamentos.TabelaPrecoConvenios;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Faturamentos.Taxas;
using SW10.SWMANAGER.Web.Controllers;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Faturamentos
{
    public class FaturamentoTabelaPrecoConveniosController : SWMANAGERControllerBase
    {
        #region Cabecalho
        private readonly IFaturamentoConfigConvenioAppService _configConvenioAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly IContaAppService _contaAppService;
        private readonly UserManager _userManager;
        private readonly IUserAppService _userAppService;
        private readonly IFaturamentoTaxaAppService _taxaAppService;

        public FaturamentoTabelaPrecoConveniosController (
            //   ITabelaPrecoConvenioAppService tabelaPrecoConvenioAppService
            //   ,

            IFaturamentoConfigConvenioAppService configConvenioAppService
            ,
            IContaAppService contaAppService
            ,
            IConvenioAppService convenioAppService
            ,
            UserManager userManager
            ,
            IUserAppService userAppService
            ,
            IFaturamentoTaxaAppService taxaAppService
            )
        {
            // _tabelaPrecoConvenioAppService = tabelaPrecoConvenioAppService;
            _configConvenioAppService = configConvenioAppService;
            _contaAppService = contaAppService;
            _convenioAppService = convenioAppService;
            _userManager = userManager;
            _userAppService = userAppService;
            _taxaAppService = taxaAppService;
        }
        #endregion cabecalho.

        public ActionResult Index ()
        {
            var model = new FaturamentoTabelaPrecoConveniosViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Faturamentos/TabelaPrecoConvenios/Index.cshtml", model);
        }

        //[AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_ContaKit_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_ContaKit_Edit)]
        public async Task<ActionResult> CriarOuEditarModal (long? id/*, long? contaId = null*/)
        {
            FaturamentoConfigConvenioDto viewModel;

            viewModel = new FaturamentoConfigConvenioDto();
            viewModel.Convenio =  AsyncHelper.RunSync(() => _convenioAppService.ObterDto((long)id));
            viewModel.ConvenioId = id;


            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);

            ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);

            viewModel.Empresa = empresas.Items.FirstOrDefault();

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Faturamentos/TabelaPrecoConvenios/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<ActionResult> _Taxa (long? id)
        {
            FaturamentoTaxaViewModel viewModel;

            if (id.HasValue)
            {
                var taxaDto = AsyncHelper.RunSync(() => _taxaAppService.Obter((long)id));
                viewModel = new FaturamentoTaxaViewModel(taxaDto);
                
            }
            else
            {
                viewModel = new FaturamentoTaxaViewModel(new FaturamentoTaxaDto());
            }



            viewModel.Convenio =  AsyncHelper.RunSync(() => _convenioAppService.ObterDto((long)id));
            viewModel.ConvenioId = id;
            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
            ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);
            viewModel.Empresa = empresas.Items.FirstOrDefault();

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Faturamentos/TabelaPrecoConvenios/_Taxa.cshtml", viewModel);
        }



    }
}