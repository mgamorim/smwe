﻿using Abp.Application.Navigation;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.Authorization.Users.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ConsultorTabelas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ConsultorTabelas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Desenvolvimento;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Desenvolvimento;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Desenvolvimento.DocItens;
using SW10.SWMANAGER.Web.Areas.Mpa.Startup;
using SW10.SWMANAGER.Web.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Desenvolvimento.DocItens
{
    public class DocItensController : SWMANAGERControllerBase
    {
        private readonly IDocItemAppService _docItemAppService;
      
        public DocItensController(
            IDocItemAppService docItemAppService
            )
        {
            _docItemAppService = docItemAppService;
        }

        public ActionResult Index()
        {
            var model = new DocItensListagemViewModel();
            //model.DocItem = new ClassesAplicacao.Desenvolvimento.DocItemDto();

            return View("~/Areas/Mpa/Views/Aplicacao/Desenvolvimento/DocItens/Listagem.cshtml", model);
        }

    }
}