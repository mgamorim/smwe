﻿using Abp.Threading;
using Abp.UI;
using Newtonsoft.Json;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.KitsExames;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Manutencoes.MailingTemplates;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Enumeradores;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.AtestadosMedicos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.FichasPacientes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Home;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Prescricoes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.SolicitacoesExames;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.AtendimentosLeitosMov.Altas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.ProntuarioEletronico;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Assistenciais
{
    public class AssistenciaisController : SWMANAGERControllerBase
    {
        private readonly IUserAppService _userAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IMailingTemplateAppService _mailingTemplateAppService;
        private readonly IOperacaoAppService _operacaoAppService;
        private readonly IProntuarioEletronicoAppService _prontuarioAppService;
        private readonly IFormConfigAppService _formConfigAppService;
        private readonly ISolicitacaoExameAppService _solicitacaoExameAppService;
        private readonly ISolicitacaoExamePrioridadeAppService _solicitacaoExamePrioridadeAppService;
        private readonly ISolicitacaoExameItemAppService _solicitacaoExameItemAppService;
        private readonly IFaturamentoItemAppService _faturamentoItemAppService;
        private readonly IKitExameAppService _kitExameAppService;
        private readonly IPrescricaoMedicaAppService _prescricaoMedicaAppService;
        private readonly IDivisaoAppService _divisaoAppService;
        private readonly IPrescricaoItemRespostaAppService _prescricaoItemRespostaAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly IPrescricaoItemAppService _prescricaoItemAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IFrequenciaAppService _frequenciaAppService;
        private readonly IFormaAplicacaoAppService _formaAplicacaoAppService;
        private readonly ILocalChamadasAppService _localChamadaAppService;
        private readonly IPrescricaoStatusAppService _prescricaoStatusAppService;
        private readonly IRegistroArquivoAppService _registroArquivoAppService;

        public AssistenciaisController(
            IPacienteAppService pacienteAppService,
            IMailingTemplateAppService mailingTemplateAppService,
            IUserAppService userAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            IFormConfigAppService formConfigAppService,
            IAtendimentoAppService atendimentoAppService,
            ISolicitacaoExameAppService solicitacaoExameAppService,
            ISolicitacaoExameItemAppService solicitacaoExameItemAppService,
            ISolicitacaoExamePrioridadeAppService solicitacaoExamePrioridadeAppService,
            IFaturamentoItemAppService faturamentoItemAppService,
            IDivisaoAppService divisaoAppService,
            IPrescricaoItemRespostaAppService prescricaoItemRespostaAppService,
            IPrescricaoMedicaAppService prescricaoMedicaAppService,
            IOperacaoAppService operacaoAppService,
            IProntuarioEletronicoAppService prontuarioAppService,
            IKitExameAppService kitExameAppService,
            IUnidadeAppService unidadeAppService,
            IPrescricaoItemAppService prescricaoItemAppService,
            IMedicoAppService medicoAppService,
            IFrequenciaAppService frequenciaAppService,
            IFormaAplicacaoAppService formaAplicacaoAppService,
            ILocalChamadasAppService localChamadaAppService,
            IPrescricaoStatusAppService prescricaoStatusAppService,
            IRegistroArquivoAppService registroArquivoAppService
            )
        {
            _pacienteAppService = pacienteAppService;
            _mailingTemplateAppService = mailingTemplateAppService;
            _userAppService = userAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _formConfigAppService = formConfigAppService;
            _atendimentoAppService = atendimentoAppService;
            _solicitacaoExameAppService = solicitacaoExameAppService;
            _solicitacaoExameItemAppService = solicitacaoExameItemAppService;
            _solicitacaoExamePrioridadeAppService = solicitacaoExamePrioridadeAppService;
            _faturamentoItemAppService = faturamentoItemAppService;
            _divisaoAppService = divisaoAppService;
            _prescricaoItemRespostaAppService = prescricaoItemRespostaAppService;
            _prescricaoMedicaAppService = prescricaoMedicaAppService;
            _operacaoAppService = operacaoAppService;
            _prontuarioAppService = prontuarioAppService;
            _kitExameAppService = kitExameAppService;
            _unidadeAppService = unidadeAppService;
            _prescricaoItemAppService = prescricaoItemAppService;
            _medicoAppService = medicoAppService;
            _frequenciaAppService = frequenciaAppService;
            _formaAplicacaoAppService = formaAplicacaoAppService;
            _localChamadaAppService = localChamadaAppService;
            _prescricaoStatusAppService = prescricaoStatusAppService;
            _registroArquivoAppService = registroArquivoAppService;
        }

        // GET: Mpa/Assistenciais
        public ActionResult AmbulatoriosEmergencias()
        {
            var userId = AbpSession.UserId;
            var user = Task.Run(() => _userAppService.GetUser()).Result;
            var model = new AtendimentoDto();
            var viewModel = new AssistenciaisViewModel(model);
            HttpCookie coockie = Request.Cookies["localChamada"];
            if (coockie != null && !string.IsNullOrWhiteSpace(coockie.Value))
            {
                long localChamadaId = 0;
                if (long.TryParse(coockie.Value, out localChamadaId))
                {
                    var localChamada = Task.Run(() => _localChamadaAppService.Obter(localChamadaId)).Result;
                    ViewBag.LocalChamadaId = localChamadaId;
                    ViewBag.LocalChamada = localChamada;
                    ViewBag.TipoLocalChamadaId = localChamada.TipoLocalChamadaId;
                    ViewBag.TipoLocalChamada = localChamada.TipoLocalChamada;
                }
            }
            viewModel.IsAmbulatorioEmergencia = true;
            viewModel.IsInternacao = false;
            if (user.MedicoId.HasValue)
            {
                ViewBag.UserMedicoId = user.MedicoId;
                ViewBag.UserMedico = Task.Run(() => _medicoAppService.Obter(user.MedicoId.Value)).Result;
            }
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Home/Index.cshtml", viewModel);
        }

        public async Task<ActionResult> Internacoes()
        {
            var model = new AtendimentoDto();
            var viewModel = new AssistenciaisViewModel(model);
            viewModel.IsAmbulatorioEmergencia = false;
            viewModel.IsInternacao = true;

            var listaStatus = await _prescricaoStatusAppService.ListarTodos();

            if (listaStatus != null && listaStatus.Items != null)
            {
                viewModel.ListaStatus = listaStatus.Items.ToList();
            }
            else
            {
                viewModel.ListaStatus = new List<PrescricaoStatusDto>();
            }

            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Home/Index.cshtml", viewModel);
        }

        public async Task<ViewResult> FichaPaciente(long? id)
        {
            try
            {
                var model = new PacienteDto();
                if (id.HasValue)
                {
                    //model = AsyncHelper.RunSync(() => _pacienteAppService.Obter((long)id)).MapTo<PacienteDto>(); //await _pacienteAppService.Obter((long)id).MapTo<CriarOuEditarPaciente>();
                    model = await _pacienteAppService.Obter((long)id);
                }
                else
                {
                    var paciente = TempData.Peek("Paciente") as PacienteDto;
                    if (paciente != null)
                    {
                        model = paciente;
                    }
                }
                var criarOuEditarPacienteModalViewModel = new CriarOuEditarPacienteModalViewModel(model);

                var viewModel = new FichasPacientesViewModel();

                viewModel.CriarOuEditarPacienteModalViewModel = new CriarOuEditarPacienteModalViewModel(model); // viewModel;

                return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/FichasPacientes/Index.cshtml", viewModel);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroOcorrido", ex.Message.ToString()));
            }
        }

        public async Task<ActionResult> AtestadosMedicos(long id)
        {
            var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            if (atendimento == null)
            {
                PersistirAtendimento(id);
                atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            }
            atendimento = await _atendimentoAppService.Obter(id);
            var paciente = TempData.Peek("Paciente") as PacienteDto;
            var medico = TempData.Peek("Medico") as MedicoDto;
            var empresa = TempData.Peek("Empresa") as EmpresaDto;

            var viewModel = new AtestadoMedicoViewModel();
            var templates = await _mailingTemplateAppService.ListarTodos();
            viewModel.Templates = templates.Items.Where(m => m.Name.Contains("Atestado Médico")).ToList();
            if (paciente != null)
            {
                viewModel.Paciente = paciente;
            }
            if (medico != null)
            {
                viewModel.Medico = medico;
            }
            if (empresa != null)
            {
                viewModel.Empresa = empresa;
            }
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/AtestadosMedicos/Index.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _LerAtendimento(long id)
        {
            var atendimento = await _atendimentoAppService.ObterAssistencial(id);
            TempData["Atendimento"] = atendimento;

            var listaStatus = await _prescricaoStatusAppService.ListarTodos();

            if (listaStatus != null && listaStatus.Items != null)
            {
                atendimento.ListaStatus = listaStatus.Items.ToList();
            }
            else
            {
                atendimento.ListaStatus = new List<PrescricaoStatusDto>();
            }


            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Home/_LerAtendimento.cshtml", atendimento);
        }

        public ActionResult HeaderAtendimento(long id)
        {
            var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            if (atendimento == null)
            {
                PersistirAtendimento(id);
                atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            }
            //atendimento = AsyncHelper.RunSync(() => _atendimentoAppService.Obter(id));
            ViewBag.AtendimentoId = atendimento.Id;
            var paciente = atendimento.Paciente;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Pacientes/_HeaderPaciente.cshtml", atendimento);
        }

        public JsonResult PersistirAtendimento(long id)
        {
            if (id.Equals(0))
            {
                TempData.Remove("Atendimento");
            }
            else
            {
                var atendimento = AsyncHelper.RunSync(() => _atendimentoAppService.Obter(id));
                TempData["Atendimento"] = atendimento;
            }
            return Json("Successfully", JsonRequestBehavior.AllowGet);
        }

        #region enfermagem
        public ActionResult EnfermagemAdmissao()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "EnfermagemAdmissao";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult EnfermagemEvolucao()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "EnfermagemEvolucao";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult EnfermagemPassagemPlantao()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "EnfermagemPassagemPlantao";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult EnfermagemPrescricao()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Enfermagens/Prescricoes/Index.cshtml");
        }

        public ActionResult EnfermagemSinalVital()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Enfermagens/SinaisVitais/Index.cshtml");
        }

        public ActionResult EnfermagemChecagem()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Enfermagens/Checagens/Index.cshtml");
        }

        public ActionResult EnfermagemControleBalancoHidrico()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Enfermagens/ControlesBalancoHidrico/Index.cshtml");
        }
        #endregion

        #region medico
        public PartialViewResult MedicoAdmissao()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "MedicoAdmissao";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public async Task<ActionResult> MedicoAlta()
        {


            //var atendimentoDto = (AtendimentoDto)TempData["Atendimento"];
            //long atendimentoId = atendimentoDto.Id;

            ////  long.TryParse(TempData["Atendimento"].ToString(), out atendimentoId);


            //var atendimento = await _atendimentoAppService.Obter(atendimentoId);

            //AltaModalViewModel viewModel = new AltaModalViewModel(); ;

            //viewModel.AtendimentoId = atendimentoId;
            //viewModel.Data = (DateTime)(atendimento.DataAlta != null ? atendimento.DataAlta : DateTime.Now);
            //viewModel.DataAltaMedica = (DateTime)(atendimento.DataAltaMedica != null ? atendimento.DataAltaMedica : DateTime.Now);
            //viewModel.PrevisaoAlta = (DateTime)(atendimento.DataPrevistaAlta != null ? atendimento.DataPrevistaAlta : DateTime.Now);
            //if (atendimento.LeitoId.HasValue)
            //{
            //    viewModel.LeitoId = atendimento.Leito.Id;
            //    viewModel.Leito = atendimento.Leito;
            //}
            ////ViewBag.IsConsulta = false;

            //ViewBag.Title = "Alta";

            //return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Altas/Alta/_CriarOuEditarModal.cshtml", viewModel);


            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Altas/Index.cshtml");
        }

        public ActionResult MedicoAnamnese()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "MedicoAnamnese";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult MedicoEvolucao()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "MedicoEvolucao";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult MedicoParecerEspecialista()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "MedicoParecerEspecialista";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult MedicoPrescricao()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/Index.cshtml");
        }

        public ActionResult MedicoSolicitacaoExame()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/SolicitacoesExames/Index.cshtml");
        }

        public ActionResult MedicoResultadoExame()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/ExamesImagem/Index.cshtml");
        }

        public ActionResult MedicoResumoAlta()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "MedicoResumoAlta";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult MedicoDescricaoAtoCirurgico()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/DescricoesAtosCirurgicos/Index.cshtml");
        }

        public ActionResult MedicoDescricaoAtoAnestesico()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/DescricoesAtosAnestesicos/Index.cshtml");
        }

        public ActionResult MedicoFolhaGastoCentroCirurgico()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/FolhasGastosCentroCirurgicos/Index.cshtml");
        }

        public ActionResult MedicoPartograma()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Partogramas/Index.cshtml");
        }

        public PartialViewResult CriarOuEditarAdmissaoMedica(long? id)
        {
            var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            CriarOuEditarProntuarioEletronicoViewModel viewModel;
            ProntuarioEletronicoDto output;
            var formResposta = new FormResposta();
            if (id.HasValue)
            {
                output = AsyncHelper.RunSync(() => _prontuarioAppService.Obter(id.Value)); //await _admissaoMedicaAppService.Obter(id.Value);
                formResposta = output.FormResposta;
                if (output.FormRespostaId.HasValue)
                {
                    if (output.FormResposta != null && formResposta.FormConfig != null)
                    {
                        ViewBag.FormName = formResposta.FormConfig.Nome;
                    }
                    else
                    {
                        var formConfig = AsyncHelper.RunSync(() => _formConfigAppService.Obter(output.FormResposta.FormConfigId));
                        ViewBag.FormName = formConfig.Nome;
                    }
                }
                else
                {
                    ViewBag.FormName = L("SelecionarLista");
                }
            }
            else
            {
                //CriarOuEditarAssistencialAtendimento
                output = new ProntuarioEletronicoDto();
                output.AtendimentoId = atendimento.Id;
            }
            viewModel = new CriarOuEditarProntuarioEletronicoViewModel(output);
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            var especialidadeId = atendimento.EspecialidadeId;
            var formsDisp = Task.Run(() => _formConfigAppService.ListarRelacionados(operacao.Id, atendimento.UnidadeOrganizacionalId.Value, especialidadeId)).Result.ToList();
            if (formsDisp.Count() == 1)
            {
                viewModel.FormConfigId = formsDisp.FirstOrDefault().Id;
                viewModel.FormConfig = formsDisp.FirstOrDefault();
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/CriarOuEditarProntuarioEletronicoViewModel.cshtml", viewModel);
        }

        public async Task<PartialViewResult> CriarOuEditarSolicitacaoExameModal(long atendimentoId, long? id)
        {
            var atendimento = await _atendimentoAppService.Obter(atendimentoId); //TempData.Peek("Atendimento") as CriarOuEditarAssistencialAtendimento;
            CriarOuEditarSolicitacaoExameViewModel viewModel;
            SolicitacaoExameDto output;
            if (id.HasValue)
            {
                output = await _solicitacaoExameAppService.Obter(id.Value); //AsyncHelper.RunSync(() => _admissaoMedicaAppService.Obter(id.Value)); //await _admissaoMedicaAppService.Obter(id.Value);
                var prioriade = await _solicitacaoExamePrioridadeAppService.Obter(output.Prioridade);
                ViewBag.NomePrioridade = prioriade.Descricao;
            }
            else
            {
                output = new SolicitacaoExameDto();
                output.DataSolicitacao = DateTime.Now;
                output.AtendimentoId = atendimento.Id;
                output.Atendimento = atendimento;
            }
            viewModel = new CriarOuEditarSolicitacaoExameViewModel(output);
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/SolicitacoesExames/CriarOuEditarSolicitacaoExame.cshtml", viewModel);
        }

        public async Task<PartialViewResult> CriarOuEditarSolicitacaoExameItemModal(long solicitacaoId, long? id)
        {
            var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            CriarOuEditarSolicitacaoExameItemViewModel viewModel;
            SolicitacaoExameItemDto output;
            if (id.HasValue)
            {
                //output = await _solicitacaoExameItemAppService.ObterParaEdicao(id.Value);
                output = await _solicitacaoExameItemAppService.Obter(id.Value);
                if (output.FaturamentoItemId.HasValue)
                {
                    output.FaturamentoItem = await _faturamentoItemAppService.Obter(output.FaturamentoItemId.Value);
                }

                if (output.KitExameId.HasValue)
                {
                    output.KitExame = await _kitExameAppService.Obter(output.KitExameId.Value);
                }

                output.Solicitacao = await _solicitacaoExameAppService.Obter(output.SolicitacaoExameId);

                if (output.Solicitacao.AtendimentoId.HasValue)
                {
                    if (output.Solicitacao.AtendimentoId.Value == atendimento.Id)
                    {
                        output.Solicitacao.Atendimento = atendimento;
                    }
                    else
                    {
                        output.Solicitacao.Atendimento = await _atendimentoAppService.Obter(output.Solicitacao.AtendimentoId.Value);
                    }
                }
            }
            else
            {
                var solicitacao = await _solicitacaoExameAppService.Obter(solicitacaoId);
                output = new SolicitacaoExameItemDto();
                output.DataValidade = DateTime.Now;
                output.SolicitacaoExameId = solicitacao.Id;
                output.Solicitacao = solicitacao;
            }
            viewModel = new CriarOuEditarSolicitacaoExameItemViewModel(output);
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/SolicitacoesExames/CriarOuEditarSolicitacaoExameItem.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _CriarOuEditarMedicoPrescricao(long atendimentoId, long? id)
        {
            CriarOuEditarPrescricaoViewModel viewModel;
            if (id.HasValue)
            {
                var output = await _prescricaoMedicaAppService.Obter(id.Value);
                viewModel = new CriarOuEditarPrescricaoViewModel(output);
                viewModel.RespostasList = output.RespostasList;
            }
            else
            {
                viewModel = new CriarOuEditarPrescricaoViewModel(new PrescricaoMedicaDto());
                viewModel.RespostasList = "";
                viewModel.DataPrescricao = DateTime.Now;
            }
            var divisoes = await _divisaoAppService.ListarTodos();
            viewModel.Divisoes = divisoes.Items.OrderBy(m => m.Ordem).ToList();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_CriarOuEditar.cshtml", viewModel);
        }

        //public PartialViewResult _PrescricaoCompleta(List<PrescricaoItemRespostaDto> list)
        //{
        //    var result = Task.Run(() => _prescricaoMedicaAppService.ListarPrescricaoCompleta(list)).Result;
        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_PrescricaoCompleta.cshtml", result);
        //}

        public PartialViewResult _CriarOuEditarRespostaModal(long? id, long? divisaoId, long? gridId)
        {
            var model = new PrescricaoItemRespostaDto();
            if (id.HasValue)
            {
                model = Task.Run(() => _prescricaoItemRespostaAppService.Obter(id.Value)).Result;
            }
            var viewModel = new PrescricaoItemRespostaViewModel(model);
            if (!id.HasValue)
            {
                viewModel.DivisaoId = divisaoId;
                viewModel.Divisao = new DivisaoDto();
            }
            viewModel.IdGridPrescricaoItemResposta = gridId;
            TempData["IsModal"] = "modal";
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_CriarOuEditarRespostaModal.cshtml", viewModel);
        }

        public PartialViewResult _PrescricaoItemResposta(long? divisaoId)
        {
            var output = new PrescricaoItemRespostaDto();
            var viewModel = new PrescricaoItemRespostaViewModel(output);
            viewModel.DivisaoId = divisaoId;
            if (divisaoId.HasValue && divisaoId.Value > 0)
            {
                viewModel.Divisao = Task.Run(() => _divisaoAppService.Obter(divisaoId.Value)).Result;
            }
            else
            {
                viewModel.Divisao = new DivisaoDto();
            }
            viewModel.DataInicial = DateTime.Now;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_PrescricaoItemResposta.cshtml", viewModel);
        }

        public PartialViewResult _PrescricaoCompleta(List<PrescricaoItemRespostaDto> list = null)
        {
            var listFill = new List<PrescricaoItemRespostaDto>();
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (item.DivisaoId.HasValue && item.DivisaoId.Value > 0)
                    {
                        item.Divisao = Task.Run(() => _divisaoAppService.Obter(item.DivisaoId.Value)).Result;
                    }
                    else
                    {
                        var prescricaoItem = Task.Run(() => _prescricaoItemAppService.Obter(item.PrescricaoItemId.Value)).Result;
                        item.DivisaoId = prescricaoItem.DivisaoId;
                        item.Divisao = Task.Run(() => _divisaoAppService.Obter(item.DivisaoId.Value)).Result;
                    }

                    if (item.FormaAplicacaoId.HasValue)
                    {
                        item.FormaAplicacao = Task.Run(() => _formaAplicacaoAppService.Obter(item.FormaAplicacaoId.Value)).Result;
                    }
                    if (item.FrequenciaId.HasValue)
                    {
                        item.Frequencia = Task.Run(() => _frequenciaAppService.Obter(item.FrequenciaId.Value)).Result;
                    }
                    if (item.MedicoId.HasValue)
                    {
                        item.Medico = Task.Run(() => _medicoAppService.Obter(item.MedicoId.Value)).Result;
                    }
                    if (item.PrescricaoItemId.HasValue && item.PrescricaoItemId.Value > 0)
                    {
                        item.PrescricaoItem = Task.Run(() => _prescricaoItemAppService.Obter(item.PrescricaoItemId.Value)).Result;
                    }
                    if (item.UnidadeId.HasValue)
                    {
                        item.Unidade = Task.Run(() => _unidadeAppService.Obter(item.UnidadeId.Value)).Result;
                    }
                    if (item.UnidadeOrganizacionalId.HasValue)
                    {
                        item.UnidadeOrganizacional = Task.Run(() => _unidadeOrganizacionalAppService.ObterPorId(item.UnidadeOrganizacionalId.Value)).Result;
                    }
                    if (item.VelocidadeInfusaoId.HasValue)
                    {
                        item.Divisao = Task.Run(() => _divisaoAppService.Obter(item.DivisaoId.Value)).Result;
                    }
                    listFill.Add(item);
                }
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_PrescricaoCompleta.cshtml", listFill);
        }

        public PartialViewResult _TevMovimento(long atendimentoId)
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_TevMovimento.cshtml");
        }

        public PartialViewResult _ControlaTev(long atendimentoId)
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Home/_ControlaTev.cshtml");
        }

        #endregion

        #region administrativo
        public ActionResult AdministrativoCAT()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/CATs/Index.cshtml");
        }

        public ActionResult AdministrativoAlergia()
        {
            var activePage = TempData.Peek("ActivePage").ToString();
            var operacao = Task.Run(() => _operacaoAppService.ObterPorNome(activePage)).Result;
            TempData["OperacaoId"] = operacao.Id;
            ViewBag.Title = "AdministrativoAlergia";
            var viewModel = new IndexViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml", viewModel);
        }

        public ActionResult AdministrativoDocumentacaoPaciente()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/DocumentacaoPacientes/Index.cshtml");
        }

        public ActionResult AdministrativoConfirmacaoAgendaConsulta()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/ConfirmacoesAgendaConsultas/Index.cshtml");
        }

        public ActionResult AdministrativoConfirmacaoAgendaExame()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/ConfirmacoesAgendaExames/Index.cshtml");
        }

        public ActionResult AdministrativoConfirmacaoAgendaCirurgia()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/ConfirmacoesAgendaCirurgias/Index.cshtml");
        }

        public ActionResult AdministrativoTranferenciaLeito()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/TransferenciasLeitos/Index.cshtml");
        }

        public ActionResult AdministrativoTransferenciaMedicoResponsavel()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/TransferenciasMedicosResponsaveis/Index.cshtml");
        }

        public ActionResult AdministrativoTransferenciaSetor()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/TransferenciasSetores/Index.cshtml");
        }

        public ActionResult AdministrativoAlta()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/Altas/Index.cshtml");
        }

        public ActionResult AdministrativoAlteracaoAtendimento()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/AlteracoesAtendimentos/Index.cshtml");
        }

        public ActionResult AdministrativoPassagemPlantaoEnfermagem()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/PassagensPlantoesEnfermagem/Index.cshtml");
        }

        public ActionResult AdministrativoSolicitacaoProrrogacao()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/SolicitacoesProrrogacoes/Index.cshtml");
        }

        public ActionResult AdministrativoSolicitacaoProdutoSetor()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/SolicitacoesProdutosSetores/Index.cshtml");
        }

        public ActionResult AdministrativoSolicitacaoProdutoSOS()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/SolicitacoesProdutosSOS/Index.cshtml");
        }

        public ActionResult AdministrativoLiberacaoInterdicaoLeito()
        {
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Administrativos/LiberacoesInterdicoesLeitos/Index.cshtml");
        }
        #endregion

        public async Task<ActionResult> ListarRegistroArquivos(long id)
        {
            var atendimentoDto = await _atendimentoAppService.Obter(id);

            var model = new AtendimentoDto();
            model.Id = id;
            model.Codigo = atendimentoDto.Codigo;
            model.Paciente = atendimentoDto.Paciente;



            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/RegistrosArquivos/Index.cshtml", model);
        }

        [HttpPost]
        public string VisualizarImagemRegistroArquivo(long id)
        {

            try
            {
                //  RetornoArquivo retornoArquivo = new RetornoArquivo();

                var registro = _registroArquivoAppService.ObterPorId(id);
                var arquivo = registro.Arquivo;
                var base64 = Convert.ToBase64String(arquivo);
                var imgSrc = string.Format("data:{0};base64,{1}", "image/png", base64);






                //if (registro != null)
                //{
                //    arquivo = registro.Arquivo;

                //    if( registro.RegistroTabelaId == (long)EnumArquivoTabela.PrescricaoMedica)
                //    {
                //        retornoArquivo.IsPDF = true;
                //        retornoArquivo.FilePDF = File(arquivo, "application/pdf");
                //    }
                //    else
                //    {
                //        var base64 = Convert.ToBase64String(arquivo);
                //        var imgSrc = string.Format("data:{0};base64,{1}", "image/png", base64);

                //        retornoArquivo.ImgSrc = imgSrc;
                //    }

                //}

                //   Response.Headers.Add("Content-Disposition", string.Format("inline; filename=RegistroArquivo.png"));
                //  return File(arquivo, "application/png");


                return imgSrc;


            }
            catch (Exception ex)
            {

            }
            return null;
        }


        public async Task<ActionResult> VisualizarPorId(long id)
        {
            var registroArquivo = _registroArquivoAppService.ObterPorId(id);

            try
            {
                Response.Headers.Add("Content-Disposition", "inline; filename=desctino.pdf");
                return File(registroArquivo.Arquivo, "application/pdf");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return null;

        }

        //public async Task<JsonResult> ListarAtendimento(long id)
        //{
        //    var result = await _solicitacaoExameItemAppService.ListarAtendimento(id);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //[ValidateInput(false)]
        //public void GravarHtml(string html)
        //{
        //    var registroHTML = JsonConvert.DeserializeObject<RegistroHTML>(html);
        //    var teste = Session["testeArquivo"];  //TempData["testeArquivo"];

        //}
    }
}