﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.ProntuarioEletronico;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Assistenciais
{
    public class ProntuariosEletronicosController : SWMANAGERControllerBase
    {
        private readonly IProntuarioEletronicoAppService _prontuarioEletronicoAppService;
        private readonly IOperacaoAppService _operacaoAppService;

        public ProntuariosEletronicosController(
            IProntuarioEletronicoAppService prontuarioEletronicoAppService,
            IOperacaoAppService operacaoAppService
            )
        {
            _prontuarioEletronicoAppService = prontuarioEletronicoAppService;
            _operacaoAppService = operacaoAppService;
        }

        // GET: Mpa/ProntuariosEletronicos
        public ActionResult Index()
        {
            var viewModel = new IndexViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/Index.cshtml",viewModel);
        }

        public async Task<ActionResult> CriarOuEditarProntuarioEletronico(long? id, long? atendimentoId = null)
        {
            CriarOuEditarProntuarioEletronicoViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _prontuarioEletronicoAppService.Obter(id.Value);
                viewModel = new CriarOuEditarProntuarioEletronicoViewModel(output);
            }
            else
            {
                viewModel = new CriarOuEditarProntuarioEletronicoViewModel(new ProntuarioEletronicoDto());
                var pageName = TempData.Peek("ActivePage") as string;
                var operacao = await _operacaoAppService.ObterPorNome(pageName);
                viewModel.OperacaoId = operacao.Id;
                viewModel.Operacao = operacao;
                viewModel.DataAdmissao = DateTime.Now;
                viewModel.AtendimentoId = (long)atendimentoId;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/CriarOuEditarProntuarioEletronicoViewModel.cshtml", viewModel);
        }
    }
}