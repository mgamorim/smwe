﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Web.Mvc.Authorization;
using Microsoft.Reporting.WebForms;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Enumeradores;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Relatorios;
using SW10.SWMANAGER.Web.Controllers;
using SW10.SWMANAGER.Web.Relatorios.Assistenciais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Assistenciais.Relatorios
{
    public class AssistenciaisRelatoriosController : SWMANAGERControllerBase
    {
        private readonly IPrescricaoMedicaAppService _prescricaoMedicaAppService;
        private readonly IPrescricaoItemAppService _prescricaoItemAppService;
        private readonly IPrescricaoItemHoraAppService _prescricaoItemHoraAppService;
        private readonly ISessionAppService _sessionAppService;
        private readonly IUserAppService _userAppService;
        private readonly IEmpresaAppService _empresaAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly ILeitoAppService _leitoAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IPrescricaoItemRespostaAppService _prescricaoItemRespostaAppService;
        private readonly IDivisaoAppService _divisaoAppService;
        private readonly IRepository<RegistroArquivo, long> _registroArquivoRepository;
        private readonly IRegistroArquivoAppService _registroArquivoAppService;

        public AssistenciaisRelatoriosController(
            IPrescricaoMedicaAppService prescricaoMedicaAppService,
            IPrescricaoItemAppService prescricaoItemAppService,
            IPrescricaoItemHoraAppService prescricaoItemHoraAppService,
            ISessionAppService sessionAppService,
            IUserAppService userAppService,
            IEmpresaAppService empresaAppService,
            IPacienteAppService pacienteAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            ILeitoAppService leitoAppService,
            IConvenioAppService convenioAppService,
            IMedicoAppService medicoAppService,
            IPrescricaoItemRespostaAppService prescricaoItemRespostaAppService,
            IDivisaoAppService divisaoAppService,
            IRepository<RegistroArquivo, long> registroArquivoRepository,
            IRegistroArquivoAppService registroArquivoAppService
            )
        {
            _prescricaoMedicaAppService = prescricaoMedicaAppService;
            _prescricaoItemAppService = prescricaoItemAppService;
            _prescricaoItemHoraAppService = prescricaoItemHoraAppService;
            _sessionAppService = sessionAppService;
            _userAppService = userAppService;
            _empresaAppService = empresaAppService;
            _pacienteAppService = pacienteAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _leitoAppService = leitoAppService;
            _convenioAppService = convenioAppService;
            _medicoAppService = medicoAppService;
            _prescricaoItemRespostaAppService = prescricaoItemRespostaAppService;
            _divisaoAppService = divisaoAppService;
            _registroArquivoRepository = registroArquivoRepository;
            _registroArquivoAppService = registroArquivoAppService;
        }

        /// <summary>
        /// Entrada para filtro de visualização do Report de produtos
        /// </summary>
        /// <returns></returns>
        //GET: Mpa/Relatorios/SaldoProduto
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Assistencial_AmbulatorioEmergencia_Medico_Prescricao)]
        public PartialViewResult PrescricaoMedica()
        {
            FiltroModel result = Task.Run(() => CarregarIndex()).Result;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Relatorios/PrescricoesMedicas/IndexPrescricaoMedica.cshtml", result);
        }

        /// <summary>
        /// PartialView que renderiza o relatório com os filtros selecionados no formulário
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Assistencial_AmbulatorioEmergencia_Medico_Prescricao)]
        public async Task<ActionResult> Visualizar(FiltroModel filtro)
        {
            var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            var lista = TempData.Peek("Respostas") as List<RespostaDto>;
            if (lista == null)
            {
                lista = new List<RespostaDto>();
            }
            filtro.Respostas = lista;
            var usuarioEmpresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);
            var usuario = await _userAppService.GetUser();
            var relatorioMedicoPrescricao = filtro.Respostas;
            var loginInformations = await _sessionAppService.GetCurrentLoginInformations();
            FiltroModel _filtro = new FiltroModel();
            if (filtro.Respostas.Count() > 0)
            {
                long medicoId = 0;
                var medico = new MedicoDto();
                if (atendimento.MedicoId.HasValue)
                {
                    medicoId = atendimento.MedicoId.Value;
                }
                else if (usuario.MedicoId.HasValue)
                {
                    medicoId = usuario.MedicoId.Value;
                }
                if (medicoId > 0)
                {
                    medico = await _medicoAppService.Obter(medicoId);
                    _filtro.Medico = medico.NomeCompleto;
                    _filtro.CRM = medico.NumeroConselho.ToString();
                }
                else
                {
                    _filtro.Medico = string.Empty;
                    _filtro.CRM = string.Empty;
                }
                _filtro.Titulo = string.Concat("Prescrição Médica - ", Convert.ToString(atendimento.DataRegistro));
                var empresa = await _empresaAppService.Obter(atendimento.EmpresaId.Value);
                var paciente = await _pacienteAppService.Obter(atendimento.PacienteId.Value);
                var convenio = atendimento.ConvenioId.HasValue ? await _convenioAppService.Obter(atendimento.ConvenioId.Value) : new CriarOuEditarConvenio();
                var unidadeOrganizacional = atendimento.UnidadeOrganizacionalId.HasValue ? await _unidadeOrganizacionalAppService.ObterPorId(atendimento.UnidadeOrganizacionalId.Value) : new UnidadeOrganizacionalDto();
                var leito = atendimento.LeitoId.HasValue ? await _leitoAppService.Obter(atendimento.LeitoId.Value) : new LeitoDto();
                _filtro.NomeHospital = empresa.NomeFantasia;
                _filtro.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                _filtro.DataHora = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                _filtro.Paciente = string.Concat(paciente.Codigo, " - ", paciente.NomeCompleto);
                if (paciente.Nascimento.HasValue)
                {
                    var _idade = DateDifference.GetExtendedDifference(paciente.Nascimento.Value);
                    _filtro.Nascimento = string.Format("{0} ({1}a {2}m {3}d)", paciente.Nascimento.Value.ToString("dd/MM/yyyy"), _idade.Ano, _idade.Mes, _idade.Dia);
                }
                else
                {
                    _filtro.Nascimento = string.Empty;
                }
                _filtro.Convenio = convenio.NomeFantasia.IsNullOrWhiteSpace() ? " " : convenio.NomeFantasia;
                _filtro.Atendimento = atendimento.Codigo;
                _filtro.UnidadeOrganizacional = unidadeOrganizacional.Descricao.IsNullOrWhiteSpace() ? " " : unidadeOrganizacional.Descricao;
                _filtro.Leito = leito.Descricao.IsNullOrWhiteSpace() ? " " : leito.Descricao;
                if (filtro.PrescricaoId.HasValue && filtro.PrescricaoId.Value > 0)
                {
                    var prescricao = await _prescricaoMedicaAppService.Obter(filtro.PrescricaoId.Value);
                    _filtro.Prescricao = prescricao.Codigo;
                }
                else
                {
                    _filtro.Prescricao = string.Empty;
                }

                _filtro.Respostas = filtro.Respostas;
                _filtro.Internacao = atendimento.DataRegistro.ToString("dd/MM/yyyy");
            }

            return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Relatorios/PrescricoesMedicas/PrescricaoMedica.aspx", _filtro);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Assistencial_AmbulatorioEmergencia_Medico_Prescricao)]
        public async Task<ActionResult> VisualizarPDF(FiltroModel filtro)
        {
            try
            {
                var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
                var lista = TempData.Peek("Respostas") as List<RespostaDto>;
                if (lista == null)
                {
                    lista = new List<RespostaDto>();
                }
                if (filtro.Respostas == null)
                {
                    filtro.Respostas = new List<RespostaDto>();
                }
                foreach (var resposta in lista)
                {
                    filtro.Respostas.Add(resposta);
                }
                var usuarioEmpresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);
                var usuario = await _userAppService.GetUser();
                var loginInformations = await _sessionAppService.GetCurrentLoginInformations();
                if (filtro.Respostas.Count() > 0)
                {
                    long medicoId = 0;
                    var medico = new MedicoDto();
                    if (atendimento.MedicoId.HasValue)
                    {
                        medicoId = atendimento.MedicoId.Value;
                    }
                    else if (usuario.MedicoId.HasValue)
                    {
                        medicoId = usuario.MedicoId.Value;
                    }
                    if (medicoId > 0)
                    {
                        medico = await _medicoAppService.Obter(medicoId);
                        filtro.Medico = medico.NomeCompleto;
                        filtro.CRM = medico.NumeroConselho.ToString();
                    }
                    else
                    {
                        filtro.Medico = string.Empty;
                        filtro.CRM = string.Empty;
                    }
                    filtro.Titulo = string.Concat("Prescrição Médica - ", Convert.ToString(atendimento.DataRegistro));
                    var empresa = await _empresaAppService.Obter(atendimento.EmpresaId.Value);
                    var paciente = await _pacienteAppService.Obter(atendimento.PacienteId.Value);
                    var convenio = atendimento.ConvenioId.HasValue ? await _convenioAppService.Obter(atendimento.ConvenioId.Value) : new CriarOuEditarConvenio();
                    var unidadeOrganizacional = atendimento.UnidadeOrganizacionalId.HasValue ? await _unidadeOrganizacionalAppService.ObterPorId(atendimento.UnidadeOrganizacionalId.Value) : new UnidadeOrganizacionalDto();
                    var leito = atendimento.LeitoId.HasValue ? await _leitoAppService.Obter(atendimento.LeitoId.Value) : new LeitoDto();
                    filtro.NomeHospital = empresa.NomeFantasia;
                    filtro.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                    filtro.DataHora = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    filtro.Paciente = string.Concat(paciente.CodigoPaciente, " - ", paciente.NomeCompleto);
                    if (paciente.Nascimento.HasValue)
                    {
                        var _idade = DateDifference.GetExtendedDifference(paciente.Nascimento.Value);
                        filtro.Nascimento = string.Format("{0} ({1}a {2}m {3}d)", paciente.Nascimento.Value.ToString("dd/MM/yyyy"), _idade.Ano, _idade.Mes, _idade.Dia);
                    }
                    else
                    {
                        filtro.Nascimento = string.Empty;
                    }
                    filtro.Convenio = convenio.NomeFantasia.IsNullOrWhiteSpace() ? " " : convenio.NomeFantasia;
                    filtro.Atendimento = atendimento.Codigo;
                    filtro.UnidadeOrganizacional = unidadeOrganizacional.Descricao.IsNullOrWhiteSpace() ? " " : unidadeOrganizacional.Descricao;
                    filtro.Leito = leito.Descricao.IsNullOrWhiteSpace() ? " " : leito.Descricao;



                    filtro.PrescricaoId = filtro.Respostas[0].PrescricaoId;
                    //}


                    if (filtro.PrescricaoId.HasValue && filtro.PrescricaoId.Value > 0)
                    {
                        var prescricao = await _prescricaoMedicaAppService.Obter(filtro.PrescricaoId.Value);
                        filtro.Prescricao = prescricao.Codigo;
                    }
                    else
                    {
                        filtro.Prescricao = string.Empty;
                    }

                    filtro.Internacao = atendimento.DataRegistro.ToString("dd/MM/yyyy");
                }

                // Obtido do ASPX
                ScriptManager scriptManager = new ScriptManager();
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Assistenciais\PrescricoesMedicas\PrescricaoMedica.rdlc");
                var dados = filtro;

                if (dados != null)
                {
                    var a = dados.Atendimento;
                    //parâmetros para o relatório
                    ReportParameter nomeHospital = new ReportParameter("NomeHospital", dados.NomeHospital);
                    ReportParameter titulo = new ReportParameter("Titulo", dados.Titulo);
                    ReportParameter nomeUsuario = new ReportParameter("NomeUsuario", dados.NomeUsuario);
                    ReportParameter dataHora = new ReportParameter("DataHora", dados.DataHora);
                    ReportParameter paciente = new ReportParameter("Paciente", dados.Paciente);
                    ReportParameter atendimentoParam = new ReportParameter("Atendimento", dados.Atendimento);
                    ReportParameter convenio = new ReportParameter("Convenio", dados.Convenio);
                    ReportParameter internacao = new ReportParameter("Internacao", dados.Internacao);
                    ReportParameter leito = new ReportParameter("Leito", dados.Leito);
                    ReportParameter nascimento = new ReportParameter("Nascimento", dados.Nascimento);
                    ReportParameter prescricao = new ReportParameter("Prescricao", dados.Prescricao);
                    ReportParameter medico = new ReportParameter("Medico", dados.Medico);
                    ReportParameter crm = new ReportParameter("CRM", dados.CRM);
                    ReportParameter unidadeOrganizacional = new ReportParameter("unidadeOrganizacional", dados.UnidadeOrganizacional);

                    reportViewer.LocalReport.SetParameters(new ReportParameter[] {
                            nomeHospital,
                            titulo,
                            nomeUsuario,
                            dataHora,
                            paciente,
                            atendimentoParam,
                            convenio,
                            internacao,
                            leito,
                            nascimento,
                            prescricao,
                            medico,
                            crm,
                            unidadeOrganizacional
                        });

                    //fonte de dados para o relatório - datasource
                    Assistencial relDS = new Assistencial();
                    DataTable tabela = ConvertToDataTable(dados.Respostas, relDS.Tables["PrescricaoMedica"]);


                    // Logotipo
                    tabela.Rows[tabela.Rows.Count - 1]["Logotipo"] = atendimento.Empresa.Logotipo;
                    // fim - logotipo

                    ReportDataSource dataSource = new ReportDataSource("PrescricaoMedica", tabela);

                    reportViewer.LocalReport.DataSources.Add(dataSource);

                    scriptManager.RegisterPostBackControl(reportViewer);

                    // Gerando PDF
                    string mimeType = "application/pdf"; //string.Empty;
                    string encoding = string.Empty;
                    string extension = "pdf";

                    string[] streamIds;
                    Warning[] warnings;
                    byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);


                    RegistroArquivo registroArquivo = new RegistroArquivo();

                    registroArquivo.Arquivo = pdfBytes;
                    registroArquivo.RegistroTabelaId = (long)EnumArquivoTabela.PrescricaoMedica;
                    registroArquivo.RegistroId = (long)filtro.PrescricaoId;

                    var id = _registroArquivoRepository.InsertAndGetId(registroArquivo);



                    reportViewer.LocalReport.Refresh();

                    Response.Headers.Add("Content-Disposition", string.Format("inline; filename=prescricao-{0}-{1}.pdf", atendimento.Codigo, DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")));
                    return File(pdfBytes, "application/pdf");

                    // return View("~/Areas/Mpa/Views/Aplicacao/Assistenciais/Relatorios/PrescricoesMedicas/PrescricaoMedica.aspx", _filtro);
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }

            return null;
        }

        private async Task<FiltroModel> CarregarIndex()
        {
            var loginInformations = await _sessionAppService.GetCurrentLoginInformations();

            var userId = AbpSession.UserId;

            FiltroModel result = new FiltroModel();
            var empresas = await _userAppService.GetUserEmpresas(userId.Value);
            result.Empresas = empresas.Items
                .Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.NomeFantasia
                }).ToList();

            //var padrao = new SelectListItem { Text = "Selecione", Value = "0" };
            //result.Empresas.Insert(0, padrao);
            return result;
        }

        public async Task AtualizarTempData(List<PrescricaoItemRespostaDto> list)
        {
            var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
            var _list = new List<RespostaDto>();
            foreach (var item in list)
            {
                var _item = item;
                if (item.Id > 0)
                {
                    _item = Task.Run(() => _prescricaoItemRespostaAppService.Obter(item.Id)).Result;
                    _item.IdGridPrescricaoItemResposta = item.IdGridPrescricaoItemResposta;
                }
                if (_item.DivisaoId.HasValue && _item.Divisao == null)
                {
                    _item.Divisao = await _divisaoAppService.Obter(_item.DivisaoId.Value);
                }
                if (_item.PrescricaoItemId.HasValue)
                {
                    _item.PrescricaoItem = await _prescricaoItemAppService.Obter(_item.PrescricaoItemId.Value);
                }
                var resposta = new RespostaDto();
                resposta.DataInicial = _item.DataInicial.HasValue ? string.Concat(_item.DataInicial.Value.Day.ToString("00"), "/", _item.DataInicial.Value.Month.ToString("00"), "/", _item.DataInicial.Value.Year.ToString("0000")) : string.Empty;
                resposta.Divisao = _item.Divisao != null ? _item.Divisao.Descricao : string.Empty;
                resposta.DivisaoId = _item.DivisaoId.ToString();
                resposta.FormaAplicacao = _item.FormaAplicacao != null ? _item.FormaAplicacao.Descricao : string.Empty;
                resposta.Frequencia = _item.Frequencia != null ? _item.Frequencia.Descricao : string.Empty;
                resposta.Observacao = _item.Observacao;
                resposta.PrescricaoItem = _item.PrescricaoItem != null ? _item.PrescricaoItem.Descricao : string.Empty;
                resposta.PrescricaoItemId = _item.PrescricaoItemId.ToString();
                resposta.Quantidade = _item.Quantidade.ToString();
                resposta.Unidade = _item.Unidade != null ? _item.Unidade.Descricao : string.Empty;
                resposta.VelocidadeInfusao = _item.VelocidadeInfusao != null ? _item.VelocidadeInfusao.Descricao : string.Empty;
                resposta.Prescricao = _item.PrescricaoMedica != null ? _item.PrescricaoMedica.Codigo : string.Empty;
                resposta.UnidadeOrganizacional = _item.UnidadeOrganizacional != null ? _item.UnidadeOrganizacional.Descricao : string.Empty;
                resposta.DataCriacao = _item.CreationTime.ToString("dd/MM/yyyy HH:mm:ss");

                resposta.PrescricaoId = (long)item.PrescricaoMedicaId;

                if (_item.FrequenciaId.HasValue)
                {
                    var horarios = await _prescricaoItemHoraAppService.ListarPorItem(_item.Id);
                    if (horarios != null && horarios.Items.Count() > 0)
                    {
                        var _d0 = horarios.Items.Where(m => m.DiaMedicamento == 0).Select(s => s.Hora).ToList();
                        var d0 = string.Empty;
                        foreach (var hora in _d0)
                        {
                            d0 += hora + ":00 ";
                        }
                        if (d0.Length > 1)
                        {
                            d0 = d0.Substring(0, d0.Length - 1);
                            resposta.D0 = d0;
                        }
                        var _d1 = horarios.Items.Where(m => m.DiaMedicamento == 1).Select(s => s.Hora).ToList();
                        var d1 = string.Empty;
                        foreach (var hora in _d1)
                        {
                            d1 += hora + ":00 ";
                        }
                        if (d1.Length > 0)
                        {
                            d1 = d1.Substring(0, d1.Length - 1);
                            resposta.D1 = d1;
                        }
                    }
                }
                _list.Add(resposta);
            }
            TempData["Respostas"] = _list;
        }

        private DataTable ConvertToDataTable<T>(IList<T> data, DataTable table)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            if (data != null)
            {
                foreach (T item in data)
                {
                    try
                    {
                        DataRow row = table.NewRow();
                        foreach (PropertyDescriptor prop in properties)
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        table.Rows.Add(row);
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
            }

            return table;
        }

        public async Task<ActionResult> VisualizarPrescricao(long prescricaoId)
        {
            var registro = _registroArquivoAppService.ObterPorRegistro(prescricaoId, (long)EnumArquivoTabela.PrescricaoMedica);

            byte[] arquivo = null;

            if (registro != null)
            {
                arquivo = registro.Arquivo;
            }

            Response.Headers.Add("Content-Disposition", string.Format("inline; filename=prescricao-{0}-{1}.pdf", "Prescrição", DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss")));
            return File(arquivo, "application/pdf");
        }

        public async Task<ActionResult> GerarArquivoPrescricao(FiltroModel filtro)
        {
            try
            {
                var atendimento = TempData.Peek("Atendimento") as AtendimentoDto;
                var lista = TempData.Peek("Respostas") as List<RespostaDto>;
                if (lista == null)
                {
                    lista = new List<RespostaDto>();
                }
                if (filtro.Respostas == null)
                {
                    filtro.Respostas = new List<RespostaDto>();
                }
                foreach (var resposta in lista)
                {
                    filtro.Respostas.Add(resposta);
                }
                var usuarioEmpresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);
                var usuario = await _userAppService.GetUser();
                var loginInformations = await _sessionAppService.GetCurrentLoginInformations();
                if (filtro.Respostas.Count() > 0)
                {
                    long medicoId = 0;
                    var medico = new MedicoDto();
                    if (atendimento.MedicoId.HasValue)
                    {
                        medicoId = atendimento.MedicoId.Value;
                    }
                    else if (usuario.MedicoId.HasValue)
                    {
                        medicoId = usuario.MedicoId.Value;
                    }
                    if (medicoId > 0)
                    {
                        medico = await _medicoAppService.Obter(medicoId);
                        filtro.Medico = medico.NomeCompleto;
                        filtro.CRM = medico.NumeroConselho.ToString();
                    }
                    else
                    {
                        filtro.Medico = string.Empty;
                        filtro.CRM = string.Empty;
                    }
                    filtro.Titulo = string.Concat("Prescrição Médica - ", Convert.ToString(atendimento.DataRegistro));
                    var empresa = await _empresaAppService.Obter(atendimento.EmpresaId.Value);
                    var paciente = await _pacienteAppService.Obter(atendimento.PacienteId.Value);
                    var convenio = atendimento.ConvenioId.HasValue ? await _convenioAppService.Obter(atendimento.ConvenioId.Value) : new CriarOuEditarConvenio();
                    var unidadeOrganizacional = atendimento.UnidadeOrganizacionalId.HasValue ? await _unidadeOrganizacionalAppService.ObterPorId(atendimento.UnidadeOrganizacionalId.Value) : new UnidadeOrganizacionalDto();
                    var leito = atendimento.LeitoId.HasValue ? await _leitoAppService.Obter(atendimento.LeitoId.Value) : new LeitoDto();
                    filtro.NomeHospital = empresa.NomeFantasia;
                    filtro.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                    filtro.DataHora = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                    filtro.Paciente = string.Concat(paciente.CodigoPaciente, " - ", paciente.NomeCompleto);
                    if (paciente.Nascimento.HasValue)
                    {
                        var _idade = DateDifference.GetExtendedDifference(paciente.Nascimento.Value);
                        filtro.Nascimento = string.Format("{0} ({1}a {2}m {3}d)", paciente.Nascimento.Value.ToString("dd/MM/yyyy"), _idade.Ano, _idade.Mes, _idade.Dia);
                    }
                    else
                    {
                        filtro.Nascimento = string.Empty;
                    }
                    filtro.Convenio = convenio.NomeFantasia.IsNullOrWhiteSpace() ? " " : convenio.NomeFantasia;
                    filtro.Atendimento = atendimento.Codigo;
                    filtro.UnidadeOrganizacional = unidadeOrganizacional.Descricao.IsNullOrWhiteSpace() ? " " : unidadeOrganizacional.Descricao;
                    filtro.Leito = leito.Descricao.IsNullOrWhiteSpace() ? " " : leito.Descricao;



                    filtro.PrescricaoId = filtro.Respostas[0].PrescricaoId;
                    //}


                    if (filtro.PrescricaoId.HasValue && filtro.PrescricaoId.Value > 0)
                    {
                        var prescricao = await _prescricaoMedicaAppService.Obter(filtro.PrescricaoId.Value);
                        filtro.Prescricao = prescricao.Codigo;
                    }
                    else
                    {
                        filtro.Prescricao = string.Empty;
                    }

                    filtro.Internacao = atendimento.DataRegistro.ToString("dd/MM/yyyy");
                }

                // Obtido do ASPX
                ScriptManager scriptManager = new ScriptManager();
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Assistenciais\PrescricoesMedicas\PrescricaoMedica.rdlc");
                var dados = filtro;

                if (dados != null)
                {
                    var a = dados.Atendimento;
                    //parâmetros para o relatório
                    ReportParameter nomeHospital = new ReportParameter("NomeHospital", dados.NomeHospital);
                    ReportParameter titulo = new ReportParameter("Titulo", dados.Titulo);
                    ReportParameter nomeUsuario = new ReportParameter("NomeUsuario", dados.NomeUsuario);
                    ReportParameter dataHora = new ReportParameter("DataHora", dados.DataHora);
                    ReportParameter paciente = new ReportParameter("Paciente", dados.Paciente);
                    ReportParameter atendimentoParam = new ReportParameter("Atendimento", dados.Atendimento);
                    ReportParameter convenio = new ReportParameter("Convenio", dados.Convenio);
                    ReportParameter internacao = new ReportParameter("Internacao", dados.Internacao);
                    ReportParameter leito = new ReportParameter("Leito", dados.Leito);
                    ReportParameter nascimento = new ReportParameter("Nascimento", dados.Nascimento);
                    ReportParameter prescricao = new ReportParameter("Prescricao", dados.Prescricao);
                    ReportParameter medico = new ReportParameter("Medico", dados.Medico);
                    ReportParameter crm = new ReportParameter("CRM", dados.CRM);
                    ReportParameter unidadeOrganizacional = new ReportParameter("unidadeOrganizacional", dados.UnidadeOrganizacional);

                    reportViewer.LocalReport.SetParameters(new ReportParameter[] {
                            nomeHospital,
                            titulo,
                            nomeUsuario,
                            dataHora,
                            paciente,
                            atendimentoParam,
                            convenio,
                            internacao,
                            leito,
                            nascimento,
                            prescricao,
                            medico,
                            crm,
                            unidadeOrganizacional
                        });

                    //fonte de dados para o relatório - datasource
                    Assistencial relDS = new Assistencial();
                    DataTable tabela = ConvertToDataTable(dados.Respostas, relDS.Tables["PrescricaoMedica"]);


                    // Logotipo
                    tabela.Rows[tabela.Rows.Count - 1]["Logotipo"] = atendimento.Empresa.Logotipo;
                    // fim - logotipo

                    ReportDataSource dataSource = new ReportDataSource("PrescricaoMedica", tabela);

                    reportViewer.LocalReport.DataSources.Add(dataSource);

                    scriptManager.RegisterPostBackControl(reportViewer);

                    // Gerando PDF
                    string mimeType = "application/pdf"; //string.Empty;
                    string encoding = string.Empty;
                    string extension = "pdf";

                    string[] streamIds;
                    Warning[] warnings;
                    byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);


                    RegistroArquivo registroArquivo = new RegistroArquivo();

                    registroArquivo.Arquivo = pdfBytes;
                    registroArquivo.RegistroTabelaId = (long)EnumArquivoTabela.PrescricaoMedica;
                    registroArquivo.RegistroId = (long)filtro.PrescricaoId;
                    registroArquivo.AtendimentoId = atendimento.Id;

                    var id = _registroArquivoRepository.InsertAndGetId(registroArquivo);
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }

            return null;
        }
    }
}