﻿using Abp.Domain.Repositories;
using Abp.Web.Mvc.Authorization;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Exames;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Home;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Laboratorios.Resultados;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros.Laboratorios
{
    public class ResultadosController : SWMANAGERControllerBase
    {
        private readonly IResultadoAppService _resultadoAppService;
        private readonly IResultadoExameAppService _resultadoExameAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IUserAppService _userAppService;
      //  private readonly ISolicitacaoExameItemAppService _solicitacaoExameItemAppService;
      //  private readonly IExameAppService _exameAppService;
        private readonly IRepository<SolicitacaoExameItem, long> _solicitacaoExameItemRepository;


        public ResultadosController(
            IResultadoAppService resultadoAppService,
            IResultadoExameAppService resultadoExameAppService,
            IAtendimentoAppService atendimentoAppService,
            IUserAppService userAppService,
         //   ISolicitacaoExameItemAppService solicitacaoExameItemAppService,
         //   IExameAppService exameAppService,
            IRepository<SolicitacaoExameItem, long> solicitacaoExameItemRepository
            )
        {
            _resultadoAppService = resultadoAppService;
            _resultadoExameAppService = resultadoExameAppService;
            _atendimentoAppService = atendimentoAppService;
            _userAppService = userAppService;
           // _solicitacaoExameItemAppService = solicitacaoExameItemAppService;
          //  _exameAppService = exameAppService;
            _solicitacaoExameItemRepository = solicitacaoExameItemRepository;
        }

        public async Task<ActionResult> Index()
        {
            var userId = AbpSession.UserId.Value;


            //kernel.Bind<IUserAppService>().To<UserAppService>();

            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var empresa = TempData.Peek("Empresa") as EmpresaDto;

            var model = new AtendimentoDto();
            var viewModel = new AssistenciaisViewModel(model);
            if (empresa == null || (empresa != null && empresa.Id == 0))
            {
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
            }
            else
            {
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", empresa.Id);
                viewModel.EmpresaId = empresa.Id;
                viewModel.Empresa = empresa;
            }
            viewModel.IsAmbulatorioEmergencia = true;
            viewModel.IsInternacao = false;
            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Laboratorios/Resultados/Index.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Laboratorio_Cadastros_Resultado_Create, AppPermissions.Pages_Tenant_Laboratorio_Cadastros_Resultado_Edit)]
        public async Task<ActionResult> CriarOuEditarModal(long? id, long? atendimentoId)
        {
            CriarOuEditarResultadoModalViewModel viewModel;


            var ambulatorioInternacao = new List<GenericoIdNome>();

            ambulatorioInternacao.Add(new GenericoIdNome { Id = 1, Nome = "Ambulatório" });
            ambulatorioInternacao.Add(new GenericoIdNome { Id = 2, Nome = "Internação" });



            if (id.HasValue)
            {
                var output = await _resultadoAppService.Obter(id.Value);
                viewModel = new CriarOuEditarResultadoModalViewModel(output);

                var list = await _resultadoExameAppService.ListarPorResultado(id.Value);
                viewModel.ResultadosExamesList = JsonConvert.SerializeObject(list.Items.ToList());
                viewModel.IsSolicitacao = true;


                viewModel.AmbulatorioInternacao = output.Atendimento.IsAmbulatorioEmergencia ? 1 : 2;

                viewModel.ListaAmbulatorioInternacao = new SelectList(ambulatorioInternacao, "Id", "Nome", viewModel.AmbulatorioInternacao);

            }
            else
            {
                viewModel = new CriarOuEditarResultadoModalViewModel(new ResultadoDto());
                if (atendimentoId.HasValue)
                {
                    viewModel.AtendimentoId = atendimentoId.Value;
                }
                viewModel.ResultadosExamesList = JsonConvert.SerializeObject(new List<ResultadoExameIndexCrudDto>());
                viewModel.ListaAmbulatorioInternacao = new SelectList(ambulatorioInternacao, "Id", "Nome");


            }
            // var atendimento = await _atendimentoAppService.Obter(atendimentoId.Value);
            // viewModel.Atendimento = atendimento;
            viewModel.PacienteNome = viewModel.Atendimento?.Paciente?.NomeCompleto;

         

            //if(viewModel.Atendimento?.Paciente?.Nascimento!=null)
            //{
            //    var idade = DateDifference.GetExtendedDifference((DateTime)viewModel.Atendimento.Paciente.Nascimento);
            //    viewModel.IsRn = (idade.Ano == 0 && idade.Mes == 0 && idade.Dia <= 30);
            //}

            TempData["AtendimentoLab"] = viewModel.Atendimento;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Laboratorios/Resultados/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<JsonResult> AutoComplete(string term)
        {
            var query = await _resultadoAppService.ListarAutoComplete(term);
            var result = query.Items.Select(m => new { m.Id, m.Nome }).ToList();
            return Json(result.ToArray(), JsonRequestBehavior.AllowGet);
            //  return Json(query.Items.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> CriarColetaDeExamesSolicitado(string listIds)
        {
            var ids = JsonConvert.DeserializeObject<List<long>>(listIds);


            var ambulatorioInternacao = new List<GenericoIdNome>();

            ambulatorioInternacao.Add(new GenericoIdNome { Id = 1, Nome = "Ambulatório" });
            ambulatorioInternacao.Add(new GenericoIdNome { Id = 2, Nome = "Internação" });


            CriarOuEditarResultadoModalViewModel viewModel = null;

            try
            {
                if (ids != null)
                {
                    var solicitacoesExamesEntidades = _solicitacaoExameItemRepository.GetAll()
                                                                            .Include(i=> i.Solicitacao)
                                                                            .Include(i => i.Solicitacao.MedicoSolicitante)
                                                                            .Include(i => i.Solicitacao.MedicoSolicitante.SisPessoa)
                                                                            .Include(i => i.Solicitacao.Atendimento)
                                                                            .Include(i => i.Solicitacao.Atendimento.Convenio)
                                                                            .Include(i => i.Solicitacao.Atendimento.Convenio.SisPessoa)
                                                                            .Include(i => i.Solicitacao.Atendimento.Leito)
                                                                            .Include(i => i.Solicitacao.Atendimento.Paciente)
                                                                            .Include(i => i.Solicitacao.Atendimento.Paciente.SisPessoa)
                                                                            .Include(i => i.Solicitacao.Atendimento.Medico)
                                                                            .Include(i => i.Solicitacao.Atendimento.Medico.SisPessoa)
                                                                            .Include(i => i.FaturamentoItem)
                                                                            .Include(i => i.FaturamentoItem.Material)
                                                                            .Where(m => ids.Any(a => a == m.Id))
                                                                            .ToList();




                    var solicitacoesExames = SolicitacaoExameItemDto.Mapear(solicitacoesExamesEntidades);


                   // var solicitacoesExames = await _solicitacaoExameItemAppService.ObterPorLista(ids);

                    if (solicitacoesExames != null && solicitacoesExames.Count > 0)
                    {
                        var solicitacao = solicitacoesExames[0];

                        var resultadoDto = new ResultadoDto();

                        resultadoDto.DataColeta = DateTime.Now;
                        resultadoDto.Atendimento = solicitacao.Solicitacao.Atendimento;
                        resultadoDto.AtendimentoId = solicitacao.Solicitacao.Atendimento.Id;
                        resultadoDto.PacienteNome = solicitacao.Solicitacao.Atendimento.Paciente.NomeCompleto;
                        if (solicitacao.Solicitacao.MedicoSolicitante != null)
                        {
                            resultadoDto.MedicoSolicitante = new MedicoDto
                            {
                                Id = solicitacao.Solicitacao.MedicoSolicitante.Id
                                                                           ,
                                NomeCompleto = solicitacao.Solicitacao.Atendimento.Medico.SisPessoa.NomeCompleto,
                                SisPessoa = solicitacao.Solicitacao.MedicoSolicitante.SisPessoa
                            };

                            solicitacao.Solicitacao.MedicoSolicitanteId = solicitacao.Solicitacao.MedicoSolicitante.Id;

                            resultadoDto.NomeMedicoSolicitante = solicitacao.Solicitacao.MedicoSolicitante.SisPessoa.NomeCompleto;
                            resultadoDto.CRMSolicitante = solicitacao.Solicitacao.MedicoSolicitante.NumeroConselho.ToString();
                        }

                        resultadoDto.LeitoAtual = solicitacao.Solicitacao.Atendimento.Leito;//.MapTo<LeitoDto>();
                        resultadoDto.LeitoAtualId = solicitacao.Solicitacao.Atendimento.LeitoId;

                        if (solicitacao.Solicitacao.Atendimento.Paciente.Nascimento != null)
                        {
                            var idade = DateDifference.GetExtendedDifference((DateTime)solicitacao.Solicitacao.Atendimento.Paciente.Nascimento);

                            resultadoDto.IsRn = (idade.Ano == 0 && idade.Mes == 0 && idade.Dia <= 30);
                        }


                        if (solicitacao.Solicitacao.Atendimento.Convenio != null)
                        {
                            resultadoDto.ConvenioId =  solicitacao.Solicitacao.Atendimento.ConvenioId;
                            resultadoDto.Convenio = solicitacao.Solicitacao.Atendimento.Convenio;
                        }
  



                        List<ResultadoExameIndexCrudDto> exames = new List<ResultadoExameIndexCrudDto>();

                        long idGrid = 0;
                        foreach (var item in solicitacoesExames)
                        {
                            ResultadoExameIndexCrudDto resultadoExame = new ResultadoExameIndexCrudDto();

                            //  var exame = new ResultadoExameIndexCrudDto();   //await _exameAppService.ObterPorItemFaturamento((long)item.FaturamentoItemId);
                            // if (exame != null)
                            // {

                            resultadoExame.SolicitacaoItemId = item.Id;
                            resultadoExame.FaturamentoItemId = item.FaturamentoItem?.Id;
                            resultadoExame.Exame = item.FaturamentoItem?.Descricao;
                            resultadoExame.MaterialId = item.FaturamentoItem?.MaterialId;
                            resultadoExame.Material = item.FaturamentoItem?.Material?.Descricao;
                            resultadoExame.Quantidade = item.FaturamentoItem.QtdFatura;
                            resultadoExame.IdGridResultadoExame = ++idGrid;
                            // }
                            exames.Add(resultadoExame);
                        }

                        viewModel = new CriarOuEditarResultadoModalViewModel(resultadoDto);

                        viewModel.Atendimento = solicitacao.Solicitacao.Atendimento;
                        TempData["AtendimentoLab"] = viewModel.Atendimento;


                        viewModel.ResultadosExamesList = JsonConvert.SerializeObject(exames);
                        viewModel.IsSolicitacao = true;

                        viewModel.AmbulatorioInternacao = resultadoDto.Atendimento.IsAmbulatorioEmergencia ? 1 : 2;

                        viewModel.ListaAmbulatorioInternacao = new SelectList(ambulatorioInternacao, "Id", "Nome", viewModel.AmbulatorioInternacao);
                    }
                }
                else
                {
                    viewModel = new CriarOuEditarResultadoModalViewModel(new ResultadoDto());
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Laboratorios/Resultados/_CriarOuEditarModal.cshtml", viewModel);
        }


    }
}