﻿using Abp.Threading;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Sexos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosCodigosMedicamento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosPortaria;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosPalavrasChave;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosAcoesTerapeutica;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposUnidade;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using Abp.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEmpresa;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEmpresa.Dto;
using SW10.SWMANAGER.Authorization.Users;
using Abp.Runtime.Session;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros
{
    //public class ProdutosController : SWMANAGERControllerBase //Controller 
    public class ProdutosController : Controller
    {
        private readonly IProdutoPortariaAppService _produtoPortariaAppService;
        private readonly IProdutoLaboratorioAppService _produtoLaboratorioAppService;
        private readonly IProdutoPalavraChaveAppService _produtoPalavraChaveAppService;
        private readonly IProdutoAcaoTerapeuticaAppService _produtoAcaoTerapeuticaAppService;
        private readonly IEstoqueAppService _estoqueAppService;

        private readonly IProdutoListaSubstituicaoAppService _produtoListaSubstituicaoAppService;
        private readonly IProdutoRelacaoPortariaAppService _produtoRelacaoPortariaAppService;
        private readonly IProdutoRelacaoLaboratorioAppService _produtoRelacaoLaboratorioAppService;
        private readonly IProdutoRelacaoPalavraChaveAppService _produtoRelacaoPalavraChaveAppService;
        private readonly IProdutoRelacaoAcaoTerapeuticaAppService _produtoRelacaoAcaoTerapeuticaAppService;
        private readonly IProdutoEstoqueAppService _produtoEstoqueAppService;

        private readonly IProdutoAppService _produtoAppService;
        private readonly ISexoAppService _sexoAppService;
        private readonly IProdutoUnidadeAppService _produtoUnidadeAppService;
        private readonly IProdutoUnidadeTipoAppService _produtoUnidadeTipoAppService;
        private readonly IAbpSession AbpSession;
        private readonly IUserAppService _userAppService;
        private readonly IProdutoEmpresaAppService _produtoEmpresaAppService;
        private readonly IGrupoAppService _produtoGrupoAppService;
        private readonly IGrupoClasseAppService _produtoClasseAppService;
        private readonly IGrupoSubClasseAppService _produtoSubClasseAppService;
        private readonly IProdutoCodigoMedicamentoAppService _produtoCodigoMedicamentoAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly ITipoUnidadeAppService _tipoUnidadeAppService;

        public ProdutosController(
                IProdutoPortariaAppService produtoPortariaAppService,
                IProdutoLaboratorioAppService produtoLaboratorioAppService,
                IProdutoPalavraChaveAppService produtoPalavraChaveAppService,
                IProdutoAcaoTerapeuticaAppService produtoAcaoTerapeuticaAppService,
                IEstoqueAppService estoqueAppService,

                IProdutoListaSubstituicaoAppService produtoListaSubstituicaoAppService,
                IProdutoRelacaoPortariaAppService produtoRelacaoPortariaAppService,
                IProdutoRelacaoLaboratorioAppService produtoRelacaoLaboratorioAppService,
                IProdutoRelacaoPalavraChaveAppService produtoRelacaoPalavraChaveAppService,
                IProdutoRelacaoAcaoTerapeuticaAppService produtoRelacaoAcaoTerapeuticaAppService,
                IProdutoEstoqueAppService produtoEstoqueAppService,

                IProdutoAppService produtoAppService,
                ISexoAppService sexoAppService,
                IProdutoUnidadeAppService produtoUnidadeAppService,
                IProdutoUnidadeTipoAppService produtoUnidadeTipoAppService,
                IAbpSession abpSession,
                IUserAppService userAppService,
                IProdutoEmpresaAppService produtoEmpresaAppService,
                IGrupoAppService produtoGrupoAppService,
                IGrupoClasseAppService produtoClasseAppService,
                IGrupoSubClasseAppService produtoSubClasseAppService,
                IProdutoCodigoMedicamentoAppService produtoCodigoMedicamentoAppService,
                IUnidadeAppService unidadeAppService,
                ITipoUnidadeAppService tipoUnidadeAppService
            )
        {
            _produtoListaSubstituicaoAppService = produtoListaSubstituicaoAppService;
            _produtoPortariaAppService = produtoPortariaAppService;
            _produtoLaboratorioAppService = produtoLaboratorioAppService;
            _produtoPalavraChaveAppService = produtoPalavraChaveAppService;
            _produtoAcaoTerapeuticaAppService = produtoAcaoTerapeuticaAppService;
            _estoqueAppService = estoqueAppService;

            _produtoRelacaoPortariaAppService = produtoRelacaoPortariaAppService;
            _produtoRelacaoLaboratorioAppService = produtoRelacaoLaboratorioAppService;
            _produtoRelacaoPalavraChaveAppService = produtoRelacaoPalavraChaveAppService;
            _produtoRelacaoAcaoTerapeuticaAppService = produtoRelacaoAcaoTerapeuticaAppService;
            _produtoEstoqueAppService = produtoEstoqueAppService;

            _produtoAppService = produtoAppService;
            _sexoAppService = sexoAppService;
            _produtoUnidadeAppService = produtoUnidadeAppService;
            _produtoUnidadeTipoAppService = produtoUnidadeTipoAppService;
            _userAppService = userAppService;
            AbpSession = abpSession;
            _produtoEmpresaAppService = produtoEmpresaAppService;
            _produtoGrupoAppService = produtoGrupoAppService;
            _produtoClasseAppService = produtoClasseAppService;
            _produtoSubClasseAppService = produtoSubClasseAppService;
            _produtoCodigoMedicamentoAppService = produtoCodigoMedicamentoAppService;
            // _produtoRelacaoEstoqueAppService = produtoRelacaoEstoqueAppService;
            _unidadeAppService = unidadeAppService;
            _tipoUnidadeAppService = tipoUnidadeAppService;
        }

        public async Task<ActionResult> Index()
        {
            var model = new ProdutosViewModel();

            var grupos = await _produtoGrupoAppService.ListarTodos();
            var classes = new ListResultDto<GrupoClasseDto>();
            var subClasses = new ListResultDto<GrupoSubClasseDto>();

            #region filtroPrincipal
            List<GenericoIdNome> filtroPrincipal = new List<GenericoIdNome>();
            var opcaoPrincipal1 = new GenericoIdNome();
            opcaoPrincipal1.Id = 1;
            opcaoPrincipal1.Nome = "Sim";
            filtroPrincipal.Add(opcaoPrincipal1);

            var opcaoPrincipal2 = new GenericoIdNome();
            opcaoPrincipal2.Id = 2;
            opcaoPrincipal2.Nome = "Não";
            filtroPrincipal.Add(opcaoPrincipal2);
            #endregion

            #region filtroStatus
            List<GenericoIdNome> filtroStatus = new List<GenericoIdNome>();
            var opcaoStatus1 = new GenericoIdNome();
            opcaoStatus1.Id = 1;
            opcaoStatus1.Nome = "Ativo";
            filtroStatus.Add(opcaoStatus1);

            var opcaoStatus2 = new GenericoIdNome();
            opcaoStatus2.Id = 2;
            opcaoStatus2.Nome = "Inativo";
            filtroStatus.Add(opcaoStatus2);
            #endregion

            model.Grupos = new SelectList(grupos.Items, "Id", "Descricao");
            model.Classes = new SelectList(classes.Items, "Id", "Descricao");
            model.SubClasses = new SelectList(subClasses.Items, "Id", "Descricao");

            model.FiltroPrincipais = new SelectList(filtroPrincipal, "Id", "Nome");
            model.FiltroStatus = new SelectList(filtroStatus, "Id", "Nome");

            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/Index.cshtml", model);
        }

        /// <summary>
        /// Prepara E exibe cadastro para Novo/Ediçao de Produto
        /// </summary>
        [AcceptVerbs("GET", "POST", "PUT")]
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Suprimentos_Produto_Create, AppPermissions.Pages_Tenant_Cadastros_Suprimentos_Produto_Edit)]
        public async Task<ActionResult> CriarOuEditarModal(long? id)
        {
            CriarOuEditarProdutoModalViewModel viewModel;

            var generos = await _sexoAppService.ListarTodos();
            var unidadesreferenciais = await _unidadeAppService.ListarUnidadesReferenciais();
            var unidadesgerenciais = new ListResultDto<UnidadeDto>();
            var produtosprincipais = await _produtoAppService.ListarProdutosMestre();
            var grupos = await _produtoGrupoAppService.ListarTodos();
            var classes = new ListResultDto<GrupoClasseDto>();
            var subClasses = new ListResultDto<GrupoSubClasseDto>();
            var codigoMedicamentosAppService = await _produtoCodigoMedicamentoAppService.ListarTodos();
            var dcbs = await _produtoAppService.ListarDCBs();

            #region etiquetas
            List<EtiquetaDto> etiquetas = new List<EtiquetaDto>();
            var lista = new EtiquetaDto();
            lista.Id = 1;
            lista.Descricao = "Sistema";
            etiquetas.Add(lista);

            var lista2 = new EtiquetaDto();
            lista2.Id = 2;
            lista2.Descricao = "Fornecedor";
            etiquetas.Add(lista2);

            var lista3 = new EtiquetaDto();
            lista3.Id = 3;
            lista3.Descricao = "Não utilizar";
            etiquetas.Add(lista3);

            //var Etiquetas = await _unidadeAppService.ListarTodos();  pendentes implementacoes

            #endregion
            //-----------------------------------------------------------------------------------------------------

            if (id.HasValue)
            {
                #region EdicaoProduto

                var output = await _produtoAppService.Obter((long)id);
                viewModel = new CriarOuEditarProdutoModalViewModel(output);

                var produtoAtual = produtosprincipais.Items.FirstOrDefault(p => p.Id == id);
                List<GenericoIdNome> prods = new List<GenericoIdNome>();
                prods.Add(produtoAtual);
                var produtosMestresEdicao = produtosprincipais.Items.Except(prods);

                //viewModel.EtiquetaId = 2;
                viewModel.EstoqueLocalizacaoId = 1;
                viewModel.DCBs = new SelectList(dcbs.Items, "Id", "Nome", output.DCBId);
                viewModel.Etiquetas = new SelectList(etiquetas, "Id", "Descricao", output.EtiquetaId);
                viewModel.Generos = new SelectList(generos.Items, "Id", "Descricao", output.Genero);
                viewModel.ProdutosPrincipais = new SelectList(produtosMestresEdicao, "Id", "Nome", output.ProdutoPrincipalId);

                viewModel.Grupos = new SelectList(grupos.Items, "Id", "Descricao", output.GrupoId);
                if (viewModel.GrupoId != null)
                {
                    classes = await _produtoClasseAppService.ListarPorGrupo(viewModel.GrupoId);
                    viewModel.Classes = new SelectList(classes.Items, "Id", "Descricao", output.GrupoClasseId);
                }

                if (viewModel.GrupoClasseId != null)
                {
                    subClasses = await _produtoSubClasseAppService.ListarPorClasse((long)viewModel.GrupoClasseId);
                    viewModel.SubClasses = new SelectList(subClasses.Items, "Id", "Descricao", output.GrupoSubClasseId);
                }
                else {
                    viewModel.SubClasses = new SelectList(subClasses.Items, "Id", "Descricao");
                };

                viewModel.UnidadeReferencial = await _produtoAppService.ObterUnidadePorTipo(viewModel.Id, 1);
                if (viewModel.UnidadeReferencial != null)
                {
                    viewModel.UnidadeReferencialId = viewModel.UnidadeReferencial.Id;
                }
                viewModel.UnidadesReferenciais = new SelectList(unidadesreferenciais.Items, "Id", "Descricao", viewModel.UnidadeReferencialId);
                //viewModel.UnidadesReferenciais = new SelectList(unidadesreferenciais.Items, "Sigla", "Descricao", viewModel.UnidadeReferencialId);

                viewModel.UnidadeGerencial = await _produtoAppService.ObterUnidadePorTipo(viewModel.Id, 2);
                viewModel.UnidadeGerencialId = viewModel.UnidadeGerencial.Id;
                unidadesgerenciais = await _unidadeAppService.ListarPorReferencial(viewModel.UnidadeReferencialId, true);
                viewModel.UnidadesGerenciais = new SelectList(unidadesgerenciais.Items, "Id", "Descricao", viewModel.UnidadeGerencialId);

                #endregion EdicaoProduto
            }
            else
            {
                viewModel = new CriarOuEditarProdutoModalViewModel(new ProdutoDto());
                viewModel.EstoqueLocalizacaoId = 1;
                viewModel.DCBs = new SelectList(dcbs.Items, "Id", "Nome");
                viewModel.Generos = new SelectList(generos.Items, "Id", "Descricao");
                viewModel.Etiquetas = new SelectList(etiquetas, "Id", "Descricao");
                viewModel.ProdutosPrincipais = new SelectList(produtosprincipais.Items, "Id", "Nome");
                viewModel.Grupos = new SelectList(grupos.Items, "Id", "Descricao");
                viewModel.Classes = new SelectList(classes.Items, "Id", "Descricao");
                viewModel.SubClasses = new SelectList(subClasses.Items, "Id", "Descricao");
                viewModel.CodigosMedicamentos = new SelectList(codigoMedicamentosAppService.Items, "Id", "Descricao");
                //viewModel.Unidades = new SelectList(Unidades.Items, "Id", "Descricao");
                viewModel.UnidadesReferenciais = new SelectList(unidadesreferenciais.Items, "Id", "Descricao");
                unidadesgerenciais = await _unidadeAppService.ListarPorReferencial(viewModel.Id, true);
                viewModel.UnidadesGerenciais = new SelectList(unidadesgerenciais.Items.Select(m => new { UnidadeReferencialId = m.Id, Descricao = m.Descricao }), "UnidadeReferencialId", "Descricao");
            }

            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarModal.cshtml", viewModel);
        }

        /// <summary>
        /// Retona Json com Id e Descricao de Classes para um determinado Grupo
        /// </summary>
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult GetClasses(long id)
        {
            try
            {
                if (id > 0)
                {
                    var classes = AsyncHelper.RunSync(() => _produtoClasseAppService.ListarPorGrupo(id));

                    var lista = classes.Items.ToList().Select(
                        c => new { DisplayText = c.Id, Value = c.Descricao });

                    return Json(new { Result = "OK", Options = lista }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = "OK", Options = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (System.Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retona Json com Id e Descricao de SubClasses para uma determinada Classe
        /// </summary>
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult GetSubClasses(long id)
        {
            try
            {
                if (id > 0)
                {
                    var subClasses = AsyncHelper.RunSync(() => _produtoSubClasseAppService.ListarPorClasse(id));

                    var lista = subClasses.Items.ToList().Select(
                        c => new { DisplayText = c.Id, Value = c.Descricao });

                    return Json(new { Result = "OK", Options = lista }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = "OK", Options = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (System.Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retorna Json com Id e Descricao de Unidades. Parametro Id = id da classe referencial
        /// </summary>
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult GetUnidadesPorReferencia(long id, bool addPai)
        {
            try
            {
                if (id > 0)
                {
                    //var unidades = AsyncHelper.RunSync(() => _unidadeAppService.ListarPorReferencial(id, false));
                    var unidades = AsyncHelper.RunSync(() => _unidadeAppService.ListarPorReferencial(id, addPai));

                    var lista = unidades.Items.ToList().Select(
                        c => new { DisplayText = c.Id, Value = c.Descricao });

                    return Json(new { Result = "OK", Options = lista }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = "OK", Options = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (System.Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        //Controllers das Abas
        //------------------------------------------------------------------------------------------------------------------------------------------------

        public async Task<ActionResult> CriarOuEditarProdutoEstoqueModal(long? id, long idProduto=0)
        {
           var  _id = id != null ? (long)id : 0;

            var estoques = _produtoEstoqueAppService.ListarTodosNaoRelacionadosProdutos(idProduto, _id);//_estoqueAppService.ListarTodos();

            CriarOuEditarProdutoEstoqueModalViewModel viewModel = new CriarOuEditarProdutoEstoqueModalViewModel(new ProdutoEstoqueDto());

            if (id.HasValue)
            {
                var output = await _produtoEstoqueAppService.Obter((long)id);

                viewModel = new CriarOuEditarProdutoEstoqueModalViewModel(output);
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);
            }
            else
            {
                viewModel = new CriarOuEditarProdutoEstoqueModalViewModel(new ProdutoEstoqueDto());
                viewModel.ProdutoId = idProduto;
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoEstoqueModal.cshtml", viewModel);


        }

        public async Task<ActionResult> CriarOuEditarProdutoUnidadeModal(long? id, long? unidadeReferencialId, long? unidadeGerencialId, long idProduto)
        {
            //-------------------------------------------------------------------------------------------------------------
            //popula combo de unidades. Lista unidades relacionadas a uma unidade referencial
            var unidadesPrincipais = await _unidadeAppService.ListarPorReferencial(unidadeReferencialId, true);
            var unidadesLista = unidadesPrincipais.Items.ToList();

            var unidadesUtilizadas = await _produtoAppService.ObterUnidadePorProduto(idProduto, false);
            List<UnidadeDto> unidadesUtilizadasList = new List<UnidadeDto>();
            unidadesUtilizadasList = unidadesUtilizadas.Items.ToList();
            List<UnidadeDto> unidades = new List<UnidadeDto>();

            //------------

            //var tiposunidades = await _tipoUnidadeAppService.ListarSemReferencialGerencial();

            var tiposunidades = await _tipoUnidadeAppService.ListarTodos();

            //------------

            CriarOuEditarProdutoUnidadeModalViewModel viewModel;

            var _unidadeReferencialId = (long)unidadeReferencialId;

            if (id.HasValue)
            {
                var output = await _produtoUnidadeAppService.Obter((long)id);
                viewModel = new CriarOuEditarProdutoUnidadeModalViewModel(output);
                viewModel.UnidadeReferencial = await _unidadeAppService.ObterUnidadeDto(_unidadeReferencialId);

                unidades = unidadesLista
                            .Where(m => !m.Id.IsIn(unidadesUtilizadasList
                                                   .Select(p => p.Id)
                                                   .ToArray())
                                   )
                            .ToList();

                //viewModel.IsAtivo = output.IsAtivo;

                //viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao", output.UnidadeId);
                viewModel.Unidades = new SelectList(unidades, "Id", "Descricao", output.UnidadeId);
                viewModel.TiposUnidades = new SelectList(tiposunidades.Items, "Id", "Descricao", output.UnidadeTipoId);
            }
            else
            {
                unidades = unidadesLista.Where(m => !m.Id.IsIn(unidadesUtilizadasList.Select(p => p.Id).ToArray())).ToList();

                viewModel = new CriarOuEditarProdutoUnidadeModalViewModel(new ProdutoUnidadeDto());
                viewModel.ProdutoId = idProduto;
                viewModel.UnidadeReferencial = await _unidadeAppService.ObterUnidadeDto(_unidadeReferencialId);

                //viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao");
                viewModel.Unidades = new SelectList(unidades, "Id", "Descricao");
                viewModel.TiposUnidades = new SelectList(tiposunidades.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoUnidadeModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarProdutoEmpresaModal(long? id, long idProduto)
        {
            //-------------------------------------------------------------------------------------------------------------
            //popula combo de empresas. Lista empresas relacionadas a uma empresa referencial
            //var empresasPrincipais = await _empresaAppService.ListarPorReferencial(empresaReferencialId, true);
            //var empresasLista = empresasPrincipais.Items.ToList();

            //var empresasUtilizadas = await _produtoAppService.ObterEmpresaPorProduto(idProduto);
            //List<EmpresaDto> empresasUtilizadasList = new List<EmpresaDto>();
            //empresasUtilizadasList = empresasUtilizadas.Items.ToList();
            //List<EmpresaDto> empresas = new List<EmpresaDto>();

            //------------

            //var tiposempresas = await _tipoEmpresaAppService.ListarSemReferencialGerencial();

            //------------

            var userId = AbpSession.UserId.Value;
            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            CriarOuEditarProdutoEmpresaModalViewModel viewModel;


            //var _empresaReferencialId = (long)empresaReferencialId;

            if (id.HasValue)
            {
                var output = await _produtoEmpresaAppService.Obter((long)id);
                viewModel = new CriarOuEditarProdutoEmpresaModalViewModel(output);
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);

                //viewModel.EmpresaReferencial = await _empresaAppService.ObterEmpresaDto(_empresaReferencialId);

                //empresas = empresasLista.Where(m => !m.Id.IsIn(empresasUtilizadasList.Select(p => p.Id).ToArray())).ToList();

                //viewModel.IsAtivo = output.IsAtivo;

                //viewModel.Empresas = new SelectList(empresas.Items, "Id", "Descricao", output.EmpresaId);
                //viewModel.Empresas = new SelectList(empresas, "Id", "Descricao", output.EmpresaId);
                //viewModel.TiposEmpresas = new SelectList(tiposempresas.Items, "Id", "Descricao", output.EmpresaTipoId);
            }
            else
            {
                //empresas = empresasLista.Where(m => !m.Id.IsIn(empresasUtilizadasList.Select(p => p.Id).ToArray())).ToList();

                viewModel = new CriarOuEditarProdutoEmpresaModalViewModel(new ProdutoEmpresaDto());
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                viewModel.ProdutoId = idProduto;
                //viewModel.ProdutoId = idProduto;
                //viewModel.EmpresaReferencial = await _empresaAppService.ObterEmpresaDto(_empresaReferencialId);

                ////viewModel.Empresas = new SelectList(empresas.Items, "Id", "Descricao");
                //viewModel.Empresas = new SelectList(empresas, "Id", "Descricao");
                //viewModel.TiposEmpresas = new SelectList(tiposempresas.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoEmpresaModal.cshtml", viewModel);
        }

        /// <summary>
        ///  id = id do produto        
        /// </summary>
        public async Task<ActionResult> ProdutoSaldoDetalhadoModal(long id, long idEstoque)
        {
            //ProdutosViewModel viewModel = new CriarOuEditarProdutoEstoqueModalViewModel(new ProdutoEstoqueDto());
            //var produtoSaldo = await _produtoAppService.ObterProdutoSaldo(id); isso aqui nao rola

            var estoque = await _estoqueAppService.Obter(idEstoque);
            //var estoque = await _estoqueAppService.Obter(1);

            CriarOuEditarProdutoSaldoViewModel viewModel;
            //viewModel = new CriarOuEditarProdutoSaldoViewModel(produtoSaldo);
            viewModel = new CriarOuEditarProdutoSaldoViewModel(new ProdutoSaldoDto());
            viewModel.ProdutoId = id;
            //viewModel.EstoqueId = idEstoque;
            viewModel.EstoqueId = idEstoque;
            viewModel.Estoque = estoque;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_SaldoDetalhado.cshtml", viewModel);
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------

        public async Task<ActionResult> CriarOuEditarProdutoListaSubstituicaoModal(long? id, long idProduto)
        {
            var produtos = await _produtoAppService.ListarTodos();

            ProdutoListaSubstituicaoModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _produtoListaSubstituicaoAppService.Obter((long)id);

                viewModel = new ProdutoListaSubstituicaoModalViewModel(output);
                viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao", output.ProdutoSubstituicaoId);
            }
            else
            {
                viewModel = new ProdutoListaSubstituicaoModalViewModel(new ProdutoListaSubstituicaoDto());
                viewModel.ProdutoId = idProduto;
                viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoListaSubstituicaoModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarProdutoRelacaoLaboratorioModal(long? id, long idProduto)
        {
            var laboratorios = await _produtoLaboratorioAppService.ListarTodos();

            CriarOuEditarProdutoRelacaoLaboratorioModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _produtoRelacaoLaboratorioAppService.Obter((long)id);

                viewModel = new CriarOuEditarProdutoRelacaoLaboratorioModalViewModel(output);
                viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao", output.ProdutoLaboratorio);
            }
            else
            {
                viewModel = new CriarOuEditarProdutoRelacaoLaboratorioModalViewModel(new ProdutoRelacaoLaboratorioDto());
                viewModel.ProdutoId = idProduto;
                viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoRelacaoLaboratorioModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarProdutoRelacaoPalavraChaveModal(long? id, long idProduto)
        {
            var palavraschaves = await _produtoPalavraChaveAppService.ListarTodos();

            CriarOuEditarProdutoRelacaoPalavraChaveModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _produtoRelacaoPalavraChaveAppService.Obter((long)id);

                viewModel = new CriarOuEditarProdutoRelacaoPalavraChaveModalViewModel(output);
                viewModel.PalavrasChaves = new SelectList(palavraschaves.Items, "Id", "Palavra", output.ProdutoPalavraChave);
            }
            else
            {
                viewModel = new CriarOuEditarProdutoRelacaoPalavraChaveModalViewModel(new ProdutoRelacaoPalavraChaveDto());
                viewModel.ProdutoId = idProduto;
                viewModel.PalavrasChaves = new SelectList(palavraschaves.Items, "Id", "Palavra");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoRelacaoPalavraChaveModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarProdutoRelacaoAcaoTerapeuticaModal(long? id, long idProduto)
        {
            var acoesterapeuticas = await _produtoAcaoTerapeuticaAppService.ListarTodos();

            CriarOuEditarProdutoRelacaoAcaoTerapeuticaModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _produtoRelacaoAcaoTerapeuticaAppService.Obter((long)id);

                viewModel = new CriarOuEditarProdutoRelacaoAcaoTerapeuticaModalViewModel(output);
                viewModel.AcoesTerapeuticas = new SelectList(acoesterapeuticas.Items, "Id", "Descricao", output.ProdutoAcaoTerapeutica);
            }
            else
            {
                viewModel = new CriarOuEditarProdutoRelacaoAcaoTerapeuticaModalViewModel(new ProdutoRelacaoAcaoTerapeuticaDto());
                viewModel.ProdutoId = idProduto;
                viewModel.AcoesTerapeuticas = new SelectList(acoesterapeuticas.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Produtos/_CriarOuEditarProdutoRelacaoAcaoTerapeuticaModal.cshtml", viewModel);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------

        public async Task<JsonResult> AutoComplete(string term)
        {
            var query = await _produtoAppService.ListarAutoComplete(term);
            return Json(query.Items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SalvarProduto(ProdutoDto input)
        {
            var produto = _produtoAppService.CriarGetId(input);
            return Json(produto, JsonRequestBehavior.AllowGet);
        }

        #region Unidade
        //------------------------------------------------------------------------------------------------------------------------
        // Unidade
        //------------------------------------------------------------------------------------------------------------------------
        [HttpPost]
        public JsonResult EditarProdutoUnidadeTipo(ProdutoUnidadeDto input)
        {
            try
            {
                //_produtoUnidadeTipoAppService.Editar(input);
                AsyncHelper.RunSync(() => _produtoUnidadeTipoAppService.Editar(input));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult CriarProdutoUnidadeTipo(ProdutoUnidadeDto input)
        {
            try
            {
                var objUnidadeTipo = AsyncHelper.RunSync(() => _produtoUnidadeTipoAppService.CriarOuEditar(input, input.Id));
                return Json(new { Result = "OK", Record = objUnidadeTipo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }



        [HttpPost]
        public JsonResult ExcluirProdutoUnidadeTipo(ProdutoUnidadeDto input)
        {
            try
            {
                //input. ProdutoListaSubstituicaoDto
                AsyncHelper.RunSync(() => _produtoUnidadeTipoAppService.Excluir(input));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        public async Task<JsonResult> InserirUnidadeReferencia(int produtoId, int unidadeId, int tipoUnidade)
        {
            var produtoTipo = AsyncHelper.RunSync(() => _produtoUnidadeTipoAppService.ObterPorUnidadeProduto(unidadeId, produtoId));

            if (produtoTipo == null)
            {
                ProdutoUnidadeDto input = new ProdutoUnidadeDto();

                input.ProdutoId = produtoId;
                //input.TipoUnidadeId = tipoUnidade;
                input.UnidadeId = unidadeId;
                //input.TipoUnidade = new ClassesAplicacao.Services.Cadastros.ProdutosTiposUnidade.Dto.ProdutoTipoUnidadeDto { Id = 1 };
                AsyncHelper.RunSync(() => _produtoUnidadeTipoAppService.CriarOuEditar(input, 0));

                return Json(input, JsonRequestBehavior.AllowGet);
            }
            return Json(produtoTipo, JsonRequestBehavior.AllowGet);
        }


        #endregion

        //------------------------------------------------------------------------------------------------------------------------
        // Substituicao
        //------------------------------------------------------------------------------------------------------------------------
        #region Substituição
        [HttpPost]
        public JsonResult EditarProdutoListaSubstituicao(ProdutoListaSubstituicaoDto input)
        {
            try
            {
                AsyncHelper.RunSync(() => _produtoListaSubstituicaoAppService.Editar(input));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult CriarProdutoListaSubstituicao(ProdutoListaSubstituicaoDto input)
        {
            try
            {
                var objResult = AsyncHelper.RunSync(() => _produtoListaSubstituicaoAppService.CriarOuEditar(input));
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public async Task<ActionResult> SalvarProdutoSubstituicao(ProdutoListaSubstituicaoDto produtoSubstituicao)
        {
            await _produtoListaSubstituicaoAppService.CriarOuEditar(produtoSubstituicao);
            return Content("Sucesso");
        }

        [HttpPost]
        public JsonResult ExcluirProdutoSubstituicao(long id)
        {
            try
            {
                AsyncHelper.RunSync(() => _produtoListaSubstituicaoAppService.Excluir(id));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        #region Portaria
        //------------------------------------------------------------------------------------------------------------------------
        // Portaria
        //------------------------------------------------------------------------------------------------------------------------
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult EditarProdutoRelacaoPortaria(ProdutoRelacaoPortariaDto input)
        {
            try
            {
                //var objTeste = AsyncHelper.RunSync(() => _produtoRelacaoPortariaAppService.Obter(1));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPortariaAppService.CriarOuEditar(input));

                //return Json(new { Result = "OK", Record = objTeste }, JsonRequestBehavior.AllowGet);
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult CriarProdutoRelacaoPortaria(ProdutoRelacaoPortariaDto input)
        {
            try
            {
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPortariaAppService.CriarOuEditar(input));
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public JsonResult ExcluirProdutoRelacaoPortaria(long id)
        {
            try
            {
                AsyncHelper.RunSync(() => _produtoRelacaoPortariaAppService.Excluir(id));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        #region Laboratório

        //------------------------------------------------------------------------------------------------------------------------
        // Laboratorio
        //------------------------------------------------------------------------------------------------------------------------
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult EditarProdutoRelacaoLaboratorio(ProdutoRelacaoLaboratorioDto input)
        {
            try
            {
                //teste//var objTeste = AsyncHelper.RunSync(() => _produtoRelacaoLaboratorioAppService.Obter(1));
                //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoLaboratorioAppService.CriarOuEditar(input, input.Id));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoLaboratorioAppService.CriarOuEditar(input));
                //teste// return Json(new { Result = "OK", Record = objTeste }, JsonRequestBehavior.AllowGet);
                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult CriarProdutoRelacaoLaboratorio(ProdutoRelacaoLaboratorioDto input)
        {
            try
            {
                //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoLaboratorioAppService.CriarOuEditar(input, input.Id));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoLaboratorioAppService.CriarOuEditar(input));
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public JsonResult ExcluirProdutoRelacaoLaboratorio(long id)
        {
            try
            {
                AsyncHelper.RunSync(() => _produtoRelacaoLaboratorioAppService.Excluir(id));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        #region Palavra chave
        //------------------------------------------------------------------------------------------------------------------------
        // PalavraChave
        //------------------------------------------------------------------------------------------------------------------------
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult EditarProdutoRelacaoPalavraChave(ProdutoRelacaoPalavraChaveDto input)
        {

            try
            {
                // var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPalavraChaveAppService.Obter(1));
                // var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPalavraChaveAppService.CriarOuEditar(input, input.Id));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPalavraChaveAppService.CriarOuEditar(input));

                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult CriarProdutoRelacaoPalavraChave(ProdutoRelacaoPalavraChaveDto input)
        {
            try
            {
                //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPalavraChaveAppService.CriarOuEditar(input, input.Id));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoPalavraChaveAppService.CriarOuEditar(input));
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public JsonResult ExcluirProdutoRelacaoPalavraChave(long id)
        {
            try
            {
                AsyncHelper.RunSync(() => _produtoRelacaoPalavraChaveAppService.Excluir(id));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        #region Estoque

        //------------------------------------------------------------------------------------------------------------------------
        // Estoque
        //------------------------------------------------------------------------------------------------------------------------
        //[AcceptVerbs("GET", "POST", "PUT")]
        //public JsonResult EditarProdutoRelacaoEstoque(ProdutoRelacaoEstoqueDto input)
        //{

        //    try
        //    {
        //        //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoEstoqueAppService.Obter(1));
        //        //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoEstoqueAppService.CriarOuEditar(input, input.Id));

        //        //return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);

        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
        //    }
        //}

        // [AcceptVerbs("GET", "POST", "PUT")]
        //public JsonResult CriarProdutoRelacaoEstoque(ProdutoRelacaoEstoqueDto input)
        //{
        //    try
        //    {
        //        var objResult = AsyncHelper.RunSync(() => _produtoRelacaoEstoqueAppService.CriarOuEditar(input, input.Id));
        //        return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
        //    }
        //}

        //[HttpPost]
        //public JsonResult ExcluirProdutoRelacaoEstoque(long id)
        //{
        //    try
        //    {
        //        AsyncHelper.RunSync(() => _produtoRelacaoEstoqueAppService.Excluir(id));
        //        return Json(new { Result = "OK" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        #endregion

        #region Ação terapeutica
        //------------------------------------------------------------------------------------------------------------------------
        // AcaoTerapeutica
        //------------------------------------------------------------------------------------------------------------------------
        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult EditarProdutoRelacaoAcaoTerapeutica(ProdutoRelacaoAcaoTerapeuticaDto input)
        {
            try
            {
                //var objTeste = AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.Obter(1));
                //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.CriarOuEditar(input, input.Id));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.CriarOuEditar(input));

                //return Json(new { Result = "OK", Record = objTeste }, JsonRequestBehavior.AllowGet);
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult CriarProdutoRelacaoAcaoTerapeutica(ProdutoRelacaoAcaoTerapeuticaDto input)
        {
            try
            {
                //var objResult = AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.CriarOuEditar(input, input.Id));
                var objResult = AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.CriarOuEditar(input));
                return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public JsonResult ExcluirProdutoRelacaoAcaoTerapeutica(long id)
        {
            try
            {
                AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.Excluir(id));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        #endregion

        //------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------------------

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult ListarNomeProdutosExcetoId(long id)
        {
            try
            {
                var unidades = AsyncHelper.RunSync(() => _produtoAppService.ListarProdutosExcetoId(id));
                var lista = unidades.Items.ToList().Select(
                    c => new { DisplayText = c.DescricaoResumida, Value = c.Id });
                return Json(new { Result = "OK", Options = lista }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult ListarCodigoProdutosExcetoId(long id)
        {
            try
            {
                if (id > 0)
                {
                    var unidades = AsyncHelper.RunSync(() => _produtoAppService.ListarProdutosExcetoId(id));
                    var lista = unidades.Items.ToList().Select(
                        c => new { DisplayText = c.Id, Value = c.Id });
                    return Json(new { Result = "OK", Options = lista }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = "OK", Options = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (System.Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}