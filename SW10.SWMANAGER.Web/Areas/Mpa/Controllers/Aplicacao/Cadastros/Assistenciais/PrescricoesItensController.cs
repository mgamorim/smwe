﻿using Abp.Web.Mvc.Authorization;
using Newtonsoft.Json;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.Prescricoes.FormulasEstoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.Prescricoes.FormulasExamesImagens;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.Prescricoes.FormulasExamesLaboratoriais;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.Prescricoes.FormulasFaturamentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros.Assistenciais
{
    public class PrescricoesItensController : SWMANAGERControllerBase
    {
        private readonly IPrescricaoItemAppService _prescricaoItemAppService;
        private readonly IFormulaEstoqueAppService _formulaEstoqueAppService;
        //private readonly IFormulaEstoqueItemAppService _formulaEstoqueItemAppService;
        private readonly IFormulaFaturamentoAppService _formulaFaturamentoAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IFormulaEstoqueKitAppService _formulaEstoqueKitAppService;

        public PrescricoesItensController(
            IPrescricaoItemAppService tipoControleAppService,
            IFormulaEstoqueAppService formulaEstoqueAppService,
            //IFormulaEstoqueItemAppService formulaEstoqueItemAppService,
            IFormulaFaturamentoAppService formulaFaturamentoAppService,
            IProdutoAppService produtoAppService,
            IFormulaEstoqueKitAppService formulaEstoqueKitAppService
            )
        {
            _prescricaoItemAppService = tipoControleAppService;
            _formulaEstoqueAppService = formulaEstoqueAppService;
            //_formulaEstoqueItemAppService = formulaEstoqueItemAppService;
            _formulaFaturamentoAppService = formulaFaturamentoAppService;
            _produtoAppService = produtoAppService;
            _formulaEstoqueKitAppService = formulaEstoqueKitAppService;
        }

        public ActionResult Index()
        {
            var model = new PrescricaoItemViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/Index.cshtml", model);
        }

        [OutputCache(Duration = 1, VaryByParam = "*")]
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Edit)]
        public ViewResult CriarOuEditar(long? id)
        {
            ViewBag.Id = id;
            return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/CriarOuEditar.cshtml");
        }

        [OutputCache(Duration = 1, VaryByParam = "*")]
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Edit)]
        public async Task<PartialViewResult> _CriarOuEditarPartial(long? id)
        {
            CriarOuEditarPrescricaoItemViewModel viewModel;
            if (id.HasValue)
            {
                var output = await _prescricaoItemAppService.Obter(id.Value);
                viewModel = new CriarOuEditarPrescricaoItemViewModel(output);
                var formulaEstoqueList = await _formulaEstoqueAppService.ListarPorPrescricaoItem(id.Value);
                var formulaFaturamentoList = await _formulaFaturamentoAppService.ListarFaturamentoPorPrescricaoItem(id.Value);
                var formulaExameLaboratorialList = await _formulaFaturamentoAppService.ListarExameLaboratorialPorPrescricaoItem(id.Value);
                var formulaExameImagemList = await _formulaFaturamentoAppService.ListarExameImagemPorPrescricaoItem(id.Value);
                viewModel.FormulaEstoqueList = JsonConvert.SerializeObject(formulaEstoqueList.Items.ToList());

                viewModel.FormulaEstoqueKitJson = JsonConvert.SerializeObject(_formulaEstoqueKitAppService.ListarPorPrescricaoItem(id.Value));


                viewModel.FormulaFaturamentoList = JsonConvert.SerializeObject(formulaFaturamentoList.Items.ToList());
                viewModel.FormulaExameLaboratorialList = JsonConvert.SerializeObject(formulaExameLaboratorialList.Items.ToList());
                viewModel.FormulaExameImagemList = JsonConvert.SerializeObject(formulaExameImagemList.Items.ToList());
            }
            else
            {
                viewModel = new CriarOuEditarPrescricaoItemViewModel(new PrescricaoItemDto());
                viewModel.FormulaEstoqueList = JsonConvert.SerializeObject(new List<FormulaEstoqueDto>());
                viewModel.FormulaFaturamentoList = JsonConvert.SerializeObject(new List<FormulaFaturamentoDto>());
                viewModel.FormulaExameLaboratorialList = JsonConvert.SerializeObject(new List<FormulaFaturamentoDto>());
                viewModel.FormulaExameImagemList = JsonConvert.SerializeObject(new List<FormulaFaturamentoDto>());
                viewModel.FormulaEstoqueKitJson = JsonConvert.SerializeObject(new List<FormulaEstoqueKitDto>());
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_CriarOuEditarModal.cshtml", viewModel);
        }

        [OutputCache(Duration = 1, VaryByParam = "*")]
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Edit)]
        public PartialViewResult _CriarOuEditar(long? produtoId)
        {
            var model = new PrescricaoItemDto();

            if (produtoId.HasValue && produtoId.Value > 0)
            {
                model = Task.Run(() => _prescricaoItemAppService.ObterPorProduto(produtoId.Value)).Result;
                if (model == null)
                {
                    model = new PrescricaoItemDto();
                    model.ProdutoId = produtoId.Value;
                    model.Produto = Task.Run(() => _produtoAppService.Obter(produtoId.Value)).Result;
                }
            }

            var viewModel = new CriarOuEditarPrescricaoItemViewModel(model);

            if (model.Id > 0)
            {
                var formulaEstoqueList = Task.Run(() => _formulaEstoqueAppService.ListarPorPrescricaoItem(model.Id)).Result;
                var formulaFaturamentoList = Task.Run(() => _formulaFaturamentoAppService.ListarFaturamentoPorPrescricaoItem(model.Id)).Result;
                var formulaExameLaboratorialList = Task.Run(() => _formulaFaturamentoAppService.ListarExameLaboratorialPorPrescricaoItem(model.Id)).Result;
                var formulaExameImagemList = Task.Run(() => _formulaFaturamentoAppService.ListarExameImagemPorPrescricaoItem(model.Id)).Result;
                viewModel.FormulaEstoqueList = JsonConvert.SerializeObject(formulaEstoqueList.Items.ToList());
                viewModel.FormulaFaturamentoList = JsonConvert.SerializeObject(formulaFaturamentoList.Items.ToList());
                viewModel.FormulaExameLaboratorialList = JsonConvert.SerializeObject(formulaExameLaboratorialList.Items.ToList());
                viewModel.FormulaExameImagemList = JsonConvert.SerializeObject(formulaExameImagemList.Items.ToList());
            }
            else
            {
                viewModel.FormulaEstoqueList = JsonConvert.SerializeObject(new List<FormulaEstoqueDto>());
                viewModel.FormulaFaturamentoList = JsonConvert.SerializeObject(new List<FormulaFaturamentoDto>());
                viewModel.FormulaExameLaboratorialList = JsonConvert.SerializeObject(new List<FormulaFaturamentoDto>());
                viewModel.FormulaExameImagemList = JsonConvert.SerializeObject(new List<FormulaFaturamentoDto>());
            }
            viewModel.IsCadastroProduto = true;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_CriarOuEditarModal.cshtml", viewModel);
        }

        [ChildActionOnly]
        public PartialViewResult _IndexFormulaEstoque(long prescricaoItemId)
        {
            var model = new FormulaEstoqueViewModel();
            model.PrescricaoItemId = prescricaoItemId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_IndexFormulaEstoque.cshtml", model);
        }

        [ChildActionOnly]
        public PartialViewResult _IndexFormulaFaturamento(long prescricaoItemId)
        {
            var model = new FormulaFaturamentoViewModel();
            model.PrescricaoItemId = prescricaoItemId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_IndexFormulaFaturamento.cshtml", model);
        }

        [ChildActionOnly]
        public PartialViewResult _IndexFormulaExameLaboratorial(long prescricaoItemId)
        {
            var model = new FormulaExameLaboratorialViewModel();
            model.PrescricaoItemId = prescricaoItemId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_IndexFormulaExameLaboratorial.cshtml", model);
        }

        [ChildActionOnly]
        public PartialViewResult _IndexFormulaExameImagem(long prescricaoItemId)
        {
            var model = new FormulaExameImagemViewModel();
            model.PrescricaoItemId = prescricaoItemId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_IndexFormulaExameImagem.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaEstoque_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaEstoque_Edit)]
        public PartialViewResult _CriarOuEditarFormulaEstoqueModal(long prescricaoItemId, long? id, long? idGrid)
        {
            CriarOuEditarFormulaEstoqueViewModel viewModel;
            if (id.HasValue)
            {
                var output = Task.Run(() => _formulaEstoqueAppService.Obter(id.Value)).Result;
                viewModel = new CriarOuEditarFormulaEstoqueViewModel(output);
            }
            else
            {
                viewModel = new CriarOuEditarFormulaEstoqueViewModel(new FormulaEstoqueDto());
                viewModel.PrescricaoItemId = prescricaoItemId;
            }
            viewModel.PrescricaoItemId = prescricaoItemId;
            if (idGrid.HasValue)
            {
                viewModel.IdGridFormulasEstoque = idGrid.Value;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_CriarOuEditarFormulaEstoqueModal.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaFaturamento_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaFaturamento_Edit)]
        //public PartialViewResult _CriarOuEditarFormulaFaturamentoModal(long prescricaoItemId, long? id, long? idGrid)
        public PartialViewResult _CriarOuEditarFormulaFaturamentoModal(long prescricaoItemId, long? id, long? idGrid)
        {
            CriarOuEditarFormulaFaturamentoViewModel viewModel;
            if (id.HasValue)
            {
                var output = Task.Run(() => _formulaFaturamentoAppService.Obter(id.Value)).Result;

                viewModel = new CriarOuEditarFormulaFaturamentoViewModel(output);
            }
            else
            {
                viewModel = new CriarOuEditarFormulaFaturamentoViewModel(new FormulaFaturamentoDto());
                viewModel.PrescricaoItemId = prescricaoItemId;
            }
            viewModel.PrescricaoItemId = prescricaoItemId;
            if (idGrid.HasValue)
            {
                viewModel.IdGridFormulasFaturamento = idGrid.Value;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_CriarOuEditarFormulaFaturamentoModal.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaExameLaboratorial_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaExameLaboratorial_Edit)]
        public PartialViewResult _CriarOuEditarFormulaExameLaboratorialModal(long prescricaoItemId, long? id, long? idGrid)
        {
            CriarOuEditarFormulaExameLaboratorialViewModel viewModel;
            if (id.HasValue && id.Value > 0)
            {
                var query = Task.Run(() => _formulaFaturamentoAppService.Obter(id.Value)).Result;
                var output = new FormulaExameLaboratorialDto();
                output.Codigo = query.Codigo;
                output.CreationTime = query.CreationTime;
                output.CreatorUserId = query.CreatorUserId;
                output.DeleterUserId = query.DeleterUserId;
                output.DeletionTime = query.DeletionTime;
                output.Descricao = query.Descricao;
                output.FaturamentoItemId = query.FaturamentoItemId;
                output.Id = query.Id;
                output.IdGridFormulasExameLaboratorial = query.IdGridFormulasExameLaboratorial;
                output.IsDeleted = query.IsDeleted;
                output.IsFatura = query.IsFatura;
                output.IsSistema = query.IsSistema;
                output.LastModificationTime = query.LastModificationTime;
                output.LastModifierUserId = query.LastModifierUserId;
                output.MaterialId = query.MaterialId;
                output.PrescricaoItemId = query.PrescricaoItemId;
                output.FaturamentoItem = query.FaturamentoItem;
                output.Material = query.Material;
                //PrescricaoItem = query.PrescricaoItem
                viewModel = new CriarOuEditarFormulaExameLaboratorialViewModel(output);
            }
            else
            {
                viewModel = new CriarOuEditarFormulaExameLaboratorialViewModel(new FormulaExameLaboratorialDto());
            }
            viewModel.PrescricaoItemId = prescricaoItemId;
            if (idGrid.HasValue)
            {
                viewModel.IdGridFormulasExameLaboratorial = idGrid.Value;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_CriarOuEditarFormulaExameLaboratorialModal.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaExameImagem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_FormulaExameImagem_Edit)]
        public PartialViewResult _CriarOuEditarFormulaExameImagemModal(long prescricaoItemId, long? id, long? idGrid)
        {
            CriarOuEditarFormulaExameImagemViewModel viewModel;
            if (id.HasValue && id.Value > 0)
            {
                var query = Task.Run(() => _formulaFaturamentoAppService.Obter(id.Value)).Result;
                var output = new FormulaExameImagemDto();
                output.Codigo = query.Codigo;
                output.CreationTime = query.CreationTime;
                output.CreatorUserId = query.CreatorUserId;
                output.DeleterUserId = query.DeleterUserId;
                output.DeletionTime = query.DeletionTime;
                output.Descricao = query.Descricao;
                output.FaturamentoItemId = query.FaturamentoItemId;
                output.Id = query.Id;
                output.IdGridFormulasExameLaboratorial = query.IdGridFormulasExameLaboratorial;
                output.IsDeleted = query.IsDeleted;
                output.IsFatura = query.IsFatura;
                output.IsSistema = query.IsSistema;
                output.LastModificationTime = query.LastModificationTime;
                output.LastModifierUserId = query.LastModifierUserId;
                output.MaterialId = query.MaterialId;
                output.PrescricaoItemId = query.PrescricaoItemId;
                output.FaturamentoItem = query.FaturamentoItem;
                output.Material = query.Material;
                viewModel = new CriarOuEditarFormulaExameImagemViewModel(output);
            }
            else
            {
                viewModel = new CriarOuEditarFormulaExameImagemViewModel(new FormulaExameImagemDto());
            }
            viewModel.PrescricaoItemId = prescricaoItemId;
            if (idGrid.HasValue)
            {
                viewModel.IdGridFormulasExameImagem = idGrid.Value;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_CriarOuEditarFormulaExameImagemModal.cshtml", viewModel);
        }

        public PartialViewResult _FormCriarOuEditar(long? produtoId)
        {
            var model = new PrescricaoItemDto();
            if (produtoId.HasValue && produtoId.Value > 0)
            {
                model = Task.Run(() => _prescricaoItemAppService.ObterPorProduto(produtoId.Value)).Result;
                if (model == null)
                {
                    model = new PrescricaoItemDto();
                }
            }
            var viewModel = new CriarOuEditarPrescricaoItemViewModel(model);
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_FormCriarOuEditar.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Edit, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Delete)]
        public PartialViewResult _Produtos()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_Produtos.cshtml");
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Edit, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Delete)]
        public PartialViewResult _Laboratorios()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_Laboratorios.cshtml");
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Create, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Edit, AppPermissions.Pages_Tenant_Cadastros_Assistenciais_Prescricao_PrescricaoItem_Delete)]
        public PartialViewResult _Imagens()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Assistenciais/Prescricoes/PrescricoesItens/_Imagens.cshtml");
        }

    }

}