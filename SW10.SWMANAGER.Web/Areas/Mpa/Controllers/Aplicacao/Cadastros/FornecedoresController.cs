﻿using Abp.Extensions;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using Newtonsoft.Json;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.CoresPele;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Escolaridades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.EstadosCivis;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Religioes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Sexos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.TiposPessoa;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.TiposTelefone;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Enderecos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Naturalidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Profissoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Fornecedores;
using SW10.SWMANAGER.Web.Controllers;
using SW10.SWMANAGER.Web.CorreiosService;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros
{
    public class FornecedoresController : SWMANAGERControllerBase
	{
		private readonly IFornecedorAppService _fornecedorAppService;
		private readonly IPaisAppService _paisAppService;
		private readonly IEstadoAppService _estadoAppService;
		private readonly ICidadeAppService _cidadeAppService;
		private readonly IProfissaoAppService _profissaoAppService;
		private readonly INaturalidadeAppService _naturalidadeAppService;
		private readonly IEspecialidadeAppService _especialidadeAppService;
		private readonly ISexoAppService _sexoAppService;
		private readonly IEscolaridadeAppService _escolaridadeAppService;
		private readonly ICorPeleAppService _corPeleAppService;
		private readonly IReligiaoAppService _religiaoAppService;
		private readonly IEstadoCivilAppService _estadoCivilAppService;
		private readonly ITipoTelefoneAppService _tipoTelefoneAppService;
		private readonly ITipoPessoaAppService _tipoPessoaAppService;
		private readonly IConvenioAppService _convenioAppService;
		private readonly IPacienteAppService _pacienteAppService;
		private readonly IMedicoAppService _medicoAppService;
		private readonly IEmpresaAppService _empresaAppService;
        private readonly ITipoLogradouroAppService _tipoLogradouroAppService;

        public FornecedoresController(
			IFornecedorAppService fornecedorAppService,
			IPaisAppService paisAppService,
			IEstadoAppService estadoAppService,
			IProfissaoAppService profissaoAppService,
			INaturalidadeAppService naturalidadeAppService,
            ICidadeAppService cidadeAppService,
			IEspecialidadeAppService especialidadeAppService,
			ISexoAppService sexoAppService,
			IEscolaridadeAppService escolaridadeAppService,
			ICorPeleAppService corPeleAppService,
			IReligiaoAppService religiaoAppService,
			IEstadoCivilAppService estadoCivilAppService,
			ITipoTelefoneAppService tipoTelefoneAppService,
			ITipoPessoaAppService tipoPessoaAppService,
			IPacienteAppService pacienteAppService,
			IConvenioAppService convenioAppService,
			IMedicoAppService medicoAppService,
			IEmpresaAppService empresaAppService,
            ITipoLogradouroAppService tipoLogradouroAppService
            )
		{
			_fornecedorAppService = fornecedorAppService;
			_paisAppService = paisAppService;
			_estadoAppService = estadoAppService;
			_cidadeAppService = cidadeAppService;
			_profissaoAppService = profissaoAppService;
			_naturalidadeAppService = naturalidadeAppService;
			_especialidadeAppService = especialidadeAppService;
			_sexoAppService = sexoAppService;
			_escolaridadeAppService = escolaridadeAppService;
			_corPeleAppService = corPeleAppService;
			_religiaoAppService = religiaoAppService;
			_estadoCivilAppService = estadoCivilAppService;
			_tipoTelefoneAppService = tipoTelefoneAppService;
			_tipoPessoaAppService = tipoPessoaAppService;
			_pacienteAppService = pacienteAppService;
			_convenioAppService = convenioAppService;
			_medicoAppService = medicoAppService;
			_empresaAppService = empresaAppService;
            _tipoLogradouroAppService = tipoLogradouroAppService;
		}

		public ActionResult Index()
		{
			var model = new FornecedoresViewModel();
			return View("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/Index.cshtml",model);
		}

		[AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_Fornecedor_Create,AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_Fornecedor_Edit)]
		public async Task<PartialViewResult> CriarOuEditarModal(long? id)
		{
            CriarOuEditarFornecedorModalViewModel viewModel = null;
            if (id==null || id==0)
            {
                viewModel = new CriarOuEditarFornecedorModalViewModel(new SisFornecedorDto() { SisPessoa = new SisPessoaDto() });
                viewModel.Enderecos = JsonConvert.SerializeObject(new List<EnderecoDto>());
            }
            else
            {
                var fornecedorDto = await _fornecedorAppService.Obter((long)id);

                viewModel=new CriarOuEditarFornecedorModalViewModel(fornecedorDto);
                viewModel.Enderecos = JsonConvert.SerializeObject(fornecedorDto.SisPessoa.Enderecos);
            }

            
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_CriarOuEditarModal.cshtml",viewModel);
		}

		public ActionResult ObterIdade(DateTime data)
		{
			var idade = DateDifference.GetExtendedDifference(data);
			return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_ObterIdade.cshtml",idade);
		}

		public PartialViewResult _CarregarFoto()
		{
			return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_CarregarFoto.cshtml");
		}

		//public async Task<ActionResult> _CriarOuEditarFornecedorEspecialidadeModal(long fornecedorId,long? id)
		//{
		//	CriarOuEditarFornecedorEspecialidadeModalViewModel viewModel; //=new CriarOuEditarFornecedorEspecialidadeModalViewModel()
		//	var fornecedor = await _fornecedorAppService.Obter(fornecedorId);
		//	var especialidades = await _especialidadeAppService.Listar();
		//	var fornecedorEspecialidades = fornecedor.FornecedorEspecialidades;
		//	var especialidadesCadastradas = fornecedorEspecialidades.Select(m => m.EspecialidadeId);

		//	var especialidadesFornecedor = especialidades.Items.Where(m => m.Id.IsIn(especialidadesCadastradas.ToArray()));
		//	var especialidadesDisponiveis = especialidades.Items.Except(especialidadesFornecedor).ToList();
		//	if(id.HasValue)
		//	{
		//		var output = await _fornecedorEspecialidadeAppService.Obter((long)id);
		//		viewModel = new CriarOuEditarFornecedorEspecialidadeModalViewModel(output);
		//		var especialidade = await _especialidadeAppService.Obter(output.EspecialidadeId);
		//		viewModel.Especialidade = especialidade;
		//		viewModel.EspecialidadeId = output.EspecialidadeId;
		//		//var include = especialidade.MapTo<EspecialidadeDto>();
		//		//especialidadesDisponiveis.Add(especialidades.Items.Where(m=>m.Id==output.EspecialidadeId).FirstOrDefault());
		//		viewModel.Especialidades = new SelectList(especialidadesDisponiveis,"Id","Nome",viewModel.EspecialidadeId);
		//	}
		//	else
		//	{
		//		viewModel = new CriarOuEditarFornecedorEspecialidadeModalViewModel(new FornecedorEspecialidadeDto());
		//		viewModel.Especialidades = new SelectList(especialidadesDisponiveis,"Id","Nome");
		//	}
		//	viewModel.Fornecedor = fornecedor;
		//	viewModel.FornecedorId = fornecedorId;
		//	return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_CriarOuEditarFornecedorEspecialidadeModal.cshtml",viewModel);
		//}

		//public async Task<ActionResult> _FornecedorEspecialidades(long id)
		//{
		//	var especialidades = await _especialidadeAppService.Listar();
		//	var result = await _fornecedorAppService.Obter(id);
		//	var fornecedorEspecialidades = result.FornecedorEspecialidades.ToList();
		//	var viewModel = new FornecedorEspecialidadesViewModel();
		//	viewModel.Especialidades = especialidades.Items.ToList();
		//	viewModel.FornecedorEspecialidades = fornecedorEspecialidades;
		//	return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorEspecialidades.cshtml",viewModel);
		//}

		//public async Task<FileContentResult> ObterFotoFornecedor(long id)
		//{
		//	var fornecedorDto = await _fornecedorAppService.Obter(id);
		//	var fornecedor = fornecedorDto.MapTo<Fornecedor>();
		//	if(fornecedor.Foto.Length > 0)
		//	{
		//		return File(fornecedor.Foto,fornecedor.FotoMimeType);
		//	}
		//	else
		//	{
		//		return null;
		//	}
		//}

		public async Task<JsonResult> ConsultaCep(string cep)
		{
			var cepCorreios = new CepCorreios();
			try
			{
				var service = new AtendeClienteClient();
				var endereco = await service.consultaCEPAsync(cep);
				service.Close();
				if(endereco.@return.end.IsNullOrEmpty())
				{
					throw new UserFriendlyException(L("CepInvalido"));
				}
				cepCorreios.Bairro = endereco.@return.bairro;
				cepCorreios.Cidade = endereco.@return.cidade;
				cepCorreios.Uf = endereco.@return.uf;
				cepCorreios.Cep = endereco.@return.cep;
				cepCorreios.Complemento = endereco.@return.complemento;
				cepCorreios.Complemento2 = endereco.@return.complemento2;
				cepCorreios.End = endereco.@return.end;
				cepCorreios.Bairro = endereco.@return.bairro;

				//procurando o estado
				var estado = await _estadoAppService.Obter(cepCorreios.Uf);

				if(estado == null)
				{
					//estado não existe, cadastrar
					var input = new EstadoDto();
					input.PaisId = 1; //Brasil
					input.Nome = cepCorreios.Uf;
					input.Uf = cepCorreios.Uf;
					await _estadoAppService.CriarOuEditar(input);
					estado = await _estadoAppService.Obter(cepCorreios.Uf);
				}
				cepCorreios.EstadoId = estado.Id;
				cepCorreios.PaisId = estado.PaisId;

				//procurando a cidade
				var cidade = await _cidadeAppService.ObterComEstado(cepCorreios.Cidade,estado.Id);
				if(cidade == null)
				{
					//se não existir, incluir
					var input = new CidadeDto();
					input.Capital = false;
					input.EstadoId = estado.Id;
					input.Nome = cepCorreios.Cidade;
					await _cidadeAppService.CriarOuEditar(input);
					cidade = await _cidadeAppService.ObterComEstado(cepCorreios.Cidade,estado.Id);
				}
				cepCorreios.CidadeId = cidade.Id;
			}
			catch(System.Exception ex)
			{
				return Json(string.Format("ERRO: {0}",ex.Message),"application/json; charset=UTF-8",JsonRequestBehavior.AllowGet);
			}
			return Json(cepCorreios,"application/json; charset=UTF-8",JsonRequestBehavior.AllowGet);
		}

		//public async Task<JsonResult> FornecedoresPorEspecialidade(long id)
		//{
		//	var fornecedores = await _fornecedorAppService.ListarPorEspecialidade(id);
		//	return Json(fornecedores,"application/json;charset=UTF-8",JsonRequestBehavior.AllowGet);
		//}

		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public async Task<ActionResult> SalvarFornecedorEspecialidade(FornecedorEspecialidadeDto fornecedorEspecialidade)
		//{
		//	await _fornecedorEspecialidadeAppService.CriarOuEditar(fornecedorEspecialidade);
		//	return Content(L("Sucesso"));
		//}
		//public async Task<ActionResult> ExcluirFornecedorEspecialidade(long id)
		//{
		//	var fornecedorEspecialidade = await _fornecedorEspecialidadeAppService.Obter(id);
		//	var input = fornecedorEspecialidade.MapTo<FornecedorEspecialidadeDto>();
		//	await _fornecedorEspecialidadeAppService.Excluir(input);
		//	return Content(L("Sucesso"));
		//}

		public async Task<PartialViewResult> ExibirFornecedorPessoaFisica(long? fornecedorId)
		{
			CriarOuEditarFornecedorModalViewModel viewModel;


            /*

			var sexos = await _sexoAppService.ListarTodos();
			var coresPele = await _corPeleAppService.ListarTodos();
			var escolaridades = await _escolaridadeAppService.ListarTodos();
			var religioes = await _religiaoAppService.ListarTodos();
			var estadosCivis = await _estadoCivilAppService.ListarTodos();
			var tiposTelefone = await _tipoTelefoneAppService.ListarTodos();

			// Se for apenas criacao, nao precisa mais do if
			if(fornecedorId.HasValue)
			{
				var output = await _fornecedorAppService.Obter((long)fornecedorId);
				viewModel = new CriarOuEditarFornecedorModalViewModel(output);

				viewModel.Sexos = new SelectList(sexos.Items,"Id","Descricao");
				viewModel.CoresPele = new SelectList(coresPele.Items,"Id","Descricao");
				viewModel.Escolaridades = new SelectList(escolaridades.Items,"Id","Descricao");
				viewModel.Religioes = new SelectList(religioes.Items,"Id","Descricao");
				viewModel.EstadosCivis = new SelectList(estadosCivis.Items,"Id","Descricao");
				viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");
			}
			else
			{
				viewModel = new CriarOuEditarFornecedorModalViewModel(new CriarOuEditarFornecedor());

				viewModel.Sexos = new SelectList(sexos.Items,"Id","Descricao");
				viewModel.CoresPele = new SelectList(coresPele.Items,"Id","Descricao");
				viewModel.Escolaridades = new SelectList(escolaridades.Items,"Id","Descricao");
				viewModel.Religioes = new SelectList(religioes.Items,"Id","Descricao");
				viewModel.EstadosCivis = new SelectList(estadosCivis.Items,"Id","Descricao");
				viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");

				var pessoaFisica = new FornecedorPessoaFisicaDto();

				//pessoaFisica.Profissao = new ProfissaoDto();
				//pessoaFisica.Profissao.Id = 0;
				//pessoaFisica.Naturalidade = new NaturalidadeDto();
				//pessoaFisica.Naturalidade.Id = 0;
				pessoaFisica.Cidade = new CidadeDto();
				pessoaFisica.Cidade.Id = 0;
				pessoaFisica.Cidade.Estado = new EstadoDto();
				pessoaFisica.Cidade.Estado.Id = 0;
				pessoaFisica.Cidade.Estado.Pais = new PaisDto();
				pessoaFisica.Cidade.Estado.Pais.Id = 0;
				pessoaFisica.Estado = new EstadoDto();
				pessoaFisica.Estado.Id = 0;
				pessoaFisica.Pais = new PaisDto();
				pessoaFisica.Pais.Id = 0;
				pessoaFisica.CidadeId = 0;
				pessoaFisica.EstadoId = 0;
				pessoaFisica.PaisId = 0;
				pessoaFisica.ProfissaoId = 0;
				pessoaFisica.NaturalidadeId = 0;
				pessoaFisica.Bairro = string.Empty;
				pessoaFisica.Cep = string.Empty;

				pessoaFisica.Complemento = string.Empty;
				pessoaFisica.CorPele = 0;
				pessoaFisica.Cpf = string.Empty;
				pessoaFisica.Email = string.Empty;
				pessoaFisica.Emissao = DateTime.MinValue;
				pessoaFisica.Emissor = string.Empty;
				pessoaFisica.Logradouro = string.Empty;
				pessoaFisica.Escolaridade = 0;
				pessoaFisica.EstadoCivil = 0;
				pessoaFisica.Nascimento = DateTime.MinValue;
				pessoaFisica.Naturalidade = new NaturalidadeDto();
				pessoaFisica.NomeCompleto = string.Empty;
				pessoaFisica.NomeMae = pessoaFisica.NomePai = pessoaFisica.Numero = string.Empty;
				pessoaFisica.Religiao = 0;
				pessoaFisica.Rg = string.Empty;
				pessoaFisica.Sexo = 0;
				pessoaFisica.Telefone1 = pessoaFisica.Telefone2 = pessoaFisica.Telefone3 = pessoaFisica.Telefone4 = string.Empty;
				pessoaFisica.TipoTelefone1 = pessoaFisica.TipoTelefone2 = pessoaFisica.TipoTelefone3 = pessoaFisica.TipoTelefone4 = 0;

				viewModel.FornecedorPessoaFisica = pessoaFisica;
			}
            */

            viewModel = new CriarOuEditarFornecedorModalViewModel(new SisFornecedorDto());

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaFisica.cshtml",viewModel);
		}

		public async Task<PartialViewResult> ExibirFornecedorPessoaJuridica(long? fornecedorId)
		{
			CriarOuEditarFornecedorModalViewModel viewModel;

            /*

			var tiposTelefone = await _tipoTelefoneAppService.ListarTodos();

			// Se for apenas criacao, nao precisa mais do if
			if(fornecedorId.HasValue)
			{
				var output = await _fornecedorAppService.Obter((long)fornecedorId);
				viewModel = new CriarOuEditarFornecedorModalViewModel(output);

				viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");
			}
			else
			{
				viewModel = new CriarOuEditarFornecedorModalViewModel(new CriarOuEditarFornecedor());

				viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");

				var pessoaJuridica = new FornecedorPessoaJuridicaDto();

				pessoaJuridica.InscricaoEstadual = string.Empty;
				pessoaJuridica.InscricaoMunicipal = string.Empty;
				pessoaJuridica.RazaoSocial = string.Empty;
				pessoaJuridica.Cidade = new CidadeDto();
				pessoaJuridica.Cidade.Id = 0;
				pessoaJuridica.Cidade.Estado = new EstadoDto();
				pessoaJuridica.Cidade.Estado.Id = 0;
				pessoaJuridica.Cidade.Estado.Pais = new PaisDto();
				pessoaJuridica.Cidade.Estado.Pais.Id = 0;
				pessoaJuridica.Estado = new EstadoDto();
				pessoaJuridica.Estado.Id = 0;
				pessoaJuridica.Pais = new PaisDto();
				pessoaJuridica.Pais.Id = 0;
				pessoaJuridica.CidadeId = 0;
				pessoaJuridica.EstadoId = 0;
				pessoaJuridica.PaisId = 0;
				pessoaJuridica.Bairro = string.Empty;
				pessoaJuridica.Cep = string.Empty;
				pessoaJuridica.Complemento = string.Empty;
				pessoaJuridica.Logradouro = string.Empty;
				pessoaJuridica.Telefone1 = string.Empty;
				pessoaJuridica.Telefone2 = string.Empty;
				pessoaJuridica.Telefone3 = string.Empty;
				pessoaJuridica.Telefone4 = string.Empty;
				pessoaJuridica.TipoTelefone1 = 0;
				pessoaJuridica.TipoTelefone2 = 0;
				pessoaJuridica.TipoTelefone3 = 0;
				pessoaJuridica.TipoTelefone4 = 0;

				viewModel.FornecedorPessoaJuridica = pessoaJuridica;
			}
            */

            viewModel = new CriarOuEditarFornecedorModalViewModel(new SisFornecedorDto());

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaJuridica.cshtml",viewModel);
		}

		public async Task<PartialViewResult> ExibirPessoa(long pessoaId,long tipoCadastro)
		{
			CriarOuEditarFornecedorModalViewModel viewModel;

            /*
			var tiposTelefone = await _tipoTelefoneAppService.ListarTodos();

			switch(tipoCadastro)
			{
				case 1:
					var paciente = await _pacienteAppService.Obter(pessoaId);
					var pacientePessoaFisica = new FornecedorPessoaFisicaDto();

					pacientePessoaFisica.NomeCompleto = paciente.NomeCompleto;
					pacientePessoaFisica.Nascimento = paciente.Nascimento;
					pacientePessoaFisica.NomeMae = paciente.NomeMae;
					pacientePessoaFisica.NomePai = paciente.NomePai;
					pacientePessoaFisica.Rg = paciente.Rg;
					pacientePessoaFisica.Emissao = paciente.Emissao;
					pacientePessoaFisica.Emissor = paciente.Emissor;
					pacientePessoaFisica.Cpf = paciente.Cpf;
					pacientePessoaFisica.Profissao = paciente.Profissao.MapTo<ProfissaoDto>();
					pacientePessoaFisica.Escolaridade = paciente.Escolaridade;
					pacientePessoaFisica.Naturalidade = new NaturalidadeDto();
					pacientePessoaFisica.Naturalidade.Descricao = paciente.Naturalidade.Descricao;
					//pacientePessoaFisica.Naturalidade.EstadoOrigem = paciente.Naturalidade.EstadoOrigem;
					pacientePessoaFisica.Naturalidade.Id = paciente.Naturalidade.Id;
					pacientePessoaFisica.EstadoCivil = paciente.EstadoCivil;
					pacientePessoaFisica.Sexo = paciente.Sexo;
					pacientePessoaFisica.Religiao = paciente.Religiao;
					pacientePessoaFisica.CorPele = paciente.CorPele;
					pacientePessoaFisica.Bairro = paciente.Bairro;
					pacientePessoaFisica.Cep = paciente.Cep;
					pacientePessoaFisica.Cidade = new CidadeDto();
					pacientePessoaFisica.Cidade.Capital = paciente.Cidade.Capital;
					pacientePessoaFisica.Cidade.Id = paciente.Cidade.Id;
					pacientePessoaFisica.Cidade.EstadoId = paciente.Cidade.EstadoId;
					pacientePessoaFisica.Cidade.Nome = paciente.Cidade.Nome;
					pacientePessoaFisica.Cidade.Estado = new EstadoDto();
					pacientePessoaFisica.Cidade.Estado.Nome = paciente.Cidade.Estado.Nome;
					pacientePessoaFisica.Cidade.Estado.Id = paciente.Cidade.Estado.Id;
					pacientePessoaFisica.Cidade.Estado.PaisId = paciente.Cidade.Estado.PaisId;
					pacientePessoaFisica.Cidade.Estado.Uf = paciente.Cidade.Estado.Uf;
					pacientePessoaFisica.Cidade.Estado.Pais = new PaisDto();
					pacientePessoaFisica.Cidade.Estado.Pais.Id = paciente.Cidade.Estado.Pais.Id;
					pacientePessoaFisica.Cidade.Estado.Pais.Nome = paciente.Cidade.Estado.Pais.Nome;
					pacientePessoaFisica.Cidade.Estado.Pais.Sigla = paciente.Cidade.Estado.Pais.Sigla;
					pacientePessoaFisica.Complemento = paciente.Complemento;
					pacientePessoaFisica.Email = paciente.Email;
					pacientePessoaFisica.Logradouro = paciente.Logradouro;
					pacientePessoaFisica.Estado = new EstadoDto();
					pacientePessoaFisica.Estado.Nome = paciente.Estado.Nome;
					pacientePessoaFisica.Estado.Id = paciente.Estado.Id;
					pacientePessoaFisica.Estado.PaisId = paciente.Estado.PaisId;
					pacientePessoaFisica.Estado.Uf = paciente.Estado.Uf;
					pacientePessoaFisica.Id = paciente.Id;
					pacientePessoaFisica.Numero = paciente.Numero;
					pacientePessoaFisica.Pais = new PaisDto();
					pacientePessoaFisica.Pais.Nome = paciente.Pais.Nome;
					pacientePessoaFisica.Pais.Id = paciente.Pais.Id;
					pacientePessoaFisica.Pais.Sigla = paciente.Pais.Sigla;
					pacientePessoaFisica.Telefone1 = paciente.Telefone1;
					pacientePessoaFisica.Telefone2 = paciente.Telefone2;
					pacientePessoaFisica.Telefone3 = paciente.Telefone3;
					pacientePessoaFisica.Telefone4 = paciente.Telefone4;
					pacientePessoaFisica.TipoTelefone1 = paciente.TipoTelefone1;
					pacientePessoaFisica.TipoTelefone2 = paciente.TipoTelefone2;
					pacientePessoaFisica.TipoTelefone3 = paciente.TipoTelefone3;
					pacientePessoaFisica.TipoTelefone4 = paciente.TipoTelefone4;
					pacientePessoaFisica.CidadeId = paciente.CidadeId;
					pacientePessoaFisica.EstadoId = paciente.EstadoId;
					pacientePessoaFisica.PaisId = paciente.PaisId;
					pacientePessoaFisica.ProfissaoId = paciente.ProfissaoId;
					pacientePessoaFisica.NaturalidadeId = paciente.NaturalidadeId;

					var pacienteSexos = await _sexoAppService.ListarTodos();
					var pacienteCoresPele = await _corPeleAppService.ListarTodos();
					var pacienteEscolaridades = await _escolaridadeAppService.ListarTodos();
					var pacienteReligioes = await _religiaoAppService.ListarTodos();
					var pacienteEstadosCivis = await _estadoCivilAppService.ListarTodos();
					
					viewModel = new CriarOuEditarFornecedorModalViewModel(new CriarOuEditarFornecedor());
					viewModel.Id = pessoaId;
					viewModel.FornecedorPessoaFisica = pacientePessoaFisica;
					viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");
					viewModel.Sexos = new SelectList(pacienteSexos.Items,"Id","Descricao");
					viewModel.CoresPele = new SelectList(pacienteCoresPele.Items,"Id","Descricao");
					viewModel.Escolaridades = new SelectList(pacienteEscolaridades.Items,"Id","Descricao");
					viewModel.Religioes = new SelectList(pacienteReligioes.Items,"Id","Descricao");
					viewModel.EstadosCivis = new SelectList(pacienteEstadosCivis.Items,"Id","Descricao");

					return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaFisica.cshtml",viewModel);

				case 2:
					var medico = await _medicoAppService.Obter(pessoaId);
					var medicoPessoaFisica = new FornecedorPessoaFisicaDto();
					medicoPessoaFisica.NomeCompleto = medico.NomeCompleto;
					medicoPessoaFisica.Nascimento = medico.Nascimento;
					medicoPessoaFisica.NomeMae = medico.NomeMae;
					medicoPessoaFisica.NomePai = medico.NomePai;
					medicoPessoaFisica.Rg = medico.Rg;
					medicoPessoaFisica.Emissao = medico.Emissao;
					medicoPessoaFisica.Emissor = medico.Emissor;
					medicoPessoaFisica.Cpf = medico.Cpf;
					medicoPessoaFisica.Profissao = medico.Profissao.MapTo<ProfissaoDto>();
					medicoPessoaFisica.Escolaridade = medico.Escolaridade;
					medicoPessoaFisica.Naturalidade = new NaturalidadeDto();
					medicoPessoaFisica.Naturalidade.Descricao = medico.Naturalidade.Descricao;
					//medicoPessoaFisica.Naturalidade.EstadoOrigem = medico.Naturalidade.EstadoOrigem;
					medicoPessoaFisica.Naturalidade.Id = medico.Naturalidade.Id;
					medicoPessoaFisica.EstadoCivil = medico.EstadoCivil;
					medicoPessoaFisica.Sexo = medico.Sexo;
					medicoPessoaFisica.Religiao = medico.Religiao;
					medicoPessoaFisica.CorPele = medico.CorPele;
					medicoPessoaFisica.Bairro = medico.Bairro;
					medicoPessoaFisica.Cep = medico.Cep;
					medicoPessoaFisica.Cidade = new CidadeDto();
					medicoPessoaFisica.Cidade.Capital = medico.Cidade.Capital;
					medicoPessoaFisica.Cidade.Id = medico.Cidade.Id;
					medicoPessoaFisica.Cidade.EstadoId = medico.Cidade.EstadoId;
					medicoPessoaFisica.Cidade.Nome = medico.Cidade.Nome;
					medicoPessoaFisica.Cidade.Estado = new EstadoDto();
					medicoPessoaFisica.Cidade.Estado.Nome = medico.Cidade.Estado.Nome;
					medicoPessoaFisica.Cidade.Estado.Id = medico.Cidade.Estado.Id;
					medicoPessoaFisica.Cidade.Estado.PaisId = medico.Cidade.Estado.PaisId;
					medicoPessoaFisica.Cidade.Estado.Uf = medico.Cidade.Estado.Uf;
					medicoPessoaFisica.Cidade.Estado.Pais = new PaisDto();
					medicoPessoaFisica.Cidade.Estado.Pais.Id = medico.Cidade.Estado.Pais.Id;
					medicoPessoaFisica.Cidade.Estado.Pais.Nome = medico.Cidade.Estado.Pais.Nome;
					medicoPessoaFisica.Cidade.Estado.Pais.Sigla = medico.Cidade.Estado.Pais.Sigla;
					medicoPessoaFisica.Complemento = medico.Complemento;
					medicoPessoaFisica.Email = medico.Email;
					medicoPessoaFisica.Logradouro = medico.Logradouro;
					medicoPessoaFisica.Estado = new EstadoDto();
					medicoPessoaFisica.Estado.Nome = medico.Estado.Nome;
					medicoPessoaFisica.Estado.Id = medico.Estado.Id;
					medicoPessoaFisica.Estado.PaisId = medico.Estado.PaisId;
					medicoPessoaFisica.Estado.Uf = medico.Estado.Uf;
					medicoPessoaFisica.Id = medico.Id;
					medicoPessoaFisica.Numero = medico.Numero;
					medicoPessoaFisica.Pais = new PaisDto();
					medicoPessoaFisica.Pais.Nome = medico.Pais.Nome;
					medicoPessoaFisica.Pais.Id = medico.Pais.Id;
					medicoPessoaFisica.Pais.Sigla = medico.Pais.Sigla;
					medicoPessoaFisica.Telefone1 = medico.Telefone1;
					medicoPessoaFisica.Telefone2 = medico.Telefone2;
					medicoPessoaFisica.Telefone3 = medico.Telefone3;
					medicoPessoaFisica.Telefone4 = medico.Telefone4;
					medicoPessoaFisica.TipoTelefone1 = medico.TipoTelefone1;
					medicoPessoaFisica.TipoTelefone2 = medico.TipoTelefone2;
					medicoPessoaFisica.TipoTelefone3 = medico.TipoTelefone3;
					medicoPessoaFisica.TipoTelefone4 = medico.TipoTelefone4;
					medicoPessoaFisica.CidadeId = medico.CidadeId;
					medicoPessoaFisica.EstadoId = medico.EstadoId;
					medicoPessoaFisica.PaisId = medico.PaisId;
					medicoPessoaFisica.ProfissaoId = medico.ProfissaoId;
					medicoPessoaFisica.NaturalidadeId = medico.NaturalidadeId;

					var medicoSexos = await _sexoAppService.ListarTodos();
					var medicoCoresPele = await _corPeleAppService.ListarTodos();
					var medicoEscolaridades = await _escolaridadeAppService.ListarTodos();
					var medicoReligioes = await _religiaoAppService.ListarTodos();
					var medicoEstadosCivis = await _estadoCivilAppService.ListarTodos();

					viewModel = new CriarOuEditarFornecedorModalViewModel(new CriarOuEditarFornecedor());
					viewModel.Id = pessoaId;
					viewModel.FornecedorPessoaFisica = medicoPessoaFisica;
					viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");
					viewModel.Sexos = new SelectList(medicoSexos.Items,"Id","Descricao");
					viewModel.CoresPele = new SelectList(medicoCoresPele.Items,"Id","Descricao");
					viewModel.Escolaridades = new SelectList(medicoEscolaridades.Items,"Id","Descricao");
					viewModel.Religioes = new SelectList(medicoReligioes.Items,"Id","Descricao");
					viewModel.EstadosCivis = new SelectList(medicoEstadosCivis.Items,"Id","Descricao");

					return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaFisica.cshtml",viewModel);

				case 3:
					var convenio = await _convenioAppService.Obter(pessoaId);
					var convenioPessoaJuridica = new FornecedorPessoaJuridicaDto();

					convenioPessoaJuridica.NomeFantasia = convenio.NomeFantasia;
					convenioPessoaJuridica.RazaoSocial= convenio.RazaoSocial;
					convenioPessoaJuridica.InscricaoMunicipal = convenio.InscricaoMunicipal;
					convenioPessoaJuridica.InscricaoEstadual = convenio.InscricaoEstadual;
					convenioPessoaJuridica.Bairro = convenio.Bairro;
					convenioPessoaJuridica.Cep = convenio.Cep;
					convenioPessoaJuridica.Cidade = new CidadeDto();
					convenioPessoaJuridica.Cidade.Capital = convenio.Cidade.Capital;
					convenioPessoaJuridica.Cidade.Id = convenio.Cidade.Id;
					convenioPessoaJuridica.Cidade.EstadoId = convenio.Cidade.EstadoId;
					convenioPessoaJuridica.Cidade.Nome = convenio.Cidade.Nome;
					convenioPessoaJuridica.Cidade.Estado = new EstadoDto();
					convenioPessoaJuridica.Cidade.Estado.Nome = convenio.Cidade.Estado.Nome;
					convenioPessoaJuridica.Cidade.Estado.Id = convenio.Cidade.Estado.Id;
					convenioPessoaJuridica.Cidade.Estado.PaisId = convenio.Cidade.Estado.PaisId;
					convenioPessoaJuridica.Cidade.Estado.Uf = convenio.Cidade.Estado.Uf;
					convenioPessoaJuridica.Cidade.Estado.Pais = new PaisDto();
					convenioPessoaJuridica.Cidade.Estado.Pais.Id = convenio.Cidade.Estado.Pais.Id;
					convenioPessoaJuridica.Cidade.Estado.Pais.Nome = convenio.Cidade.Estado.Pais.Nome;
					convenioPessoaJuridica.Cidade.Estado.Pais.Sigla = convenio.Cidade.Estado.Pais.Sigla;
					convenioPessoaJuridica.Complemento = convenio.Complemento;
					convenioPessoaJuridica.Email = convenio.Email;
					convenioPessoaJuridica.Logradouro = convenio.Logradouro;
					convenioPessoaJuridica.Estado = new EstadoDto();
					convenioPessoaJuridica.Estado.Nome = convenio.Estado.Nome;
					convenioPessoaJuridica.Estado.Id = convenio.Estado.Id;
					convenioPessoaJuridica.Estado.PaisId = convenio.Estado.PaisId;
					convenioPessoaJuridica.Estado.Uf = convenio.Estado.Uf;
					convenioPessoaJuridica.Id = convenio.Id;
					convenioPessoaJuridica.Numero = convenio.Numero;
					convenioPessoaJuridica.Pais = new PaisDto();
					convenioPessoaJuridica.Pais.Nome = convenio.Pais.Nome;
					convenioPessoaJuridica.Pais.Id = convenio.Pais.Id;
					convenioPessoaJuridica.Pais.Sigla = convenio.Pais.Sigla;
					convenioPessoaJuridica.Telefone1 = convenio.Telefone1;
					convenioPessoaJuridica.Telefone2 = convenio.Telefone2;
					convenioPessoaJuridica.Telefone3 = convenio.Telefone3;
					convenioPessoaJuridica.Telefone4 = convenio.Telefone4;
					convenioPessoaJuridica.TipoTelefone1 = convenio.TipoTelefone1;
					convenioPessoaJuridica.TipoTelefone2 = convenio.TipoTelefone2;
					convenioPessoaJuridica.TipoTelefone3 = convenio.TipoTelefone3;
					convenioPessoaJuridica.TipoTelefone4 = convenio.TipoTelefone4;
					convenioPessoaJuridica.CidadeId = convenio.CidadeId;
					convenioPessoaJuridica.EstadoId = convenio.EstadoId;
					convenioPessoaJuridica.PaisId = convenio.PaisId;
					
					viewModel = new CriarOuEditarFornecedorModalViewModel(new CriarOuEditarFornecedor());
					viewModel.Id = pessoaId;
					viewModel.FornecedorPessoaJuridica = convenioPessoaJuridica;
					viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");
					
					return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaJuridica.cshtml",viewModel);

				default:
					var empresa = await _empresaAppService.Obter(pessoaId);
					var empresaPessoaJuridica = new FornecedorPessoaJuridicaDto();

					empresaPessoaJuridica.NomeFantasia = empresa.NomeFantasia;
					empresaPessoaJuridica.RazaoSocial = empresa.RazaoSocial;
					empresaPessoaJuridica.InscricaoMunicipal = empresa.InscricaoMunicipal;
					empresaPessoaJuridica.InscricaoEstadual = empresa.InscricaoEstadual;
					empresaPessoaJuridica.Bairro = empresa.Bairro;
					empresaPessoaJuridica.Cep = empresa.Cep;
					empresaPessoaJuridica.Cidade = new CidadeDto();
					empresaPessoaJuridica.Cidade.Capital = empresa.Cidade.Capital;
					empresaPessoaJuridica.Cidade.Id = empresa.Cidade.Id;
					empresaPessoaJuridica.Cidade.EstadoId = empresa.Cidade.EstadoId;
					empresaPessoaJuridica.Cidade.Nome = empresa.Cidade.Nome;
					empresaPessoaJuridica.Cidade.Estado = new EstadoDto();
					empresaPessoaJuridica.Cidade.Estado.Nome = empresa.Cidade.Estado.Nome;
					empresaPessoaJuridica.Cidade.Estado.Id = empresa.Cidade.Estado.Id;
					empresaPessoaJuridica.Cidade.Estado.PaisId = empresa.Cidade.Estado.PaisId;
					empresaPessoaJuridica.Cidade.Estado.Uf = empresa.Cidade.Estado.Uf;
					empresaPessoaJuridica.Cidade.Estado.Pais = new PaisDto();
					empresaPessoaJuridica.Cidade.Estado.Pais.Id = empresa.Cidade.Estado.Pais.Id;
					empresaPessoaJuridica.Cidade.Estado.Pais.Nome = empresa.Cidade.Estado.Pais.Nome;
					empresaPessoaJuridica.Cidade.Estado.Pais.Sigla = empresa.Cidade.Estado.Pais.Sigla;
					empresaPessoaJuridica.Complemento = empresa.Complemento;
					empresaPessoaJuridica.Email = empresa.Email;
					empresaPessoaJuridica.Logradouro = empresa.Logradouro;
					empresaPessoaJuridica.Estado = new EstadoDto();
					empresaPessoaJuridica.Estado.Nome = empresa.Estado.Nome;
					empresaPessoaJuridica.Estado.Id = empresa.Estado.Id;
					empresaPessoaJuridica.Estado.PaisId = empresa.Estado.PaisId;
					empresaPessoaJuridica.Estado.Uf = empresa.Estado.Uf;
					empresaPessoaJuridica.Id = empresa.Id;
					empresaPessoaJuridica.Numero = empresa.Numero;
					empresaPessoaJuridica.Pais = new PaisDto();
					empresaPessoaJuridica.Pais.Nome = empresa.Pais.Nome;
					empresaPessoaJuridica.Pais.Id = empresa.Pais.Id;
					empresaPessoaJuridica.Pais.Sigla = empresa.Pais.Sigla;
					empresaPessoaJuridica.Telefone1 = empresa.Telefone1;
					empresaPessoaJuridica.Telefone2 = empresa.Telefone2;
					empresaPessoaJuridica.Telefone3 = empresa.Telefone3;
					empresaPessoaJuridica.Telefone4 = empresa.Telefone4;
					empresaPessoaJuridica.TipoTelefone1 = empresa.TipoTelefone1;
					empresaPessoaJuridica.TipoTelefone2 = empresa.TipoTelefone2;
					empresaPessoaJuridica.TipoTelefone3 = empresa.TipoTelefone3;
					empresaPessoaJuridica.TipoTelefone4 = empresa.TipoTelefone4;
					empresaPessoaJuridica.CidadeId = empresa.CidadeId;
					empresaPessoaJuridica.EstadoId = empresa.EstadoId;
					empresaPessoaJuridica.PaisId = empresa.PaisId;
					

					viewModel = new CriarOuEditarFornecedorModalViewModel(new CriarOuEditarFornecedor());
					viewModel.Id = pessoaId;
					viewModel.FornecedorPessoaJuridica = empresaPessoaJuridica;
					viewModel.TiposTelefone = new SelectList(tiposTelefone.Items,"Id","Descricao");

    */
            viewModel = new CriarOuEditarFornecedorModalViewModel(new SisFornecedorDto());
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaJuridica.cshtml",viewModel);

			

			//	return PartialView("~/Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_FornecedorPessoaFisica.cshtml",viewModel);
		}




    /*

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<JsonResult> SalvarFornecedor(CriarOuEditarFornecedor fornecedor)
		{
			var result = L("SavedSuccessfully");
			if(ModelState.IsValid)
			{
				await _fornecedorAppService.CriarOuEditar(fornecedor);
			}
			else
			{
				result = L("ErroSalvar");
			}
			return Json(result,JsonRequestBehavior.AllowGet);
		}


        public async Task<JsonResult> AutoComplete(string term)
        {
            var query = await _fornecedorAppService.ListarAutoComplete(term);
            return Json(query.Items.ToArray(), JsonRequestBehavior.AllowGet);
        }

    */

    }
}