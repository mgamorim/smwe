﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Session;
using Newtonsoft.Json;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros
{
    public class SolicitacaoSaidasController : Controller
    {
        private readonly IEstoquePreMovimentoAppService _preMovimentoAppService;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly ITipoMovimentoAppService _tipoMovimentoAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAbpSession _AbpSession;
        private readonly IEstoqueAppService _produtoEstoqueAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;

        public SolicitacaoSaidasController(
            IEstoquePreMovimentoAppService preMovimentacaoAppService,
            IFornecedorAppService fornecedorAppService,
            ITipoMovimentoAppService tipoMovimentoAppService,
            IUserAppService userAppService,
        IAbpSession AbpSession,
        IEstoqueAppService produtoEstoqueAppService,
        IProdutoAppService produtoAppService,
        IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
        IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
        IPacienteAppService pacienteAppService,
        IMedicoAppService medicoAppService,
        IAtendimentoAppService atendimentoAppService

            )
        {
            _preMovimentoAppService = preMovimentacaoAppService;
            _fornecedorAppService = fornecedorAppService;
            _tipoMovimentoAppService = tipoMovimentoAppService;
            _userAppService = userAppService;
            _AbpSession = AbpSession;
            _produtoEstoqueAppService = produtoEstoqueAppService;
            _produtoAppService = produtoAppService;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _pacienteAppService = pacienteAppService;
            _medicoAppService = medicoAppService;
            _atendimentoAppService = atendimentoAppService;
        }

        public async Task<ActionResult> Index()
        {
            var tipoMovimnetacoes = await _tipoMovimentoAppService.ListarTipoMovimentoDevolucao();
            var fornecedores = await _fornecedorAppService.ListarTodos();
            var model = new PreMovimentoViewModel();
            model.TipoMovimentos = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");
            model.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Solicitacoes/Index.cshtml", model);
        }


        public async Task<ActionResult> CriarOuEditarModal(long? id)
        {
            var userId = _AbpSession.UserId.Value;
            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var tipoMovimnetacoes = await _tipoMovimentoAppService.ListarTipoMovimentoDevolucao();
            var estoques = await _produtoEstoqueAppService.ListarTodos();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();

            CriarOuEditarPreMovimentoModalViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);
                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
                viewModel.TipoMovimentos = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao", output.EstTipoMovimentoId);
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);

                // if(output.AtendimentoId!=null)


                if (output.PacienteId != null)
                {
                    var paciente = await _pacienteAppService.Obter((long)output.PacienteId);
                    viewModel.Paciente = new PacienteDto { Id = paciente.Id, NomeCompleto = paciente.NomeCompleto };
                }
                //  viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);

                if (output.MedicoSolcitanteId != null)
                {
                    var medico = await _medicoAppService.Obter((long)output.MedicoSolcitanteId);
                    viewModel.MedicoSolicitante = new MedicoDto { Id = medico.Id, NomeCompleto = medico.NomeCompleto };
                }

                // viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto", output.MedicoSolcitanteId);

                if (output.AtendimentoId != null)
                {
                    viewModel.Atendimento = await _atendimentoAppService.Obter((long)output.AtendimentoId);
                }



                var itens = _estoquePreMovimentoItemAppService.ObterItensSolicitacaoPorPreMovimento((long)id);
                viewModel.Itens = JsonConvert.SerializeObject(itens);
                viewModel.Unidades = new SelectList(new ListResultDto<Unidade>().Items, "Id", "Descricao");

            }
            else //Novo
            {
                viewModel = new CriarOuEditarPreMovimentoModalViewModel(new PreMovimentoViewModel());

                if (userEmpresas.Items.Count == 1)
                {
                    viewModel.EmpresaId = userEmpresas.Items.First().Id;
                    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", viewModel.EmpresaId);
                }
                else
                {
                    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                }
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao");
                viewModel.TipoMovimentos = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao");
            }

            viewModel.Movimento = DateTime.Now;
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Solicitacoes/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarPreMovimentoItemModal(string item)
        {
            CriarOuEditarPreMovimentoItemModalViewModel viewModel;
            var produtos = await _produtoAppService.ListarTodosParaMovimento();
            EstoquePreMovimentoItemSolicitacaoDto preMovimentoItem = new EstoquePreMovimentoItemSolicitacaoDto();

            if (!string.IsNullOrEmpty(item))
            {
                preMovimentoItem = JsonConvert.DeserializeObject<EstoquePreMovimentoItemSolicitacaoDto>(item);
            }


            if (preMovimentoItem.IdGrid!=null && preMovimentoItem.IdGrid != 0)
            {
                var model = new EstoquePreMovimentoItemDto();//  await _estoquePreMovimentoItemAppService.Obter(id.Value);

                model.ProdutoId = preMovimentoItem.ProdutoId;
                model.ProdutoUnidadeId = preMovimentoItem.ProdutoUnidadeId;

                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(model);
                if (model != null)
                {
                    viewModel.Produto = new ProdutoDto { Id = preMovimentoItem.ProdutoId, Descricao = preMovimentoItem.Produto, Codigo = preMovimentoItem.CodigoProduto };
                    var unidades = await _produtoAppService.ObterUnidadePorProduto(model.ProdutoId);
                    viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao", model.ProdutoUnidadeId);
                    viewModel.Quantidade = preMovimentoItem.Quantidade;
                    viewModel.NumeroSerie = preMovimentoItem.NumeroSerie;
                    viewModel.IdGrid = preMovimentoItem.IdGrid;
                }
            }
            else
            {
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto());
               // viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao");
                viewModel.Unidades = new SelectList(new ListResultDto<Unidade>().Items, "Id", "Descricao");
                viewModel.LotesValidades = new SelectList(new ListResultDto<LoteValidadeDto>().Items, "Id", "Nome");
            }
            viewModel.Id = preMovimentoItem.Id;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Solicitacoes/_CriarOuEditarPreMovimentoItemModal.cshtml", viewModel);
        }


    }
}