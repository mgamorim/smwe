﻿using Abp.Runtime.Session;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Suprimentos.Estoques
{
    public class ConfirmacaoMovimentosController : Controller
    {

        private readonly IEstoquePreMovimentoAppService _preMovimentoAppService;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAbpSession AbpSession;
        private readonly IEstoqueAppService _produtoEstoqueAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IEstoqueLoteValidadeAppService _estoqueLoteValidadeAppService;
        private readonly IOrdemCompraAppService _ordemCompraAppService;
        private readonly ICfopAppService _CFOPAppService;
        private readonly ITipoFreteAppService _TipoFreteAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly ICentroCustoAppService _centroCustoAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IEstoquePreMovimentoLoteValidadeAppService _estoquePreMovimentoLoteValidadeAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly ITipoOperacaoAppService _tipoOperacaoAppService;
        private readonly ITipoMovimentoAppService _tipoMovimentoAppService;


        public ConfirmacaoMovimentosController(
            //IEstoqueTipoMovimentoAppService estoqueTipoMovimentoAppService            ,
              IEstoquePreMovimentoAppService preMovimentacaoAppService,
            IUserAppService userAppService,
            IAbpSession abpSession,
            IFornecedorAppService fornecedorAppService,
            IEstoqueAppService produtoEstoqueAppService,
            IProdutoAppService produtoAppService,
            IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
            IEstoqueLoteValidadeAppService estoqueLoteValidadeAppService,
            IOrdemCompraAppService ordemCompraAppService,
            ICfopAppService CFOPAppService,
            ITipoFreteAppService TipoFreteAppService,
            IUnidadeAppService unidadeAppService,
            ICentroCustoAppService centroCustoAppService,
           // ITipoDocumentoAppService tipoDocumentoAppService,
            IPacienteAppService pacienteAppService,
            IEstoquePreMovimentoLoteValidadeAppService estoquePreMovimentoLoteValidadeAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            IMedicoAppService medicoAppService,
            IAtendimentoAppService atendimentoAppService,
            ITipoOperacaoAppService tipoOperacaoAppService,
            ITipoMovimentoAppService tipoMovimentoAppService
            )
        {
         //   _estoqueTipoMovimentoAppService = estoqueTipoMovimentoAppService;

            _preMovimentoAppService = preMovimentacaoAppService;
            _userAppService = userAppService;
            AbpSession = abpSession;
            _fornecedorAppService = fornecedorAppService;
            _produtoEstoqueAppService = produtoEstoqueAppService;
            _produtoAppService = produtoAppService;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _estoqueLoteValidadeAppService = estoqueLoteValidadeAppService;
            _ordemCompraAppService = ordemCompraAppService;
            _CFOPAppService = CFOPAppService;
            _TipoFreteAppService = TipoFreteAppService;
            _unidadeAppService = unidadeAppService;
            _centroCustoAppService = centroCustoAppService;
           // _tipoDocumentoAppService = tipoDocumentoAppService;
            _pacienteAppService = pacienteAppService;
            _estoquePreMovimentoLoteValidadeAppService = estoquePreMovimentoLoteValidadeAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _medicoAppService = medicoAppService;
            _atendimentoAppService = atendimentoAppService;
            _tipoOperacaoAppService = tipoOperacaoAppService;
            _tipoMovimentoAppService = tipoMovimentoAppService;
        }


        // GET: Mpa/ConfirmacaoMovimentos
        public async Task<ActionResult> Index()
        {
            var model = new PreMovimentoViewModel();

            //   var tiposMovimentacoes = await _estoqueTipoMovimentoAppService.ListarTodos();

            //  model.TipoMovimentos = new SelectList(tiposMovimentacoes.Items, "Id", "Descricao");


            var tipoOperacoes = await _tipoOperacaoAppService.Listar();
            model.TipoOperacaoes = new SelectList(tipoOperacoes.Items, "Id", "Descricao");

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoMovimentos/Index.cshtml", model);
        }

        public async Task<ActionResult> ConfirmarEntradaModal(long? id)
        {
            var fornecedores = await _fornecedorAppService.ListarTodos();

           // var userId = AbpSession.UserId.Value;
           // var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            //var estoques = await _produtoEstoqueAppService.ListarTodos();
           // var ordens = await _ordemCompraAppService.ListarTodos();
            //var CFOPs = await _CFOPAppService.ListarTodos();
           // var tiposFretes = await _TipoFreteAppService.ListarTodos();
           // var centroCustos = await _centroCustoAppService.ListarTodos();
          //  var pacientes = await _pacienteAppService.ListarTodos();
           // var tipomovimentos = await _tipoMovimentoAppService.Listar(true);

            CriarOuEditarPreMovimentoModalViewModel viewModel=null;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
              //  viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);  //SelectList(empresas.Items, "Id", "NomeFantasia", output.EmpresaId);
              ////  viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao", output.FornecedorId);
              //  viewModel.TipoMovimentos = new SelectList(tipomovimentos.Items, "Id", "Descricao", output.EstTipoMovimentoId);
              //  viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);
              //  viewModel.Ordens = new SelectList(ordens.Items, "Id", "Descricao", output.OrdemId);
              //  viewModel.CFOPs = new SelectList(CFOPs.Items, "Id", "Descricao", output.CFOPId);
              //  viewModel.TipoFretes = new SelectList(tiposFretes.Items, "Id", "Descricao", output.TipoFreteId);
              //  viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
              //  viewModel.CentroCustos = new SelectList(centroCustos.Items, "Id", "Descricao", output.CentroCustoId);
              //  viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);
            }
          


            viewModel.Movimento = DateTime.Now;

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoMovimentos/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<ActionResult> InformarLoteValidadeProdutoModal(long preMovimentoItemId, long produtoId)
        {
            var viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto { PreMovimentoId = preMovimentoItemId, ProdutoId = produtoId });

            var preMovimentoItem = _estoquePreMovimentoItemAppService.Obter(preMovimentoItemId).Result;
            if (preMovimentoItem != null)
            {
                preMovimentoItem.EstoquePreMovimento = await _preMovimentoAppService.Obter(preMovimentoItem.PreMovimentoId);
                if (preMovimentoItem.EstoquePreMovimento != null)
                {
                    viewModel.PreMovimentoEstadoId = preMovimentoItem.EstoquePreMovimento.PreMovimentoEstadoId;
                }

                viewModel.Quantidade = preMovimentoItem.Quantidade;
            }

            var produto = await _produtoAppService.Obter(produtoId);
            if (produto != null)
            {
                viewModel.Produto = produto;
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoMovimentos/_InformarLoteValidadeProduto.cshtml", viewModel);
        }

        public async Task<ActionResult> SaidaModal(long? id)
        {
             var fornecedores = await _fornecedorAppService.ListarTodos();

            var userId = AbpSession.UserId.Value;
           // var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var estoques = await _produtoEstoqueAppService.ListarTodos();
            var centroCustos = await _centroCustoAppService.ListarTodos();
         //   var pacientes = await _pacienteAppService.ListarTodos();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();
            var medicos = await _medicoAppService.ListarTodos();
          //  var atendimentos = await _atendimentoAppService.ListarTodos();
            var tipoMovimentacaoes = await _tipoMovimentoAppService.Listar(false);


            CriarOuEditarPreMovimentoModalViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

              //  output.SaidaPorId = output.PacienteId != null ? 1 : 2;

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
              //  viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);
                 viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao", output.FornecedorId);
              //  viewModel.TipoDocumentos = new SelectList(tipoDocumentos.Items, "Id", "Descricao", output.TipoDocumentoId);
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);
                //viewModel.Ordens = new SelectList(ordens.Items, "Id", "Descricao", output.OrdemId);
                //viewModel.CFOPs = new SelectList(CFOPs.Items, "Id", "Descricao", output.CFOPId);
                //viewModel.TipoFretes = new SelectList(tiposFretes.Items, "Id", "Descricao", output.TipoFreteId);
                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
                viewModel.CentroCustos = new SelectList(centroCustos.Items, "Id", "Descricao", output.CentroCustoId);
                //viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);
                viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto", output.MedicoSolcitanteId);
                //viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Codigo", output.AtendimentoId);
                viewModel.TipoMovimentos = new SelectList(tipoMovimentacaoes.Items, "Id", "Descricao", output.EstTipoMovimentoId);

            }
            else //Novo
            {
                viewModel = new CriarOuEditarPreMovimentoModalViewModel(new PreMovimentoViewModel());

                //if (userEmpresas.Items.Count == 1)
                //{
                //    viewModel.EmpresaId = userEmpresas.Items.First().Id;
                //    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", viewModel.EmpresaId);
                //}
                //else
                //{
                //    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                //}
                //viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");
               // viewModel.TipoDocumentos = new SelectList(tipoDocumentos.Items, "Id", "Descricao");
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao");
                //viewModel.Ordens = new SelectList(ordens.Items, "Id", "Descricao");
                //viewModel.CFOPs = new SelectList(CFOPs.Items, "Id", "Descricao");
                //viewModel.TipoFretes = new SelectList(tiposFretes.Items, "Id", "Descricao");
                viewModel.CentroCustos = new SelectList(centroCustos.Items, "Id", "Descricao");
               // viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto");
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao");
                viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto");
                //viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Codigo");
                viewModel.TipoMovimentos = new SelectList(tipoMovimentacaoes.Items, "Id", "Descricao");

            }

            viewModel.Movimento = DateTime.Now;
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoMovimentos/_SaidaModal.cshtml", viewModel);
        }

        public async Task<ActionResult> TransferenciaModal(long? id)
        {
            var estoques = await _produtoEstoqueAppService.ListarTodos();

            EstoqueTransferenciaProdutoViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoqueTransferenciaProdutoDto output = await _preMovimentoAppService.ObterTransferenciaPorEntradaId((long)id);

                viewModel = new EstoqueTransferenciaProdutoViewModel();
                viewModel.EstoqueSaidaId = (long)output.PreMovimentoSaida.EstoqueId;
                viewModel.EstoqueEntradaId = (long)output.PreMovimentoEntrada.EstoqueId;
                viewModel.EstoquesSaida = new SelectList(estoques.Items, "Id", "Descricao", output.PreMovimentoSaida.EstoqueId);
                viewModel.EstoquesEntrada = new SelectList(estoques.Items, "Id", "Descricao", output.PreMovimentoEntrada.EstoqueId);
                viewModel.Documento = output.Documento;
                viewModel.PreMovimentoSaidaId = output.PreMovimentoSaidaId;
                viewModel.PreMovimentoEntradaId = output.PreMovimentoEntradaId;
                viewModel.Id = output.Id;

            }
            else //Novo
            {
                viewModel = new EstoqueTransferenciaProdutoViewModel();

                long var;

                viewModel.EstoquesSaida = FuncoesGlobais.SelecionarSelectListUnitario<EstoqueDto>(estoques.Items, "Id", "Descricao", out var);
                viewModel.EstoqueSaidaId = var;

                viewModel.EstoquesEntrada = FuncoesGlobais.SelecionarSelectListUnitario<EstoqueDto>(estoques.Items, "Id", "Descricao", out var);
                viewModel.EstoqueEntradaId = var;

            }

            viewModel.Movimento = DateTime.Now;

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoMovimentos/_TransferenciaModal.cshtml", viewModel);
        }

        public async Task<ActionResult> DevolucaoModal(long? id)
        {
            // var fornecedores = await _fornecedorAppService.ListarTodos();

            var userId = AbpSession.UserId.Value;
            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var estoques = await _produtoEstoqueAppService.ListarTodos();
            var centroCustos = await _centroCustoAppService.ListarTodos();
            var pacientes = await _pacienteAppService.ListarTodos();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();
            var medicos = await _medicoAppService.ListarTodos();
            var atendimentos = await _atendimentoAppService.ListarTodos();
            var tipoMovimentacaoes = await _tipoMovimentoAppService.Listar(false);


            CriarOuEditarPreMovimentoModalViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

                //  output.SaidaPorId = output.PacienteId != null ? 1 : 2;

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);
                // viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao", output.FornecedorId);
                //  viewModel.TipoDocumentos = new SelectList(tipoDocumentos.Items, "Id", "Descricao", output.TipoDocumentoId);
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);
                //viewModel.Ordens = new SelectList(ordens.Items, "Id", "Descricao", output.OrdemId);
                //viewModel.CFOPs = new SelectList(CFOPs.Items, "Id", "Descricao", output.CFOPId);
                //viewModel.TipoFretes = new SelectList(tiposFretes.Items, "Id", "Descricao", output.TipoFreteId);
                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
                viewModel.CentroCustos = new SelectList(centroCustos.Items, "Id", "Descricao", output.CentroCustoId);
                viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);
                viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto", output.MedicoSolcitanteId);
                viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Codigo", output.AtendimentoId);
                viewModel.TipoMovimentos = new SelectList(tipoMovimentacaoes.Items, "Id", "Descricao", output.EstTipoMovimentoId);

            }
            else //Novo
            {
                viewModel = new CriarOuEditarPreMovimentoModalViewModel(new PreMovimentoViewModel());

                if (userEmpresas.Items.Count == 1)
                {
                    viewModel.EmpresaId = userEmpresas.Items.First().Id;
                    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", viewModel.EmpresaId);
                }
                else
                {
                    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                }
                //viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");
                // viewModel.TipoDocumentos = new SelectList(tipoDocumentos.Items, "Id", "Descricao");
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao");
                //viewModel.Ordens = new SelectList(ordens.Items, "Id", "Descricao");
                //viewModel.CFOPs = new SelectList(CFOPs.Items, "Id", "Descricao");
                //viewModel.TipoFretes = new SelectList(tiposFretes.Items, "Id", "Descricao");
                viewModel.CentroCustos = new SelectList(centroCustos.Items, "Id", "Descricao");
                viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto");
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao");
                viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto");
                viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Codigo");
                viewModel.TipoMovimentos = new SelectList(tipoMovimentacaoes.Items, "Id", "Descricao");

            }

            viewModel.Movimento = DateTime.Now;
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoMovimentos/_DevolucaoModal.cshtml", viewModel);
        }


    }
}