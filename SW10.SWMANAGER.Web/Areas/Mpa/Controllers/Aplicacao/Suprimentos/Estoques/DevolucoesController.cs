﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
//using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.TipoMovimentacoes;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros
{
    public class DevolucoesController : Controller
    {
        private readonly IEstoquePreMovimentoAppService _preMovimentoAppService;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly ITipoMovimentoAppService _tipoMovimentoAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAbpSession _AbpSession;
        private readonly IEstoqueAppService _produtoEstoqueAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IProdutoLaboratorioAppService _produtoLaboratorioAppService;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IEstoqueLoteValidadeAppService _estoqueLoteValidadeAppService;

        public DevolucoesController(
            IEstoquePreMovimentoAppService preMovimentacaoAppService,
            IFornecedorAppService fornecedorAppService,
            ITipoMovimentoAppService tipoMovimentoAppService,
            IUserAppService userAppService,
        IAbpSession AbpSession,
        IEstoqueAppService produtoEstoqueAppService,
        IPacienteAppService pacienteAppService,
        IMedicoAppService medicoAppService,
        IAtendimentoAppService atendimentoAppService,
        IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
        IProdutoAppService produtoAppService,
        IProdutoLaboratorioAppService produtoLaboratorioAppService,
        IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
        IEstoqueLoteValidadeAppService estoqueLoteValidadeAppService

            )
        {
            _preMovimentoAppService = preMovimentacaoAppService;
            _fornecedorAppService = fornecedorAppService;
            _tipoMovimentoAppService = tipoMovimentoAppService;
            _userAppService = userAppService;
            _AbpSession = AbpSession;
            _produtoEstoqueAppService = produtoEstoqueAppService;
            _pacienteAppService = pacienteAppService;
            _medicoAppService = medicoAppService;
            _atendimentoAppService = atendimentoAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _produtoAppService = produtoAppService;
            _produtoLaboratorioAppService = produtoLaboratorioAppService;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _estoqueLoteValidadeAppService = estoqueLoteValidadeAppService;
        }

        public async Task<ActionResult> Index()
        {
            var tipoMovimnetacoes = await _tipoMovimentoAppService.ListarTipoMovimentoDevolucao();
            var fornecedores = await _fornecedorAppService.ListarTodos();
            var model = new PreMovimentoViewModel();
            model.TipoMovimentos = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");
            model.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Devolucoes/Index.cshtml", model);
        }


        public async Task<ActionResult> CriarOuEditarModal(long? id)
        {
            // var fornecedores = await _fornecedorAppService.ListarTodos();

            var userId = _AbpSession.UserId.Value;
            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var tipoMovimnetacoes = await _tipoMovimentoAppService.ListarTipoMovimentoDevolucao();
            var estoques = await _produtoEstoqueAppService.ListarTodos();

            //  var pacientes = await _pacienteAppService.ListarTodos();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();
            //    var medicos = await _medicoAppService.ListarTodos();
            var atendimentos = await _atendimentoAppService.ListarAtendimentoPaciente();

            CriarOuEditarPreMovimentoModalViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);


                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);


                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
                if (output.PacienteId != null)
                {
                    var paciente = await _pacienteAppService.Obter((long)output.PacienteId);
                    viewModel.Paciente = new PacienteDto { Id = paciente.Id, NomeCompleto = paciente.NomeCompleto };
                }
                //  viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);

                if (output.MedicoSolcitanteId != null)
                {
                    var medico = await _medicoAppService.Obter((long)output.MedicoSolcitanteId);
                    viewModel.MedicoSolicitante = new MedicoDto { Id = medico.Id, NomeCompleto = medico.NomeCompleto };
                }

                // viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto", output.MedicoSolcitanteId);

                if (output.AtendimentoId != null)
                {
                    viewModel.Atendimento = await _atendimentoAppService.Obter((long)output.AtendimentoId);
                }

                viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Nome", output.AtendimentoId);
                viewModel.SaidaPor = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao", output.TipoDocumentoId);

            }
            else //Novo
            {
                viewModel = new CriarOuEditarPreMovimentoModalViewModel(new PreMovimentoViewModel());

                if (userEmpresas.Items.Count == 1)
                {
                    viewModel.EmpresaId = userEmpresas.Items.First().Id;
                    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", viewModel.EmpresaId);
                }
                else
                {
                    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                }
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao");
                // viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto");
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao");
                //  viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto");
                // viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Nome");
                viewModel.SaidaPor = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");
            }

            viewModel.Movimento = DateTime.Now;

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Devolucoes/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarPreMovimentoItemModal(long? id, long preMovimentoId = 0, long estoqueId = 0)
        {
            CriarOuEditarPreMovimentoItemModalViewModel viewModel;
            var produtos = await _produtoAppService.ListarTodosParaMovimento();
            var laboratorios = await _produtoLaboratorioAppService.ListarTodos();

            if (id.HasValue && id.Value != 0)
            {
                var model = await _estoquePreMovimentoItemAppService.Obter(id.Value);
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(model);
                if (model != null)
                {
                    viewModel.Hidden = (model.Produto.IsValidade || model.Produto.IsLote) ? "" : "hidden";

                    viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao", model.ProdutoId);
                    var unidades = await _produtoAppService.ObterUnidadePorProduto(model.ProdutoId);
                    viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao", model.ProdutoUnidadeId);

                    var EstoquePreMovimentoLoteValidadeResult = await _estoqueLoteValidadeAppService.ListarPorPreMovimentoItem(new ListarEstoquePreMovimentoInput { Filtro = model.Id.ToString() });
                    if (EstoquePreMovimentoLoteValidadeResult != null && EstoquePreMovimentoLoteValidadeResult.Items.Count() > 0)
                    {
                        var loteValidate = EstoquePreMovimentoLoteValidadeResult.Items[0].LoteValidade;
                        viewModel.LaboratorioId = loteValidate.ProdutoLaboratorioId;
                        viewModel.Lote = loteValidate.Lote;
                        viewModel.Validade = loteValidate.Validade;
                        viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao", loteValidate.ProdutoLaboratorioId);
                        viewModel.LoteValidadeId = loteValidate.Id;
                        viewModel.EstoquePreMovimentoLoteValidadeId = EstoquePreMovimentoLoteValidadeResult.Items[0].Id;

                        model.EstoquePreMovimento = await _preMovimentoAppService.Obter(model.PreMovimentoId);

                        var lotesValidades = await _estoqueLoteValidadeAppService.ObterPorProdutoEstoqueComSaida(model.ProdutoId, estoqueId, (long)model.EstoquePreMovimento.EstTipoMovimentoId, model.EstoquePreMovimento.UnidadeOrganizacionalId, model.EstoquePreMovimento.PacienteId);

                        viewModel.LotesValidades = new SelectList(lotesValidades, "Id", "Nome", loteValidate.Id);
                    }
                    else
                    {
                        viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Nome");
                        viewModel.LotesValidades = new SelectList(new ListResultDto<LoteValidadeDto>().Items, "Id", "Nome");
                    }
                }
            }
            else
            {
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto());
                viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao");
                viewModel.Unidades = new SelectList(new ListResultDto<Unidade>().Items, "Id", "Descricao");
                viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao");
                viewModel.LotesValidades = new SelectList(new ListResultDto<LoteValidadeDto>().Items, "Id", "Nome");
            }
            viewModel.Id = id.Value;
            viewModel.PreMovimentoId = preMovimentoId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Devolucoes/_CriarOuEditarPreMovimentoItemModal.cshtml", viewModel);
        }

        public async Task<JsonResult> SelecionarLotesValidades(long produtoId, long estoqueId, long tipoMovimentoId, long? unidadeOrganizacionalId, long? atendimentoId)
        {
            var lotesValidades = await _estoqueLoteValidadeAppService.ObterPorProdutoEstoqueComSaida(produtoId, estoqueId, tipoMovimentoId, unidadeOrganizacionalId, atendimentoId);
            return Json(lotesValidades, JsonRequestBehavior.AllowGet);
        }

    }
}