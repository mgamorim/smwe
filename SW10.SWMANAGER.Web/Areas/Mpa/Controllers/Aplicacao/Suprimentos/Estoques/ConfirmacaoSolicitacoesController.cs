﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Session;
using Newtonsoft.Json;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Relatorios;
using SW10.SWMANAGER.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Suprimentos.Estoques
{
    public class ConfirmacaoSolicitacoesController : Controller
    {

        private readonly IEstoquePreMovimentoAppService _preMovimentoAppService;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAbpSession AbpSession;
        private readonly IEstoqueAppService _produtoEstoqueAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IEstoqueLoteValidadeAppService _estoqueLoteValidadeAppService;
        private readonly IOrdemCompraAppService _ordemCompraAppService;
        private readonly ICfopAppService _CFOPAppService;
        private readonly ITipoFreteAppService _TipoFreteAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly ICentroCustoAppService _centroCustoAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IEstoquePreMovimentoLoteValidadeAppService _estoquePreMovimentoLoteValidadeAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly ITipoOperacaoAppService _tipoOperacaoAppService;
        private readonly ITipoMovimentoAppService _tipoMovimentoAppService;
        private readonly IProdutoLaboratorioAppService _produtoLaboratorioAppService;


        public ConfirmacaoSolicitacoesController(
            IEstoquePreMovimentoAppService preMovimentacaoAppService,
            IUserAppService userAppService,
            IAbpSession abpSession,
            IFornecedorAppService fornecedorAppService,
            IEstoqueAppService produtoEstoqueAppService,
            IProdutoAppService produtoAppService,
            IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
            IEstoqueLoteValidadeAppService estoqueLoteValidadeAppService,
            IOrdemCompraAppService ordemCompraAppService,
            ICfopAppService CFOPAppService,
            ITipoFreteAppService TipoFreteAppService,
            IUnidadeAppService unidadeAppService,
            ICentroCustoAppService centroCustoAppService,
            IPacienteAppService pacienteAppService,
            IEstoquePreMovimentoLoteValidadeAppService estoquePreMovimentoLoteValidadeAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            IMedicoAppService medicoAppService,
            IAtendimentoAppService atendimentoAppService,
            ITipoOperacaoAppService tipoOperacaoAppService,
            ITipoMovimentoAppService tipoMovimentoAppService,
            IProdutoLaboratorioAppService produtoLaboratorioAppService
            )
        {
            _preMovimentoAppService = preMovimentacaoAppService;
            _userAppService = userAppService;
            AbpSession = abpSession;
            _fornecedorAppService = fornecedorAppService;
            _produtoEstoqueAppService = produtoEstoqueAppService;
            _produtoAppService = produtoAppService;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _estoqueLoteValidadeAppService = estoqueLoteValidadeAppService;
            _ordemCompraAppService = ordemCompraAppService;
            _CFOPAppService = CFOPAppService;
            _TipoFreteAppService = TipoFreteAppService;
            _unidadeAppService = unidadeAppService;
            _centroCustoAppService = centroCustoAppService;
            _pacienteAppService = pacienteAppService;
            _estoquePreMovimentoLoteValidadeAppService = estoquePreMovimentoLoteValidadeAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _medicoAppService = medicoAppService;
            _atendimentoAppService = atendimentoAppService;
            _tipoOperacaoAppService = tipoOperacaoAppService;
            _tipoMovimentoAppService = tipoMovimentoAppService;
            _produtoLaboratorioAppService = produtoLaboratorioAppService;
        }


        // GET: Mpa/ConfirmacaoMovimentos
        public async Task<ActionResult> Index()
        {
            var model = new PreMovimentoViewModel();
            var tipoOperacoes = await _tipoOperacaoAppService.Listar();
            model.TipoOperacaoes = new SelectList(tipoOperacoes.Items, "Id", "Descricao");

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoSolicitacoes/Index.cshtml", model);
        }

        public async Task<ActionResult> ConfirmarSolicitacaoSaida(long? id)
        {

            var userId = AbpSession.UserId.Value;
            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var estoques = await _produtoEstoqueAppService.ListarTodos();
            var tipomovimentos = await _tipoMovimentoAppService.ListarTipoMovimentoDevolucao();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();

            CriarOuEditarPreMovimentoModalViewModel viewModel=null;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
                viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);  
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);

                viewModel.TipoMovimentos = new SelectList(tipomovimentos.Items, "Id", "Descricao", output.EstTipoMovimentoId);
                viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);
                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);

                if(output.AtendimentoId!=null)
                {
                    viewModel.Atendimento = await _atendimentoAppService.Obter((long)output.AtendimentoId);
                }

                if(output.MedicoSolcitanteId!=null)
                {
                    var medico = await _medicoAppService.Obter((long)output.MedicoSolcitanteId);
                    if(medico!=null)
                    {
                        viewModel.MedicoSolicitante = new MedicoDto {Id= medico.Id, NomeCompleto = medico.NomeCompleto };
                    }
                }

                if(output.PacienteId !=null)
                {
                    var paciente = await _pacienteAppService.Obter((long)output.PacienteId);
                    viewModel.Paciente = new PacienteDto { Id = paciente.Id, NomeCompleto = paciente.NomeCompleto };
                }

                var itens = _estoquePreMovimentoItemAppService.ObterItensSolicitacaoPorPreMovimento((long)id);
                viewModel.Itens = JsonConvert.SerializeObject(itens);
            }
          


            viewModel.Movimento = DateTime.Now;

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoSolicitacoes/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<ActionResult> CriarOuEditarPreMovimentoItemModal(string item)
        {
            CriarOuEditarPreMovimentoItemModalViewModel viewModel;
            var produtos = await _produtoAppService.ListarTodosParaMovimento();
            EstoquePreMovimentoItemSolicitacaoDto preMovimentoItem = new EstoquePreMovimentoItemSolicitacaoDto();

            if (!string.IsNullOrEmpty(item))
            {
                preMovimentoItem = JsonConvert.DeserializeObject<EstoquePreMovimentoItemSolicitacaoDto>(item);
            }


            if (preMovimentoItem.IdGrid != null && preMovimentoItem.IdGrid != 0)
            {
                var model = new EstoquePreMovimentoItemDto();//  await _estoquePreMovimentoItemAppService.Obter(id.Value);

                model.ProdutoId = preMovimentoItem.ProdutoId;
                model.ProdutoUnidadeId = preMovimentoItem.ProdutoUnidadeId;
                model.QuantidadeAtendida = preMovimentoItem.QuantidadeAtendida;

                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(model);
                if (model != null)
                {
                    var produto = await _produtoAppService.Obter(preMovimentoItem.ProdutoId);
                    viewModel.Produto = new ProdutoDto { Id = preMovimentoItem.ProdutoId, Descricao = preMovimentoItem.Produto, Codigo = preMovimentoItem.CodigoProduto, IsLote = produto.IsLote, IsValidade= produto.IsValidade, IsSerie= produto.IsSerie };

                    if(produto.IsValidade || produto.IsLote)
                    {
                        var laboratorios = await _produtoLaboratorioAppService.ListarTodos();
                        viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao");
                    }
                    else
                    {
                        viewModel.Laboratorios = new SelectList(new ListResultDto<ProdutoLaboratorioDto>().Items , "Id", "Descricao");
                    }

                    var unidade = await _unidadeAppService.Obter((long)preMovimentoItem.ProdutoUnidadeId);
                    viewModel.ProdutoUnidade = unidade;

                  //  viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao", model.ProdutoUnidadeId);
                    viewModel.Quantidade = preMovimentoItem.Quantidade;
                    viewModel.IdGrid = preMovimentoItem.IdGrid;
                    viewModel.LotesValidadesJson = preMovimentoItem.LotesValidadesJson;
                    viewModel.NumerosSerieJson = preMovimentoItem.NumerosSerieJson;
                }
            }
            else
            {
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto());
                // viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao");
                viewModel.Unidades = new SelectList(new ListResultDto<Unidade>().Items, "Id", "Descricao");
                viewModel.LotesValidades = new SelectList(new ListResultDto<LoteValidadeDto>().Items, "Id", "Nome");
            }
            viewModel.Id = preMovimentoItem.Id;




          //  viewModel.LotesValidadesJson = 

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoSolicitacoes/_CriarOuEditarPreMovimentoItemModal.cshtml", viewModel);
        }


        [AcceptVerbs("GET", "POST", "PUT")]
        public ActionResult Visualizar(long id)
        {
            var movimentoRelatorio = _preMovimentoAppService.ObterDadosRelatorioSolicitacao(id);

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/Solicitacao.aspx", new RelatorioSolicitacaoModel(movimentoRelatorio));
        }


        public ActionResult VisualizarIndex(long solicitacaoId)
        {
            var movimentoRelatorio = new RelatorioSolicitacaoSaidaModelDto { PreMovimentoId = solicitacaoId };

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/IndexSolicitacao.cshtml", new RelatorioSolicitacaoModel(movimentoRelatorio));
        }


    }
}