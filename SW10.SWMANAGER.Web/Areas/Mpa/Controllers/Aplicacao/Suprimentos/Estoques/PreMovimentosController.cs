﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using DFe.Utils;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NFe.Classes;
using NFe.Classes.Informacoes.Detalhe;
using NFe.Classes.Servicos.Tipos;
using NFe.Servicos;
using NFe.Utils;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NotasFiscais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Entradas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros
{
    public class PreMovimentosController : Controller // Web.Controllers.SWMANAGERControllerBase
    {

        private string arquivoConfiguracao = @"\config_02746015000129.xml"; //criar rotina para buscar pelo cnpj da empresa
        private ConfiguracaoApp _configuracoes = new ConfiguracaoApp();

        private readonly IEstoquePreMovimentoAppService _preMovimentoAppService;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly ITipoMovimentoAppService _tipoMovimentoAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAbpSession AbpSession;
        private readonly IEstoqueAppService _produtoEstoqueAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IEstoqueLoteValidadeAppService _estoqueLoteValidadeAppService;
        private readonly IOrdemCompraAppService _ordemCompraAppService;
        private readonly ICfopAppService _CFOPAppService;
        private readonly ITipoFreteAppService _TipoFreteAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly ICentroCustoAppService _centroCustoAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IEstoquePreMovimentoLoteValidadeAppService _estoquePreMovimentoLoteValidadeAppService;
        private readonly IProdutoLaboratorioAppService _produtoLaboratorioAppService;
        private readonly IEstMovimentoBaixaAppService _estMovimentoBaixaAppService;
        private readonly INotaFiscalAppService _notaFiscalAppService;
        private readonly IEstoqueImportacaoProdutoAppService _estoqueImportacaoProdutoAppService;
        private readonly IContasPagarAppService _contasPagarAppService;

        public PreMovimentosController(
            IEstoquePreMovimentoAppService preMovimentacaoAppService,
            IUserAppService userAppService,
            IAbpSession abpSession,
            IFornecedorAppService fornecedorAppService,
            ITipoMovimentoAppService tipoMovimentoAppService,
            IEstoqueAppService produtoEstoqueAppService,
            IProdutoAppService produtoAppService,
            IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
            IEstoqueLoteValidadeAppService estoqueLoteValidadeAppService,
            IProdutoLaboratorioAppService produtoLaboratorioAppService,
            IOrdemCompraAppService ordemCompraAppService,
            ICfopAppService CFOPAppService,
            ITipoFreteAppService TipoFreteAppService,
            IUnidadeAppService unidadeAppService,
            ICentroCustoAppService centroCustoAppService,
            IPacienteAppService pacienteAppService,
            IEstoquePreMovimentoLoteValidadeAppService estoquePreMovimentoLoteValidadeAppService,
            IEstMovimentoBaixaAppService estMovimentoBaixaAppService,
            INotaFiscalAppService notaFiscalAppService,
            IEstoqueImportacaoProdutoAppService estoqueImportacaoProdutoAppService,
            IContasPagarAppService contasPagarAppService
            )
        {
            _preMovimentoAppService = preMovimentacaoAppService;
            _userAppService = userAppService;
            AbpSession = abpSession;
            _fornecedorAppService = fornecedorAppService;
            _tipoMovimentoAppService = tipoMovimentoAppService;
            _produtoEstoqueAppService = produtoEstoqueAppService;
            _produtoAppService = produtoAppService;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _estoqueLoteValidadeAppService = estoqueLoteValidadeAppService;
            _produtoLaboratorioAppService = produtoLaboratorioAppService;
            _ordemCompraAppService = ordemCompraAppService;
            _CFOPAppService = CFOPAppService;
            _TipoFreteAppService = TipoFreteAppService;
            _unidadeAppService = unidadeAppService;
            _centroCustoAppService = centroCustoAppService;
            _pacienteAppService = pacienteAppService;
            _estoquePreMovimentoLoteValidadeAppService = estoquePreMovimentoLoteValidadeAppService;
            _estMovimentoBaixaAppService = estMovimentoBaixaAppService;
            _notaFiscalAppService = notaFiscalAppService;
            _estoqueImportacaoProdutoAppService = estoqueImportacaoProdutoAppService;
            _contasPagarAppService = contasPagarAppService;
        }

        public async Task<ActionResult> Index()
        {
            var fornecedores = await _fornecedorAppService.ListarTodos();
            var tipoMovimnetacoes = await _tipoMovimentoAppService.Listar(true);
            var model = new PreMovimentoViewModel();
            model.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");
            model.TipoMovimentos = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/Index.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Edit)]
        public async Task<ActionResult> CriarOuEditarModal(long? id)
        {
            // var fornecedores = await _fornecedorAppService.ListarTodos();

            var userId = AbpSession.UserId.Value;
            var userEmpresas = await _userAppService.GetUserEmpresas(userId);
            var tipoMovimentacoes = await _tipoMovimentoAppService.Listar(true);
            //    var estoques = await _produtoEstoqueAppService.ListarTodos();
            var ordens = await _ordemCompraAppService.ListarTodos();
           // var CFOPs = await _CFOPAppService.ListarTodos();
            //   var tiposFretes = await _TipoFreteAppService.ListarTodos();
            var centroCustos = await _centroCustoAppService.ListarTodos();
            // var tipoDocumentos = await _tipoDocumentoAppService.ListarPorEntradaSaida(true);
         //   var pacientes = await _pacienteAppService.ListarTodos();

            CriarOuEditarPreMovimentoModalViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
                //  viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);  //SelectList(empresas.Items, "Id", "NomeFantasia", output.EmpresaId);
                //   viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao", output.FornecedorId);


                if (output.EmpresaId != null && output.EmpresaId != 0)
                {
                    viewModel.Empresa = userEmpresas.Items.Where(w => w.Id == output.EmpresaId).FirstOrDefault();
                }


                viewModel.TipoMovimentos = new SelectList(tipoMovimentacoes.Items, "Id", "Descricao", output.TipoDocumentoId);
                // viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);

                if (output.EstoqueId != null && output.EstoqueId != 0)
                {
                    viewModel.Estoque = await _produtoEstoqueAppService.Obter((long)output.EstoqueId);
                }

                viewModel.Ordens = new SelectList(ordens.Items, "Id", "Descricao", output.OrdemId);
                //  viewModel.TipoFretes = new SelectList(tiposFretes.Items, "Id", "Descricao", output.TipoFreteId);

                if (output.TipoFreteId != null && output.TipoFreteId != 0)
                {
                    viewModel.TipoFrete = await _TipoFreteAppService.Obter((long)output.TipoFreteId);
                }

                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
                viewModel.CentroCustos = new SelectList(centroCustos.Items, "Id", "Descricao", output.CentroCustoId);
               // viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);


                //if (viewModel.FornecedorId != null && viewModel.FornecedorId != 0)
                //{
                //    var fornecedor = await _fornecedorAppService.Obter((long)viewModel.FornecedorId);
                //    viewModel.Fornecedor = new SisFornecedorDto { Id = fornecedor.Id, Descricao = fornecedor.Descricao };
                //}

                //if (viewModel.Frete_FornecedorId != null && viewModel.Frete_FornecedorId != 0)
                //{
                //    var fornecedor = await _fornecedorAppService.Obter((long)viewModel.Frete_FornecedorId);
                //    viewModel.Frete_Fornecedor = new DisFornecedorDto { Id = fornecedor.Id, Descricao = fornecedor.Descricao };
                //}

                if (viewModel.CFOPId != null && viewModel.CFOPId != 0)
                {
                    var CFOP = await _CFOPAppService.Obter((long)viewModel.CFOPId);
                    if (CFOP != null)
                    {
                        viewModel.CFOP = new CfopDto { Id = CFOP.Id, Descricao = CFOP.Descricao, Numero = CFOP.Numero };
                    }
                }

                viewModel.PossuiNota = _estMovimentoBaixaAppService.PossuiNota(output.Id);
                viewModel.PossuiVales = _estMovimentoBaixaAppService.PossuiVales(output.Id);
                viewModel.PossuiItensConsignados = _estMovimentoBaixaAppService.PossuiItemConsignados(output.Id);



                if(output.EstTipoMovimentoId == (long)EnumTipoMovimento.NotaFiscal_Entrada)
                {
                   var fornecedor = await _fornecedorAppService.Obter((long)output.FornecedorId);

                    if (fornecedor != null)
                    {
                       var documento =  await _contasPagarAppService.ObterPorPessoaNumero((long)fornecedor.SisPessoaId, output.Documento);


                        List<LancamentoIndex> lancamentos = new List<LancamentoIndex>();

                        if (documento != null)
                        {

                            foreach (var item in documento.LancamentosDto)
                            {
                                var lancamento = new LancamentoIndex();

                                lancamento.AnoCompetencia = item.AnoCompetencia;
                                lancamento.MesCompetencia = item.MesCompetencia;
                                lancamento.CodigoBarras = item.CodigoBarras;
                                lancamento.Competencia = string.Concat(item.MesCompetencia.ToString().PadLeft(2, '0'), "/", item.AnoCompetencia);
                                lancamento.ValorAcrescimoDecrescimo = item.ValorAcrescimoDecrescimo;
                                lancamento.ValorLancamento = item.ValorLancamento;
                                lancamento.Juros = item.Juros;
                                lancamento.LinhaDigitavel = item.LinhaDigitavel;
                                lancamento.Multa = item.Multa;
                                lancamento.NossoNumero = item.NossoNumero;
                                lancamento.Parcela = item.Parcela;
                                lancamento.SituacaoLancamentoId = item.SituacaoLancamentoId;
                                lancamento.SituacaoDescricao = item.SituacaoDescricao;
                                lancamento.DataVencimento = item.DataVencimento;
                                lancamento.DataLancamento = item.DataLancamento;
                                lancamento.CorLancamentoFundo = item.CorLancamentoFundo;
                                lancamento.CorLancamentoLetra = item.CorLancamentoLetra;
                                lancamento.IdGrid = item.IdGrid;
                                lancamento.Id = item.Id;

                                lancamentos.Add(lancamento);


                            }
                        }

                        viewModel.LancamentosJson = JsonConvert.SerializeObject(lancamentos);
                    }
                }


                

            }
            else //Novo
            {
                viewModel = new CriarOuEditarPreMovimentoModalViewModel(new PreMovimentoViewModel());


                if (userEmpresas.Items.Count == 1)
                {
                    //  var empresaId = userEmpresas.Items.First().Id;
                    // viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", empresaId);

                    viewModel.Empresa = userEmpresas.Items.First();
                    viewModel.EmpresaId = userEmpresas.Items.First().Id;
                }
                //else
                //{
                //    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                //}


                long var;


                viewModel.TipoMovimentos = new SelectList(tipoMovimentacoes.Items, "Id", "Descricao");

                //viewModel.Fornecedores = SelecionarSelectListUnitario<CriarOuEditarFornecedor>(fornecedores.Items, "Id", "Descricao", out var);
                //viewModel.FornecedorId = var;

                //viewModel.TipoDocumentos = SelecionarSelectListUnitario<TipoDocumentoDto>(tipoDocumentos.Items, "Id", "Descricao", out var);
                //viewModel.TipoDocumentoId = var;

                //viewModel.Estoques = SelecionarSelectListUnitario<EstoqueDto>(estoques.Items, "Id", "Descricao", out var);
                //viewModel.EstoqueId = var;

                viewModel.Ordens = SelecionarSelectListUnitario<OrdemCompraDto>(ordens.Items, "Id", "Descricao", out var);
                viewModel.OrdemId = var;

                //viewModel.TipoFretes = SelecionarSelectListUnitario<TipoFreteDto>(tiposFretes.Items, "Id", "Descricao", out var);
                //viewModel.TipoFreteId = var;

                viewModel.CentroCustos = SelecionarSelectListUnitario<CentroCustoDto>(centroCustos.Items, "Id", "Descricao", out var);
                viewModel.CentroCustoId = var;

                //viewModel.Pacientes = SelecionarSelectListUnitario<PacienteDto>(pacientes.Items, "Id", "NomeCompleto", out var);
                //viewModel.PacienteId = var;

                viewModel.Serie = "1";

                viewModel.LancamentosJson = JsonConvert.SerializeObject(new List<LancamentoDto>());
            }


            viewModel.Movimento = DateTime.Now;

            SelecionarSelectList(viewModel);
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_CriarOuEditarModal.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Edit)]
        public async Task<ActionResult> CriarOuEditarPreMovimentoItemModal(long? id, long preMovimentoId = 0)
        {
            CriarOuEditarPreMovimentoItemModalViewModel viewModel;
            var produtos = await _produtoAppService.ListarTodosParaMovimento();

            if (id.HasValue && id.Value != 0)
            {
                var model = await _estoquePreMovimentoItemAppService.Obter(id.Value);
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(model);
                if (model != null)
                {
                    viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao", model.ProdutoId);
                    var unidades = await _produtoAppService.ObterUnidadePorProduto(model.ProdutoId);
                    viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao", model.ProdutoUnidadeId);
                    var unidade = await _unidadeAppService.ObterUnidadeDto((long)model.ProdutoUnidadeId);
                    if (unidade != null)
                    {
                        viewModel.Quantidade = viewModel.Quantidade / unidade.Fator;
                    }
                    viewModel.CustoTotal = viewModel.Quantidade * viewModel.CustoUnitario;
                    viewModel.ValorIPI = (viewModel.CustoTotal * viewModel.PerIPI) / 100;

                    var produto = await _produtoAppService.Obter(model.ProdutoId);
                    if (produto != null)
                    {
                        viewModel.IsNumeroSerie = produto.IsSerie;
                        viewModel.Produto = produto;
                    }

                }
            }
            else
            {
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto());

                long var;

                viewModel.Produtos = SelecionarSelectListUnitario<ProdutoDto>(produtos.Items, "Id", "Descricao", out var);
                viewModel.ProdutoId = var;

                viewModel.Unidades = new SelectList(new ListResultDto<Unidade>().Items, "Id", "Descricao");
            }
            viewModel.PreMovimentoId = preMovimentoId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_CriarOuEditarPreMovimentoItemModal.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Edit)]
        public async Task<ActionResult> InformarLoteValidadeModal(long preMovimentoId)
        {

            var viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto { PreMovimentoId = preMovimentoId });


            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_InformarLoteValidade.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Edit)]
        public async Task<ActionResult> InformarLoteValidadeProdutoModal(long preMovimentoItemId, long produtoId)
        {
            var viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto { PreMovimentoId = preMovimentoItemId, ProdutoId = produtoId });

            var preMovimentoItem = _estoquePreMovimentoItemAppService.Obter(preMovimentoItemId).Result;
            if (preMovimentoItem != null)
            {
                preMovimentoItem.EstoquePreMovimento = await _preMovimentoAppService.Obter(preMovimentoItem.PreMovimentoId);
                if (preMovimentoItem.EstoquePreMovimento != null)
                {
                    viewModel.PreMovimentoEstadoId = preMovimentoItem.EstoquePreMovimento.PreMovimentoEstadoId;
                }

                viewModel.Quantidade = preMovimentoItem.Quantidade;
            }

            var produto = await _produtoAppService.Obter(produtoId);
            if (produto != null)
            {
                viewModel.Produto = produto;
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_InformarLoteValidadeProduto.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_Entrada_Edit)]
        public async Task<ActionResult> CriarOuEditarLoteValidadeModal(long preMovimentoItemId, long produtoLoteValidadeId = 0)
        {
            var viewModel = new CriarOuEditarEstoquePreMovimentoLoteValidadeDtoModalViewModel(new EstoquePreMovimentoLoteValidadeDto());
            var laboratorios = await _produtoLaboratorioAppService.ListarTodos();

            viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao");

            var itemProduto = await _estoquePreMovimentoItemAppService.Obter(preMovimentoItemId);





            if (itemProduto != null)
            {
                viewModel.Id = produtoLoteValidadeId;
                viewModel.ProdutoId = itemProduto.ProdutoId;
                if (itemProduto.Produto != null)
                {
                    viewModel.ProdutoDescricao = itemProduto.Produto.Descricao;

                    var quantidade = await _estoquePreMovimentoLoteValidadeAppService.ObterQuantidadeRestanteLoteValidade(preMovimentoItemId);

                    viewModel.Quantidade = quantidade;
                }

                if (produtoLoteValidadeId != 0)
                {
                    var loteValidade = _estoqueLoteValidadeAppService.Obter(produtoLoteValidadeId).Result;
                    if (loteValidade != null && loteValidade.LoteValidade != null)
                    {
                        // viewModel.ProdutoId = loteValidade.EstoquePreMovimentoItem.ProdutoId;

                        viewModel.Lote = loteValidade.LoteValidade.Lote;
                        viewModel.Validade = loteValidade.LoteValidade.Validade;
                        viewModel.Quantidade = loteValidade.Quantidade;
                        viewModel.LaboratorioId = loteValidade.LoteValidade.ProdutoLaboratorioId;
                        viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao", viewModel.LaboratorioId);
                    }
                }

                viewModel.EstoquePreMovimentoItemId = preMovimentoItemId;
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_CriarOuEditarLoteValidadeModal.cshtml", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SalvarPreMovimentacao(EstoquePreMovimentoDto input)
        {
            var produto = _preMovimentoAppService.CriarOuEditar(input);
            return Json(produto, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SalvarPreMovimentacaoItem(EstoquePreMovimentoItemDto input)
        {
            var produto = _estoquePreMovimentoItemAppService.CriarOuEditar(input);
            return Json(produto, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult EditarPreMovimentoItem(EstoquePreMovimentoItemDto input)
        {
            try
            {
                // _estoquePreMovimentoItemAppService.Editar(input);
                AsyncHelper.RunSync(() => _estoquePreMovimentoItemAppService.Editar(input));
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult ExcluirPreMovimentoItem(long id)
        {
            try
            {
                AsyncHelper.RunSync(() => _estoquePreMovimentoItemAppService.Excluir(id));
                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public async Task<JsonResult> SelecionarUnidades(int id)
        {






            var unidades = await _produtoAppService.ObterUnidadePorProduto(id);
            return Json(unidades, JsonRequestBehavior.AllowGet);
        }


        void SelecionarSelectList(CriarOuEditarPreMovimentoModalViewModel model)
        {
            foreach (var item in model.GetType().GetProperties().Where(w => w.PropertyType == typeof(SelectList)))
            {
                // SelecionarSelectListUnitario((SelectList)item.GetValue(model));
            }
        }

        SelectList SelecionarSelectListUnitario<T>(IReadOnlyList<T> items, string id, string descricao, out long identificador)
        {
            identificador = 0;
            if (items.Count == 1)
            {
                var item = ((List<T>)items)[0];
                var i = item.GetType().GetProperty("Id").GetValue(item);

                long.TryParse(i.ToString(), out identificador);

                return new SelectList(items, id, descricao, identificador);
            }


            return new SelectList(items, id, descricao);
        }


        [AcceptVerbs("GET", "POST", "PUT")]
        public ActionResult Visualizar(long preMovimentoId)
        {

            var movimentoRelatorio = _preMovimentoAppService.ObterDadosRelatorioEntrada(preMovimentoId);

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/Entrada.aspx", new RelatorioEntradaModel(movimentoRelatorio));
        }



        public ActionResult BuscarLotesValidades(long preMovimentoId, string chave, long empresaId)
        {
            List<det> listDets;

            if (!string.IsNullOrEmpty(chave))
            {
                var nf = _notaFiscalAppService.ObterNFeReceita(chave, empresaId).ReturnObject;
                listDets = nf.NFe.infNFe.det;
            }
            else
            {
                listDets = new List<det>();
            }

            var lista = _estoquePreMovimentoLoteValidadeAppService.ObterLotesValidadesPreMovimento(preMovimentoId, listDets);

            InfornacaoLoteValidadeTodosModel model = new InfornacaoLoteValidadeTodosModel();
            model.InformacoesLoteValidade = lista;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_InformacaoLoteValidadeTodosItens.cshtml", model);
        }

        public async Task<JsonResult> BuscarNfe(string chave, long? empresaId)
        {
            var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();


            if (empresaId == null || empresaId == 0)
            {
                _retornoPadrao.Errors.Add(new ErroDto { Descricao = "Selecione uma empresa." });
            }

            if (string.IsNullOrEmpty(chave))
            {
                _retornoPadrao.Errors.Add(new ErroDto { Descricao = "Chave da nota fiscal não informada." });
            }

            if (_retornoPadrao.Errors.Count() > 0)
            {
                return Json(_retornoPadrao, JsonRequestBehavior.AllowGet);
            }


            try
            {

                var retorno = _notaFiscalAppService.ObterNFeReceita(chave, (long)empresaId);





                if (retorno.Errors.Count() > 0)
                {
                    _retornoPadrao.Errors.AddRange(retorno.Errors);
                }
                else
                {


                    if (!string.IsNullOrEmpty(retorno.ReturnObject.NFe.infNFe.emit.CNPJ))
                    {
                        var fornecedorDto = await _fornecedorAppService.ObterPorCNPJ(retorno.ReturnObject.NFe.infNFe.emit.CNPJ);
                        if (fornecedorDto == null)
                        {
                            retorno.Errors.Add(new ErroDto { Descricao = "Fornecedor não cadastrado." });
                            return Json(retorno, JsonRequestBehavior.AllowGet);
                        }
                    }




                    var nf = retorno.ReturnObject;

                    var preMovimento = new EstoquePreMovimentoDto();

                    preMovimento.Documento = nf.NFe.infNFe.ide.nNF.ToString();
                    preMovimento.Emissao = nf.NFe.infNFe.ide.dhEmi;
                    preMovimento.Serie = nf.NFe.infNFe.ide.serie.ToString();
                    preMovimento.ValorICMS = nf.NFe.infNFe.total.ICMSTot.vICMS;
                    preMovimento.TotalDocumento = nf.NFe.infNFe.total.ICMSTot.vNF;
                    preMovimento.TotalProduto = nf.NFe.infNFe.total.ICMSTot.vProd;
                    preMovimento.DescontoPer = nf.NFe.infNFe.total.ICMSTot.vDesc;
                    preMovimento.AcrescimoDecrescimo = nf.NFe.infNFe.total.ICMSTot.vOutro;
                    preMovimento.ValorFrete = nf.NFe.infNFe.total.ICMSTot.vFrete;

                    preMovimento.EstTipoMovimentoId = (long)EnumTipoMovimento.NotaFiscal_Entrada;
                    preMovimento.Movimento = DateTime.Now;

                    var fornecedor = await _fornecedorAppService.ObterPorCNPJ(nf.NFe.infNFe.emit.CNPJ);
                    if (fornecedor != null)
                    {
                        preMovimento.FornecedorId = fornecedor.Id;
                        preMovimento.Fornecedor = new SisFornecedorDto { Id = fornecedor.Id, Descricao = fornecedor.Descricao };
                    }


                    var transportadora = await _fornecedorAppService.ObterPorCNPJ(nf.NFe.infNFe.transp.transporta.CNPJ);
                    if (transportadora != null)
                    {
                        preMovimento.Frete_FornecedorId = transportadora.Id;
                        preMovimento.Frete_Forncedor = new FornecedorDto { Id = transportadora.Id, Descricao = transportadora.Descricao };
                    }

                    //preMovimento.TipoFreteId = (long)nf.NFe.infNFe.transp.modFrete;
                    //var tipoFrete = await _TipoFreteAppService.Obter((long)nf.NFe.infNFe.transp.modFrete);

                    //if (tipoFrete != null)
                    //{
                    //    preMovimento.TipoFrete = new TipoFreteDto { Id = tipoFrete.Id, Descricao = tipoFrete.Descricao };
                    //}

                    preMovimento.CNPJNota = nf.NFe.infNFe.emit.CNPJ;

                    var importacaoProdutos = _estoqueImportacaoProdutoAppService.ObterListaImportacaoProduto(nf);

                    preMovimento.ImportacaoProdutos = importacaoProdutos.Where(w => w.ProdutoId == null).ToList();

                    if (nf.NFe.infNFe.det.Count() > 0)
                    {
                        var cfop = await _CFOPAppService.ObterPorNumero(nf.NFe.infNFe.det[0].prod.CFOP);
                        if (cfop != null)
                        {
                            preMovimento.CFOPId = cfop.Id;
                        }
                    }

                    if (preMovimento.ImportacaoProdutos.Count == 0)
                    {
                        var preMovimentoId = _preMovimentoAppService.CriarGetIdEntrada(preMovimento).Id;
                        preMovimento.Id = preMovimentoId;

                        //foreach (var item in nf.NFe.infNFe.det)
                        foreach (var importacaoProduto in importacaoProdutos)
                        {
                            var preMovimentoItem = new EstoquePreMovimentoItemDto();

                            //     var importacaoProduto = importacaoProdutos.Where(w => w.CodigoProdutoNota == item.prod.cProd).FirstOrDefault();

                            preMovimentoItem.PreMovimentoId = preMovimentoId;
                            preMovimentoItem.Quantidade = importacaoProduto.Quantidade;
                            preMovimentoItem.ProdutoUnidadeId = importacaoProduto.UnidadeId;
                            preMovimentoItem.ProdutoId = (long)importacaoProduto.ProdutoId;
                            preMovimentoItem.CustoUnitario = importacaoProduto.CustoUnitario;

                            var produto = Task.Run(() => _produtoAppService.Obter((long)importacaoProduto.ProdutoId)).Result;

                            if (produto != null && produto.IsSerie)
                            {
                                preMovimentoItem.NumeroSerie = importacaoProduto.Serie;
                            }

                            var preMovimentoItemResult = await _estoquePreMovimentoItemAppService.CriarOuEditar(preMovimentoItem);

                            if (produto != null && (produto.IsValidade || produto.IsLote))
                            {
                                InserirLoteValidade(importacaoProduto, preMovimentoItemResult.ReturnObject);
                            }
                        }

                    }
                    _retornoPadrao.ReturnObject = preMovimento;
                }

            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return Json(_retornoPadrao, JsonRequestBehavior.AllowGet);
        }

        void InserirLoteValidade(EstoqueImportacaoProdutoDto importacaoProduto, EstoquePreMovimentoItemDto preMovimentoItem)
        {


            EstoquePreMovimentoLoteValidadeDto estoquePreMovimentoLoteValidade = new EstoquePreMovimentoLoteValidadeDto();

            estoquePreMovimentoLoteValidade.EstoquePreMovimentoItemId = preMovimentoItem.Id;
            //  estoquePreMovimentoLoteValidade.LaboratorioId = 2;//Alterar
            estoquePreMovimentoLoteValidade.Lote = importacaoProduto.Lote;
            estoquePreMovimentoLoteValidade.Validade = importacaoProduto.Validade;
            estoquePreMovimentoLoteValidade.ProdutoId = (long)importacaoProduto.ProdutoId;
            estoquePreMovimentoLoteValidade.Quantidade = preMovimentoItem.Quantidade;


            _estoqueLoteValidadeAppService.CriarOuEditar(estoquePreMovimentoLoteValidade);



            //var loteValidade = _estoqueLoteValidadeAppService.Obter((long)importacaoProduto.ProdutoId, importacaoProduto.Lote, importacaoProduto.Validade, 2);


        }

        [AcceptVerbs("GET", "POST", "PUT")]
        public ActionResult CarregarRelacionarImportacaoProdutos(string importacaoProdutosRegistrados, long fornecedorId, string CNPJNota) //List<EstoqueImportacaoProdutoDto> importacaoProdutosRegistrados)
        {
            var importacaoProdutos = JsonConvert.DeserializeObject<List<EstoqueImportacaoProdutoDto>>(importacaoProdutosRegistrados);

            int i = 0;
            importacaoProdutos.ForEach(f => f.Index = i++);

            var importacaoProdutosViewModel = new ImportacaoProdutosViewModel { ImportacaoProdutos = importacaoProdutos };
            importacaoProdutosViewModel.FornecedorId = fornecedorId;
            importacaoProdutosViewModel.CNPJNota = CNPJNota;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_ImportacaoProdutos.cshtml", importacaoProdutosViewModel);//  importacaoProdutos);
        }


        private ConfiguracaoApp CarregarConfiguracao()
        {
            var fullPath = @"c:\nfe" + arquivoConfiguracao; // /configuracao.xml");
            try
            {
                ConfiguracaoApp file = new ConfiguracaoApp();
                if (System.IO.File.Exists(fullPath))
                {
                    file = FuncoesXml.ArquivoXmlParaClasse<ConfiguracaoApp>(fullPath);
                    //_configuracoes = file;
                    if (file.CfgServico.TimeOut == 0)
                    {
                        file.CfgServico.TimeOut = 999999; //mínimo
                    }
                    file.CfgServico.DiretorioSchemas = @"c:\nfe\Schemas";
                    file.CfgServico.DiretorioSalvarXml = @"c:\nfe\Xmls";
                    //throw new Exception("acessou pasta " + file.CfgServico.Certificado.Arquivo);


                    file.CfgServico.tpAmb = NFe.Classes.Informacoes.Identificacao.Tipos.TipoAmbiente.taProducao;

                    return file;
                }
                else
                {
                    throw new Exception("sem acesso ao caminho " + fullPath);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message.ToString());
            }
        }

        //public NFe.Classes.nfeProc ObterDetalhesNota(string chave)
        //{
        //    _configuracoes = CarregarConfiguracao();
        //    //CarregaDadosCertificado();
        //    var service = new ServicosNFe(_configuracoes.CfgServico);
        //    var manifest = service.RecepcaoEventoManifestacaoDestinatario(1, 1, chave, TipoEventoManifestacaoDestinatario.TeMdCienciaDaEmissao, _configuracoes.Emitente.CNPJ);
        //    var resultManifest = manifest.Retorno;
        //    var breakPoint = resultManifest.retEvento;
        //    service.Dispose();

        //    service = new ServicosNFe(_configuracoes.CfgServico);
        //    var resultDownload = service.NfeDownloadNf(_configuracoes.Emitente.CNPJ, new List<string> { chave });
        //    //var result = service.NfeConsultaProtocolo(chave);
        //    service.Dispose();

        //    var retConsulta = resultDownload.Retorno.retNFe;
        //    if (retConsulta.Count() == 0)
        //    {
        //        throw new Exception(resultDownload.Retorno.cStat + " - " + resultDownload.Retorno.xMotivo);
        //    }
        //    else
        //    {
        //        NFe.Classes.nfeProc nfeProc = new NFe.Classes.nfeProc();
        //        var result = retConsulta[0].XmlNfe;
        //        if (result != null)
        //        {
        //            foreach (var propriedade in result.LerPropriedades())
        //            {
        //                if (propriedade.Key.Equals("nfeProc"))
        //                {
        //                    var a = propriedade.Value.As<NFe.Classes.nfeProc>();
        //                    nfeProc = a;
        //                    break;
        //                }
        //            }
        //        }
        //        return nfeProc;
        //    }
        //}



        public JsonResult Salvar(string input)
        {
            try
            {
                var preMovimento = JsonConvert.DeserializeObject<EstoquePreMovimentoDto>(input, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                var result = _preMovimentoAppService.CriarOuEditar(preMovimento);

               return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                // var mensagem = ex.Message;
                // var erros = JsonConvert.DeserializeObject<List<ErroDto>>(mensagem);

                //var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
                // _retornoPadrao.Warnings = new List<ErroDto>();

                // _retornoPadrao.Errors = erros;

                // return Json(_retornoPadrao, JsonRequestBehavior.AllowGet);

                ex.Source = ex.Message;

               // throw (ex);

            }

            return null;
        }
    }
}