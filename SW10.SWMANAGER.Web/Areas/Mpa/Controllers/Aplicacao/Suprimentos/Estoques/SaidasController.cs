﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
//using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.TipoMovimentacoes;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Cadastros
{
    public class SaidasController : Controller // Web.Controllers.SWMANAGERControllerBase
    {
        private readonly IEstoquePreMovimentoAppService _preMovimentoAppService;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly ITipoMovimentoAppService _tipoMovimentoAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAbpSession AbpSession;
        private readonly IEstoqueAppService _produtoEstoqueAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IEstoqueLoteValidadeAppService _estoqueLoteValidadeAppService;
        private readonly IOrdemCompraAppService _ordemCompraAppService;
        private readonly ICfopAppService _CFOPAppService;
        private readonly ITipoFreteAppService _TipoFreteAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly ICentroCustoAppService _centroCustoAppService;
        private readonly IProdutoLaboratorioAppService _produtoLaboratorioAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IMotivoPerdaProdutoAppService _motivoPerdaProdutoAppService;


        public SaidasController(
            IEstoquePreMovimentoAppService preMovimentacaoAppService,
            IUserAppService userAppService,
            IAbpSession abpSession,
            IFornecedorAppService fornecedorAppService,
            IEstoqueAppService produtoEstoqueAppService,
            IProdutoAppService produtoAppService,
            IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
            IEstoqueLoteValidadeAppService estoqueLoteValidadeAppService,
            IProdutoLaboratorioAppService produtoLaboratorioAppService,
            IOrdemCompraAppService ordemCompraAppService,
            ICfopAppService CFOPAppService,
            ITipoFreteAppService TipoFreteAppService,
            IUnidadeAppService unidadeAppService,
            ICentroCustoAppService centroCustoAppService,
            IPacienteAppService pacienteAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            IMedicoAppService medicoAppService,
            IAtendimentoAppService atendimentoAppService,
            IMotivoPerdaProdutoAppService motivoPerdaProdutoAppService,
            ITipoMovimentoAppService tipoMovimentoAppService
            )
        {
            _preMovimentoAppService = preMovimentacaoAppService;
            _userAppService = userAppService;
            AbpSession = abpSession;
            _fornecedorAppService = fornecedorAppService;
            _produtoEstoqueAppService = produtoEstoqueAppService;
            _produtoAppService = produtoAppService;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _estoqueLoteValidadeAppService = estoqueLoteValidadeAppService;
            _produtoLaboratorioAppService = produtoLaboratorioAppService;
            _ordemCompraAppService = ordemCompraAppService;
            _CFOPAppService = CFOPAppService;
            _TipoFreteAppService = TipoFreteAppService;
            _unidadeAppService = unidadeAppService;
            _centroCustoAppService = centroCustoAppService;
            // _tipoDocumentoAppService = tipoDocumentoAppService;
            _pacienteAppService = pacienteAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _medicoAppService = medicoAppService;
            _atendimentoAppService = atendimentoAppService;
            _motivoPerdaProdutoAppService = motivoPerdaProdutoAppService;
            _tipoMovimentoAppService = tipoMovimentoAppService;
        }
        public async Task<ActionResult> Index()
        {
            var tipoMovimnetacoes = await _tipoMovimentoAppService.Listar(false);
            var fornecedores = await _fornecedorAppService.ListarTodos();
            var model = new PreMovimentoViewModel();
            model.TipoMovimentos = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");
            model.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");

            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Saidas/Index.cshtml", model);
        }



        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_SaidaProduto_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_SaidaProduto_Edit)]
        public async Task<ActionResult> CriarOuEditarModal(long? id)
        {
          //  var fornecedores = await _fornecedorAppService.ListarTodos();

            var userId = AbpSession.UserId.Value;
         //   var userEmpresas = await _userAppService.GetUserEmpresas(userId);
         //   var tipoMovimnetacoes = await _tipoMovimentoAppService.Listar(false);
          //  var estoques = await _produtoEstoqueAppService.ListarTodos();
           // var pacientes = await _pacienteAppService.ListarTodos();
            //   var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();
            var medicos = await _medicoAppService.ListarTodos();
         //   var atendimentos = await _atendimentoAppService.ListarAtendimentoPaciente();
            var motivosPerdas = await _motivoPerdaProdutoAppService.ListarTodos();
            var tiposAtendimentos = new ListResultDto<GenericoIdNome>
            {
                Items = new List<GenericoIdNome> { new GenericoIdNome { Id = 0, Nome = "Ambulatório/Emergência" },
                                                                                                           new GenericoIdNome { Id = 1, Nome = "Internação" }}
            };


            CriarOuEditarPreMovimentoModalViewModel viewModel;

            if (id.HasValue) //edição
            {
                EstoquePreMovimentoDto output = await _preMovimentoAppService.Obter((long)id);

                viewModel = new CriarOuEditarPreMovimentoModalViewModel(output);
              //  viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", output.EmpresaId);
               // viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao", output.FornecedorId);
                // viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao", output.EstoqueId);

                if (output.EstoqueId != null && output.EstoqueId != 0)
                {
                    viewModel.Estoque = await _produtoEstoqueAppService.Obter((long)output.EstoqueId);
                }


                viewModel.PermiteConfirmacaoEntrada = await _preMovimentoAppService.PermiteConfirmarEntrada(output);
               // viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", output.PacienteId);
                //   viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao", output.UnidadeOrganizacionalId);
                viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto", output.MedicoSolcitanteId);
              //  viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Nome", output.AtendimentoId);
              //  viewModel.SaidaPor = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao", output.EstTipoMovimentoId);
                viewModel.MotivosPerda = new SelectList(motivosPerdas.Items, "Id", "Descricao", output.MotivoPerdaProdutoId);
                if (output.AtendimentoId != null && output.AtendimentoId != 0)
                {
                    var atendimento = await _atendimentoAppService.Obter((long)output.AtendimentoId);
                    viewModel.Atendimento = new AtendimentoDto { Id = atendimento.Id, Codigo = atendimento.Codigo, Paciente = new PacienteDto { NomeCompleto = atendimento.Paciente.NomeCompleto } };


                    viewModel.TiposAtendimentos = new SelectList(tiposAtendimentos.Items, "Id", "Nome", atendimento.IsAmbulatorioEmergencia ? 0 : 1);
                }
                else
                {
                    viewModel.TiposAtendimentos = new SelectList(tiposAtendimentos.Items, "Id", "Nome");
                }

                if (output.UnidadeOrganizacionalId != null && output.UnidadeOrganizacionalId != 0)
                {
                    var unidadeOrganizacional = await _unidadeOrganizacionalAppService.ObterPorId((long)output.UnidadeOrganizacionalId);

                    if (unidadeOrganizacional != null)
                    {
                        viewModel.UnidadeOrganizacional = new UnidadeOrganizacionalDto { Id = unidadeOrganizacional.Id, Descricao = unidadeOrganizacional.Descricao };
                    }
                }
            }
            else //Novo
            {
                viewModel = new CriarOuEditarPreMovimentoModalViewModel(new PreMovimentoViewModel());

                //if (userEmpresas.Items.Count == 1)
                //{
                //    viewModel.EmpresaId = userEmpresas.Items.First().Id;
                //    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia", viewModel.EmpresaId);
                //}
                //else
                //{
                //    viewModel.Empresas = new SelectList(userEmpresas.Items, "Id", "NomeFantasia");
                //}
               // viewModel.Fornecedores = new SelectList(fornecedores.Items, "Id", "Descricao");
             //   viewModel.Estoques = new SelectList(estoques.Items, "Id", "Descricao");
            //    viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto");
                //  viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items, "Id", "Descricao");
                viewModel.Medicos = new SelectList(medicos.Items, "Id", "NomeCompleto");
              //  viewModel.Atendimentos = new SelectList(atendimentos.Items, "Id", "Nome");
              //  viewModel.SaidaPor = new SelectList(tipoMovimnetacoes.Items, "Id", "Descricao");
                viewModel.MotivosPerda = new SelectList(motivosPerdas.Items, "Id", "Descricao");
                viewModel.TiposAtendimentos = new SelectList(tiposAtendimentos.Items, "Id", "Nome");

            }

            viewModel.Movimento = DateTime.Now;
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Saidas/_CriarOuEditarModal.cshtml", viewModel);
        }



        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Estoque_SaidaProduto_Create, AppPermissions.Pages_Tenant_Suprimentos_Estoque_SaidaProduto_Edit)]
        public async Task<ActionResult> CriarOuEditarPreMovimentoItemModal(long? id, long preMovimentoId = 0, long estoqueId = 0)
        {
            CriarOuEditarPreMovimentoItemModalViewModel viewModel;
            var produtos = await _produtoAppService.ListarTodosParaMovimento();
            var laboratorios = await _produtoLaboratorioAppService.ListarTodos();

            if (id.HasValue && id.Value != 0)
            {
                var model = await _estoquePreMovimentoItemAppService.Obter(id.Value);
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(model);
                if (model != null)
                {
                    viewModel.Hidden = (model.Produto.IsValidade || model.Produto.IsLote) ? "" : "hidden";

                    viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao", model.ProdutoId);
                    var unidades = await _produtoAppService.ObterUnidadePorProduto(model.ProdutoId);
                    viewModel.Unidades = new SelectList(unidades.Items, "Id", "Descricao", model.ProdutoUnidadeId);

                    var unidade = await _unidadeAppService.ObterUnidadeDto((long)model.ProdutoUnidadeId);
                    if (unidade != null)
                    {
                        viewModel.Quantidade = viewModel.Quantidade / unidade.Fator;
                    }


                    var EstoquePreMovimentoLoteValidadeResult = await _estoqueLoteValidadeAppService.ListarPorPreMovimentoItem(new ListarEstoquePreMovimentoInput { Filtro = model.Id.ToString() });
                    if (EstoquePreMovimentoLoteValidadeResult != null && EstoquePreMovimentoLoteValidadeResult.Items.Count() > 0)
                    {
                        var loteValidate = EstoquePreMovimentoLoteValidadeResult.Items[0].LoteValidade;
                        viewModel.LaboratorioId = loteValidate.ProdutoLaboratorioId;
                        viewModel.Lote = loteValidate.Lote;
                        viewModel.Validade = loteValidate.Validade;
                        viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao", loteValidate.ProdutoLaboratorioId);
                        viewModel.LoteValidadeId = loteValidate.Id;
                        viewModel.EstoquePreMovimentoLoteValidadeId = EstoquePreMovimentoLoteValidadeResult.Items[0].Id;

                        var lotesValidades = await _estoqueLoteValidadeAppService.ObterPorProdutoEstoque(model.ProdutoId, estoqueId, preMovimentoId);

                        viewModel.LotesValidades = new SelectList(lotesValidades, "Id", "Nome", loteValidate.Id);
                    }
                    else
                    {
                        viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Nome");
                        viewModel.LotesValidades = new SelectList(new ListResultDto<LoteValidadeDto>().Items, "Id", "Nome");
                    }
                }
            }
            else
            {
                viewModel = new CriarOuEditarPreMovimentoItemModalViewModel(new EstoquePreMovimentoItemDto());
                viewModel.Produtos = new SelectList(produtos.Items, "Id", "Descricao");
                viewModel.Unidades = new SelectList(new ListResultDto<Unidade>().Items, "Id", "Descricao");
                viewModel.Laboratorios = new SelectList(laboratorios.Items, "Id", "Descricao");
                viewModel.LotesValidades = new SelectList(new ListResultDto<LoteValidadeDto>().Items, "Id", "Nome");
            }
            viewModel.Id = id.Value;
            viewModel.PreMovimentoId = preMovimentoId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/Saidas/_CriarOuEditarPreMovimentoItemModal.cshtml", viewModel);
        }


        public async Task<JsonResult> SelecionarLotesValidades(long produtoId, long estoqueId, long preMovimentoId)
        {
            var lotesValidades = await _estoqueLoteValidadeAppService.ObterPorProdutoEstoque(produtoId, estoqueId, preMovimentoId);
            return Json(lotesValidades, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SelecionarAtendimento(long id)
        {
            var Atendimento = await _atendimentoAppService.Obter(id);
            return Json(Atendimento, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CarregarNumerosSeries(long produtoId, long estoqueId)
        {
            var numerosSeries = await _estoquePreMovimentoItemAppService.ObterNumerosSerieProduto(estoqueId, produtoId);
            return Json(numerosSeries, JsonRequestBehavior.AllowGet);
        }

    }
}