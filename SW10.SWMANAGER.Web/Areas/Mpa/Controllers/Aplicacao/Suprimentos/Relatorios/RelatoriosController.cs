﻿using Abp.Web.Mvc.Authorization;
using Microsoft.Reporting.WebForms;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Relatorios;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Relatorios;
using SW10.SWMANAGER.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Suprimentos.Relatorios
{
    public class RelatoriosController : Controller
    {
        private IRelatorioSuprimentoAppService IRelatorioSuprimentoAppService;
        public RelatoriosController(IRelatorioSuprimentoAppService _IRelatorioSuprimentoAppService)
        {
            IRelatorioSuprimentoAppService = _IRelatorioSuprimentoAppService;
        }

        /// <summary>
        /// Entrada para filtro de visualização do Report de produtos
        /// </summary>
        /// <returns></returns>
        //GET: Mpa/Relatorios/SaldoProduto
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto)]
        public async Task<ActionResult> SaldoProduto()
        {
            FiltroModel result = await CarregarIndex();
            result.EhMovimentacao = false;
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/Index.csHtml", result);
        }

        /// <summary>
        /// Entrada para filtro e visualização do Report de Movimentação
        /// </summary>
        /// <returns></returns>
        //GET: Mpa/Relatorios/MovimentacaoProduto
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public async Task<ActionResult> MovimentacaoProduto()
        {
            FiltroModel result = await CarregarIndex();
            result.EhMovimentacao = true;
            return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/Index.csHtml", result);
        }

        /// <summary>
        /// PartialView que renderiza o relatório com os filtros selecionados no formulário
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public ActionResult Visualizar(FiltroModel filtro)
        {
            if (filtro.EhMovimentacao)
            {
                ProcessarDadosMovimentacao(filtro);
                return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/Movimentacao.aspx", filtro);
            }
            else
            {
                ProcessarDadosProduto(filtro);
                return View("~/Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/SaldoProduto.aspx", filtro);
            }
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public ActionResult Exportar(FiltroModel filtro, string formato)
        {
            if (formato != "Word" && formato != "Excel" && formato != "PDF")
            {
                throw new Exception();
            }

            ReportDataSource rd;
            LocalReport report;
            string mimeType;
            string encoding;
            string filenameExtension;
            string[] streams;
            Warning[] warnings;

            if (filtro.EhMovimentacao)
            {
                ProcessarDadosMovimentacao(filtro);
                report = new LocalReport()
                {
                    ReportPath = "Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/Movimentacao.rdlc"
                };
                rd = new ReportDataSource("DataSet1", filtro.DadosMovimentacao);
            }
            else
            {
                ProcessarDadosProduto(filtro);
                report = new LocalReport()
                {
                    ReportPath = "Areas/Mpa/Views/Aplicacao/Suprimentos/Relatorios/ExtratoProdutos.rdlc"
                };
                rd = new ReportDataSource("DataSet1", filtro.Dados);
            }

            report.DataSources.Add(rd);
            var bytes = report.Render(formato, "", out mimeType, out encoding, out filenameExtension, out streams, out warnings);
            string extensionType = string.Empty;
            switch (formato)
            {
                case "Word":
                    extensionType = ".doc";
                    break;
                case "Excel":
                    extensionType = ".xls";
                    break;
                default:
                    extensionType = ".pdf";
                    break;
            }
            string fileName = string.Concat("RELATORIO", extensionType);
            return File(bytes, mimeType, fileName);
        }

        /// <summary>
        /// Listar GrupoClasse com base do Grupo Selecionado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public async Task<JsonResult> ListarGrupoClasse(int id)
        {
            var result = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Selecione" } };

            result.AddRange((await IRelatorioSuprimentoAppService.Listar(new Grupo { Id = id }))
                .Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.Nome
                }));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public async Task<JsonResult> ListarGrupoSubClasse(int id)
        {
            var result = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Selecione" } };

            result.AddRange((await IRelatorioSuprimentoAppService.Listar(new GrupoClasse { Id = id }))
                .Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.Nome
                }));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private async Task<FiltroModel> CarregarIndex()
        {
            var result = new FiltroModel();
            result.Grupos = (await IRelatorioSuprimentoAppService.Listar())
                .Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.Nome
                }).ToList();
            var padrao = new SelectListItem { Text = "Selecione", Value = "0" };
            result.Grupos.Insert(0, padrao);
            result.Default = new List<SelectListItem> { padrao };
            return result;
        }

        private void ProcessarDadosProduto(FiltroModel filtro)
        {
            var db = new Core.DataSetReportsTableAdapters.ReportProdutosTableAdapter();

            var grupo = filtro.GrupoProduto.GetValueOrDefault();
            var classe = filtro.Classe.GetValueOrDefault();
            var subClasse = filtro.SubClasse.GetValueOrDefault();
            if (grupo != 0 && classe == 0 && subClasse == 0)
            {
                filtro.Dados = db.GetDataByGrupoId(grupo).ToList();
            }
            else if (grupo != 0 && classe != 0 && subClasse == 0)
            {
                filtro.Dados = db.GetDataByGrupoClasseId(grupo, classe).ToList();
            }
            else if (grupo != 0 && classe != 0 && subClasse != 0)
            {
                filtro.Dados = db.GetDataBy(grupo, classe, subClasse).ToList();
            }
            else
            {
                filtro.Dados = db.GetData().ToList();
            }
        }

        private void ProcessarDadosMovimentacao(FiltroModel filtro)
        {
            var db = new Core.DataSetReportsTableAdapters.RelatorioMovimentoAdapter();

            var grupo = filtro.GrupoProduto.GetValueOrDefault();
            var classe = filtro.Classe.GetValueOrDefault();
            var subClasse = filtro.SubClasse.GetValueOrDefault();
            var query = db.GetData().Where(w => w.GrupoId == grupo);

            if (classe != 0)
            {
                query = query.Where(w => w.GrupoClasseId == classe);
            }

            if (subClasse != 0)
            {
                query = query.Where(w => w.GrupoSubClasseId == subClasse);
            }

            filtro.DadosMovimentacao = query.ToList();
        }
    }
}