﻿using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Interface;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Financeiros;
using SW10.SWMANAGER.Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Financeiros
{
    public class QuitacaoController : SWMANAGERControllerBase
    {
        // GET: Mpa/Agendamentos
        private readonly ILancamentoAppService _lancamentoAppService;

        public QuitacaoController(ILancamentoAppService lancamentoAppService)
        {
            _lancamentoAppService = lancamentoAppService;
        }


        public async Task<ActionResult> CriarOuEditarModalPorLancamentos(List<long> ids)
        {
            QuitacaoViewModel viewModel = null;
            viewModel = new QuitacaoViewModel(new QuitacaoDto());


            List<QuitacaoIndex> lancamentosIndex = new List<QuitacaoIndex>();
            var lancamentosDto = _lancamentoAppService.ObterLancamentos(ids);


            #region Lista Lançamentos
            long idGrid = 0;

            foreach (var item in lancamentosDto)
            {
                var quitacaoIndex = new QuitacaoIndex();

                quitacaoIndex.LancamentoId = item.Id;
                quitacaoIndex.DataVencimento = item.DataVencimento;
                //lancamentoIndex.Juros = item.Juros;
                //lancamentoIndex.Multa = item.Multa;
                //lancamentoIndex.ValorAcrescimoDecrescimo = item.ValorAcrescimoDecrescimo;
                quitacaoIndex.ValorLancamento = item.ValorLancamento;
                quitacaoIndex.ValorRestante = item.ValorRestante;
                quitacaoIndex.ValorRestanteEfetivado = item.ValorRestante;

                quitacaoIndex.Parcela = item.Parcela;
                quitacaoIndex.Documento = item.Documento.Numero;
                quitacaoIndex.Fornecedor = (item.Documento.Pessoa.FisicaJuridica == "F") ? item.Documento.Pessoa.NomeCompleto : item.Documento.Pessoa.NomeFantasia;
                quitacaoIndex.IdGrid = idGrid++;

                lancamentosIndex.Add(quitacaoIndex);
            }

            #endregion





            viewModel.LancamentosJson = JsonConvert.SerializeObject(lancamentosIndex);

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Financeiros/ContasPagar/_CriarOuEditarModalQuitacaoLancamentos.cshtml", viewModel);
        }

    }
}