﻿using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Interface;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Financeiros;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Financeiros.ContasReceber;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Financeiros
{
    public class ContasReceberController : SWMANAGERControllerBase
    {


        private readonly IContasReceberAppService _contasPagarAppService;

        public ContasReceberController(IContasReceberAppService contasPagarAppService)
        {
            _contasPagarAppService = contasPagarAppService;
        }
        // GET: Mpa/Agendamentos

        public ActionResult Index()
        {
            var model = new ContasPagarViewModel(new DocumentoDto());
            return View("~/Areas/Mpa/Views/Aplicacao/Financeiros/ContasReceber/Index.cshtml", model);
        }

        public async Task<ActionResult> CriarOuEditarModal(long? id)
        {
            ContasPagarViewModel viewModel = null;
            if (id == null || id == 0)
            {
                viewModel = new ContasPagarViewModel(new DocumentoDto());
                viewModel.LancamentosJson = JsonConvert.SerializeObject(new List<LancamentoDto>());
                viewModel.RateioJson = JsonConvert.SerializeObject(new List<DocumentoRateioIndex>());
            }
            else
            {
                var documento = await _contasPagarAppService.ObterPorLancamento((long)id);

                viewModel = new ContasPagarViewModel(documento);
                List<LancamentoIndex> lancamentosIndex = new List<LancamentoIndex>();
                List<DocumentoRateioIndex> rateiosIndex = new List<DocumentoRateioIndex>();

                #region Lista Lançamentos

                foreach (var item in documento.LancamentosDto)
                {
                    var lancamentoIndex = new LancamentoIndex();

                    lancamentoIndex.Id = item.Id;
                    lancamentoIndex.AnoCompetencia = item.AnoCompetencia;
                    lancamentoIndex.CodigoBarras = item.CodigoBarras;
                    lancamentoIndex.CorLancamentoFundo = item.SituacaoLancamento.CorLancamentoFundo;
                    lancamentoIndex.CorLancamentoLetra = item.SituacaoLancamento.CorLancamentoLetra;
                    lancamentoIndex.DataLancamento = item.DataLancamento;
                    lancamentoIndex.DataVencimento = item.DataVencimento;
                    lancamentoIndex.Juros = item.Juros;
                    lancamentoIndex.LinhaDigitavel = item.LinhaDigitavel;
                    lancamentoIndex.MesCompetencia = item.MesCompetencia;
                    lancamentoIndex.Multa = item.Multa;
                    lancamentoIndex.NossoNumero = item.NossoNumero;
                    lancamentoIndex.Parcela = item.Parcela;
                    lancamentoIndex.SituacaoDescricao = item.SituacaoDescricao;
                    lancamentoIndex.SituacaoLancamentoId = item.SituacaoLancamentoId;
                    lancamentoIndex.ValorAcrescimoDecrescimo = item.ValorAcrescimoDecrescimo;
                    lancamentoIndex.ValorLancamento = item.ValorLancamento;
                    lancamentoIndex.IsSelecionado = item.IsSelecionado;
                    lancamentoIndex.IdGrid = item.IdGrid;

                    lancamentosIndex.Add(lancamentoIndex);
                }

                #endregion


                long idGrid = 0;
                foreach (var item in documento.DocumentosRateiosDto)
                {
                    var documentoRateioIndex = new DocumentoRateioIndex();

                    documentoRateioIndex.Id = item.Id;
                    documentoRateioIndex.CentroCustoId = item.CentroCustoId;
                    documentoRateioIndex.CentroCustoDescricao = string.Concat(item.CentroCusto.Codigo, " - ", item.CentroCusto.Descricao);
                    documentoRateioIndex.ContaAdministrativaId = item.ContaAdministrativaId;
                    documentoRateioIndex.ContaAdministrativaDescricao = string.Concat(item.ContaAdministrativa.Codigo, " - ", item.ContaAdministrativa.Descricao);
                    documentoRateioIndex.EmpresaId = item.EmpresaId;
                    documentoRateioIndex.EmpresaDescricao = string.Concat(item.Empresa.Codigo, " - ", item.Empresa.NomeFantasia);
                    documentoRateioIndex.Valor = item.Valor;
                    documentoRateioIndex.IsImposto = item.IsImposto;
                    documentoRateioIndex.Observacao = item.Observacao;
                    documentoRateioIndex.IdGrid = idGrid++;
                    rateiosIndex.Add(documentoRateioIndex);
                }

                viewModel.ValorTotalParcelas = documento.LancamentosDto.Sum(s => s.ValorLancamento);
                viewModel.ValorTotalRateio = documento.DocumentosRateiosDto.Sum(s => s.Valor);


                viewModel.LancamentosJson = JsonConvert.SerializeObject(lancamentosIndex);
                viewModel.RateioJson = JsonConvert.SerializeObject(rateiosIndex);
            }


            return View("~/Areas/Mpa/Views/Aplicacao/Financeiros/ContasReceber/_CriarOuEditarModal.cshtml", viewModel);
        }
    }
}