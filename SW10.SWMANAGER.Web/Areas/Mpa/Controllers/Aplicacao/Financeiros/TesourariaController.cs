﻿using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Financeiros.Tesouraria;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Financeiros
{
    public class TesourariaController : SWMANAGERControllerBase
    {
        // GET: Mpa/Agendamentos
        public ActionResult Index()
        {
            var viewModel = new TesourariaViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Financeiros/Tesouraria/Index.cshtml", viewModel);
        }
    }
}