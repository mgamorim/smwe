﻿using Abp.Collections.Extensions;
using Abp.Extensions;
using MoreLinq;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Intervalos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos
{
    public class AgendamentoConsultasController : SWMANAGERControllerBase
    {
        private readonly IAgendamentoConsultaAppService _agendamentoConsultaAppService;
        private readonly IAgendamentoConsultaMedicoDisponibilidadeAppService _agendamentoConsultaMedicoDisponibilidadeAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IMedicoEspecialidadeAppService _medicoEspecialidadeAppService;
        private readonly IEspecialidadeAppService _especialidadeAppService;
        private readonly IIntervaloAppService _intervaloAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly IPlanoAppService _planoAppService;

        public AgendamentoConsultasController(
            IAgendamentoConsultaAppService agedamentoConsultaAppService,
            IAgendamentoConsultaMedicoDisponibilidadeAppService agendamentoConsultaMedicoDisponibilidadeAppService,
            IEspecialidadeAppService especialidadeAppService,
            IMedicoAppService medicoAppService,
            IMedicoEspecialidadeAppService medicoEspecialidadeAppService,
            IIntervaloAppService intervaloAppService,
            IPacienteAppService pacienteAppService,
            IPlanoAppService planoAppService,
            IConvenioAppService convenioAppService
            )
        {
            _agendamentoConsultaAppService = agedamentoConsultaAppService;
            _agendamentoConsultaMedicoDisponibilidadeAppService = agendamentoConsultaMedicoDisponibilidadeAppService;
            _medicoAppService = medicoAppService;
            _medicoEspecialidadeAppService = medicoEspecialidadeAppService;
            _especialidadeAppService = especialidadeAppService;
            _intervaloAppService = intervaloAppService;
            _pacienteAppService = pacienteAppService;
            _convenioAppService = convenioAppService;
            _planoAppService = planoAppService;
        }

        // GET: Mpa/Agendamentos
        public async Task<ActionResult> Index()
        {
            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarAtivos(0, 0);
            var especialidadesAgendadas = agendamentos.Select(m => m.MedicoEspecialidade.EspecialidadeId).Distinct().ToList();
            var especialidades = await _especialidadeAppService.Listar(especialidadesAgendadas);

            var viewModel = new AgendamentoConsultasViewModel();

            viewModel.Especialidades = new SelectList(especialidades.Items, "Id", "Nome");

            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/Index.cshtml", viewModel);
        }

        public PartialViewResult _CriarModal(DateTime date)
        {
            var model = new CriarOuEditarAgendamentoConsulta();
            //var pacientes = await _pacienteAppService.ListarTodos();
            //var convenios = await _convenioAppService.ListarTodos();
            //var planos = await _planoAppService.ListarTodos();
            var viewModel = new CriarOuEditarAgendamentoConsultaModal(model);
            viewModel.DataAgendamento = date;
            if (!date.ToString("HH:mm").Equals("00:00"))
            {
                viewModel.HoraAgendamento = date;
            }

            //viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", model.PacienteId);
            //viewModel.Convenios = new SelectList(convenios.Items, "Id", "NomeFantasia");
            //viewModel.Planos = new SelectList(planos.Items, "Id", "Descricao");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _EditarModal(long id)
        {
            var model = await _agendamentoConsultaAppService.Obter(id);
            //var pacientes = await _pacienteAppService.ListarTodos();
            //var convenios = await _convenioAppService.ListarTodos();
            //var planos = await _planoAppService.ListarTodos();
            var viewModel = new CriarOuEditarAgendamentoConsultaModal(model);
            //viewModel.Pacientes = new SelectList(pacientes.Items, "Id", "NomeCompleto", model.PacienteId);
            //viewModel.Convenios = new SelectList(convenios.Items, "Id", "NomeFantasia");
            //viewModel.Planos = new SelectList(planos.Items, "Id", "Descricao");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _ListarMedicoDisponibilidades(long? medicoId, long? especialidadeId)
        {
            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarAtivos(medicoId, especialidadeId);
            var medicos = agendamentos.Select(m => m.Medico).DistinctBy(m => m.Id).ToList();
            var viewModel = new List<ListarMedicoEspecialidadesViewModel>();
            foreach (var _medicoId in medicos)
            {
                var _viewModel = new ListarMedicoEspecialidadesViewModel();
                var diasSemana = string.Empty;
                var listAgendamentos = new List<AgendamentoViewModel>();
                _viewModel.Medico = _medicoId; //agendamentosFiltro.Where(m => m.MedicoId == _medicoId).FirstOrDefault().Medico;

                var medicoEspecialidades = agendamentos.Where(m => m.MedicoId == _medicoId.Id).Select(m => m.MedicoEspecialidade).DistinctBy(m => m.Id).ToList(); // medicosEspecialidades.Select(m=>m.esp.Where(m=>m. //agendamentosFiltro.Where(m => m.MedicoId == _medicoId).Select(m => m.MedicoEspecialidade.EspecialidadeId).Distinct();
                foreach (var _medicoEspecialidade in medicoEspecialidades)
                {
                    var _agendamento = new AgendamentoViewModel();
                    var _medicoEspecialidadeId = _medicoEspecialidade.Id;  //_viewModel.Medico.MedicoEspecialidades.Where(m => m.EspecialidadeId == _medicoEspecialidade.Id).FirstOrDefault().Id;
                    var _agendamentoFiltro = agendamentos.Where(m => m.MedicoId == _medicoId.Id && m.MedicoEspecialidadeId == _medicoEspecialidadeId).FirstOrDefault();
                    _agendamento.Especialidade = _agendamentoFiltro.MedicoEspecialidade.Especialidade;
                    _agendamento.IntervaloMinutos = _agendamentoFiltro.Intervalo.IntervaloMinutos;
                    _agendamento.Horarios = _agendamentoFiltro.HoraInicio.ToString("HH:mm") + " - " + _agendamentoFiltro.HoraFim.ToString("HH:mm");
                    if (_agendamentoFiltro.Domingo)
                    {
                        diasSemana += diasSemana.IndexOf(L("Dom")) == -1 ? L("Dom") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Dom");
                    }
                    if (_agendamentoFiltro.Segunda)
                    {
                        diasSemana += diasSemana.IndexOf(L("Seg")) == -1 ? L("Seg") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Seg");
                    }
                    if (_agendamentoFiltro.Terca)
                    {
                        diasSemana += diasSemana.IndexOf(L("Ter")) == -1 ? L("Ter") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Ter");
                    }
                    if (_agendamentoFiltro.Quarta)
                    {
                        diasSemana += diasSemana.IndexOf(L("Qua")) == -1 ? L("Qua") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Qua");
                    }
                    if (_agendamentoFiltro.Quinta)
                    {
                        diasSemana += diasSemana.IndexOf(L("Qui")) == -1 ? L("Qui") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Qui");
                    }
                    if (_agendamentoFiltro.Sexta)
                    {
                        diasSemana += diasSemana.IndexOf(L("Sex")) == -1 ? L("Sex") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Sex");
                    }
                    if (_agendamentoFiltro.Sabado)
                    {
                        diasSemana += diasSemana.IndexOf(L("Sab")) == -1 ? L("Sab") + ", " : string.Empty;
                        _agendamento.DiasSemana += L("Sab");
                    }
                    _agendamento.DiasSemana = _agendamento.DiasSemana.Substring(0, _agendamento.DiasSemana.Length - 2);

                    listAgendamentos.Add(_agendamento);
                }
                _viewModel.Agendamentos = listAgendamentos;
                diasSemana = diasSemana.Substring(0, diasSemana.Length - 2);
                _viewModel.DiasSemana = diasSemana;
                viewModel.Add(_viewModel);
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/_ListarMedicoDisponibilidades.cshtml", viewModel);
        }

        public async Task<JsonResult> EventosPorMedico(DateTime start, DateTime end, long? medicoId, long? medicoEspecialidadeId)
        {
            var eventos = await _agendamentoConsultaAppService.ListarPorMedico(medicoId, medicoEspecialidadeId, start, end);
            var eventosViewModel = new List<EventosViewModel>();
            foreach (var item in eventos)
            {
                var horaAgendamento = new DateTime(item.DataAgendamento.Year, item.DataAgendamento.Month, item.DataAgendamento.Day, item.HoraAgendamento.Hour, item.HoraAgendamento.Minute, item.HoraAgendamento.Second);
                var eventoViewModel = new EventosViewModel();
                eventoViewModel.id = item.Id;
                eventoViewModel.title = string.Format("{0}", item.Paciente != null ? item.Paciente.NomeCompleto : item.NomeReservante != null ? item.NomeReservante : string.Empty);
                eventoViewModel.start = horaAgendamento;
                eventoViewModel.end = horaAgendamento.AddMinutes(item.AgendamentoConsultaMedicoDisponibilidade.Intervalo.IntervaloMinutos);
                eventoViewModel.allDay = false;
                eventoViewModel.color = item.Medico.CorAgendamentoConsulta; //agendamento.CorAgendamento;
                eventosViewModel.Add(eventoViewModel);
            }
            return Json(eventosViewModel.ToArray(), "application/json; charset=utf-8", JsonRequestBehavior.AllowGet);
        }

        public async Task<PartialViewResult> _MontarComboMedicos(DateTime date, long? medicoId)
        {
            //var dayOfWeek = (int)date.DayOfWeek;

            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarPorData(date);
            var medicos = agendamentos.Select(m => m.Medico).DistinctBy(m => m.Id).ToList();
            var viewModel = new MontarComboMedicosViewModel();
            viewModel.Medicos = new SelectList(medicos, "Id", "NomeCompleto", medicoId.HasValue ? medicoId.ToString() : medicos.Count() == 1 ? medicos.FirstOrDefault().Id.ToString() : "");
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/_MontarComboMedicos.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _MontarComboMedicoEspecialidades(DateTime date, long? medicoId)
        {
            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarPorData(date);

            var medicosEspecialidades = agendamentos
                .WhereIf(medicoId.HasValue, m => m.MedicoId == medicoId)
                .Select(m => m.MedicoEspecialidade)
                .DistinctBy(m => m.EspecialidadeId)
                .ToList();

            var especialidades = medicosEspecialidades
                .Select(m => m.Especialidade)
                .DistinctBy(m => m.Id)
                .ToList();

            string medicoEspecialidadeId = string.Empty;
            if (medicosEspecialidades.Count() == 1)
            {
                medicoEspecialidadeId = medicosEspecialidades.FirstOrDefault().Id.ToString();
            }
            var viewModel = new MontarComboMedicoEspecialidadesViewModel();
            viewModel.MedicoEspecialidades = new SelectList(medicosEspecialidades.Select(m => new { Id = m.Id, Nome = especialidades.Single(e => e.Id == m.EspecialidadeId).Nome }), "Id", "Nome", medicoEspecialidadeId);
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/_MontarComboMedicoEspecialidades.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _MontarComboHorarios(DateTime date, long medicoId, long medicoEspecialidadeId, long id = 0)
        {
            var dayOfWeek = (int)date.DayOfWeek;
            var start = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            var end = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarPorData(date);
            var marcacoes = await _agendamentoConsultaAppService.ListarPorData(start, end);
            var agendamentosData =
                agendamentos
                .Where(m => m.MedicoId == medicoId
                && m.MedicoEspecialidadeId == medicoEspecialidadeId)
                .DistinctBy(m => m.Id);
            var horarios = new List<SelectListItem>();
            foreach (var item in agendamentosData)
            {
                var _marcacoes = marcacoes.Where(m => m.MedicoId == item.MedicoId && m.MedicoEspecialidadeId == item.MedicoEspecialidadeId).ToList();
                var horariosMarcados = _marcacoes.Select(m => m.HoraAgendamento).ToList();
                bool disponivel = false;
                var horaIni = new DateTime(date.Year, date.Month, date.Day, item.HoraInicio.Hour, item.HoraInicio.Minute, item.HoraInicio.Second);
                var horaFim = new DateTime(date.Year, date.Month, date.Day, item.HoraFim.Hour, item.HoraFim.Minute, item.HoraFim.Second);
                var intervaloMinutos = item.Intervalo.IntervaloMinutos;
                var horaLoop = horaIni;
                var agendamentoAtual = false;
                while (horaLoop <= horaFim)
                {
                    if (horaLoop < horaFim)
                    {
                        if (id > 0 && !agendamentoAtual)
                        {
                            var agendamento = _marcacoes
                                .SingleOrDefault(m => m.Id == id && m.HoraAgendamento == horaLoop);
                            if (agendamento != null)
                            {
                                disponivel = true;
                                agendamentoAtual = true;
                            }
                            else
                            {
                                disponivel = !horaLoop.IsIn(horariosMarcados.ToArray()); //await _agendamentoConsultaAppService.ChecarDisponibilidade(item.Id, horaLoop, id);
                            }
                        }
                        else
                        {
                            disponivel = !horaLoop.IsIn(horariosMarcados.ToArray()); //await _agendamentoConsultaAppService.ChecarDisponibilidade(item.Id, horaLoop, id);
                        }
                        if (disponivel)
                        {
                            horarios.Add(new SelectListItem
                            {
                                Value = item.Id.ToString(),
                                Text = horaLoop.ToString("HH:mm"),
                                Selected = horaLoop.Equals(date)
                            });
                        }
                    }
                    horaLoop = horaLoop.AddMinutes(intervaloMinutos);
                }
            }
            var viewModel = new MontarComboHorariosViewModel();
            viewModel.Horarios = new SelectList(horarios, "Value", "Text");
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoConsultas/_MontarComboHorarios.cshtml", viewModel);
        }

    }
}