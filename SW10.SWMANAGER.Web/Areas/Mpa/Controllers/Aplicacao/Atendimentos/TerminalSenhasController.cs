﻿
using iTextSharp.text;
using Microsoft.Reporting.WebForms;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.AtendimentosLeitosMov.Altas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Filas;
using SW10.SWMANAGER.Web.Controllers;
using SW10.SWMANAGER.Web.Relatorios.Atendimento;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos
{
    public class TerminalSenhasController : SWMANAGERControllerBase
    {
        private readonly ITerminalSenhasAppService _terminalSenhasAppService;


        public TerminalSenhasController(ITerminalSenhasAppService terminalSenhasAppService)
        {
            _terminalSenhasAppService = terminalSenhasAppService;
        }
        // GET: Mpa/Alta
        public ActionResult Index()
        {
            var viewModel = new TermialSenhaViewModel();

            viewModel.Filas = _terminalSenhasAppService.ListarFilasDisponiveis();


            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/TerminalSenhas/Index.cshtml", viewModel);
        }



        public ActionResult AlteracaoTipoLocalChamada()
        {
            var viewModel = new FilaViewModel(new FilaDto());

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/FilasSenhas/Index.cshtml", viewModel);
        }

        public void ImprimirSenha(string tipoLocalChamada, string numero, string hospital)
        {
            using (var relatorio = new Microsoft.Reporting.WebForms.LocalReport())
            {
                relatorio.ReportPath = "Relatorios/Atendimento/senha.rdlc";
                relatorio.SetParameters(new ReportParameter("NomeHospital", hospital));
                relatorio.SetParameters(new ReportParameter("Numero", numero));
                relatorio.SetParameters(new ReportParameter("Fila", tipoLocalChamada));
                relatorio.SetParameters(new ReportParameter("Data", DateTime.Now.ToShortDateString()));

                Microsoft.Reporting.WebForms.Warning[] warnings;
                LimparStreams();
                relatorio.Render("image", CriarDeviceInfo(relatorio), CreateStreamCallback, out warnings);

                // Exportar(relatorio);
                Imprimir(relatorio);
            }
        }



        private List<System.IO.Stream> _streams = new List<System.IO.Stream>();
        public System.IO.Stream CreateStreamCallback(string name, string extension, Encoding encoding, string mimeType, bool willSeek)
        {
            var stream = new System.IO.MemoryStream();
            _streams.Add(stream);
            return stream;
        }

        private void LimparStreams()
        {
            foreach (var stream in _streams)
            {
                stream.Dispose();
            }
            _streams.Clear();
        }

        private string CriarDeviceInfo(Microsoft.Reporting.WebForms.LocalReport relatorio)
        {
            var pageSettings = relatorio.GetDefaultPageSettings();
            return string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                    <PageWidth>10.0in</PageWidth>
                    <PageHeight>11in</PageHeight>
                    <MarginTop>0.0in</MarginTop>
                    <MarginLeft>0.0in</MarginLeft>
                    <MarginRight>0.0in</MarginRight>
                    <MarginBottom>0.0in</MarginBottom>
            </DeviceInfo>");

            //,                pageSettings.PaperSize.Width / 50m, pageSettings.PaperSize.Height / 100m, pageSettings.Margins.Top / 100m, pageSettings.Margins.Left / 100m, pageSettings.Margins.Right / 100m, pageSettings.Margins.Bottom / 100m

            //   string deviceInfo =
            //@"<DeviceInfo>
            //       <OutputFormat>EMF</OutputFormat>
            //       <PageWidth>8.5in</PageWidth>
            //       <PageHeight>11in</PageHeight>
            //       <MarginTop>0.25in</MarginTop>
            //       <MarginLeft>0.25in</MarginLeft>
            //       <MarginRight>0.25in</MarginRight>
            //       <MarginBottom>0.25in</MarginBottom>
            //   </DeviceInfo>";

            //   return deviceInfo;


        }

        private void Imprimir(Microsoft.Reporting.WebForms.LocalReport relatorio)
        {
            try
            {
                using (var pd = new System.Drawing.Printing.PrintDocument())
                {
                   // pd.PrinterSettings.PrinterName = "MP-4200 TH";
                    var pageSettings = new System.Drawing.Printing.PageSettings();
                    var pageSettingsRelatorio = relatorio.GetDefaultPageSettings();
                    pageSettings.PaperSize = pageSettingsRelatorio.PaperSize;
                    pageSettings.Margins = pageSettingsRelatorio.Margins;
                    pd.DefaultPageSettings = pageSettings;

                    pd.PrintPage += Pd_PrintPage;
                    _streamAtual = 0;
                    pd.Print();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private int _streamAtual;
        private void Pd_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs ev)
        {
            var stream = _streams[_streamAtual];

            stream.Seek(0, System.IO.SeekOrigin.Begin);

            Metafile pageImage = new  Metafile(stream);

            // Adjust rectangular area with printer margins.
            System.Drawing.Rectangle adjustedRect = new System.Drawing.Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            _streamAtual++;
            ev.HasMorePages = _streamAtual < _streams.Count;



            //var stream = _streams[_streamAtual];
            //stream.Seek(0, System.IO.SeekOrigin.Begin);

            //using (var metadata = new System.Drawing.Imaging.Metafile(stream))
            //{
            //    e.Graphics.DrawImage(metadata, e.PageBounds);
            //}

            //_streamAtual++;
            //e.HasMorePages = _streamAtual < _streams.Count;
        }

    }
}