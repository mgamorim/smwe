﻿using Abp.Domain.Repositories;
using Abp.Web.Mvc.Authorization;
using Abp.Web.Security.AntiForgery;
using Microsoft.Reporting.WebForms;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Relatorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Visitantes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Visitantes.Dto;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.Internacao;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.Relatorios;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos;
using SW10.SWMANAGER.Web.Controllers;
using SW10.SWMANAGER.Web.Relatorios.Faturamento.Guias.InternacaoSolicitacao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos.Relatorios
{
    [DisableAbpAntiForgeryTokenValidation]
    public class AtendimentoRelatorioController : SWMANAGERControllerBase
    {
        #region Dependencias
        private IRelatorioAtendimentoAppService _relatorioAtendimentoAppService;
        private ISessionAppService _sessionAppService;
        private IAtendimentoAppService _atendimentoAppService;
        private IVisitanteAppService _visitanteAppService;
        private readonly ILeitoAppService _leitoAppService;
        private readonly IRepository<Erro, long> _erroRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;

        private readonly IUserAppService _userAppService;
        public AtendimentoRelatorioController(
            IRelatorioAtendimentoAppService relatorioAtendimentoAppService,
            ISessionAppService sessionAppService,
            IAtendimentoAppService atendimentoAppService,
            IUserAppService userAppService,
            IVisitanteAppService visitanteAppService,
            ILeitoAppService leitoAppService,
            IRepository<Erro, long> erroRepository,
            IRepository<Atendimento, long> atendimentoRepository
            )
        {
            _relatorioAtendimentoAppService = relatorioAtendimentoAppService;
            _sessionAppService = sessionAppService;
            _userAppService = userAppService;
            _atendimentoAppService = atendimentoAppService;
            _visitanteAppService = visitanteAppService;
            _leitoAppService = leitoAppService;
            _erroRepository = erroRepository;
            _atendimentoRepository = atendimentoRepository;
        }
        #endregion dependencias.

        string obterIdade(DateTime? nascimento)
        {
            string ret = "";

            if (nascimento != null)
            {
                string retorno = "{0} {1}";
                var idade = DateDifference.GetExtendedDifference(((DateTime)nascimento));

                if (idade.Ano > 0)
                {
                    ret = string.Format(retorno, idade.Ano, "Anos");
                }
                else if (idade.Mes > 0)
                {
                    ret = string.Format(retorno, idade.Mes, "Meses");
                }
                else
                {
                    ret = string.Format(retorno, idade.Dia, "Dias");
                }
            }

            if (ret == "0 Dias")
                ret = "";

            return ret;
        }

        public async Task<ActionResult> ReltorioLeitosPdf(long? empresaId = null)
        {
            try
            {
                ReportViewer reportViewer = new ReportViewer();
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Atendimento\RelatorioInternado.rdlc");

                //localização do relatório
                var dados = await CarregarIndex(empresaId);
                var relatorioInternacao = await _relatorioAtendimentoAppService.ListarRelatorio(dados.Empresa);

                //  var relatorioInternacao = _leitoAppService.ListarPorUnidadePaginado(new ListarLeitosInput());

                var loginInformations = await _sessionAppService.GetCurrentLoginInformations();

                if (relatorioInternacao.Items.Count != 0)
                {
                    dados.Titulo = string.Concat("Mapa Diário - ", Convert.ToString(DateTime.Now));

                    if (empresaId != 0)
                    {
                        dados.NomeHospital = relatorioInternacao.Items[0].Empresa.NomeFantasia.ToString();
                    }
                    else
                    {
                        dados.NomeHospital = "";
                    }

                    dados.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                    dados.DataHora = Convert.ToString(DateTime.Now);


                    dados.Lista = relatorioInternacao.Items.OrderBy(o => o.LeitoId).Select(m => new TesteObjeto
                    {
                        CodAtendimento = m.Codigo,
                        CodPaciente = (m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente)) == "0" ? "" : m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente),
                        Convenio = m.Convenio == null ? string.Empty : m.Convenio.NomeFantasia,//.Substring(0,),
                        DataInternacao = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : string.Format("{0:dd/MM/yyyy}", m.DataRegistro),
                        Empresa = m.Empresa == null ? string.Empty : m.Empresa.NomeFantasia,//.Substring(0, 15),
                        Leito = m.Leito == null ? string.Empty : m.Leito.Descricao,
                        Medico = m.Medico == null ? string.Empty : m.Medico.NomeCompleto,
                        Origem = m.Origem == null ? string.Empty : m.Origem.Descricao,

                        Paciente = m.Paciente == null ? string.Empty : m.Paciente.NomeCompleto,

                        //    UnidOrganizacional = m.UnidadeOrganizacional == null ? string.Empty : m.UnidadeOrganizacional.Descricao,
                        Idade = (m.Paciente == null || m.Paciente.Nascimento == null || m.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : obterIdade(m.Paciente.Nascimento),
                        DiasInternado = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(m.DataRegistro).TotalDays, 0))
                    }
                    )
                    .OrderBy(x => x.Leito)
                    .ToList();


                }

                if (dados != null)
                {
                    ReportParameter nomeHospital = new ReportParameter("NomeHospital", dados.NomeHospital);
                    ReportParameter titulo = new ReportParameter("Titulo", dados.Titulo);
                    ReportParameter usuario = new ReportParameter("Usuario", dados.NomeUsuario);
                    ReportParameter dataHora = new ReportParameter("DataHora", dados.DataHora);
                    reportViewer.LocalReport.SetParameters(new ReportParameter[] { nomeHospital, titulo, usuario, dataHora });

                    Web.Relatorios.Atendimento.Atendimento relDS = new Web.Relatorios.Atendimento.Atendimento();
                    DataTable tabela = this.ConvertToDataTable(dados.Lista, relDS.Tables["AtendimentoDS"]);
                    ReportDataSource dataSource = new ReportDataSource();
                    dataSource.Value = tabela;
                    dataSource.Name = "RelatorioInternado";
                    reportViewer.LocalReport.DataSources.Clear();
                    reportViewer.LocalReport.DataSources.Add(dataSource);
                    reportViewer.LocalReport.Refresh();

                    string mimeType = string.Empty;
                    string encoding = string.Empty;
                    string extension = "pdf";

                    string[] streamIds;
                    Warning[] warnings;
                    byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                    reportViewer.LocalReport.Refresh();

                    Response.Headers.Add("Content-Disposition", "inline; filename=relatorio_leitos.pdf");
                    return File(pdfBytes, "application/pdf");
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }

            return null;
        }

        public async Task<ActionResult> ReltorioLeitosPdfTeste(long? empresaId = null)
        {
            try
            {
                ReportViewer reportViewer = new ReportViewer();
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Atendimento\RelatorioInternado.rdlc");

                //localização do relatório
                var dados = await CarregarIndex(empresaId);
                //      var relatorioInternacao = await _relatorioAtendimentoAppService.ListarRelatorio(dados.Empresa);

                var leitos = await _leitoAppService.ListarParaRelatorioMapaLeitos(dados.Empresa);

                //  var relatorioInternacao = _leitoAppService.ListarPorUnidadePaginado(new ListarLeitosInput());

                var loginInformations = await _sessionAppService.GetCurrentLoginInformations();

                if (leitos.Count != 0)
                {
                    dados.Titulo = string.Concat("Mapa Diário - ", Convert.ToString(DateTime.Now));

                    //if (empresaId != 0)
                    //{
                    //    dados.NomeHospital = relatorioInternacao.Items[0].Empresa.NomeFantasia.ToString();
                    //}
                    //else
                    //{
                    //    dados.NomeHospital = "";
                    //}

                    dados.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                    dados.DataHora = Convert.ToString(DateTime.Now);

                    dados.Lista = new List<TesteObjeto>();

                    leitos = leitos.OrderBy(o => o.Descricao).ToList();// Ordenação por descrição do leito

                    foreach (var leito in leitos)
                    {
                        TesteObjeto obj;// = new TesteObjeto();

                        if (leito.LeitoStatusId == 2)
                        {
                            var m = _atendimentoRepository.GetAll()
                                .Include(x => x.Paciente)
                                .Include(x => x.Paciente.SisPessoa)
                                .Include(x => x.Medico)
                                .Include(x => x.Medico.SisPessoa)
                                .Include(x => x.AtendimentoTipo)
                                .Include(x => x.Convenio)
                                .Include(x => x.Convenio.SisPessoa)
                                .Include(x => x.Empresa)
                                .Include(x => x.Especialidade)
                                .Include(x => x.Guia)
                                .Include(x => x.Leito)
                                .Include(x => x.Leito.UnidadeOrganizacional)
                                .Include(x => x.Leito.LeitoStatus)
                                .Include(x => x.MotivoAlta)
                                .Include(x => x.Nacionalidade)
                                .Include(x => x.Origem)
                                .Include(x => x.Plano)
                                .Include(x => x.ServicoMedicoPrestado)
                                .Include(x => x.UnidadeOrganizacional)
                                .Where(a => a.IsInternacao == true)
                                .Where(a => a.DataAlta == null)
                                .Where(a => (empresaId == 0 || a.EmpresaId == empresaId) && a.DataAlta == null)
                                .Where(x => x.LeitoId == leito.Id)
                                .FirstOrDefault();

                            if (m != null)
                            {
                                obj = new TesteObjeto
                                {
                                      CodAtendimento = m.Codigo,
                                    CodPaciente = (m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente)) == "0" ? "" : m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente),
                                    Convenio = m.Convenio == null ? string.Empty : m.Convenio.NomeFantasia,//.Substring(0,),
                                    Plano = m.Plano == null ? string.Empty : m.Plano.Descricao,
                                    DataInternacao = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : string.Format("{0:dd/MM/yyyy}", m.DataRegistro),
                                    Empresa = m.Empresa == null ? string.Empty : m.Empresa.NomeFantasia,//.Substring(0, 15),
                                    Leito = m.Leito == null ? string.Empty : m.Leito.Descricao,
                                    Medico = m.Medico == null ? string.Empty : m.Medico.NomeCompleto,
                                    Origem = m.Origem == null ? string.Empty : m.Origem.Descricao,
                                    Paciente = m.Paciente == null ? string.Empty : m.Paciente.NomeCompleto,
                                    Idade = (m.Paciente == null || m.Paciente.Nascimento == null || m.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : obterIdade(m.Paciente.Nascimento),
                                    DiasInternado = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(m.DataRegistro).TotalDays, 0))
                                };

                                dados.Lista.Add(obj);
                            }
                            else
                            {
                                obj = new TesteObjeto
                                {
                                    CodAtendimento = "",
                                    CodPaciente = "",
                                    Convenio = "",
                                    DataInternacao = "",
                                    Empresa = "",
                                    Leito = leito.Descricao,
                                    Medico = "",
                                    Origem = "",
                                    Paciente = leito.LeitoStatus?.Descricao + " - Atendimento não encontrado",
                                    Idade = "",
                                    DiasInternado = ""
                                };

                                dados.Lista.Add(obj);
                            }

                        }
                        else
                        {
                            obj = new TesteObjeto
                            {
                                CodAtendimento = "",
                                CodPaciente = "",
                                Convenio = "",
                                DataInternacao = "",
                                Empresa = "",
                                Leito = leito.Descricao,
                                Medico = "",
                                Origem = "",
                                Paciente = leito.LeitoStatus?.Descricao,
                                Idade = "",
                                DiasInternado = ""
                            };

                            dados.Lista.Add(obj);
                        }
                    }


                }

                if (dados != null)
                {
                    ReportParameter nomeHospital = new ReportParameter("NomeHospital", dados.NomeHospital);
                    ReportParameter titulo = new ReportParameter("Titulo", dados.Titulo);
                    ReportParameter usuario = new ReportParameter("Usuario", dados.NomeUsuario);
                    ReportParameter dataHora = new ReportParameter("DataHora", dados.DataHora);
                    reportViewer.LocalReport.SetParameters(new ReportParameter[] { nomeHospital, titulo, usuario, dataHora });

                    Web.Relatorios.Atendimento.Atendimento relDS = new Web.Relatorios.Atendimento.Atendimento();
                    DataTable tabela = this.ConvertToDataTable(dados.Lista, relDS.Tables["AtendimentoDS"]);
                    ReportDataSource dataSource = new ReportDataSource();
                    dataSource.Value = tabela;
                    dataSource.Name = "RelatorioInternado";
                    reportViewer.LocalReport.DataSources.Clear();
                    reportViewer.LocalReport.DataSources.Add(dataSource);
                    reportViewer.LocalReport.Refresh();

                    string mimeType = string.Empty;
                    string encoding = string.Empty;
                    string extension = "pdf";

                    string[] streamIds;
                    Warning[] warnings;
                    byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                    reportViewer.LocalReport.Refresh();

                    Response.Headers.Add("Content-Disposition", "inline; filename=relatorio_leitos.pdf");
                    return File(pdfBytes, "application/pdf");
                }
            }
            catch (Exception e)
            {
                await _erroRepository.InsertAsync(new Erro(e));
            }

            return null;
        }


        public async Task<ActionResult> ReltorioLeitosPdfSintetico(long? empresaId = null)
        {
            var novaLista = new List<TesteObjeto>();
            try
            {
                ReportViewer reportViewer = new ReportViewer();
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Atendimento\RelatorioInternadosSintetico.rdlc");

                //localização do relatório
                var dados = await CarregarIndex(empresaId);

                var leitos = await _leitoAppService.ListarParaRelatorioMapaLeitos(dados.Empresa);

                //  var relatorioInternacao = _leitoAppService.ListarPorUnidadePaginado(new ListarLeitosInput());

                var loginInformations = await _sessionAppService.GetCurrentLoginInformations();

                if (leitos.Count != 0)
                {
                    dados.Titulo = string.Concat("Mapa Diário - ", Convert.ToString(DateTime.Now));

                    //if (empresaId != 0)
                    //{
                    //    dados.NomeHospital = relatorioInternacao.Items[0].Empresa.NomeFantasia.ToString();
                    //}
                    //else
                    //{
                    //    dados.NomeHospital = "";
                    //}

                    dados.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                    dados.DataHora = Convert.ToString(DateTime.Now);

                    dados.Lista = new List<TesteObjeto>();

                    leitos = leitos.OrderBy(o => o.Descricao).ToList();// Ordenação por descrição do leito

                    foreach (var leito in leitos)
                    {
                        TesteObjeto obj;// = new TesteObjeto();

                        if (leito.LeitoStatusId == 2)
                        {
                            var m = _atendimentoRepository.GetAll()
                                .Include(x => x.Paciente)
                                .Include(x => x.Paciente.SisPessoa)
                                .Include(x => x.Medico)
                                .Include(x => x.Medico.SisPessoa)
                                .Include(x => x.AtendimentoTipo)
                                .Include(x => x.Convenio)
                                .Include(x => x.Convenio.SisPessoa)
                                .Include(x => x.Empresa)
                                .Include(x => x.Especialidade)
                                .Include(x => x.Guia)
                                .Include(x => x.Leito)
                                .Include(x => x.Leito.UnidadeOrganizacional)
                                .Include(x => x.Leito.LeitoStatus)
                                .Include(x => x.MotivoAlta)
                                .Include(x => x.Nacionalidade)
                                .Include(x => x.Origem)
                                .Include(x => x.Plano)
                                .Include(x => x.ServicoMedicoPrestado)
                                .Include(x => x.UnidadeOrganizacional)
                                .Where(a => a.IsInternacao == true)
                                .Where(a => a.DataAlta == null)
                                .Where(a => (empresaId == 0 || a.EmpresaId == empresaId) && a.DataAlta == null)
                                .Where(x => x.LeitoId == leito.Id)
                                .FirstOrDefault();

                            if (m != null)
                            {
                                var med = m.Medico == null && m.Medico.SisPessoa != null ? string.Empty : m.Medico.SisPessoa.NomeCompleto;

                                if (string.IsNullOrEmpty(med))
                                {
                                    med = m.Medico == null && m.Medico.SisPessoa != null ? string.Empty : m.Medico.SisPessoa.NomeFantasia;

                                }

                                try
                                {
                                    if (med.Length > 31)
                                        med = med.Substring(0, 31);
                                }
                                catch { }

                                obj = new TesteObjeto
                                {
                                    CodAtendimento = m.Codigo,
                                    CodPaciente = (m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente)) == "0" ? "" : m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente),
                                    Convenio = m.Convenio == null ? string.Empty : m.Convenio.NomeFantasia,//.Substring(0,),
                                    Plano = m.Plano == null ? string.Empty : m.Plano.Descricao,//.Substring(0,),
                                    DataInternacao = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : string.Format("{0:dd/MM/yyyy}", m.DataRegistro),
                                    Empresa = m.Empresa == null ? string.Empty : m.Empresa.NomeFantasia,//.Substring(0, 15),
                                    Leito = m.Leito == null ? string.Empty : m.Leito.Descricao,
                                    Medico = med,
                                    Origem = m.Plano == null ? string.Empty : m.Plano.Descricao,//NO SINTETICO QUE EXIBE PLANO SOBRESCREVE O CAMPO ORIGEM
                                    Paciente = m.Paciente == null ? string.Empty : m.Paciente.NomeCompleto,
                                    Idade = (m.Paciente == null || m.Paciente.Nascimento == null || m.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : obterIdade(m.Paciente.Nascimento),
                                    DiasInternado = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(m.DataRegistro).TotalDays, 0))
                                };

                                dados.Lista.Add(obj);
                            }
                            else
                            {
                                obj = new TesteObjeto
                                {
                                    CodAtendimento = "",
                                    CodPaciente = "",
                                    Convenio = "",
                                    DataInternacao = "",
                                    Empresa = "",
                                    Leito = leito.Descricao,
                                    Medico = "",
                                    Origem = "",
                                    Paciente = leito.LeitoStatus.Descricao + " - Atendimento não encontrado",
                                    Idade = "",
                                    DiasInternado = ""
                                };

                                dados.Lista.Add(obj);
                            }

                        }
                        else
                        {
                            obj = new TesteObjeto
                            {
                                CodAtendimento = "",
                                CodPaciente = "",
                                Convenio = "",
                                DataInternacao = "",
                                Empresa = "",
                                Leito = leito.Descricao,
                                Medico = "",
                                Origem = "",
                                Paciente = leito.LeitoStatus.Descricao,
                                Idade = "",
                                DiasInternado = ""
                            };

                            dados.Lista.Add(obj);
                        }
                    }


                    //    var relatorioInternacao = await _relatorioAtendimentoAppService.ListarRelatorio(dados.Empresa);


                    ////  var relatorioInternacao = _leitoAppService.ListarPorUnidadePaginado(new ListarLeitosInput());

                    //var loginInformations = await _sessionAppService.GetCurrentLoginInformations();

                    //if (relatorioInternacao.Items.Count != 0)
                    //{
                    //    dados.Titulo = string.Concat("Mapa Diário (Sintético) - ", Convert.ToString(DateTime.Now));

                    //    if (empresaId != 0)
                    //    {
                    //        dados.NomeHospital = relatorioInternacao.Items[0].Empresa.NomeFantasia.ToString();
                    //    }
                    //    else
                    //    {
                    //        dados.NomeHospital = "";
                    //    }

                    //    dados.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                    //    dados.DataHora = Convert.ToString(DateTime.Now);

                    //    // Testando agrupamento por unidade organizacional
                    //    var grupos = relatorioInternacao.Items.GroupBy(x => x.Leito.UnidadeOrganizacional?.Descricao).ToList();

                    //    foreach (var g in grupos)
                    //    {
                    //        // Estava exibindo nome da undiade, mas nao confirmaram se era pra exibir ou apenas uma linha divisoria
                    //        var grupo = "";// g.Key;

                    //        var divisaoUnidade = new TesteObjeto
                    //        {
                    //            CodAtendimento = "teste",
                    //            CodPaciente = "teste",
                    //            Convenio = "teste",
                    //            DataInternacao = "teste",
                    //            Empresa = "teste",
                    //            Leito = "teste", // grupo?.ToUpper(),
                    //            Medico = "teste",
                    //            Origem = "teste",
                    //            Paciente = "teste", //grupo?.ToUpper(),
                    //            Idade = "teste",
                    //            DiasInternado = "teste"
                    //        };

                    //        novaLista.Add(divisaoUnidade);

                    //        foreach (var i in g)
                    //        {

                    //            var novo = new TesteObjeto
                    //            {
                    //                CodAtendimento = i.Codigo,
                    //                CodPaciente = (i.Paciente == null ? string.Empty : Convert.ToString(i.Paciente.CodigoPaciente)) == "0" ? "" : i.Paciente == null ? string.Empty : Convert.ToString(i.Paciente.CodigoPaciente),
                    //                Convenio = i.Convenio == null ? string.Empty : i.Convenio.NomeFantasia,
                    //                DataInternacao = (i.DataRegistro == null || i.DataRegistro == DateTime.MinValue) ? string.Empty : string.Format("{0:dd/MM/yyyy}", i.DataRegistro),
                    //                Empresa = i.Empresa == null ? string.Empty : i.Empresa.NomeFantasia,
                    //                Leito = i.Leito == null ? string.Empty : i.Leito.Descricao,
                    //                Medico = i.Medico == null ? string.Empty : i.Medico.NomeCompleto,
                    //                Origem = i.Plano == null ? string.Empty : i.Plano.Descricao,
                    //                Paciente = i.Paciente == null ? string.Empty : i.Paciente.NomeCompleto,
                    //                //  Idade = (i.Paciente == null || i.Paciente.Nascimento == null || i.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : obterIdade(i.Paciente.Nascimento),
                    //                //    DiasInternado = (i.DataRegistro == null || i.DataRegistro == DateTime.MinValue) ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(i.DataRegistro).TotalDays, 0))
                    //            };

                    //            novaLista.Add(novo);
                    //        }
                    //    }

                    //    dados.Lista = relatorioInternacao.Items.OrderBy(o => o.LeitoId).Select(m => new TesteObjeto
                    //    {
                    //        CodAtendimento = m.Codigo,
                    //        CodPaciente = (m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente)) == "0" ? "" : m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente),
                    //        Convenio = m.Convenio == null ? string.Empty : m.Convenio.NomeFantasia,
                    //        DataInternacao = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : string.Format("{0:dd/MM/yyyy}", m.DataRegistro),
                    //        Empresa = m.Empresa == null ? string.Empty : m.Empresa.NomeFantasia,
                    //        Leito = m.Leito == null ? string.Empty : m.Leito.Descricao,
                    //        Medico = m.Medico == null ? string.Empty : m.Medico.NomeCompleto,
                    //        Origem = m.Origem == null ? string.Empty : m.Origem.Descricao,
                    //        Paciente = m.Paciente == null ? string.Empty : m.Paciente.NomeCompleto,
                    //        Idade = (m.Paciente == null || m.Paciente.Nascimento == null || m.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : obterIdade(m.Paciente.Nascimento),
                    //        DiasInternado = (m.DataRegistro == null || m.DataRegistro == DateTime.MinValue) ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(m.DataRegistro).TotalDays, 0))
                    //    }
                    //   )
                    //   .OrderBy(x => x.Leito)
                    //   .ToList();

                    //}

                    if (dados != null)
                    {
                        ReportParameter nomeHospital = new ReportParameter("NomeHospital", dados.NomeHospital);
                        ReportParameter titulo = new ReportParameter("Titulo", dados.Titulo);
                        ReportParameter usuario = new ReportParameter("Usuario", dados.NomeUsuario);
                        ReportParameter dataHora = new ReportParameter("DataHora", dados.DataHora);
                        reportViewer.LocalReport.SetParameters(new ReportParameter[] { nomeHospital, titulo, usuario, dataHora });

                        Web.Relatorios.Atendimento.Atendimento relDS = new Web.Relatorios.Atendimento.Atendimento();

                        //DataTable tabela = this.ConvertToDataTable(dados.Lista, relDS.Tables["AtendimentoDS"]);
                        DataTable tabela = this.ConvertToDataTable(dados.Lista, relDS.Tables["AtendimentoDS"]);

                        ReportDataSource dataSource = new ReportDataSource();

                        dataSource.Value = tabela;
                        dataSource.Name = "relatorio_leitos_sintetico_dataset";
                        reportViewer.LocalReport.DataSources.Clear();
                        reportViewer.LocalReport.DataSources.Add(dataSource);
                        reportViewer.LocalReport.Refresh();

                        string mimeType = string.Empty;
                        string encoding = string.Empty;
                        string extension = "pdf";

                        string[] streamIds;
                        Warning[] warnings;
                        byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                        reportViewer.LocalReport.Refresh();

                        Response.Headers.Add("Content-Disposition", "inline; filename=relatorio_leitos.pdf");
                        return File(pdfBytes, "application/pdf");
                    }
                }
            }
            catch (Exception e)
            {
                await _erroRepository.InsertAsync(new Erro(e));
            }

            return null;
        }


        /// <summary>
        /// Entrada para filtro de visualização do Report de produtos
        /// </summary>
        /// <returns></returns>
        //GET: Mpa/Relatorios/RelatorioInternado
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioIntenado)]
        public async Task<ActionResult> Index()
        {
            FiltroModel result = await CarregarIndex(0);
            //result.EhMovimentacao = false;
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Relatorios/Index.cshtml", result);
        }

        /// <summary>
        /// PartialView que renderiza o relatório com os filtros selecionados no formulário
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioIntenado)]
        public async Task<ActionResult> Visualizar(FiltroModel filtro)
        {
            var relatorioInternacao = await _relatorioAtendimentoAppService.ListarRelatorio(filtro.Empresa);

            var loginInformations = await _sessionAppService.GetCurrentLoginInformations();
            //foreach (var item in relatorioInternacao.Items)
            //{
            //    //Math.Truncate(decimalNumber)
            //    string teste = Convert.ToString(Math.Round(DateTime.Now.Subtract(item.DataRegistro).TotalDays, 0));
            //   //decimal teste2 = Math.Truncate(Convert.ToDecimal(teste));
            //}
            FiltroModel _filtro = new FiltroModel();
            if (relatorioInternacao.Items.Count != 0)
            {
                _filtro.Titulo = string.Concat("Mapa Diário - ", Convert.ToString(DateTime.Now));
                //_filtro.NomeHospital = "Lipp";
                _filtro.NomeHospital = relatorioInternacao.Items[0].Empresa.NomeFantasia.ToString();
                _filtro.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                _filtro.DataHora = Convert.ToString(DateTime.Now);


                _filtro.Lista = relatorioInternacao.Items.Select(m => new TesteObjeto
                {
                    CodAtendimento = m.Codigo,
                    CodPaciente = m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente),
                    Convenio = m.Convenio == null ? string.Empty : m.Convenio.NomeFantasia,
                    DataInternacao = m.DataRegistro == null ? string.Empty : m.DataRegistro.ToString(),
                    Empresa = m.Empresa == null ? string.Empty : m.Empresa.NomeFantasia,
                    Leito = m.Leito == null ? string.Empty : m.Leito.Descricao,
                    Medico = m.Medico == null ? string.Empty : m.Medico.NomeCompleto,
                    Origem = m.Origem == null ? string.Empty : m.Origem.Descricao,
                    Paciente = m.Paciente == null ? string.Empty : m.Paciente.NomeCompleto,
                    UnidOrganizacional = m.UnidadeOrganizacional == null ? string.Empty : m.UnidadeOrganizacional.Descricao,
                    Idade = !(m.Paciente == null || m.Paciente.Nascimento == null && m.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : Convert.ToString(DateTime.Now.Year - ((DateTime)m.Paciente.Nascimento).Year),
                    DiasInternado = m.DataRegistro == null ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(m.DataRegistro).TotalDays, 0))
                }
                ).ToList();
            }

            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Relatorios/RelatorioInternado.aspx", _filtro);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioAtendimento)]
        public async Task<ActionResult> IndexRelatorioAtendimento()
        {
            FiltroModel result = await CarregarIndex(0);
            //result.EhMovimentacao = false;
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Relatorios/IndexRelatorioAtendimento.cshtml", result);
        }

        /// <summary>
        /// PartialView que renderiza o relatório com os filtros selecionados no formulário
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioAtendimento)]
        public async Task<ActionResult> Gerar(FiltroModel filtro)
        {
            var relatorioAtendimento = await _relatorioAtendimentoAppService.ListarRelatorio(filtro.Empresa);

            var loginInformations = await _sessionAppService.GetCurrentLoginInformations();
            //foreach (var item in relatorioInternacao.Items)
            //{
            //    //Math.Truncate(decimalNumber)
            //    string teste = Convert.ToString(Math.Round(DateTime.Now.Subtract(item.DataRegistro).TotalDays, 0));
            //   //decimal teste2 = Math.Truncate(Convert.ToDecimal(teste));
            //}
            FiltroModel _filtro = new FiltroModel();
            if (relatorioAtendimento.Items.Count != 0)
            {
                _filtro.Titulo = string.Concat("Relatório de Atendimentos - ", Convert.ToString(DateTime.Now));
                //_filtro.NomeHospital = "Lipp";
                _filtro.NomeHospital = relatorioAtendimento.Items[0].Empresa.NomeFantasia.ToString();
                _filtro.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
                _filtro.DataHora = Convert.ToString(DateTime.Now);


                _filtro.Lista = relatorioAtendimento.Items.Select(m => new TesteObjeto
                {
                    CodAtendimento = m.Codigo,
                    CodPaciente = m.Paciente == null ? string.Empty : Convert.ToString(m.Paciente.CodigoPaciente),
                    Convenio = m.Convenio == null ? string.Empty : m.Convenio.NomeFantasia,
                    DataInternacao = m.DataRegistro == null ? string.Empty : m.DataRegistro.ToString(),
                    Empresa = m.Empresa == null ? string.Empty : m.Empresa.NomeFantasia,
                    Leito = m.Leito == null ? string.Empty : m.Leito.Descricao,
                    Medico = m.Medico == null ? string.Empty : m.Medico.NomeCompleto,
                    Origem = m.Origem == null ? string.Empty : m.Origem.Descricao,
                    Paciente = m.Paciente == null ? string.Empty : m.Paciente.NomeCompleto,
                    UnidOrganizacional = m.UnidadeOrganizacional == null ? string.Empty : m.UnidadeOrganizacional.Descricao,
                    Idade = !(m.Paciente == null || m.Paciente.Nascimento == null && m.Paciente.Nascimento == DateTime.MinValue) ? string.Empty : Convert.ToString(DateTime.Now.Year - ((DateTime)m.Paciente.Nascimento).Year),
                    DiasInternado = m.DataRegistro == null ? string.Empty : Convert.ToString(Math.Round(DateTime.Now.Subtract(m.DataRegistro).TotalDays, 0))
                }
                ).ToList();
            }

            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Relatorios/", _filtro);
        }




        private async Task<FiltroModel> CarregarIndex(long? empresaId)
        {
            var loginInformations = await _sessionAppService.GetCurrentLoginInformations();

            var userId = AbpSession.UserId;
            //var userEmpresas = _userAppService.GetUserEmpresas(userId.Value);
            // var userEmpresas = _relatorioAtendimentoAppService.ListarEmpresaUsuario(userId.Value);

            FiltroModel result = new FiltroModel();

            result.Empresas = (_relatorioAtendimentoAppService.ListarEmpresaUsuario(userId.Value))
                .Select(s => new SelectListItem
                {
                    Value = s.Id.ToString(),
                    Text = s.Nome
                }).ToList();

            var padrao = new SelectListItem { Text = "Selecione", Value = "0" };
            result.Empresas.Insert(0, padrao);
            result.Empresa = empresaId ?? 0;
            return result;
        }

        private void ProcessarDadosInternacao(FiltroModel filtro)
        {
            var db = new Core.DataSetReportsTableAdapters.RelatorioMovimentoAdapter();

            var grupo = filtro.GrupoProduto.GetValueOrDefault();
            var classe = filtro.Classe.GetValueOrDefault();
            var subClasse = filtro.SubClasse.GetValueOrDefault();
            var query = db.GetData().Where(w => w.GrupoId == grupo);

            if (classe != 0)
            {
                query = query.Where(w => w.GrupoClasseId == classe);
            }

            if (subClasse != 0)
            {
                query = query.Where(w => w.GrupoSubClasseId == subClasse);
            }

            filtro.DadosMovimentacao = query.ToList();
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioIntenado)]
        public async Task<ActionResult> IndexAtendimentoEtiqueta(int linhas = 1)
        {
            var atendimento = TempData.Peek("Atendimento") as ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto.AtendimentoDto;
            var model = await Task.Run(() => new List<AtendimentoEtiqueta>());
            var idade = Math.Round(DateTime.Now.Subtract((DateTime)atendimento.Paciente.Nascimento).TotalDays / 365.25, 0);
            for (int l = 0; l < linhas; l++)
            {
                for (int i = 0; i < 2; i++)
                {
                    model.Add(new AtendimentoEtiqueta
                    {
                        AtendimentoId = atendimento.Id.ToString(),
                        CodigoAtendimento = atendimento.Codigo,
                        Convenio = atendimento.Convenio == null ? string.Empty : FuncoesGlobais.TresPontos(atendimento.Convenio.NomeFantasia, 25),
                        DataAtendimento = atendimento.DataRegistro,
                        DataNascimento = (DateTime)atendimento.Paciente.Nascimento,
                        Idade = string.Concat(idade, " ano(s)"),
                        MatriculaConvenio = atendimento.Convenio == null ? string.Empty : atendimento.Convenio.Codigo,
                        Paciente = atendimento.Paciente == null ? string.Empty : FuncoesGlobais.TresPontos(atendimento.Paciente.NomeCompleto, 25)
                    });
                }
            }
            var viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Relatorios\Atendimento\AtendimentoEtiqueta.rdlc";
            viewer.LocalReport.DataSources.Add(new ReportDataSource("AtendimentoEtiquetaDT", model));


            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = "pdf";
            byte[] pdfBytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            viewer.LocalReport.Refresh();


            //var printerSettings = new PrinterSettings
            //{
            //    PrinterName = "Microsoft Print to PDF"
            //};

            //var pageSettings = new PageSettings(printerSettings);

            ////pageSettings.Margins.Left = 0;
            ////pageSettings.Margins.Bottom = 0;
            ////pageSettings.Margins.Right = 0;
            ////pageSettings.Margins.Top = 0;
            ////pageSettings.PaperSize.Height = 3;
            ////pageSettings.PaperSize.Width = 5;

            //viewer.SetPageSettings(pageSettings);
            //viewer.SizeToReportContent = true;
            //viewer.ShowPrintButton = true;
            //viewer.ShowExportControls = true;
            //viewer.ShowToolBar = true;
            //viewer.ShowPageNavigationControls = false;

            //viewer.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            //viewer.Height = System.Web.UI.WebControls.Unit.Percentage(100);

            //ViewBag.ReportViewer = viewer;
            //ViewBag.Linhas = linhas;
            //return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Relatorios/IndexAtendimentoEtiqueta.csHtml", model);
            Response.Headers.Add("Content-Disposition", "inline; filename=AtendimentoEtiqueta.pdf");
            return File(pdfBytes, "application/pdf");
        }

        public async Task<ContentResult> AtendimentoTempData(long id)
        {
            var atendimento = await _atendimentoAppService.Obter(id);
            TempData["Atendimento"] = atendimento;
            return Content(string.Empty);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioIntenado)]
        public async Task<ActionResult> EtiquetaPaciente(long atendimentoId)
        {
            try
            {
                var atendimento = await _atendimentoAppService.Obter(atendimentoId);
                var model = await Task.Run(() => new List<AtendimentoEtiqueta>());

                string idade = "";
                if (atendimento.Paciente.Nascimento != null)
                {
                    idade = Math.Round(DateTime.Now.Subtract((DateTime)atendimento.Paciente.Nascimento).TotalDays / 365.25, 0).ToString();
                }

                for (int l = 0; l < 1; l++)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        model.Add(new AtendimentoEtiqueta
                        {
                            AtendimentoId = atendimento.Id.ToString(),
                            CodigoAtendimento = atendimento.Codigo,
                            Convenio = atendimento.Convenio == null ? string.Empty : FuncoesGlobais.TresPontos(atendimento.Convenio?.NomeFantasia, 25),
                            DataAtendimento = atendimento.DataRegistro,
                            //   DataNascimento = (DateTime)atendimento.Paciente?.Nascimento,
                            Idade = string.Concat(idade, " ano(s)"),
                            MatriculaConvenio = atendimento.Convenio == null ? string.Empty : atendimento.Convenio?.Codigo,
                            Paciente = atendimento.Paciente == null ? string.Empty : FuncoesGlobais.TresPontos(atendimento.Paciente?.NomeCompleto, 25)
                        });
                    }
                }
                var viewer = new ReportViewer();
                viewer.ProcessingMode = ProcessingMode.Local;
                viewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Relatorios\Atendimento\AtendimentoEtiqueta.rdlc";
                viewer.LocalReport.DataSources.Add(new ReportDataSource("AtendimentoEtiquetaDT", model));

                Warning[] warnings;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";
                byte[] pdfBytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                viewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=AtendimentoEtiqueta.pdf");
                return File(pdfBytes, "application/pdf");

            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }

        }

        public async Task<ActionResult> ModalEtiquetaPaciente(long atendimentoId)
        {
            ModalEtiquetaPacienteViewModel viewModel = new ModalEtiquetaPacienteViewModel();
            viewModel.AtendimentoId = atendimentoId.ToString();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Etiqueta/_ModalEtiquetaPaciente.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Relatorio_RelatorioIntenado)]
        public async Task<ActionResult> VisitanteEtiqueta(long visitanteId)
        {
            try
            {
                var visitante = await _visitanteAppService.Obter(visitanteId);
                var atendimento = await _atendimentoAppService.Obter((long)visitante.AtendimentoId);
                var dados = EtiquetaVisitanteModel.MapearFromAtendimento(atendimento, visitante);


                var viewer = new ReportViewer();
                viewer.ProcessingMode = ProcessingMode.Local;
                viewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Relatorios\Atendimento\etiqueta_visitante.rdlc";

                SetParametrosEtiquetaVisitante(viewer, dados, visitante, atendimento);

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";

                string[] streamIds;
                Warning[] warnings;
                byte[] pdfBytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                viewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=AtendimentoEtiqueta.pdf");
                return File(pdfBytes, "application/pdf");
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }

        }

        public async Task<ActionResult> ModalEtiquetaVisitante(long visitanteId)
        {
            ModalEtiquetaVisitanteViewModel viewModel = new ModalEtiquetaVisitanteViewModel();
            viewModel.VisitanteId = visitanteId.ToString();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Etiqueta/_ModalEtiquetaVisitante.cshtml", viewModel);
        }

        public async Task<ActionResult> PulseiraInternacao(long atendimentoId)
        {
            try
            {
                var atendimento = await _atendimentoAppService.Obter((long)atendimentoId);
                var dados = PulseiraInternacaoModel.MapearFromAtendimento(atendimento);

                ReportViewer reportViewer = new ReportViewer();
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Atendimento\pulseira_internacao.rdlc");

                SetParametrosPulseira(reportViewer, dados);

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";

                string[] streamIds;
                Warning[] warnings;
                byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                reportViewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=PulseiraInternacao.pdf");
                return File(pdfBytes, "application/pdf");
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }

        }

        public async Task<ActionResult> ModalPulseiraInternacao(long atendimentoId)
        {
            ModalPulseiraViewModel viewModel = new ModalPulseiraViewModel();
            viewModel.AtendimentoId = atendimentoId.ToString();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Pulseira/_ModalPulseira.cshtml", viewModel);
        }

        public async Task<ActionResult> SolicInternacao(long atendimentoId)
        {
            try
            {
                var atendimento = await _atendimentoAppService.Obter((long)atendimentoId);
                var dados = SolicInternacaoModel.MapearFromAtendimento(atendimento);

                // Guia principal
                solic_internacao_dataset solic_internacao_dataset = new solic_internacao_dataset();
                DataTable tabela = this.ConvertToDataTable(dados.Lista, solic_internacao_dataset.Tables["solic_internacao_table"]);
                DataRow row = tabela.NewRow();
                row["Logotipo"] = atendimento.Empresa.Logotipo;
                tabela.Rows.Add(row);
                ReportDataSource dataSource = new ReportDataSource("solic_internacao_dataset", tabela);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.DataSources.Add(dataSource);
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);

                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Faturamento\Guias\InternacaoSolicitacao\guia_internacao_solic.rdlc");

                SetParametrosSolicInternacao(reportViewer, dados);

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";

                string[] streamIds;
                Warning[] warnings;
                byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                reportViewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=PulseiraInternacao.pdf");
                return File(pdfBytes, "application/pdf");
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }
        }

        public async Task<ActionResult> ModalSolicInternacao(long atendimentoId)
        {
            ModalSolicInternacaoViewModel viewModel = new ModalSolicInternacaoViewModel();
            viewModel.AtendimentoId = atendimentoId.ToString();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/SolicInternacao/_ModalSolicInternacao.cshtml", viewModel);
        }

        public async Task<ActionResult> ResumoInternacao(long atendimentoId)
        {
            try
            {
                var atendimento = await _atendimentoAppService.Obter((long)atendimentoId);
                var dados = ResumoInternacaoModel.MapearFromAtendimento(atendimento);

                // Guia principal
                Web.Relatorios.Faturamento.Guias.InternacaoResumo.resumo_internacao_dataset resumo_internacao_dataset = new Web.Relatorios.Faturamento.Guias.InternacaoResumo.resumo_internacao_dataset();
                DataTable tabela = this.ConvertToDataTable(dados.Lista, resumo_internacao_dataset.Tables["resumo_internacao_table"]);
                DataRow row = tabela.NewRow();
                row["Logotipo"] = atendimento.Empresa.Logotipo;
                //   tabela.Rows[tabela.Rows.Count - 1].Delete();
                tabela.Rows.Add(row);

                ReportDataSource dataSource = new ReportDataSource("resumo_internacao_dataset", tabela);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.DataSources.Add(dataSource);
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);

                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Faturamento\Guias\InternacaoResumo\guia_internacao_resumo.rdlc");

                SetParametrosResumoInternacao(reportViewer, dados);

                // APARENTEMENTE FALTANDO SUB-RELATORIOS

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";

                string[] streamIds;
                Warning[] warnings;
                byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                reportViewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=PulseiraInternacao.pdf");
                return File(pdfBytes, "application/pdf");
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }
        }

        public async Task<ActionResult> ModalResumoInternacao(long atendimentoId)
        {
            ModalResumoInternacaoViewModel viewModel = new ModalResumoInternacaoViewModel();
            viewModel.AtendimentoId = atendimentoId.ToString();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/ResumoINternacao/_ModalResumoInternacao.cshtml", viewModel);
        }

        //public async Task<ActionResult> GuiaAmbulatorio(long atendimentoId)
        //{
        //    try
        //    {
        //        var atendimento = await _atendimentoAppService.Obter((long)atendimentoId);
        //        var dados = GuiaAmbulatorioModel.MapearFromAtendimento(atendimento);

        //        // Guia principal
        //        Web.Relatorios.Faturamento.Guias.InternacaoResumo.resumo_internacao_dataset resumo_internacao_dataset = new Web.Relatorios.Faturamento.Guias.InternacaoResumo.resumo_internacao_dataset();
        //        DataTable tabela = this.ConvertToDataTable(dados.Lista, resumo_internacao_dataset.Tables["resumo_internacao_table"]);
        //        DataRow row = tabela.NewRow();
        //        row["Logotipo"] = atendimento.Empresa.Logotipo;
        //        tabela.Rows.Add(row);
        //        ReportDataSource dataSource = new ReportDataSource("resumo_internacao_dataset", tabela);
        //        ReportViewer reportViewer = new ReportViewer();
        //        reportViewer.LocalReport.DataSources.Add(dataSource);
        //        ScriptManager scriptManager = new ScriptManager();
        //        scriptManager.RegisterPostBackControl(reportViewer);

        //        reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Faturamento\Guias\InternacaoResumo\guia_internacao_resumo.rdlc");

        //        // SetParametrosGuiaAmbulatorio(reportViewer, dados);

        //        // APARENTEMENTE FALTANDO SUB-RELATORIOS

        //        string mimeType = string.Empty;
        //        string encoding = string.Empty;
        //        string extension = "pdf";

        //        string[] streamIds;
        //        Warning[] warnings;
        //        byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
        //        reportViewer.LocalReport.Refresh();

        //        Response.Headers.Add("Content-Disposition", "inline; filename=PulseiraInternacao.pdf");
        //        return File(pdfBytes, "application/pdf");
        //    }
        //    catch (Exception e)
        //    {
        //        e.ToString();
        //        return null;
        //    }
        //}

        public async Task<ActionResult> ModalGuiaAmbulatorio(long atendimentoId)
        {
            ModalGuiaAmbulatorioViewModel viewModel = new ModalGuiaAmbulatorioViewModel();
            viewModel.AtendimentoId = atendimentoId.ToString();

            var atd = await _atendimentoAppService.Obter(atendimentoId);

            // Id fixada pelo seed (1 = consuta)
            switch (atd.FatGuiaId)
            {
                case 1:
                    viewModel.TipoGuia = "consulta";
                    break;
                default:
                    return null;
            }

            //                    Areas\Mpa\Views\Aplicacao\Atendimentos\AmbulatorioEmergencias\Home\Guia\_ModalGuiaAmbulatorio.cshtml
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Guia/_ModalGuiaAmbulatorio.cshtml", viewModel);
        }

        public async Task<ActionResult> HonorarioIndividual(long atendimentoId)
        {
            try
            {
                var atendimento = await _atendimentoAppService.Obter((long)atendimentoId);
                var dados = HonorarioIndividualModel.MapearFromAtendimento(atendimento);

                // Guia principal
                Web.Relatorios.Faturamento.Guias.HonorarioIndividual.honorario_individual_dataset honorario_individual_dataset = new Web.Relatorios.Faturamento.Guias.HonorarioIndividual.honorario_individual_dataset();
                DataTable tabela = this.ConvertToDataTable(dados.Lista, honorario_individual_dataset.Tables["honorario_individual_table"]);
                DataRow row = tabela.NewRow();
                row["Logotipo"] = atendimento.Empresa.Logotipo;
                //   tabela.Rows[tabela.Rows.Count - 1].Delete();
                tabela.Rows.Add(row);

                ReportDataSource dataSource = new ReportDataSource("honorario_individual_dataset", tabela);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.DataSources.Add(dataSource);
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);

                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Relatorios\Faturamento\Guias\HonorarioIndividual\guia_honorario_individual.rdlc");

                SetParametrosHonorarioIndividual(reportViewer, dados);

                // APARENTEMENTE FALTANDO SUB-RELATORIOS

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = "pdf";

                string[] streamIds;
                Warning[] warnings;
                byte[] pdfBytes = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                reportViewer.LocalReport.Refresh();

                Response.Headers.Add("Content-Disposition", "inline; filename=PulseiraInternacao.pdf");
                return File(pdfBytes, "application/pdf");
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }
        }


        public async Task<ActionResult> ModalHorarioIndividual(long atendimentoId)
        {
            ModalHonorarioIndividualViewModel viewModel = new ModalHonorarioIndividualViewModel();
            viewModel.AtendimentoId = atendimentoId.ToString();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/HonorarioIndividual/_ModalHonorarioIndividual.cshtml", viewModel);
        }

        private void SetParametrosSolicInternacao(ReportViewer rv, SolicInternacaoModel dados)
        {
            ReportParameter RegistroAns = new ReportParameter("RegistroAns", dados.RegistroAns);
            ReportParameter AtendimentoRn = new ReportParameter("AtendimentoRn", dados.AtendimentoRn);
            ReportParameter NomePaciente = new ReportParameter("NomePaciente", dados.NomePaciente);
            ReportParameter NomeContratado = new ReportParameter("NomeContratado", dados.NomeContratado);
            ReportParameter RegimeInternacao = new ReportParameter("RegimeInternacao", dados.RegimeInternacao);
            ReportParameter CodigoOperadoraCnpj = new ReportParameter("CodigoOperadoraCnpj", dados.CodigoOperadoraCnpj);
            ReportParameter NomeHospital = new ReportParameter("NomeHospital", dados.NomeHospital);
            ReportParameter DataSugerInterna = new ReportParameter("DataSugerInterna", dados.DataSugerInterna);
            ReportParameter QtdDiariasSolicitadas = new ReportParameter("QtdDiariasSolicitadas", dados.QtdDiariasSolicitadas);
            ReportParameter PrevOPME = new ReportParameter("PrevOPME", dados.PrevOPME);
            ReportParameter PrevQuimio = new ReportParameter("PrevQuimio", dados.PrevQuimio);
            ReportParameter Cid1 = new ReportParameter("Cid1", dados.Cid1);
            ReportParameter Cid2 = new ReportParameter("Cid2", dados.Cid2);
            ReportParameter Cid3 = new ReportParameter("Cid3", dados.Cid3);
            ReportParameter Cid4 = new ReportParameter("Cid4", dados.Cid4);
            ReportParameter NumeroGuiaPrestador = new ReportParameter("NumeroGuiaPrestador", dados.NumeroGuiaPrestador);
            ReportParameter NumeroCarteira = new ReportParameter("NumeroCarteira", dados.NumeroCarteira);
            ReportParameter ValidadeCarteira = new ReportParameter("ValidadeCarteira", dados.ValidadeCarteira);
            ReportParameter IndicacaoClinica = new ReportParameter("IndicacaoClinica", dados.IndicacaoClinica);
            ReportParameter NomeProfissionalSolicitante = new ReportParameter("NomeProfissionalSolicitante", dados.NomeProfissionalSolicitante);
            ReportParameter ConselhoProfissional = new ReportParameter("ConselhoProfissional", dados.ConselhoProfissional);
            ReportParameter NumeroConselho = new ReportParameter("NumeroConselho", dados.NumeroConselho);
            ReportParameter UF = new ReportParameter("UF", dados.UF);
            ReportParameter CodigoCbo = new ReportParameter("CodigoCbo", dados.CodigoCbo);
            ReportParameter CodigoCnes = new ReportParameter("CodigoCnes", dados.CodigoCnes);

            rv.LocalReport.SetParameters(new ReportParameter[] {
                RegistroAns                         ,
                AtendimentoRn                       ,
                NomePaciente                        ,
                NomeContratado                      ,
                RegimeInternacao                    ,
                CodigoOperadoraCnpj                 ,
                NomeHospital                        ,
                DataSugerInterna                    ,
                QtdDiariasSolicitadas               ,
                PrevOPME                            ,
                PrevQuimio                          ,
                Cid1                                ,
                Cid2                                ,
                Cid3                                ,
                Cid4                                ,
                NumeroGuiaPrestador                 ,
                NumeroCarteira                      ,
                ValidadeCarteira                    ,
                IndicacaoClinica                    ,
                NomeProfissionalSolicitante         ,
                ConselhoProfissional                ,
                NumeroConselho                      ,
                UF                                  ,
                CodigoCbo                           ,
                CodigoCnes
            });
        }

        private void SetParametrosPulseira(ReportViewer rv, PulseiraInternacaoModel dados)
        {
            ReportParameter NomePaciente = new ReportParameter("NomePaciente", dados.NomePaciente);
            ReportParameter Nascimento = new ReportParameter("Nascimento", dados.Nascimento);
            ReportParameter CodigoAtendimento = new ReportParameter("CodigoAtendimento", dados.CodigoAtendimento);
            ReportParameter Atendimento = new ReportParameter("Atendimento", dados.Atendimento);
            ReportParameter Matricula = new ReportParameter("Matricula", dados.Matricula);
            ReportParameter Convenio = new ReportParameter("Convenio", dados.Convenio);

            rv.LocalReport.SetParameters(new ReportParameter[] {
                NomePaciente     ,
                Nascimento       ,
                CodigoAtendimento,
                Atendimento      ,
                Matricula        ,
                Convenio
            });
        }

        private void SetParametrosEtiquetaVisitante(ReportViewer rv, EtiquetaVisitanteModel dados, VisitanteDto visitante, AtendimentoDto atendimento)
        {
            ReportParameter NomePaciente = new ReportParameter("NomePaciente", dados.NomePaciente);
            ReportParameter Nascimento = new ReportParameter("Nascimento", dados.Nascimento);
            ReportParameter CodigoAtendimento = new ReportParameter("CodigoAtendimento", dados.CodigoAtendimento);
            ReportParameter Atendimento = new ReportParameter("Atendimento", dados.Atendimento);
            ReportParameter Matricula = new ReportParameter("Matricula", dados.Matricula);
            ReportParameter Convenio = new ReportParameter("Convenio", dados.Convenio);

            ReportParameter NomeVisitante = new ReportParameter("NomeVisitante", visitante.Nome);
            ReportParameter Documento = new ReportParameter("Documento", visitante.Documento);
            ReportParameter DataEntrada = new ReportParameter("DataEntrada", ((DateTime)visitante.DataEntrada).ToString("dd/MM/yyyy"));
            ReportParameter Fornecedor = new ReportParameter("Fornecedor", visitante.Fornecedor?.Descricao);
            ReportParameter Local = new ReportParameter("Local", atendimento.UnidadeOrganizacional?.Descricao);

            rv.LocalReport.SetParameters(new ReportParameter[] {
                NomePaciente     ,
                Nascimento       ,
                CodigoAtendimento,
                Atendimento      ,
                Matricula        ,
                Convenio,

                NomeVisitante,
                Documento,
                DataEntrada,
                Fornecedor,
                Local
            });
        }

        private void SetParametrosResumoInternacao(ReportViewer rv, ResumoInternacaoModel dados)
        {
            ReportParameter NomePaciente = new ReportParameter("NomePaciente", dados.NomePaciente);
            ReportParameter Matricula = new ReportParameter("Matricula", dados.Matricula);
            ReportParameter RegistroANS = new ReportParameter("RegistroANS", dados.RegistroANS);
            ReportParameter ValidadeCarteira = new ReportParameter("ValidadeCarteira", dados.ValidadeCarteira);
            ReportParameter Senha = new ReportParameter("Senha", dados.Senha);
            ReportParameter CodCNES = new ReportParameter("CodCNES", dados.CodCNES);
            ReportParameter DataAutorizacao = new ReportParameter("DataAutorizacao", dados.DataAutorizacao);
            ReportParameter NomeContratado = new ReportParameter("NomeContratado", dados.NomeContratado);
            ReportParameter ValidadeSenha = new ReportParameter("ValidadeSenha", dados.ValidadeSenha);
            ReportParameter NumeroGuia = new ReportParameter("NumeroGuia", dados.NumeroGuia);
            ReportParameter Cid1 = new ReportParameter("Cid1", dados.Cid1);
            ReportParameter Cid2 = new ReportParameter("Cid2", dados.Cid2);
            ReportParameter Cid3 = new ReportParameter("Cid3", dados.Cid3);
            ReportParameter Cid4 = new ReportParameter("Cid4", dados.Cid4);
            ReportParameter CodOperadora = new ReportParameter("CodOperadora", dados.CodOperadora);
            ReportParameter CaraterAtendimento = new ReportParameter("CaraterAtendimento", dados.CaraterAtendimento);
            ReportParameter TipoFaturamento = new ReportParameter("TipoFaturamento", dados.TipoFaturamento);
            ReportParameter DataIniFaturamento = new ReportParameter("DataIniFaturamento", dados.DataIniFaturamento);
            ReportParameter DataFimFaturamento = new ReportParameter("DataFimFaturamento", dados.DataFimFaturamento);
            ReportParameter HoraIniFaturamento = new ReportParameter("HoraIniFaturamento", dados.HoraIniFaturamento);
            ReportParameter HoraFimFaturamento = new ReportParameter("HoraFimFaturamento", dados.HoraFimFaturamento);
            ReportParameter TipoInternacao = new ReportParameter("TipoInternacao", dados.TipoInternacao);
            ReportParameter RegimeInternacao = new ReportParameter("RegimeInternacao", dados.RegimeInternacao);
            ReportParameter TotalProcedimentos = new ReportParameter("TotalProcedimentos", dados.TotalProcedimentos);
            ReportParameter TotalDiaria = new ReportParameter("TotalDiaria", dados.TotalDiaria);
            ReportParameter TotalTaxasAlugueis = new ReportParameter("TotalTaxasAlugueis", dados.TotalTaxasAlugueis);
            ReportParameter TotalMateriais = new ReportParameter("TotalMateriais", dados.TotalMateriais);
            ReportParameter TotalOpme = new ReportParameter("TotalOpme", dados.TotalOpme);
            ReportParameter TotalMedicamentos = new ReportParameter("TotalMedicamentos", dados.TotalMedicamentos);
            ReportParameter TotalGasesMedicinais = new ReportParameter("TotalGasesMedicinais", dados.TotalGasesMedicinais);
            ReportParameter TotalGeral = new ReportParameter("TotalGeral", dados.TotalGeral);
            ReportParameter RN = new ReportParameter("RN", dados.RN ? "S" : "N");




            rv.LocalReport.SetParameters(new ReportParameter[] {
                NomePaciente            ,
                Matricula               ,
                RegistroANS             ,
                ValidadeCarteira        ,
                Senha                   ,
                CodCNES                 ,
                DataAutorizacao         ,
                NomeContratado          ,
                ValidadeSenha           ,
                NumeroGuia              ,
                Cid1                    ,
                Cid2                    ,
                Cid3                    ,
                Cid4                    ,
                CodOperadora            ,
                CaraterAtendimento      ,
                TipoFaturamento         ,
                DataIniFaturamento      ,
                DataFimFaturamento      ,
                HoraIniFaturamento      ,
                HoraFimFaturamento      ,
                TipoInternacao          ,
                RegimeInternacao        ,
                TotalProcedimentos      ,
                TotalDiaria             ,
                TotalTaxasAlugueis      ,
                TotalMateriais          ,
                TotalOpme               ,
                TotalMedicamentos       ,
                TotalGasesMedicinais    ,
                TotalGeral              ,
                RN
            });
        }

        private void SetParametrosHonorarioIndividual(ReportViewer rv, HonorarioIndividualModel dados)
        {
            ReportParameter RegistroANS = new ReportParameter("RegistroANS", dados.RegistroANS);
            ReportParameter NumeroGuiaSolicitacao = new ReportParameter("NumeroGuiaSolicitacao", dados.NumeroGuiaSolicitacao);
            ReportParameter Senha = new ReportParameter("Senha", dados.Senha);
            ReportParameter NumeroGuiaOperadora = new ReportParameter("NumeroGuiaOperadora", dados.NumeroGuiaOperadora);
            ReportParameter Matricula = new ReportParameter("Matricula", dados.Matricula);
            ReportParameter NomePaciente = new ReportParameter("NomePaciente", dados.NomePaciente);
            ReportParameter RN = new ReportParameter("RN", dados.RN ? "S" : "N");
            ReportParameter CodOperadora = new ReportParameter("CodOperadora", dados.CodOperadora);
            ReportParameter NomeHospitalLocal = new ReportParameter("NomeHospitalLocal", dados.NomeHospitalLocal);
            ReportParameter CodCNES = new ReportParameter("CodCNES", dados.CodCNES);
            ReportParameter NomeContratado = new ReportParameter("NomeContratado", dados.NomeContratado);
            ReportParameter DataIniFaturamento = new ReportParameter("DataIniFaturamento", dados.DataIniFaturamento);
            ReportParameter DataFimFaturamento = new ReportParameter("DataFimFaturamento", dados.DataFimFaturamento);
            ReportParameter DataEmissao = new ReportParameter("DataEmissao", dados.DataEmissao);
            ReportParameter TotaGeral = new ReportParameter("TotalGeral", dados.TotaGeral);




            rv.LocalReport.SetParameters(new ReportParameter[] {
                RegistroANS                ,
                NumeroGuiaSolicitacao      ,
                Senha                      ,
                NumeroGuiaOperadora        ,
                Matricula                  ,
                NomePaciente               ,
                RN                         ,
                CodOperadora               ,
                NomeHospitalLocal          ,
                CodCNES                    ,
                NomeContratado             ,
                DataIniFaturamento         ,
                DataFimFaturamento         ,
                DataEmissao                ,
                TotaGeral
            });
        }

        public DataTable ConvertToDataTable<T>(IList<T> data, DataTable table)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            if (data != null)
            {
                foreach (T item in data)
                {
                    try
                    {
                        DataRow row = table.NewRow();
                        foreach (PropertyDescriptor prop in properties)
                            try
                            {
                                if (prop.GetValue(item) != null)
                                {
                                    if (prop.PropertyType == typeof(string))
                                    {
                                        if ((string)prop.GetValue(item) != string.Empty)
                                        {
                                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                                        }
                                    }
                                    else
                                    {
                                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                                    }
                                }
                                else
                                {
                                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                                }


                            }
                            catch (Exception exs)
                            {

                            }
                        table.Rows.Add(row);
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
            }

            return table;
        }


        public class SolicInternacaoModel
        {
            public string RegistroAns { get; set; }
            public string AtendimentoRn { get; set; }
            public string NomePaciente { get; set; }
            public string NomeContratado { get; set; }
            public string RegimeInternacao { get; set; }
            public string CodigoOperadoraCnpj { get; set; }
            public string NomeHospital { get; set; }
            public string DataSugerInterna { get; set; }
            public string QtdDiariasSolicitadas { get; set; }
            public string PrevOPME { get; set; }
            public string PrevQuimio { get; set; }
            public string Cid1 { get; set; }
            public string Cid2 { get; set; }
            public string Cid3 { get; set; }
            public string Cid4 { get; set; }
            public string NumeroGuiaPrestador { get; set; }
            public string NumeroCarteira { get; set; }
            public string ValidadeCarteira { get; set; }
            public string IndicacaoClinica { get; set; }
            public string NomeProfissionalSolicitante { get; set; }
            public string ConselhoProfissional { get; set; }
            public string NumeroConselho { get; set; }
            public string UF { get; set; }
            public string CodigoCbo { get; set; }
            public string CodigoCnes { get; set; }


            public List<string> Lista { get; set; }

            public SolicInternacaoModel()
            {
                Lista = new List<string>();
            }

            public static SolicInternacaoModel MapearFromAtendimento(AtendimentoDto atendimento)
            {
                var solic = new SolicInternacaoModel();

                solic.RegistroAns = atendimento.Convenio?.RegistroANS;
                solic.AtendimentoRn = "";
                solic.NomePaciente = atendimento.Paciente.NomeCompleto;
                solic.NomeContratado = atendimento.Empresa?.NomeFantasia;
                solic.RegimeInternacao = atendimento.AtendimentoTipo?.Codigo;
                solic.CodigoOperadoraCnpj = atendimento.Empresa?.Cnpj;
                solic.NomeHospital = atendimento.Empresa?.NomeFantasia;
                solic.DataSugerInterna = DateTime.Now.ToString();
                solic.QtdDiariasSolicitadas = "";
                solic.PrevOPME = "";
                solic.PrevQuimio = "";
                solic.Cid1 = "";
                solic.Cid2 = "";
                solic.Cid3 = "";
                solic.Cid4 = "";
                solic.NumeroGuiaPrestador = atendimento.GuiaNumero;
                solic.NumeroCarteira = atendimento.Matricula;

                if (atendimento.ValidadeCarteira != null)
                {
                    solic.ValidadeCarteira = ((DateTime)atendimento.ValidadeCarteira).ToString("dd/MM/yy");
                }

                solic.IndicacaoClinica = "";
                solic.NomeProfissionalSolicitante = atendimento.Medico?.NomeCompleto;
                solic.ConselhoProfissional = atendimento.Medico?.Conselho?.Descricao;
                solic.NumeroConselho = atendimento.Medico?.NumeroConselho.ToString();
                solic.UF = atendimento.Medico?.Conselho?.Uf;
                solic.CodigoCbo = atendimento.Especialidade?.SisCbo?.Codigo;
                solic.CodigoCnes = atendimento.Medico?.Cns;

                return solic;
            }
        }

        public class PulseiraInternacaoModel
        {
            public string NomePaciente { get; set; }
            public string Nascimento { get; set; }
            public string CodigoAtendimento { get; set; }
            public string Atendimento { get; set; }
            public string Matricula { get; set; }
            public string Convenio { get; set; }

            public List<string> Lista { get; set; }

            public PulseiraInternacaoModel()
            {
                Lista = new List<string>();
            }

            public static PulseiraInternacaoModel MapearFromAtendimento(AtendimentoDto atendimento)
            {
                var pulseira = new PulseiraInternacaoModel();

                pulseira.NomePaciente = atendimento.Paciente?.NomeCompleto;

                if (atendimento.Paciente.Nascimento != null)
                {
                    pulseira.Nascimento = ((DateTime)atendimento.Paciente.Nascimento).ToString("dd/MM/yy");
                }

                pulseira.CodigoAtendimento = atendimento.Codigo;
                pulseira.Atendimento = atendimento.DataRegistro.ToString();
                pulseira.Matricula = atendimento.Matricula;
                pulseira.Convenio = atendimento.Convenio?.NomeFantasia;

                return pulseira;
            }
        }

        public class EtiquetaVisitanteModel
        {
            public string NomePaciente { get; set; }
            public string Nascimento { get; set; }
            public string CodigoAtendimento { get; set; }
            public string Atendimento { get; set; }
            public string Matricula { get; set; }
            public string Convenio { get; set; }


            public string NomeVisitante { get; set; }
            public string Documento { get; set; }
            public string Fornecedor { get; set; }
            public string Entrada { get; set; }
            public string Local { get; set; }

            public List<string> Lista { get; set; }

            public EtiquetaVisitanteModel()
            {
                Lista = new List<string>();
            }

            public static EtiquetaVisitanteModel MapearFromAtendimento(AtendimentoDto atendimento, VisitanteDto visitante)
            {
                var etiqueta = new EtiquetaVisitanteModel();

                etiqueta.NomePaciente = atendimento.Paciente?.NomeCompleto;

                if (atendimento.Paciente.Nascimento != null)
                {
                    etiqueta.Nascimento = ((DateTime)atendimento.Paciente?.Nascimento).ToString("dd/MM/yy");
                }

                etiqueta.CodigoAtendimento = atendimento.Codigo;
                etiqueta.Atendimento = atendimento.DataRegistro.ToString();
                etiqueta.Matricula = atendimento.Matricula;
                etiqueta.Convenio = atendimento.Convenio?.NomeFantasia;
                etiqueta.NomeVisitante = visitante.Nome;
                etiqueta.Documento = visitante.Documento;
                etiqueta.Fornecedor = visitante.Fornecedor?.Descricao;
                etiqueta.Entrada = ((DateTime)visitante.DataEntrada).ToString("dd/MM/yyyy");
                etiqueta.Local = atendimento.UnidadeOrganizacional?.Descricao;
                
                return etiqueta;
            }
        }

        public class ResumoInternacaoModel
        {
            public string NomePaciente { get; set; }
            public string Matricula { get; set; }
            public string RegistroANS { get; set; }
            public string ValidadeCarteira { get; set; }
            public string Senha { get; set; }
            public string DataAutorizacao { get; set; }
            public string CodCNES { get; set; }
            public string NomeContratado { get; set; }
            public string ValidadeSenha { get; set; }
            public string NumeroGuia { get; set; }
            public string Cid1 { get; set; }
            public string Cid2 { get; set; }
            public string Cid3 { get; set; }
            public string Cid4 { get; set; }
            public string CodOperadora { get; set; }
            public string CaraterAtendimento { get; set; }
            public string TipoFaturamento { get; set; }
            public string DataIniFaturamento { get; set; }
            public string DataFimFaturamento { get; set; }
            public string HoraIniFaturamento { get; set; }
            public string HoraFimFaturamento { get; set; }
            public string TipoInternacao { get; set; }
            public string RegimeInternacao { get; set; }
            public string TotalProcedimentos { get; set; }
            public string TotalDiaria { get; set; }
            public string TotalTaxasAlugueis { get; set; }
            public string TotalMateriais { get; set; }
            public string TotalOpme { get; set; }
            public string TotalMedicamentos { get; set; }
            public string TotalGasesMedicinais { get; set; }
            public string TotalGeral { get; set; }
            public bool RN { get; set; }

            public List<string> Lista { get; set; }

            public ResumoInternacaoModel()
            {
                Lista = new List<string>();
            }

            public static ResumoInternacaoModel MapearFromAtendimento(AtendimentoDto atendimento)
            {
                var model = new ResumoInternacaoModel();

                model.NomePaciente = atendimento.Paciente?.NomeCompleto;
                model.RegistroANS = atendimento.Convenio?.RegistroANS;
                model.Matricula = atendimento.Matricula;
                model.Senha = atendimento.Senha;
                model.DataAutorizacao = ((DateTime)atendimento.DataAutorizacao).ToString("dd/MM/yyyy");
                model.CodCNES = atendimento.Empresa?.Cnes.ToString();
                model.NomeContratado = atendimento.Empresa?.NomeFantasia;
                model.ValidadeSenha = ((DateTime)atendimento.ValidadeSenha).ToString("dd/MM/yyyy");
                model.NumeroGuia = atendimento.GuiaNumero;
                model.Cid1 = "";
                model.Cid2 = "";
                model.Cid3 = "";
                model.Cid4 = "";
                model.CodOperadora = "";
                model.CaraterAtendimento = "";
                model.TipoFaturamento = "";
                model.DataIniFaturamento = "";
                model.DataFimFaturamento = "";
                model.HoraIniFaturamento = "";
                model.HoraFimFaturamento = "";
                model.TipoInternacao = "";
                model.RegimeInternacao = "";
                model.TotalProcedimentos = "";
                model.TotalDiaria = "";
                model.TotalTaxasAlugueis = "";
                model.TotalMateriais = "";
                model.TotalOpme = "";
                model.TotalMedicamentos = "";
                model.TotalGasesMedicinais = "";
                model.TotalGeral = "";



                if (atendimento.ValidadeCarteira != null)
                {
                    model.ValidadeCarteira = ((DateTime)atendimento.ValidadeCarteira).ToString("dd/MM/yyyy");
                }

                if (atendimento.Paciente.Nascimento.HasValue)
                {
                    var idade = DateDifference.GetExtendedDifference((DateTime)atendimento.Paciente.Nascimento);
                    model.RN = (idade.Ano == 0 && idade.Mes == 0 && idade.Dia <= 30);
                }


                //model.Convenio = atendimento.Convenio?.NomeFantasia;
                //if (atendimento.Paciente.Nascimento != null)
                //{
                //    model.Nascimento = ((DateTime)atendimento.Paciente?.Nascimento).ToString("dd/MM/yy");
                //}

                //model.CodigoAtendimento = atendimento.Codigo;
                //model.Atendimento = atendimento.DataRegistro.ToString();


                return model;
            }
        }

        public class HonorarioIndividualModel
        {
            public string RegistroANS { get; set; }
            public string NumeroGuiaSolicitacao { get; set; }
            public string Senha { get; set; }
            public string NumeroGuiaOperadora { get; set; }
            public string Matricula { get; set; }
            public string NomePaciente { get; set; }
            public bool RN { get; set; }
            public string CodOperadora { get; set; }
            public string NomeHospitalLocal { get; set; }
            public string CodCNES { get; set; }
            public string NomeContratado { get; set; }
            public string DataIniFaturamento { get; set; }
            public string DataFimFaturamento { get; set; }
            public string DataEmissao { get; set; }
            public string TotaGeral { get; set; }

            public List<string> Lista { get; set; }

            public HonorarioIndividualModel()
            {
                Lista = new List<string>();
            }

            public static HonorarioIndividualModel MapearFromAtendimento(AtendimentoDto atendimento)
            {
                var model = new HonorarioIndividualModel();

                model.RegistroANS = atendimento.Convenio?.RegistroANS;
                model.NumeroGuiaSolicitacao = "";
                model.Senha = atendimento.Senha;
                model.NumeroGuiaOperadora = atendimento.GuiaNumero;
                model.Matricula = atendimento.Matricula;
                model.NomePaciente = atendimento.Paciente.NomeCompleto;
                model.CodOperadora = "";
                model.NomeHospitalLocal = atendimento.Empresa?.NomeFantasia;
                model.CodCNES = atendimento.Empresa?.Cnes.ToString();
                model.NomeContratado = atendimento.Medico?.NomeCompleto;
                model.DataIniFaturamento = "";
                model.DataFimFaturamento = "";
                model.DataEmissao = ((DateTime)atendimento.DataRegistro).ToString("dd/MM/yyyy");
                model.TotaGeral = "";


                if (atendimento.Paciente.Nascimento.HasValue)
                {
                    var idade = DateDifference.GetExtendedDifference((DateTime)atendimento.Paciente.Nascimento);
                    model.RN = (idade.Ano == 0 && idade.Mes == 0 && idade.Dia <= 30);
                }

                return model;
            }
        }
    }
}