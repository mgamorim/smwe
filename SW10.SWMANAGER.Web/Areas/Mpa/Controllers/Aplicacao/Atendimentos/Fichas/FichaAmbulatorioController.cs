﻿#region Usings
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.Reporting.WebForms;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Manutencoes.MailingTemplates;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Views.Aplicacao.Atendimentos.AmbulatorioEmergencias.Ficha;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
#endregion usings.

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos.Fichas
{
    public class FichaAmbulatorioController : SWMANAGERControllerBase
    {
        #region Dependencias
        private readonly IContaAppService                     _contaMedicaAppService;
        private readonly IPacienteAppService                  _pacienteAppService;
        private readonly IFaturamentoContaItemAppService      _contaItemAppService;
        private readonly IAtendimentoAppService               _atendimentoAppService;
        private readonly IUserAppService                      _userAppService;
        private readonly IFaturamentoConfigConvenioAppService _configConvenioAppService;
        private readonly IFaturamentoGuiaAppService           _guiaAppService;
        private readonly IMailingTemplateAppService           _mailingTemplateAppService;
        private ISessionAppService                            _sessionAppService;

        // Dados Guia Spsadt
        List<FaturamentoContaItemDto> _itensGuiaPrincipal = new List<FaturamentoContaItemDto>();

        public FichaAmbulatorioController(
            IContaAppService                     contaMedicaAppService,
            IPacienteAppService                  pacienteAppService,
            IFaturamentoContaItemAppService      contaItemAppService,
            IAtendimentoAppService               atendimentoAppService,
            ISessionAppService                   sessionAppService,
            IUserAppService                      userAppService,
            IFaturamentoConfigConvenioAppService configConvenioAppService,
            IMailingTemplateAppService           mailingTemplateAppService,
            IFaturamentoGuiaAppService           guiaAppService
            )
        {
            _contaMedicaAppService     = contaMedicaAppService;
            _pacienteAppService        = pacienteAppService;
            _contaItemAppService       = contaItemAppService;
            _atendimentoAppService     = atendimentoAppService;
            _sessionAppService         = sessionAppService;
            _userAppService            = userAppService;
            _configConvenioAppService  = configConvenioAppService;
            _mailingTemplateAppService = mailingTemplateAppService;
            _guiaAppService            = guiaAppService;
        }
        #endregion dependencias.

        public async Task<ActionResult> ModalFichaAmbulatorio(long atendimentoId)
        {
            ModalFichaAmbulatorioViewModel viewModel = new ModalFichaAmbulatorioViewModel
            {
                AtendimentoId = atendimentoId.ToString()
                // ,
                // FichaPdf = await GerarFichaInternacao(atendimentoId) as FileContentResult
            };

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Ficha/_ModalFichaAmbulatorio.cshtml", viewModel);
        }


        public async Task<ActionResult> GerarFichaAmbulatorio(long atendimentoId)
        {
            var atendimento = await _atendimentoAppService.Obter(atendimentoId);
            var dados = FichaAmbulatorioModel.MapearFromAtendimento(atendimento);
           
            try
            {
                // Guia principal
                ficha_ambulatorio_dataset ficha_amb_dataset = new ficha_ambulatorio_dataset();
                DataTable tabela = this.ConvertToDataTable(dados.Lista, ficha_amb_dataset.Tables["ficha_ambulatorio_table"]);//precisa?
                DataRow row = tabela.NewRow();
                row["Logotipo"] = atendimento.Empresa.Logotipo;
                tabela.Rows.Add(row);
                ReportDataSource dataSource = new ReportDataSource("ficha_ambulatorio_dataset", tabela);
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.LocalReport.DataSources.Add(dataSource);
                ScriptManager scriptManager = new ScriptManager();
                scriptManager.RegisterPostBackControl(reportViewer);
                reportViewer.LocalReport.ReportPath = string.Concat(Server.MapPath("~"), @"\Areas\Mpa\Views\Aplicacao\Atendimentos\AmbulatorioEmergencias\Ficha\ficha.rdlc");
                SetParametrosFicha(reportViewer, dados);
                
                // Renderizar Png
                string mime, enc, ext;
                string[] streams;
                Warning[] ws;
                string deviceInfo = "<DeviceInfo>" +
                                    "<OutputFormat>png</OutputFormat>"   +
                                    "<PageWidth>7.2in</PageWidth>"       +
                                    "<PageHeight>4.5in</PageHeight>"     +
                                    "<MarginTop>0.1in</MarginTop>"       +
                                    "<MarginLeft>0.1in</MarginLeft>"     +
                                    "<MarginRight>0.1in</MarginRight>"   +
                                    "<MarginBottom>0.1in</MarginBottom>" +
                                    "</DeviceInfo>";

                byte[] bytes = reportViewer.LocalReport.Render("Image", deviceInfo, out mime, out enc, out ext, out streams, out ws);
                byte[] pdfOut;
                // iText - Gerar pdf e anexar report_png

                // Margens
                float ml = 1f;
                float mr = 1f;
                float mt = 1f;
                float mb = 1f;

                using (Document documento = new Document(PageSize.A4, ml, mr, mt, mb))
                using (MemoryStream msOutput = new MemoryStream())
                using (PdfWriter writer = PdfWriter.GetInstance(documento, msOutput))
                {
                    writer.CloseStream = false;
                    documento.Open();

                    // Report_png
                    Image img = Image.GetInstance(bytes);
                    img.ScalePercent(90f);
                    img.ScaleToFit(documento.PageSize.Width, documento.PageSize.Height / 2);
                    img.Alignment = Element.ALIGN_CENTER;
                    documento.Add(img);

                    // Linha horizontal
                    Font lineFont = FontFactory.GetFont("Verdana", 14, Font.BOLD, BaseColor.BLACK);
                    Paragraph line = new Paragraph("__________________________________________________________\n\n", lineFont);
                    line.Alignment = Element.ALIGN_CENTER;
                    documento.Add(line);
                    
                    // CONFIG PRONTUARIO
                    //var email = await _mailingTemplateAppService.Obter(2); // id 1 temp para teste
                    
                    //// XML Worker
                    //var w = XMLWorkerHelper.GetInstance();

                    //using (var stream = GenerateStreamFromString(email.ContentTemplate))
                    //{
                    //    w.ParseXHtml(writer, documento, stream, System.Text.Encoding.UTF8);
                    //}
                    // Fim - XML Worker
                    // Fim - mailing template

                    documento.Close();
                    pdfOut = msOutput.ToArray();
                }
                // Fim - iText
                
                Response.Headers.Add("Content-Disposition", "inline; filename=ficha_amb.pdf");
                return File(pdfOut, "application/pdf");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return null;
        }

        // Metodo auxiliar String to Stream

        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        //  Fim - metodo auxiliar String to Stream

        public void SetParametrosFicha(ReportViewer rv, FichaAmbulatorioModel dados)
        {
            ReportParameter Paciente = new ReportParameter("Paciente", dados.Paciente);
            ReportParameter Usuario = new ReportParameter("Usuario", dados.Usuario);
            ReportParameter Empresa           = new ReportParameter("Empresa",           dados.Empresa);
            ReportParameter DataHora          = new ReportParameter("DataHora",          dados.DataHora);
            ReportParameter CodigoPaciente    = new ReportParameter("CodigoPaciente",    dados.CodigoPaciente);
            ReportParameter DataAtendimento   = new ReportParameter("DataAtendimento",   dados.DataAtendimento);
            ReportParameter Sexo              = new ReportParameter("Sexo",              dados.Sexo);
            ReportParameter Nascimento        = new ReportParameter("Nascimento",        dados.Nascimento);
            ReportParameter Identidade        = new ReportParameter("Identidade",        dados.Identidade);
            ReportParameter Cpf               = new ReportParameter("Cpf",               dados.Cpf);
            ReportParameter EstadoCivil       = new ReportParameter("EstadoCivil",       dados.EstadoCivil);
            ReportParameter Complemento       = new ReportParameter("Complemento",       dados.Complemento);
            ReportParameter Cidade            = new ReportParameter("Cidade",            dados.Cidade);
            ReportParameter Telefone          = new ReportParameter("Telefone",          dados.Telefone);
            ReportParameter Profissao         = new ReportParameter("Profissao",         dados.Profissao);
            ReportParameter Numero            = new ReportParameter("Numero",            dados.Numero);
            ReportParameter Pais              = new ReportParameter("Pais",              dados.Pais);
            ReportParameter Cep               = new ReportParameter("Cep",               dados.Cep);
            ReportParameter CodigoAtendimento = new ReportParameter("CodigoAtendimento", dados.CodigoAtendimento);
            ReportParameter DataAlta          = new ReportParameter("DataAlta",          dados.DataAlta);
            ReportParameter Alta              = new ReportParameter("Alta",              dados.Alta);
            ReportParameter Matricula         = new ReportParameter("Matricula",         dados.Matricula);
            ReportParameter Validade          = new ReportParameter("DataValidade",      dados.Validade);
            ReportParameter DataPagto         = new ReportParameter("DataPagto",         dados.DataPagto);
            ReportParameter IdAcompanahante   = new ReportParameter("IdAcompanhante",    dados.IdAcompanahante);
            ReportParameter CodDep            = new ReportParameter("CodDep",            dados.CodDep);
            ReportParameter Endereco          = new ReportParameter("Endereco",          dados.Endereco);
            ReportParameter Bairro            = new ReportParameter("Bairro",            dados.Bairro);
            ReportParameter Estado            = new ReportParameter("Estado",            dados.Estado);
            ReportParameter Nacionalidade     = new ReportParameter("Nacionalidade",     dados.Nacionalidade);
            ReportParameter Filiacao          = new ReportParameter("Filiacao",          dados.Filiacao);
            ReportParameter Medico            = new ReportParameter("Medico",            dados.Medico);
            ReportParameter Especialidade     = new ReportParameter("Especialidade",     dados.Especialidade);
            ReportParameter IndicadoPor       = new ReportParameter("IndicadoPor",       dados.IndicadoPor);
            ReportParameter Origem            = new ReportParameter("Origem",            dados.Origem);
            ReportParameter Tratamento        = new ReportParameter("Tratamento",        dados.Tratamento);
            ReportParameter Convenio          = new ReportParameter("Convenio",          dados.Convenio);
            ReportParameter Plano             = new ReportParameter("Plano",             dados.Plano);
            ReportParameter Guia              = new ReportParameter("Guia",              dados.Guia);
            ReportParameter NumeroGuia        = new ReportParameter("NumeroGuia",        dados.NumeroGuia);
            ReportParameter Titular           = new ReportParameter("Titular",           dados.Titular);


            rv.LocalReport.SetParameters(new ReportParameter[] {

                Paciente ,
                Usuario          ,
                Empresa          ,
                        DataHora         ,
                        CodigoPaciente   ,
                        DataAtendimento  ,
                        Sexo             ,
                        Nascimento       ,
                        Identidade       ,
                        Cpf              ,
                        EstadoCivil      ,
                        Complemento      ,
                        Cidade           ,
                        Telefone         ,
                        Profissao        ,
                        Numero           ,
                        Pais             ,
                        Cep              ,
                        CodigoAtendimento,
                        DataAlta         ,
                        Alta             ,
                        Matricula        ,
                        Validade         ,
                        DataPagto        ,
                        IdAcompanahante  ,
                        CodDep           ,
                        Endereco         ,
                        Bairro           ,
                        Estado           ,
                        Nacionalidade    ,
                        Filiacao         ,
                        Medico           ,
                        Especialidade    ,
                        IndicadoPor      ,
                        Origem           ,
                        Tratamento       ,
                        Convenio         ,
                        Plano            ,
                        Guia             ,
                        NumeroGuia       ,
                        Titular
            });
        }

        public DataTable ConvertToDataTable<T>(IList<T> data, DataTable table)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));

            if (data != null)
            {
                foreach (T item in data)
                {
                    try
                    {
                        DataRow row = table.NewRow();
                        foreach (PropertyDescriptor prop in properties)
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        table.Rows.Add(row);
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
            }

            return table;
        }
    }

    public class FichaAmbulatorioInput
    {
        public long AtendimentoId { get; set; }
    }

    public class FichaAmbulatorioModel
    {
        public string Paciente          { get; set; }
        public string Usuario           { get; set; }
        public string Empresa           { get; set; }
        public string DataHora          { get; set; }
        public string CodigoPaciente    { get; set; }
        public string DataAtendimento   { get; set; }
        public string Sexo              { get; set; }
        public string Nascimento        { get; set; }
        public string Identidade        { get; set; }
        public string Cpf               { get; set; }
        public string EstadoCivil       { get; set; }
        public string Complemento       { get; set; }
        public string Cidade            { get; set; }
        public string Telefone          { get; set; }
        public string Profissao         { get; set; }
        public string Numero            { get; set; }
        public string Pais              { get; set; }
        public string Cep               { get; set; }
        public string CodigoAtendimento { get; set; }
        public string DataAlta          { get; set; }
        public string Alta              { get; set; }
        public string Matricula         { get; set; }
        public string Validade          { get; set; }
        public string DataPagto         { get; set; }
        public string IdAcompanahante   { get; set; }
        public string CodDep            { get; set; }
        public string Endereco          { get; set; }
        public string Bairro            { get; set; }
        public string Estado            { get; set; }
        public string Nacionalidade     { get; set; }
        public string Filiacao          { get; set; }
        public string Medico            { get; set; }
        public string Especialidade     { get; set; }
        public string IndicadoPor       { get; set; }
        public string Origem            { get; set; }
        public string Tratamento        { get; set; }
        public string Convenio          { get; set; }
        public string Plano             { get; set; }
        public string Guia              { get; set; }
        public string NumeroGuia        { get; set; }
        public string Titular           { get; set; }

        public static FichaAmbulatorioModel MapearFromAtendimento(AtendimentoDto atendimento)
        {
            var ficha = new FichaAmbulatorioModel();

            ficha.Paciente = atendimento.Paciente?.NomeCompleto;
            ficha.Empresa = atendimento.Empresa?.NomeFantasia;
            ficha.DataHora = atendimento.DataRegistro.ToString("dd/MM/yyyy hh:mm");
            ficha.CodigoPaciente = atendimento.Paciente.Codigo;
            ficha.DataAtendimento = atendimento.DataRegistro.ToString("dd/MM/yyyy hh:mm");
            ficha.Sexo = atendimento.Paciente.Sexo != null ? atendimento.Paciente.Sexo.Descricao : atendimento.Paciente.SisPessoa.Sexo?.Descricao;
            ficha.Nascimento = !atendimento.Paciente.Nascimento.HasValue ? "" : ((DateTime)atendimento.Paciente.Nascimento).ToString("dd/MM/yy");
            ficha.Identidade = atendimento.Paciente.Rg;
            ficha.Cpf = atendimento.Paciente.Cpf;
            ficha.EstadoCivil = atendimento.Paciente.EstadoCivil != null ? atendimento.Paciente.EstadoCivil?.Descricao : atendimento.Paciente.SisPessoa.EstadoCivil?.Descricao;
            ficha.Complemento = atendimento.Paciente.Complemento != null ? atendimento.Paciente.Complemento : atendimento.Paciente.SisPessoa.Enderecos?[0].Complemento;
            ficha.Pais = atendimento.Paciente.Pais != null ? atendimento.Paciente.Pais?.Nome : ( (atendimento.Paciente.SisPessoa?.Enderecos?.Count > 0) ? atendimento.Paciente.SisPessoa.Enderecos?[0].Pais?.Nome : ""  );
            ficha.Estado = atendimento.Paciente.Estado != null ? atendimento.Paciente.Estado?.Uf : ((atendimento.Paciente.SisPessoa?.Enderecos?.Count > 0) ? atendimento.Paciente.SisPessoa.Enderecos?[0].Estado?.Nome : "");
            ficha.Cidade = atendimento.Paciente.Cidade != null ? atendimento.Paciente.Cidade?.Nome : ((atendimento.Paciente.SisPessoa?.Enderecos?.Count > 0) ? atendimento.Paciente.SisPessoa.Enderecos?[0].Cidade?.Nome : "");
            ficha.Endereco = atendimento.Paciente.Logradouro != null ? atendimento.Paciente.Logradouro : ((atendimento.Paciente.SisPessoa?.Enderecos?.Count > 0) ? atendimento.Paciente.SisPessoa.Enderecos?[0].Logradouro : "");
            ficha.Bairro = atendimento.Paciente.Bairro != null ? atendimento.Paciente.Bairro : ((atendimento.Paciente.SisPessoa?.Enderecos?.Count > 0) ? atendimento.Paciente.SisPessoa.Enderecos?[0].Bairro : "");
            ficha.Nacionalidade = atendimento.Paciente.Nacionalidade != null ? atendimento.Paciente.Nacionalidade?.Descricao : atendimento.Paciente.SisPessoa.Nacionalidade?.Descricao;
            ficha.Cep = atendimento.Paciente.Cep != null ? atendimento.Paciente.Cep : atendimento.Paciente.SisPessoa.Enderecos?[0].Cep;
            ficha.Telefone = atendimento.Paciente.Telefone1;
            ficha.Profissao = atendimento.Paciente.Profissao != null ? atendimento.Paciente.Profissao.Descricao : atendimento.Paciente.SisPessoa.Profissao?.Descricao;
            ficha.Numero = atendimento.Paciente.Numero;
            ficha.CodigoAtendimento = atendimento.Codigo;
            ficha.DataAlta = atendimento.DataAlta.ToString();
            ficha.Alta = "";
            ficha.Matricula = atendimento.Matricula;
            ficha.Validade = "";
            ficha.DataPagto = "";
            ficha.IdAcompanahante = atendimento.RgResponsavel;
            ficha.CodDep = "";
            ficha.Filiacao = atendimento.Paciente.NomeMae;
            ficha.Medico = atendimento.Medico.NomeCompleto;
            ficha.Especialidade = atendimento.Especialidade?.Descricao;
            ficha.IndicadoPor = "";
            ficha.Origem = atendimento.Origem?.Descricao;
            ficha.Tratamento = "";
            ficha.Convenio = atendimento.Convenio?.NomeFantasia;
            ficha.Plano = atendimento.Plano?.Descricao;
            ficha.Guia = atendimento.FatGuia?.Descricao;
            ficha.NumeroGuia = atendimento.GuiaNumero;
            ficha.Titular = atendimento.Titular;

            return ficha;
        }
        
        public List<string> Lista { get; set; }

        public FichaAmbulatorioModel()
        {
            Lista = new List<string>();
        }
    }
}