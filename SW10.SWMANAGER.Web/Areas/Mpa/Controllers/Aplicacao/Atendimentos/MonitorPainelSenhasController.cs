﻿
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.AtendimentosLeitosMov.Altas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Filas;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos
{
    public class MonitorPainelSenhasController : SWMANAGERControllerBase
    {
        private readonly ITerminalSenhasAppService _terminalSenhasAppService;


       public MonitorPainelSenhasController(ITerminalSenhasAppService terminalSenhasAppService)
        {
            _terminalSenhasAppService = terminalSenhasAppService;
        }
        // GET: Mpa/Alta
        public ActionResult Index()
        {
            var viewModel = new PainelSenhaViewModel();

            //  viewModel.Filas = _terminalSenhasAppService.ListarFilasDisponiveis();
            viewModel.LocalChamadaAtual = "Recepção";
            viewModel.SenhaAtual = 1234;

            viewModel.LocalChamadaAterior1 = "Recepção 1";
            viewModel.SenhaAnterior1 = 9874;

            viewModel.LocalChamadaAterior2 = "Recepção 2";
            viewModel.SenhaAnterior2 = 2657;

            viewModel.LocalChamadaAterior3 = "Recepção 3";
            viewModel.SenhaAnterior3 = 3985;

            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/PainelSenhas/Index.cshtml", viewModel);
        }

      
    }
}