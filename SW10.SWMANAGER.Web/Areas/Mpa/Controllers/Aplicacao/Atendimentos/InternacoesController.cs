﻿using Abp.Application.Navigation;
using Abp.Application.Services.Dto;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Favoritos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Sexos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos;
using SW10.SWMANAGER.ClassesAplicacao.Services.ClassificacoesRisco.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Dto;
using SW10.SWMANAGER.Organizations;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.ClassificacoesRisco;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.PreAtendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.ClassificacoesRisco;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Orcamentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.PreAtendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Layout;
using SW10.SWMANAGER.Web.Areas.Mpa.Startup;
using SW10.SWMANAGER.Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.Visitantes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Visitantes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Visitantes;
using System;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Internacoes
{
    public class InternacoesController : SWMANAGERControllerBase
    {
        private readonly ISessionAppService _sessionAppService;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly ILanguageManager _languageManager;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly ILeitoAppService _leitoAppService;
        private readonly IEmpresaAppService _empresaAppService;
        private readonly IOrganizationUnitAppService _organizationUnitAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IOrigemAppService _origemAppService;
        private readonly IPlanoAppService _planoAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly ISexoAppService _sexoAppService;
        private readonly IEspecialidadeAppService _especialidadeAppService;
        private readonly IPreAtendimentoAppService _preAtendimentoAppService;
        private readonly IUserNavigationManager _userNavigationManager;
        private readonly IUserAppService _userAppService;
        private readonly IFavoritoAppService _favoritoAppService;
        private readonly UserManager _userManager;
        private readonly IFornecedorAppService _fornecedorAppService;
        private readonly IVisitanteAppService _visitanteRepository;

        public InternacoesController(
            IUserNavigationManager userNavigationManager,
            IAtendimentoAppService atendimentoAppService,
            IPacienteAppService pacienteAppService,
            IMedicoAppService medicoAppService,
            ILeitoAppService leitoAppService,
            IOrganizationUnitAppService organizationUnitAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            IEmpresaAppService empresaAppService,
            IOrigemAppService origemAppService,
            IPlanoAppService planoAppService,
            IConvenioAppService convenioAppService,
            ISexoAppService sexoAppService,
            IEspecialidadeAppService especialidadeAppService,
            IPreAtendimentoAppService preAtendimentoAppService,
            ISessionAppService sessionAppService,
            IMultiTenancyConfig multiTenancyConfig,
            ILanguageManager languageManager,
            IFavoritoAppService favoritoAppService,
            UserManager userManager,
            IUserAppService userAppService,
            IFornecedorAppService fornecedorAppService,
            IVisitanteAppService visitanteRepository
            )
        {
            _atendimentoAppService = atendimentoAppService;
            _pacienteAppService = pacienteAppService;
            _medicoAppService = medicoAppService;
            _leitoAppService = leitoAppService;
            _organizationUnitAppService = organizationUnitAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _empresaAppService = empresaAppService;
            _origemAppService = origemAppService;
            _planoAppService = planoAppService;
            _convenioAppService = convenioAppService;
            _sexoAppService = sexoAppService;
            _especialidadeAppService = especialidadeAppService;
            _preAtendimentoAppService = preAtendimentoAppService;
            _sessionAppService = sessionAppService;
            _multiTenancyConfig = multiTenancyConfig;
            _languageManager = languageManager;
            _userNavigationManager = userNavigationManager;
            _favoritoAppService = favoritoAppService;
            _userManager = userManager;
            _userAppService = userAppService;
            _fornecedorAppService = fornecedorAppService;
            _visitanteRepository = visitanteRepository;
        }

        public async Task<ActionResult> Index()
        {
            //var pacientes = await _pacienteAppService.Listar (new ListarPacientesInput ());
            //var medicos = await _medicoAppService.ListarTodos ();
            //var convenios = await _convenioAppService.ListarTodos ();
            //var origens = await _origemAppService.ListarTodos ();
            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
            ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);

            //if (empresas == null || empresas.Items.Count == 0)
            //{
            //    empresas = await _empresaAppService.ListarTodos();
            //}

            //ListarParaInternacao
            //var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();
           // var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaInternacao();


            InternacoesViewModel viewModel;
            viewModel = new InternacoesViewModel();

            //viewModel.Pacientes = new SelectList (pacientes.Items.Select (m => new { Id = m.Id, Nome = string.Format ("{0} - {1} - {2}", m.NomeCompleto, m.Cpf, m.Nascimento) }), "Id", "Nome");
            //viewModel.Medicos = new SelectList (medicos.Items.Select (m => new { Id = m.Id, Nome = string.Format ("{0}", m.NomeCompleto) }), "Id", "Nome");
            viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
            //viewModel.Origens = new SelectList (origens.Items.Select (m => new { Id = m.Id, Descricao = string.Format ("{0}", m.Descricao) }), "Id", "Descricao");
            //viewModel.Convenios = new SelectList (convenios.Items.Select (m => new { Id = m.Id, NomeFantasia = string.Format ("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
           // viewModel.UnidadesOrganizacionais = unidadesOrganizacionais.Items.ToList();

            //viewModel.MenuItemName = 
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Index.cshtml", viewModel);
        }

        public async Task<PartialViewResult> MapaLeitos()
        {
            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
            ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);

            if (empresas == null || empresas.Items.Count == 0)
            {
                empresas = await _empresaAppService.ListarTodos();
            }

            //var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaInternacao();

            InternacoesViewModel model;
            model = new InternacoesViewModel();
            //model.TipoAtendimento = tipoAtendimento;
            model.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
            model.UnidadesOrganizacionais = unidadesOrganizacionais.Items.ToList();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/MapaLeitos/Index.cshtml", model);
        }

        public async Task<PartialViewResult> PrevisaoEntradas()
        {
            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
            ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);

            if (empresas == null || empresas.Items.Count == 0)
            {
                empresas = await _empresaAppService.ListarTodos();
            }

            // var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaInternacao();

            InternacoesViewModel model;
            model = new InternacoesViewModel();
            model.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
            model.UnidadesOrganizacionais = unidadesOrganizacionais.Items.ToList();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/PrevisaoEntradas/Index.cshtml", model);

        }

        public async Task<PartialViewResult> Visitantes()
        {
            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
            VisitantesViewModel model = new VisitantesViewModel();
            try
            {

               // ListResultDto<CriarOuEditarFornecedor> fonecedores = await _fornecedorAppService.ListarTodos();

                //ListResultDto<CriarOuEditarFornecedor> fonecedores = new ListResultDto<CriarOuEditarFornecedor>();
                //fonecedores = await _fornecedorAppService.ListarTodos();

                //var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();
                //  var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaInternacao();

               
               
                 //model.Documento = new SelectList(fonecedores.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //model.UnidadesOrganizacionais = unidadesOrganizacionais.Items.ToList();


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
                // return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/MapaLeitos/Index.cshtml", model);
                return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Visitantes/Index.cshtml", model);

        }

        /// <summary>
        /// Chamar o ViewModel do cadastro de visitante
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<ActionResult> CriarOuEditarVisitanteModal(long? id)
        {
            //var paises = await _paisAppService.Listar(new ListarPaisesInput());

            CriarOuEditarVisitanteModalViewModel viewModel;
            try
            {
                if (id.HasValue)
                {
                    var output = await _visitanteRepository.Obter((long)id);
                    viewModel = new CriarOuEditarVisitanteModalViewModel(output);
                    //   viewModel.Paises = new SelectList(paises.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} ({1})", m.Nome, m.Sigla) }), "Id", "Nome", output.PaisId);
                }
                else
                {
                    viewModel = new CriarOuEditarVisitanteModalViewModel(new VisitanteDto());
                    //  viewModel.Paises = new SelectList(paises.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} ({1})", m.Nome, m.Sigla) }), "Id", "Nome");
                }

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            }
        

           
            // var viewModel = new CriarOuEditarVisitanteModalViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Visitantes/_CriarOuEditarModal.cshtml", viewModel);

            //return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Visitantes/Index.cshtml");
        }

        public async Task<ListResultDto<LeitoComAtendimentoDto>> ListarLeitos()
        {
            var leitos = await _leitoAppService.ListarTodos();

            return leitos;
            //    [AcceptVerbs("GET", "POST", "PUT")]
            //public JsonResult CriarProdutoRelacaoAcaoTerapeutica(ProdutoRelacaoAcaoTerapeuticaDto input)
            //{
            //    try
            //    {
            //        var objResult = AsyncHelper.RunSync(() => _produtoRelacaoAcaoTerapeuticaAppService.CriarOuEditar(input, input.Id));
            //        return Json(new { Result = "OK", Record = objResult }, JsonRequestBehavior.AllowGet);
            //    }
            //    catch (Exception ex)
            //    {
            //        return Json(new { Result = "ERROR", Message = ex.Message, JsonRequestBehavior.AllowGet });
            //    }
            //}
        }

        [ChildActionOnly]
        public PartialViewResult Header()
        {
            var headerModel = new HeaderViewModel
            {
                LoginInformations = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentLoginInformations()),
                Languages = _languageManager.GetLanguages(),
                CurrentLanguage = _languageManager.CurrentLanguage,
                IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled,
                IsImpersonatedLogin = AbpSession.ImpersonatorUserId.HasValue
            };

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Layout/_Header.cshtml", headerModel);
        }

        [ChildActionOnly]
        public PartialViewResult Favoritos(string currentPageName = "", string menuName = MpaNavigationProvider.MenuName)
        {
            var userIdentifier = AbpSession.ToUserIdentifier();
            var favoritosList = AsyncHelper.RunSync(() => _favoritoAppService.Listar(userIdentifier.UserId));
            var favoritos = favoritosList.Items;

            UserMenu menu = new UserMenu()
            {
                Name = "Favoritos",
                CustomData = null,
                DisplayName = "Favoritos",
                Items = new List<UserMenuItem>()
            };

            foreach (var fav in favoritos)
            {
                var item = new UserMenuItem()
                {
                    Name = fav.Name,
                    Icon = fav.Icon,
                    Url = fav.Url
                };

                menu.Items.Add(item);
            }

            var favoritosModel = new FavoritosViewModel
            {
                Menu = menu,
                CurrentPageName = currentPageName
            };

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Layout/_Favoritos.cshtml", favoritosModel);
        }

        [HttpPost]
        public async Task<long> CriarNovoAtendimento()
        {
            long id = await _atendimentoAppService.CriarNovoAtendimento();
            return id;
        }

        public async Task<ActionResult> ModalPacientes()
        {
            var model = new PacientesViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Pacientes/Index.cshtml", model);
        }

        public async Task<ActionResult> ModalPreAtendimentos()
        {
            var model = new PreAtendimentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/PreAtendimentos/Index.cshtml", model);
        }

        public async Task<ActionResult> ModalOrcamentos()
        {
            var model = new OrcamentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Orcamentos/Index.cshtml", model);
        }

        public async Task<ActionResult> ModalClassificacaoRiscos()
        {
            var model = new ClassificacoesRiscoViewModel();
            //var model = new ClassificacaoRiscosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/ClassificacaoRiscos/Index.cshtml", model);
        }

        [HttpPost]
        public void AtendimentoAtual(CriarOuEditarAtendimento item)
        {
            TempData["AtendimentoAtual"] = item.Id;
        }

        [HttpPost]
        public void NovoAtendimento(long? item)
        {
            TempData["AtendimentoAtual"] = 0;
        }

        public PartialViewResult _MenuTopo()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Layout/_MenuTopo.cshtml");
        }

        public PartialViewResult _PreAtendimento()
        {
            var model = new PreAtendimentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/PreInternacoes/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _CriarOuEditarPreAtendimento()
        {
            CriarOuEditarPreAtendimentoModalViewModel viewModel;
            viewModel = new CriarOuEditarPreAtendimentoModalViewModel(new CriarOuEditarPreAtendimento());
            var sexos = await _sexoAppService.ListarTodos();
            viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/PreInternacoes/_CriarOuEditarModal.cshtml", viewModel);
        }

        public PartialViewResult _ClassificacaoRisco()
        {
            var model = new ClassificacoesRiscoViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/ClassificacoesRisco/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _CriarOuEditarClassificacaoRisco()
        {
            var especialidades = await _especialidadeAppService.ListarTodos();

            CriarOuEditarClassificacaoRiscoModalViewModel viewModel;

            viewModel = new CriarOuEditarClassificacaoRiscoModalViewModel(new CriarOuEditarClassificacaoRisco());
            viewModel.Especialidades = new SelectList(especialidades.Items, "Id", "Nome");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/ClassificacoesRisco/_CriarOuEditarModal.cshtml", viewModel);
        }

        public PartialViewResult _PesquisarPaciente()
        {
            var model = new PacientesViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Pacientes/Index.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Atendimento_Create, AppPermissions.Pages_Tenant_Cadastros_Atendimento_Edit)]
        public async Task<PartialViewResult> CriarOuEditarModal(long? id)
        {
            var pacientes = await _pacienteAppService.Listar(new ListarPacientesInput());
            var medicos = await _medicoAppService.ListarTodos();
            var empresas = await _empresaAppService.ListarTodos();
            var convenios = await _convenioAppService.ListarTodos();
            var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();

            CriarOuEditarAtendimentoModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _atendimentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarAtendimentoModalViewModel(output);
                viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            }
            else
            {
                viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());
                viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
                viewModel.Medicos = new SelectList(medicos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
                viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items.Select(m => new { Id = m.Id, DisplayName = string.Format("{0}", m.DisplayName) }), "Id", "DisplayName");
                viewModel.IsInternacao = true;
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/_CriarOuEditarModal.cshtml", viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> SalvarAtendimento(AtendimentoDto ambulatorioEmergencia)
        {
            // AtendimentoDto relacao = new AtendimentoDto ();
            await _atendimentoAppService.CriarOuEditar(ambulatorioEmergencia);
            return Content(L("Sucesso"));
        }

        [HttpPost]
        public async Task<long> SalvarPreAtendimento(CriarOuEditarPreAtendimento preAtendimento)
        {
            var preAtendimentoInserido = await _preAtendimentoAppService.CriarGetId(preAtendimento);
            return preAtendimentoInserido;
        }
    }
}