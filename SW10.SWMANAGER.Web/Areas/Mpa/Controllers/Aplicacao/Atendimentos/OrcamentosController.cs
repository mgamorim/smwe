﻿using Abp.Application.Navigation;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.CoresPele;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Escolaridades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.EstadosCivis;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Religioes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Sexos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.TiposTelefone;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Naturalidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Profissoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Orcamentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Orcamentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.Orcamentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.PreAtendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Orcamentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.PreAtendimentos;
using SW10.SWMANAGER.Web.Controllers;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Orcamentos
{
    public class OrcamentosController : SWMANAGERControllerBase
    {
        private readonly IOrcamentoAppService _orcamentoAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IProfissaoAppService _profissaoAppService;
        private readonly INaturalidadeAppService _naturalidadeAppService;
        private readonly IOrigemAppService _origemAppService;
        private readonly IPlanoAppService _planoAppService;
        private readonly IPaisAppService _paisAppService;
        private readonly IEstadoAppService _estadoAppService;
        private readonly ICidadeAppService _cidadeAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly ISexoAppService _sexoAppService;
        private readonly IEscolaridadeAppService _escolaridadeAppService;
        private readonly ICorPeleAppService _corPeleAppService;
        private readonly IReligiaoAppService _religiaoAppService;
        private readonly IEstadoCivilAppService _estadoCivilAppService;
        private readonly ITipoTelefoneAppService _tipoTelefoneAppService;
        //private readonly IPacienteConvenioAppService _pacienteConvenioAppService;
        private readonly IPacientePesoAppService _pacientePesoAppService;
        //private readonly IPacientePlanoAppService _pacientePlanoAppService;
        private readonly IAgendamentoConsultaMedicoDisponibilidadeAppService _agendamentoConsultaMedicoDisponibilidadeAppService;
        private readonly IEspecialidadeAppService _especialidadeAppService;
        private readonly IAgendamentoConsultaAppService _agendamentoConsultaAppService;
        private readonly IPreAtendimentoAppService _preAtendimentoAppService;
        private readonly IEmpresaAppService _empresaAppService;

        public OrcamentosController(
            IUserNavigationManager userNavigationManager,
            IOrcamentoAppService orcamentoAppService,
            IPacienteAppService pacienteAppService,
            IProfissaoAppService profissaoAppService,
            INaturalidadeAppService naturalidadeAppService,
            IOrigemAppService origemAppService,
            IPaisAppService paisAppService,
            IEstadoAppService estadoAppService,
            ICidadeAppService cidadeAppService,
            IPlanoAppService planoAppService,
            IConvenioAppService convenioAppService,
            ISexoAppService sexoAppService,
            IEscolaridadeAppService escolaridadeAppService,
            ICorPeleAppService corPeleAppService,
            IReligiaoAppService religiaoAppService,
            IEstadoCivilAppService estadoCivilAppService,
            ITipoTelefoneAppService tipoTelefoneAppService,
            //IPacienteConvenioAppService pacienteConvenioAppService,
            IPacientePesoAppService pacientePesoAppService,
            //IPacientePlanoAppService pacientePlanoAppService,
            IAgendamentoConsultaMedicoDisponibilidadeAppService agendamentoConsultaMedicoDisponibilidadeAppService,
            IEspecialidadeAppService especialidadeAppService,
            IAgendamentoConsultaAppService agendamentoConsultaAppService,
            IPreAtendimentoAppService preAtendimentoAppService,
            IEmpresaAppService empresaAppService

            )
        {
            _orcamentoAppService = orcamentoAppService;
            _pacienteAppService = pacienteAppService;
            _profissaoAppService = profissaoAppService;
            _naturalidadeAppService = naturalidadeAppService;
            _origemAppService = origemAppService;
            _planoAppService = planoAppService;
            _paisAppService = paisAppService;
            _estadoAppService = estadoAppService;
            _cidadeAppService = cidadeAppService;
            _convenioAppService = convenioAppService;
            _sexoAppService = sexoAppService;
            _escolaridadeAppService = escolaridadeAppService;
            _corPeleAppService = corPeleAppService;
            _religiaoAppService = religiaoAppService;
            _estadoCivilAppService = estadoCivilAppService;
            _tipoTelefoneAppService = tipoTelefoneAppService;
            //_pacienteConvenioAppService = pacienteConvenioAppService;
            _pacientePesoAppService = pacientePesoAppService;
            //_pacientePlanoAppService = pacientePlanoAppService;
            _agendamentoConsultaMedicoDisponibilidadeAppService = agendamentoConsultaMedicoDisponibilidadeAppService;
            _especialidadeAppService = especialidadeAppService;
            _agendamentoConsultaAppService = agendamentoConsultaAppService;
            _preAtendimentoAppService = preAtendimentoAppService;
            _empresaAppService = empresaAppService;
        }

        public ActionResult Index()
        {
            //TempData["Orcamento"] = new OrcamentoDto();
            //TempData["OrcamentoId"] = 0;
            var model = new OrcamentosViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Orcamentos/Index.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_Orcamentos, AppPermissions.Pages_Tenant_Atendimento_Orcamentos_Edit)]
        public async Task<PartialViewResult> CriarOuEditarModal(long? id)
        {
            var planos = await _planoAppService.ListarTodos();
            var convenios = await _convenioAppService.Listar(new ListarConveniosInput());

            CriarOuEditarOrcamentoModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _orcamentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarOrcamentoModalViewModel(output);
                viewModel.Planos = new SelectList(planos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} - {1}", m.Descricao, m.Convenio.NomeFantasia) }), "Id", "Nome", output.PlanoId);
                viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeFantasia) }), "Id", "Nome", output.ConvenioId);
            }
            else
            {
                viewModel = new CriarOuEditarOrcamentoModalViewModel(new CriarOuEditarOrcamento());
                viewModel.Planos = new SelectList(planos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} - {1}", m.Descricao, m.Convenio.NomeFantasia) }), "Id", "Nome");
                viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeFantasia) }), "Id", "Nome");
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Orcamentos/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _CriarOuEditarOrcamento()
        {
            var planos = await _planoAppService.Listar(new ListarPlanosInput());
            var convenios = await _convenioAppService.Listar(new ListarConveniosInput());
            var empresas = await _empresaAppService.Listar(new ListarEmpresasInput());
            CriarOuEditarOrcamentoModalViewModel viewModel;
            viewModel = new CriarOuEditarOrcamentoModalViewModel(new CriarOuEditarOrcamento());
            viewModel.Planos = new SelectList(planos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} - {1}", m.Descricao, m.Convenio.NomeFantasia) }), "Id", "Nome");
            viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeFantasia) }), "Id", "Nome");
            viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeFantasia) }), "Id", "Nome");
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Orcamentos/_CriarOuEditarModal.cshtml", viewModel);
        }


        public PartialViewResult _PesquisarPreAtendimento()
        {
            var model = new PreAtendimentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Orcamentos/_PesquisarPreAtendimentos.cshtml", model);
        }

        public async Task<PartialViewResult> _PreAtendimento(long? id)
        {
            var sexos = await _sexoAppService.ListarTodos();

            CriarOuEditarPreAtendimentoModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _preAtendimentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarPreAtendimentoModalViewModel(output);
                viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao", output.Sexo);
            }
            else
            {
                viewModel = new CriarOuEditarPreAtendimentoModalViewModel(new CriarOuEditarPreAtendimento());
                viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/Orcamentos/_CriarOuEditarPreAtendimento.cshtml", viewModel);
        }

        public PartialViewResult _PesquisarPaciente()
        {
            var model = new PacientesViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/Pacientes/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _IdentificacaoPaciente(long? id)
        {
            var origens = await _origemAppService.Listar(new ListarOrigensInput());
            var sexos = await _sexoAppService.ListarTodos();
            var coresPele = await _corPeleAppService.ListarTodos();
            var escolaridades = await _escolaridadeAppService.ListarTodos();
            var religioes = await _religiaoAppService.ListarTodos();
            var estadosCivis = await _estadoCivilAppService.ListarTodos();
            var tiposTelefone = await _tipoTelefoneAppService.ListarTodos();

            CriarOuEditarPacienteModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _pacienteAppService.Obter2((long)id);

                viewModel = new CriarOuEditarPacienteModalViewModel(output);
                //viewModel.Origens = new SelectList(origens.Items, "Id", "Descricao", output.OrigemId);
                //viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao", output.Sexo);
                //viewModel.Escolaridades = new SelectList(escolaridades.Items, "Id", "Descricao", output.Escolaridade);
                //viewModel.CoresPele = new SelectList(coresPele.Items, "Id", "Descricao", output.CorPele);
                //viewModel.Religioes = new SelectList(religioes.Items, "Id", "Descricao", output.Religiao);
                //viewModel.EstadosCivis = new SelectList(estadosCivis.Items, "Id", "Descricao", output.EstadoCivil);
                //viewModel.TiposTelefone = new SelectList(tiposTelefone.Items, "Id", "Descricao");
            }
            else
            {
                viewModel = new CriarOuEditarPacienteModalViewModel(new PacienteDto());
                //viewModel.Origens = new SelectList(origens.Items, "Id", "Descricao");
                //viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
                //viewModel.CoresPele = new SelectList(coresPele.Items, "Id", "Descricao");
                //viewModel.Escolaridades = new SelectList(escolaridades.Items, "Id", "Descricao");
                //viewModel.Religioes = new SelectList(religioes.Items, "Id", "Descricao");
                //viewModel.EstadosCivis = new SelectList(estadosCivis.Items, "Id", "Descricao");
                //viewModel.TiposTelefone = new SelectList(tiposTelefone.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/Pacientes/_IdentificacaoPaciente.cshtml", viewModel);
        }

        [HttpPost]
        public async Task<long> SalvarPreAtendimento(CriarOuEditarPreAtendimento preAtendimento)
        {
            var preAtendimentoInserido = await _preAtendimentoAppService.CriarGetId(preAtendimento);
            return preAtendimentoInserido;
        }

        [HttpPost]
        public async Task<ActionResult> SalvarOrcamento(CriarOuEditarOrcamento orcamento)
        {
            OrcamentoDto relacao = new OrcamentoDto();
            await _orcamentoAppService.CriarOuEditar(orcamento);
            return Content(L("Sucesso"));
        }
    }
}