﻿using Abp.Application.Navigation;
using Abp.Application.Services.Dto;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.CoresPele;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Escolaridades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.EstadosCivis;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Favoritos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Religioes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Sexos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.TiposTelefone;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.ClassificacoesRisco.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Dto;
using SW10.SWMANAGER.Organizations;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.ClassificacoesRisco;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.PreAtendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.ClassificacoesRisco;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.PreAtendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Layout;
using SW10.SWMANAGER.Web.Areas.Mpa.Startup;
using SW10.SWMANAGER.Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.AmbulatorioEmergencias
{
    public class AmbulatorioEmergenciasController : SWMANAGERControllerBase
    {
        private readonly ISessionAppService _sessionAppService;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly ILanguageManager _languageManager;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IEmpresaAppService _empresaAppService;
        private readonly IOrganizationUnitAppService _organizationUnitAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IOrigemAppService _origemAppService;
        private readonly IPlanoAppService _planoAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly ISexoAppService _sexoAppService;
        private readonly IEscolaridadeAppService _escolaridadeAppService;
        private readonly ICorPeleAppService _corPeleAppService;
        private readonly IReligiaoAppService _religiaoAppService;
        private readonly IEstadoCivilAppService _estadoCivilAppService;
        private readonly ITipoTelefoneAppService _tipoTelefoneAppService;
        //private readonly IPacienteConvenioAppService _pacienteConvenioAppService;
        private readonly IPacientePesoAppService _pacientePesoAppService;
        //private readonly IPacientePlanoAppService _pacientePlanoAppService;
        private readonly IAgendamentoConsultaMedicoDisponibilidadeAppService _agendamentoConsultaMedicoDisponibilidadeAppService;
        private readonly IEspecialidadeAppService _especialidadeAppService;
        private readonly IPreAtendimentoAppService _preAtendimentoAppService;
        private readonly IUserNavigationManager _userNavigationManager;
        private readonly IUserAppService _userAppService;
        private readonly IFavoritoAppService _favoritoAppService;
        private readonly UserManager _userManager;

        public AmbulatorioEmergenciasController(
            IUserNavigationManager userNavigationManager,
            IAtendimentoAppService atendimentoAppService,
            IPacienteAppService pacienteAppService,
            IMedicoAppService medicoAppService,
            IOrganizationUnitAppService organizationUnitAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            IEmpresaAppService empresaAppService,
            IOrigemAppService origemAppService,
            IPlanoAppService planoAppService,
            IConvenioAppService convenioAppService,
            ISexoAppService sexoAppService,
            IEscolaridadeAppService escolaridadeAppService,
            ICorPeleAppService corPeleAppService,
            IReligiaoAppService religiaoAppService,
            IEstadoCivilAppService estadoCivilAppService,
            ITipoTelefoneAppService tipoTelefoneAppService,
            //IPacienteConvenioAppService pacienteConvenioAppService,
            IPacientePesoAppService pacientePesoAppService,
            //IPacientePlanoAppService pacientePlanoAppService,
            IAgendamentoConsultaMedicoDisponibilidadeAppService agendamentoConsultaMedicoDisponibilidadeAppService,
            IEspecialidadeAppService especialidadeAppService,
            IPreAtendimentoAppService preAtendimentoAppService,
            ISessionAppService sessionAppService,
            IMultiTenancyConfig multiTenancyConfig,
            ILanguageManager languageManager,
            IFavoritoAppService favoritoAppService,
            UserManager userManager,
            IUserAppService userAppService
            )
        {
            _atendimentoAppService = atendimentoAppService;
            _pacienteAppService = pacienteAppService;
            _medicoAppService = medicoAppService;
            _organizationUnitAppService = organizationUnitAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _empresaAppService = empresaAppService;
            _origemAppService = origemAppService;
            _planoAppService = planoAppService;
            _convenioAppService = convenioAppService;
            _sexoAppService = sexoAppService;
            _escolaridadeAppService = escolaridadeAppService;
            _corPeleAppService = corPeleAppService;
            _religiaoAppService = religiaoAppService;
            _estadoCivilAppService = estadoCivilAppService;
            _tipoTelefoneAppService = tipoTelefoneAppService;
            //_pacienteConvenioAppService = pacienteConvenioAppService;
            _pacientePesoAppService = pacientePesoAppService;
            //_pacientePlanoAppService = pacientePlanoAppService;
            _agendamentoConsultaMedicoDisponibilidadeAppService = agendamentoConsultaMedicoDisponibilidadeAppService;
            _especialidadeAppService = especialidadeAppService;
            _preAtendimentoAppService = preAtendimentoAppService;
            _sessionAppService = sessionAppService;
            _multiTenancyConfig = multiTenancyConfig;
            _languageManager = languageManager;
            _userNavigationManager = userNavigationManager;
            _favoritoAppService = favoritoAppService;
            _userManager = userManager;
            _userAppService = userAppService;
        }

        public async Task<ActionResult> Index()
        {
            //var pacientes = await _pacienteAppService.ListarTodos(); //.Listar(new ListarPacientesInput());
            var medicos = await _medicoAppService.ListarTodos();
            var convenios = await _convenioAppService.ListarTodos();
            //var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();
            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarTodos();
            //  var origens = await _origemAppService.ListarTodos();
            var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
            ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);

            if (empresas == null || empresas.Items.Count == 0)
            {
                empresas = await _empresaAppService.ListarTodos();
            }
            //var userId
            //var user = Task.Run(() => _userAppService.GetUser()).Result;
            var medicoId = user.MedicoId;
            if (medicoId.HasValue)
            {
                ViewBag.UserMedicoId = medicoId.Value;
                ViewBag.UserMedico = Task.Run(() => _medicoAppService.Obter(medicoId.Value)).Result;
            }


            CriarOuEditarAtendimentoModalViewModel viewModel;
            viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());

            // var atendimentos = await _atendimentoAppService.Listar();
            // List<CriarOuEditarAtendimento> atendimentosList = new List<CriarOuEditarAtendimento>();
            // TempData["Atendimentos"] = atendimentosList;
            // viewModel.Atendimentos = atendimentosList;

            // viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "NomeCompleto");
            //viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} - {1} - {2}", m.NomeCompleto, m.Cpf, m.Nascimento) }), "Id", "Nome");
            viewModel.Medicos = new SelectList(medicos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
            //viewModel.Origens = new SelectList(origens.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
            viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
            viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
            // viewModel.Internacao = true;
            viewModel.FiltroDataAtendimento = true;
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Index.cshtml", viewModel);
        }

        [ChildActionOnly]
        public PartialViewResult Header()
        {
            var headerModel = new HeaderViewModel
            {
                LoginInformations = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentLoginInformations()),
                Languages = _languageManager.GetLanguages(),
                CurrentLanguage = _languageManager.CurrentLanguage,
                IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled,
                IsImpersonatedLogin = AbpSession.ImpersonatorUserId.HasValue
            };

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Layout/_Header.cshtml", headerModel);
        }

        [ChildActionOnly]
        public PartialViewResult Favoritos(string currentPageName = "", string menuName = MpaNavigationProvider.MenuName)
        {
            var userIdentifier = AbpSession.ToUserIdentifier();
            var favoritosList = AsyncHelper.RunSync(() => _favoritoAppService.Listar(userIdentifier.UserId));
            var favoritos = favoritosList.Items;

            UserMenu menu = new UserMenu()
            {
                Name = "Favoritos",
                CustomData = null,
                DisplayName = "Favoritos",
                Items = new List<UserMenuItem>()
            };

            foreach (var fav in favoritos)
            {
                var item = new UserMenuItem()
                {
                    Name = fav.Name,
                    Icon = fav.Icon,
                    Url = fav.Url
                };

                menu.Items.Add(item);
            }

            var favoritosModel = new FavoritosViewModel
            {
                Menu = menu,
                CurrentPageName = currentPageName
            };

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Layout/_Favoritos.cshtml", favoritosModel);
        }

        [HttpPost]
        public async Task<long> CriarNovoAtendimento()
        {
            long id = await _atendimentoAppService.CriarNovoAtendimento();
            return id;
        }

        //NÃO ESTÃO SENDO USADOS NO MOMENTO PABLO 20/07/2017
        //public async Task<ActionResult> ModalPacientes()
        //{
        //    var model = new PacientesViewModel();
        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Pacientes/Index.cshtml", model);
        //}

        //public async Task<ActionResult> ModalPreAtendimentos()
        //{
        //    var model = new PreAtendimentosViewModel();
        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/PreAtendimentos/Index.cshtml", model);
        //}

        //public async Task<ActionResult> ModalOrcamentos()
        //{
        //    var model = new OrcamentosViewModel();
        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Orcamentos/Index.cshtml", model);
        //}

        //public async Task<ActionResult> ModalClassificacaoRiscos()
        //{
        //    var model = new ClassificacoesRiscoViewModel();
        //    //var model = new ClassificacaoRiscosViewModel();
        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/ClassificacaoRiscos/Index.cshtml", model);
        //}

        [HttpPost]
        public void AtendimentoAtual(CriarOuEditarAtendimento item)
        {
            TempData["AtendimentoAtual"] = item.Id;
        }

        [HttpPost]
        public void NovoAtendimento(long? id, long abaId)
        {
            TempData["AtendimentoAtual"] = id;
        }

        public PartialViewResult _MenuTopo()
        {
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Layout/_MenuTopo.cshtml");
        }

        public PartialViewResult _PreAtendimento()
        {
            var model = new PreAtendimentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/PreAmbulatorioEmergencias/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _CriarOuEditarPreAtendimento()
        {
            CriarOuEditarPreAtendimentoModalViewModel viewModel;
            viewModel = new CriarOuEditarPreAtendimentoModalViewModel(new CriarOuEditarPreAtendimento());
            var sexos = await _sexoAppService.ListarTodos();
            viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/PreAmbulatorioEmergencias/_CriarOuEditarModal.cshtml", viewModel);
        }

        public PartialViewResult _ClassificacaoRisco()
        {
            var model = new ClassificacoesRiscoViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/ClassificacoesRisco/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _CriarOuEditarClassificacaoRisco()
        {
            var especialidades = await _especialidadeAppService.ListarTodos();

            CriarOuEditarClassificacaoRiscoModalViewModel viewModel;

            viewModel = new CriarOuEditarClassificacaoRiscoModalViewModel(new CriarOuEditarClassificacaoRisco());
            viewModel.Especialidades = new SelectList(especialidades.Items, "Id", "Nome");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/ClassificacoesRisco/_CriarOuEditarModal.cshtml", viewModel);
        }

        public PartialViewResult _PesquisarPaciente()
        {
            var model = new PacientesViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Pacientes/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _IdentificacaoPaciente(long? id)
        {
            var origens = await _origemAppService.Listar(new ListarOrigensInput());
            var sexos = await _sexoAppService.ListarTodos();
            var coresPele = await _corPeleAppService.ListarTodos();
            var escolaridades = await _escolaridadeAppService.ListarTodos();
            var religioes = await _religiaoAppService.ListarTodos();
            var estadosCivis = await _estadoCivilAppService.ListarTodos();
            var tiposTelefone = await _tipoTelefoneAppService.ListarTodos();

            CriarOuEditarPacienteModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _pacienteAppService.Obter2((long)id);

                viewModel = new CriarOuEditarPacienteModalViewModel(output);
                //viewModel.Origens = new SelectList(origens.Items, "Id", "Descricao", output.OrigemId);
                //viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao", output.Sexo);
                //viewModel.Escolaridades = new SelectList(escolaridades.Items, "Id", "Descricao", output.Escolaridade);
                //viewModel.CoresPele = new SelectList(coresPele.Items, "Id", "Descricao", output.CorPele);
                //viewModel.Religioes = new SelectList(religioes.Items, "Id", "Descricao", output.Religiao);
        //        viewModel.EstadosCivis = new SelectList(estadosCivis.Items, "Id", "Descricao", output.EstadoCivil);
           //     viewModel.TiposTelefone = new SelectList(tiposTelefone.Items, "Id", "Descricao");
            }
            else
            {
                viewModel = new CriarOuEditarPacienteModalViewModel(new PacienteDto());
                //viewModel.Origens = new SelectList(origens.Items, "Id", "Descricao");
                //viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
                //viewModel.CoresPele = new SelectList(coresPele.Items, "Id", "Descricao");
                //viewModel.Escolaridades = new SelectList(escolaridades.Items, "Id", "Descricao");
                //viewModel.Religioes = new SelectList(religioes.Items, "Id", "Descricao");
                //viewModel.EstadosCivis = new SelectList(estadosCivis.Items, "Id", "Descricao");
                //viewModel.TiposTelefone = new SelectList(tiposTelefone.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Pacientes/_IdentificacaoPaciente.cshtml", viewModel);
            //return PartialView("~/Areas/Mpa/Views/Aplicacao/AmbulatorioEmergencias/AmbulatorioEmergencias/_IdentificacaoPaciente.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _Agendamento()
        {
            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarAtivos(0, 0);
            var especialidadesAgendadas = agendamentos.Select(m => m.MedicoEspecialidade.EspecialidadeId).Distinct().ToList();
            var especialidades = await _especialidadeAppService.Listar(especialidadesAgendadas);
            var viewModel = new AgendamentoConsultasViewModel();
            viewModel.Especialidades = new SelectList(especialidades.Items, "Id", "Nome");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/Agendamentos/Index.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Atendimento_Create, AppPermissions.Pages_Tenant_Cadastros_Atendimento_Edit)]
        public async Task<PartialViewResult> CriarOuEditarModal(long? id)
        {
            var pacientes = await _pacienteAppService.ListarTodos(); //.Listar(new ListarPacientesInput());
            var medicos = await _medicoAppService.ListarTodos();
            var empresas = await _empresaAppService.ListarTodos();
            var convenios = await _convenioAppService.ListarTodos();
            var unidadesOrganizacionais = await _organizationUnitAppService.GetOrganizationUnits();

            CriarOuEditarAtendimentoModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _atendimentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarAtendimentoModalViewModel(output);
                viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            }
            else
            {
                viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());
                viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
                viewModel.Medicos = new SelectList(medicos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
                viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items.Select(m => new { Id = m.Id, DisplayName = string.Format("{0}", m.DisplayName) }), "Id", "DisplayName");
            }
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AmbulatorioEmergencias/Home/_CriarOuEditarModal.cshtml", viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> SalvarAtendimento(AtendimentoDto ambulatorioEmergencia)
        {
            AtendimentoDto relacao = new AtendimentoDto();
            await _atendimentoAppService.CriarOuEditar(ambulatorioEmergencia);
            return Content(L("Sucesso"));
        }

        [HttpPost]
        public async Task<long> SalvarPreAtendimento(CriarOuEditarPreAtendimento preAtendimento)
        {
            var preAtendimentoInserido = await _preAtendimentoAppService.CriarGetId(preAtendimento);
            return preAtendimentoInserido;
        }
    }
}