﻿using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.AgendamentoCirurgias;
using SW10.SWMANAGER.Web.Controllers;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos
{
    public class AgendamentoCirurgiasController : SWMANAGERControllerBase
    {
        // GET: Mpa/Agendamentos
        public ActionResult Index()
        {
            var viewModel = new AgendamentoCirurgiasViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AgendamentoCirurgias/Index.cshtml", viewModel);
        }
    }
}