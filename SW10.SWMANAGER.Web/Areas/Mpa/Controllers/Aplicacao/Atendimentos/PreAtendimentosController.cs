﻿using Abp.Application.Services.Dto;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.PreAtendimentos;
using SW10.SWMANAGER.Web.Controllers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.PreAtendimentos
{
    public class PreAtendimentosController : SWMANAGERControllerBase
    {
        //    private readonly IPreAtendimentoAppService _motivoAltaAppService;
        //private readonly IUserNavigationManager _userNavigationManager;

        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IPreAtendimentoAppService _preAtendimentoAppService;
        //private readonly IPacienteAppService _pacienteAppService;
        //private readonly IProfissaoAppService _profissaoAppService;
        //private readonly INaturalidadeAppService _naturalidadeAppService;
        //private readonly IOrigemAppService _origemAppService;
        //private readonly IPlanoAppService _planoAppService;
        //private readonly IPaisAppService _paisAppService;
        //private readonly IEstadoAppService _estadoAppService;
        //private readonly ICidadeAppService _cidadeAppService;
        //private readonly IConvenioAppService _convenioAppService;
        //private readonly ISexoAppService _sexoAppService;
        //private readonly IEscolaridadeAppService _escolaridadeAppService;
        //private readonly ICorPeleAppService _corPeleAppService;
        //private readonly IReligiaoAppService _religiaoAppService;
        //private readonly IEstadoCivilAppService _estadoCivilAppService;
        //private readonly ITipoTelefoneAppService _tipoTelefoneAppService;
        ////private readonly IPacienteConvenioAppService _pacienteConvenioAppService;
        //private readonly IPacientePesoAppService _pacientePesoAppService;
        ////private readonly IPacientePlanoAppService _pacientePlanoAppService;
        //private readonly IAgendamentoConsultaMedicoDisponibilidadeAppService _agendamentoConsultaMedicoDisponibilidadeAppService;
        //private readonly IEspecialidadeAppService _especialidadeAppService;
        //private readonly IAgendamentoConsultaAppService _agendamentoConsultaAppService;

        public PreAtendimentosController(

            //      IPreAtendimentoAppService motivoAltaAppService,

           // IUserNavigationManager userNavigationManager,
             IAtendimentoAppService atendimentoAppService,
            IPreAtendimentoAppService preAtendimentoAppService
           // IPacienteAppService pacienteAppService,
           // IProfissaoAppService profissaoAppService,
           // INaturalidadeAppService naturalidadeAppService,
           // IOrigemAppService origemAppService,
           // IPaisAppService paisAppService,
           // IEstadoAppService estadoAppService,
           //ICidadeAppService cidadeAppService,
           // IPlanoAppService planoAppService,
           // IConvenioAppService convenioAppService,
           // ISexoAppService sexoAppService,
           // IEscolaridadeAppService escolaridadeAppService,
           // ICorPeleAppService corPeleAppService,
           // IReligiaoAppService religiaoAppService,
           // IEstadoCivilAppService estadoCivilAppService,
           // ITipoTelefoneAppService tipoTelefoneAppService,
           // //IPacienteConvenioAppService pacienteConvenioAppService,
           // IPacientePesoAppService pacientePesoAppService,
           // //IPacientePlanoAppService pacientePlanoAppService,
           // IAgendamentoConsultaMedicoDisponibilidadeAppService agendamentoConsultaMedicoDisponibilidadeAppService,
           // IEspecialidadeAppService especialidadeAppService,
           // IAgendamentoConsultaAppService agendamentoConsultaAppService

            )
        {
            //     _motivoAltaAppService = motivoAltaAppService;
            //_userNavigationManager = userNavigationManager;
            _atendimentoAppService = atendimentoAppService;
            _preAtendimentoAppService = preAtendimentoAppService;
            //_pacienteAppService = pacienteAppService;
            //_profissaoAppService = profissaoAppService;
            //_naturalidadeAppService = naturalidadeAppService;
            //_origemAppService = origemAppService;
            //_planoAppService = planoAppService;
            //_paisAppService = paisAppService;
            //_estadoAppService = estadoAppService;
            //_cidadeAppService = cidadeAppService;
            //_convenioAppService = convenioAppService;
            //_sexoAppService = sexoAppService;
            //_escolaridadeAppService = escolaridadeAppService;
            //_corPeleAppService = corPeleAppService;
            //_religiaoAppService = religiaoAppService;
            //_estadoCivilAppService = estadoCivilAppService;
            //_tipoTelefoneAppService = tipoTelefoneAppService;
            ////_pacienteConvenioAppService = pacienteConvenioAppService;
            //_pacientePesoAppService = pacientePesoAppService;
            ////_pacientePlanoAppService = pacientePlanoAppService;
            //_agendamentoConsultaMedicoDisponibilidadeAppService = agendamentoConsultaMedicoDisponibilidadeAppService;
            //_especialidadeAppService = especialidadeAppService;
            //_agendamentoConsultaAppService = agendamentoConsultaAppService;
        }

        public ActionResult Index()
        {
            TempData["PreAtendimento"] = new PreAtendimentoDto();
            TempData["PreAtendimentoId"] = 0;
            var model = new PreAtendimentosViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/PreAtendimentos/Index.cshtml", model);
        }


        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_PreAtendimentos_Create, AppPermissions.Pages_Tenant_Atendimento_PreAtendimentos_Edit)]
        public async Task<PartialViewResult> CriarOuEditarModal(long? id)
        {
            //var sexos = await _sexoAppService.ListarTodos();

            CriarOuEditarAtendimentoModalViewModel viewModel;

            //if (id.HasValue)
            //{
            //    var output = await _preAtendimentoAppService.Obter((long)id);
            //    viewModel = new CriarOuEditarPreAtendimentoModalViewModel(output);
            //    viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao", output.Sexo);
            //}
            //else
            //{
            //    viewModel = new CriarOuEditarPreAtendimentoModalViewModel(new CriarOuEditarPreAtendimento());
            //    viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
            //}
            if (id.HasValue)
            {
                var output = await _atendimentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarAtendimentoModalViewModel(output);
               // viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            }
            else
            {
                viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());
               // viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            }

            viewModel.PreAtendimento = true;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/PreAtendimentos/_CriarOuEditarModal.cshtml", viewModel);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> SalvarPreAtendimento(CriarOuEditarPreAtendimento preAtendimento)
        {
            PreAtendimentoDto relacao = new PreAtendimentoDto();
            await _preAtendimentoAppService.CriarOuEditar(preAtendimento);
            return Content(L("Sucesso"));
        }

        public async Task<PartialViewResult> FormPreAtendimento()
        {
            var userId = AbpSession.UserId;
            //var user = await _userManager.GetUserByIdAsync((long)userId);

            CriarOuEditarAtendimentoModalViewModel viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());

            //if (id == 0)
            //    id = null;

            if (1!=1)
            {
                var output = await _atendimentoAppService.Obter((long)1);
               // var output = await _atendimentoAppService.Obter(1);
                viewModel = new CriarOuEditarAtendimentoModalViewModel(output);
            }
            else
            {

            }
           // viewModel.AbaId = abaId;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/IndexParcial.cshtml", viewModel);
        }
    
    }
}