﻿#region usings
using Abp.Application.Navigation;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Web.Mvc.Authorization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.CoresPele;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Escolaridades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.EstadosCivis;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.GuiaTipos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Religioes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Sexos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.TiposTelefone;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Nacionalidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Naturalidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Profissoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.ClassificacoesRisco.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Dto;
using SW10.SWMANAGER.Organizations;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.ClassificacoesRisco;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.PreAtendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.ClassificacoesRisco;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Orcamentos;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.PreAtendimentos;
using SW10.SWMANAGER.Web.Controllers;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
#endregion usings.

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendimentos
{
    public class AtendimentosController : SWMANAGERControllerBase
    {
        #region Injecao e contrutor

        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IGuiaAppService _guiaAppService;
        private readonly IGuiaTipoAppService _guiaTipoAppService;
        //private readonly ITipoAtendimentoAppService _tipoAtendimentoAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IEmpresaAppService _empresaAppService;
        private readonly IOrganizationUnitAppService _organizationUnitAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IProfissaoAppService _profissaoAppService;
        private readonly INaturalidadeAppService _naturalidadeAppService;
        //private readonly IOrigemAppService _origemAppService;
        //private readonly IPlanoAppService _planoAppService;
        private readonly IPaisAppService _paisAppService;
        private readonly IEstadoAppService _estadoAppService;
        private readonly ICidadeAppService _cidadeAppService;
        //private readonly IConvenioAppService _convenioAppService;
        private readonly ISexoAppService _sexoAppService;
        private readonly IEscolaridadeAppService _escolaridadeAppService;
        private readonly ICorPeleAppService _corPeleAppService;
        private readonly IReligiaoAppService _religiaoAppService;
        private readonly IEstadoCivilAppService _estadoCivilAppService;
        private readonly ITipoTelefoneAppService _tipoTelefoneAppService;
        private readonly IPacientePesoAppService _pacientePesoAppService;
        //private readonly ILeitoAppService _leitoAppService;
        private readonly IAgendamentoConsultaMedicoDisponibilidadeAppService _agendamentoConsultaMedicoDisponibilidadeAppService;
        private readonly IEspecialidadeAppService _especialidadeAppService;
        //private readonly IServicoMedicoPrestadoAppService _servicoMedicoPrestadoAppService;
        private readonly IAgendamentoConsultaAppService _agendamentoConsultaAppService;
        private readonly IPreAtendimentoAppService _preAtendimentoAppService;
        private readonly IUserAppService _userAppService;
        private readonly UserManager _userManager;
        private readonly INacionalidadeAppService _nacionalidadeAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly ILocalChamadasAppService _localChamadasAppService;


        public AtendimentosController(
            IUserNavigationManager userNavigationManager,
            IAtendimentoAppService atendimentoAppService,
            IGuiaAppService guiaAppService,
            IGuiaTipoAppService guiaTipoAppService,
            // ITipoAtendimentoAppService tipoAtendimentoAppService,
            IPacienteAppService pacienteAppService,
             IMedicoAppService medicoAppService,
            IOrganizationUnitAppService organizationUnitAppService,
            IEmpresaAppService empresaAppService,
            IProfissaoAppService profissaoAppService,
            INaturalidadeAppService naturalidadeAppService,
            //IOrigemAppService origemAppService,
            IPaisAppService paisAppService,
            IEstadoAppService estadoAppService,
            ICidadeAppService cidadeAppService,
            // IPlanoAppService planoAppService,
            // IConvenioAppService convenioAppService,
            ISexoAppService sexoAppService,
            IEscolaridadeAppService escolaridadeAppService,
            ICorPeleAppService corPeleAppService,
            IReligiaoAppService religiaoAppService,
            IEstadoCivilAppService estadoCivilAppService,
            ITipoTelefoneAppService tipoTelefoneAppService,
            IPacientePesoAppService pacientePesoAppService,
            //ILeitoAppService leitoAppService,
            IAgendamentoConsultaMedicoDisponibilidadeAppService agendamentoConsultaMedicoDisponibilidadeAppService,
            IEspecialidadeAppService especialidadeAppService,
            IAgendamentoConsultaAppService agendamentoConsultaAppService,
            // IServicoMedicoPrestadoAppService servicoMedicoPrestadoAppService,
            IPreAtendimentoAppService preAtendimentoAppService,
             IUserAppService userAppService,
            UserManager userManager,
            INacionalidadeAppService nacionalidadeAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            ILocalChamadasAppService localChamadasAppService
            )
        {
            _atendimentoAppService = atendimentoAppService;
            _guiaTipoAppService = guiaTipoAppService;
            _guiaAppService = guiaAppService;
            // _tipoAtendimentoAppService = tipoAtendimentoAppService;
            _pacienteAppService = pacienteAppService;
            _medicoAppService = medicoAppService;
            _organizationUnitAppService = organizationUnitAppService;
            _empresaAppService = empresaAppService;
            _profissaoAppService = profissaoAppService;
            _naturalidadeAppService = naturalidadeAppService;
            //_origemAppService = origemAppService;
            //_planoAppService = planoAppService;
            _paisAppService = paisAppService;
            _estadoAppService = estadoAppService;
            _cidadeAppService = cidadeAppService;
            //_convenioAppService = convenioAppService;
            _sexoAppService = sexoAppService;
            _escolaridadeAppService = escolaridadeAppService;
            _corPeleAppService = corPeleAppService;
            _religiaoAppService = religiaoAppService;
            _estadoCivilAppService = estadoCivilAppService;
            _tipoTelefoneAppService = tipoTelefoneAppService;
            _pacientePesoAppService = pacientePesoAppService;
            //  _leitoAppService = leitoAppService;
            _agendamentoConsultaMedicoDisponibilidadeAppService = agendamentoConsultaMedicoDisponibilidadeAppService;
            _especialidadeAppService = especialidadeAppService;
            _agendamentoConsultaAppService = agendamentoConsultaAppService;
            _preAtendimentoAppService = preAtendimentoAppService;
            //_servicoMedicoPrestadoAppService = servicoMedicoPrestadoAppService;
            _userAppService = userAppService;
            _userManager = userManager;
            _nacionalidadeAppService = nacionalidadeAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _localChamadasAppService = localChamadasAppService;
        }

        #endregion

        #region Index e Modais

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Atendimento_Create, AppPermissions.Pages_Tenant_Cadastros_Atendimento_Edit)]
        public async Task<ActionResult> Index(long? id, int abaId, bool internacao = false, bool ambulatorioEmergencia = false)
        {
            // var pacientes = await _pacienteAppService.Listar(new ListarPacientesInput());
            //  var origens = await _origemAppService.ListarTodos();
            // var locaisProcedencia = await _origemAppService.ListarTodos();// temp, atualizar depois de confirmar o que eh local de procedencia
            // var tiposAtendimento = await _tipoAtendimentoAppService.Listar(new ListarTiposAtendimentoInput());
            // var medicos = await _medicoAppService.ListarTodos();
            //var especialidades = await _especialidadeAppService.ListarTodos();
            //var convenios = await _convenioAppService.ListarTodos();
            // var planos = await _planoAppService.ListarTodos();
            // var leitos = await _leitoAppService.ListarTodos();
            // var servicosMedicosPrestados = await _servicoMedicoPrestadoAppService.ListarTodos();
            // var guiaTipos = await _guiaAppService.ListarTodos();

            // Unidades organizacionais, populando por tipo de atendimento
            //ListResultDto<UnidadeOrganizacionalDto> unidadesOrganizacionais = new ListResultDto<UnidadeOrganizacionalDto>();
            //ListResultDto<LeitoDto> leitos = new ListResultDto<LeitoDto>();

            //if (internacao)
            //{
            //    unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaInternacao();
            //}
            //else
            //{
            //    unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaAmbulatorioEmergencia();
            //}

            //var uoID = unidadesOrganizacionais.Items[0].Id;
            //leitos = await _leitoAppService.ListarPorUnidadeParaDrop2(uoID);

            //if (unidadesOrganizacionais.Items.Count == 1)
            //{
            //    var uoID = unidadesOrganizacionais.Items[0].Id;
            //    leitos = await _leitoAppService.ListarPorUnidadeParaDrop2(uoID);
            //}else
            //{
            //    leitos.Items[0].Id = 0;

            //}

            var userId = AbpSession.UserId;
            //var user = await _userManager.GetUserByIdAsync((long)userId);
            // var nacionalidades = _origemAppService.ListarDropdown();

            var user = Task.Run(() => _userAppService.GetUser()).Result;
            var medicoId = user.MedicoId;
            if (medicoId.HasValue)
            {
                ViewBag.UserMedicoId = medicoId.Value;
                ViewBag.UserMedico = Task.Run(() => _medicoAppService.Obter(medicoId.Value)).Result;
            }

            var userEmpresas = await _userAppService.GetUserEmpresas(userId.Value);
            // ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);

            ListResultDto<EmpresaDto> empresas = userEmpresas;

            //if (empresas == null || empresas.Items.Count == 0)
            //{
            //    empresas = await _empresaAppService.ListarTodos();
            //}

            CriarOuEditarAtendimentoModalViewModel viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());

           
            HttpCookie cookie = Request.Cookies.Get("localChamada");

            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                long localChamadaId;

                if (long.TryParse(cookie.Value, out localChamadaId))
                {
                    var localChamada = await _localChamadasAppService.Obter(localChamadaId);

                    if (localChamada != null)
                    {
                        viewModel.LocalChamadaId = localChamada.Id;
                        viewModel.LocalChamada = localChamada;

                        viewModel.TipoLocalChamadaId = localChamada.TipoLocalChamadaId;
                    }
                }
            }

            if (id == 0)
                id = null;

            #region Edicao
            if (id.HasValue)
            {
                var output = await _atendimentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarAtendimentoModalViewModel(output);

                // viewModel.AtendimentoTipos = new SelectList(tiposAtendimento.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao",output.AtendimentoTipoId);
                //viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} - {1} - {2}", m.NomeCompleto, m.Cpf, m.Nascimento) }), "Id", "Nome");
                //  viewModel.Origens = new SelectList(origens.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //  viewModel.LocaisProcedencia = new SelectList(locaisProcedencia.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.Medicos = new SelectList(medicos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
                // viewModel.Especialidades = new SelectList(especialidades.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.Nome) }), "Id", "Nome");
                viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                //viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                //viewModel.Planos = new SelectList(planos.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //  viewModel.Leitos = new SelectList(leitos.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                // viewModel.ServicosMedicosPrestados = new SelectList(servicosMedicosPrestados.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                // viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.GuiaTipos = new SelectList(guiaTipos.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.Nacionalidades = new SelectList(nacionalidades.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao", output.NacionalidadeResponsavelId);
                // viewModel.AbaId = abaId;
                //viewModel.Internacao = internacao;
                //viewModel.IsAmbulatorioEmergencia = ambulatorioEmergencia;

            }
            #endregion edicao.
            #region Criacao
            else
            {
                // viewModel = new CriarOuEditarAtendimentoModalViewModel(new CriarOuEditarAtendimento());
                //   viewModel.AtendimentoTipos = new SelectList(tiposAtendimento.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //  viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0} - {1} - {2}", m.NomeCompleto, m.Cpf, m.Nascimento) }), "Id", "Nome");

                //viewModel.Pacientes = new SelectList("","");

                //viewModel.Origens = new SelectList(origens.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.LocaisProcedencia = new SelectList(locaisProcedencia.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.Medicos = new SelectList(medicos.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
                //viewModel.Especialidades = new SelectList(especialidades.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.Nome) }), "Id", "Nome");
                viewModel.Empresas = new SelectList(empresas.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                //viewModel.Convenios = new SelectList(convenios.Items.Select(m => new { Id = m.Id, NomeFantasia = string.Format("{0}", m.NomeFantasia) }), "Id", "NomeFantasia");
                //viewModel.Planos = new SelectList(planos.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                // viewModel.Leitos = new SelectList(leitos.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.ServicosMedicosPrestados = new SelectList(servicosMedicosPrestados.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                // viewModel.UnidadesOrganizacionais = new SelectList(unidadesOrganizacionais.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.GuiaTipos = new SelectList(guiaTipos.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.Nacionalidades = new SelectList(nacionalidades.Items.Select(m => new { Id = m.Id, Descricao = string.Format("{0}", m.Descricao) }), "Id", "Descricao");
                //viewModel.AbaId = abaId;
                //viewModel.Internacao = internacao;
                //viewModel.IsAmbulatorioEmergencia = ambulatorioEmergencia;
                //viewModel.DataRegistro = DateTime.Now;
            }
            #endregion criacao.
            viewModel.AbaId = abaId;
            viewModel.IsInternacao = internacao;
            viewModel.IsAmbulatorioEmergencia = ambulatorioEmergencia;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/IndexParcial.cshtml", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Cadastros_Atendimento_Create, AppPermissions.Pages_Tenant_Cadastros_Atendimento_Edit)]
        public async Task<PartialViewResult> CriarOuEditarModal(long? id)
        {
            var pacientes = await _pacienteAppService.Listar(new ListarPacientesInput());

            CriarOuEditarAtendimentoModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _atendimentoAppService.Obter((long)id);
                viewModel = new CriarOuEditarAtendimentoModalViewModel(output);
                viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            }
            else
            {
                viewModel = new CriarOuEditarAtendimentoModalViewModel(new AtendimentoDto());
                viewModel.Pacientes = new SelectList(pacientes.Items.Select(m => new { Id = m.Id, Nome = string.Format("{0}", m.NomeCompleto) }), "Id", "Nome");
            }

            viewModel.PreAtendimento = false;

            viewModel.FichaAmbulatorioInput = new Fichas.FichaAmbulatorioInput();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_IndexParcial.cshtml", viewModel);
        }

        public ActionResult ModalOrcamento()
        {
            var model = new OrcamentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/Orcamentos/Index.cshtml", model);
        }

        public ActionResult ModalPreAtendimento()
        {
            var model = new PreAtendimentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/PreAtendimentos/Index.cshtml", model);
        }

        public ActionResult ModalClassificacaoRisco()
        {
            var model = new ClassificacoesRiscoViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/ClassificacaoRiscos/Index.cshtml", model);
        }




        public async Task<PartialViewResult> CancelarAtendimentoModal(long? id)
        {
            var viewModel = new CancelamentoViewModel();
            viewModel.AtendimentoId = id;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Cancelamento/CancelarAtendimentoModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> ReativarAtendimentoModal(long? id)
        {
            var viewModel = new CancelamentoViewModel();
            viewModel.AtendimentoId = id;

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/Cancelamento/ReativarAtendimentoModal.cshtml", viewModel);
        }


        #endregion

        #region Parciais

        //public PartialViewResult DropdownEspecialidades(int abaId, long? medicoId)
        //{
        //    var viewModel = new DropdownEspecialidadesViewModel();

        //    if (medicoId.HasValue)
        //    {
        //        var especialidades = AsyncHelper.RunSync(() => _especialidadeAppService.ListarPorMedico((long)medicoId));
        //        viewModel.Especialidades = new SelectList(especialidades.Select(e => new { Id = e.Id, Nome = string.Format("{0}", e.Nome) }), "Id", "Nome");
        //    }
        //    else
        //    {
        //        var especialidades = AsyncHelper.RunSync(() => _especialidadeAppService.ListarTodos());
        //        viewModel.Especialidades = new SelectList(especialidades.Items.Select(e => new { Id = e.Id, Nome = string.Format("{0}", e.Nome) }), "Id", "Nome");
        //    }

        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_DropdownEspecialidades.cshtml", viewModel);
        //}

        //public PartialViewResult DropdownLeitos(int abaId, long? unidadeId)
        //{
        //    var viewModel = new DropdownLeitosViewModel();

        //    if (unidadeId.HasValue)
        //    {
        //        var leitos = AsyncHelper.RunSync(() => _leitoAppService.ListarPorUnidadeParaDrop((long)unidadeId));
        //        viewModel.Leitos = new SelectList(leitos.Select(e => new { Id = e.Id, Descricao = string.Format("{0}", e.Descricao) }), "Id", "Descricao");
        //    }
        //    else
        //    {
        //        var leitos = AsyncHelper.RunSync(() => _leitoAppService.ListarTodos());
        //        viewModel.Leitos = new SelectList(leitos.Items.Select(e => new { Id = e.Leito.Id, Descricao = string.Format("{0}", e.Leito.Descricao) }), "Id", "Descricao");
        //    }

        //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_DropdownLeitos.cshtml", viewModel);
        //}

        public async Task<PartialViewResult> ExibirGuiaParaImpressao(string nomeGuia)
        {
            var viewModel = new ExibirGuiaImpressaoViewModel();
            viewModel.Guia = nomeGuia;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_ExibirGuiaImpressao.cshtml", viewModel);
        }

        public PartialViewResult _PesquisarPreAtendimento()
        {
            var model = new PreAtendimentosViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_PesquisarPreAtendimentos.cshtml", model);
        }

        public PartialViewResult _PesquisarPaciente()
        {
            var model = new PacientesViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_PesquisarPacientes.cshtml", model);
        }

        public PartialViewResult _SelecionarreAtendimentoModal(PreAtendimentosViewModel model)
        {
            ViewBag.DivisaoPrincipalId = model.Filtro;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/_SelecionarPreAtendimentoModal.cshtml", model);

            //public PartialViewResult _PreAtendimento()
            //{
            //    var model = new PreAtendimentosViewModel();
            //    //return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/PreAtendimentos/Index.cshtml", model);
            //    return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/PreAtendimentos/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _CriarOuEditarPreAtendimento()
        {
            CriarOuEditarPreAtendimentoModalViewModel viewModel;
            viewModel = new CriarOuEditarPreAtendimentoModalViewModel(new CriarOuEditarPreAtendimento());
            var sexos = await _sexoAppService.ListarTodos();
            viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/PreAtendimentos/_CriarOuEditarModal.cshtml", viewModel);
        }

        public PartialViewResult _ClassificacaoRisco()
        {
            var model = new ClassificacoesRiscoViewModel();
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/ClassificacoesRisco/Index.cshtml", model);
        }

        public async Task<PartialViewResult> _CriarOuEditarClassificacaoRisco()
        {
            var especialidades = await _especialidadeAppService.ListarTodos();

            CriarOuEditarClassificacaoRiscoModalViewModel viewModel;

            viewModel = new CriarOuEditarClassificacaoRiscoModalViewModel(new CriarOuEditarClassificacaoRisco());
            viewModel.Especialidades = new SelectList(especialidades.Items, "Id", "Nome");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/ClassificacoesRisco/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _IdentificacaoPaciente(long? id)
        {
            //var origens = await _origemAppService.Listar(new ListarOrigensInput());
            var sexos = await _sexoAppService.ListarTodos();
            var coresPele = await _corPeleAppService.ListarTodos();
            var escolaridades = await _escolaridadeAppService.ListarTodos();
            var religioes = await _religiaoAppService.ListarTodos();
            var estadosCivis = await _estadoCivilAppService.ListarTodos();
            var tiposTelefone = await _tipoTelefoneAppService.ListarTodos();

            CriarOuEditarPacienteModalViewModel viewModel;

            if (id.HasValue)
            {
                var output = await _pacienteAppService.Obter2((long)id);

                viewModel = new CriarOuEditarPacienteModalViewModel(output);
                //viewModel.Origens = new SelectList(origens.Items, "Id", "Descricao", output.OrigemId);
                //viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao", output.Sexo);
                //viewModel.Escolaridades = new SelectList(escolaridades.Items, "Id", "Descricao", output.Escolaridade);
                //viewModel.CoresPele = new SelectList(coresPele.Items, "Id", "Descricao", output.CorPele);
                //viewModel.Religioes = new SelectList(religioes.Items, "Id", "Descricao", output.Religiao);
                //     viewModel.EstadosCivis = new SelectList(estadosCivis.Items, "Id", "Descricao", output.EstadoCivil);
                //  viewModel.TiposTelefone = new SelectList(tiposTelefone.Items, "Id", "Descricao");
            }
            else
            {
                viewModel = new CriarOuEditarPacienteModalViewModel(new PacienteDto());
                //viewModel.Origens = new SelectList(origens.Items, "Id", "Descricao");
                //viewModel.Sexos = new SelectList(sexos.Items, "Id", "Descricao");
                //viewModel.CoresPele = new SelectList(coresPele.Items, "Id", "Descricao");
                //viewModel.Escolaridades = new SelectList(escolaridades.Items, "Id", "Descricao");
                //viewModel.Religioes = new SelectList(religioes.Items, "Id", "Descricao");
                //viewModel.EstadosCivis = new SelectList(estadosCivis.Items, "Id", "Descricao");
                //viewModel.TiposTelefone = new SelectList(tiposTelefone.Items, "Id", "Descricao");
            }

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/Pacientes/_IdentificacaoPaciente.cshtml", viewModel);
            //return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Atendimentos/_IdentificacaoPaciente.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _Agendamento()
        {
            var agendamentos = await _agendamentoConsultaMedicoDisponibilidadeAppService.ListarAtivos(0, 0);
            var especialidadesAgendadas = agendamentos.Select(m => m.MedicoEspecialidade.EspecialidadeId).Distinct().ToList();
            var especialidades = await _especialidadeAppService.Listar(especialidadesAgendadas);
            var viewModel = new AgendamentoConsultasViewModel();
            viewModel.Especialidades = new SelectList(especialidades.Items, "Id", "Nome");

            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Home/Agendamentos/Index.cshtml", viewModel);
        }

        #endregion

        #region Metodos auxiliares

        [HttpPost]
        public async Task<long> SalvarAtendimento(AtendimentoDto atendimento)
        {
            //AtendimentoDto atend = new AtendimentoDto();
            long atend = new long();
            atend = await _atendimentoAppService.CriarOuEditar(atendimento);
            // return Content(L("Sucesso"));
            return atend;
        }

        [HttpPost]
        public async Task<long> SalvarPreAtendimento(CriarOuEditarPreAtendimento preAtendimento)
        {
            var preAtendimentoInserido = await _preAtendimentoAppService.CriarGetId(preAtendimento);
            return preAtendimentoInserido;
        }

        //[HttpPost]
        //public async Task<string> ImprimirGuia(CriarOuEditarAtendimento at)
        //{
        //    var id = input.Id;
        //    var atendimento = await _atendimentoAppService.Obter(id);
        //    string guiaTemplate = Caminhos.Atendimentos.Guias.GuiasModelo + atendimento.Guia.Descricao + ".pdf";
        //    string guiaDestino = Caminhos.Atendimentos.Guias.GuiasModelo + atendimento.Guia.Descricao + "_" + atendimento.Id.ToString() + ".pdf";

        //    try
        //    {
        //        ByteBuffer.HIGH_PRECISION = true;
        //        PdfReader reader = new PdfReader(guiaTemplate);
        //        using (FileStream fs = new FileStream(guiaDestino, FileMode.Create, FileAccess.Write, FileShare.None))
        //        using (PdfStamper stamper = new PdfStamper(reader, fs))
        //        {
        //            PdfContentByte cb = stamper.GetOverContent(1);
        //            PdfLayer camada = new PdfLayer("CamadaDados", stamper.Writer);
        //            cb.BeginLayer(camada);
        //            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10);
        //            PdfGState gState = new PdfGState();
        //            cb.SetGState(gState);
        //            cb.SetColorFill(BaseColor.BLACK);
        //            cb.BeginText();



        //            Atendimento atd = atendimento.MapTo<Atendimento>();
        //            var atendimentoProps = atd.GetType().GetProperties();
        //            var guia = await _guiaAppService.Obter(atd.Guia.Id);
        //            CriarOuEditarGuia guiaConsulta = guia;

        //            foreach (var item in guiaConsulta.Campos)
        //            {
        //                guiaConsulta.NomesCampos.Add(item.GuiaCampo.Descricao);
        //            }

        //            foreach (PropertyInfo p in atendimentoProps)
        //            {
        //                // temp?
        //                if (p.PropertyType != typeof(Paciente) && p.PropertyType != typeof(Medico) && p.PropertyType != typeof(Plano) && p.PropertyType != typeof(Convenio))
        //                    continue;

        //                string nomeProp = p.Name;

        //                if (guiaConsulta.NomesCampos.Contains(nomeProp))
        //                {
        //                    // retirar os try/catch quando terminar os testes
        //                    try
        //                    {
        //                        var valor = atd[nomeProp];
        //                        if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                        {
        //                            List<RelacaoGuiaCampoDto> campos = null;// guiaConsulta.Campos.ToList();
        //                            var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp);
        //                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, item.CoordenadaY, 0f);
        //                        }
        //                    }
        //                    catch (Exception ex) { ex.ToString(); }
        //                }

        //                var pProps = p.PropertyType.GetProperties();

        //                foreach (PropertyInfo pp in pProps)
        //                {
        //                    // temp?
        //                    if (pp.PropertyType != typeof(string) && pp.PropertyType != typeof(DateTime) && pp.PropertyType != typeof(Cidade))
        //                        continue;

        //                    string nomeProp2 = nomeProp + "." + pp.Name;

        //                    if (guiaConsulta.NomesCampos.Contains(nomeProp2))
        //                    {
        //                        // retirar os try/catch quando terminar os testes
        //                        try
        //                        {
        //                            var valor = atd[nomeProp2];
        //                            if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                            {
        //                                List<RelacaoGuiaCampoDto> campos = null;//guiaConsulta.Campos.ToList();
        //                                var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp2);

        //                                var page = reader.GetPageSize(1);

        //                                //    double pag = page.Height * 1.3f;

        //                                //double pag = page.Height;

        //                                //double y = (item.CoordenadaY * page.Height) / pag;

        //                                //y = ((page.Height - y) * 1.1);

        //                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX * 0.55f, (float)y, 0f);

        //                                var y = page.Height - item.CoordenadaY - 15;

        //                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, y, 0f);
        //                            }
        //                        }
        //                        catch (Exception ex) { ex.ToString(); }
        //                    }

        //                    var bases = guiaConsulta.PegarClassesBase(pp.PropertyType);

        //                    foreach (Type t in bases)
        //                    {
        //                        var ppProps = t.GetProperties();

        //                        foreach (var ppp in ppProps)
        //                        {
        //                            if (ppp.PropertyType != typeof(string) && ppp.PropertyType != typeof(DateTime))
        //                                continue;

        //                            string nomeProp3 = nomeProp2 + "." + ppp.Name;

        //                            if (guiaConsulta.NomesCampos.Contains(nomeProp3))
        //                            {
        //                                var valor = atd[nomeProp3];
        //                                if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                                {
        //                                    List<RelacaoGuiaCampoDto> campos = null;//guiaConsulta.Campos.ToList();
        //                                    var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp3);
        //                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, item.CoordenadaY, 0f);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            // Logo
        //            string logoPath = @"C:\Users\SWDev\Source\Repos\SW10.SWMANAGER\SW10.SWMANAGER.Web\Areas\Mpa\Views\Aplicacao\Atendimentos\Home\Arquivos\logo.png";
        //            Image logo = Image.GetInstance(logoPath);
        //            logo.ScalePercent(75.0f);
        //            logo.Alignment = Element.ALIGN_CENTER;
        //            logo.SetAbsolutePosition(50.0f, 740.0f);
        //            cb.AddImage(logo);

        //            cb.EndText();
        //            cb.EndLayer();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }


        //    string nomeArquivo = atendimento.Guia.Descricao + "_" + atendimento.Id.ToString() + ".pdf";
        //    TempData["guiaParaImprimir"] = guiaDestino;
        //    return nomeArquivo;
        //}


        // COMENTADO TEMPORARIAMENTE
        //[HttpPost]
        //public async Task<string> ImprimirGuia(CriarOuEditarAtendimento input)
        //{
        //    var id = input.Id;
        //    var criarOuEditarAtendimento = await _atendimentoAppService.Obter(id);
        //    string guiaTemplate = Caminhos.Atendimentos.Guias.GuiasModelo + criarOuEditarAtendimento.Guia.Descricao + ".pdf";
        //    string guiaDestino = Caminhos.Atendimentos.Guias.GuiasModelo + criarOuEditarAtendimento.Guia.Descricao + "_" + criarOuEditarAtendimento.Id.ToString() + ".pdf";

        //    try
        //    {
        //        ByteBuffer.HIGH_PRECISION = true;
        //        PdfReader reader = new PdfReader(guiaTemplate);
        //        using (FileStream fs = new FileStream(guiaDestino, FileMode.Create, FileAccess.Write, FileShare.None))
        //        using (PdfStamper stamper = new PdfStamper(reader, fs))
        //        {
        //            PdfContentByte cb = stamper.GetOverContent(1);
        //            PdfLayer camada = new PdfLayer("CamadaDados", stamper.Writer);
        //            cb.BeginLayer(camada);
        //            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10);
        //            PdfGState gState = new PdfGState();
        //            cb.SetGState(gState);
        //            cb.SetColorFill(BaseColor.BLACK);
        //            cb.BeginText();

        //            Atendimento atendimento = criarOuEditarAtendimento.MapTo<Atendimento>();
        //            var atendimentoProps = atendimento.GetType().GetProperties();
        //            var guia = await _guiaAppService.Obter(atendimento.Guia.Id);
        //            CriarOuEditarGuia guiaConsulta = guia;

        //            List<RelacaoGuiaCampoDto> campos = guiaConsulta.Campos.ToList();

        //            //List<Type> tiposValidos = new List<Type>();


        //            foreach (var item in guiaConsulta.Campos)
        //            {
        //                guiaConsulta.NomesCampos.Add(item.GuiaCampo.Descricao);
        //            }

        //            foreach (PropertyInfo p in atendimentoProps)
        //            {
        //                // temp?
        //                if (p.PropertyType != typeof(Paciente) && p.PropertyType != typeof(Medico) && p.PropertyType != typeof(Plano) && p.PropertyType != typeof(Convenio))
        //                    continue;

        //                string nomeProp = p.Name;

        //                // verificacao se campo contem '.' nao e propriedade (e apenas classe/conjunto) // temp
        //                if (nomeProp.Contains('.') && guiaConsulta.NomesCampos.Contains(nomeProp))
        //                {
        //                    // retirar os try/catch quando terminar os testes
        //                    try
        //                    {
        //                        var valor = atendimento[nomeProp];
        //                        if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                        {
        //                            var item = campos.FirstOrDefault(c => c.GuiaCampo.Descricao == nomeProp);// apenas First?
        //                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, item.CoordenadaY, 0f);
        //                            campos.FirstOrDefault(c => c.GuiaCampo.Descricao == nomeProp).GuiaCampo.Descricao = string.Empty; // evitando repeticao (temp)
        //                        }
        //                    }
        //                    catch (Exception ex) { ex.ToString(); }
        //                }

        //                var pProps = p.PropertyType.GetProperties();

        //                foreach (PropertyInfo pp in pProps)
        //                {
        //                    // temp?
        //                    if (pp.PropertyType != typeof(string) && pp.PropertyType != typeof(DateTime) && pp.PropertyType != typeof(Cidade))
        //                        continue;

        //                    string nomeProp2 = nomeProp + "." + pp.Name;

        //                    if (guiaConsulta.NomesCampos.Contains(nomeProp2))
        //                    {
        //                        // retirar os try/catch quando terminar os testes
        //                        try
        //                        {
        //                            var valor = atendimento[nomeProp2];
        //                            if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                            {
        //                                var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp2);
        //                                var page = reader.GetPageSize(1);
        //                                var y = page.Height - item.CoordenadaY - 15;
        //                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, y, 0f);
        //                                item.GuiaCampo.Descricao = string.Empty;
        //                            }
        //                        }
        //                        catch (Exception ex) { ex.ToString(); }
        //                    }

        //                    var bases = guiaConsulta.PegarClassesBase(pp.PropertyType);

        //                    foreach (Type t in bases)
        //                    {
        //                        var ppProps = t.GetProperties();

        //                        foreach (var ppp in ppProps)
        //                        {
        //                            if (ppp.PropertyType != typeof(string) && ppp.PropertyType != typeof(DateTime))
        //                                continue;

        //                            string nomeProp3 = nomeProp2 + "." + ppp.Name;

        //                            if (guiaConsulta.NomesCampos.Contains(nomeProp3))
        //                            {
        //                                var valor = atendimento[nomeProp3];
        //                                if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                                {
        //                                    var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp3);
        //                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, item.CoordenadaY, 0f);
        //                                    item.GuiaCampo.Descricao = string.Empty;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            try
        //            {
        //                string logoPath = Caminhos.Atendimentos.Guias.GuiasModelo + "logo.png";

        //                Image logo = Image.GetInstance(logoPath);
        //                logo.ScalePercent(75.0f);
        //                logo.Alignment = Element.ALIGN_CENTER;
        //                logo.SetAbsolutePosition(50.0f, 740.0f);
        //                cb.AddImage(logo);
        //            }
        //            catch {}

        //            cb.EndText();
        //            cb.EndLayer();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    string nomeArquivo = criarOuEditarAtendimento.Guia.Descricao + "_" + criarOuEditarAtendimento.Id.ToString() + ".pdf";
        //    TempData["guiaParaImprimir"] = guiaDestino;
        //    return nomeArquivo;
        //}
        // FIM COMENTADO TEMPORARIAMENTE


        //[HttpPost]
        //public async Task<string> ImprimirGuia(CriarOuEditarAtendimento at)
        //{
        //    var id = input.Id;
        //    var atendimento = await _atendimentoAppService.Obter(id);
        //    string guiaTemplate = Caminhos.Atendimentos.Guias.GuiasModelo + atendimento.Guia.Descricao + ".pdf";
        //    string guiaDestino = Caminhos.Atendimentos.Guias.GuiasModelo + atendimento.Guia.Descricao + "_" + atendimento.Id.ToString() + ".pdf";

        //    try
        //    {
        //        ByteBuffer.HIGH_PRECISION = true;
        //        PdfReader reader = new PdfReader(guiaTemplate);
        //        using (FileStream fs = new FileStream(guiaDestino, FileMode.Create, FileAccess.Write, FileShare.None))
        //        using (PdfStamper stamper = new PdfStamper(reader, fs))
        //        {
        //            PdfContentByte cb = stamper.GetOverContent(1);
        //            PdfLayer camada = new PdfLayer("CamadaDados", stamper.Writer);
        //            cb.BeginLayer(camada);
        //            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10);
        //            PdfGState gState = new PdfGState();
        //            cb.SetGState(gState);
        //            cb.SetColorFill(BaseColor.BLACK);
        //            cb.BeginText();



        //            Atendimento atd = atendimento.MapTo<Atendimento>();
        //            var atendimentoProps = atd.GetType().GetProperties();
        //            var guia = await _guiaAppService.Obter(atd.Guia.Id);
        //            CriarOuEditarGuia guiaConsulta = guia;

        //            foreach (var item in guiaConsulta.Campos)
        //            {
        //                guiaConsulta.NomesCampos.Add(item.GuiaCampo.Descricao);
        //            }

        //            foreach (PropertyInfo p in atendimentoProps)
        //            {
        //                // temp?
        //                if (p.PropertyType != typeof(Paciente) && p.PropertyType != typeof(Medico) && p.PropertyType != typeof(Plano) && p.PropertyType != typeof(Convenio))
        //                    continue;

        //                string nomeProp = p.Name;

        //                if (guiaConsulta.NomesCampos.Contains(nomeProp))
        //                {
        //                    // retirar os try/catch quando terminar os testes
        //                    try
        //                    {
        //                        var valor = atd[nomeProp];
        //                        if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                        {
        //                            List<RelacaoGuiaCampoDto> campos = null;// guiaConsulta.Campos.ToList();
        //                            var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp);
        //                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, item.CoordenadaY, 0f);
        //                        }
        //                    }
        //                    catch (Exception ex) { ex.ToString(); }
        //                }

        //                var pProps = p.PropertyType.GetProperties();

        //                foreach (PropertyInfo pp in pProps)
        //                {
        //                    // temp?
        //                    if (pp.PropertyType != typeof(string) && pp.PropertyType != typeof(DateTime) && pp.PropertyType != typeof(Cidade))
        //                        continue;

        //                    string nomeProp2 = nomeProp + "." + pp.Name;

        //                    if (guiaConsulta.NomesCampos.Contains(nomeProp2))
        //                    {
        //                        // retirar os try/catch quando terminar os testes
        //                        try
        //                        {
        //                            var valor = atd[nomeProp2];
        //                            if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                            {
        //                                List<RelacaoGuiaCampoDto> campos = null;//guiaConsulta.Campos.ToList();
        //                                var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp2);

        //                                var page = reader.GetPageSize(1);

        //                                //    double pag = page.Height * 1.3f;

        //                                //double pag = page.Height;

        //                                //double y = (item.CoordenadaY * page.Height) / pag;

        //                                //y = ((page.Height - y) * 1.1);

        //                                //cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX * 0.55f, (float)y, 0f);

        //                                var y = page.Height - item.CoordenadaY - 15;

        //                                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, y, 0f);
        //                            }
        //                        }
        //                        catch (Exception ex) { ex.ToString(); }
        //                    }

        //                    var bases = guiaConsulta.PegarClassesBase(pp.PropertyType);

        //                    foreach (Type t in bases)
        //                    {
        //                        var ppProps = t.GetProperties();

        //                        foreach (var ppp in ppProps)
        //                        {
        //                            if (ppp.PropertyType != typeof(string) && ppp.PropertyType != typeof(DateTime))
        //                                continue;

        //                            string nomeProp3 = nomeProp2 + "." + ppp.Name;

        //                            if (guiaConsulta.NomesCampos.Contains(nomeProp3))
        //                            {
        //                                var valor = atd[nomeProp3];
        //                                if (!String.IsNullOrWhiteSpace(valor.ToString()))
        //                                {
        //                                    List<RelacaoGuiaCampoDto> campos = null;//guiaConsulta.Campos.ToList();
        //                                    var item = campos.Find(c => c.GuiaCampo.Descricao == nomeProp3);
        //                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), item.CoordenadaX, item.CoordenadaY, 0f);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            // Logo
        //            string logoPath = @"C:\Users\SWDev\Source\Repos\SW10.SWMANAGER\SW10.SWMANAGER.Web\Areas\Mpa\Views\Aplicacao\Atendimentos\Home\Arquivos\logo.png";
        //            Image logo = Image.GetInstance(logoPath);
        //            logo.ScalePercent(75.0f);
        //            logo.Alignment = Element.ALIGN_CENTER;
        //            logo.SetAbsolutePosition(50.0f, 740.0f);
        //            cb.AddImage(logo);

        //            cb.EndText();
        //            cb.EndLayer();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }


        //    string nomeArquivo = atendimento.Guia.Descricao + "_" + atendimento.Id.ToString() + ".pdf";
        //    TempData["guiaParaImprimir"] = guiaDestino;
        //    return nomeArquivo;
        //}

        [HttpPost]
        public async Task<string> ImprimirGuia(long atendimentoId)
        {
            var _atendimento = await _atendimentoAppService.Obter(atendimentoId);
            var atendimento = _atendimento.MapTo<Atendimento>();
            var guia = await _guiaAppService.Obter((long)atendimento.GuiaId);
            var guiaTemplateBytes = guia.ModeloPDF;

            string guiaDestino = @"C:\Users\SWDev\Documents\Sudano\teste.pdf";

            try
            {
                ByteBuffer.HIGH_PRECISION = true;
                PdfReader reader = new PdfReader(guiaTemplateBytes);
                using (FileStream fs = new FileStream(guiaDestino, FileMode.Create, FileAccess.Write, FileShare.None))
                using (PdfStamper stamper = new PdfStamper(reader, fs))
                {
                    PdfContentByte cb = stamper.GetOverContent(1);
                    PdfLayer camada = new PdfLayer("CamadaDados", stamper.Writer);
                    cb.BeginLayer(camada);
                    cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10);
                    PdfGState gState = new PdfGState();
                    cb.SetGState(gState);
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.BeginText();

                    var atendimentoProps = atendimento.GetType().GetProperties();


                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer(new SimpleTypeResolver());
                    GuiaCampoDto[] campos = new GuiaCampoDto[] { };
                    campos = jsonSerializer.Deserialize<GuiaCampoDto[]>(guia.CamposJson);

                    foreach (var campo in campos)
                    {
                        if (!campo.Descricao.Contains("."))
                        {
                            foreach (var subCampo in campo.SubConjuntos)
                            {
                                try
                                {
                                    var valor = atendimento[subCampo.Descricao];
                                    if (!String.IsNullOrWhiteSpace(valor.ToString()))
                                    {
                                        var page = reader.GetPageSize(1);
                                        var y = page.Height - subCampo.CoordenadaY - 15;
                                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), subCampo.CoordenadaX, y, 0f);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ex.ToString();
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                var valor = atendimento[campo.Descricao];
                                if (!String.IsNullOrWhiteSpace(valor.ToString()))
                                {
                                    var page = reader.GetPageSize(1);
                                    var y = page.Height - campo.CoordenadaY - 15;
                                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, valor.ToString(), campo.CoordenadaX, y, 0f);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.ToString();
                            }
                        }
                    }

                    try
                    {
                        string logoPath = @"logo.png";

                        Image logo = Image.GetInstance(logoPath);
                        logo.ScalePercent(75.0f);
                        logo.Alignment = Element.ALIGN_CENTER;
                        logo.SetAbsolutePosition(50.0f, 740.0f);
                        cb.AddImage(logo);
                    }
                    catch { }

                    cb.EndText();
                    cb.EndLayer();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            //TempData["guiaParaImprimir"] = guiaDestino;// esta sendo usado
            return guiaDestino;
        }



        [HttpPost]
        public async Task<string> ChecarControleAlta(long id)
        {
            return await _unidadeOrganizacionalAppService.ChecarControlaAlta(id);
        }

        #endregion


    }
};