﻿#region usings
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.AtendimentosLeitosMov.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.AtendimentosLeitosMov;
using SW10.SWMANAGER.Web.Controllers;
using System.Threading.Tasks;
using System.Web.Mvc;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AtendimentosLeitosMov;
using SW10.SWMANAGER.Sessions;
using System;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;
using System.Linq;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.AtendimentosLeitosMov.Altas;
using SW10.SWMANAGER.ClassesAplicacao.AtendimentosLeitosMov;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos;
#endregion usings.

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers.Aplicacao.Atendoimentos
{
    public class AtendimentoLeitoMovController : SWMANAGERControllerBase
    {
        #region Dependencias
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly ILeitoAppService _leitoAppService;
        private readonly IAtendimentoLeitoMovAppService _atendimentoLeitoMovAppService;
        private readonly ITipoAcomodacaoAppService _tipoAcomodacaoAppService;
        private ISessionAppService _sessionAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;

        public AtendimentoLeitoMovController(
            IAtendimentoAppService atendimentoAppService,
            ILeitoAppService leitoAppService,
            IAtendimentoLeitoMovAppService atendimentoLeitoMovAppService,
            ITipoAcomodacaoAppService tipoAcomodacaoAppService,
            ISessionAppService sessionAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService

            )
        {
            _atendimentoAppService = atendimentoAppService;
            _atendimentoLeitoMovAppService = atendimentoLeitoMovAppService;
            _tipoAcomodacaoAppService = tipoAcomodacaoAppService;
            _sessionAppService = sessionAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _leitoAppService = leitoAppService;
        }
        #endregion dependencias.

        public ActionResult Index()
        {
            var model = new AtendimentosLeitosMovViewModel();
            return View("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AtendimentosLeitosMov/Index.cshtml", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Atendimento_PreAtendimentos_Create, AppPermissions.Pages_Tenant_Atendimento_PreAtendimentos_Edit)]
        public PartialViewResult CriarOuEditarModal(AtendimentoLeitoMovDto data)
        {
            CriarOuEditarAtendimentoLeitoMovModalViewModel viewModel = new CriarOuEditarAtendimentoLeitoMovModalViewModel(data);
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/AtendimentosLeitosMov/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> CriarOuMapaLeitorModal(AtendimentoLeitoMovDto data)
        {
            CriarOuEditarAtendimentoLeitoMovModalViewModel viewModel = new CriarOuEditarAtendimentoLeitoMovModalViewModel(data);
            ListarTiposAcomodacaoInput teste = new ListarTiposAcomodacaoInput();
            var tiposLeitos = await _tipoAcomodacaoAppService.ListarComLeito(teste);
            data.LeitoId = data.Leito.Id;

            var atendimentoId = data.AtendimentoLeitoMov.AtendimentoId.HasValue ? data.AtendimentoLeitoMov.AtendimentoId.Value : data.AtendimentoId.Value;

            data.AtendimentoLeitoMov.Atendimento = await _atendimentoAppService.Obter(atendimentoId);
            viewModel.TiposLeito = tiposLeitos.Items.ToList();

            viewModel.DataInicial = DateTime.Now; //data.AtendimentoLeitoMov.DataInicial;
            viewModel.DataFinal = data.AtendimentoLeitoMov.DataFinal;
            viewModel.DataInclusao = data.AtendimentoLeitoMov.DataInclusao;
            viewModel.UserId = 1;
            viewModel.AtendimentoLeitoMovId = data.AtendimentoLeitoMovId;
            viewModel.AtendimentoLeitoMov = data.AtendimentoLeitoMov;
            viewModel.AtendimentoId = data.AtendimentoId;
            viewModel.Atendimento = data.AtendimentoLeitoMov.Atendimento;
            viewModel.Atendimento.Paciente = data.AtendimentoLeitoMov.Atendimento.Paciente;
            viewModel.Atendimento.Paciente.NomeCompleto = data.AtendimentoLeitoMov.Atendimento.Paciente.NomeCompleto;
            viewModel.LeitoId = data.AtendimentoLeitoMov.LeitoId;
            viewModel.Leito = data.AtendimentoLeitoMov.Leito;
            viewModel.UnidadeOrganizacionalId = data.AtendimentoLeitoMov.Leito.UnidadeOrganizacionalId;
            viewModel.UnidadeOrganizacional = data.AtendimentoLeitoMov.Leito.UnidadeOrganizacional;

            var unidadesOrganizacionais = await _unidadeOrganizacionalAppService.ListarParaInternacao();

            viewModel.UnidadesOrganizacionais = unidadesOrganizacionais.Items.ToList();


            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/MapaLeitos/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> HistoricoLeitoModal(AtendimentoLeitoMovDto data)
        {
            try
            {
                var mov = _atendimentoLeitoMovAppService.Obter(data.AtendimentoId);
                var leito = new LeitoDto();
                var atendimento = await _atendimentoAppService.Obter(data.AtendimentoId.Value);
                if (data.LeitoId.HasValue)
                {
                    leito = await _leitoAppService.Obter(data.LeitoId.Value);
                }
                else
                {
                    leito = atendimento.Leito;
                }
                CriarOuEditarAtendimentoLeitoMovModalViewModel viewModel = new CriarOuEditarAtendimentoLeitoMovModalViewModel(mov);
                ListarTiposAcomodacaoInput teste = new ListarTiposAcomodacaoInput();
                var tiposLeitos = await _tipoAcomodacaoAppService.Listar(teste);
                viewModel.TiposLeito = tiposLeitos.Items.ToList();
                viewModel.AtendimentoId = data.AtendimentoId.Value;
                viewModel.Atendimento = atendimento;
                viewModel.LeitoId = data.LeitoId.HasValue ? data.LeitoId.Value : atendimento.LeitoId.Value;
                viewModel.Leito = leito;
                return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Internacoes/Home/MapaLeitos/_HistoricoLeito.cshtml", viewModel);
            }
            catch (Exception ex)
            {
                return PartialView(Url.Action("Erro", "Home"));

            }
        }

        [HttpPost]
        public async Task<ActionResult> SalvarAtendimentoLeitoMov(string AtendimentoId, string LeitoId, string UnidOrg, string DataInicial, string DataInclusao, int Edita, string DataFinal = null)
        {
            //DADOS DO USUARIO LOGADO DANDO ERRO DE PERMISSÃO
            var loginInformations = await _sessionAppService.GetCurrentLoginInformations();
            var userId = AbpSession.UserId;

            AtendimentoLeitoMovDto atendimentoLeitoMovDto = new AtendimentoLeitoMovDto();

            atendimentoLeitoMovDto.CreatorUserId = userId;
            atendimentoLeitoMovDto.AtendimentoId = Convert.ToInt64(AtendimentoId);
            atendimentoLeitoMovDto.LeitoId = Convert.ToInt64(LeitoId);
            atendimentoLeitoMovDto.UnidadeOrganizacionalId = Convert.ToInt64(UnidOrg);
            if (!string.IsNullOrWhiteSpace(DataInicial))
            { atendimentoLeitoMovDto.DataInicial = Convert.ToDateTime(DataInicial); }
            if (!string.IsNullOrWhiteSpace(DataInclusao))
            { atendimentoLeitoMovDto.DataInclusao = Convert.ToDateTime(DataInclusao); }
            if (!string.IsNullOrWhiteSpace(DataFinal))
            { atendimentoLeitoMovDto.DataFinal = Convert.ToDateTime(DataFinal); }

            try
            {
                if (!Convert.ToBoolean(Edita))
                {
                    await _atendimentoLeitoMovAppService.Criar(atendimentoLeitoMovDto);
                }
                else
                {
                    await _atendimentoLeitoMovAppService.Editar(atendimentoLeitoMovDto);
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return Content(L("Sucesso"));
        }

        public async Task<ActionResult> AltaModal(AtendimentoLeitoMov input)
        {
            var atendimento = await _atendimentoAppService.Obter((long)input.AtendimentoId);

            AltaModalViewModel viewModel = new AltaModalViewModel();

            viewModel.AtendimentoId = input.AtendimentoId;
            viewModel.Data = (DateTime)(atendimento.DataAlta != null ? atendimento.DataAlta : DateTime.Now);
            viewModel.DataAltaMedica = (DateTime)(atendimento.DataAltaMedica != null ? atendimento.DataAltaMedica : DateTime.Now);
            viewModel.PrevisaoAlta = (DateTime)(atendimento.DataPrevistaAlta != null ? atendimento.DataPrevistaAlta : DateTime.Now);
            viewModel.NumeroObito = atendimento.NumeroObito;
            viewModel.MotivoAlta = atendimento.MotivoAlta;
            viewModel.MotivoAltaId = atendimento.MotivoAltaId;

            if (atendimento.LeitoId.HasValue)
            {
                viewModel.LeitoId = atendimento.Leito.Id;
                viewModel.Leito = atendimento.Leito;
            }
            ViewBag.IsConsulta = false;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Altas/Alta/_CriarOuEditarModal.cshtml", viewModel);
        }

        public async Task<PartialViewResult> _AltaModal(long atendimentoId, bool isConsulta = false)
        {
            var atendimento = await _atendimentoAppService.Obter(atendimentoId);

            AltaModalViewModel viewModel = new AltaModalViewModel(); ;

            viewModel.AtendimentoId = atendimentoId;
            viewModel.Data = (DateTime)(atendimento.DataAlta != null ? atendimento.DataAlta : DateTime.Now);
            viewModel.DataAltaMedica = (DateTime)(atendimento.DataAltaMedica != null ? atendimento.DataAltaMedica : DateTime.Now);
            viewModel.PrevisaoAlta = (DateTime)(atendimento.DataPrevistaAlta != null ? atendimento.DataPrevistaAlta : DateTime.Now);
            if (atendimento.LeitoId.HasValue)
            {
                viewModel.LeitoId = atendimento.Leito.Id;
                viewModel.Leito = atendimento.Leito;
            }
            ViewBag.IsConsulta = isConsulta;
            return PartialView("~/Areas/Mpa/Views/Aplicacao/Atendimentos/Altas/Alta/_CriarOuEditarModal.cshtml", viewModel);
        }

    }
}