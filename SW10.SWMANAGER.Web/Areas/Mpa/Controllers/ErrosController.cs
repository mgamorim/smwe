using Abp.Web.Mvc.Controllers;
using Newtonsoft.Json;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Erros;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers
{
    public class ErrosController : AbpController
    {
        public ActionResult ExibirErros(List<ErroDto> erros)
        {
            //List<ErroDto> _erros = JsonConvert.DeserializeObject<List<ErroDto>>(erros);

            var view = new ErrosViewModel { Erros = erros };

            return PartialView("~/Areas/Mpa/Views/Erros/Index.cshtml", view);
        }
    }
}