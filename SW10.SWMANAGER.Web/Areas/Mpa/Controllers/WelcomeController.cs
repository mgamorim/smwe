using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Web.Controllers;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers
{
    [AbpMvcAuthorize]
    public class WelcomeController : SWMANAGERControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}