﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Services.Dashboards;
using Abp.Threading;
using System;
using System.Linq;
using SW10.SWMANAGER.Web.Areas.Mpa.Models.Dashboards;
using System.Collections.Generic;
using SW10.SWMANAGER.ClassesAplicacao.Services.ViewModels;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Manutencoes.BIs;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Controllers
{

    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class DashboardController : Controller //SWMANAGERControllerBase
    {
        private readonly IDashboardAppService _dashboardAppService;
        private readonly IOperacaoAppService _operacaoAppService;

        public DashboardController(
            IDashboardAppService dashboardAppService,
            IOperacaoAppService operacaoAppService
            )
        {
            _dashboardAppService = dashboardAppService;
            _operacaoAppService = operacaoAppService;
        }

        public ActionResult Index()
        {
            var model = new DashboardViewModel();
            //var empresasItens = new List<SelectListItem>
            //{
            //    new SelectListItem
            //    {
            //        Value="101",
            //        Text="American Cor"
            //    },
            //    new SelectListItem
            //    {
            //        Value="141",
            //        Text="Log Health"
            //    }
            //};
            try
            {
                //var empresas = Task.Run(() => _dashboardAppService.ListarTodasEmpresas()).Result;
                //model.Empresas = new SelectList(empresas.Items.Select(m => new { EmpresaId = m.Id, Descricao = m.Nome }), "EmpresaId", "Descricao");
                var empresas = new List<VWEmpresaDto>();
                model.Empresas = new SelectList(empresas.Select(m => new { EmpresaId = m.Id, Descricao = m.Nome }), "EmpresaId", "Descricao");
                return View(model);
            }
            catch
            {
                model.Empresas = new SelectList(new List<VWEmpresaDto>());
                return View(model);
            }
        }



        [AcceptVerbs("GET", "POST", "PUT")]
        public JsonResult ListarFaturamentoEntregue()
        {
            try
            {
                var objResult = AsyncHelper.RunSync(() => _dashboardAppService.ListarFaturamentoEntrega());

                return Json(objResult.Items.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ListarFaturamentoAberto()
        {
            try
            {
                var objResult = AsyncHelper.RunSync(() => _dashboardAppService.ListarFaturamentoAberto());

                return Json(objResult.Items.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ListarFaturamentoRecebimento()
        {
            try
            {
                var objResult = AsyncHelper.RunSync(() => _dashboardAppService.ListarFaturamentoRecebimento());

                return Json(objResult.Items.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}