using System.Collections.Generic;
using SW10.SWMANAGER.Authorization.Permissions.Dto;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}