using System.Collections.Generic;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Editions.Dto;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}