﻿using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Erros
{
    public class ErrosViewModel
    {
        public List<ErroDto> Erros { get; set; }
    }
}