using System.Collections.Generic;
using SW10.SWMANAGER.Authorization.Users.Dto;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Users
{
    public class UserLoginAttemptModalViewModel
    {
        public List<UserLoginAttemptDto> LoginAttempts { get; set; }
    }
}