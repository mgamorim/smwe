﻿using Abp.Application.Navigation;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Layout
{
    public class SidebarViewModel
    {
        public UserMenu Menu { get; set; }

        public string CurrentPageName { get; set; }
        public string CurrentPageFather { get; set; }
    }
}