﻿using System.Collections.Generic;
using SW10.SWMANAGER.Caching.Dto;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}