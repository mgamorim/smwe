﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Suprimentos.Estoques.Relatorios
{

    [AutoMap(typeof(RelatorioSolicitacaoSaidaModelDto))]
    public class RelatorioSolicitacaoModel : RelatorioSolicitacaoSaidaModelDto
    {
        public RelatorioSolicitacaoModel(RelatorioSolicitacaoSaidaModelDto dto)
        {
            dto.MapTo(this);
        }
    }
}