﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Home
{
    [AutoMap(typeof(AtendimentoDto))]
    public class AssistenciaisViewModel : AtendimentoDto
    {
        public SelectList Empresas { get; set; }

        public SelectList UnidadesOrganizacionais { get; set; }

        public string Filtro { get; set; }

        public virtual ICollection<AtendimentoDto> Atendimentos { get; set; }

        public bool IsEditMode { get { return Id > 0; } }

       

        public AssistenciaisViewModel(AtendimentoDto output)
        {
            output.MapTo(this);
        }
    }
}