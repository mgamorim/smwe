﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto;
using System.Collections.Generic;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Prescricoes
{
    [AutoMap(typeof(PrescricaoMedicaDto))]
    public class CriarOuEditarPrescricaoViewModel : PrescricaoMedicaDto
    {
        public bool IsEditMode { get { return Id > 0; } }

        public ICollection<DivisaoDto> Divisoes { get; set; }

        public ICollection<TipoRespostaDto> TiposRespostas { get; set; }

        public CriarOuEditarPrescricaoViewModel(PrescricaoMedicaDto output)
        {
            output.MapTo(this);
        }
    }
}