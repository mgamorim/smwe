﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using System.Collections.Generic;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.Prescricoes
{
    [AutoMap(typeof(PrescricaoItemRespostaDto))]
    public class PrescricaoItemRespostaViewModel : PrescricaoItemRespostaDto
    {

        public bool IsEditMode { get { return Id > 0; } }

        public PrescricaoItemRespostaViewModel(PrescricaoItemRespostaDto output)
        {
            output.MapTo(this);
        }

    }
}