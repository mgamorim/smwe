﻿using Abp.AutoMapper;
using SW10.SWMANAGER.Authorization.Users.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Assistenciais.SolicitacoesExames
{
    [AutoMap(typeof(SolicitacaoExameItemDto))]
    public class CriarOuEditarSolicitacaoExameItemViewModel : SolicitacaoExameItemDto
    {
        public UserEditDto UpdateUser { get; set; }

        public bool IsEditMode { get { return Id > 0; } }

        public CriarOuEditarSolicitacaoExameItemViewModel(SolicitacaoExameItemDto output)
        {
            output.MapTo(this);
        }
    }
}