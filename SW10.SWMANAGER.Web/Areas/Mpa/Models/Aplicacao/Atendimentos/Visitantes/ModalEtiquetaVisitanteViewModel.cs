﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos
{
    public class ModalEtiquetaVisitanteViewModel //: CriarOuEditarAtendimento
    {
        public string VisitanteId { get; set; }
    }
}