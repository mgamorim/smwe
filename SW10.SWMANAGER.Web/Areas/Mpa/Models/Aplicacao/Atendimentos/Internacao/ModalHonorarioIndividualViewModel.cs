﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Atendimentos.Internacao
{
    public class ModalHonorarioIndividualViewModel : CriarOuEditarAtendimento
    {
        public string AtendimentoId { get; set; }
    }
}