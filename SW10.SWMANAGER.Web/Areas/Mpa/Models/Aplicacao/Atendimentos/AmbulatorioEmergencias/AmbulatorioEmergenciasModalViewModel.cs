﻿using Abp.AutoMapper;
using SW10.SWMANAGER.Authorization.Users.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Atendimentos
{
    [AutoMapFrom(typeof(CriarOuEditarAtendimento))]
    public class AmbulatorioEmergenciasModalViewModel : CriarOuEditarAtendimento
    {
        public UserEditDto UpdateUser { get; set; }

        public Empresa UserEmpresa { get; set; }

        public SelectList Pacientes { get; set; }

        public SelectList Medicos { get; set; }

        public SelectList Empresas { get; set; }

        public SelectList Origens { get; set; }

        public SelectList Convenios { get; set; }

        public SelectList UnidadesOrganizacionais { get; set; }

        public List<CriarOuEditarAtendimento> Atendimentos { get; set; }

        public bool IsEditMode
        {
            get { return Id > 0; }
        }
        public AmbulatorioEmergenciasModalViewModel(CriarOuEditarAtendimento output)
        {
            output.MapTo(this);
        }

        public string Filtro { get; set; }
    }
}