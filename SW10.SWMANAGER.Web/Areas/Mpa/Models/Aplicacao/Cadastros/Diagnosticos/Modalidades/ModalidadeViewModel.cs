﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Diagnosticos.Modalidades
{
    public class ModalidadeViewModel
    {
        public bool IsParecer { get; set; }
        public string Filtro { get; set; }
    }
}