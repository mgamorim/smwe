﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao.Dto;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusoes
{
    [AutoMap(typeof(VelocidadeInfusaoDto))]
    public class CriarOuEditarVelocidadeInfusaoViewModel : VelocidadeInfusaoDto
    {
        public bool IsEditMode { get { return Id > 0; } }

        public CriarOuEditarVelocidadeInfusaoViewModel(VelocidadeInfusaoDto output)
        {
            output.MapTo(this);
        }
    }
}