﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Filas
{
    public class TermialSenhaViewModel
    {
        public List<FilaTerminalIndex> Filas { get; set; }
    }
}