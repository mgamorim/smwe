﻿using Abp.AutoMapper;
using SW10.SWMANAGER.Authorization.Users.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using System.Web.Mvc;

namespace SW10.SWMANAGER.Web.Areas.Mpa.Models.Aplicacao.Cadastros.Convenios
{
    [AutoMapFrom(typeof(CriarOuEditarConvenio))]
    public class CriarOuEditarConvenioModalViewModel : CriarOuEditarConvenio
    {
        public UserEditDto UpdateUser { get; set; }

        public bool IsEditMode
        {
            get { return Id > 0; }
        }

        public long Tabelas { get; set; }

        //public SelectList Estados { get; set; }

        //public SelectList Cidades { get; set; }

        //public SelectList Paises { get; set; }

        //public SelectList TiposTelefone { get; set; }

        public CriarOuEditarConvenioModalViewModel(CriarOuEditarConvenio  output)
        {
            output.MapTo(this);
        }
    }
}