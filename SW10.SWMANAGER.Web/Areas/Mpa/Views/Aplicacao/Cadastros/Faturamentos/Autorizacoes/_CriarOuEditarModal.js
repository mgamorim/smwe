﻿(function ($) {
    app.modals.CriarOuEditarFaturamentoAutorizacaoModal = function () {

        var _autorizacoesService = abp.services.app.faturamentoAutorizacao;
        var _modalManager;
        var _$autorizacaoInformationForm = null;

        this.init = function (modalManager) {
            _modalManager = modalManager;

            _$autorizacaoInformationForm = _modalManager.getModal().find('form[name=AutorizacaoInformationsForm]');
            _$autorizacaoInformationForm.validate({ ignore: "" });
            $('.modal-dialog:last').css('width', '900px');
        };

        this.save = function () {
            if (!_$autorizacaoInformationForm.valid()) {
                return;
            }

            var autorizacao = _$autorizacaoInformationForm.serializeFormToObject();

            autorizacao.Mensagem = $('#mensagem').val();

            _modalManager.setBusy(true);

            _autorizacoesService.criarOuEditar(autorizacao)
                 .done(function () {
                     abp.notify.info(app.localize('SavedSuccessfully'));
                     _modalManager.close();
                     abp.event.trigger('app.CriarOuEditarAutorizacaoModalSaved');
                 })
                .always(function () {
                    _modalManager.setBusy(false);
                });
        };
    };
})(jQuery);