﻿(function () {
    $(function () {
        var _$FornecedoresTable = $('#FornecedoresTable');
        var _FornecedoresService = abp.services.app.fornecedor;

        var _$filterForm = $('#FornecedoresFilterForm');

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Cadastros.CadastrosGlobais.Fornecedor.Create'),
            edit: abp.auth.hasPermission('Pages.Tenant.Cadastros.CadastrosGlobais.Fornecedor.Edit'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Cadastros.CadastrosGlobais.Fornecedor.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Fornecedores/CriarOuEditarModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_CriarOuEditarModal.js',
            modalClass: 'CriarOuEditarFornecedorModal'
        });

        var _userPermissionsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Users/PermissionsModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Cadastros/Fornecedores/_PermissionsModal.js',
            modalClass: 'UserPermissionsModal'
        });

        _$FornecedoresTable.jtable({

            title: app.localize('Fornecedores'),
            paging: true,
            sorting: true,
            multiSorting: true,

            actions: {
                listAction: {
                    method: _FornecedoresService.listarFornecedores
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '8%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissions.edit) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    _createOrEditModal.open({ id: data.record.id });
                                });
                        }

                        if (_permissions.delete) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    deleteFornecedores(data.record);
                                });
                        }

                        return $span;
                    }
                },

                FisicaJuridica: {
                    title: app.localize('FisicaJuridica'),
                    width: '15%',
                    display: function (data) {
                        if (data.record.sisPessoa) {
                            return data.record.sisPessoa.fisicaJuridica;
                        }
                    }
                },

                nomeFornecedor: {
                    title: app.localize('Nome'),
                    width: '15%',
                    display: function (data) {


                        if (data.record.sisPessoa) {

                            if (data.record.sisPessoa.fisicaJuridica == "F") {
                                return data.record.sisPessoa.nomeCompleto;
                            }
                            else if (data.record.sisPessoa.fisicaJuridica == "J") {
                                return data.record.sisPessoa.nomeFantasia;
                            }
                        }
                    }
                } ,
                CPFCNPJ: {
                    title: app.localize('CPFCNPJ'),
                    width: '15%',
                    display: function (data) {


                        if (data.record.sisPessoa) {

                            if (data.record.sisPessoa.fisicaJuridica == "F") {
                                return data.record.sisPessoa.cpf;
                            }
                            else if (data.record.sisPessoa.fisicaJuridica == "J") {
                                return data.record.sisPessoa.cnpj;
                            }
                        }
                    }
                }
            }
        });

        function getFornecedores(reload) {
            if (reload) {
                _$FornecedoresTable.jtable('reload');
            } else {
                _$FornecedoresTable.jtable('load', {
                    filtro: $('#FornecedoresTableFilter').val()
                });
            }
        }

        function deleteFornecedores(Fornecedor) {

            abp.message.confirm(
                app.localize('DeleteWarning', Fornecedor.primeiroNome),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _FornecedoresService.excluir(Fornecedor)
                            .done(function () {
                                getFornecedores(true);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms);
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewFornecedorButton').click(function () {
            _createOrEditModal.open();
        });

        $('#ExportarFornecedoresParaExcelButton').click(function () {
            _FornecedoresService
                .listarParaExcel({
                    filtro: $('#FornecedoresTableFilter').val(),
                    //sorting: $(''),
                    maxResultCount: $('span.jtable-page-size-change select').val()
                })
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#GetFornecedoresButton, #RefreshFornecedoresListButton').click(function (e) {
            e.preventDefault();
            getFornecedores();
        });

        abp.event.on('app.CriarOuEditarFornecedorModalSaved', function () {
            getFornecedores(true);
        });

        getFornecedores();

        $('#FornecedoresTableFilter').focus();
    });
})();