﻿(function () {
    $(function () {

        /*  Permissões ↓
        ----------------------------------------------------------------------------------------------------------------- */
        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Suprimentos.CompraAprovacao.Create'),
            edit: abp.auth.hasPermission('Pages.Tenant.Suprimentos.CompraAprovacao.Edit'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Suprimentos.CompraAprovacao.Delete')
        };


        /*  Servicos ↓
        ----------------------------------------------------------------------------------------------------------------- */
        var _$requisicoesComprasTable = $('#requisicoesComprasTable');
        var _requisicoesCompraService = abp.services.app.compraRequisicao;
        var _$requisicoesCompraFilterForm = $('#contasPagarFilterForm');


        /*  Vars Globais ↓
        ----------------------------------------------------------------------------------------------------------------- */


        /*  Sets iniciais ↓
        ----------------------------------------------------------------------------------------------------------------- */
        $('#AdvacedContasMedicasFiltersArea').swPiqueEsconde('#ShowAdvancedFiltersSpan', '#HideAdvancedFiltersSpan');

        // Date range filtro
        var _selectedDateRangeLocal = {
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month')
        };

        $('.date-range-picker').daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRangeLocal),
            function (start, end, label) {
                _selectedDateRangeLocal.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRangeLocal.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
            });
        // Fim - date range filtro

        $('#createNew').click(function (e) {
            e.preventDefault();
            location.href = 'ComprasRequisicao/CriarOuEditarModal/';
        });

        _$requisicoesComprasTable.jtable({

            title: app.localize('RequisicoesCompras'),
            paging: true,
            sorting: true,
            multiSorting: true,
            //selecting: true,
            //selectingCheckboxes: true,
            //multiselect: true,

            rowUpdated: function (event, data) {
                //if (data) {
                //    if (data.record.corLancamentoLetra) {
                //        data.row[0].cells[2].setAttribute('color', data.record.corLancamentoLetra);
                //    }

                //    if (data.record.corLancamentoFundo) {
                //        data.row[0].cells[2].setAttribute('bgcolor', data.record.corLancamentoFundo);
                //    }
                //}


            },
            rowInserted: function (event, data) {
                //if (data) {
                //    if (data.record.corLancamentoLetra) {
                //        data.row[0].cells[2].setAttribute('color', data.record.corLancamentoLetra);
                //    }

                //    if (data.record.corLancamentoFundo) {
                //        data.row[0].cells[2].setAttribute('bgcolor', data.record.corLancamentoFundo);
                //    }
                //}

            },

            actions: {
                listAction: {
                    method: _requisicoesCompraService.listar
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '5%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if ((_permissions.edit) && (data.record.isEncerrada != true)) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('AprovacaoCompra') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    // _createOrEditModal.open({ id: data.record.id });

                                    location.href = 'ComprasAprovacao/CriarOuEditarModal/' + data.record.id

                                });
                        }

                        //if (_permissions.delete) {
                        //    $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                        //        .appendTo($span)
                        //        .click(function () {
                        //            deleteRegistro(data.record);
                        //        });
                        //}

                        return $span;
                    }
                },

                Codigo: {
                    title: app.localize('Codigo'),
                    width: '10%',
                    display: function (data) {
                        if (data) {
                            return data.record.codigo;
                        }
                    }
                },

                Modo: {
                    title: app.localize('Modo'),
                    width: '10%',
                    display: function (data) {
                        if (data) {
                            return data.record.modo;
                        }
                    }
                },

                Empresa: {
                    title: app.localize('Empresa'),
                    width: '20%',
                    display: function (data) {
                        if (data) {
                            return data.record.empresa;
                        }
                    }
                },

                Estoque: {
                    title: app.localize('Estoque'),
                    width: '20%',
                    display: function (data) {
                        if (data) {
                            return data.record.estoque;
                        }
                    }
                },

                MotivoPedido: {
                    title: app.localize('MotivoPedido'),
                    width: '15%',
                    display: function (data) {
                        if (data) {
                            return data.record.motivoPedido;
                        }
                    }
                },

                IsUrgente: {
                    title: app.localize('IsUrgente'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.isUrgente) {
                            return '<div style="text-align:center;vertical-align:middle"> <span class="label bg-red-pink" style="text-align:center;">' + app.localize('Yes') + '</span> </div>'
                        } else {
                            return '<div style="text-align:center;vertical-align:middle"> <span class="label bg-default" style="text-align:center; color:dimgrey">' + app.localize('No') + '</span> </div>'
                        }
                    }
                },

                dataEmissao: {
                    title: app.localize('DataEmissao'),
                    width: '10%',
                    display: function (data) {
                        return moment(data.record.dataEmissao).format('L');
                    }
                }

                //CreationTime: {
                //    title: app.localize('CreationTime'),
                //    width: '10%',
                //    display: function (data) {
                //        return moment(data.record.creationTime).format('L');
                //    }
                //}

                //TipoDocumento: {
                //    title: app.localize('TipoDocumento'),
                //    width: '7%',
                //    display: function (data) {
                //        if (data) {
                //            return data.record.tipoDocumento;
                //        }
                //    }
                //},
            }
        });

        var filtroJTableLocal = {
            //filtro: $('#ContasMedicasTableFilter').val(),
            empresaId: $('#comboEmpresa option:selected').val(),
            estoqueId: $('#comboEstoque option:selected').val(),
            motivoPedidoId: $('#comboMotivoPedido option:selected').val(),
            isUrgente: $('#checkUrgente').val(),
            codigo: $('#codigo').val(),
            statusRequisicao: $('#comboStatusRequisicao').val(),
            statusAprovacao: $('#comboStatusAprovacao').val(),

            StartDate: _selectedDateRangeLocal.startDate,
            EndDate: _selectedDateRangeLocal.endDate,

            get() {
                //this.Filtro = $('#ContasMedicasTableFilter').val();
                this.empresaId = $('#comboEmpresa option:selected').val();
                this.estoqueId = $('#comboEstoque option:selected').val();
                this.motivoPedidoId = $('#comboMotivoPedido option:selected').val();
                this.isUrgente = $('#checkUrgente').val(),
                this.codigo = $('#codigo').val();
                this.statusRequisicao = $('#comboStatusRequisicao').val();
                this.statusAprovacao = $('#comboStatusAprovacao').val();
                this.StartDate = _selectedDateRangeLocal.startDate;
                this.EndDate = _selectedDateRangeLocal.endDate;
                return this;
            }
        };

        function getRegistros(reload) {
           
            if (reload) {
                _$requisicoesComprasTable.jtable('reload');
            } else {
                _$requisicoesComprasTable.jtable('load', filtroJTableLocal.get());
            }
        }

        function deleteRegistro(record) {

            abp.message.confirm(
                app.localize('DeleteWarning', record.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _$requisicoesComprasTable.excluir(record)
                            .done(function () {
                                getRegistros(true);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms);
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedLancamentosFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedLancamentosFiltersArea').slideUp();
        });

        $('#checkUrgente').change(function () {
            //            $(this).val($(this).is(':checked'));

            if ($(this).is(':checked')) {
                $(this).val(true);
            } else {
                $(this).val("");
            };

        });

        $('#refreshButton').click(function (e) {
            try {
                e.preventDefault();

                $(this).buttonBusy(true);

                getRegistros();
            }
            finally {

                $(this).buttonBusy(false);
            }
        });

        abp.event.on('app.CriarOuEditarCompraRequisicaoSaved', function () {
            getRegistros(true);
        });

        $('#tableFilter').focus();

        getRegistros();

    });
})();