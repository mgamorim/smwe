﻿(function ($) {
    app.modals.CriarOuEditarPreMovimentoItemModal = function () {

        var _preMovimentoItemService = abp.services.app.estoquePreMovimentoItem;
        var _produtoService = abp.services.app.produto;
        var _estoqueLoteValidadeService = abp.services.app.estoqueLoteValidade;



        var _modalManager;
        var _$PreMovimentoItemInformationForm = null;

        $(document).ready(function () {
            $('#quantidadeLote').mask('000.000.000,00', { reverse: true });
            $('#quantidadeAtendida').mask('000.000.000,00', { reverse: true });

           
        });


        this.init = function (modalManager) {
            _modalManager = modalManager;

            _$PreMovimentoItemInformationForm = _modalManager.getModal().find('form[name=PreMovimentoItemInformationsForm]');
            _$PreMovimentoItemInformationForm.validate();

            $('ul.ui-autocomplete').css('z-index', '2147483647 !important');
            $('.modal-dialog').css('width', '900px');

           

            var timerID = setInterval(function () { $('#LaboratorioId').focus(); clearInterval(timerID); }, 1000);
           
            //timerID = null;

            
            
        };


        this.save = function () {

            
            if (!_$PreMovimentoItemInformationForm.valid()) {
                return;
            }

            var preMovimentoItem = _$PreMovimentoItemInformationForm.serializeFormToObject();

            preMovimentoItem.Quantidade = retirarMascara(preMovimentoItem.Quantidade);

            if ($('#itens').val() != '') {
                lista = JSON.parse($('#itens').val());
            }

            if ($('#idGrid').val() != '') {

                for (var i = 0; i < lista.length; i++) {
                    if (lista[i].IdGrid == $('#idGrid').val()) {
                        lista[i].QuantidadeAtendida = preMovimentoItem.QuantidadeAtendida;
                        lista[i].LotesValidadesJson = preMovimentoItem.LotesValidadesJson;
                        lista[i].NumerosSerieJson = preMovimentoItem.NumerosSerieJson;
                    }

                }
            }
            else {
                preMovimentoItem.IdGrid = lista.length == 0 ? 1 : lista[lista.length - 1].IdGrid + 1;
                lista.push(preMovimentoItem);
            }

            $('#itens').val(JSON.stringify(lista));
            abp.event.trigger('app.CriarOuEditarPreMovimentoItemModalSaved');
            _modalManager.close();
        
        };


        function retirarMascara(valor) {

            while (valor.indexOf('_') != -1) valor = valor.replace('_', '');
            while (valor.indexOf('.') != -1) valor = valor.replace('.', '');

            valor = valor.replace(',', '.');

            return valor;
        }

        $('input[name="Validade"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": "MM/DD/YYYY",//moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
           function (selDate) {
               $('input[name="Validade"]').val(selDate.format('L')).addClass('form-control edited');
           });

        var _$loteValidadeTable2 = $('#loteValidadeTable');
        var _$numeroSerieTable = $('#numeroSerieTable');

        

        function retornarListaLoteValidade(filtro) {
            
                var js = $('#lotesValidadesJson').val();
                var res = _preMovimentoItemService.listarLoteValidadeJson(js);  
                return res;
        }


        _$loteValidadeTable2.jtable({

            title: app.localize('LoteValidade'),
            paging: true,
            sorting: true,
            edit: false,
            create: false,
            multiSorting: true,


            actions: {
                listAction: {
                    method: retornarListaLoteValidade
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },


                actions: {

                    title: app.localize('Actions'),
                    width: '8%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if ($('#preMovimentoEstadoId').val() != 2) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    editLoteValidade(data);
                                   // _createOrEditLoteValidadeModal.open({ preMovimentoItemId: data.record.estoquePreMovimentoItemId, produtoLoteValidadeId: data.record.id });
                                });

                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();

                                    excluirLoteValidade(data);
                                });
                        }
                        return $span;
                    }
                },

                Laboratorio: {
                    title: app.localize('Laboratorio'),
                    width: '40%',
                    display: function (data) {
                        if (data.record) {
                            return data.record.laboratorio;
                        }
                    }
                },
                Lote: {
                    title: app.localize('Lote'),
                    width: '10%',
                    display: function (data) {
                        if (data.record) {
                            return data.record.lote;
                        }
                    }
                },


                Validade: {
                    title: app.localize('Validade2'),
                    width: '20%',
                    display: function (data) {
                        if (data.record.validade) {
                            return moment(data.record.validade).format("L");
                        }
                    }
                },

                Quantidade: {
                    title: app.localize('Quantidade'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.quantidade) {
                            return posicionarDireita(data.record.quantidade.toFixed(2));
                        }
                    }
                },
            }
        });



        function getLoteValidadeTable(reload) {
            
            if (reload) {
                _$loteValidadeTable2.jtable('reload');
            } else {

                _$loteValidadeTable2.jtable('load', { filtro: $('#id').val() });//, entradaConfirmada: $('#entradaConfirmadaId').val() });
            }
        }

        getLoteValidadeTable();





        function retornarNumeroSerie(filtro) {
           
            var js = $('#numerosSerieJson').val();
            var res = _preMovimentoItemService.listarNumeroSerieJson(js);
            return res;
        }

        _$numeroSerieTable.jtable({

            title: app.localize('NumeroSerie'),
            paging: true,
            sorting: true,
            edit: false,
            create: false,
            multiSorting: true,


            actions: {
                listAction: {
                    method: retornarNumeroSerie
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },


                actions: {

                    title: app.localize('Actions'),
                    width: '8%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                            .appendTo($span)
                            .click(function (e) {
                                e.preventDefault();

                                excluirNumeroSerie(data);
                            });
                        return $span;
                    }
                },

                NumeroSerie: {
                    title: app.localize('NumeroSerie'),
                    width: '40%',
                    display: function (data) {
                        if (data.record) {
                            return data.record.numeroSerie;
                        }
                    }
                }
            }
        });

        function getNumeroSerieTable(reload) {

            if (reload) {
                _$numeroSerieTable.jtable('reload');
            } else {

                _$numeroSerieTable.jtable('load', { filtro: $('#id').val() });//, entradaConfirmada: $('#entradaConfirmadaId').val() });
            }
        }

        getNumeroSerieTable();

        var lista = [];
   
        $('#inserir').on('click', function () {

           

            var _$LoteValidadeForm = $('form[name=LoteValidadeForm');

            //if (!_$LoteValidadeForm.valid()) {
            //    return;
            //}
            var preMovimentoItem = _$LoteValidadeForm.serializeFormToObject();

            if ($('#lotesValidadesJson').val() != '') {
                lista = JSON.parse($('#lotesValidadesJson').val());
            }

            if ($('#idGridLoteValidade').val() != '') {

                for (var i = 0; i < lista.length; i++) {
                    if (lista[i].IdGridLoteValidade == $('#idGridLoteValidade').val())
                    {
                        lista[i].LaboratorioId = $('#LaboratorioId').val();
                        lista[i].Lote = $('#LoteId').val();
                        lista[i].Validade = $('#validadeId').val();
                        lista[i].Quantidade = retirarMascara($('#quantidadeLote').val());
                        lista[i].LoteValidadeId = $('#loteValidadeId').val();
                        break;
                    }

                }
            }
            else {
                preMovimentoItem.Quantidade = retirarMascara(preMovimentoItem.Quantidade);
                preMovimentoItem.IdGridLoteValidade = lista.length == 0 ? 1 : lista[lista.length - 1].IdGridLoteValidade + 1;
                lista.push(preMovimentoItem);
            }

            $('#lotesValidadesJson').val(JSON.stringify(lista));

            $('#idGridLoteValidade').val('');
            $('#LaboratorioId').val('').selectpicker('refresh');
            $('#LoteId').val('');
            $('#validadeId').val('');
            $('#quantidadeLote').val('');

            $('#loteValidadeId').val(null).trigger("change");

            getLoteValidadeTable();
            atulizarQuantidadeAtendida();

            $('#LaboratorioId').focus();

        });

        $('#inserirNuneroSerie').on('click', function () {

            var _$NumeroSerieForm = $('form[name=NumeroSerieForm');

            //if (!_$LoteValidadeForm.valid()) {
            //    return;
            //}
            var preMovimentoItem = _$NumeroSerieForm.serializeFormToObject();

            if ($('#numerosSerieJson').val() != '') {
                lista = JSON.parse($('#numerosSerieJson').val());
            }

            preMovimentoItem.IdGridNumeroSerie = lista.length == 0 ? 1 : lista[lista.length - 1].IdGridNumeroSerie + 1;
            lista.push(preMovimentoItem);

            $('#numeroSerie').val('');

            $('#numerosSerieJson').val(JSON.stringify(lista));

            getNumeroSerieTable();
            atualizarQuantidadeAtendidaNumeroSerie();
            $('#numeroSerie').focus();
        });

        function editLoteValidade(data)
        {

            
            $('#idGridLoteValidade').val(data.record.idGridLoteValidade);
            $('#LaboratorioId').val(data.record.laboratorioId);
            $('#LoteId').val(data.record.lote);
            $('#validadeId').val(data.record.validade);
            $('#quantidadeLote').val(data.record.quantidade);
        }
        
        function excluirLoteValidade(data)
        {
            abp.message.confirm(
             app.localize('DeleteWarning', data.record.lote),
             function (isConfirmed) {
                 if (isConfirmed) {
                     lista = JSON.parse($('#lotesValidadesJson').val());

                     for (var i = 0; i < lista.length; i++) {
                         if (lista[i].IdGridLoteValidade == data.record.idGridLoteValidade) {
                             lista.splice(i, 1);
                             $('#lotesValidadesJson').val(JSON.stringify(lista));
                             break;
                         }
                     }
                     getLoteValidadeTable();
                     atulizarQuantidadeAtendida();
                 }
             }
         );
        }


        function excluirNumeroSerie(data) {
            abp.message.confirm(
             app.localize('DeleteWarning', data.record.numeroSerie),
             function (isConfirmed) {
                 if (isConfirmed) {
                     lista = JSON.parse($('#numerosSerieJson').val());

                     for (var i = 0; i < lista.length; i++) {
                         if (lista[i].IdGridNumeroSerie == data.record.idGridNumeroSerie) {
                             lista.splice(i, 1);
                             $('#numerosSerieJson').val(JSON.stringify(lista));
                             break;
                         }
                     }
                     getNumeroSerieTable();
                      atualizarQuantidadeAtendidaNumeroSerie();
                 }
             }
         );
        }

        function atulizarQuantidadeAtendida()
        {
            lista = JSON.parse($('#lotesValidadesJson').val());
            var quantidade = parseFloat(0);
            for (var i = 0; i < lista.length; i++) {
                quantidade += parseFloat( lista[i].Quantidade);
            }

            $('#quantidadeAtendida').val(quantidade);
        }

        function atualizarQuantidadeAtendidaNumeroSerie()
        {
            lista = JSON.parse($('#numerosSerieJson').val());
            $('#quantidadeAtendida').val(lista.length);
        }

        selectSW('.selectProduto', "/api/services/app/produto/ListarProdutoDropdown");

        selectSWMultiplosFiltros('.loteValidade', '/api/services/app/EstoqueLoteValidade/ListarProdutoDropdownPorLaboratorio', ['produtoId', 'EstoqueId', 'LaboratorioId']);


        $('#LaboratorioId').on('change', function () {
            selectSWMultiplosFiltros('.loteValidade', '/api/services/app/EstoqueLoteValidade/ListarProdutoDropdownPorLaboratorio', ['produtoId', 'EstoqueId', 'LaboratorioId']);

        });

        $('#loteValidadeId').on('select2:select', function () {
            $('#quantidadeLote').focus();
        });

    };
})(jQuery);