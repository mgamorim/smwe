﻿
(function ($) {
    $(function () {

        $(document).ready(function () {

            CamposRequeridos();
        });

        var _modalManager;

        this.init = function (modalManager) {
            _modalManager = modalManager;

            atendimentoChange();
        };



        $('.modal-dialog').css('width', '1800px');

        $.validator.setDefaults({ ignore: ":hidden:not(select)" });



        $('#empresa-search').autocomplete({
            minLength: 2,
            delay: 0,
            source: function (request, response) {
                var term = $('#empresa-search').val();
                var url = '/mpa/empresas/autocompleteDescricao';


                var fullUrl = url + '/?term=' + term;
                $.getJSON(fullUrl, function (data) {
                    if (data.result.length == 0) {
                        $('#empresa-Id').val(0);
                        $("#empresa-search").focus();
                        abp.notify.info(app.localize("ListaVazia"));
                        return false;
                    };
                    response($.map(data.result, function (item) {
                        $('#empresa-Id').val(0);
                        return {
                            label: item.nome,
                            value: item.nome,
                            realValue: item.id
                        };
                    }));
                });
            },
            select: function (event, ui) {
                $('#empresa-Id').val(ui.item.realValue);
                $('#empresa-search').val(ui.item.value);
                //$('.save-button').removeAttr('disabled');
                return false;
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    //$('.save-button').attr('disabled', 'disabled');
                    $('#empresa-Id').val(0);
                    $("#empresa-search").val('').focus();
                    abp.notify.info(app.localize("EstadoInvalido"));
                    return false;
                }
            },
        });

        $('#Movimento').on('load', function () {
            var d = new Date();
            var n = d.getDate();
            $('#movimento').val(moment().format("L LT"));
        });


        var _preMovimentoService = abp.services.app.estoquePreMovimento;
        var _estoquePreMovimentoService = abp.services.app.estoquePreMovimento;
        var _estoquePreMovimentoItemService = abp.services.app.estoquePreMovimentoItem;

        var _movimentoService = abp.services.app.estoqueMovimento;

        var _$EstoquePreMovimentoItemTable = $('#EstoquePreMovimentoItemTable');


        var _createOrEditPreMovimentoItemModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/ConfirmacaoSolicitacoes/CriarOuEditarPreMovimentoItemModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/ConfirmacaoSolicitacoes/_CriarOuEditarPreMovimentoItemModal.js',
            modalClass: 'CriarOuEditarPreMovimentoItemModal'
        });

        var _ErrorModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Erros/ExibirErros',
        });



        $('#btn-novo-PreMovimentoItem').click(function (e) {
            e.preventDefault()
            _createOrEditPreMovimentoItemModal.open({ id: 0 });

           


        });

        $('#salvar-PreMovimento').click(function (e) {
            e.preventDefault()

            

            var _$preMovimentoInformationsForm = $('form[name=preMovimentoInformationsForm');

            _$preMovimentoInformationsForm.validate();

            if (!_$preMovimentoInformationsForm.valid()) {
                return;
            }

            var preMovimento = _$preMovimentoInformationsForm.serializeFormToObject();
            preMovimento.IsEntrada = true;
            var editMode = $('#is-edit-mode').val();

            _preMovimentoService.atenderSolicitacao(preMovimento)
                  .done(function (data) {

                      if (data.errors.length > 0) {
                          _ErrorModal.open({ erros: data.errors });
                      }
                      else {
                          abp.notify.info(app.localize('SavedSuccessfully'));
                          $('#id').val(data.returnObject.id);
                          location.href = '/mpa/ConfirmacaoSolicitacoes';
                      }

                  })
                 .always(function () {
                 });
        });

        $('input[name="Movimento"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            maxDate: new Date(),
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
           function (selDate) {
               $('input[name="Movimento"]').val(selDate.format('L')).addClass('form-control edited');
               // obterIdade(selDate);
           });

        $('input[name="Emissao"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            maxDate: new Date(),
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
         function (selDate) {
             $('input[name="Emissao"]').val(selDate.format('L')).addClass('form-control edited');
         });

        abp.event.on('app.CriarOuEditarPreMovimentoItemModalSaved', function () {
            getEstoquePreMovimentoItemTable();
        });
       

        $('.close-button').on('click', function () {
            location.href = '/mpa/ConfirmacaoSolicitacoes';
        });


        function retornarLista(filtro)
        {
            if ($('#itens').val() != '')
            {
                
                var js = $('#itens').val();
                var res = _preMovimentoService.listarItensJson(js);  //  '"{Result":"OK","Records":' + js + '}'
                return res;
            }
            else
            {
                var res = _preMovimentoService.listarItens({ filtro: $('#id').val() });
                return res;
            }
        }

        _$EstoquePreMovimentoItemTable.jtable
        ({
            title: app.localize('Item'),
            paging: true,
            sorting: true,
            edit: false,
            create: false,
            multiSorting: true,

            actions:
            {
                listAction:
                {
                    method: retornarLista
                },
            },
            fields:
            {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '11%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');

                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    _createOrEditPreMovimentoItemModal.open({ item: JSON.stringify(data.record)});
                                });


                        return $span;
                    }
                },
                //IdGrid: {
                //    display: function (data) {
                       
                //        return data.record.idGrid;
                //    }
                //},

                ProdutoId: {
                    title: app.localize('Produto'),
                    width: '30%',
                    display: function (data) {
                        if (data.record.produto) {
                             
                            return data.record.produto;
                        }
                    }
                },
             

                quantidade: {
                    title: app.localize('QuantidadePendente'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.quantidade) {
                            return posicionarDireita(data.record.quantidade);
                        }
                    }
                },
                quantidadeAtendida: {
                    title: app.localize('QuantidadeAtendida'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.quantidadeAtendida) {
                            return posicionarDireita(data.record.quantidadeAtendida);
                        }
                    }
                },
                Unidade: {
                    title: app.localize('Unidade'),
                    width: '20%',
                    display: function (data) {
                        if (data.record.produtoUnidade) {
                            return data.record.produtoUnidade;
                        }
                    }
                },
            }
        });



        function deletePreMovimentoItem(preMovimentoItem) {

            abp.message.confirm(
                app.localize('DeleteWarning', preMovimentoItem.produto.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {

                        
                        
                        lista = JSON.parse($('#itens').val());

                        for (var i = 0; i < lista.length; i++) {
                            if (lista[i].IdGrid == preMovimentoItem.idGrid) {
                                lista.splice(i,1);
                                $('#itens').val(JSON.stringify(lista));

                                break;
                            }
                        }

                        getEstoquePreMovimentoItemTable();
                    }
                }
            );
        }


        function getEstoquePreMovimentoItemTable(reload) {

            if (reload) {
                _$EstoquePreMovimentoItemTable.jtable('reload');
            } else {
               

                _$EstoquePreMovimentoItemTable.jtable('load', { filtro: $('#id').val() });//, entradaConfirmada: $('#entradaConfirmadaId').val() });
            }
        }

        getEstoquePreMovimentoItemTable();



        $('#EstTipoMovimentoId').change(function () {
            configurarCampos();
        });

        function configurarCampos() {

            

            var valor = $('#EstTipoMovimentoId').val();

            if (valor == '3') {
                $('#grupoOrganizacional').hide();
                $('#grupoOrganizacional').val('');
                $('#paciente').show();
                $('#medico').show();
                $('#atendimento').show();
            }
            else {
                $('#grupoOrganizacional').show();
                $('#paciente').hide();
                $('#medico').hide();
                $('#atendimento').hide();

                $('#paciente').val('');
                $('#medico').val('');
                $('#atendimento').val('');


            }



            if (valor == 4) {
                $('#motivoPerdaId').show();
            }
            else {
                $('#motivoPerdaId').hide();
                $('#motivoPerdaId').val('');
            }

        }

        $('#atendimentoId').change(function () {
            atendimentoChange();
        });


        function atendimentoChange()
        {
            
            var valor = $('#atendimentoId').val();

            if (valor == '' || valor == '0') {
                // $("#MedicoSolcitanteId").attr("disabled", false).change();
                $("#divMedico").removeClass('hidden');
                $("#medicoSolcitanteId").addClass('hidden');
                $("#pacienteInputId").addClass('hidden');
                $("#divPaciente").removeClass('hidden');

            }
            else {
                $.ajax({
                    url: "/mpa/Saidas/SelecionarAtendimento/" + valor,
                    success: function (data) {

                        $("#pacienteInputId").removeClass('hidden');
                        $("#divPaciente").addClass('hidden');
                        //$("#MedicoSolcitanteId").val(data.MedicoId).change()
                        //                   .selectpicker('refresh');

                        //  $("#MedicoSolcitanteId").attr("disabled", true).change();

                        $("#medicoSolcitanteId").removeClass('hidden');
                        $("#divMedico").addClass('hidden');

                        $("#pacienteInputId").val(data.Paciente.CodigoPaciente + ' - ' + data.Paciente.NomeCompleto);

                        $("#medicoSolcitanteId").val(' - ' + data.Medico.NomeCompleto);
                    }
                });
            }
        }


        $('#produtoId').on('select2:select', function () {
           


            if ($('#itens').val() != '') {
                lista = JSON.parse($('#itens').val());
            }

            for (var i = 0; i < lista.length; i++) {
                if (lista[i].ProdutoId == $('#produtoId').val()) {

                    _createOrEditPreMovimentoItemModal.open({ item: JSON.stringify(lista[i]) });
                }
            }
        });

        selectSW('.selectForncedor', "/api/services/app/fornecedor/ListarDropdown");
        selectSW('.selectAtendimento', "/api/services/app/Atendimento/ListarDropdown");
        selectSW('.selectPaciente', "/api/services/app/Paciente/ListarDropdown");
        selectSW('.selectMedico', "/api/services/app/Medico/ListarDropdown");
        selectSW('.selectProduto', "/api/services/app/produto/ListarProdutoDropdown");

        abp.event.on('app.CriarOuEditarPreMovimentoItemModalSaved', function () {
            getEstoquePreMovimentoItemTable();
        });


        var _imprimirEntrada = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/ConfirmacaoSolicitacoes/VisualizarIndex'

        });



        $('#btnImprimir').on('click', function (e) {
            _imprimirEntrada.open({ solicitacaoId: $('#id').val() });
        });

        
    });

})(jQuery);