﻿
(function ($) {
    $(function () {

        $(document).ready(function () {

            $('#totalDocumento').mask('000.000.000,00', { reverse: true });
            $('#ICMSPer').mask('000.000.000,00', { reverse: true });
            $('#valorICMS').mask('000.000.000,00', { reverse: true });
            $('#DescontoPer').mask('000.000.000,00', { reverse: true });
            $('#ValorDesconto').mask('000.000.000,00', { reverse: true });
            $('#ValorAcrescimo').mask('000.000.000,00', { reverse: true });
            $('#frete').mask('000.000.000,00', { reverse: true });
            $('#FretePer').mask('000.000.000,00', { reverse: true });
            $('#ValorFrete').mask('000.000.000,00', { reverse: true });

            $('#totalProdutoId').mask('000.000.000,00', { reverse: true });
            $('#ValorDesconto').mask('000.000.000,00', { reverse: true });




            $('#valor').mask('000.000.000,00', { reverse: true });
            $('#juros').mask('000.000.000,00', { reverse: true });
            $('#multa').mask('000.000.000,00', { reverse: true });
            $('#acrescimoDecrescimo').mask('000.000.000,00', { reverse: true });
            $('#total').mask('000.000.000,00', { reverse: true });


            CamposRequeridos();
        });

        $('.modal-dialog').css('width', '1800px');

        $('.chosen-select').chosen({ no_results_text: app.localize("NotFound"), width: '100%' });
        $.validator.setDefaults({ ignore: ":hidden:not(select)" });

        // validation of chosen on change
        $('ul.ui-autocomplete').css('z-index', '2147483647 !important');


        //$('#totalDocumento').change(function () {
        //    CalcularValorICMS();
        //});

        //$('#ICMSPer').change(function () {
        //    CalcularValorICMS();
        //});

        function CalcularValorICMS() {
            var valorIcms = parseFloat($('#totalDocumento').val()) * parseFloat($('#ICMSPer').val()) / 100;
            $('#valorICMS').val(valorIcms);

        }


        //$('#totalProdutoId, #freteId, #ValorDesconto, #ValorAcrescimo').change(function () {
        //    CalcularTotalProduto();
        //});


        function CalcularTotalProduto() {
            var totalProduto = ($('#totalProdutoId').val() != '') ? parseFloat($('#totalProdutoId').val()) : 0;

            var valorFrete = ($('#ValorFrete').val() != '') ? parseFloat($('#ValorFrete').val()) : 0;
            var valorDesconto = ($('#ValorDesconto').val() != '') ? parseFloat($('#ValorDesconto').val()) : 0;
            var valorAcrescimo = ($('#ValorAcrescimo').val() != '') ? parseFloat($('#ValorAcrescimo').val()) : 0;

            var valorTotalProduto = totalProduto + valorFrete - valorDesconto + valorAcrescimo;
            $('#totalDocumento').val(valorTotalProduto);
            CalcularValorICMS();
        }

        function CalcularValorFrete() {
            var valorDesconto = 0;
            if ($('#FretePer').val() != '') {
                var valorDesconto = parseFloat($('#freteId').val()) * parseFloat($('#FretePer').val()) / 100;
            }
            var valorFrete = parseFloat($('#freteId').val()) - valorDesconto;
            $('#ValorFrete').val(valorFrete);
        }

        function CalcularValorDesconto() {
            var valorDesconto = parseFloat($('#totalDocumento').val()) * parseFloat($('#DescontoPer').val()) / 100;
            $('#ValorDesconto').val(valorDesconto);
        }

        $('#empresa-search').autocomplete({
            minLength: 2,
            delay: 0,
            source: function (request, response) {
                var term = $('#empresa-search').val();
                var url = '/mpa/empresas/autocompleteDescricao';


                var fullUrl = url + '/?term=' + term;
                $.getJSON(fullUrl, function (data) {
                    if (data.result.length == 0) {
                        $('#empresa-Id').val(0);
                        $("#empresa-search").focus();
                        abp.notify.info(app.localize("ListaVazia"));
                        return false;
                    };
                    response($.map(data.result, function (item) {
                        $('#empresa-Id').val(0);
                        return {
                            label: item.nome,
                            value: item.nome,
                            realValue: item.id
                        };
                    }));
                });
            },
            select: function (event, ui) {
                $('#empresa-Id').val(ui.item.realValue);
                $('#empresa-search').val(ui.item.value);
                //$('.save-button').removeAttr('disabled');
                return false;
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    //$('.save-button').attr('disabled', 'disabled');
                    $('#empresa-Id').val(0);
                    $("#empresa-search").val('').focus();
                    abp.notify.info(app.localize("EstadoInvalido"));
                    return false;
                }
            },
        });

        $('#fornecedor-search').autocomplete({
            minLength: 2,
            delay: 0,
            source: function (request, response) {
                var term = $('#fornecedor-search').val();
                var url = '/mpa/fornecedores/autocomplete';


                var fullUrl = url + '/?term=' + term;
                $.getJSON(fullUrl, function (data) {
                    if (data.result.length == 0) {
                        $('#fornecedor-Id').val(0);
                        $("#fornecedor-search").focus();
                        abp.notify.info(app.localize("ListaVazia"));
                        return false;
                    };
                    response($.map(data.result, function (item) {
                        $('#fornecedor-Id').val(0);
                        return {
                            label: item.nome,
                            value: item.nome,
                            realValue: item.id
                        };
                    }));
                });
            },
            select: function (event, ui) {
                $('#fornecedor-Id').val(ui.item.realValue);
                $('#fornecedor-search').val(ui.item.value);
                //$('.save-button').removeAttr('disabled');
                return false;
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    //$('.save-button').attr('disabled', 'disabled');
                    $('#fornecedor-Id').val(0);
                    $("#fornecedor-search").val('').focus();
                    abp.notify.info(app.localize("EstadoInvalido"));
                    return false;
                }
            },
        });

        $('#centroCusto-search').autocomplete({
            minLength: 2,
            delay: 0,
            source: function (request, response) {
                var term = $('#centroCusto-search').val();
                var url = '/mpa/centrosCustos/autocomplete';

                var fullUrl = url + '/?term=' + term;
                $.getJSON(fullUrl, function (data) {
                    if (data.result.length == 0) {
                        $('#centroCusto-Id').val(0);
                        $("#centroCusto-search").focus();
                        abp.notify.info(app.localize("ListaVazia"));
                        return false;
                    };
                    response($.map(data.result, function (item) {
                        $('#centroCusto-Id').val(0);
                        return {
                            label: item.nome,
                            value: item.nome,
                            realValue: item.id
                        };
                    }));
                });
            },
            select: function (event, ui) {
                $('#centroCusto-Id').val(ui.item.realValue);
                $('#centroCusto-search').val(ui.item.value);
                //$('.save-button').removeAttr('disabled');
                return false;
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    //$('.save-button').attr('disabled', 'disabled');
                    $('#centroCusto-Id').val(0);
                    $("#centroCusto-search").val('').focus();
                    abp.notify.info(app.localize("CentroCustoInvalido"));
                    return false;
                }
            },
        });

        $('#Movimento').on('load', function () {
            var d = new Date();
            var n = d.getDate();
            $('#movimento').val(moment().format("L LT"));
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Suprimentos.Estoque.PreMovimento.Create'),
            edit: abp.auth.hasPermission('Pages.Tenant.Suprimentos.Estoque.PreMovimento.Edit'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Suprimentos.Estoque.PreMovimento.Delete')
        };

        var iValidador = {
            init: function () {
                // Execute seus códigos iniciais
                // ...
                //alert('Entrou no validador agora!');
                // Chame as funções desejadas...
                iValidador.outraFuncao();
            },
            outraFuncao: function () {
                // Códigos desejados...
            }
        };

        var _preMovimentoService = abp.services.app.estoquePreMovimento;
        var _movimentoService = abp.services.app.estoqueMovimento;

        var _estoquePreMovimentoItemService = abp.services.app.estoquePreMovimentoItem;
        var _$EstoquePreMovimentoItemTable = $('#EstoquePreMovimentoItemTable');
        var _$valesTable = $('#valesTable');
        var _$notaTable = $('#notaTable');
        var _$itensConsignadosTable = $('#itensConsignadosTable');

        var _createOrEditPreMovimentoItemModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/PreMovimentos/CriarOuEditarPreMovimentoItemModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_CriarOuEditarPreMovimentoItemModal.js',
            modalClass: 'CriarOuEditarPreMovimentoItemModal'
        });

        var _createOrEditLoteValidadeModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/PreMovimentos/InformarLoteValidadeModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Suprimentos/Estoques/PreMovimentos/_InformarLoteValidade.js',
            modalClass: 'EstoquePreMovimentoLoteValidadeProduto'
        });

        var _ErrorModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Erros/ExibirErros',
        });

        var _createOrEditLoteValidadeProdutoModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/PreMovimentos/InformarLoteValidadeProdutoModal',
            modalClass: 'EstoquePreMovimentoLoteValidadeProdutoViewModel'
        });


        var _importacaoProdutosModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/PreMovimentos/CarregarRelacionarImportacaoProdutos',
            modalClass: 'ImportacaoProdutosViewModel'
        });

        $('#btn-buscarNotas').click(function (e) {
            e.preventDefault()
                sincronizarNotasSefaz();
        });


        var _lotesValidades = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/PreMovimentos/BuscarLotesValidades'
            , modalClass: 'ImportacaoProdutosViewModel'

        });

        $('#btnLoteValidade').click(function (e) {
            e.preventDefault();

           
            _lotesValidades.open({ preMovimentoId: $('#id').val(), chave: $('#NumeroNotaFiscal').val(), empresaId: $('#EmpresaId').val() });
        });


        function sincronizarNotasSefaz() {
            if (confirm(app.localize('ConfirmSincronizarNotaFiscal'))) {
                $.ajax({
                    url: "/PreMovimentos/BuscarNfe",
                    data: { chave: $('#NumeroNotaFiscal').val(), empresaId: $('#EmpresaId').val() },
                    type: "GET",
                    timeout: 864000,
                    cache: false,
                    async: true,
                    beforeSend: function () {
                        $('#btn-sincronizar').buttonBusy(true);
                    },
                    complete: function () {
                        $('#btn-sincronizar').buttonBusy(false);
                    },
                    success: function (result) {

                       

                        if (result.Errors.length > 0) {
                            _ErrorModal.open({ erros: result.Errors });
                        }
                        else {

                            $('#DocumentoId').val(result.ReturnObject.Documento);
                            $('#SerieId').val(result.ReturnObject.Serie);
                            $('#Emissao').val(moment(result.ReturnObject.Emissao).format('L'));
                            $('#valorICMS').val(result.ReturnObject.ValorICMS.toLocaleString("pt-BR", { style: "currency", currency: "BRL" }).replace('R', '').replace('$', ''));
                            $('#totalDocumento').val(result.ReturnObject.TotalDocumento.toLocaleString("pt-BR", { style: "currency", currency: "BRL" }).replace('R', '').replace('$', ''));
                            $('#totalProdutoId').val(result.ReturnObject.TotalProduto.toLocaleString("pt-BR", { style: "currency", currency: "BRL" }).replace('R', '').replace('$', ''));
                            $('#descontoPer').val(result.ReturnObject.DescontoPer.toLocaleString("pt-BR", { style: "currency", currency: "BRL" }).replace('R', '').replace('$', ''));
                            $('#ValorAcrescimo').val(result.ReturnObject.AcrescimoDecrescimo.toLocaleString("pt-BR", { style: "currency", currency: "BRL" }).replace('R', '').replace('$', ''));
                            $('#EstTipoMovimentoId').val(result.ReturnObject.EstTipoMovimentoId).trigger("change");
                            $('#CFOPId').val(result.CFOPId).trigger("change");
                            $('#NFeItens').val(result.ReturnObject.NFeItens);
                            

                            $('#id').val(result.ReturnObject.Id);
                            $('#FornecedorId')
                                .append($("<option/>") //add option tag in select
                  .val(result.ReturnObject.Fornecedor.Id) //set value for option to post it
                  .text(result.ReturnObject.Fornecedor.Descricao))
                                .val(result.ReturnObject.Fornecedor.Id)
                                .trigger("change");

                            if (result.ReturnObject.Frete_Forncedor != undefined && result.ReturnObject.Frete_Forncedor != null)
                            {

                                $('#Frete_FornecedorId')
                                   .append($("<option/>") //add option tag in select
                                   .val(result.ReturnObject.Frete_Forncedor.Id) //set value for option to post it
                                   .text(result.ReturnObject.Frete_Forncedor.Descricao))
                                   .val(result.ReturnObject.Frete_Forncedor.Id)
                                   .trigger("change");
                            }


                            if (result.ReturnObject.TipoFrete != undefined && result.ReturnObject.TipoFrete != null)
                            {

                                $('#TipoFreteId')
                                   .append($("<option/>") //add option tag in select
                                   .val(result.ReturnObject.TipoFrete.Id) //set value for option to post it
                                   .text(result.ReturnObject.TipoFrete.Descricao))
                                   .val(result.ReturnObject.TipoFrete.Id)
                                   .trigger("change");
                            }



                            if (result.ReturnObject.ImportacaoProdutos.length > 0) {

                                _importacaoProdutosModal.open({ importacaoProdutosRegistrados: JSON.stringify(result.ReturnObject.ImportacaoProdutos), fornecedorId: result.ReturnObject.Fornecedor.Id, CNPJNota: result.ReturnObject.CNPJNota });
                            }
                            else {
                                getEstoquePreMovimentoItemTable();
                            }

                        }
                    }
                });
            }
        }


        $('#btn-novo-PreMovimentoItem').click(function (e) {
            e.preventDefault()
           
            var _$preMovimentoInformationsForm = $('form[name=preMovimentoInformationsForm');

            _$preMovimentoInformationsForm.validate();

            if (!_$preMovimentoInformationsForm.valid()) {
                return;
            }

            var preMovimento = _$preMovimentoInformationsForm.serializeFormToObject();

            preMovimento.ValorICMS = retirarMascara(preMovimento.ValorICMS);
            preMovimento.TotalDocumento = retirarMascara(preMovimento.TotalDocumento);
           // preMovimento.ICMSPer = retirarMascara(preMovimento.ICMSPer);
            preMovimento.DescontoPer = retirarMascara(preMovimento.DescontoPer);
           // preMovimento.ValorDesconto = retirarMascara(preMovimento.ValorDesconto);
            preMovimento.AcrescimoDecrescimo = retirarMascara(preMovimento.AcrescimoDecrescimo);
          //  preMovimento.FretePer = retirarMascara(preMovimento.FretePer);
            preMovimento.ValorFrete = retirarMascara(preMovimento.ValorFrete);
           // preMovimento.Frete = retirarMascara(preMovimento.Frete);
            preMovimento.ValorICMS = retirarMascara(preMovimento.ValorICMS);
            preMovimento.IsEntrada = true;

            //  _modalManager.setBusy(true);
            var editMode = $('#is-edit-mode').val();

            if ($('#id').val() == '' || $('#id').val() == '0') {

                _preMovimentoService.criarGetIdEntrada(preMovimento)
                      .done(function (data) {
                          abp.notify.info(app.localize('SavedSuccessfully'));
                          $('#id').val(data.id);

                          _createOrEditPreMovimentoItemModal.open({ preMovimentoId: $('#id').val(), id: 0 });

                      })
                     .always(function () {
                         //  _modalManager.setBusy(false);
                     });
            }
            else {

                 _createOrEditPreMovimentoItemModal.open({ preMovimentoId: $('#id').val(), id: 0 });


                //location.href = '/Mpa/PreMovimentos/CriarOuEditarPreMovimentoItemModal';
            }
        });

        $('#salvar-PreMovimento').click(function (e) {
            e.preventDefault()


            debugger;

           
            var _$preMovimentoInformationsForm = $('form[name=preMovimentoInformationsForm');

            _$preMovimentoInformationsForm.validate();

            if (!_$preMovimentoInformationsForm.valid()) {
                return;
            }

            var preMovimento = _$preMovimentoInformationsForm.serializeFormToObject();
           
            preMovimento.TotalDocumento = retirarMascara(preMovimento.TotalDocumento);
            preMovimento.TotalProduto = retirarMascara(preMovimento.TotalProduto);
          //  preMovimento.ICMSPer = retirarMascara(preMovimento.ICMSPer);
            preMovimento.DescontoPer = retirarMascara(preMovimento.DescontoPer);
         //   preMovimento.ValorDesconto = retirarMascara(preMovimento.ValorDesconto);
            preMovimento.AcrescimoDecrescimo = retirarMascara(preMovimento.AcrescimoDecrescimo);
         //   preMovimento.FretePer = retirarMascara(preMovimento.FretePer);
            preMovimento.ValorFrete = retirarMascara(preMovimento.ValorFrete);
        //    preMovimento.Frete = retirarMascara(preMovimento.Frete);
            preMovimento.ValorICMS = retirarMascara(preMovimento.ValorICMS);
            preMovimento.IsEntrada = true;
            //  _modalManager.setBusy(true);
            var editMode = $('#is-edit-mode').val();

          //  _preMovimentoService.criarOuEditar(preMovimento)
            //      .done(function (data) {

            preMovimento.fornecedorId = preMovimento.FornecedorId;


            $.ajax({
                url: "/PreMovimentos/Salvar",
                data: { input: JSON.stringify( preMovimento) },
                type: "POST",
                timeout: 864000,
                cache: false,
                async: false,
                beforeSend: function (result) {
                    debugger;
                    //  $('#btn-sincronizar').buttonBusy(true);
                },
                complete: function (result) {
                    debugger;
                    //$('#btn-sincronizar').buttonBusy(false);
                },
                success: function (result) {

                    debugger;

                    if (result.Errors.length > 0) {
                        _ErrorModal.open({ erros: result.Errors });
                    }
                    else {

                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $('#id').val(result.ReturnObject.id);

                        if (result.ReturnObject.possuiLoteValidade) {
                            _createOrEditLoteValidadeModal.open({ preMovimentoId: result.ReturnObject.id });
                        }
                        else {

                            location.href = '/mpa/preMovimentos';

                            //  $('#divConfirmarEntrada').show();


                        }
                    }

                }
                ,error: function (result, execption,a,b,c,d) {
                debugger;
                    //$('#btn-sincronizar').buttonBusy(false);
                },

                fail: function (result) {
                    debugger;
                    //$('#btn-sincronizar').buttonBusy(false);
                },





                //.always(function () {
                //  _modalManager.setBusy(false);
                //});

            });
        });

        //$('#ConfirmarEntradaButton').click(function (e) {
        //    e.preventDefault()

        //    _movimentoService.gerarMovimentoEntrada($('#id').val())
        //        .done(function (data) {

        //            if (data.errors.length > 0) {
        //                _ErrorModal.open({ erros: data.errors });
        //            }
        //            else {
        //                 abp.notify.info(app.localize('SavedSuccessfully'));
        //                 location.href = '/mpa/preMovimentos';
        //            }
        //        });
        //});

        function retirarMascara(valor) {

            while (valor.indexOf('_') != -1) valor = valor.replace('_', '');
            while (valor.indexOf('.') != -1) valor = valor.replace('.', '');

            valor = valor.replace(',', '.');

            return valor;
        }

        function salvar(e) {

        }


        $('input[name="Movimento"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            maxDate: new Date(),
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
           function (selDate) {
               $('input[name="Movimento"]').val(selDate.format('L')).addClass('form-control edited');
               // obterIdade(selDate);
           });

        $('input[name="Emissao"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            maxDate: new Date(),
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
         function (selDate) {
             $('input[name="Emissao"]').val(selDate.format('L')).addClass('form-control edited');
         });


        abp.event.on('app.CriarOuEditarPreMovimentoItemModalSaved', function () {
            getEstoquePreMovimentoItemTable();
        });

        var _estoquePreMovimentoService = abp.services.app.estoquePreMovimento;

        var _modalManager;

        this.init = function (modalManager) {
            _modalManager = modalManager;

        };

        $('.close').on('click', function () {
            location.href = '/mpa/preMovimentos';
        });

        $('.close-button').on('click', function () {
            location.href = '/mpa/preMovimentos';
        });

        _$EstoquePreMovimentoItemTable.jtable
        ({
            title: app.localize('Item'),
            paging: true,
            sorting: true,
            edit: false,
            create: false,
            multiSorting: true,
            actions:
            {
                listAction:
                {
                    method: _estoquePreMovimentoService.listarItens
                },
            },
            fields:
            {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '11%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');

                        if (_permissions.edit && $('#PreMovimentoEstadoId').val() != 2) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                     _createOrEditPreMovimentoItemModal.open({ id: data.record.id, preMovimentoId: $('#id').val() });


                                  

                                });
                        }

                        if (_permissions.delete && $('#PreMovimentoEstadoId').val()!=2) {

                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                              .appendTo($span)
                              .click(function (e) {
                                  e.preventDefault();
                                  deletePreMovimentoItem(data.record);
                              });
                        }

                        if (data.record.isValidade || data.record.isLote) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('LoteValidade') + '"><i class="fa fa-calendar"></i></button>')
                               .appendTo($span)
                               .click(function (e) {
                                   e.preventDefault();
                                   abrirPreMovimentoItemLoteValidade(data.record);
                               });
                        }
                        return $span;
                    }
                },
                PreMovimentoId: {
                    type: 'hidden',
                    defaultValue: function (data) {
                        return $('#id').val();
                    },
                },

                ProdutoId: {
                    title: app.localize('Produto'),
                    width: '30%',
                    display: function (data) {
                        if (data.record.produto) {
                            return data.record.produto;
                        }
                    }
                },
                NumeroSerie: {
                    title: app.localize('NumeroSerie'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.numeroSerie) {
                            return data.record.numeroSerie;
                        }
                    }
                },

                quantidade: {
                    title: app.localize('Quantidade'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.quantidade) {
                            return posicionarDireita(data.record.quantidade.toFixed(2));
                        }
                    }
                },

                CustoUnitario: {
                    title: app.localize('CustoUnitario'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.custoUnitario) {
                            return posicionarDireita(formatarValor4(data.record.custoUnitario));
                        }
                    }
                },

                CustoTotal: {
                    title: app.localize('CustoTotal'),
                    width: '10%',
                    display: function (data) {
                        if (data.record.custoTotal) {
                            return posicionarDireita(formatarValor(data.record.custoTotal));
                        }
                    }
                },

                Unidade: {
                    title: app.localize('Unidade'),
                    width: '20%',
                    display: function (data) {
                        if (data.record.unidade) {
                            return data.record.unidade;
                        }
                    }
                },
            }
        });

        _$valesTable.jtable
      ({
          title: app.localize('Vales'),
          paging: true,
          sorting: true,
          edit: false,
          create: false,
          multiSorting: true,
          actions:
          {
              listAction:
              {
                  method: _movimentoService.listarVales
              },
          },
          fields:
          {
              id: {
                  key: true,
                  list: false
              },
              Empresa: {
                  title: app.localize('Empresa'),
                  width: '15%',
                  display: function (data) {
                      return data.record.empresa;
                  }
              },

              Fornecedor: {
                  title: app.localize('Fornecedor'),
                  width: '15%',
                  display: function (data) {
                      return data.record.fornecedor;
                  }
              },
              Emissao: {
                  title: app.localize('Emissao'),
                  width: '15%',
                  display: function (data) {
                      return moment(data.record.emissao).format('L');
                  }
              },

              Documento: {
                  title: app.localize('Documento'),
                  width: '10%',
                  display: function (data) {
                      return data.record.documento;
                  }
              },

              Valor: {
                  title: app.localize('Valor'),
                  width: '10%',
                  display: function (data) {
                      return posicionarDireita(formatarValor(data.record.valorDocumento));
                  }
              }
             
          }
      });

        _$notaTable.jtable
    ({
        title: app.localize('Vales'),
        paging: true,
        sorting: true,
        edit: false,
        create: false,
        multiSorting: true,
        actions:
        {
            listAction:
            {
                method: _movimentoService.listarNota
            },
        },
        fields:
        {
            id: {
                key: true,
                list: false
            },
            Empresa: {
                title: app.localize('Empresa'),
                width: '15%',
                display: function (data) {
                    return data.record.empresa;
                }
            },

            Fornecedor: {
                title: app.localize('Fornecedor'),
                width: '15%',
                display: function (data) {
                    return data.record.fornecedor;
                }
            },
            Emissao: {
                title: app.localize('Emissao'),
                width: '15%',
                display: function (data) {
                    return moment(data.record.emissao).format('L');
                }
            },

            Documento: {
                title: app.localize('Documento'),
                width: '10%',
                display: function (data) {
                    return data.record.documento;
                }
            },

            Valor: {
                title: app.localize('Valor'),
                width: '10%',
                display: function (data) {
                    return posicionarDireita(formatarValor(data.record.valorDocumento));
                }
            }

        }
    });

             

        _$itensConsignadosTable.jtable
      ({
          title: app.localize('Item'),
          paging: true,
          sorting: true,
          edit: false,
          create: false,
          multiSorting: true,
          actions:
          {
              listAction:
              {
                  method: _movimentoService.listarItensConsignados
              },
          },
          fields:
          {
              id: {
                  key: true,
                  list: false
              },
            
              PreMovimentoId: {
                  type: 'hidden',
                  defaultValue: function (data) {
                      return $('#id').val();
                  },
              },

              ProdutoId: {
                  title: app.localize('Produto'),
                  width: '30%',
                  display: function (data) {
                      if (data.record.produto) {
                          return data.record.produto;
                      }
                  }
              },
              NumeroSerie: {
                  title: app.localize('NumeroSerie'),
                  width: '10%',
                  display: function (data) {
                      if (data.record.numeroSerie) {
                          return data.record.numeroSerie;
                      }
                  }
              },

              quantidade: {
                  title: app.localize('Quantidade'),
                  width: '10%',
                  display: function (data) {
                      if (data.record.quantidade) {
                          return posicionarDireita(data.record.quantidade);
                      }
                  }
              },

              CustoUnitario: {
                  title: app.localize('CustoUnitario'),
                  width: '10%',
                  display: function (data) {
                      if (data.record.custoUnitario) {
                          return posicionarDireita(formatarValor(data.record.custoUnitario));
                      }
                  }
              },

              CustoTotal: {
                  title: app.localize('CustoTotal'),
                  width: '10%',
                  display: function (data) {
                      if (data.record.custoTotal) {
                          return posicionarDireita(formatarValor(data.record.custoTotal));
                      }
                  }
              },

              Unidade: {
                  title: app.localize('Unidade'),
                  width: '20%',
                  display: function (data) {
                      if (data.record.unidade) {
                          return data.record.unidade;
                      }
                  }
              },
          }
      });



        function getValesTable(reload) {

            if (reload) {
                _$valesTable.jtable('reload');
            } else {
               
                _$valesTable.jtable('load', { filtro: $('#id').val() });
            }
        }

        function getNotaTable(reload) {

            if (reload) {
                _$notaTable.jtable('reload');
            } else {
               
                _$notaTable.jtable('load', { filtro: $('#id').val() });
            }
        }

        function getItensConsignados(reload) {

            if (reload) {
                _$itensConsignadosTable.jtable('reload');
            } else {
               
                _$itensConsignadosTable.jtable('load', { filtro: $('#id').val() });
            }
        }
        
        

        function deletePreMovimentoItem(preMovimentoItem) {

            abp.message.confirm(
                app.localize('DeleteWarning', preMovimentoItem.produto.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _estoquePreMovimentoItemService.excluir(preMovimentoItem.id)
                            .done(function () {
                                getEstoquePreMovimentoItemTable(true);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }


        function getEstoquePreMovimentoItemTable(reload) {

            if (reload) {
                _$EstoquePreMovimentoItemTable.jtable('reload');
            } else {
                _$EstoquePreMovimentoItemTable.jtable('load', { filtro: $('#id').val(), entradaConfirmada: $('#entradaConfirmadaId').val() });
            }
        }


        function abrirPreMovimentoItemLoteValidade(preMovimentoItem) {
            _createOrEditLoteValidadeProdutoModal.open({ preMovimentoItemId: preMovimentoItem.id, produtoId: preMovimentoItem.produtoId });

        }


        getEstoquePreMovimentoItemTable();
        getValesTable();
        getNotaTable();
        getItensConsignados();

        $('#AplicacaoDiretaId').on('click', function (e) {
           

            var checkbox = e.target;
            if (checkbox.checked)
            {
                $('#divPaciente').show();   
            }
            else
            {
                $('#divPaciente').hide();
            }

        });



        var _imprimirEntrada = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/RelatorioEntrada'
           
        });


        $('#btnImprimir').on('click', function (e)
        {
            _imprimirEntrada.open({ preMovimentoId: $('#id').val() });
        });

    


        selectSW('.selectForncedor', "/api/services/app/fornecedor/ListarDropdownSisFornecedor");
        selectSW('.selectCFOP', "/api/services/app/cfop/ListarDropdown");
        selectSW('.selectTipoFrete', "/api/services/app/tipoFrete/ListarDropdown");
        selectSW('.selectEstoque', "/api/services/app/estoque/ResultDropdownList");
        selectSW('.selectEmpresa', "/api/services/app/empresa/ListarDropdownPorUsuario");
        selectSW('.selectTipoMovimento', "/api/services/app/tipomovimento/ListarDropdownEntrada");
        selectSW('.selectCentroCusto', "/api/services/app/centrocusto/ListarDropdown");
        selectSW('.selectOrdemCompra', "/api/services/app/ordemcompra/ListarDropdown");
        selectSW('.selectPaciente', "/api/services/app/paciente/ListarDropdown");


        function getRegistros() {

            debugger;

            lista = JSON.parse($('#lancamentosJson').val());

            var allRows = _$lancamentosTable.find('.jtable-data-row')

            $.each(allRows, function () {
                var id = $(this).attr('data-record-key');
                _$lancamentosTable.jtable('deleteRecord', { key: id, clientOnly: true });
            });

            for (var i = 0; i < lista.length; i++) {
                var item = lista[i];

                item.DataVencimento = moment(item.DataVencimento).format('L');
                item.DataLancamento = moment(item.DataLancamento).format('L');
                _$lancamentosTable.jtable('addRecord', {
                    record: item
                    , clientOnly: true
                });
            }
        }


        var _$lancamentosTable = $('#lancamentosTable');

        _$lancamentosTable.jtable({

            title: app.localize('Parcelas'),
            sorting: true,
            edit: false,
            create: false,
            multiSorting: true,
            selecting: true,
            selectingCheckboxes: true,

            rowUpdated: function (event, data) {
                if (data) {

                    debugger;
                    if (data.record.CorLancamentoFundo) {

                        data.row[0].cells[2].setAttribute('bgcolor', data.record.CorLancamentoFundo);
                        data.row[0].cells[2].setAttribute('color', data.record.CorLancamentoLetra);

                        // data.row.css("background", data.record.CorLancamentoFundo);
                        // data.row.css("color", data.record.CorLancamentoLetra);
                    }


                }
            },

            rowInserted: function (event, data) {
                if (data) {

                    debugger;
                    if (data.record.CorLancamentoFundo) {

                        data.row[0].cells[2].setAttribute('bgcolor', data.record.CorLancamentoFundo);
                        data.row[0].cells[2].setAttribute('color', data.record.CorLancamentoLetra);
                    }
                }


                if (data.record.IsSelecionado) {
                    editRegistro(data.record);
                    //  data.row.addClass('jtable-row-selected');

                    data.row.click();

                }

            },

            fields: {
                IdGrid: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '8%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                            .appendTo($span)
                            .click(function (e) {
                                e.preventDefault();
                                editRegistro(data.record);
                               // getQuitacoes();
                            });

                        $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                            .appendTo($span)
                            .click(function (e) {
                                e.preventDefault();
                                deleteRegistro(data.record);
                            });

                        return $span;
                    }
                },

                Situacao: {
                    title: app.localize('Situacao'),
                    width: '15%',
                    display: function (data) {
                        debugger;
                        return data.record.SituacaoDescricao;
                    }
                },

                Parcela: {
                    title: app.localize('Parcela'),
                    width: '10%',
                    display: function (data) {
                        debugger;
                        return data.record.Parcela;
                    }
                },

                Vencimento: {
                    title: app.localize('Vencimento'),
                    width: '15%',
                    display: function (data) {
                        debugger;
                        return data.record.DataVencimento;
                    }
                },

                Valor: {
                    title: app.localize('Valor'),
                    width: '10%',
                    display: function (data) {
                        debugger;
                        if (data.record.ValorLancamento) {
                          
                            return posicionarDireita(formatarValor(data.record.ValorLancamento));
                        }
                    }
                },

                NossoNumero: {
                    title: app.localize('NossoNumero'),
                    width: '30%',
                    display: function (data) {
                        debugger;
                        return data.record.NossoNumero;
                    }
                },

                Competencia: {
                    title: app.localize('Competencia'),
                    width: '10%',
                    display: function (data) {
                        debugger;
                        return (data.record.MesCompetencia.length == 2 ? data.record.MesCompetencia : ('0' + data.record.MesCompetencia)) + "/" + data.record.AnoCompetencia;
                    }
                },

            }
        });

        var lista = [];


        getRegistros();

        $('#inserir').click(function (e) {
            e.preventDefault();

            debugger;


            var _$lancamentoInformationsForm = $('form[name=LancamentoInformationsForm]');
        
            //AltararValidacaoParcela(true);

            //if (!ValidarParcela()) {
            //    AltararValidacaoParcela(false);
            //    return;
            //}

            //AltararValidacaoParcela(false);



            var lancamento = _$lancamentoInformationsForm.serializeFormToObject();


            if ($('#lancamentosJson').val() != '') {
                lista = JSON.parse($('#lancamentosJson').val());
            }

            if ($('#idGridLancamento').val() != '') {

                for (var i = 0; i < lista.length; i++) {
                    if (lista[i].IdGrid == $('#idGridLancamento').val()) {

                        //var situacao = $('#situacaoId').select2('data');
                        //if (situacao && situacao.length > 0) {

                        //    lista[i].SituacaoDescricao = situacao[0].text;
                        //}


                        lista[i].SituacaoDescricao = $('#situacaoDescricao').val();

                        lista[i].SituacaoLancamentoId = $('#situacaoId').val();
                        lista[i].ValorLancamento = retirarMascara($('#valor').val());
                        lista[i].ValorAcrescimoDecrescimo = retirarMascara($('#acrescimoDecrescimo').val());
                        lista[i].Juros = retirarMascara($('#juros').val());
                        lista[i].Multa = retirarMascara($('#multa').val());
                        lista[i].Total = retirarMascara($('#total').val());
                        lista[i].MesCompetencia = $('#mes').val();
                        lista[i].AnoCompetencia = $('#ano').val();
                        lista[i].DataVencimento = $('#dataVencimento').val();
                        lista[i].DataLancamento = $('#dataCadastro').val();
                        lista[i].CorLancamentoFundo = $('#corLancamentoFundo').val();
                        lista[i].CorLancamentoLetra = $('#corLancamentoLetra').val();
                        lista[i].CodigoBarras = $('#codigoBarras').val();
                        lista[i].NossoNumero = $('#nossoNumero').val();
                        lista[i].LinhaDigitavel = $('#linhaDigitavel').val();
                        lista[i].Parcela = $('#parcela').val();

                        _$lancamentosTable.jtable('updateRecord', {
                            record: lista[i]
                        , clientOnly: true
                        });

                    }
                }
            }
            else {
                lancamento.IdGrid = lista.length == 0 ? 1 : lista[lista.length - 1].IdGrid + 1;

                //lancamento.SituacaoLancamentoId = $('#situacaoId').val();

                //var situacao = $('#situacaoId').select2('data');
                //if (situacao && situacao.length > 0) {

                //    lancamento.SituacaoDescricao = situacao[0].text;
                //}

                lancamento.SituacaoDescricao = $('#situacaoDescricao').val();
                lancamento.SituacaoLancamentoId = $('#situacaoId').val();
                lancamento.ValorLancamento = retirarMascara($('#valor').val());
                lancamento.ValorAcrescimoDecrescimo = retirarMascara($('#acrescimoDecrescimo').val());
                lancamento.Juros = retirarMascara($('#juros').val());
                lancamento.Multa = retirarMascara($('#multa').val());
                lancamento.Total = retirarMascara($('#total').val());
                lancamento.MesCompetencia = $('#mes').val();
                lancamento.AnoCompetencia = $('#ano').val();
                lancamento.DataVencimento = $('#dataVencimento').val();
                lancamento.DataLancamento = $('#dataCadastro').val();
                lancamento.CorLancamentoFundo = $('#corLancamentoFundo').val();
                lancamento.CorLancamentoLetra = $('#corLancamentoLetra').val();
                lancamento.CodigoBarras = $('#codigoBarras').val();
                lancamento.NossoNumero = $('#nossoNumero').val();
                lancamento.LinhaDigitavel = $('#linhaDigitavel').val();
                lancamento.Parcela = $('#parcela').val();

                lista.push(lancamento);

                _$lancamentosTable.jtable('addRecord', {
                    record: lancamento
                  , clientOnly: true
                });

            }
            $('#lancamentosJson').val(JSON.stringify(lista));
            limpar();
            //$('#lancamentosJson').val(JSON.stringify(lista));
            //$('#idGridLancamento').val('');
            //$('#situacaoDescricao').val('01 - Aberto');
            //$('#situacaoId').val(0);
            //$('#valor').val('');
            //$('#acrescimoDecrescimo').val('');
            //$('#juros').val('');
            //$('#multa').val('');
            //$('#total').val('');
            //$('#mes').val('');
            //$('#ano').val('');
            //$('#dataVencimento').val('');
            //$('#dataCadastro').val(moment(new Date()).format('L'));
            //$('#corLancamentoFundo').val('');
            //$('#corLancamentoLetra').val('');
            //$('#codigoBarras').val('');
            //$('#nossoNumero').val('');
            //$('#linhaDigitavel').val('');

            CalculaValorLancamento();

            $('#valor').focus();

        });

        function CalculaValorLancamento() {
            var totalLancamento = 0;

            for (var i = 0; i < lista.length; i++) {
                totalLancamento += parseFloat(lista[i].ValorLancamento);
            }

            $('#valorTotalParcelas').val(formatarValor(totalLancamento));
        }


        function calcularTotal() {
            var total = 0;

            var valor = $('#valor').val() != '' ? parseFloat(retirarMascara($('#valor').val())) : 0;
            var juros = $('#juros').val() != '' ? parseFloat(retirarMascara($('#juros').val())) : 0;
            var multa = $('#multa').val() != '' ? parseFloat(retirarMascara($('#multa').val())) : 0;
            var acrescimoDecrescimo = $('#acrescimoDecrescimo').val() != '' ? parseFloat(retirarMascara($('#acrescimoDecrescimo').val())) : 0;

            total = valor + juros + multa + acrescimoDecrescimo;
            var totalFormatado = formatarValor(total);

            $('#total').val(totalFormatado);
        }

        $('.calcularTotal').on('blur', function (e) {
            e.preventDefault();

            calcularTotal();



        });

        function limpar() {
            $('#idGridLancamento').val('');
            $('#situacaoDescricao').val('01 - Aberto');
            $('#situacaoId').val(0);
            $('#valor').val('');
            $('#acrescimoDecrescimo').val('');
            $('#juros').val('');
            $('#multa').val('');
            $('#total').val('');
            $('#mes').val('');
            $('#ano').val('');
            $('#dataVencimento').val('');
            $('#dataCadastro').val(moment(new Date()).format('L'));
            $('#corLancamentoFundo').val('');
            $('#corLancamentoLetra').val('');
            $('#codigoBarras').val('');
            $('#nossoNumero').val('');
            $('#linhaDigitavel').val('');

            $('#parcela').val(lista.length + 1);

           // CalculaValorLancamento();



            $('#inserir > i').removeClass('fa-check');
            $('#inserir > i').addClass('fa-plus');
        }


        $('input[name="DataVencimento"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: false,
            //  maxDate: new Date(),
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
    function (selDate) {
        $('input[name="DataVencimento"]').val(selDate.format('L')).addClass('form-control edited');
    });

        
        $('input[name="DataPrimeiraParcela"]').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            autoUpdateInput: true,
            changeYear: true,
            yearRange: 'c-10:c+10',
            showOn: "both",
            "locale": {
                "format": moment.locale().toUpperCase() === 'PT-BR' ? "DD/MM/YYYY" : moment.locale().toUpperCase() === 'US' ? "MM/DD/YYYY" : "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    app.localize('Dom'),
                    app.localize('Seg'),
                    app.localize('Ter'),
                    app.localize('Qua'),
                    app.localize('Qui'),
                    app.localize('Sex'),
                    app.localize('Sab')
                ],
                "monthNames": [
                    app.localize("Jan"),
                    app.localize("Fev"),
                    app.localize("Mar"),
                    app.localize("Abr"),
                    app.localize("Mai"),
                    app.localize("Jun"),
                    app.localize("Jul"),
                    app.localize("Ago"),
                    app.localize("Set"),
                    app.localize("Out"),
                    app.localize("Nov"),
                    app.localize("Dez"),
                ],
                "firstDay": 0
            }
        },
   function (selDate) {
       $('input[name="DataPrimeiraParcela"]').val(selDate.format('L')).addClass('form-control edited');
   });


        $('#gerarParcelas').on('click', function (e) {
            e.preventDefault();

            if ($('#totalDocumento').val() != '' && $('#quantidadeParcelas').val() != '') {
                var valorDocumento = parseFloat(retirarMascara($('#totalDocumento').val()));



                var parcelaInicial = parseInt($('#parcelaInicial').val());

                var quantidadeTotalParcelas = parseFloat($('#quantidadeParcelas').val());
                var quantidadeParcelas = quantidadeTotalParcelas - parcelaInicial + 1;

                var valorParcela = valorDocumento / quantidadeParcelas;




                //  var _$lancamentoInformationsForm = $('form[name=LancamentoInformationsForm]');


                var data = $('#dataPrimeiraParcela').val().split('/');

                var dataCorrente = new Date(data[2], data[1] - 1, data[0]);

                for (var i = 0 ; i < quantidadeParcelas; i++) {


                    var lancamento = $('form[name=LancamentoInformationsForm]').serializeFormToObject();

                    lancamento.IdGrid = lista.length == 0 ? 1 : lista[lista.length - 1].IdGrid + 1;

                    //Situação: Aberto
                    lancamento.SituacaoLancamentoId = 1;
                    lancamento.SituacaoDescricao = '01 - Aberto';
                    lancamento.ValorLancamento = valorParcela;
                    lancamento.Total = valorParcela;
                    lancamento.MesCompetencia = $('#mesCompetenciaParcelas').val();
                    lancamento.AnoCompetencia = $('#anoCompetenciaParcelas').val();
                    lancamento.DataVencimento = moment(dataCorrente).format('L');
                    lancamento.DataLancamento = moment(new Date()).format('L');
                    lancamento.Parcela = i + parcelaInicial;
                    lancamento.NossoNumero = $('#DocumentoId').val() + ' - ' + lancamento.Parcela + '/' + quantidadeTotalParcelas;

                    if (i == quantidadeParcelas - 1) {

                        var somaTodasParcelasLancadas = 0;

                        for (var j = 0; j < lista.length; j++) {
                            somaTodasParcelasLancadas += parseFloat(lista[j].ValorLancamento.toFixed(2));
                        }

                        lancamento.ValorLancamento = parseFloat((valorDocumento - somaTodasParcelasLancadas).toFixed(2));


                    } else {
                        lancamento.ValorLancamento = parseFloat(valorParcela.toFixed(2));;
                    }

                    lista.push(lancamento);

                    _$lancamentosTable.jtable('addRecord', {
                        record: lancamento
                      , clientOnly: true
                    });

                    dataCorrente.setMonth(dataCorrente.getMonth() + 1);

                }

                $('#lancamentosJson').val(JSON.stringify(lista));


                CalculaValorLancamento();
            }

        });



        function editRegistro(lancamento) {

            debugger;


            $('#lancamentoId').val(lancamento.Id);
            $('#valor').val(formatarValor(lancamento.ValorLancamento));
            $('#dataVencimento').val(lancamento.DataVencimento);
            $('#idGridLancamento').val(lancamento.IdGrid);
            $('#corFundoLancamento').val(lancamento.CorFundoLancamento);
            $('#corLentraLancamento').val(lancamento.CorLentraLancamento);
            $('#juros').val(formatarValor(lancamento.Juros));
            $('#multa').val(formatarValor(lancamento.Multa));
            $('#acrescimoDecrescimo').val(formatarValor(lancamento.ValorAcrescimoDecrescimo));
            $('#total').val(lancamento.Total);
            $('#dataCadastro').val(lancamento.DataLancamento);
            $('#mes').val(lancamento.MesCompetencia);
            $('#ano').val(lancamento.AnoCompetencia);
            $('#situacaoId').val(lancamento.SituacaoLancamentoId);
            $('#situacaoDescricao').val(lancamento.SituacaoDescricao);
            $('#codigoBarras').val(lancamento.CodigoBarras);
            $('#nossoNumero').val(lancamento.NossoNumero);
            $('#linhaDigitavel').val(lancamento.LinhaDigitavel);
            $('#parcela').val(lancamento.Parcela);
            $('#idGridResultadoExame').val(lancamento.IdGridResultadoExame);





            // $('#inserir > i').removeClass('fa');
            $('#inserir > i').removeClass('fa-plus');
            // $('#inserir > i').addClass('glyphicon');
            $('#inserir > i').addClass('fa-check');


            calcularTotal();
        }

        function deleteRegistro(lancamento) {
            abp.message.confirm(
                app.localize('DeleteWarning', lancamento.NossoNumero),
                function (isConfirmed) {
                    if (isConfirmed) {



                        lista = JSON.parse($('#lancamentosJson').val());

                        for (var i = 0; i < lista.length; i++) {
                            if (lista[i].IdGrid == lancamento.IdGrid) {
                                lista.splice(i, 1);
                                $('#lancamentosJson').val(JSON.stringify(lista));

                                _$lancamentosTable.jtable('deleteRecord', {
                                    key: lancamento.IdGrid
                                , clientOnly: true
                                });

                                break;
                            }
                        }

                        CalculaValorLancamento();
                        limpar();
                    }
                }
            );
        }

    });

})(jQuery);