﻿(function () {
    $(function () {
        var _$contasPagarTable = $('#contasPagarTable');
        var _contasPagarService = abp.services.app.contasPagar;
        var _$contasPagarFilterForm = $('#contasPagarFilterForm');

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Financeiro.ContasPagar.Create'),
            edit: abp.auth.hasPermission('Pages.Tenant.Financeiro.ContasPagar.Edit'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Financeiro.ContasPagar.Delete')
        };



        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Quitacao/CriarOuEditarModalPorLancamentos',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Financeiros/ContasPagar/_CriarOuEditarModalQuitacaoLancamentos.js',
            modalClass: 'CriarOuEditarQuitacaoModal'
        });



        _$contasPagarTable.jtable({

            title: app.localize('ContasPagar'),
            paging: true,
            sorting: true,
            multiSorting: true,
            selecting: true,
            selectingCheckboxes: true,
            multiselect: true,

            rowUpdated: function (event, data) {
                if (data) {
                    if (data.record.corLancamentoLetra) {
                        data.row[0].cells[2].setAttribute('color', data.record.corLancamentoLetra);
                    }

                    if (data.record.corLancamentoFundo) {
                        data.row[0].cells[2].setAttribute('bgcolor', data.record.corLancamentoFundo);
                    }
                }


            },
            rowInserted: function (event, data) {
                if (data) {
                    if (data.record.corLancamentoLetra) {
                        data.row[0].cells[2].setAttribute('color', data.record.corLancamentoLetra);
                    }

                    if (data.record.corLancamentoFundo) {
                        data.row[0].cells[2].setAttribute('bgcolor', data.record.corLancamentoFundo);
                    }
                }

            },

            actions: {
                listAction: {
                    method: _contasPagarService.listarLancamento
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '6%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissions.edit) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    // _createOrEditModal.open({ id: data.record.id });
                                    location.href = 'ContasPagar/CriarOuEditarModal/' + data.record.id
                                });
                        }

                        //if (_permissions.delete) {
                        //    $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                        //        .appendTo($span)
                        //        .click(function () {
                        //            deleteRegistro(data.record);
                        //        });
                        //}

                        return $span;
                    }
                },


                SituacaoLancamento: {
                    title: app.localize('Situacao'),
                    width: '10%',
                    display: function (data) {
                        return data.record.situacaoDescricao;
                    }
                },

                Emissao: {
                    title: app.localize('Emissao'),
                    width: '7%',
                    display: function (data) {
                        return moment(data.record.dataEmissao).format('L');
                    }
                },

                Vencimento: {
                    title: app.localize('Vencimento'),
                    width: '7%',
                    display: function (data) {
                        return moment(data.record.dataVencimento).format('L');
                    }
                },

                Competencia: {
                    title: app.localize('Competencia'),
                    width: '7%',
                    display: function (data) {
                        if (data) {
                            return data.record.competencia;
                        }
                    }
                },
                Documento: {
                    title: app.localize('Documento'),
                    width: '7%',
                    display: function (data) {
                        if (data) {
                            return data.record.documento;
                        }
                    }
                },
                Fornecedor: {
                    title: app.localize('Fornecedor'),
                    width: '7%',
                    display: function (data) {
                        if (data) {
                            return data.record.fornecedor;
                        }
                    }
                },

                TotalLancamento: {
                    title: app.localize('TotalLancamento'),
                    width: '7%',
                    display: function (data) {
                        if (data) {
                            return posicionarDireita(formatarValor(data.record.totalLancamento));
                        }
                    }
                },

                TotalQuitacao: {
                    title: app.localize('TotalQuitacao'),
                    width: '7%',
                    display: function (data) {
                        if (data) {
                            return posicionarDireita(formatarValor(data.record.totalQuitacao));
                        }
                    }
                },

                //TipoDocumento: {
                //    title: app.localize('TipoDocumento'),
                //    width: '7%',
                //    display: function (data) {
                //        if (data) {
                //            return data.record.tipoDocumento;
                //        }
                //    }
                //},
            }
        });

        function getRegistros(reload) {
           
            if (reload) {
                _$contasPagarTable.jtable('reload');
            } else {
                _$contasPagarTable.jtable('load', {
                    // filtro: $('#tableFilter').val()
                    pessoaId: $('#pessoaId').val(),
                    emissaoDe: _selectedDateRange.startDate,
                    emissaoAte: _selectedDateRange.endDate,
                    empresaId: $('#empresaFiltroId').val(),
                    situacaoLancamentoId: $('#situacaoId').val(),
                    documento: $('#documento').val(),
                    contaAdministrativaId: $('#contaAdministrativaId').val(),
                    centroCustoId: $('#centroCustoId').val(),
                    vencimentoDe: $('#vencimento').val() != '' ? _selectedDateRangeVencimento.startDate : '',
                    vencimentoAte: $('#vencimento').val() != '' ? _selectedDateRangeVencimento.endDate : '',
                    meioPagamentoId: $('#meioPagamentoFiltroId').val(),
                    tipoDocumentoId: $('#tipoDocumentoId').val(),
                    ignorarVencimento: $('#ignorarVencimento')[0].checked,
                    ignorarEmissao: $('#ignorarEmissao')[0].checked

                });
            }
        }

        function deleteRegistro(record) {

            abp.message.confirm(
                app.localize('DeleteWarning', record.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _$contasPagarTable.excluir(record)
                            .done(function () {
                                getRegistros(true);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms);
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedLancamentosFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedLancamentosFiltersArea').slideUp();
        });

        $('.novo-lancamento').click(function (e) {
            //_createOrEditModal.open();
            e.preventDefault();
           
            location.href = 'ContasPagar/CriarOuEditarModal/';
        });


        $('#refreshLancamentosButton').click(function (e) {
            e.preventDefault();
            getRegistros();
        });

        abp.event.on('app.CriarOuEditarFeriadoModalSaved', function () {
            getRegistros(true);
        });



        $('#tableFilter').focus();


        $('#btnQuitarLancamentos').click(function (e) {
            e.preventDefault();
           
            var lancamentosSelecionados = _$contasPagarTable.jtable('selectedRows');

            var listaId = [];

            lancamentosSelecionados.each(function () {
                var registro = $(this).data('record');
                //      //console.log(JSON.stringify(registro));
                listaId.push(registro.id);


            });

            if (listaId.length > 0) {
                _createOrEditModal.open({ ids: listaId });
            }
        });


        function formatarValor(valor) {
            if (valor != '' && valor != null) {
                var retorno = valor.toLocaleString("pt-BR", { style: "currency", currency: "BRL" }).replace('R', '').replace('$', '');
                return retorno;
            }
            return '';

        }

        var _selectedDateRange = {
            startDate: moment().startOf('day'),
            endDate: moment().endOf('day'),
            maxDate: "31/12/2030"
        };

        _$contasPagarFilterForm.find('input.emissao').daterangepicker(

         $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
         function (start, end, label) {

             _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
             _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
         });




        var _selectedDateRangeVencimento = {
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            maxDate: "31/12/2030"
        };

        _$contasPagarFilterForm.find('input.vencimento').daterangepicker(
        $.extend(true, app.createDateRangePickerOptions(), _selectedDateRangeVencimento),
        function (start, end, label) {
            _selectedDateRangeVencimento.startDate = start.format('YYYY-MM-DDT00:00:00Z');
            _selectedDateRangeVencimento.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
        });


        $('#emissao').on('cancel.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
            $('#emissao').val('');
        });

        $('#vencimento').on('cancel.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
           
            $('#vencimento').val('');
        });



        getRegistros();
        $('#emissao').val('');

        selectSW('.selectForncedor', "/api/services/app/sisPessoa/ListarDropdownSisIsPagar");
        selectSW('.selectEmpresa', "/api/services/app/empresa/ListarDropdownPorUsuario");
        selectSW('.selectSituacao', "/api/services/app/SituacaoLancamento/ListarDropdown");
        selectSW('.selectContaAdministrativa', "/api/services/app/ContaAdministrativa/ListarContaAdministrivaDespesaDropdown");
        selectSW('.selectCentroCusto', "/api/services/app/CentroCusto/ListarDropdownCodigoCentroCusto");
        selectSW('.selectMeioPagamento', "/api/services/app/MeioPagamento/ListarDropdown");
        selectSW('.selectTipoDocumento', "/api/services/app/tipoDocumento/ListarDropdown");



    });
})();