﻿(function () {
    $(function () {
        var _$SolicitacoesExamesTable = $('#SolicitacoesExamesTable-' + localStorage["AtendimentoId"]);
        var _$DetalharSolicitacoesExamesTable = $('#DetalharSolicitacaoExameTable-' + localStorage["AtendimentoId"]);
        var _SolicitacoesExamesService = abp.services.app.solicitacaoExame;
        var _SolicitacoesExamesItensService = abp.services.app.solicitacaoExameItem;
        var _$filterForm = $('#SolicitacoesExamesFilterForm-' + localStorage["AtendimentoId"]);
        var _selectedDateRange = {
            startDate: moment(localStorage["DataAtendimento"]).startOf('day'), //moment().startOf('day'),
            endDate: moment().endOf('day')
        };

        $('#date-range-' + localStorage["AtendimentoId"]).daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
            });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExame'),
            edit: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExame'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExame')
        };

        var _permissionsItem = {
            create: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExameItem'),
            edit: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExameItem'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExameItem')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Assistenciais/CriarOuEditarSolicitacaoExameModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/SolicitacoesExames/CriarOuEditarSolicitacaoExame.js',
            modalClass: 'CriarOuEditarSolicitacaoExameModal'
        });

        var _createOrEditItemModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Assistenciais/CriarOuEditarSolicitacaoExameItemModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/SolicitacoesExames/CriarOuEditarSolicitacaoExameItem.js',
            modalClass: 'CriarOuEditarSolicitacaoExameItemModal'
        });

        $('#SolicitacoesExamesTable-' + localStorage["AtendimentoId"]).jtable({

            title: app.localize('SolicitacaoExame'),
            paging: true,
            sorting: true,
            multiSorting: true,
            selecting: true,
            multiselect: false,
            selectingCheckboxes: true,

            actions: {
                listAction: {
                    method: _SolicitacoesExamesService.listar
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },
                codigo: {
                    title: app.localize('Codigo'),
                    width: '15%',
                    display: function (data) {
                        return zeroEsquerda(data.record.codigo, '8');
                    }
                },
                dataSolicitacao: {
                    title: app.localize('Data'),
                    width: '25%',
                    display: function (data) {
                        return moment(data.record.dataSolicitacao).format('L LT');
                    }
                },
                unidadeOrganizacional: {
                    title: app.localize('UnidadeOrganizacional'),
                    width: '30%'
                    //display: function (data) {
                    //    if (data.record.unidadeOrganizacional != null) {
                    //        return data.record.unidadeOrganizacional.descricao;
                    //    }
                    //    else {
                    //        return '';
                    //    }
                    //}
                },
                medicoSolicitante: {
                    title: app.localize('MedicoSolicitante'),
                    width: '30%'
                    //display: function (data) {
                    //    return data.record.medicoSolicitante.nomeCompleto;
                    //}
                },
            },
            selectionChanged: function () {
                //Get all selected rows
                var $selectedRows = $('#SolicitacoesExamesTable-' + localStorage["AtendimentoId"]).jtable('selectedRows');
                if ($selectedRows.length > 0) {
                    //Show selected rows
                    $selectedRows.each(function () {
                        var record = $(this).data('record');
                        localStorage["SolicitacaoExameId"] = record.id;
                        $('#codigo-item-' + localStorage["AtendimentoId"]).html("<span>" + app.localize('Codigo') + ": " + zeroEsquerda(record.codigo, '8') + "</span>");
                        $('#medico-item-' + localStorage["AtendimentoId"]).html("<span>" + app.localize('Medico') + ": " + record.medicoSolicitante + "</span>");
                        $('#content-detalhe-solicitacao-exame-' + localStorage["AtendimentoId"]).removeClass('hidden');
                        getDetalharSolicitacoesExames(record.id)
                    });
                }
                else {
                    localStorage.removeItem("SolicitacaoExameId");
                    $('#content-detalhe-solicitacao-exame-' + localStorage["AtendimentoId"]).addClass('hidden');
                }
            }
        });

        $('#DetalharSolicitacaoExameTable-' + localStorage["AtendimentoId"]).jtable({
            title: app.localize('DetalharSolicitacaoExame'),
            paging: true,
            sorting: true,
            multiSorting: true,

            actions: {
                listAction: {
                    method: _SolicitacoesExamesItensService.listarPorSolicitacao
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '20%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissionsItem.edit) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    _createOrEditItemModal.open({ solicitacaoId: localStorage["SolicitacaoExameId"], id: data.record.id });
                                });
                        }
                        if (_permissionsItem.delete) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    deleteSolicitacoesExamesItens(data.record);
                                });
                        }
                        return $span;
                    }
                },
                faturamentoItem: {
                    title: app.localize('Exame'),
                    width: '40%',
                    sorting: false,
                    //display: function (data) {
                    //    if (data.record.faturamentoItem) {
                    //        return data.record.faturamentoItem.descricao;
                    //    }
                    //}
                },
                material: {
                    title: app.localize('Material'),
                    width: '40%',
                    sorting: false,
                    //display: function (data) {
                    //    if (data.record.material) {
                    //        return data.record.material.descricao;
                    //    }
                    //}
                }
            }
        });

        function getDetalharSolicitacoesExames(id) {
            $('#DetalharSolicitacaoExameTable-' + localStorage["AtendimentoId"]).jtable('load', {
                Filtro: id
            });
        }

        function getSolicitacoesExames() {
            $('#SolicitacoesExamesTable-' + localStorage["AtendimentoId"]).jtable('load', createRequestParams());
        }

        function getSolicitacaoExameItens() {
            $('#DetalharSolicitacaoExameTable-' + localStorage["AtendimentoId"]).jtable('load', {
                Filtro: localStorage["SolicitacaoExameId"]
            });
        }

        function deleteSolicitacoesExames(solicitacaoExame) {
            abp.message.confirm(
                app.localize('DeleteWarning', solicitacaoExame.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _SolicitacoesExamesService.excluir(solicitacaoExame)
                            .done(function () {
                                getSolicitacoesExames();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms, _selectedDateRange);
        }

        function deleteSolicitacoesExamesItens(solicitacaoExameItem) {
            abp.message.confirm(
                app.localize('DeleteWarning', solicitacaoExameItem.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _SolicitacoesExamesItensService.excluir(solicitacaoExameItem.id)
                            .done(function () {
                                getSolicitacaoExameItens();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewSolicitacaoExameButton-' + localStorage["AtendimentoId"]).click(function (e) {
            e.preventDefault();
            _createOrEditModal.open({ atendimentoId: localStorage["AtendimentoId"] });
        });

        $('#ExportarSolicitacoesExamesParaExcelButton-' + localStorage["AtendimentoId"]).click(function () {
            _SolicitacoesExamesService
                .listarParaExcel({
                    filtro: $('#SolicitacoesExamesTableFilter-' + localStorage["AtendimentoId"]).val(),
                    maxResultCount: $('span.jtable-page-size-change select').val()
                })
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#RefreshSolicitacaoExameItensListButton-' + localStorage["AtendimentoId"]).click(function (e) {
            e.preventDefault();
            getSolicitacaoExameItens();
        });

        $('#CreateNewSolicitacaoExameItemButton-' + localStorage["AtendimentoId"]).click(function (e) {
            e.preventDefault();
            _createOrEditItemModal.open({ solicitacaoId: localStorage["SolicitacaoExameId"] });
        });

        $('#GetSolicitacoesExamesButton-' + localStorage["AtendimentoId"]).on('click', function (e) {
            e.preventDefault();
            getSolicitacoesExames();
        });

        $('#RefreshSolicitacoesExamesListButton-' + localStorage["AtendimentoId"]).on('click', function (e) {
            e.preventDefault();
            getSolicitacoesExames();
        });

        abp.event.on('app.CriarOuEditarSolicitacaoExameModalSaved', function () {
            getSolicitacoesExames();
        });

        getSolicitacoesExames();

        $('#SolicitacoesExamesTableFilter-' + localStorage["AtendimentoId"]).focus();
    });
})();