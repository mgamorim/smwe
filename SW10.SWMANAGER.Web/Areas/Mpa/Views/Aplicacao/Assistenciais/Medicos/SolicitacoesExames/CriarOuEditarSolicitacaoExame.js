﻿(function ($) {
    app.modals.CriarOuEditarSolicitacaoExameModal = function () {

        var _solicitacoesExamesService = abp.services.app.solicitacaoExame;
        var _solicitacaoExameItensService = abp.services.app.solicitacaoExameItem;
        var _$filterForm = $('#SolicitacoesExamesItensFilterForm-' + localStorage["AtendimentoId"]);

        var $_SolicitacaoExameItensTable = $('#SolicitacaoExameItensTable-' + localStorage["AtendimentoId"]);
        var _modalManager;
        var _$solicitacaoExameInformationForm = null;

        var _createOrEditItemModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Assistenciais/CriarOuEditarSolicitacaoExameItemModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/SolicitacoesExames/CriarOuEditarSolicitacaoExameItem.js',
            modalClass: 'CriarOuEditarSolicitacaoExameItemModal'
        });

        var _selecionarKitExameModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/KitsExames/SelecionarKitExameModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Cadastros/Laboratorios/KitsExames/_SelecionarKitExameItemModal.js',
            modalClass: 'SelecionarKitExameItemModal'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExameItem'),
            edit: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExameItem'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.SolicitacaoExameItem')
        };

        this.init = function (modalManager) {
            _modalManager = modalManager;
            $('.modal-dialog').css({ 'width': '90%', 'max-width': '1100px' });
            $.fn.modal.Constructor.prototype.enforceFocus = function () { };
        };

        this.save = function () {
            _$solicitacaoExameInformationForm = _modalManager.getModal().find('form[name=SolicitacaoExameInformationsForm-' + localStorage["AtendimentoId"] + ']');
            _$solicitacaoExameInformationForm.validate();
            if (!_$solicitacaoExameInformationForm.valid()) {
                return;
            }

            var solicitacaoExame = _$solicitacaoExameInformationForm.serializeFormToObject();

            _modalManager.setBusy(true);
            _solicitacoesExamesService.criarOuEditar(solicitacaoExame)
                 .done(function (data) {
                     $('#codigo-label-' + localStorage["AtendimentoId"]).removeAttr('disabled').val(zeroEsquerda(data.codigo, '8')).attr('disabled', 'disabled');
                     $('#codigo-' + localStorage["AtendimentoId"]).val(data.codigo);
                     $('#id-solicitacao-' + localStorage["AtendimentoId"]).val(data.id);
                     abp.notify.success(app.localize('SavedSuccessfully'));
                     abp.event.trigger('app.CriarOuEditarSolicitacaoExameModalSaved');
                     $('#solicitacao-exame-itens-row-' + localStorage["AtendimentoId"]).removeClass('hidden');
                     $('#RefreshSolicitacaoExameItensListButton-' + localStorage["AtendimentoId"]).trigger('click');
                 })
                .always(function () {
                    _modalManager.setBusy(false);
                });
        };

        $("#medico-solicitante-id-" + localStorage["AtendimentoId"]).select2({
            allowClear: true,
            placeholder: app.localize("SelecioneLista"),
            ajax: {
                url: "/api/services/app/medico/ListarDropdown",
                dataType: 'json',
                delay: 250,
                method: 'Post',
                data: function (params) {
                    if (params.page == undefined)
                        params.page = '1';
                    return {
                        search: params.term,
                        page: params.page,
                        totalPorPagina: 10
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.result.items,
                        pagination: {
                            more: (params.page * 10) < data.result.totalCount
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0
        });

        $("#prioridade-" + localStorage["AtendimentoId"]).select2({
            allowClear: true,
            placeholder: app.localize("SelecioneLista"),
            ajax: {
                url: "/api/services/app/solicitacaoExamePrioridade/ListarDropdown",
                dataType: 'json',
                delay: 250,
                method: 'Post',
                data: function (params) {
                    if (params.page == undefined)
                        params.page = '1';
                    return {
                        search: params.term,
                        page: params.page,
                        totalPorPagina: 10
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.result.items,
                        pagination: {
                            more: (params.page * 10) < data.result.totalCount
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0
        });

        $("#cbo-kits-" + localStorage["AtendimentoId"]).select2({
            allowClear: true,
            placeholder: app.localize("SelecioneLista"),
            ajax: {
                url: "/api/services/app/kitExame/ListarDropdown",
                dataType: 'json',
                delay: 250,
                method: 'Post',
                data: function (params) {
                    if (params.page == undefined)
                        params.page = '1';
                    return {
                        search: params.term,
                        page: params.page,
                        totalPorPagina: 10
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.result.items,
                        pagination: {
                            more: (params.page * 10) < data.result.totalCount
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 0
        }).on('select2:select', function (evt) {
            evt.preventDefault();
            _selecionarKitExameModal.open({ id: $(this).val() });
        });

        $('#SolicitacaoExameItensTable-' + localStorage["AtendimentoId"]).jtable({
            title: app.localize('SolicitacaoExameItem'),
            paging: true,
            sorting: true,
            multiSorting: true,
            actions: {
                listAction: {
                    method: _solicitacaoExameItensService.listar
                }
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '10%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissions.edit) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    _createOrEditItemModal.open({ solicitacaoId: $('#id-solicitacao-' + localStorage["AtendimentoId"]).val(), id: data.record.id });
                                });
                        }
                        if (_permissions.delete) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    deleteSolicitacoesExamesItens(data.record.id);
                                });
                        }
                        return $span;
                    }
                },
                faturamentoItem: {
                    title: app.localize('Exame'),
                    width: '40%',
                    sorting: false,
                    //display: function (data) {
                    //    if (data.record.faturamentoItem) {
                    //        return data.record.faturamentoItem.descricao;
                    //    }
                    //}
                },
                material: {
                    title: app.localize('Material'),
                    width: '40%',
                    sorting: false,
                    //display: function (data) {
                    //    if (data.record.material) {
                    //        return data.record.material.descricao;
                    //    }
                    //}
                },
                guiaNumero: {
                    title: app.localize('GuiaNumero'),
                    width: '10%'
                }
            }
        });

        function getSolicitacaoExameItens() {
            $('#SolicitacaoExameItensTable-' + localStorage["AtendimentoId"]).jtable('load', {
                Filtro: $('#SolicitacaoExameItensTableFilter-' + localStorage["AtendimentoId"]).val(),
                SolicitacaoExameId: $('#id-solicitacao-' + localStorage["AtendimentoId"]).val()
            });
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms, _selectedDateRange);
        }

        $('#CreateNewSolicitacaoExameItemSubButton-' + localStorage["AtendimentoId"]).on('click', function (e) {
            e.preventDefault();
            _createOrEditItemModal.open({ solicitacaoId: $('#id-solicitacao-' + localStorage["AtendimentoId"]).val() });
        });

        $('#ExportarSolicitacaoExameItensParaExcelButton-' + localStorage["AtendimentoId"]).on('click', function (e) {
            e.preventDefault();
            _solicitacaoExameItensService
                .listarParaExcel({
                    filtro: $('#SolicitacoesExamesItensTableFilter').val(),
                    maxResultCount: $('span.jtable-page-size-change select').val()
                })
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#GetSolicitacaoExameItensButton-' + localStorage["AtendimentoId"]).click(function (e) {
            e.preventDefault();
            getSolicitacaoExameItens();
        });

        $('#RefreshSolicitacaoExameItensListButton-' + localStorage["AtendimentoId"]).click(function (e) {
            e.preventDefault();
            getSolicitacaoExameItens();
        });

        abp.event.on('app.CriarOuEditarSolicitacaoExameItemModalSaved', function () {
            getSolicitacaoExameItens();
        });

        abp.event.on('app.SelecionarKitExameItemModalSaved', function () {
            getSolicitacaoExameItens();
        });

        function deleteSolicitacoesExamesItens(solicitacaoExameItem) {
            abp.message.confirm(
                app.localize('DeleteWarning', solicitacaoExameItem.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _solicitacaoExameItensService.excluir(solicitacaoExameItem.id)
                            .done(function () {
                                getSolicitacaoExameItens();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        CamposRequeridos();
        aplicarDateSingle();
        aplicarDateRange();
    };
})(jQuery);