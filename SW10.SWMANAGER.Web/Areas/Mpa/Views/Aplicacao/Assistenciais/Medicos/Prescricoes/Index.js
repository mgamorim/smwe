﻿(function () {
    $(function () {
        var _$PrescricoesTable = $('#PrescricoesTable-' + localStorage["AtendimentoId"]);
        var _prescricaoService = abp.services.app.prescricaoMedica;
        var _$filterForm = $('#PrescricoesFilterForm-' + localStorage["AtendimentoId"]);
        var _selectedDateRange = {
            startDate: moment(localStorage["DataAtendimento"]).startOf('day'),
            endDate: moment().endOf('day')
        };

        $('#date-range-prescricao-' + localStorage["AtendimentoId"]).daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
            });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.Prescricao.Create'),
            edit: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.Prescricao.Edit'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.Prescricao.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Assistenciais/_CriarOuEditarMedicoPrescricao',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/Medicos/Prescricoes/_CriarOuEditar.js',
            modalClass: 'CriarOuEditarPrescricaoModal'
        });

        $('#PrescricoesTable-' + localStorage["AtendimentoId"]).jtable({
            title: app.localize('Prescricao'),
            paging: true,
            sorting: true,
            multiSorting: true,
            selecting: true,
            selectingCheckboxes: true,
            actions: {
                listAction: {
                    method: _prescricaoService.listar
                }
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '20%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissions.edit && data.record.prescricaoStatusId == 1 || data.record.prescricaoStatusId == 2 || data.record.prescricaoStatusId == 6) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    localStorage.removeItem("RespostasList");
                                    localStorage.removeItem("DivisaoId");
                                    localStorage.removeItem("PrescricaoId");
                                    var target = localStorage["TargetConteudo"];
                                    var url = '/Mpa/Assistenciais/_CriarOuEditarMedicoPrescricao?atendimentoid=' + localStorage["AtendimentoId"] + '&id=' + data.record.id;
                                    $('#' + target).data('reload', '0');
                                    LerParcial(target, url, '');
                                });
                        }
                        if (_permissions.delete && (data.record.prescricaoStatusId == 1)) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    deletePrescricoes(data.record);
                                });
                        }
                        //Copiar prescrição
                        if (data.record.prescricaoStatusId == 2 || data.record.prescricaoStatusId == 6) {
                            $('<button class="btn btn-primary btn-xs" title="' + app.localize('CopiarPrescricao') + '"><i class="fa fa-files-o"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    copiarPrescricao(data.record.id);
                                });
                        }
                        //Suspender prescrição
                        if (data.record.prescricaoStatusId == 2 || data.record.prescricaoStatusId == 6) {
                            $('<button class="btn btn-danger btn-xs" title="' + app.localize('SuspenderPrescricao') + '"><i class="fa fa-ban"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    suspenderPrescricao(data.record.id);
                                });
                        }
                        //Liberar prescrição - solicitar exames
                        if (data.record.prescricaoStatusId == 1) {
                            $('<button class="btn btn-success btn-xs" title="' + app.localize('LiberarPrescricao') + '"><i class="fa fa-check-square"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    liberarPrescricao(data.record.id);
                                });
                        }

                        //Aprovar prescrição - solicitar estoque
                        if (data.record.prescricaoStatusId == 2) {
                            $('<button class="btn btn-success btn-xs" title="' + app.localize('AprovarPrescricao') + '"><i class="fa fa-thumbs-o-up"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    aprovarPrescricao(data.record.id);
                                });
                        }

                        //Aprovar prescrição - solicitar estoque
                        if (data.record.prescricaoStatusId == 5) {
                            $('<button class="btn btn-success btn-xs" title="' + app.localize('ReAtivar') + '"><i class="fa fa-thumbs-o-up"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    reativarPrescricao(data.record.id);
                                });
                        }


                        return $span;
                    }
                },
                prescricaoStatusId: {
                    title: app.localize('Status'),
                    width: '5%',
                    display: function (data) {
                        var $span = $('<div></div>');
                        $('<span class="sw-btn-display" style="background-color:' + data.record.statusCor + ';" title="' + data.record.status + '"></span>')
                            .appendTo($span);
                        return $span;
                    }
                },
                codigo: {
                    title: app.localize('Codigo'),
                    width: '10%',
                    display: function (data) {
                        return data.record.id;
                    }
                },
                dataPrescricao: {
                    title: app.localize('DataPrescricao'),
                    width: '15%',
                    display: function (data) {
                        return moment(data.record.dataPrescricao).format('L LT');
                    }
                },
                medico: {
                    title: app.localize('Medico'),
                    width: '25%',
                    display: function (data) {
                        return data.record.medico ? data.record.medico : '';
                    }
                },

            },
            selectionChanged: function () {

                var $selectedRows = $('#PrescricoesTable-' + localStorage["AtendimentoId"]).jtable('selectedRows');

                if ($selectedRows.length > 0) {
                    $('#criarRegistroButton').enable(true);

                    $selectedRows.each(function () {
                        var record = $(this).data('record');
                        exibirRelatorio(record.id);
                    });
                }
            }
        });

        function getPrescricoes() {
            $('#PrescricoesTable-' + localStorage["AtendimentoId"]).jtable('load', createRequestParams());
        }

        function deletePrescricoes(prescricao) {
            abp.message.confirm(
                app.localize('DeleteWarning', prescricao.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _prescricaoService.excluir(prescricao.id)
                            .done(function () {
                                getPrescricoes();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function copiarPrescricao(id) {
            //            abp.message.confirm(
            //                app.localize('CopiarPrescricaoWarning'),
            //                function (isConfirmed) {
            //                    if (isConfirmed) {
            swal({
                title: "Copiar Prescrição",
                text: "Copiar Prescricao",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: '/api/services/app/prescricaoMedica/copiar?id=' + id + '&atendimentoId=' + localStorage["AtendimentoId"],
                    method: 'POST',
                    //data: {
                    //    id: id,
                    //    atendimentoId: localStorage["AtendimentoId"]
                    //},
                    success: function (data) {
                        if (data != null && data != '' && data != undefined) {
                            swal(app.localize('OperacaoConcluida'), data, 'success');
                        }
                        else {
                            abp.notify.success(app.localize('SuccessfullyCopied'));
                        }
                        getPrescricoes();
                    },
                    error: function (request, status, error) {
                        var req = JSON.parse(request.responseText);
                        swal(app.localize('Error'), req.message, 'error');
                    }
                });
                //_prescricaoService.copiar(id, localStorage["AtendimentoId"])
                //    .done(function (data) {
                //        if (data != null && data != '' && data != undefined) {
                //            swal(app.localize('OperacaoConcluida'), data);
                //        }
                //        else {
                //            abp.notify.success(app.localize('SuccessfullyCopied'));
                //        }
                //        getPrescricoes();
                //    });
            });
            //_prescricaoService.copiar(id, localStorage["AtendimentoId"])
            //    .done(function (data) {
            //        if (data != null && data != '' && data != undefined) {
            //            swal(app.localize('OperacaoConcluida'), data);
            //        }
            //        else {
            //            abp.notify.success(app.localize('SuccessfullyCopied'));
            //        }
            //        getPrescricoes();
            //    });
            ////.always(function () {
            //(function () {
            //    abp.ui.clearBusy();
            //});
            //})
            //                    }
            //                }
            //            );
        }

        function suspenderPrescricao(id) {
            swal({
                title: "Suspender Prescrição",
                text: "Suspender Prescricao",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: '/api/services/app/prescricaoMedica/suspender?id=' + id + '&atendimentoId=' + localStorage["AtendimentoId"],
                    method: 'POST',
                    //data: {
                    //    id: id,
                    //    atendimentoId: localStorage["AtendimentoId"]
                    //},
                    success: function (data) {
                        if (data != null && data != '' && data != undefined) {
                            swal(app.localize('OperacaoConcluida'), '', 'success');
                        }
                        else {
                            abp.notify.success(app.localize('SuccessfullySuspended'));
                        }
                        getPrescricoes();
                    },
                    error: function (request, status, error) {
                        var req = JSON.parse(request.responseText);
                        swal(app.localize('Error'), req.message, 'error');
                    }
                });
            });
        }

        function liberarPrescricao(id) {
            swal({
                title: "Liberar Prescrição",
                text: "Liberar Prescrição",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: '/api/services/app/prescricaoMedica/liberar?id=' + id + '&atendimentoId=' + localStorage["AtendimentoId"],
                    method: 'POST',
                    //data: {
                    //    id: id,
                    //    atendimentoId: localStorage["AtendimentoId"]
                    //},
                    success: function (data) {
                        if (data != null && data != '' && data != undefined) {
                            swal(app.localize('OperacaoConcluida'), data, 'success');
                        }
                        else {
                            abp.notify.success(app.localize('SuccessfullyLiberated'));
                        }
                        getPrescricoes();
                    },
                    error: function (request, status, error) {
                        var req = JSON.parse(request.responseText);
                        swal(app.localize('Error'), req.message, 'error');
                    }
                });
            });
        }

        function aprovarPrescricao(id) {
            swal({
                title: "Aprovar Prescrição",
                text: "Aprovar Prescricao",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: '/api/services/app/prescricaoMedica/aprovar?id=' + id + '&atendimentoId=' + localStorage["AtendimentoId"],
                    method: 'POST',
                    //data: {
                    //    id: id,
                    //    atendimentoId: localStorage["AtendimentoId"]
                    //},
                    success: function (data) {
                        if (data != null && data != '' && data != undefined) {
                            swal(app.localize('OperacaoConcluida'), data, 'success');
                        }
                        else {
                            abp.notify.success(app.localize('SuccessfullyCopied'));
                        }
                        getPrescricoes();
                    },
                    error: function (request, status, error) {
                        var req = JSON.parse(request.responseText);
                        swal(app.localize('Error'), req.message, 'error');
                    }
                });
            });
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms, _selectedDateRange);
        }

        function reativarPrescricao(id) {
            swal({
                title: "Reativar Prescrição",
                text: "Reativar Prescricao",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: '/api/services/app/prescricaoMedica/ReAtivar?id=' + id,
                    method: 'POST',
                    //data: {
                    //    id: id,
                    //    atendimentoId: localStorage["AtendimentoId"]
                    //},
                    success: function (data) {

                        if (data != null && data != '' && data != undefined) {
                            swal(app.localize('OperacaoConcluida'), '', 'success');
                        }
                        else {
                            abp.notify.success(app.localize('SuccessfullySuspended'));
                        }
                        getPrescricoes();
                    },
                    error: function (request, status, error) {
                        var req = JSON.parse(request.responseText);
                        swal(app.localize('Error'), req.message, 'error');
                    }
                });
            });
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewPrescricaoButton-' + localStorage["AtendimentoId"]).click(function (e) {
            e.preventDefault();
            localStorage.removeItem("RespostasList");
            localStorage.removeItem("DivisaoId");
            localStorage.removeItem("PrescricaoId");
            var target = localStorage["TargetConteudo"];
            var url = '/Mpa/Assistenciais/_CriarOuEditarMedicoPrescricao?atendimentoid=' + localStorage["AtendimentoId"];
            $('#' + target).data('reload', '0');
            LerParcial(target, url, '');
        });

        $('#ExportarPrescricoesParaExcelButton-' + localStorage["AtendimentoId"]).click(function () {
            _prescricaoService
                .listarParaExcel({
                    filtro: $('#PrescricoesTableFilter-' + localStorage["AtendimentoId"]).val(),
                    maxResultCount: $('span.jtable-page-size-change select').val()
                })
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#GetPrescricoesButton-' + localStorage["AtendimentoId"]).on('click', function (e) {
            e.preventDefault();
            getPrescricoes();
        });

        $('#RefreshPrescricoesListButton-' + localStorage["AtendimentoId"]).on('click', function (e) {
            e.preventDefault();
            getPrescricoes();
        });

        abp.event.on('app.CriarOuEditarPrescricaoModalSaved', function () {
            getPrescricoes();
        });

        abp.event.on('app.CriarOuEditarPrescricaoCompletaModalSaved', function () {
            getPrescricoes();
        });

        getPrescricoes();

        $('#PrescricoesTableFilter-' + localStorage["AtendimentoId"]).focus();

        function exibirRelatorio(prescricaoId) {
            var action = "VisualizarPrescricao?prescricaoId=" + prescricaoId;
            var caminho = "/mpa/AssistenciaisRelatorios/" + action;
            PDFObject.embed(caminho, "#imagemPrescricoes-" + localStorage["AtendimentoId"] );
        }

    });
})();