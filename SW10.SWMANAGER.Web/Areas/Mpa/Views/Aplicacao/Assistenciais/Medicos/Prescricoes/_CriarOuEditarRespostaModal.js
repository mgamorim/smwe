﻿(function ($) {
    app.modals.CriarOuEditarRespostaModal = function () {

        var _prescricoesService = abp.services.app.prescricaoMedica;
        var _prescricaoItemService = abp.services.app.prescricaoItem;

        var _modalManager;
        var _$respostaInformationForm = null;

        this.init = function (modalManager) {
            _modalManager = modalManager;
            _$respostaInformationForm = _modalManager.getModal().find('form[name="FormDivisao"]');
            _$respostaInformationForm.validate();
            $.fn.modal.Constructor.prototype.enforceFocus = function () { };
            //$('.modal-dialog').css({ 'width': '90%', 'max-width': '1100px' });
            CamposRequeridos();
            aplicarDateSingle();
            aplicarDateRange();
            $('.select2').css('width', '100%');
        };

        this.save = function () {
            //if (!_$respostaInformationForm.valid()) {
            //    return;
            //}

            //var resposta = _$respostaInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            //$('button[data-divisao-id="' + localStorage["DivisaoId"] + '"]').trigger('click');

            salvarDivisao();

            //_cidadesService.criarOuEditar(resposta)
            //     .done(function () {
            //         abp.notify.info(app.localize('SavedSuccessfully'));
            //         _modalManager.close();
            //         abp.event.trigger('app.CriarOuEditarRespostaModalSaved');
            //     })
            //    .always(function () {
            _modalManager.setBusy(false);
            abp.notify.info(app.localize('SavedSuccessfully'));
            _modalManager.close();
            abp.event.trigger('app.CriarOuEditarRespostaModalSaved');
            //                 });
        };

        function salvarDivisao() {
            //var _prescricaoForm = $('form[data-atendimento-id="' + localStorage["AtendimentoId"] + '"]'); //$(this).parents('form:first');
            var divisaoForm = $('form[data-divisao-id="' + localStorage["DivisaoId"] + '"]'); //$('form[name="FormDivisao"]');
            divisaoForm.validate();
            if (!divisaoForm.valid()) {
                return;
            }
            //var prescricaoForm = _prescricaoForm.serializeFormToObject();
            var form1 = divisaoForm.serializeFormToObject();
            form1.PrescricaoId = prescricaoForm.Id;
            if (form1.Horarios != null || form1.Horarios != '' || form1.Horarios != "" || form1.Horarios != undefined) {
                var horas = form1.Horarios.split(' ')
                var strHorarios = ''
                for (var i = 0; i < horas.length; i++) {
                    strHorarios += zeroEsquerda($('select[name=HoraDiaId-' + i + ']').val(), 2) + ':00 ';
                }
                var len = strHorarios.length;
                strHorarios = strHorarios.substring(0, len - 1);
                form1.Horarios = strHorarios;
            }
            //localStorage["DivisaoId"] = $(this).data('divisao-id');
            //if ($(this).data('divisao-id') == 0) {
            _prescricaoItemService.obter(form1.PrescricaoItemId)
                .done(function (data) {
                    form1.DivisaoId = data.divisaoId;
                });
            //}

            if ($('#respostas-list-' + localStorage["AtendimentoId"]).val() == '') {
                $('#respostas-list-' + localStorage["AtendimentoId"]).val('[]')
            }
            var lista = JSON.parse($('#respostas-list-' + localStorage["AtendimentoId"]).val());
            if (lista.length > 0) {
                var itemProcessado = false;
                for (var i = 0; i < lista.length; i++) {
                    if (lista[i].IdGridPrescricaoItemResposta == form1.IdGridPrescricaoItemResposta) {
                        lista[i].Quantidade = form1.Quantidade;
                        lista[i].UnidadeId = form1.UnidadeId;
                        lista[i].VelocidadeInfusaoId = form1.VelocidadeInfusaoId;
                        lista[i].FormaAplicacaoId = form1.FormaAplicacaoId;
                        lista[i].FrequenciaId = form1.FrequenciaId;
                        lista[i].IsSeNecessario = form1.IsSeNecessario;
                        lista[i].IsUrgente = form1.IsUrgente;
                        lista[i].UnidadeOrganizacionalId = form1.UnidadeOrganizacionalId;
                        lista[i].MedicoId = form1.MedicoId;
                        lista[i].DataInicial = form1.DataInicial;
                        lista[i].TotalDias = form1.TotalDias;
                        lista[i].Observacao = form1.Observacao;
                        lista[i].PrescricaoItemId = form1.PrescricaoItemId;
                        lista[i].DivisaoId = form1.DivisaoId;
                        lista[i].PrescricaoId = form1.PrescricaoId;
                        lista[i].Horarios = form1.Horarios;
                        itemProcessado = true;
                        break;
                    }
                }
                if (!itemProcessado) {
                    form1.IdGridPrescricaoItemResposta = lista.length == 0 ? 1 : lista[lista.length - 1].IdGridPrescricaoItemResposta + 1;
                    form1.PrescricaoId = $('#id-' + localStorage["AtendimentoId"]).val();
                    lista.push(form1);
                }
            }
            else {
                form1.IdGridPrescricaoItemResposta = lista.length == 0 ? 1 : lista[lista.length - 1].IdGridPrescricaoItemResposta + 1;
                form1.PrescricaoId = $('#id-' + localStorage["AtendimentoId"]).val();
                lista.push(form1);
            }
            var strList = JSON.stringify(lista);
            $('#respostas-list-' + localStorage["AtendimentoId"]).val(JSON.stringify(lista));
            abp.notify.info(app.localize('ListaAtualizada'));
            abp.event.trigger('app.CriarOuEditarPrescricaoModalSaved');
        }

        $('.input-group-addon').on('click', function (e) {
            e.preventDefault();
            var id = $('#frequencia-id-' + localStorage["AtendimentoId"] + '-modal').val();
            if (id && id != null && id != undefined && id != '') {
                _frequenciaService.obter(id)
                .done(function (data) {
                    $('#horarios-' + localStorage["AtendimentoId"] + '-modal')
                        .val(definirHorarios(
                            data.intervalo,
                            $('#hora-inicial-' + localStorage["AtendimentoId"] + '-modal').val()
                        ));
                    criarComboHora();
                });
                //aplicarSelect2Padrao();
                //$('.select2').css('width', '100%');
            }
        });

    };
})(jQuery);