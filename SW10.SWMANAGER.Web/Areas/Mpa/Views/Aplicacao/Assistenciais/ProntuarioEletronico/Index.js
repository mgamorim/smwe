﻿(function () {
    $(function () {
        var _$ProntuarioEletronicoTable = $('#ProntuarioEletronicoTable-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]);
        console.log('Tabela: ' + $('#ProntuarioEletronicoTable-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]));
        console.log('Ate/Ope: ' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]);
        var _ProntuariosEletronicosService = abp.services.app.prontuarioEletronico;
        //var _prescricaoMedicaService = abp.services.app.prescricaoMedica;
        var _$filterForm = $('#ProntuariosEletronicosFilterForm-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]);

        var _selectedDateRange = {
            startDate: moment().startOf('day'),
            endDate: moment().endOf('day')
        };

        _$filterForm.find('.date-range-picker').daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
            });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.Admissao'),
            edit: abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.Admissao'),
            'delete': abp.auth.hasPermission('Pages.Tenant.Assistencial.AmbulatorioEmergencia.Medico.Admissao')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/ProntuariosEletronicos/CriarOuEditarProntuarioEletronico',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/ProntuarioEletronico/CriarOuEditarProntuarioEletronicoViewModel.js',
            modalClass: 'CriarOuEditarProntuarioEletronicoModal'
        });

        _$ProntuarioEletronicoTable.jtable({
            //title: app.localize('ProntuarioEletronico'),
            paging: true,
            sorting: true,
            multiSorting: true,
            actions: {
                listAction: {
                    method: _ProntuariosEletronicosService.listar
                }
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),
                    width: '10%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissions.edit) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function (e) {
                                    e.preventDefault();
                                    if (data.record.formRespostaId && data.record.formRespostaId > 0) {
                                        url = '/Mpa/GeradorFormularios/_EditarPreenchimento';//'@(Url.Action("_EditarPreenchimento","GeradorFormularios"))';
                                        url += '?nomeClasse=ProntuarioEletronico';
                                        url += '&formRespostaId=' + data.record.formRespostaId; //$('#form-resposta-id').val();
                                        url += '&registroClasseId=' + data.record.id;
                                        url += '&atendimentoId=' + localStorage["AtendimentoId"]; // necessario para popular adequadamente campos reservados dos forms.dinamicos
                                        $('#formulario-dinamico-area-' + localStorage["AtendimentoId"]).attr('src', url);
                                        $('#menu-modulo-' + localStorage["AtendimentoId"]).addClass('hidden');
                                        $('#conteudo-modulo-' + localStorage["AtendimentoId"]).removeClass('hidden');
                                    }
                                    else {
                                        _createOrEditModal.open({ id: data.record.id, operacaoId: sessionStorage["OperacaoId"], atendimentoId: localStorage["AtendimentoId"] });
                                    }
                                    //$('#CriarOuEditarProntuarioEletronicoArea').html('');
                                    //$('#FormularioDinamicoArea').html('');
                                    //LerParcial('CriarOuEditarProntuarioEletronicoArea', '/mpa/Assistenciais/CriarOuEditarProntuarioEletronico/' + data.record.id);
                                });
                        }
                        if (_permissions.delete) {
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    deleteProntuariosEletronicos(data.record.id);
                                });
                        }
                        return $span;
                    }
                },
                dataAdmissao: {
                    title: app.localize('Data'),
                    width: '10%',
                    display: function (data) {
                        return moment(data.record.dataAdmissao).format('L LT');
                    }
                },
                codigoAtendimento: {
                    title: app.localize('Atendimento'),
                    width: '10%',
                },
                paciente: {
                    title: app.localize('Paciente'),
                    width: '15%',
                    //display: function (data) {
                    //    return app.localize(data.record.operacao.descricao);
                    //}
                },
                medico: {
                    title: app.localize('Medico'),
                    width: '15%',
                    //display: function (data) {
                    //    return app.localize(data.record.operacao.descricao);
                    //}
                },
                unidadeOrganizacional: {
                    title: app.localize('UnidadeOrganizacional'),
                    width: '15%',
                    //display: function (data) {
                    //    return app.localize(data.record.operacao.descricao);
                    //}
                },
                empresa: {
                    title: app.localize('Empresa'),
                    width: '15%',
                    //display: function (data) {
                    //    return app.localize(data.record.operacao.descricao);
                    //}
                },
                formulario: {
                    title: app.localize('Formulário'),
                    width: '20%',
                    //display: function (data) {
                    //    return app.localize(data.record.operacao.descricao);
                    //}
                },
            }
        });

        function getProntuariosEletronicos() {
            _$ProntuarioEletronicoTable.jtable('load', createRequestParams());
        }

        function deleteProntuariosEletronicos(prontuarioEletronico) {
            abp.message.confirm(
                app.localize('DeleteWarning', prontuarioEletronico.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _ProntuariosEletronicosService.excluir(prontuarioEletronico)
                            .done(function () {
                                getProntuariosEletronicos(true);
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms, _selectedDateRange);
        }

        //$('#ShowAdvancedFiltersSpan-' + localStorage["AtendimentoId"]).click(function () {
        //    $('#ShowAdvancedFiltersSpan-' + localStorage["AtendimentoId"]).hide();
        //    $('#HideAdvancedFiltersSpan-' + localStorage["AtendimentoId"]).show();
        //    $('#AdvacedAuditFiltersArea-' + localStorage["AtendimentoId"]).slideDown();
        //});
        //$('#HideAdvancedFiltersSpan-' + localStorage["AtendimentoId"]).click(function () {
        //    $('#HideAdvancedFiltersSpan-' + localStorage["AtendimentoId"]).hide();
        //    $('#ShowAdvancedFiltersSpan-' + localStorage["AtendimentoId"]).show();
        //    $('#AdvacedAuditFiltersArea-' + localStorage["AtendimentoId"]).slideUp();
        //});

        $('#CreateNewProntuarioEletronicoButton-' + +localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]).click(function (e) {
            e.preventDefault();
            _createOrEditModal.open({ operacaoId: sessionStorage["OperacaoId"], atendimentoId: localStorage["AtendimentoId"] });
        });

        $('#ExportarProntuariosEletronicosParaExcelButton-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]).click(function () {
            _ProntuariosEletronicosService
                .listarParaExcel({
                    filtro: $('#ProntuariosEletronicosTableFilter-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]).val(),
                    maxResultCount: $('span.jtable-page-size-change select').val()
                })
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#GetProntuariosEletronicosButton-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]).click(function (e) {
            e.preventDefault();
            getProntuariosEletronicos();
        });

        $('#RefreshProntuariosEletronicosListButton-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]).click(function (e) {
            e.preventDefault();
            getProntuariosEletronicos();
        });

        abp.event.on('app.CriarOuEditarProntuarioEletronicoModalSaved', function () {
            getProntuariosEletronicos();
        });

        getProntuariosEletronicos();

        $('#ProntuariosEletronicosTableFilter-' + localStorage["AtendimentoId"] + '-' + sessionStorage["OperacaoId"]).focus();
    });
})();
