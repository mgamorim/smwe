﻿(function ($) {


    app.modals.ListarRegistroArquivos = function () {
        var _modalManager;


        $('.modal-dialog').css('width', '1300px');
        


        this.init = function (modalManager) {
            _modalManager = modalManager;

        }


        var _$registrosArquivosTable = $('#registrosArquivosTable');

        var _atendimentosService = abp.services.app.registroArquivo;

        _$registrosArquivosTable.jtable({

            title: app.localize('Imagens'),
            paging: true,
            sorting: true,
            multiSorting: true,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column

            actions: {
                listAction: {
                    method: _atendimentosService.listarPorAtendimento, 
                    

                }

            },

            fields: {
                id: {
                    key: true,
                    list: false
                },

                //actions: {
                //    title: app.localize('Actions'),
                //    width: '6%',
                //    sorting: false,
                //    display: function (data) {
                //        var $span = $('<span></span>');
                //        $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                //            .appendTo($span)
                //            .click(function () {
                //                _registrosArquivos.open({ id: data.record.id });
                //                //location.href = '/Assistenciais/ListarRegistroArquivos/' + data.record.id
                //            });

                //        return $span;
                //    }
                //},


                OperacaoDescricao: {
                    title: app.localize('Operacao'),
                    width: '10%',
                    display: function (data) {
                        return data.record.operacaoDescricao;
                    }

                    

                },

                data: {
                    title: app.localize('Data'),
                    width: '10%',
                    display: function (data) {

                        return moment(data.record.dataRegistro).format('L HH:mm');
                    }
                },
            },


            recordsLoaded : function (event, data) {
    
                $('#registrosArquivosTable .jtable-main-container tr.jtable-data-row:first input[type=checkbox]').trigger('click');
            },



            selectionChanged: function () {

               
                // $('#divImg').setBusy(true);
                
                

                var $selectedRows = _$registrosArquivosTable.jtable('selectedRows');

                if ($selectedRows.length > 0) {
                    // $('#criarRegistroButton').enable(true);

                    $selectedRows.each(function () {
                        var record = $(this).data('record');
                        exibirImagemRegistroArquivo(record);
                    });
                }
               
               
            }


        })



        function getRegistrosArquivos() {

            _$registrosArquivosTable.jtable('load', { atendimentoId: $('#atendimentoDoRegistroId').val() });
            
           

            
        }

        getRegistrosArquivos();



        function exibirImagemRegistroArquivo(record) {
           
            if (record.isPDF) {
                $('#imagemRegistroArquivo').hide();
                $('#divPDF').show();

                caminho = "/Mpa/ResultadoLaboratorio/VisualizarPorId?id=" + record.registroId;
                PDFObject.embed(caminho, "#divPDF");
            }
            else
            {
                $('#divPDF').hide();
                $('#imagemRegistroArquivo').show();

                var caminho = "/mpa/Assistenciais/VisualizarImagemRegistroArquivo/" + record.registroId;
                $.ajax({
                    url: caminho,
                    type: 'POST',

                    beforeSend: function () {
                        abp.ui.setBusy('#divImg');
                    },

                    success: function (data) {

                        //var imgSrc = "data:image/png;base64," + response; //, Model.FotoMimeType, base64);

                      //  if (data.isPDF) {
                       //     PDFObject.embed(data.file, "#divImg");
                        //}
                       // else {
                            $('#imagemRegistroArquivo').attr('src', data);
                            abp.ui.clearBusy('#divImg');
                      //  }
                    }
                });
            }
        }

        
        

        
        
    };

    



})(jQuery);