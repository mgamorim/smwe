﻿(function () {
    $(function () {
        var _$AssistencialAtendimentosTable = $('#AssistencialAtendimentosTable-' + localStorage["TipoAtendimento"]);
        //var _atendimentosService = abp.services.app.assistencialAtendimento;
        var _atendimentosService = abp.services.app.atendimento;
        var _$filterForm = $('#AssistencialAtendimentosFilterForm-' + localStorage["TipoAtendimento"]);
        var _terminalSenhasService = abp.services.app.terminalSenhas;
        var _senhasService = abp.services.app.senha;
        var msg = '';

        var _selectedDateRange = {
            startDate: moment().startOf('day'),
            endDate: moment().endOf('day')
        };

        $('#date-field-' + localStorage["TipoAtendimento"]).daterangepicker(
            $.extend(true, app.createDateRangePickerOptions(), _selectedDateRange),
            function (start, end, label) {
                _selectedDateRange.startDate = start.format('YYYY-MM-DDT00:00:00Z');
                _selectedDateRange.endDate = end.format('YYYY-MM-DDT23:59:59.999Z');
            });

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Assistenciais/CriarOuEditarAssistencialAtendimentoModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/Home/_CriarOuEditar.js',
            modalClass: 'CriarOuEditarAssistencialAtendimentoModal'
        });

        var _atendimentoModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Atendimentos/CriarOuEditarModal',
            //scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Atendimentos/Home/_CriarOuEditar.js',
            modalClass: 'CriarOuEditarAtendimentoModal'
        });


        var _registrosArquivos = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Assistenciais/ListarRegistroArquivos',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Assistenciais/RegistrosArquivos/Index.js',
            modalClass: 'ListarRegistroArquivos'
        });
        var _modalAlta = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/AtendimentoLeitoMov/_AltaModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Aplicacao/Atendimentos/Altas/Alta/_CriarOuEditarModal.js',
            modalClass: 'AltaModalViewModel'
        });


        

        _$AssistencialAtendimentosTable.jtable({

            title: app.localize('Atendimentos'),
            paging: true,
            sorting: true,
            multiSorting: true,
            selecting: false, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column

            actions: {
                listAction: {
                    method: localStorage["TipoAtendimento"] === "amb" ? _atendimentosService.listarFiltro : _atendimentosService.listarFiltroInternacao
                }
            },

            fields: {
                id: {
                    key: true,
                    list: false
                },

                actions: {
                    title: app.localize('Actions'),
                    width: '15%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');


                        $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                .appendTo($span)
                                .click(function () {

                                    //var $selectedRows = $('#AssistencialAtendimentosTable-' + localStorage["TipoAtendimento"]).jtable('selectedRows');
                                    //var record = $('#AssistencialAtendimentosTable-' + localStorage["TipoAtendimento"]).jtable('registroSelecionado');
                                    criarAba(data.record);
                                    $('.jtable-row-selected').removeClass('jtable-row-selected');
                                    $(':checked').removeAttr('checked');


                                });

                        $('<button class="btn btn-default btn-xs" title="' + "PEP - Prontuário Eletrônico do Paciente" + '"><i class="fa fa-tasks"></i></button>')
                                .appendTo($span)
                                .click(function () {
                                    _registrosArquivos.open({ id: data.record.id });
                                    
                                });

                            //if (_permissionsInternacao.alta && (!data.record.dataAlta || data.record.dataAlta == null || data.record.dataAlta == undefined || data.record.dataAlta == '') && data.record.atendimentoMotivoCancelamentoId == null)
                            //{
                                $('<button class="btn btn-default btn-xs" title="' + app.localize('Alta') + '"><i class="fa fa-blind fa-3"></i></button>')
                                    .appendTo($span)
                                    .click(function () {
                                        //e.preventDefault();
                                        _modalAlta.open({ atendimentoId: data.record.id });
                                    });
                            //}


                        return $span;
                    }
                },


                codigoAtendimento: {
                    title: app.localize('Codigo'),
                    width: '5%',
                    display: function (data) {
                        var ans = '';
                        if (data.record.codigoAtendimento !== null && data.record.codigoAtendimento !== '' && data.record.codigoAtendimento !== undefined) {
                            ans = zeroEsquerda(data.record.codigoAtendimento, '10');
                        }
                        return ans;
                    }
                },
                dataRegistro: {
                    title: app.localize('DataAtendimento'),
                    width: '8%',
                    display: function (data) {
                        return data.record.dataRegistro !== '' ? moment(data.record.dataRegistro).format('L LT') : '';
                    }
                },
                pacienteId: {
                    title: app.localize('Paciente'),
                    width: '20%',
                    display: function (data) {
                        var nome = ''
                        if (data.record.paciente !== null) {
                            nome = data.record.paciente;
                        }
                        return nome;
                    }
                },
                medicoId: {
                    title: app.localize('Medico'),
                    width: '20%',
                    display: function (data) {
                        var nome = ''
                        if (data.record.medico !== null) {
                            nome = data.record.medico;
                        }
                        return nome;
                    }
                },
                convenioId: {
                    title: app.localize('Convenio'),
                    width: '8%',
                    display: function (data) {
                        var nome = ''
                        if (data.record.convenio !== null) {
                            nome = data.record.convenio;
                        }
                        return nome;
                    }
                },
                empresaId: {
                    title: app.localize('Empresa'),
                    width: '10%',
                    display: function (data) {
                        var nome = ''
                        if (data.record.empresa !== null) {
                            nome = data.record.empresa;
                        }
                        return nome;
                    }
                },
                unidadeId: {
                    title: app.localize('Unidade'),
                    width: '10%',
                    display: function (data) {
                        var nome = ''
                        if (data.record.unidade !== null) {
                            nome = data.record.unidade;
                        }
                        return nome;
                    }
                },
                leitoId: {
                    title: app.localize('Leito'),
                    width: '10%',
                    display: function (data) {
                        var nome = ''
                        if (data.record.leito !== null) {
                            nome = data.record.leito;
                        }
                        return nome;
                    }
                },
                isControlaTev: {
                    list: true, //$(this).data('record').isControlaTev,
                    title: app.localize('Tev'),
                    width: '10%',
                    display: function (data) {
                        var span;
                        if (data.record.isControlaTev) {
                            abp.services.app.tevMovimento.obterUltimo(data.record.id, { async: false })
                            .done(function (record) {
                                if (record != null && record != '' && record != undefined) {
                                    var dd = moment()._d;
                                    var strD = dd.getFullYear() + "-" + zeroEsquerda(dd.getMonth() + 1, 2) + "-" + zeroEsquerda(dd.getDate(), 2) + "T00:00:00";
                                    var date1 = new Date(record.data);
                                    var date2 = new Date(strD);
                                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                    if (record.risco.codigo > 4) {
                                        msg += 'O paciente ' + data.record.paciente + ' possui um alto grau de risco segundo o protocolo TEV\n';
                                        span = '<span class="label label-danger blink" title="Atenção, o paciente ' + data.record.paciente + ' possui um alto grau de risco segundo o protocolo TEV">' + app.localize('Yes') + '</span>';
                                    }
                                    else if (diffDays >= 2) {
                                        msg += 'O paciente ' + data.record.paciente + ' está com sua conferência atrasada segundo o protocolo TEV\n';
                                        span = '<span class="label label-danger blink" title="Atenção, o paciente ' + data.record.paciente + ' está com sua conferência atrasada segundo o protocolo TEV">' + app.localize('Yes') + '</span>';
                                    }
                                    else if (diffDays == 1) {
                                        span = '<span class="label label-warning">' + app.localize('Yes') + '</span>';
                                    }
                                    else {
                                        span = '<span class="label label-success">' + app.localize('Yes') + '</span>';
                                    }
                                }
                                else {
                                    msg += 'Não há registro de avaliação de risco Tev para o paciente ' + data.record.paciente + '. Favor realizar a avaliação.\n';
                                    span = '<span class="label label-danger blink" title="Atenção, não há registro de avaliação de risco Tev para o paciente ' + data.record.paciente + '. Favor realizar a avaliação.">' + app.localize('Yes') + '</span>';

                                }
                            });
                            return span;
                        }
                        else {
                            return '<span class="label label-info">' + app.localize('No') + '</span>';
                        }
                    }
                }
            },
            selectionChanged: function () {
                //Get all selected rows
                //var $selectedRows = $('#AssistencialAtendimentosTable-' + localStorage["TipoAtendimento"]).jtable('selectedRows');
                var record = $('#AssistencialAtendimentosTable-' + localStorage["TipoAtendimento"]).jtable('registroSelecionado');
                criarAba(record);
                $('.jtable-row-selected').removeClass('jtable-row-selected');
                $(':checked').removeAttr('checked');
                //getAssistencialAtendimentos();
            },
            recordsLoaded: function (event, data) {
                if (msg.length > 0) {
                    //alertSw('Controle TEV', msg, 'warning');
                    alert('Controle TEV', msg, 'warning');
                    msg = '';
                }
            }
        });

        function criarAba(record) {
            if (record !== null && record !== undefined) {
                localStorage["AtendimentoId"] = record.id;
                localStorage["DataAtendimento"] = moment(record.dataRegistro);
                //$('#abas-' + localStorage["TipoAtendimento"] + ' li').removeClass('active');
                //$('#conteudo-abas-' + localStorage["TipoAtendimento"] + ' div.tab-pane').removeClass('active');
                if ($('#atendimento-' + record.id).length === 0) {
                    var ans = record.codigoAtendimento !== null ? zeroEsquerda(record.codigoAtendimento, '10') : '';
                    //$('<li id="atendimento-' + record.id + '" name="Atendimento-' + record.id + '" class="active"><a id="link-atendimento-' + record.id + '" href="#atendimento-' + record.id + '" data-toggle="tab" title="' + record.paciente + '" onclick="lerAtendimentoAmbulatorioEmergencia(' + record.id + ',' + "'assistencial', '" + localStorage["TipoAtendimento"] + "');" + '">' + ans + ' - ' + record.paciente + ' <i class="fa fa-close"></i></a></li>')
                    $('<li id="atendimento-' + record.id + '" name="Atendimento-' + record.id + '"><a id="link-atendimento-' + record.id + '" href="#conteudo-atendimento-' + record.id + '" data-toggle="tab" title="' + record.paciente + '" onclick="atualizarAtendimento(' + "'" + record.id + "'" + ');">' + ans + ' - ' + record.paciente + '<i class="fa fa-close"></i></a></li>')
                        .appendTo('#abas-' + localStorage["TipoAtendimento"]);

                    $('<div class="tab-pane" id="conteudo-atendimento-' + record.id + '" style="padding:5px;">').appendTo('#conteudo-abas-' + localStorage["TipoAtendimento"]).load('/mpa/assistenciais/_leratendimento/' + record.id); //, function () {
                    //});
                    $('#link-atendimento-' + record.id).trigger('click');
                } else {
                    $('#link-atendimento-' + record.id).trigger('click');
                }
            }
        }

        function criarAbaAte(record) {
            if (record !== null && record !== undefined) {
                localStorage["AtendimentoId"] = record.id;
                localStorage["DataAtendimento"] = moment(record.dataRegistro);
                //$('#abas-' + localStorage["TipoAtendimento"] + ' li').removeClass('active');
                //$('#conteudo-abas-' + localStorage["TipoAtendimento"] + ' div.tab-pane').removeClass('active');
                if ($('#atendimento-' + record.id).length === 0) {
                    var ans = record.codigo !== null ? zeroEsquerda(record.codigo, '10') : '';
                    //$('<li id="atendimento-' + record.id + '" name="Atendimento-' + record.id + '" class="active"><a id="link-atendimento-' + record.id + '" href="#atendimento-' + record.id + '" data-toggle="tab" title="' + record.paciente + '" onclick="lerAtendimentoAmbulatorioEmergencia(' + record.id + ',' + "'assistencial', '" + localStorage["TipoAtendimento"] + "');" + '">' + ans + ' - ' + record.paciente + ' <i class="fa fa-close"></i></a></li>')
                    $('<li id="atendimento-' + record.id + '" name="Atendimento-' + record.id + '"><a id="link-atendimento-' + record.id + '" href="#conteudo-atendimento-' + record.id + '" data-toggle="tab" title="' + record.paciente.nomeCompleto + '" onclick="atualizarAtendimento(' + "'" + record.id + "'" + ');">' + ans + ' - ' + record.paciente.nomeCompleto + '<i class="fa fa-close"></i></a></li>')
                        .appendTo('#abas-' + localStorage["TipoAtendimento"]);

                    $('<div class="tab-pane" id="conteudo-atendimento-' + record.id + '" style="padding:5px;">').appendTo('#conteudo-abas-' + localStorage["TipoAtendimento"]).load('/mpa/assistenciais/_leratendimento/' + record.id); //, function () {
                    //});
                    $('#link-atendimento-' + record.id).trigger('click');
                } else {
                    $('#link-atendimento-' + record.id).trigger('click');
                }
            }
        }

        function getAssistencialAtendimentos() {
            _$AssistencialAtendimentosTable.jtable('load', createRequestParams());
        }

        function getAtendimentos() {
            _$AssistencialAtendimentosTable.jtable('load', { Filtro: $('#paciente-id-' + localStorage["TipoAtendimento"]).val() });
        }

        function deleteAssistencialAtendimentos(assistencialAtendimento) {

            abp.message.confirm(
                app.localize('DeleteWarning', assistencialAtendimento.descricao),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _atendimentosService.excluir(assistencialAtendimento)
                            .done(function () {
                                getAssistencialAtendimentos();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                    }
                }
            );
        }

        function createRequestParams() {
            var prms = {};
            _$filterForm.serializeArray().map(function (x) { prms[x.name] = x.value; });
            return $.extend(prms, _selectedDateRange);
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAssistencialAtendimentosFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAssistencialAtendimentosFiltersArea').slideUp();
        });

        $('#CreateNewAssistencialAtendimentoButton-' + localStorage["TipoAtendimento"]).click(function () {
            _createOrEditModal.open();
        });

        $('#ExportarAssistencialAtendimentosParaExcelButton').click(function () {
            _atendimentosService
                .listarParaExcel({
                    filtro: $('#AssistencialAtendimentosTableFilter-' + localStorage["TipoAtendimento"]).val(),
                    //sorting: $(''),
                    maxResultCount: $('span.jtable-page-size-change select').val()
                })
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        $('#tipo-filtro-data-' + localStorage["TipoAtendimento"]).on('change', function (e) {
            e.preventDefault();
            $('#filtro-data-' + localStorage["TipoAtendimento"]).val($(this).val())
            switch ($(this).val()) {
                case "Atendimento":
                    $('#date-field-area-' + localStorage["TipoAtendimento"]).removeClass('hidden');
                    break;
                case "Internado":
                    $('#date-field-area-' + localStorage["TipoAtendimento"]).addClass('hidden');
                    break;
                default:
                    $('#date-field-area-' + localStorage["TipoAtendimento"]).removeClass('hidden');
            }
        });

        $('#GetAssistencialAtendimentosButton-' + localStorage["TipoAtendimento"]).click(function (e) {
            e.preventDefault();
            getAssistencialAtendimentos();
        });

        $('#RefreshAssistencialAtendimentosButton-' + localStorage["TipoAtendimento"]).click(function (e) {
            e.preventDefault();
            getAssistencialAtendimentos();
        });

        abp.event.on('app.CriarOuEditarAssistencialAtendimentoModalSaved', function () {
            getAssistencialAtendimentos();
        });

        abp.event.on('app.NovoTevMovimentoModalSaved', function () {
            getAssistencialAtendimentos();
        });

        getAssistencialAtendimentos();

        $('#AssistencialAtendimentosTableFilter-' + localStorage["TipoAtendimento"]).focus();

        $('body').addClass('page-sidebar-closed');

        $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');

        if (localStorage["TipoAtendimento"] === "int") {
            $('#tipo-filtro-data-' + localStorage["TipoAtendimento"]).val("Internado").change();
        }

        aplicarSelect2Padrao();

        selectSW('.selectTipoLocalChamada', "/api/services/app/TipoLocalChamada/ListarTipoLocalChamadaDropdown");
        selectSW('.selectLocalChamada', "/api/services/app/LocalChamadas/ListarLocalChamadaPorTipoDropdown", $('#tipo-local-chamada-id'));
        selectSW('.selectSenha', "/api/services/app/Senha/ListarSenhasPorlocalChamadaDropdown", $('#local-chamada-id'));

        $('#tipo-local-chamada-id').on('change', function (e) {
            e.preventDefault();
            $('#local-chamada-id').val('').trigger('change');
            selectSW('.selectLocalChamada', "/api/services/app/LocalChamadas/ListarLocalChamadaPorTipoDropdown", $('#tipo-local-chamada-id'));
        });

        $('#local-chamada-id').on('change', function (e) {
            e.preventDefault();
            selectSW('.selectSenha', "/api/services/app/Senha/ListarSenhasPorlocalChamadaDropdown", $('#local-chamada-id'));
            $('#movimentacao-senha-id').val('').trigger('change');
            $.cookie('localChamada', $('#local-chamada-id').val());
        });

        $('#movimentacao-senha-id').on('change', function (e) {
            e.preventDefault();
            if ($(this).val() !== null && $(this).val() !== '' && $(this).val() !== undefined) {
                _senhasService.obterMovimento($(this).val())
                .done(function (data) {
                    _senhasService.obter(data.senhaId)
                    .done(function (senha) {
                        _atendimentosService.obter(senha.atendimentoId)
                        .done(function (atendimento) {
                            criarAbaAte(atendimento);
                        });
                    })
                });
            }
        });

        $('#senha-btn').on('click', function (e) {
            e.preventDefault();
            _terminalSenhasService.chamarSenha($('#tipo-local-chamada-id').val(), $('#local-chamada-id').val(), $('#movimentacao-senha-id').val());
            $.cookie('localChamada', $('#local-chamada-id').val());
        });

        $('.select2').css('width', '100%');


       







    });
})();