﻿(function () {
    $(function () {

        var _terminalSenhasService = abp.services.app.terminalSenhas;



        $('.fila-painel').each(function () {
            $(this).click(function (e) {
                e.preventDefault();

                _terminalSenhasService.gerarSenha(this.id)
                                      .done(function (result) {
                                           $.ajax({
                                              url: 'TerminalSenhas/ImprimirSenha',
                                              data: {
                                                  tipoLocalChamada: result.tipoLocalChamada,
                                                  numero: result.numeroSenha,
                                                  hospital: result.hospital
                                              }
                                          }).done(function () {

                                          });

                                      });
            });
        });
    });
})();

