﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.Dto
{
    public class ErroDto
    {
        public string CodigoErro { get; set; }
        public string Descricao { get; set; }
        public List<object> Parametros { get; set; }
    }
}
