﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SW10.SWMANAGER.Sessions.Dto;

namespace SW10.SWMANAGER.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
