﻿using Abp.Application.Services.Dto;

namespace SW10.SWMANAGER.Caching.Dto
{
    public class CacheDto
    {
        public string Name { get; set; }
    }
}
