using System.Collections.Generic;
using SW10.SWMANAGER.Authorization.Users.Dto;
using SW10.SWMANAGER.Dto;

namespace SW10.SWMANAGER.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}