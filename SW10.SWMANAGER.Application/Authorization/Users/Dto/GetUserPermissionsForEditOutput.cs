﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Authorization.Permissions.Dto;

namespace SW10.SWMANAGER.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}