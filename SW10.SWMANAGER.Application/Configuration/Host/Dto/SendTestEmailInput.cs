﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Authorization.Users;

namespace SW10.SWMANAGER.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}