﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SW10.SWMANAGER.Configuration.Host.Dto;

namespace SW10.SWMANAGER.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
