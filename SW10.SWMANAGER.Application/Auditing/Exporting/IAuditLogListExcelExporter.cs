﻿using System.Collections.Generic;
using SW10.SWMANAGER.Auditing.Dto;
using SW10.SWMANAGER.Dto;

namespace SW10.SWMANAGER.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
