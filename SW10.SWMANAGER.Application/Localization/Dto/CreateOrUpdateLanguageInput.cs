﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace SW10.SWMANAGER.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}