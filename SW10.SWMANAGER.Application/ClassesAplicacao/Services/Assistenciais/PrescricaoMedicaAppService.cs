﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public class PrescricaoMedicaAppService : SWMANAGERAppServiceBase, IPrescricaoMedicaAppService
    {
        private readonly IRepository<PrescricaoMedica, long> _prescricaoMedicaRepository;
        private readonly IRepository<PrescricaoItemResposta, long> _prescricaoItemRespostaRepository;
        private readonly IRepository<PrescricaoItemHora, long> _prescricaoItemHoraRepositorio;
        private readonly IPrescricaoItemAppService _prescricaoItemAppService;
        private readonly IPrescricaoItemStatusAppService _prescricaoItemStatusAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IPrescricaoItemRespostaAppService _prescricaoItemRespostaAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDivisaoAppService _divisaoAppService;
        private readonly IFormaAplicacaoAppService _formaAplicacaoAppService;
        private readonly IFrequenciaAppService _frequenciaAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly IVelocidadeInfusaoAppService _velocidadeInfusaoAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly ISolicitacaoExameAppService _solicitacaoExameAppService;
        private readonly IEstoquePreMovimentoAppService _estoquePreMovimentoAppService;
        private readonly IFaturamentoItemAppService _fatItemAppService;
        private readonly IFormulaEstoqueAppService _formulaEstoqueAppService;
        private readonly IFormulaEstoqueKitAppService _formulaEstoqueKitAppService;


        public PrescricaoMedicaAppService(
            IRepository<PrescricaoMedica, long> prescricaoMedicaRepository,
            IRepository<PrescricaoItemResposta, long> prescricaoItemRespostaRepository,
            IRepository<PrescricaoItemHora, long> prescricaoItemHoraRepositorio,
            IAtendimentoAppService atendimentoAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IPrescricaoItemAppService prescricaoItemAppService,
            IPrescricaoItemStatusAppService prescricaoItemStatusAppService,
            IPrescricaoItemRespostaAppService prescricaoItemRespostaAppService,
            IDivisaoAppService divisaoAppService,
            IFormaAplicacaoAppService formaAplicacaoAppService,
            IFrequenciaAppService frequenciaAppService,
            IUnidadeAppService unidadeAppService,
            IVelocidadeInfusaoAppService velocidadeInfusaoAppService,
            IMedicoAppService medicoAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService,
            ISolicitacaoExameAppService solicitacaoExameAppService,
            IEstoquePreMovimentoAppService estoquePreMovimentoAppService,
            IFaturamentoItemAppService fatItemAppService,
            IFormulaEstoqueAppService formulaEstoqueAppService,
            IFormulaEstoqueKitAppService formulaEstoqueKitAppService
            )
        {
            _prescricaoMedicaRepository = prescricaoMedicaRepository;
            _prescricaoItemRespostaRepository = prescricaoItemRespostaRepository;
            _prescricaoItemHoraRepositorio = prescricaoItemHoraRepositorio;
            _atendimentoAppService = atendimentoAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _prescricaoItemAppService = prescricaoItemAppService;
            _prescricaoItemStatusAppService = prescricaoItemStatusAppService;
            _prescricaoItemRespostaAppService = prescricaoItemRespostaAppService;
            _divisaoAppService = divisaoAppService;
            _formaAplicacaoAppService = formaAplicacaoAppService;
            _frequenciaAppService = frequenciaAppService;
            _unidadeAppService = unidadeAppService;
            _velocidadeInfusaoAppService = velocidadeInfusaoAppService;
            _medicoAppService = medicoAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
            _solicitacaoExameAppService = solicitacaoExameAppService;
            _estoquePreMovimentoAppService = estoquePreMovimentoAppService;
            _fatItemAppService = fatItemAppService;
            _formulaEstoqueAppService = formulaEstoqueAppService;
            _formulaEstoqueKitAppService = formulaEstoqueKitAppService;
        }

        [UnitOfWork]
        public async Task<PrescricaoMedicaDto> CriarOuEditar(PrescricaoMedicaDto input)
        {
            try
            {
                var prescricaoMedica = PrescricaoMedicaDto.Mapear(input); //input.MapTo<PrescricaoMedica>();

                input.RespostasList = input.RespostasList.Replace("undefined:00", "");
                var respostas = JsonConvert.DeserializeObject<List<PrescricaoItemRespostaDto>>(input.RespostasList, new IsoDateTimeConverter() { DateTimeFormat = "dd/MM/yyyy" });
                //var respostas = JsonConvert.DeserializeObject<List<PrescricaoItemRespostaDto>>(input.RespostasList, new JsonSerializerSettings() { DateFormatString = "dd/MM/yyyy HH:mm:ss" });
                //var respostas = JsonConvert.DeserializeObject<List<PrescricaoItemRespostaDto>>(input.RespostasList);

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _prescricaoMedicaRepository.InsertAndGetIdAsync(prescricaoMedica);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    var entidade = await _prescricaoMedicaRepository.GetAsync(prescricaoMedica.Id);
                    entidade.AtendimentoId = prescricaoMedica.AtendimentoId;
                    entidade.Codigo = prescricaoMedica.Codigo;
                    entidade.CreationTime = prescricaoMedica.CreationTime;
                    entidade.CreatorUserId = prescricaoMedica.CreatorUserId;
                    entidade.DataPrescricao = prescricaoMedica.DataPrescricao;
                    entidade.DeleterUserId = prescricaoMedica.DeleterUserId;
                    entidade.DeletionTime = prescricaoMedica.DeletionTime;
                    entidade.Descricao = prescricaoMedica.Descricao;
                    //entidade.Id = prescricaoMedica.Id;
                    entidade.Observacao = prescricaoMedica.Observacao;
                    entidade.PrescricaoStatusId = prescricaoMedica.PrescricaoStatusId;
                    entidade.PrestadorId = prescricaoMedica.PrestadorId;
                    entidade.UnidadeOrganizacionalId = prescricaoMedica.UnidadeOrganizacionalId;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _prescricaoMedicaRepository.UpdateAsync(entidade);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                var retornos = new List<PrescricaoItemRespostaDto>();
                foreach (var resposta in respostas)
                {
                    resposta.PrescricaoMedicaId = input.Id;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (resposta.IsDeleted)
                        {
                            await _prescricaoItemRespostaAppService.Excluir(resposta);
                        }
                        else
                        {
                            var retorno = await _prescricaoItemRespostaAppService.CriarOuEditar(resposta);
                            resposta.Id = retorno.Id;
                            retornos.Add(resposta);
                        }
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                input.RespostasList = JsonConvert.SerializeObject(retornos);
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _prescricaoMedicaRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoMedicaDto>> ListarTodos()
        {
            try
            {
                var query = _prescricaoMedicaRepository
                    .GetAll();

                var prescricoesMedicasDto = await query.ToListAsync();

                var result = PrescricaoMedicaDto.Mapear(prescricoesMedicasDto).ToList();

                return new ListResultDto<PrescricaoMedicaDto> { Items = result };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoMedicaIndexDto>> Listar(ListarInput input)
        {
            var contarPrescricoesMedicas = 0;
            List<PrescricaoMedicaIndexDto> prescricoesMedicasDto = new List<PrescricaoMedicaIndexDto>();
            try
            {
                var principalId = string.IsNullOrWhiteSpace(input.PrincipalId) ? 0 : Convert.ToInt64(input.PrincipalId);
                var query = _prescricaoMedicaRepository
                    .GetAll()
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Atendimento)
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Atendimento.Medico)
                    .Include(m => m.Atendimento.Medico.SisPessoa)
                    .Include(m => m.PrescricaoStatus)
                    .WhereIf(principalId > 0, m => m.AtendimentoId == principalId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                var prescricoesMedicas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                contarPrescricoesMedicas = await query.CountAsync();

                //prescricoesMedicasDto = prescricoesMedicas.MapTo<List<PrescricaoMedicaDto>>();
                foreach (var m in prescricoesMedicas)
                {
                    prescricoesMedicasDto.Add(new PrescricaoMedicaIndexDto
                    {
                        Codigo = m.Codigo,
                        DataPrescricao = m.DataPrescricao,
                        Id = m.Id,
                        Medico = m.Atendimento?.Medico?.NomeCompleto,
                        Paciente = m.Atendimento?.Paciente?.NomeCompleto,
                        PrescricaoStatusId = m.PrescricaoStatusId,
                        Status = m.PrescricaoStatus?.Descricao,
                        StatusCor = m.PrescricaoStatus?.Cor
                    });
                }

                return new PagedResultDto<PrescricaoMedicaIndexDto>(
                    contarPrescricoesMedicas,
                    prescricoesMedicasDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoMedicaDto> Obter(long id)
        {
            try
            {
                var query = await _prescricaoMedicaRepository.GetAsync(id);
                var result = new PrescricaoMedicaDto
                {
                    Id = query.Id,
                    DataPrescricao = query.DataPrescricao,
                    PrescricaoStatusId = query.PrescricaoStatusId,
                    AtendimentoId = query.AtendimentoId
                };
                var respostas = await ListarRespostasPorPrescricao(result.Id);
                var list = respostas.Items.OrderBy(m => m.IdGridPrescricaoItemResposta).ToList();
                //var idGrid = 1;
                ////list.ForEach(m => m.IdGridPrescricaoItemResposta = idGrid++);
                //foreach (var item in list)
                //{
                //    item.IdGridPrescricaoItemResposta = idGrid++;
                //}

                var json = JsonConvert.SerializeObject(list);
                result.RespostasList = json;
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoMedicaDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _prescricaoMedicaRepository
                    .GetAll();
                //.WhereIf(!filtro.IsNullOrWhiteSpace(), m =>
                //     m.CreationTime.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Medico.NomeCompleto.ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Medico.Cpf.ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Medico.Nascimento.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Medico.Rg.ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Paciente.Cpf.ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Paciente.Nascimento.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                //     m.Atendimento.Paciente.Rg.ToUpper().Contains(filtro.ToUpper())
                //);

                var prescricoesMedicas = await query.ToListAsync();
                var prescricoesMedicasDto = PrescricaoMedicaDto.Mapear(prescricoesMedicas).ToList();

                return new ListResultDto<PrescricaoMedicaDto> { Items = prescricoesMedicasDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoItemRespostaDto>> ListarRespostas(ListarPrescricaoMedicaInput input)
        {
            var contar = 0;
            List<PrescricaoItemRespostaDto> listDto = new List<PrescricaoItemRespostaDto>();
            try
            {
                long id = 0;
                var isValid = long.TryParse(input.PrincipalId, out id);
                var query = _prescricaoItemRespostaRepository
                    .GetAll()
                    .Include(m => m.PrescricaoItem)
                    .Include(m => m.Unidade)
                    .Include(m => m.Frequencia)
                    .Include(m => m.Divisao)
                    .Include(m => m.FormaAplicacao)
                    .Include(m => m.Medico)
                    .Include(m => m.PrescricaoItemStatus)
                    .Include(m => m.PrescricaoMedica)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.VelocidadeInfusao)
                    .Where(m => m.PrescricaoMedicaId == id)
                    //.WhereIf(input.DivisaoId > 0, m => m.DivisaoId == input.DivisaoId)
                    .WhereIf(input.StartDate.HasValue && input.EndDate.HasValue, m => m.CreationTime.IsBetween(input.StartDate.Value, input.EndDate.Value))
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                              m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                              m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contar = await query.CountAsync();

                var list = await query
                    //.AsNoTracking()
                    //.OrderBy(input.Sorting)
                    //.PageBy(input)
                    .ToListAsync();

                listDto = new List<PrescricaoItemRespostaDto>();
                var idGrid = 1;
                foreach (var m in list)
                {
                    var item = PrescricaoItemRespostaDto.Mapear(m);
                    item.IdGridPrescricaoItemResposta = idGrid++;
                    listDto.Add(item);
                }
                var result = listDto
                    .AsQueryable()
                    .WhereIf(input.DivisaoId > 0, m => m.DivisaoId == input.DivisaoId)
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<PrescricaoItemRespostaDto>(
                    contar,
                    result.OrderBy(m => m.IdGridPrescricaoItemResposta).ToList()
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoItemRespostaDto>> ListarRespostasJson(List<PrescricaoItemRespostaDto> list, long divisaoId)
        {
            try
            {
                var count = 0;
                if (list == null)
                {
                    list = new List<PrescricaoItemRespostaDto>();
                }
                var idGrid = 1;
                for (int i = 0; i < list.Count(); i++)
                {
                    list[i].PrescricaoItem = new PrescricaoItemDto();
                    list[i].Unidade = new UnidadeDto();
                    list[i].Frequencia = new FrequenciaDto();
                    list[i].IdGridPrescricaoItemResposta = idGrid++;

                    var prescricaoItem = await _prescricaoItemAppService.Obter(list[i].PrescricaoItemId.Value);
                    list[i].PrescricaoItem.Descricao = prescricaoItem.Descricao;
                    list[i].PrescricaoItem.Codigo = prescricaoItem.Codigo;
                    if (list[i].UnidadeId.HasValue)
                    {
                        var unidade = await _unidadeAppService.Obter(list[i].UnidadeId.Value);
                        list[i].Unidade.Descricao = unidade.Descricao;
                        list[i].Unidade.Codigo = unidade.Codigo;
                    }
                    if (list[i].FrequenciaId.HasValue)
                    {
                        var frequencia = await _frequenciaAppService.Obter(list[i].FrequenciaId.Value);
                        list[i].Frequencia.Descricao = frequencia.Descricao;
                        list[i].Frequencia.Codigo = frequencia.Codigo;
                        var id = list[i].Id;
                        var horas = await _prescricaoItemHoraRepositorio.GetAll()
                            .Where(p => p.PrescricaoItemRespostaId == id)
                            .ToListAsync();
                        var horarios = string.Empty;
                        foreach (var hora in horas)
                        {
                            horarios += string.Format("{0:00}:00 ", hora.Descricao);
                        }
                        if (horarios.Length > 0)
                        {
                            horarios = horarios.Substring(0, horarios.Length - 1);
                        }
                        list[i].Horarios = horarios;
                    }
                }
                var result = list.Where(m => m.DivisaoId == divisaoId).ToList();
                count = await Task.Run(() => list.Count());
                return new PagedResultDto<PrescricaoItemRespostaDto>(
                      count,
                      result
                      );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<List<PrescricaoItemRespostaDto>> ListarPrescricaoCompleta(List<PrescricaoItemRespostaDto> list)
        {
            var result = new List<PrescricaoItemRespostaDto>();
            try
            {
                foreach (var resposta in list)
                {
                    if (resposta.DivisaoId.HasValue)
                    {
                        resposta.Divisao = await _divisaoAppService.Obter(resposta.DivisaoId.Value);
                    }
                    if (resposta.FormaAplicacaoId.HasValue)
                    {
                        resposta.FormaAplicacao = await _formaAplicacaoAppService.Obter(resposta.FormaAplicacaoId.Value);
                    }
                    if (resposta.FrequenciaId.HasValue)
                    {
                        resposta.Frequencia = await _frequenciaAppService.Obter(resposta.FrequenciaId.Value);
                    }
                    if (resposta.MedicoId.HasValue)
                    {
                        resposta.Medico = await _medicoAppService.Obter(resposta.MedicoId.Value);
                    }

                    resposta.PrescricaoItem = await _prescricaoItemAppService.Obter(resposta.PrescricaoItemId.Value);

                    if (resposta.UnidadeId.HasValue)
                    {
                        resposta.Unidade = await _unidadeAppService.Obter(resposta.UnidadeId.Value);
                    }
                    if (resposta.UnidadeOrganizacionalId.HasValue)
                    {
                        resposta.UnidadeOrganizacional = await _unidadeOrganizacionalAppService.ObterPorId(resposta.UnidadeOrganizacionalId.Value);
                    }
                    if (resposta.VelocidadeInfusaoId.HasValue)
                    {
                        resposta.VelocidadeInfusao = await _velocidadeInfusaoAppService.Obter(resposta.VelocidadeInfusaoId.Value);
                    }
                    result.Add(resposta);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroListar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoItemRespostaRepository);
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemRespostaDto>> ListarRespostasPorPrescricao(long id)
        {
            try
            {
                var idGrid = 1;
                var query = _prescricaoItemRespostaRepository
                    .GetAll()
                    .Where(m => m.PrescricaoMedicaId == id);

                var list = await query.ToListAsync();

                var listDto = new List<PrescricaoItemRespostaDto>();

                foreach (var m in list)
                {
                    var form = new PrescricaoItemRespostaDto
                    {
                        Codigo = m.Codigo,
                        CreationTime = m.CreationTime,
                        CreatorUserId = m.CreatorUserId,
                        DataInicial = m.DataInicial,
                        DeleterUserId = m.DeleterUserId,
                        DeletionTime = m.DeletionTime,
                        Descricao = m.Descricao,
                        DivisaoId = m.DivisaoId,
                        FormaAplicacaoId = m.FormaAplicacaoId,
                        FrequenciaId = m.FrequenciaId,
                        Id = m.Id,
                        IdGridPrescricaoItemResposta = idGrid++,
                        IsDeleted = m.IsDeleted,
                        IsSeNecessario = m.IsSeNecessario,
                        IsSistema = m.IsSistema,
                        IsUrgente = m.IsUrgente,
                        LastModificationTime = m.LastModificationTime,
                        LastModifierUserId = m.LastModifierUserId,
                        MedicoId = m.MedicoId,
                        Observacao = m.Observacao,
                        PrescricaoItemId = m.PrescricaoItemId,
                        PrescricaoItemStatusId = m.PrescricaoItemStatusId,
                        PrescricaoMedicaId = m.PrescricaoMedicaId,
                        Quantidade = m.Quantidade,
                        TotalDias = m.TotalDias,
                        UnidadeId = m.UnidadeId,
                        UnidadeOrganizacionalId = m.UnidadeOrganizacionalId,
                        VelocidadeInfusaoId = m.VelocidadeInfusaoId
                    };

                    if (form.DivisaoId.HasValue)
                    {
                        form.Divisao = await _divisaoAppService.Obter(form.DivisaoId.Value);
                    }
                    if (form.FormaAplicacaoId.HasValue)
                    {
                        form.FormaAplicacao = await _formaAplicacaoAppService.Obter(form.FormaAplicacaoId.Value);
                    }
                    if (form.FrequenciaId.HasValue)
                    {
                        form.Frequencia = await _frequenciaAppService.Obter(form.FrequenciaId.Value);
                        var horas = await _prescricaoItemHoraRepositorio.GetAll()
                            .Where(p => p.PrescricaoItemRespostaId == form.Id)
                            .ToListAsync();
                        var horarios = string.Empty;
                        foreach (var hora in horas)
                        {
                            horarios += string.Format("{0:00}:00 ", hora.Descricao);
                        }
                        if (horarios.Length > 0)
                        {
                            horarios = horarios.Substring(0, horarios.Length - 1);
                        }
                        form.Horarios = horarios;
                    }
                    if (form.MedicoId.HasValue)
                    {
                        form.Medico = await _medicoAppService.Obter(form.MedicoId.Value);
                    }
                    if (form.PrescricaoItemId.HasValue)
                    {
                        form.PrescricaoItem = await _prescricaoItemAppService.Obter(form.PrescricaoItemId.Value);
                    }
                    if (form.PrescricaoItemStatusId.HasValue)
                    {
                        form.PrescricaoItemStatus = await _prescricaoItemStatusAppService.Obter(form.PrescricaoItemStatusId.Value);
                    }
                    if (form.UnidadeId.HasValue)
                    {
                        form.Unidade = await _unidadeAppService.Obter(form.UnidadeId.Value);
                    }
                    if (form.UnidadeOrganizacionalId.HasValue)
                    {
                        form.UnidadeOrganizacional = await _unidadeOrganizacionalAppService.ObterPorId(form.UnidadeOrganizacionalId.Value);
                    }
                    if (form.VelocidadeInfusaoId.HasValue)
                    {
                        form.VelocidadeInfusao = await _velocidadeInfusaoAppService.Obter(form.VelocidadeInfusaoId.Value);
                    }

                    listDto.Add(form);
                }
                return new ListResultDto<PrescricaoItemRespostaDto> { Items = listDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoMedicaDto>> ListarPorPaciente(ListarInput input)
        {
            var contarPrescricoesMedicas = 0;
            long pacienteId = 0;
            List<PrescricaoMedicaDto> prescricoesMedicasDto = new List<PrescricaoMedicaDto>();
            try
            {
                var test = long.TryParse(input.PrincipalId, out pacienteId);
                if (!test)
                {
                    throw new UserFriendlyException(("SelecionePaciente"));
                }
                //pacienteId = string.IsNullOrWhiteSpace(input.PrincipalId) ? 0 : Convert.ToInt64(input.PrincipalId);
                var query = _prescricaoMedicaRepository
                    .GetAll()
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Atendimento)
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Atendimento.Medico)
                    .Include(m => m.Atendimento.Medico.SisPessoa)
                    .Where(m => m.Atendimento.PacienteId == pacienteId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                var prescricoesMedicas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                contarPrescricoesMedicas = await query.CountAsync();
                var resultado = PrescricaoMedicaDto.Mapear(prescricoesMedicas).ToList();
                prescricoesMedicasDto = resultado;

                return new PagedResultDto<PrescricaoMedicaDto>(
                    contarPrescricoesMedicas,
                    prescricoesMedicasDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<RetornoCopiarPrescricaoMedica> ListarRespostasParaCopiar(long id)
        {
            var retorno = new RetornoCopiarPrescricaoMedica();
            try
            {
                var idGrid = 1;
                var query = _prescricaoItemRespostaRepository
                    .GetAll()
                    .Include(m => m.Divisao)
                    .Include(m => m.PrescricaoItem)
                    .Include(m => m.FormaAplicacao)
                    .Include(m => m.Frequencia)
                    .Include(m => m.Medico)
                    .Include(m => m.PrescricaoItemStatus)
                    .Include(m => m.PrescricaoMedica)
                    .Include(m => m.Unidade)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.VelocidadeInfusao)
                    .Where(m =>
                        m.PrescricaoMedicaId == id &&
                        m.Divisao.IsCopiarPrescricao &&
                        (!m.PrescricaoItemStatusId.HasValue || m.PrescricaoItemStatusId == 1)
                    );

                var list = await query.ToListAsync();

                foreach (var m in list)
                {
                    var horarios = string.Empty;
                    var horas = await _prescricaoItemHoraRepositorio.GetAll()
                        .Where(w => w.PrescricaoItemRespostaId == m.Id)
                        .ToListAsync();
                    foreach (var hora in horas)
                    {
                        horarios += string.Format("{0:00}:00 ", hora.Descricao);
                    }
                    if (horarios.Length > 0)
                    {
                        horarios = horarios.Substring(0, horarios.Length - 1);
                    }
                    if (m.Divisao.IsMedicamento)
                    {
                        var diff = DateTime.Now.Subtract(m.DataInicial.Value);
                        if (m.DataInicial <= DateTime.Now && m.TotalDias.HasValue && diff.Days <= m.TotalDias.Value)
                        {
                            if (diff.Days == m.TotalDias.Value)
                            {
                                retorno.Mensagens.Add(string.Format("O aprazamento do item {0} termina hoje.\n", m.PrescricaoItem.Descricao));
                            }
                            if (diff.Days == m.TotalDias.Value - 1)
                            {
                                retorno.Mensagens.Add(string.Format("O aprazamento do item {0} terminará amanhã.\n", m.PrescricaoItem.Descricao));
                            }

                            var item = PrescricaoItemRespostaDto.Mapear(m);

                            if (item.FrequenciaId.HasValue && item.Horarios.IsNullOrWhiteSpace())
                            {
                                //buscando os horários atuais
                                var prescricaoItemHoras = await _prescricaoItemHoraRepositorio.GetAllListAsync(p => p.PrescricaoItemRespostaId == item.Id);
                                foreach (var hora in prescricaoItemHoras.OrderBy(o => o.DataMedicamento))
                                {
                                    item.Horarios += (string.Format("{0:00}:00 ", hora.Hora));
                                }

                                item.Horarios = item.Horarios.Substring(0, item.Horarios.Length - 1);

                            }

                            retorno.PrescricaoItens.Add(item);
                        }
                        else if (m.DataInicial <= DateTime.Now && m.TotalDias.HasValue && diff.TotalDays == m.TotalDias.Value + 1)
                        {
                            //retornar que o medicamento acabou ontem
                            retorno.Mensagens.Add(string.Format("O aprazamento do item {0} terminou ontem.\n", m.PrescricaoItem.Descricao));
                        }
                    }
                    else
                    {
                        if (!m.Divisao.IsExameImagem && !m.Divisao.IsExameLaboratorial)
                        {
                            var item = PrescricaoItemRespostaDto.Mapear(m);
                            if (item.FrequenciaId.HasValue)
                            {
                                //buscando os horários atuais
                                var prescricaoItemHoras = await _prescricaoItemHoraRepositorio.GetAllListAsync(p => p.PrescricaoItemRespostaId == item.Id);
                                foreach (var hora in prescricaoItemHoras.OrderBy(o => o.DataMedicamento))
                                {
                                    item.Horarios += (string.Format("{0:00}:00 ", hora.Hora));
                                }

                                item.Horarios = item.Horarios.Substring(0, item.Horarios.Length - 1);

                            }
                            retorno.PrescricaoItens.Add(item);
                        }
                    }
                }

                return retorno;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<string> Copiar(long id, long atendimentoId)
        {
            var itens = await ListarRespostasParaCopiar(id);
            var prescricaoItens = itens.PrescricaoItens.ToList();
            if (prescricaoItens.Count() > 0)
            {
                prescricaoItens.ForEach(m => m.PrescricaoMedicaId = 0);
                prescricaoItens.ForEach(m => m.Id = 0);
                var prescricao = await Obter(id);
                var novo = new PrescricaoMedicaDto
                {
                    AtendimentoId = atendimentoId,
                    CreatorUserId = AbpSession.UserId,
                    //CreationTime = DateTime.UtcNow,
                    Descricao = prescricao.Descricao,
                    Observacao = prescricao.Observacao,
                    PrestadorId = prescricao.PrestadorId,
                    UnidadeOrganizacionalId = prescricao.UnidadeOrganizacionalId,
                    RespostasList = JsonConvert.SerializeObject(prescricaoItens),
                    DataPrescricao = DateTime.Now,
                    DivisaoId = prescricao.DivisaoId,
                    IsDeleted = false,
                    IsSistema = prescricao.IsSistema,
                    PrescricaoStatusId = 1,
                    Id = 0
                };
                await CriarOuEditar(novo);
            }
            else
            {
                itens.Mensagens.Add(string.Format("Não foram encontrados itens copiáveis na prescrição.\nNenhuma alteração realizada."));
            }
            var retorno = new StringBuilder();
            foreach (var str in itens.Mensagens)
            {
                retorno.AppendLine(str);
            }
            return retorno.ToString();
        }

        public async Task Suspender(long id, long atendimentoId)
        {
            //var prescricao = await Obter(id);

            //prescricao.PrescricaoStatusId = 5;


            using (var unitOfWork = _unitOfWorkManager.Begin())
            {

                var input = _prescricaoMedicaRepository.Get(id);
                input.PrescricaoStatusId = 5;

                await _prescricaoMedicaRepository.UpdateAsync(input);

                var retorno = await _estoquePreMovimentoAppService.ExcluirSolicitacoesPrescritasNaoAtendidas(id);

                if (retorno.Errors.Count == 0)
                {
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
                else
                {
                    ((IDisposable)CurrentUnitOfWork).Dispose();
                }

            }

            //await CriarOuEditar(prescricao);
        }

        public async Task Liberar(long id, long atendimentoId)
        {
            try
            {
                var prescricao = await Obter(id);
                prescricao.Atendimento = await _atendimentoAppService.Obter(prescricao.AtendimentoId.Value);
                var prescricoesItens = JsonConvert.DeserializeObject<List<PrescricaoItemRespostaDto>>(prescricao.RespostasList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                var listTemp = new List<PrescricaoItemRespostaDto>();
                foreach (var item in prescricoesItens)
                {
                    item.Divisao = await _divisaoAppService.Obter(item.DivisaoId.Value);
                    if (item.PrescricaoItem == null)
                    {
                        item.PrescricaoItem = await _prescricaoItemAppService.Obter(item.PrescricaoItemId.Value);
                    }
                    if (item.PrescricaoItem != null && item.PrescricaoItem.FaturamentoItemId.HasValue)
                    {
                        item.PrescricaoItem.FaturamentoItem = await _fatItemAppService.Obter(item.PrescricaoItem.FaturamentoItemId.Value);
                    }
                    listTemp.Add(item);
                }
                prescricoesItens = listTemp;
                #region RequisicaoExame

                var examesLab = prescricoesItens
                    .Where(f => f.Divisao.IsExameLaboratorial &&
                           (f.PrescricaoItem.FaturamentoItem != null &&
                            (f.PrescricaoItem.FaturamentoItem.Grupo.IsLaboratorio || f.PrescricaoItem.FaturamentoItem.IsLaboratorio)
                           )
                          ).ToList();
                var examesImg = prescricoesItens
                    .Where(f => f.Divisao.IsExameImagem &&
                           (f.PrescricaoItem.FaturamentoItem != null &&
                            ((f.PrescricaoItem.FaturamentoItem.IsLaudo && !f.PrescricaoItem.FaturamentoItem.IsLaboratorio) ||
                              (f.PrescricaoItem.FaturamentoItem.Grupo.IsLaudo && !f.PrescricaoItem.FaturamentoItem.Grupo.IsLaboratorio))
                           )
                          ).ToList();
                var exames = examesLab;
                exames.AddRange(examesImg);
                if (exames.Count() > 0)
                {
                    var urgente = exames.Where(m => m.IsUrgente).ToList().Count() > 0;
                    //var produtos = prescricoesItens.Where(m => m.Divisao.IsProdutoEstoque).ToList();
                    var solicitacao = new SolicitacaoExameDto
                    {
                        AtendimentoId = atendimentoId,
                        DataSolicitacao = DateTime.Now,
                        IsDeleted = false,
                        IsSistema = false,
                        CreatorUserId = AbpSession.UserId,
                        MedicoSolicitanteId = prescricao.Atendimento?.Medico?.Id,
                        Observacao = prescricao.Observacao,
                        PrescricaoId = id,
                        Prioridade = urgente ? 2 : 1,
                        UnidadeOrganizacionalId = prescricao.UnidadeOrganizacionalId
                    };
                    var solicitacaoItens = new List<SolicitacaoExameItemDto>();
                    foreach (var item in exames)
                    {
                        solicitacaoItens.Add(new SolicitacaoExameItemDto
                        {
                            CreatorUserId = AbpSession.UserId,
                            Descricao = item.PrescricaoItem.Descricao,
                            FaturamentoItemId = item.PrescricaoItem.FaturamentoItemId,
                            IsDeleted = false,
                            IsSistema = false,
                            MaterialId = item.PrescricaoItem.FaturamentoItem.MaterialId
                        });
                    }
                    var abc = new SolicitacaoExameInputDto();
                    abc.Solicitacao = solicitacao;
                    abc.SolicitacaoExameItens = solicitacaoItens;
                    await _solicitacaoExameAppService.RequisitarSolicitacao(abc);
                }

                #endregion

                //#region RequisicaoEstoque
                ////será necessário fazer uma solicitação por estoque
                //var estoques = prescricoesItens
                //    .Where(m => m.Divisao.IsProdutoEstoque)
                //    .Select(s => s.PrescricaoItem.EstoqueId)
                //    .Distinct()
                //    .ToList();
                //if (estoques.Count() > 0)
                //{
                //    foreach (var estoque in estoques)
                //    {
                //        var estoquePreMovimentoDto = new EstoquePreMovimentoDto();
                //        estoquePreMovimentoDto.AtendimentoId = atendimentoId;
                //        estoquePreMovimentoDto.EmpresaId = prescricao.Atendimento.EmpresaId;
                //        estoquePreMovimentoDto.TipoMovimentoId = (long)EnumTipoMovimento.Paciente_Saida;
                //        estoquePreMovimentoDto.EstoqueId = estoque;
                //        estoquePreMovimentoDto.Movimento = DateTime.Now;
                //        estoquePreMovimentoDto.Emissao = DateTime.Now;

                //        var estoqueItens = prescricoesItens
                //            .Where(m => m.PrescricaoItem.EstoqueId == estoque)
                //            .ToList();
                //        var movItens = new List<EstoquePreMovimentoItemDto>();

                //        foreach (var item in estoqueItens)
                //        {
                //            movItens.Add(new EstoquePreMovimentoItemDto
                //            {
                //                ProdutoId = item.PrescricaoItem.ProdutoId.Value,
                //                Quantidade = item.Quantidade.Value,
                //                ProdutoUnidadeId = item.UnidadeId
                //            });
                //        }

                //        estoquePreMovimentoDto.Itens = JsonConvert.SerializeObject(movItens);

                //        var result = _estoquePreMovimentoAppService.CriarOuEditarSolicitacao(estoquePreMovimentoDto);

                //        //if (result.Errors.Count() > 0)
                //        //{
                //        //    var erros = string.Empty;
                //        //    foreach (var item in result.Errors)
                //        //    {
                //        //        erros += item.CodigoErro + " - " + item.Descricao;
                //        //    }
                //        //    throw new Exception(erros);
                //        //}
                //    }
                //}
                //#endregion

                //prescricao.PrescricaoStatusId = 2;
                //prescricao.Atendimento = null;

                var entity = _prescricaoMedicaRepository.Get(prescricao.Id);
                entity.PrescricaoStatusId = 2;

                await _prescricaoMedicaRepository.UpdateAsync(entity);
                //await CriarOuEditar(prescricao);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroLiberar", ex);
            }
        }

        public async Task Aprovar(long id, long atendimentoId)
        {
            try
            {
                var prescricao = await Obter(id);
                prescricao.Atendimento = await _atendimentoAppService.Obter(prescricao.AtendimentoId.Value);
                var prescricoesItens = JsonConvert.DeserializeObject<List<PrescricaoItemRespostaDto>>(prescricao.RespostasList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                var listTemp = new List<PrescricaoItemRespostaDto>();
                foreach (var item in prescricoesItens)
                {
                    item.Divisao = await _divisaoAppService.Obter(item.DivisaoId.Value);
                    item.PrescricaoItem = await _prescricaoItemAppService.Obter(item.PrescricaoItemId.Value);
                    //item.PrescricaoItem.FaturamentoItem = await _fatItemAppService.Obter(item.PrescricaoItem.FaturamentoItemId.Value);

                    var prescricaoHorarios = _prescricaoItemHoraRepositorio.GetAll()
                                                                           .Where(w => w.PrescricaoItemRespostaId == item.Id)
                                                                           .ToList();
                    var result = PrescricaoItemHoraDto.Mapear(prescricaoHorarios).ToList();
                    item.HorariosPrescricaoItens = result;

                    item.UnidadeOrganizacional = await _unidadeOrganizacionalAppService.ObterPorId(item.UnidadeOrganizacionalId ?? 0);

                    listTemp.Add(item);
                }
                prescricoesItens = listTemp;

                #region RequisicaoEstoque
                //será necessário fazer uma solicitação por estoque
                var estoques = prescricoesItens
                    .Where(m => m.Divisao.IsProdutoEstoque)
                    .Select(s => s.UnidadeOrganizacional?.EstoqueId)
                    .Distinct()
                    .ToList();

                //if (estoques.Count() > 0)
                //{
                foreach (var estoque in estoques)
                {
                    var est = estoque ?? 1;
                    //var estoquePreMovimentoDto = new EstoquePreMovimentoDto();
                    //estoquePreMovimentoDto.AtendimentoId = atendimentoId;
                    //estoquePreMovimentoDto.EmpresaId = prescricao.Atendimento.EmpresaId;
                    //estoquePreMovimentoDto.EstTipoMovimentoId = (long)EnumTipoMovimento.Paciente_Saida;
                    //estoquePreMovimentoDto.EstoqueId = est;
                    //estoquePreMovimentoDto.Movimento = DateTime.Now;
                    //estoquePreMovimentoDto.Emissao = DateTime.Now;



                    var estoqueItens = prescricoesItens
                        .Where(m => m.UnidadeOrganizacional.EstoqueId == est)
                        .ToList();

                    var precricoesHorarios = estoqueItens.Select(s => s.HorariosPrescricaoItens);

                    var horarios = precricoesHorarios.SelectMany(s => s.OrderBy(o => o.DataMedicamento).Select(s2 => s2.DataMedicamento)).Distinct();


                    foreach (var horario in horarios)
                    {
                        var estoquePreMovimentoDto = new EstoquePreMovimentoDto();
                        estoquePreMovimentoDto.AtendimentoId = atendimentoId;
                        estoquePreMovimentoDto.EmpresaId = prescricao.Atendimento.EmpresaId;
                        estoquePreMovimentoDto.EstTipoMovimentoId = (long)EnumTipoMovimento.Paciente_Saida;
                        estoquePreMovimentoDto.EstoqueId = est;
                        estoquePreMovimentoDto.Movimento = DateTime.Now;
                        estoquePreMovimentoDto.Emissao = DateTime.Now;
                        //estoquePreMovimentoDto.HoraDiaId = horario;
                        estoquePreMovimentoDto.HoraPrescrita = horario;
                        estoquePreMovimentoDto.PrescricaoMedicaId = prescricao.Id;


                        var itensHorario = estoqueItens.Where(a => a.HorariosPrescricaoItens.Where(w => w.DataMedicamento == horario).Count() > 0);

                        var movItens = new List<EstoquePreMovimentoItemDto>();

                        foreach (var item in itensHorario)
                        {

                            movItens.Add(new EstoquePreMovimentoItemDto
                            {
                                ProdutoId = item.PrescricaoItem.ProdutoId.Value,
                                Quantidade = item.Quantidade.Value,
                                ProdutoUnidadeId = item.UnidadeId
                            });


                            var formulasEstoque = await _formulaEstoqueAppService.ListarPorPrescricaoItem((long)item.PrescricaoItemId);

                            foreach (var formulaEstoque in formulasEstoque.Items)
                            {
                                movItens.Add(new EstoquePreMovimentoItemDto
                                {
                                    ProdutoId = formulaEstoque.ProdutoId.Value,
                                    Quantidade = formulaEstoque.Quantidade,
                                    ProdutoUnidadeId = formulaEstoque.UnidadeId
                                });
                            }


                            var kitItens = _formulaEstoqueKitAppService.ListarItensKitPorPrescricaoItem((long)item.PrescricaoItemId);

                            foreach (var kitItem in kitItens)
                            {
                                movItens.Add(new EstoquePreMovimentoItemDto
                                {
                                    ProdutoId = kitItem.ProdutoId,
                                    Quantidade = kitItem.Quantidade,
                                    ProdutoUnidadeId = kitItem.UnidadeId
                                });
                            }

                            estoquePreMovimentoDto.Itens = JsonConvert.SerializeObject(movItens);


                        }

                        var result = _estoquePreMovimentoAppService.CriarOuEditarSolicitacao(estoquePreMovimentoDto);






                        //movItens.Add(new EstoquePreMovimentoItemDto
                        //{
                        //    ProdutoId = item.PrescricaoItem.ProdutoId.Value,
                        //    Quantidade = item.Quantidade.Value,
                        //    ProdutoUnidadeId = item.UnidadeId
                        //});
                    }

                    // estoquePreMovimentoDto.Itens = JsonConvert.SerializeObject(movItens);

                    //var result = _estoquePreMovimentoAppService.CriarOuEditarSolicitacao(estoquePreMovimentoDto);

                }
                //}
                #endregion

                //prescricao.PrescricaoStatusId = 6;
                //prescricao.Atendimento = null;

                var input = _prescricaoMedicaRepository.Get(prescricao.Id);
                input.PrescricaoStatusId = 6;

                await _prescricaoMedicaRepository.UpdateAsync(input);

                //await CriarOuEditar(prescricao);

                //await CriarOuEditar(prescricao);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroAprovar", ex);
            }
        }

        public async Task ReAtivar(long id)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {



                //   await _prescricaoMedicaRepository.UpdateAsync(input);

                var retorno = await _estoquePreMovimentoAppService.ReAtivarSolicitacoDePrescricaoMedica(id);


                var input = _prescricaoMedicaRepository.Get(id);


                if (retorno.ReturnObject != null)
                {
                    input.PrescricaoStatusId = 6;
                }
                else
                {
                    input.PrescricaoStatusId = 2;
                }


                if (retorno.Errors.Count == 0)
                {
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
                else
                {
                    ((IDisposable)CurrentUnitOfWork).Dispose();
                }

            }

        }
    }
}
