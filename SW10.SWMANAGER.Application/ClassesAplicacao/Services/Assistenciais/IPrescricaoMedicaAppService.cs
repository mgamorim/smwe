﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public interface IPrescricaoMedicaAppService : IApplicationService
    {
        Task<PagedResultDto<PrescricaoMedicaIndexDto>> Listar(ListarInput input);

        Task<ListResultDto<PrescricaoMedicaDto>> ListarFiltro(string filtro);

        Task<PagedResultDto<PrescricaoItemRespostaDto>> ListarRespostas(ListarPrescricaoMedicaInput input);

        Task<ListResultDto<PrescricaoItemRespostaDto>> ListarRespostasPorPrescricao(long id);

        Task<PagedResultDto<PrescricaoItemRespostaDto>> ListarRespostasJson(List<PrescricaoItemRespostaDto> list, long divisaoId);

        Task<List<PrescricaoItemRespostaDto>> ListarPrescricaoCompleta(List<PrescricaoItemRespostaDto> list);

        Task<PagedResultDto<PrescricaoMedicaDto>> ListarPorPaciente(ListarInput input);

        Task<PrescricaoMedicaDto> CriarOuEditar(PrescricaoMedicaDto input);

        Task Excluir(long id);

        Task<PrescricaoMedicaDto> Obter(long id);

        Task<ListResultDto<PrescricaoMedicaDto>> ListarTodos();

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<RetornoCopiarPrescricaoMedica> ListarRespostasParaCopiar(long id);

        Task<string> Copiar(long id, long atendimentoId);
		
        Task Suspender(long id, long atendimentoId);

        Task Liberar(long id, long atendimentoId);

        Task Aprovar(long id, long atendimentoId);

        Task ReAtivar(long id);

    }
}
