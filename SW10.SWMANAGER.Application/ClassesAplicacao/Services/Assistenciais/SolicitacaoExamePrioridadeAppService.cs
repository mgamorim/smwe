﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public class SolicitacaoExamePrioridadeAppService : SWMANAGERAppServiceBase, ISolicitacaoExamePrioridadeAppService
    {
        private readonly IRepository<SolicitacaoExamePrioridade, long> _solicitacaoExameRepository;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;

        public SolicitacaoExamePrioridadeAppService(
            IRepository<SolicitacaoExamePrioridade, long> solicitacaoExameRepository,
            IAtendimentoAppService atendimentoAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoIdAppService
            )
        {
            _solicitacaoExameRepository = solicitacaoExameRepository;
            _atendimentoAppService = atendimentoAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoIdAppService;
        }

        [UnitOfWork]
        public async Task<SolicitacaoExamePrioridadeDto> CriarOuEditar(SolicitacaoExamePrioridadeDto input)
        {
            try
            {
                var solicitacaoExame = SolicitacaoExamePrioridadeDto.Mapear(input);
                if (input.Id.Equals(0))
                {
                    var ultimosId = await _ultimoIdAppService.ListarTodos();
                    var ultimoId = ultimosId.Items.Where(m => m.NomeTabela == "SolicitacaoExamePrioridade").FirstOrDefault();
                    solicitacaoExame.Codigo = ultimoId.Codigo;
                    input.Codigo = solicitacaoExame.Codigo;
                    var codigo = Convert.ToInt64(ultimoId.Codigo);
                    codigo++;
                    ultimoId.Codigo = codigo.ToString();
                    await _ultimoIdAppService.CriarOuEditar(ultimoId);
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        var _solicitacaoExameId = await _solicitacaoExameRepository.InsertAndGetIdAsync(solicitacaoExame);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                        input.Id = _solicitacaoExameId;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        var _solicitacaoExame = await _solicitacaoExameRepository.UpdateAsync(solicitacaoExame);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                        input = SolicitacaoExamePrioridadeDto.Mapear(_solicitacaoExame);
                    }
                }
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _solicitacaoExameRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<SolicitacaoExamePrioridadeDto>> ListarTodos()
        {
            try
            {
                var query = _solicitacaoExameRepository
                    .GetAll();


                var admissoesMedicasDto = await query.ToListAsync();

                return new ListResultDto<SolicitacaoExamePrioridadeDto> { Items = SolicitacaoExamePrioridadeDto.Mapear(admissoesMedicasDto).ToList() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<SolicitacaoExamePrioridadeDto>> Listar(ListarInput input)
        {
            var contarAdmissoesMedicas = 0;
            List<SolicitacaoExamePrioridadeDto> admissoesMedicasDto = new List<SolicitacaoExamePrioridadeDto>();
            try
            {
                var query = _solicitacaoExameRepository
                    .GetAll()
                    .Where(m => m.CreationTime >= input.StartDate && m.CreationTime <= input.EndDate)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    )
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input);

                contarAdmissoesMedicas = await query.CountAsync();
                var admissoesMedicas = await query.ToListAsync();
                admissoesMedicasDto = SolicitacaoExamePrioridadeDto.Mapear(admissoesMedicas).ToList();
                return new PagedResultDto<SolicitacaoExamePrioridadeDto>(
                    contarAdmissoesMedicas,
                    admissoesMedicasDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<SolicitacaoExamePrioridadeDto> solicitacaoExamePrioridadesDto = new List<SolicitacaoExamePrioridadeDto>();
            try
            {
                var query = from p in _solicitacaoExameRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<SolicitacaoExamePrioridadeDto> Obter(long id)
        {
            try
            {
                var query = await _solicitacaoExameRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var result = SolicitacaoExamePrioridadeDto.Mapear(query);
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<SolicitacaoExamePrioridadeDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _solicitacaoExameRepository
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrWhiteSpace(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                    );

                var admissoesMedicas = await query.ToListAsync();
                var admissoesMedicasDto = SolicitacaoExamePrioridadeDto.Mapear(admissoesMedicas).ToList();

                return new ListResultDto<SolicitacaoExamePrioridadeDto> { Items = admissoesMedicasDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
