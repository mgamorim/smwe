﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Globais.HorasDia;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public class PrescricaoItemRespostaAppService : SWMANAGERAppServiceBase, IPrescricaoItemRespostaAppService
    {
        private readonly IRepository<PrescricaoItemResposta, long> _prescricaoItemRespostaRepositorio;
        private readonly IRepository<HoraDia, long> _horaDiaRepositorio;
        private readonly IRepository<PrescricaoItemHora, long> _prescricaoItemHoraRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDivisaoAppService _divisaoAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        private readonly IPrescricaoItemAppService _prescricaoItemAppService;
        private readonly IFrequenciaAppService _frequenciaAppService;
        //private readonly IPrescricaoItemHoraAppService _prescricaoItemHoraAppService;

        public PrescricaoItemRespostaAppService(
            IRepository<PrescricaoItemResposta, long> prescricaoItemRespostaRepositorio,
            IRepository<HoraDia, long> horaDiaRepositorio,
            IRepository<PrescricaoItemHora, long> prescricaoItemHoraRepositorio,
            IUnitOfWorkManager unitOfWorkManager,
            IDivisaoAppService divisaoAppService,
            IPrescricaoItemAppService prescricaoItemAppService,
            IFrequenciaAppService frequenciaAppService,
            //IPrescricaoItemHoraAppService prescricaoItemHoraAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService
            )
        {
            _prescricaoItemRespostaRepositorio = prescricaoItemRespostaRepositorio;
            _horaDiaRepositorio = horaDiaRepositorio;
            _prescricaoItemHoraRepositorio = prescricaoItemHoraRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
            _divisaoAppService = divisaoAppService;
            _prescricaoItemAppService = prescricaoItemAppService;
            _frequenciaAppService = frequenciaAppService;
            //_prescricaoItemHoraAppService = prescricaoItemHoraAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
        }

        [UnitOfWork]
        public async Task<PrescricaoItemRespostaDto> CriarOuEditar(PrescricaoItemRespostaDto input)
        {
            try
            {
                if (!input.DivisaoId.HasValue || (input.DivisaoId.HasValue && input.DivisaoId.Value == 0))
                {
                    var prescricaoItem = await _prescricaoItemAppService.Obter(input.PrescricaoItemId.Value);
                    input.DivisaoId = prescricaoItem.DivisaoId;
                }
                var tipoResposta = new PrescricaoItemResposta();
                tipoResposta.Codigo = input.Codigo;
                tipoResposta.CreationTime = input.CreationTime;
                tipoResposta.CreatorUserId = input.CreatorUserId;
                if (input.DataInicial.HasValue)
                {
                    tipoResposta.DataInicial = input.DataInicial.Value;
                }
                tipoResposta.DeleterUserId = input.DeleterUserId;
                tipoResposta.DeletionTime = input.DeletionTime;
                tipoResposta.Descricao = input.Descricao;
                tipoResposta.DivisaoId = input.DivisaoId;
                tipoResposta.FormaAplicacaoId = input.FormaAplicacaoId;
                tipoResposta.FrequenciaId = input.FrequenciaId;
                tipoResposta.Id = input.Id;
                tipoResposta.IsDeleted = input.IsDeleted;
                tipoResposta.IsSeNecessario = input.IsSeNecessario;
                tipoResposta.IsSistema = input.IsSistema;
                tipoResposta.IsUrgente = input.IsUrgente;
                tipoResposta.LastModificationTime = input.LastModificationTime;
                tipoResposta.LastModifierUserId = input.LastModifierUserId;
                tipoResposta.MedicoId = input.MedicoId;
                tipoResposta.Observacao = input.Observacao;
                tipoResposta.PrescricaoItemId = input.PrescricaoItemId;
                tipoResposta.PrescricaoItemStatusId = input.PrescricaoItemStatusId;
                tipoResposta.PrescricaoMedicaId = input.PrescricaoMedicaId;
                tipoResposta.Quantidade = input.Quantidade;
                tipoResposta.TotalDias = input.TotalDias;
                tipoResposta.UnidadeId = input.UnidadeId;
                tipoResposta.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                tipoResposta.VelocidadeInfusaoId = input.VelocidadeInfusaoId;

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _prescricaoItemRespostaRepositorio.InsertAndGetIdAsync(tipoResposta);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    var updated = await _prescricaoItemRespostaRepositorio.GetAsync(input.Id);
                    updated.Codigo = input.Codigo;
                    updated.CreationTime = input.CreationTime;
                    updated.CreatorUserId = input.CreatorUserId;
                    updated.DataInicial = input.DataInicial.HasValue ? input.DataInicial.Value : new DateTime?();
                    updated.Descricao = input.Descricao;
                    updated.DivisaoId = input.DivisaoId;
                    updated.FormaAplicacaoId = input.FormaAplicacaoId;
                    updated.FrequenciaId = input.FrequenciaId;
                    updated.IsSeNecessario = input.IsSeNecessario;
                    updated.IsSistema = input.IsSistema;
                    updated.IsUrgente = input.IsUrgente;
                    updated.MedicoId = input.MedicoId;
                    updated.Observacao = input.Observacao;
                    updated.PrescricaoItemId = input.PrescricaoItemId;
                    updated.PrescricaoMedicaId = input.PrescricaoMedicaId;
                    updated.Quantidade = input.Quantidade;
                    updated.TotalDias = input.TotalDias;
                    updated.UnidadeId = input.UnidadeId;
                    updated.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                    updated.VelocidadeInfusaoId = input.VelocidadeInfusaoId;

                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        tipoResposta = await _prescricaoItemRespostaRepositorio.UpdateAsync(updated);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                    }
                }
                //salvando os horários de medicação
                if (input.FrequenciaId.HasValue)
                {
                    //removendo horários anteriores deste item de prescrição
                    await _prescricaoItemHoraRepositorio.DeleteAsync(m => m.PrescricaoItemRespostaId == input.Id);
                    var unidadeOrganizacional = await _unidadeOrganizacionalAppService.ObterPorId(input.UnidadeOrganizacionalId.Value);
                    var frequencia = await _frequenciaAppService.Obter(input.FrequenciaId.Value);
                    //var dif = DateTime.Now.Subtract(input.DataInicial.Value); //.Subtract(input.DataInicial.Value);
                    //var dia = dif.Days;
                    var dia = 0;
                    //se for o primeiro dia, ajustar os horários de acordo com o início da prescrição
                    var aHoraIni = !string.IsNullOrEmpty(frequencia.HoraInicialMedicacao) ? frequencia.HoraInicialMedicacao.Split(':') : unidadeOrganizacional.HoraInicioPrescricao.Split(':');
                    int horaIni = int.Parse(aHoraIni[0]);
                    //var loops = 24 / frequencia.Intervalo;
                    //var aHoraInicioPrescricao = unidadeOrganizacional.HoraInicioPrescricao.Split(':');
                    //var horaInicioPrescricao = int.Parse(aHoraInicioPrescricao[0]);
                    var horas = input.Horarios.Split(' ');
                    //var loop = int.Parse(horas[0].Split(':')[0]);
                    //if (dia == 0)
                    //{

                    List<string> horasDias = new List<string>();

                    var totalDias = input.TotalDias ?? 1;

                    var dateDiff = DateTime.Now.Subtract(input.DataInicial.Value).TotalDays;
                    //if (dateDiff > totalDias)
                    //{
                    //    throw new Exception("Não é possível aprazar o medicamento, a quantidade de dias de aprazamento ultrapassa a determinado pelo médico");
                    //}
                    //for (int i = (int)dateDiff; i < totalDias; i++)
                    for (int i = 0; i < totalDias; i++)
                    {
                        horasDias.AddRange(horas);
                    }
                    var count = 0;
                    DateTime dataMedicamento = input.DataInicial.Value;
                    foreach (var _hora in horasDias)
                    {
                        var horaLoop = string.Format("{0:D2}", _hora.Split(':')[0]);
                        if (dia == 1)
                        {
                            //dataMedicamento = count == 0 ? input.DataInicial.Value : input.DataInicial.Value.AddDays(count);
                            count++;
                            var horaAtendimento = 0;
                            if (int.TryParse(unidadeOrganizacional.HoraInicioPrescricao.Split(":")[0], out horaAtendimento))
                            {
                                var horaAtual = int.Parse(horaLoop);
                                if (horaAtual >= horaAtendimento)
                                {
                                    break;
                                }
                            }
                        }


                        horaIni = int.Parse(_hora.Split(':')[0]);
                        var horaDia = await _horaDiaRepositorio.GetAll()
                            .Where(m => m.Descricao.Equals(horaLoop)).FirstOrDefaultAsync();
                        var hora = new PrescricaoItemHora();
                        hora.Descricao = horaIni.ToString("00");
                        hora.DataMedicamento = new DateTime(dataMedicamento.Year, dataMedicamento.Month, dataMedicamento.Day, horaIni, 0, 0);
                        hora.DiaMedicamento = (int)dia;
                        hora.Hora = horaIni.ToString();
                        hora.IsDeleted = false;
                        hora.IsSistema = false;
                        hora.PrescricaoItemRespostaId = input.Id;
                        hora.HoraDiaId = horaDia.Id;
                        //await _prescricaoItemHoraAppService.CriarOuEditar(hora);
                        await _prescricaoItemHoraRepositorio.InsertAsync(hora);
                        horaIni += (int)frequencia.Intervalo;
                        if (horaIni >= 24)
                        {
                            dia++;
                            if (dia > 1)
                            {
                                break;
                            }
                            dataMedicamento = dataMedicamento.AddDays(1);
                        }
                        //loops--;
                    }
                    ////aHoraIni = frequencia.HoraInicialMedicacao.Split(':');
                    //horaIni = int.Parse(horas[0].Split(':')[0]);
                    //loops = 24 / frequencia.Intervalo;
                    //if (horaIni < horaInicioPrescricao)
                    //{
                    //    while (horaIni < horaInicioPrescricao)
                    //    {
                    //        horaIni += (int)frequencia.Intervalo;
                    //        if (horaIni >= 24)
                    //        {
                    //            horaIni -= 24;
                    //            dia++;
                    //        }
                    //        loops--;
                    //    }
                    //}
                    //while (loops > 0)
                    //{
                    //    var horaLoop = horaIni.ToString("00");
                    //    var horaDia = await _horaDiaRepositorio.GetAll()
                    //        .Where(m => m.Descricao.Equals(horaLoop)).FirstOrDefaultAsync();
                    //    var hora = new PrescricaoItemHora();
                    //    hora.Descricao = horaIni.ToString();
                    //    hora.DataMedicamento = new DateTime(input.DataInicial.Value.Year, input.DataInicial.Value.Month, input.DataInicial.Value.Day, horaIni, 0, 0);
                    //    hora.DiaMedicamento = (int)dia;
                    //    hora.Hora = horaIni.ToString();
                    //    hora.IsDeleted = false;
                    //    hora.IsSistema = false;
                    //    hora.PrescricaoItemRespostaId = input.Id;
                    //    hora.HoraDiaId = horaDia.Id;
                    //    //await _prescricaoItemHoraAppService.CriarOuEditar(hora);
                    //    await _prescricaoItemHoraRepositorio.InsertAsync(hora);
                    //    horaIni += (int)frequencia.Intervalo;
                    //    if (horaIni >= 24)
                    //    {
                    //        horaIni -= 24;
                    //        dia++;
                    //    }
                    //    loops--;
                    //}
                    //}
                    //else
                    //{
                    //    var count = 0;
                    //    foreach (var _hora in horas)
                    //    {
                    //        var horaLoop = _hora.Split(':')[0];

                    //        //if (count == 0)
                    //        //{
                    //        //    var a = _hora.Split(':');
                    //        //    horaIni = int.Parse(a[0]);
                    //        //    if (horaIni != horaInicioPrescricao)
                    //        //    {
                    //        //        horaLoop = string.Format("{0:00}", horaInicioPrescricao);
                    //        //        horaIni = horaInicioPrescricao;
                    //        //    }
                    //        //    count++;
                    //        //}

                    //        var horaDia = await _horaDiaRepositorio
                    //            .GetAll()
                    //            .Where(m => m.Descricao.Equals(horaLoop))
                    //            .FirstOrDefaultAsync();

                    //        var hora = new PrescricaoItemHora();

                    //        hora.Descricao = horaIni.ToString("00");
                    //        hora.DataMedicamento = new DateTime(input.DataInicial.Value.Year, input.DataInicial.Value.Month, input.DataInicial.Value.Day, horaIni, 0, 0);
                    //        hora.DiaMedicamento = (int)dia;
                    //        hora.Hora = horaIni.ToString();
                    //        hora.IsDeleted = false;
                    //        hora.IsSistema = false;
                    //        hora.PrescricaoItemRespostaId = input.Id;
                    //        hora.HoraDiaId = horaDia.Id;
                    //        //await _prescricaoItemHoraAppService.CriarOuEditar(hora);
                    //        await _prescricaoItemHoraRepositorio.InsertAsync(hora);
                    //        //horaIni += (int)frequencia.Intervalo;
                    //        //if (horaIni >= 24)
                    //        //{
                    //        //    dia++;
                    //        //}
                    //        ////loops--;
                    //        //count++;
                    //    }
                    //////aHoraIni = frequencia.HoraInicialMedicacao.Split(':');
                    ////horaIni = int.Parse(aHoraIni[0]);
                    ////if (horaIni < horaInicioPrescricao)
                    ////{
                    ////    horaIni = horaInicioPrescricao;
                    ////}
                    ////loops = 24 / frequencia.Intervalo;
                    ////while (loops > 0)
                    ////{
                    ////    var diaLoop = horaIni.ToString("00");
                    ////    var horaDia = await _horaDiaRepositorio.GetAll()
                    ////        .Where(m => m.Descricao.Equals(diaLoop)).FirstOrDefaultAsync();
                    ////    var hora = new PrescricaoItemHora();
                    ////    hora.Descricao = horaIni.ToString();
                    ////    hora.DataMedicamento = new DateTime(input.DataInicial.Value.Year, input.DataInicial.Value.Month, input.DataInicial.Value.Day, horaIni, 0, 0);
                    ////    hora.DiaMedicamento = (int)dia;
                    ////    hora.Hora = horaIni.ToString();
                    ////    hora.IsDeleted = false;
                    ////    hora.IsSistema = false;
                    ////    hora.PrescricaoItemRespostaId = input.Id;
                    ////    hora.HoraDiaId = horaDia.Id;
                    ////    //await _prescricaoItemHoraAppService.CriarOuEditar(hora);
                    ////    await _prescricaoItemHoraRepositorio.InsertAsync(hora);
                    ////    horaIni += (int)frequencia.Intervalo;
                    ////    if (horaIni >= 24)
                    ////    {
                    ////        horaIni -= 24;
                    ////        dia++;
                    ////    }
                    ////    loops--;
                    ////}
                    //}
                }
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(PrescricaoItemRespostaDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _prescricaoItemRespostaRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoItemRespostaDto> Obter(long id)
        {
            try
            {
                var result = await _prescricaoItemRespostaRepositorio
                    .GetAll()
                    .Include(m => m.Divisao)
                    .Include(m => m.FormaAplicacao)
                    .Include(m => m.Frequencia)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.PrescricaoItem)
                    .Include(m => m.Unidade)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.VelocidadeInfusao)
                    .Include(m => m.PrescricaoMedica)
                    .Where(m => m.Id == id)
                    .OrderBy(m => m.Codigo)
                    .FirstOrDefaultAsync();

                var prescricaoItemRespostaDto = PrescricaoItemRespostaDto.Mapear(result);

                //buscando os horários atuais
                var prescricaoItemHoras = await _prescricaoItemHoraRepositorio.GetAllListAsync(m => m.PrescricaoItemRespostaId == result.Id);
                foreach (var item in prescricaoItemHoras.OrderBy(o => o.DataMedicamento))
                {
                    prescricaoItemRespostaDto.Horarios += (string.Format("{0:00}:00 ", item.Hora));
                }
                if (!string.IsNullOrWhiteSpace(prescricaoItemRespostaDto.Horarios))
                {
                    prescricaoItemRespostaDto.Horarios = prescricaoItemRespostaDto.Horarios.Substring(0, prescricaoItemRespostaDto.Horarios.Length - 1);
                }
                return prescricaoItemRespostaDto;

            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoItemRespostaDto> ObterJson(List<PrescricaoItemRespostaDto> list, long idGrid, long idDivisao)
        {
            try
            {
                var prescricaoItemResposta = await Task.Run(() => list.Where(m => m.IdGridPrescricaoItemResposta == idGrid && m.DivisaoId == idDivisao).FirstOrDefault());

                return prescricaoItemResposta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoItemRespostaDto>> Listar(ListarInput input)
        {
            var contarTipoResposta = 0;
            List<PrescricaoItemResposta> prescricaoItemResposta;
            List<PrescricaoItemRespostaDto> prescricaoItensRespostaDto = new List<PrescricaoItemRespostaDto>();
            try
            {
                var query = _prescricaoItemRespostaRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoResposta = await query
                    .CountAsync();

                prescricaoItemResposta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                prescricaoItensRespostaDto = PrescricaoItemRespostaDto.Mapear(prescricaoItemResposta).ToList();

                var idGrid = 1;
                //prescricaoItensRespostaDto.ForEach(m => m.IdGridPrescricaoItemResposta = idGrid++);
                foreach (var prescricaoItemRespostaDto in prescricaoItensRespostaDto)
                {
                    prescricaoItemRespostaDto.IdGridPrescricaoItemResposta = idGrid++;
                    //buscando os horários atuais
                    var prescricaoItemHoras = await _prescricaoItemHoraRepositorio.GetAllListAsync(m => m.PrescricaoItemRespostaId == prescricaoItemRespostaDto.Id);
                    foreach (var item in prescricaoItemHoras.OrderBy(o => o.DataMedicamento))
                    {
                        prescricaoItemRespostaDto.Horarios += (string.Format("{0:D2}:00 ", item.Hora));
                    }

                    prescricaoItemRespostaDto.Horarios = prescricaoItemRespostaDto.Horarios.Substring(0, prescricaoItemRespostaDto.Horarios.Length - 1);
                }

                return new PagedResultDto<PrescricaoItemRespostaDto>(
                    contarTipoResposta,
                    prescricaoItensRespostaDto
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemRespostaDto>> ListarTodos()
        {
            try
            {
                var query = _prescricaoItemRespostaRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var tipoResposta = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = PrescricaoItemRespostaDto.Mapear(tipoResposta).ToList();

                return new ListResultDto<PrescricaoItemRespostaDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemRespostaDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _prescricaoItemRespostaRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var prescricaoItemResposta = await query
                    .AsNoTracking()
                    .ToListAsync();

                var prescricaoItensRespostaDto = PrescricaoItemRespostaDto.Mapear(prescricaoItemResposta).ToList();

                var idGrid = 1;
                //prescricaoItensRespostaDto.ForEach(m => m.IdGridPrescricaoItemResposta = idGrid++);
                foreach (var prescricaoItemRespostaDto in prescricaoItensRespostaDto)
                {
                    prescricaoItemRespostaDto.IdGridPrescricaoItemResposta = idGrid++;
                    //buscando os horários atuais
                    var prescricaoItemHoras = await _prescricaoItemHoraRepositorio.GetAllListAsync(m => m.PrescricaoItemRespostaId == prescricaoItemRespostaDto.Id);
                    foreach (var item in prescricaoItemHoras.OrderBy(o => o.DataMedicamento))
                    {
                        prescricaoItemRespostaDto.Horarios += (string.Format("{0:D2}:00 ", item.Hora));
                    }

                    prescricaoItemRespostaDto.Horarios = prescricaoItemRespostaDto.Horarios.Substring(0, prescricaoItemRespostaDto.Horarios.Length - 1);
                }

                return new ListResultDto<PrescricaoItemRespostaDto>
                {
                    Items = prescricaoItensRespostaDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoItemRespostaRepositorio);
        }

        [UnitOfWork(false)]
        public Task<FileDto> ListarParaExcel(ListarInput input)
        {
            throw new NotImplementedException();
        }

        [UnitOfWork]
        public async Task Suspender(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var item = await _prescricaoItemRespostaRepositorio.GetAsync(id);
                    item.PrescricaoItemStatusId = 2;
                    await _prescricaoItemRespostaRepositorio.UpdateAsync(item);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                    _unitOfWorkManager.Current.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSuspender"), ex);
            }
        }
    }
}
