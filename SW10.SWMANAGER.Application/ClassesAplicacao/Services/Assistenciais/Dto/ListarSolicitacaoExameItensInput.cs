﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using SW10.SWMANAGER.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto
{
    public class ListarSolicitacaoExameItensInput : ListarInput
    {
        public long SolicitacaoExameId { get; set; }
        public long? PacienteId { get; set; }
    }
}
