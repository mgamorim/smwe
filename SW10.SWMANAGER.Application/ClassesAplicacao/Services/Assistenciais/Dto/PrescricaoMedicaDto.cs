﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Prestadores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto
{
    [AutoMap(typeof(PrescricaoMedica))]
    public class PrescricaoMedicaDto : CamposPadraoCRUDDto
    {
        public DateTime DataPrescricao { get; set; }

        public long? UnidadeOrganizacionalId { get; set; }
        public long? AtendimentoId { get; set; }
        public long? PrestadorId { get; set; }
        public string Observacao { get; set; }

        public string RespostasList { get; set; }

        public long? DivisaoId { get; set; }

        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }

        public AtendimentoDto Atendimento { get; set; }

        public PrestadorDto Prestador { get; set; }

        public long? PrescricaoStatusId { get; set; }
        public PrescricaoStatusDto PrescricaoStatus { get; set; }

        public static PrescricaoMedicaDto Mapear(PrescricaoMedica input)
        {
            var result = new PrescricaoMedicaDto();
            result.AtendimentoId = input.AtendimentoId;
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.DataPrescricao = input.DataPrescricao;
            result.Descricao = input.Descricao;
            result.Id = input.Id;
            result.IsSistema = input.IsSistema;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;
            result.Observacao = input.Observacao;
            result.PrescricaoStatusId = input.PrescricaoStatusId;
            result.PrestadorId = input.PrestadorId;
            result.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
            if (input.Atendimento != null)
            {
                result.Atendimento = AtendimentoDto.MapearFromCore(input.Atendimento);
            }
            if (input.PrescricaoStatus != null)
            {
                result.PrescricaoStatus = PrescricaoStatusDto.Mapear(input.PrescricaoStatus);
            }
            //if (input.Prestador != null)
            //{
            //    result.Prestador = PrestadorDto.Mapear(input.Prestador);
            //}

            return result;
        }

        public static PrescricaoMedica Mapear(PrescricaoMedicaDto input)
        {
            var result = new PrescricaoMedica();
            result.AtendimentoId = input.AtendimentoId;
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.DataPrescricao = input.DataPrescricao;
            result.Descricao = input.Descricao;
            result.Id = input.Id;
            result.IsSistema = input.IsSistema;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;
            result.Observacao = input.Observacao;
            result.PrescricaoStatusId = input.PrescricaoStatusId;
            result.PrestadorId = input.PrestadorId;
            result.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
            if (input.Atendimento != null)
            {
                result.Atendimento = AtendimentoDto.Mapear(input.Atendimento);
            }
            if (input.PrescricaoStatus != null)
            {
                result.PrescricaoStatus = PrescricaoStatusDto.Mapear(input.PrescricaoStatus);
            }
            //if (input.Prestador != null)
            //{
            //    result.Prestador = PrestadorDto.Mapear(input.Prestador);
            //}

            return result;
        }

        public static IEnumerable<PrescricaoMedicaDto> Mapear(List<PrescricaoMedica> input)
        {
            foreach (var item in input)
            {
                var result = new PrescricaoMedicaDto();
                result.AtendimentoId = item.AtendimentoId;
                result.Codigo = item.Codigo;
                result.CreationTime = item.CreationTime;
                result.CreatorUserId = item.CreatorUserId;
                result.DataPrescricao = item.DataPrescricao;
                result.Descricao = item.Descricao;
                result.Id = item.Id;
                result.IsSistema = item.IsSistema;
                result.LastModificationTime = item.LastModificationTime;
                result.LastModifierUserId = item.LastModifierUserId;
                result.Observacao = item.Observacao;
                result.PrescricaoStatusId = item.PrescricaoStatusId;
                result.PrestadorId = item.PrestadorId;
                result.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                if (item.Atendimento != null)
                {
                    result.Atendimento = AtendimentoDto.MapearFromCore(item.Atendimento);
                }
                if (item.PrescricaoStatus != null)
                {
                    result.PrescricaoStatus = PrescricaoStatusDto.Mapear(item.PrescricaoStatus);
                }
                //if (item.Prestador != null)
                //{
                //    result.Prestador = PrestadorDto.Mapear(item.Prestador);
                //}

                yield return result;
            }
        }

        public static IEnumerable<PrescricaoMedica> Mapear(List<PrescricaoMedicaDto> input)
        {
            foreach (var item in input)
            {
                var result = new PrescricaoMedica();
                result.AtendimentoId = item.AtendimentoId;
                result.Codigo = item.Codigo;
                result.CreationTime = item.CreationTime;
                result.CreatorUserId = item.CreatorUserId;
                result.DataPrescricao = item.DataPrescricao;
                result.Descricao = item.Descricao;
                result.Id = item.Id;
                result.IsSistema = item.IsSistema;
                result.LastModificationTime = item.LastModificationTime;
                result.LastModifierUserId = item.LastModifierUserId;
                result.Observacao = item.Observacao;
                result.PrescricaoStatusId = item.PrescricaoStatusId;
                result.PrestadorId = item.PrestadorId;
                result.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                if (item.Atendimento != null)
                {
                    result.Atendimento = AtendimentoDto.Mapear(item.Atendimento);
                }
                if (item.PrescricaoStatus != null)
                {
                    result.PrescricaoStatus = PrescricaoStatusDto.Mapear(item.PrescricaoStatus);
                }
                //if (item.Prestador != null)
                //{
                //    result.Prestador = PrestadorDto.Mapear(item.Prestador);
                //}

                yield return result;
            }
        }

    }
}
