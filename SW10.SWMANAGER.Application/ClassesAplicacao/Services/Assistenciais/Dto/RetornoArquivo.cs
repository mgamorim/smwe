﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto
{
    public class RetornoArquivo
    {
        public bool IsPDF { get; set; }
        public string ImgSrc { get; set; }
        public FileContentResult FilePDF { get; set; }
    }
}
