﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.KitsExames.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Materiais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto
{
    [AutoMap(typeof(SolicitacaoExameItem))]
    public class SolicitacaoExameItemDto : CamposPadraoCRUDDto
    {
        public long SolicitacaoExameId { get; set; }
        public SolicitacaoExameDto Solicitacao { get; set; }
        public long? FaturamentoItemId { get; set; }
        public FaturamentoItemDto FaturamentoItem { get; set; }
        public string GuiaNumero { get; set; }
        public DateTime? DataValidade { get; set; }
        public string SenhaNumero { get; set; }
        public long? MaterialId { get; set; }
        public MaterialDto Material { get; set; }
        public string Justificativa { get; set; }
        public long? KitExameId { get; set; }
        public KitExameDto KitExame { get; set; }


        #region Mapeamento

        public static SolicitacaoExameItemDto Mapear(SolicitacaoExameItem solicitacaoExameItem)
        {
            SolicitacaoExameItemDto solicitacaoExameItemDto = new SolicitacaoExameItemDto();

            solicitacaoExameItemDto.Id = solicitacaoExameItem.Id;
            solicitacaoExameItemDto.Codigo = solicitacaoExameItem.Codigo;
            solicitacaoExameItemDto.Descricao = solicitacaoExameItem.Descricao;
            solicitacaoExameItemDto.SolicitacaoExameId = solicitacaoExameItem.SolicitacaoExameId;
            solicitacaoExameItemDto.FaturamentoItemId = solicitacaoExameItem.FaturamentoItemId;
            solicitacaoExameItemDto.GuiaNumero = solicitacaoExameItem.GuiaNumero;
            solicitacaoExameItemDto.DataValidade = solicitacaoExameItem.DataValidade;
            solicitacaoExameItemDto.SenhaNumero = solicitacaoExameItem.SenhaNumero;
            solicitacaoExameItemDto.MaterialId = solicitacaoExameItem.MaterialId;
            solicitacaoExameItemDto.Justificativa = solicitacaoExameItem.Justificativa;
            solicitacaoExameItemDto.KitExameId = solicitacaoExameItem.KitExameId;

            if (solicitacaoExameItem.Solicitacao != null)
            {
                solicitacaoExameItemDto.Solicitacao = SolicitacaoExameDto.Mapear(solicitacaoExameItem.Solicitacao);
            }

            if (solicitacaoExameItem.FaturamentoItem != null)
            {
                solicitacaoExameItemDto.FaturamentoItem = FaturamentoItemDto.Mapear(solicitacaoExameItem.FaturamentoItem);
            }



            //public SolicitacaoExameDto Solicitacao 
            //public FaturamentoItemDto FaturamentoItem 
            //public MaterialDto Material 
            //public KitExameDto KitExame 

            return solicitacaoExameItemDto;
        }

        public static SolicitacaoExameItem Mapear(SolicitacaoExameItemDto solicitacaoExameItemDto)
        {
            SolicitacaoExameItem solicitacaoExameItem = new SolicitacaoExameItem();


            solicitacaoExameItem.Id = solicitacaoExameItemDto.Id;
            solicitacaoExameItem.Codigo = solicitacaoExameItemDto.Codigo;
            solicitacaoExameItem.Descricao = solicitacaoExameItemDto.Descricao;


            solicitacaoExameItem.SolicitacaoExameId = solicitacaoExameItemDto.SolicitacaoExameId;
            solicitacaoExameItem.FaturamentoItemId = solicitacaoExameItemDto.FaturamentoItemId;
            solicitacaoExameItem.GuiaNumero = solicitacaoExameItemDto.GuiaNumero;
            solicitacaoExameItem.DataValidade = solicitacaoExameItemDto.DataValidade;
            solicitacaoExameItem.SenhaNumero = solicitacaoExameItemDto.SenhaNumero;
            solicitacaoExameItem.MaterialId = solicitacaoExameItemDto.MaterialId;
            solicitacaoExameItem.Justificativa = solicitacaoExameItemDto.Justificativa;
            solicitacaoExameItem.KitExameId = solicitacaoExameItemDto.KitExameId;

            if(solicitacaoExameItemDto.Solicitacao!=null)
            {
                solicitacaoExameItem.Solicitacao = SolicitacaoExameDto.Mapear(solicitacaoExameItemDto.Solicitacao);
            }

            if(solicitacaoExameItemDto.FaturamentoItem!=null)
            {
                solicitacaoExameItem.FaturamentoItem = FaturamentoItemDto.Mapear(solicitacaoExameItemDto.FaturamentoItem);
            }

            //public FaturamentoItemDto FaturamentoItem 
            //public MaterialDto Material 
            //public KitExameDto KitExame 

            return solicitacaoExameItem;
        }

        public static List<SolicitacaoExameItem> Mapear(List<SolicitacaoExameItemDto> listDto)
        {
            List<SolicitacaoExameItem> list = new List<SolicitacaoExameItem>();

            foreach (var item in listDto)
            {
                list.Add(Mapear(item));
            }

            return list;
        }

        public static List<SolicitacaoExameItemDto> Mapear(List<SolicitacaoExameItem> list)
        {
            List<SolicitacaoExameItemDto> listDto = new List<SolicitacaoExameItemDto>();

            foreach (var item in list)
            {
                listDto.Add(Mapear(item));
            }

            return listDto;
        }


        #endregion

    }
}
