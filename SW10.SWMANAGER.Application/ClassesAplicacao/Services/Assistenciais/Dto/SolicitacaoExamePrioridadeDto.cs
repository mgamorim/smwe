﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto
{
    [AutoMap(typeof(SolicitacaoExamePrioridade))]
    public class SolicitacaoExamePrioridadeDto : CamposPadraoCRUDDto
    {
        public static SolicitacaoExamePrioridadeDto Mapear(SolicitacaoExamePrioridade input)
        {
            var result = new SolicitacaoExamePrioridadeDto();
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.Descricao = input.Descricao;
            result.Id = input.Id;
            result.IsSistema = input.IsSistema;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;

            return result;
        }

        public static SolicitacaoExamePrioridade Mapear(SolicitacaoExamePrioridadeDto input)
        {
            var result = new SolicitacaoExamePrioridade();
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.Descricao = input.Descricao;
            result.Id = input.Id;
            result.IsSistema = input.IsSistema;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;

            return result;
        }

        public static IEnumerable<SolicitacaoExamePrioridadeDto> Mapear(List<SolicitacaoExamePrioridade> input)
        {
            foreach (var item in input)
            {
                var result = new SolicitacaoExamePrioridadeDto();
                result.Codigo = item.Codigo;
                result.CreationTime = item.CreationTime;
                result.CreatorUserId = item.CreatorUserId;
                result.Descricao = item.Descricao;
                result.Id = item.Id;
                result.IsSistema = item.IsSistema;
                result.LastModificationTime = item.LastModificationTime;
                result.LastModifierUserId = item.LastModifierUserId;

                yield return result;
            }
        }

        public static IEnumerable<SolicitacaoExamePrioridade> Mapear(List<SolicitacaoExamePrioridadeDto> input)
        {
            foreach (var item in input)
            {
                var result = new SolicitacaoExamePrioridade();
                result.Codigo = item.Codigo;
                result.CreationTime = item.CreationTime;
                result.CreatorUserId = item.CreatorUserId;
                result.Descricao = item.Descricao;
                result.Id = item.Id;
                result.IsSistema = item.IsSistema;
                result.LastModificationTime = item.LastModificationTime;
                result.LastModifierUserId = item.LastModifierUserId;

                yield return result;
            }
        }
    }
}
