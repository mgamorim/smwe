﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto
{
    [AutoMap(typeof(SolicitacaoExame))]
    public class SolicitacaoExameDto : CamposPadraoCRUDDto
    {
        public long? AtendimentoId { get; set; }
        public AtendimentoDto Atendimento { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public long? OrigemId { get; set; }
        public long? LeitoId { get; set; }
        public LeitoDto Leito { get; set; }
        public int Prioridade { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }
        public long? MedicoSolicitanteId { get; set; }
        public MedicoDto MedicoSolicitante { get; set; }
        public string Observacao { get; set; }
        public long? PrescricaoId { get; set; }
        public PrescricaoMedicaDto Prescricao { get; set; }

        #region Mapeamento

        public static SolicitacaoExameDto Mapear(SolicitacaoExame solicitacaoExame)
        {
            SolicitacaoExameDto solicitacaoExameDto = new SolicitacaoExameDto();

            solicitacaoExameDto.Id = solicitacaoExame.Id;
            solicitacaoExameDto.Codigo = solicitacaoExame.Codigo;
            solicitacaoExameDto.Descricao = solicitacaoExame.Descricao;
            solicitacaoExameDto.AtendimentoId = solicitacaoExame.AtendimentoId;
            solicitacaoExameDto.DataSolicitacao = solicitacaoExame.DataSolicitacao;
            solicitacaoExameDto.OrigemId = solicitacaoExame.OrigemId;
            solicitacaoExameDto.LeitoId = solicitacaoExameDto.LeitoId;
            solicitacaoExameDto.Prioridade = solicitacaoExameDto.Prioridade;
            solicitacaoExameDto.UnidadeOrganizacionalId = solicitacaoExame.UnidadeOrganizacionalId;
            solicitacaoExameDto.MedicoSolicitanteId = solicitacaoExame.MedicoSolicitanteId;
            solicitacaoExameDto.Observacao = solicitacaoExame.Observacao;
            solicitacaoExameDto.PrescricaoId= solicitacaoExame.PrescricaoId;

            if(solicitacaoExame.Atendimento!=null)
            {
                solicitacaoExameDto.Atendimento = AtendimentoDto.Mapear(solicitacaoExame.Atendimento);
            }

            if(solicitacaoExame.Leito!=null)
            {
                solicitacaoExameDto.Leito = LeitoDto.Mapear(solicitacaoExame.Leito);
            }

            if(solicitacaoExame.UnidadeOrganizacional!=null)
            {
                solicitacaoExameDto.UnidadeOrganizacional = UnidadeOrganizacionalDto.Mapear(solicitacaoExame.UnidadeOrganizacional);
            }

            if(solicitacaoExame.MedicoSolicitante!=null)
            {
                solicitacaoExameDto.MedicoSolicitante = MedicoDto.Mapear(solicitacaoExame.MedicoSolicitante);
            }

            return solicitacaoExameDto;
        }

        public static SolicitacaoExame Mapear(SolicitacaoExameDto solicitacaoExameDto)
        {
            SolicitacaoExame solicitacaoExame = new SolicitacaoExame();

            solicitacaoExame.Id = solicitacaoExameDto.Id;
            solicitacaoExame.Codigo = solicitacaoExameDto.Codigo;
            solicitacaoExame.Descricao = solicitacaoExameDto.Descricao;
            solicitacaoExame.AtendimentoId = solicitacaoExameDto.AtendimentoId;
            solicitacaoExame.DataSolicitacao = solicitacaoExameDto.DataSolicitacao;
            solicitacaoExame.OrigemId = solicitacaoExameDto.OrigemId;
            solicitacaoExame.LeitoId = solicitacaoExameDto.LeitoId;
            solicitacaoExame.Prioridade = solicitacaoExameDto.Prioridade;
            solicitacaoExame.UnidadeOrganizacionalId = solicitacaoExameDto.UnidadeOrganizacionalId;
            solicitacaoExame.MedicoSolicitanteId = solicitacaoExameDto.MedicoSolicitanteId;
            solicitacaoExame.Observacao = solicitacaoExameDto.Observacao;
            solicitacaoExame.PrescricaoId = solicitacaoExameDto.PrescricaoId;

            if (solicitacaoExameDto.Atendimento != null)
            {
                solicitacaoExame.Atendimento = AtendimentoDto.Mapear(solicitacaoExameDto.Atendimento);
            }

            if (solicitacaoExameDto.Leito != null)
            {
                solicitacaoExame.Leito = LeitoDto.Mapear(solicitacaoExameDto.Leito);
            }

            if (solicitacaoExameDto.UnidadeOrganizacional != null)
            {
                solicitacaoExame.UnidadeOrganizacional = UnidadeOrganizacionalDto.Mapear(solicitacaoExameDto.UnidadeOrganizacional);
            }

            if (solicitacaoExameDto.MedicoSolicitante != null)
            {
                solicitacaoExame.MedicoSolicitante = MedicoDto.Mapear(solicitacaoExameDto.MedicoSolicitante);
            }

            return solicitacaoExame;
        }

        public static IEnumerable<SolicitacaoExameDto> Mapear(List<SolicitacaoExame> solicitacaoExame)
        {
            foreach (var item in solicitacaoExame)
            {
                SolicitacaoExameDto solicitacaoExameDto = new SolicitacaoExameDto();

                solicitacaoExameDto.Id = item.Id;
                solicitacaoExameDto.Codigo = item.Codigo;
                solicitacaoExameDto.Descricao = item.Descricao;
                solicitacaoExameDto.AtendimentoId = item.AtendimentoId;
                solicitacaoExameDto.DataSolicitacao = item.DataSolicitacao;
                solicitacaoExameDto.OrigemId = item.OrigemId;
                solicitacaoExameDto.LeitoId = solicitacaoExameDto.LeitoId;
                solicitacaoExameDto.Prioridade = solicitacaoExameDto.Prioridade;
                solicitacaoExameDto.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                solicitacaoExameDto.MedicoSolicitanteId = item.MedicoSolicitanteId;
                solicitacaoExameDto.Observacao = item.Observacao;
                solicitacaoExameDto.PrescricaoId = item.PrescricaoId;

                if (item.Atendimento != null)
                {
                    solicitacaoExameDto.Atendimento = AtendimentoDto.Mapear(item.Atendimento);
                }

                if (item.Leito != null)
                {
                    solicitacaoExameDto.Leito = LeitoDto.Mapear(item.Leito);
                }

                if (item.UnidadeOrganizacional != null)
                {
                    solicitacaoExameDto.UnidadeOrganizacional = UnidadeOrganizacionalDto.Mapear(item.UnidadeOrganizacional);
                }

                if (item.MedicoSolicitante != null)
                {
                    solicitacaoExameDto.MedicoSolicitante = MedicoDto.Mapear(item.MedicoSolicitante);
                }

                yield return solicitacaoExameDto;
            }
        }

        public static IEnumerable<SolicitacaoExame> Mapear(List<SolicitacaoExameDto> solicitacaoExameDto)
        {
            foreach (var item in solicitacaoExameDto)
            {
                SolicitacaoExame solicitacaoExame = new SolicitacaoExame();

                solicitacaoExame.Id = item.Id;
                solicitacaoExame.Codigo = item.Codigo;
                solicitacaoExame.Descricao = item.Descricao;
                solicitacaoExame.AtendimentoId = item.AtendimentoId;
                solicitacaoExame.DataSolicitacao = item.DataSolicitacao;
                solicitacaoExame.OrigemId = item.OrigemId;
                solicitacaoExame.LeitoId = item.LeitoId;
                solicitacaoExame.Prioridade = item.Prioridade;
                solicitacaoExame.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                solicitacaoExame.MedicoSolicitanteId = item.MedicoSolicitanteId;
                solicitacaoExame.Observacao = item.Observacao;
                solicitacaoExame.PrescricaoId = item.PrescricaoId;

                if (item.Atendimento != null)
                {
                    solicitacaoExame.Atendimento = AtendimentoDto.Mapear(item.Atendimento);
                }

                if (item.Leito != null)
                {
                    solicitacaoExame.Leito = LeitoDto.Mapear(item.Leito);
                }

                if (item.UnidadeOrganizacional != null)
                {
                    solicitacaoExame.UnidadeOrganizacional = UnidadeOrganizacionalDto.Mapear(item.UnidadeOrganizacional);
                }

                if (item.MedicoSolicitante != null)
                {
                    solicitacaoExame.MedicoSolicitante = MedicoDto.Mapear(item.MedicoSolicitante);
                }

                yield return solicitacaoExame;
            }
        }

        #endregion


        //public virtual ICollection<SolicitacaoExameItemDto> SolicitacaoExameItens { get; set; }
    }
}
