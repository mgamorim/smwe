﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.KitsExames;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Materiais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto;
using MoreLinq;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public class SolicitacaoExameItemAppService : SWMANAGERAppServiceBase, ISolicitacaoExameItemAppService
    {
        private readonly IRepository<SolicitacaoExameItem, long> _solicitacaoExameItemRepository;
        private readonly IRepository<LaudoMovimentoItem, long> _laudoMovimentoItemRepository;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IFaturamentoItemAppService _faturamentoItemAppService;
        private readonly IMaterialAppService _materialItemAppService;
        private readonly IKitExameAppService _kitExameAppService;
        private readonly ISolicitacaoExameAppService _solicitacaoExameAppService;
        private readonly IRepository<SolicitacaoExame, long> _solicitacaoExameRepository;


        public SolicitacaoExameItemAppService(
            IRepository<SolicitacaoExameItem, long> solicitacaoExameItemRepository,
            IRepository<LaudoMovimentoItem, long> laudoMovimentoItemRepository,
            IAtendimentoAppService atendimentoAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoIdAppService,
            IFaturamentoItemAppService faturamentoItemAppService,
            IMaterialAppService materialAppService,
            IKitExameAppService kitExameAppService,
            ISolicitacaoExameAppService solicitacaoExameAppService,
            IRepository<SolicitacaoExame, long> solicitacaoExameRepository
            )
        {
            _solicitacaoExameItemRepository = solicitacaoExameItemRepository;
            _laudoMovimentoItemRepository = laudoMovimentoItemRepository;
            _atendimentoAppService = atendimentoAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoIdAppService;
            _faturamentoItemAppService = faturamentoItemAppService;
            _materialItemAppService = materialAppService;
            _kitExameAppService = kitExameAppService;
            _solicitacaoExameAppService = solicitacaoExameAppService;
            _solicitacaoExameRepository = solicitacaoExameRepository;
        }

        [UnitOfWork]
        public async Task<SolicitacaoExameItemDto> CriarOuEditar(SolicitacaoExameItemDto input)
        {
            try
            {
                var solicitacaoExameItem = new SolicitacaoExameItem
                {
                    Codigo = input.Codigo,
                    CreationTime = input.CreationTime,
                    CreatorUserId = input.CreatorUserId,
                    DataValidade = input.DataValidade,
                    DeleterUserId = input.DeleterUserId,
                    DeletionTime = input.DeletionTime,
                    Descricao = input.Descricao,
                    FaturamentoItemId = input.FaturamentoItemId,
                    GuiaNumero = input.GuiaNumero,
                    Id = input.Id,
                    IsDeleted = input.IsDeleted,
                    IsSistema = input.IsSistema,
                    Justificativa = input.Justificativa,
                    KitExameId = input.KitExameId,
                    LastModificationTime = input.LastModificationTime,
                    LastModifierUserId = input.LastModifierUserId,
                    MaterialId = input.MaterialId,
                    SenhaNumero = input.SenhaNumero,
                    SolicitacaoExameId = input.SolicitacaoExameId
                };
                if (input.Id.Equals(0))
                {
                    //var ultimosId = await _ultimoIdAppService.ListarTodos();
                    //var ultimoId = ultimosId.Items.Where(m => m.NomeTabela == "SolicitacaoExameItem").FirstOrDefault();
                    //solicitacaoExameItem.Codigo = ultimoId.Codigo;
                    //input.Codigo = solicitacaoExameItem.Codigo;
                    //var codigo = Convert.ToInt64(ultimoId.Codigo);
                    //codigo++;
                    //ultimoId.Codigo = codigo.ToString();
                    //await _ultimoIdAppService.CriarOuEditar(ultimoId);
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _solicitacaoExameItemRepository.InsertAndGetIdAsync(solicitacaoExameItem);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _solicitacaoExameItemRepository.UpdateAsync(solicitacaoExameItem);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _solicitacaoExameItemRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<SolicitacaoExameItemDto>> ListarTodos()
        {
            try
            {
                var query = _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    //.Include(m => m.KitExame)
                    .Include(m => m.Material)
                    //.Include(m => m.Solicitacao);
                    ;

                var admissoesMedicasDto = await query.ToListAsync();

                return new ListResultDto<SolicitacaoExameItemDto> { Items = SolicitacaoExameItemDto.Mapear(admissoesMedicasDto).ToList() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<SolicitacaoExameItemList>> Listar(ListarSolicitacaoExameItensInput input)
        {
            var contarSolicitacaoExameItens = 0;
            List<SolicitacaoExameItemList> admissoesMedicasDto = new List<SolicitacaoExameItemList>();
            try
            {
                var query = _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    //.Include(m => m.KitExame)
                    .Include(m => m.Material)
                    //.Include(m => m.Solicitacao)
                    .Where(m => m.SolicitacaoExameId == input.SolicitacaoExameId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        //m.CreationTime.IsBetween(input.StartDate, input.EndDate) || //.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Material.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Material.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.MedicoSolicitante.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.Nascimento.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.Rg.ToUpper().Contains(input.Filtro.ToUpper())
                    )
                    //.AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input);
                contarSolicitacaoExameItens = await query.CountAsync();
                var solicitacoesExameItem = await query.Select(m => new SolicitacaoExameItemList
                {
                    FaturamentoItem = m.FaturamentoItem.Descricao,
                    GuiaNumero = m.GuiaNumero,
                    Id = m.Id,
                    IsDeleted = m.IsDeleted,
                    IsSistema = m.IsSistema,
                    Material = m.Material.Descricao
                }).ToListAsync();
                return new PagedResultDto<SolicitacaoExameItemList>(
                    contarSolicitacaoExameItens,
                    solicitacoesExameItem
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<SolicitacaoExameItemList>> ListarAtendimento(ListarSolicitacaoExameItensInput input)
        {
            //input.Id = id.ToString();
            //input.MaxResultCount = 50;
            var contarSolicitacaoExameItens = 0;
            List<SolicitacaoExameItemList> admissoesMedicasDto = new List<SolicitacaoExameItemList>();
            try
            {
                long atendimentoId = 0;

                var boolAte = long.TryParse(input.Id, out atendimentoId);
                var query = _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    //.Include(m => m.KitExame)
                    .Include(m => m.Material)
                    .Include(m => m.Solicitacao)
                    .Where(w => w.Solicitacao.AtendimentoId == atendimentoId && (w.StatusSolicitacaoExameItemId == 1 || w.StatusSolicitacaoExameItemId == 3 || w.StatusSolicitacaoExameItemId == null))
                    //.WhereIf(atendimentoId > 0, m => m.Solicitacao.AtendimentoId == atendimentoId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        //m.CreationTime.IsBetween(input.StartDate, input.EndDate) || //.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Material.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Material.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.MedicoSolicitante.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.Nascimento.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Solicitacao.Atendimento.Paciente.Rg.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarSolicitacaoExameItens = await query.CountAsync();

                var solicitacaoList = await query
                    //.AsQueryable()
                    //.AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var solicitacoesExameItem = solicitacaoList.Select(m => new SolicitacaoExameItemList
                {
                    FaturamentoItem = m.FaturamentoItem?.Descricao,
                    GuiaNumero = m.GuiaNumero,
                    Id = m.Id,
                    IsDeleted = m.IsDeleted,
                    IsSistema = m.IsSistema,
                    Material = m.Material?.Descricao
                }).ToList();
                return new PagedResultDto<SolicitacaoExameItemList>(
                    contarSolicitacaoExameItens,
                    solicitacoesExameItem
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<SolicitacaoExameItemDto> Obter(long id)
        {
            try
            {
                var query = await _solicitacaoExameItemRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    //.Include(m => m.FaturamentoItem)
                    //.Include(m => m.KitExame)
                    //.Include(m => m.Material)
                    //.Include(m => m.Solicitacao)
                    //.Include(m => m.Solicitacao.Atendimento)
                    //.Include(m => m.Solicitacao.Atendimento.Paciente)
                    .Select(m => new SolicitacaoExameItemDto
                    {
                        Codigo = m.Codigo,
                        CreationTime = m.CreationTime,
                        CreatorUserId = m.CreatorUserId,
                        DataValidade = m.DataValidade,
                        DeleterUserId = m.DeleterUserId,
                        DeletionTime = m.DeletionTime,
                        Descricao = m.Descricao,
                        FaturamentoItemId = m.FaturamentoItemId,
                        GuiaNumero = m.GuiaNumero,
                        Id = m.Id,
                        IsDeleted = m.IsDeleted,
                        IsSistema = m.IsSistema,
                        Justificativa = m.Justificativa,
                        KitExameId = m.KitExameId,
                        LastModificationTime = m.LastModificationTime,
                        LastModifierUserId = m.LastModifierUserId,
                        MaterialId = m.MaterialId,
                        SenhaNumero = m.SenhaNumero,
                        SolicitacaoExameId = m.SolicitacaoExameId
                    })
                    .FirstOrDefaultAsync();

                var result = query;


                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<List<SolicitacaoExameItemDto>> ObterPorLista(List<long> ids)
        {
            try
            {
                var result = await _solicitacaoExameItemRepository
                    .GetAll()
                    .Where(m => ids.Any(a => a == m.Id))
                    .Select(m => new SolicitacaoExameItemDto
                    {
                        Codigo = m.Codigo,
                        CreationTime = m.CreationTime,
                        CreatorUserId = m.CreatorUserId,
                        DataValidade = m.DataValidade,
                        DeleterUserId = m.DeleterUserId,
                        DeletionTime = m.DeletionTime,
                        Descricao = m.Descricao,
                        FaturamentoItemId = m.FaturamentoItemId,
                        GuiaNumero = m.GuiaNumero,
                        Id = m.Id,
                        IsDeleted = m.IsDeleted,
                        IsSistema = m.IsSistema,
                        Justificativa = m.Justificativa,
                        KitExameId = m.KitExameId,
                        LastModificationTime = m.LastModificationTime,
                        LastModifierUserId = m.LastModifierUserId,
                        MaterialId = m.MaterialId,
                        SenhaNumero = m.SenhaNumero,
                        SolicitacaoExameId = m.SolicitacaoExameId
                    }).ToListAsync();


                if (result != null && result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        if (item.SolicitacaoExameId != 0)
                        {
                            var solicitacao = _solicitacaoExameRepository.GetAll()
                                                                         .Where(w => w.Id == item.SolicitacaoExameId).
                                                                         FirstOrDefault();
                            item.Solicitacao = new SolicitacaoExameDto();

                            if (solicitacao.AtendimentoId != null)
                            {
                                item.Solicitacao.Atendimento = await _atendimentoAppService.Obter((long)solicitacao.AtendimentoId);
                            }
                        }

                        if (item.FaturamentoItemId != null)
                        {
                            item.FaturamentoItem = await _faturamentoItemAppService.Obter((long)item.FaturamentoItemId);
                        }


                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<SolicitacaoExameItemDto> ObterParaEdicao(long id)
        {
            try
            {
                var query = await _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Material)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var result = SolicitacaoExameItemDto.Mapear(query);
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<SolicitacaoExameItemDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.KitExame)
                    .Include(m => m.Material)
                    .Include(m => m.Solicitacao)
                    .WhereIf(!filtro.IsNullOrWhiteSpace(), m =>
                         m.CreationTime.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Medico.NomeCompleto.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Medico.Cpf.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Medico.Nascimento.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Medico.Rg.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Paciente.Cpf.ToUpper().Contains(filtro.ToUpper()) ||
                         ((DateTime)m.Solicitacao.Atendimento.Paciente.Nascimento).ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                         m.Solicitacao.Atendimento.Paciente.Rg.ToUpper().Contains(filtro.ToUpper())
                    );

                var admissoesMedicas = await query.ToListAsync();
                var admissoesMedicasDto = SolicitacaoExameItemDto.Mapear(admissoesMedicas).ToList();

                return new ListResultDto<SolicitacaoExameItemDto> { Items = admissoesMedicasDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<SolicitacaoExameItemList>> ListarPorSolicitacao(ListarSolicitacaoExameItensInput input)
        {
            try
            {
                var id = Convert.ToInt64(input.Filtro);
                var query = _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    //.Include(m => m.KitExame)
                    .Include(m => m.Material)
                    //.Include(m => m.Solicitacao)
                    .Where(m => m.SolicitacaoExameId == id);

                var count = await query.CountAsync();
                var listDto = await query.Select(m => new SolicitacaoExameItemList
                {
                    FaturamentoItem = m.FaturamentoItem.Descricao,
                    GuiaNumero = m.GuiaNumero,
                    Id = m.Id,
                    IsDeleted = m.IsDeleted,
                    IsSistema = m.IsSistema,
                    Material = m.Material.Descricao
                }).ToListAsync();

                return new PagedResultDto<SolicitacaoExameItemList>
                {
                    TotalCount = count,
                    Items = listDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<SolicitacaoExameItemDto> faturamentoItensDto = new List<SolicitacaoExameItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var solicitacaoExameItens = _solicitacaoExameItemRepository.GetAll()

                    .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            .Include(s => s.Solicitacao)
                            .Include(a => a.Solicitacao.Atendimento)
                            .ToList()
                            ;

                var laudoMovs = _laudoMovimentoItemRepository.GetAll()
                    .Include(s => s.SolicitacaoExameItem.Solicitacao.Atendimento)
                    // .Where(l=>l.SolicitacaoExameItem.Solicitacao.AtendimentoId.Equals(dropdownInput.filtro))
                    .ToList();


                List<SolicitacaoExameItem> naoPertencentes = new List<SolicitacaoExameItem>();
                for (int i = 0; i < solicitacaoExameItens.Count(); i++)
                {
                    bool removido = false;
                    if (solicitacaoExameItens[i].Solicitacao.Atendimento.Id.ToString() != dropdownInput.filtro)
                    {
                        naoPertencentes.Add(solicitacaoExameItens[i]);
                        removido = true;
                    }

                    foreach (var laudoMovItem in laudoMovs)
                    {
                        if (laudoMovItem.SolicitacaoExameItem != null && laudoMovItem.SolicitacaoExameItem == solicitacaoExameItens[i] && !removido)
                        {
                            naoPertencentes.Add(solicitacaoExameItens[i]);
                        }
                    }
                }

                foreach (var sei in naoPertencentes)
                {
                    solicitacaoExameItens.Remove(sei);
                }




                var seiIds = solicitacaoExameItens.Select(s => s.Id);

                var query = from p in _solicitacaoExameItemRepository.GetAll()
                        .Where(t => seiIds.Contains(t.Id))

                                //var query = (IQueryable<DropdownItems>)
                                //        from p in solicitacaoExameItens
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdownNaoRegistradoPorAtendimento(DropdownInput dropdownInput)
        {
            long atendimentoId;
            DateTime dataHoraRegistro;

            long.TryParse(dropdownInput.filtros[0], out atendimentoId);
            DateTime.TryParse(dropdownInput.filtros[1], out dataHoraRegistro);

            var dataHoraMinimaSolicitacaoExame = dataHoraRegistro.AddHours(-48);


            return await base.ListarDropdownLambda(dropdownInput
                                      , _solicitacaoExameItemRepository
                                      , w => (w.Solicitacao.AtendimentoId == atendimentoId
                                      && w.Solicitacao.DataSolicitacao >= dataHoraMinimaSolicitacaoExame
                                      && (w.StatusSolicitacaoExameItemId == (long)EnumSolicitacaoExameItem.Liberado || w.StatusSolicitacaoExameItemId == null)
                                      && (w.FaturamentoItem.IsLaudo || w.FaturamentoItem.Grupo.IsLaudo)
                                      )
                                     , p => new DropdownItems { id = p.FaturamentoItem.Id, text = string.Concat(p.FaturamentoItem.Codigo.ToString(), " - ", p.FaturamentoItem.Descricao) }
                                     , o => o.FaturamentoItem.Codigo);
        }



        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdownExameLaboratorioNaoRegistradoPorAtendimento(DropdownInput dropdownInput)
        {
            long atendimentoId;
            DateTime dataHoraRegistro;

            long.TryParse(dropdownInput.filtros[0], out atendimentoId);
            //DateTime.TryParse(dropdownInput.filtros[1], out dataHoraRegistro);

            //var dataHoraMinimaSolicitacaoExame = dataHoraRegistro.AddHours(-48);


            return await base.ListarDropdownLambda(dropdownInput
                                      , _solicitacaoExameItemRepository
                                      , w => (w.Solicitacao.AtendimentoId == atendimentoId
                                      // && w.Solicitacao.DataSolicitacao >= dataHoraMinimaSolicitacaoExame
                                      && (w.StatusSolicitacaoExameItemId == (long)EnumSolicitacaoExameItem.Liberado || w.StatusSolicitacaoExameItemId == null)
                                      && (w.FaturamentoItem.IsLaboratorio || w.FaturamentoItem.Grupo.IsLaboratorio)
                                      )
                                     , p => new DropdownItems { id = p.FaturamentoItem.Id, text = string.Concat(p.FaturamentoItem.Codigo.ToString(), " - ", p.FaturamentoItem.Descricao) }
                                     , o => o.FaturamentoItem.Codigo);
        }



        [UnitOfWork(false)]
        public async Task<PagedResultDto<RegistroExameIndex>> ListarExamesImagensNaoRegistrados(ListarExameSolicitadosInput input)
        {
            var examesNaoRegistrados = _solicitacaoExameItemRepository.GetAll()
                                                                      .Include(i => i.FaturamentoItem)
                                                                      .Include(i => i.Solicitacao.Atendimento.Paciente)
                                                                      .Include(i => i.Solicitacao.Atendimento.Paciente.SisPessoa)
                                                                      .Include(i => i.Solicitacao.Atendimento.Convenio)
                                                                      .Include(i => i.Solicitacao.Atendimento.Convenio.SisPessoa)
                                                                      .Where(w => (w.StatusSolicitacaoExameItemId == (long)EnumSolicitacaoExameItem.Liberado || w.StatusSolicitacaoExameItemId == null)
                                                                               && (w.FaturamentoItem.IsLaudo || w.FaturamentoItem.Grupo.IsLaudo)
                                                                               && (input.AtendimentoId == null || w.Solicitacao.AtendimentoId == input.AtendimentoId)
                                                                               )
                                                                      .ToList();


            List<RegistroExameIndex> exames = new List<RegistroExameIndex>();
            RegistroExameIndex exame;

            foreach (var item in examesNaoRegistrados)
            {
                exame = new RegistroExameIndex();

                exame.Id = item.Id;
                exame.Exame = item.FaturamentoItem?.Descricao;
                exame.PacienteDescricao = item.Solicitacao?.Atendimento?.Paciente?.NomeCompleto;
                exame.ConvenioDescricao = item.Solicitacao?.Atendimento?.Convenio?.NomeFantasia;
                exame.AtendimentoId = item.Solicitacao?.Atendimento?.Id;
                exame.Data = item.Solicitacao?.DataSolicitacao;
                exames.Add(exame);
            }

            var _examesNaoRegistrados = exames.AsQueryable()
                .AsNoTracking()
               //.OrderBy(input.Sorting)
               .PageBy(input)
               .ToList();



            return new PagedResultDto<RegistroExameIndex>(examesNaoRegistrados.Count, _examesNaoRegistrados);

        }


        public async Task<PagedResultDto<RegistroExameIndex>> ListarExamesLaboratoriaisNaoColetados(ListarExameSolicitadosInput input)
        {
            try
            {
                var examesNaoRegistrados = _solicitacaoExameItemRepository
                    .GetAll()
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.Solicitacao)
                    .Include(i => i.Solicitacao.Atendimento)
                    .Include(i => i.Solicitacao.Atendimento.Paciente)
                    .Include(i => i.Solicitacao.Atendimento.Paciente.SisPessoa)
                    .Include(i => i.Solicitacao.Atendimento.Medico)
                    .Include(i => i.Solicitacao.Atendimento.Medico.SisPessoa)
                    .Include(i => i.Solicitacao.Atendimento.Convenio)
                    .Include(i => i.Solicitacao.Atendimento.Convenio.SisPessoa)
                    .Include(i => i.Solicitacao.Atendimento.Leito)
                    .Include(i => i.Solicitacao.Atendimento.Leito.TipoAcomodacao)
                    .Where(w => (w.StatusSolicitacaoExameItemId == (long)EnumSolicitacaoExameItem.Liberado || w.StatusSolicitacaoExameItemId == null)
                                 && (w.FaturamentoItem.IsLaboratorio || w.FaturamentoItem.Grupo.IsLaboratorio)
                    //&& (input.AtendimentoId != null || w.Solicitacao.AtendimentoId == input.AtendimentoId)
                    )
                    //.Select(m => m.Solicitacao.Atendimento)
                    .Distinct()
                    .ToList()
                    .GroupBy(m => m.Solicitacao.AtendimentoId)
                    .Select(m => new { AtendimentoId = m.Key, Solicitacao = m });

                List<RegistroExameIndex> exames = new List<RegistroExameIndex>();
                RegistroExameIndex exame;

                foreach (var atendimento in examesNaoRegistrados)
                {
                    var item = atendimento.Solicitacao.DistinctBy(m=>m.Solicitacao.AtendimentoId).FirstOrDefault();
                    //foreach (var item in atendimento.Solicitacao)
                    //{
                        exame = new RegistroExameIndex();
                        exame.Id = item.Id;
                        //exame.Exame = item.FaturamentoItem?.Descricao;
                        exame.PacienteDescricao = item.Solicitacao.Atendimento.Paciente?.SisPessoa?.NomeCompleto;
                        exame.ConvenioDescricao = item.Solicitacao.Atendimento.Convenio?.SisPessoa?.NomeFantasia;
                        exame.AtendimentoId = item.Solicitacao.Atendimento.Id;
                        exame.Data = item.Solicitacao.Atendimento.DataRegistro;
                        exame.Medico = item.Solicitacao.Atendimento.Medico.SisPessoa.NomeCompleto;
                        exame.Codigo = item.Solicitacao.Atendimento.Codigo;
                        exame.Leito = item.Solicitacao.Atendimento.Leito?.Descricao;
                        exame.UnidadeOrganizacional = item.Solicitacao.Atendimento.UnidadeOrganizacional?.Descricao;
                        exame.TipoAtendimento = item.Solicitacao.Atendimento.IsInternacao ? "Internação" : "Ambulatório/Emergêcia";
                        exame.TipoLeito = item.Solicitacao.Atendimento.Leito?.TipoAcomodacao?.Descricao;
                        exames.Add(exame);
                    }
                //}

                if (input.AtendimentoId != null)
                {
                    input.MaxResultCount = exames.Count;
                }



                var _examesNaoRegistrados = exames.AsQueryable()
                    .AsNoTracking()
                   //.OrderBy(input.Sorting)
                   .PageBy(input)
                   .ToList();

                return new PagedResultDto<RegistroExameIndex>(examesNaoRegistrados.Count(), _examesNaoRegistrados);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
