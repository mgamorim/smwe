﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public class SolicitacaoExameAppService : SWMANAGERAppServiceBase, ISolicitacaoExameAppService
    {
        private readonly IRepository<SolicitacaoExame, long> _solicitacaoExameRepository;
        private readonly IRepository<SolicitacaoExameItem, long> _solicitacaoExameItemRepository;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;

        public SolicitacaoExameAppService(
            IRepository<SolicitacaoExame, long> solicitacaoExameRepository,
            IRepository<SolicitacaoExameItem, long> solicitacaoExameItemRepository,
            IAtendimentoAppService atendimentoAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoIdAppService
            )
        {
            _solicitacaoExameRepository = solicitacaoExameRepository;
            _solicitacaoExameItemRepository = solicitacaoExameItemRepository;
            _atendimentoAppService = atendimentoAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoIdAppService;
        }

        [UnitOfWork]
        public async Task<SolicitacaoExameDto> CriarOuEditar(SolicitacaoExameDto input)
        {
            try
            {
                var solicitacaoExame = new SolicitacaoExame
                {
                    AtendimentoId = input.AtendimentoId,
                    Codigo = input.Codigo,
                    CreationTime = input.CreationTime,
                    CreatorUserId = input.CreatorUserId,
                    DataSolicitacao = input.DataSolicitacao,
                    DeleterUserId = input.DeleterUserId,
                    DeletionTime = input.DeletionTime,
                    Descricao = input.Descricao,
                    Id = input.Id,
                    IsDeleted = input.IsDeleted,
                    IsSistema = input.IsSistema,
                    LastModificationTime = input.LastModificationTime,
                    LastModifierUserId = input.LastModifierUserId,
                    LeitoId = input.LeitoId,
                    MedicoSolicitanteId = input.MedicoSolicitanteId,
                    Observacao = input.Observacao,
                    OrigemId = input.OrigemId,
                    PrescricaoId = input.PrescricaoId,
                    Prioridade = input.Prioridade,
                    UnidadeOrganizacionalId = input.UnidadeOrganizacionalId
                };

                //input.MapTo<SolicitacaoExame>();
                if (input.Id.Equals(0))
                {
                    var ultimosId = await _ultimoIdAppService.ListarTodos();
                    var ultimoId = ultimosId.Items.Where(m => m.NomeTabela == "SolicitacaoExame").FirstOrDefault();
                    solicitacaoExame.Codigo = ultimoId.Codigo;
                    input.Codigo = solicitacaoExame.Codigo;
                    var codigo = Convert.ToInt64(ultimoId.Codigo);
                    codigo++;
                    ultimoId.Codigo = codigo.ToString();
                    await _ultimoIdAppService.CriarOuEditar(ultimoId);
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _solicitacaoExameRepository.InsertAndGetIdAsync(solicitacaoExame);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _solicitacaoExameRepository.UpdateAsync(solicitacaoExame);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                        //input = _solicitacaoExame.MapTo<SolicitacaoExameDto>();
                    }
                }
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _solicitacaoExameRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<SolicitacaoExameDto>> ListarTodos()
        {
            try
            {
                var query = _solicitacaoExameRepository
                    .GetAll()
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    .Include(m => m.Atendimento.Medico)
                    .Include(m => m.Atendimento.Medico.SisPessoa)
                    .Include(m => m.Atendimento.Empresa)
                    .Include(m => m.UnidadeOrganizacional);


                var admissoesMedicasDto = await query.ToListAsync();

                return new ListResultDto<SolicitacaoExameDto> { Items = SolicitacaoExameDto.Mapear(admissoesMedicasDto).ToList() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<SolicitacaoExameIndex>> Listar(ListarSolicitacoesExamesInput input)
        {
            var contar = 0;
            List<SolicitacaoExameIndex> listDto = new List<SolicitacaoExameIndex>();
            try
            {
                var query = _solicitacaoExameRepository
                    .GetAll()
                    //.Include(m => m.Atendimento.Paciente)
                    //.Include(m => m.Atendimento.Medico)
                    //.Include(m => m.Atendimento.Empresa)
                    .Include(m => m.Atendimento.UnidadeOrganizacional)
                    .Include(m => m.MedicoSolicitante)
                    //.Include(m => m.Leito)
                    .Where(m => m.CreationTime >= input.StartDate && m.CreationTime <= input.EndDate)
                    .Where(m => m.Atendimento.PacienteId == input.PacienteId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        //m.CreationTime.IsBetween(input.StartDate, input.EndDate) || //.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Medico.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Medico.Nascimento.ToShortTimeString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Medico.Rg.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Paciente.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        ((DateTime)m.Atendimento.Paciente.Nascimento).ToShortTimeString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Atendimento.Paciente.Rg.ToUpper().Contains(input.Filtro.ToUpper())
                    )
                    .Select(m => new SolicitacaoExameIndex
                    {
                        Id = m.Id,
                        Codigo = m.Codigo,
                        DataSolicitacao = m.DataSolicitacao,
                        MedicoSolicitante = m.MedicoSolicitante.NomeCompleto,
                        UnidadeOrganizacional = m.UnidadeOrganizacional.Descricao,
                        IsDeleted = m.IsDeleted
                    });

                contar = await query.CountAsync();

                var list = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                listDto = list;

                return new PagedResultDto<SolicitacaoExameIndex>(
                    contar,
                    listDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<SolicitacaoExameDto> Obter(long id)
        {
            try
            {
                var query = await _solicitacaoExameRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Include(m => m.Atendimento)
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    //.Include(m => m.Atendimento.Medico)
                    //.Include(m => m.Atendimento.Empresa)
                    //.Include(m => m.UnidadeOrganizacional)
                    //.Include(m => m.MedicoSolicitante)
                    //.Include(m => m.Leito)
                    //.Include(m => m.SolicitacaoExameItens)
                    .Select(m => new SolicitacaoExameDto
                    {
                        AtendimentoId = m.AtendimentoId,
                        Atendimento = new AtendimentoDto
                        {
                            AtendimentoStatusId = m.Atendimento.AtendimentoStatusId,
                            AtendimentoTipoId = m.Atendimento.AtendimentoTipoId,
                            Codigo = m.Atendimento.Codigo,
                            ConvenioId = m.Atendimento.ConvenioId,
                            CpfResponsavel = m.Atendimento.CpfResponsavel,
                            CreationTime = m.Atendimento.CreationTime,
                            CreatorUserId = m.Atendimento.CreatorUserId,
                            DataAlta = m.Atendimento.DataAlta,
                            DataPreatendimento = m.Atendimento.DataPreatendimento,
                            DataPrevistaAtendimento = m.Atendimento.DataPrevistaAtendimento,
                            DataRegistro = m.Atendimento.DataRegistro,
                            DataRetorno = m.Atendimento.DataRetorno,
                            DataRevisao = m.Atendimento.DataRevisao,
                            DeleterUserId = m.Atendimento.DeleterUserId,
                            DeletionTime = m.Atendimento.DeletionTime,
                            Descricao = m.Atendimento.Descricao,
                            EmpresaId = m.Atendimento.EmpresaId,
                            EspecialidadeId = m.Atendimento.EspecialidadeId,
                            GuiaId = m.Atendimento.GuiaId,
                            GuiaNumero = m.Atendimento.GuiaNumero,
                            Id = m.Atendimento.Id,
                            IsAmbulatorioEmergencia = m.Atendimento.IsAmbulatorioEmergencia,
                            IsDeleted = m.Atendimento.IsDeleted,
                            IsHomeCare = m.Atendimento.IsHomeCare,
                            IsInternacao = m.Atendimento.IsInternacao,
                            IsPreatendimento = m.Atendimento.IsPreatendimento,
                            IsSistema = m.Atendimento.IsSistema,
                            LastModificationTime = m.Atendimento.LastModificationTime,
                            LastModifierUserId = m.Atendimento.LastModifierUserId,
                            LeitoId = m.Atendimento.LeitoId,
                            Matricula = m.Atendimento.Matricula,
                            MedicoId = m.Atendimento.MedicoId,
                            MotivoAltaId = m.Atendimento.MotivoAltaId,
                            NumeroGuia = m.Atendimento.NumeroGuia,
                            NacionalidadeResponsavelId = m.Atendimento.NacionalidadeResponsavelId,
                            Observacao = m.Atendimento.Observacao,
                            OrigemId = m.Atendimento.OrigemId,
                            PacienteId = m.Atendimento.PacienteId,
                            PlanoId = m.Atendimento.PlanoId,
                            QtdSessoes = m.Atendimento.QtdSessoes,
                            Responsavel = m.Atendimento.Responsavel,
                            RgResponsavel = m.Atendimento.RgResponsavel,
                            ServicoMedicoPrestadoId = m.Atendimento.ServicoMedicoPrestadoId,
                            UnidadeOrganizacionalId = m.Atendimento.UnidadeOrganizacionalId
                        },
                        Codigo = m.Codigo,
                        CreationTime = m.CreationTime,
                        CreatorUserId = m.CreatorUserId,
                        DataSolicitacao = m.DataSolicitacao,
                        DeleterUserId = m.DeleterUserId,
                        DeletionTime = m.DeletionTime,
                        Descricao = m.Descricao,
                        Id = m.Id,
                        IsDeleted = m.IsDeleted,
                        IsSistema = m.IsSistema,
                        LastModificationTime = m.LastModificationTime,
                        LastModifierUserId = m.LastModifierUserId,
                        LeitoId = m.LeitoId,
                        MedicoSolicitanteId = m.MedicoSolicitanteId,
                        Observacao = m.Observacao,
                        OrigemId = m.OrigemId,
                        PrescricaoId = m.PrescricaoId,
                        Prioridade = m.Prioridade,
                        UnidadeOrganizacionalId = m.UnidadeOrganizacionalId
                    })
                    .FirstOrDefaultAsync();

                var result = query; //.MapTo<SolicitacaoExameDto>();
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<SolicitacaoExameDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _solicitacaoExameRepository
                    .GetAll()
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    .Include(m => m.Atendimento.Medico)
                    .Include(m => m.Atendimento.Medico.SisPessoa)
                    .Include(m => m.Atendimento.Empresa)
                    .Include(m => m.UnidadeOrganizacional)
                    .WhereIf(!filtro.IsNullOrWhiteSpace(), m =>
                         m.CreationTime.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Medico.NomeCompleto.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Medico.Cpf.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Medico.Nascimento.ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Medico.Rg.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Paciente.Cpf.ToUpper().Contains(filtro.ToUpper()) ||
                         ((DateTime)m.Atendimento.Paciente.Nascimento).ToShortTimeString().ToUpper().Contains(filtro.ToUpper()) ||
                         m.Atendimento.Paciente.Rg.ToUpper().Contains(filtro.ToUpper())
                    );

                var admissoesMedicas = await query.ToListAsync();
                var admissoesMedicasDto = SolicitacaoExameDto.Mapear(admissoesMedicas).ToList();

                return new ListResultDto<SolicitacaoExameDto> { Items = admissoesMedicasDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<SolicitacaoExameDto> solicitacaoExameDtos = new List<SolicitacaoExameDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _solicitacaoExameRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )

                                //.Where(f => f.IsLaudo.Equals(isLaudo))

                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task RequisitarSolicitacao(SolicitacaoExameInputDto input)
        {
            try
            {
                var solicitacao = await CriarOuEditar(input.Solicitacao);

                foreach (var item in input.SolicitacaoExameItens)
                {
                    var solicitacaoItem = new SolicitacaoExameItem
                    {
                        Codigo = item.Codigo,
                        CreationTime = DateTime.Now,
                        CreatorUserId = AbpSession.UserId,
                        DataValidade = item.DataValidade,
                        Descricao = item.Descricao,
                        FaturamentoItemId = item.FaturamentoItemId,
                        GuiaNumero = item.GuiaNumero,
                        Id = item.Id,
                        IsDeleted = false,
                        IsSistema = item.IsSistema,
                        Justificativa = item.Justificativa,
                        KitExameId = item.KitExameId,
                        MaterialId = item.MaterialId,
                        SenhaNumero = item.SenhaNumero,
                        SolicitacaoExameId = solicitacao.Id
                    };
                    //item.SolicitacaoExameId = solicitacao.Id;

                    await _solicitacaoExameItemRepository.InsertAsync(solicitacaoItem);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroSalvar", ex);
            }
        }
    }
}
