﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public interface ISolicitacaoExameAppService : IApplicationService
    {
        Task<PagedResultDto<SolicitacaoExameIndex>> Listar(ListarSolicitacoesExamesInput input);

        Task<ListResultDto<SolicitacaoExameDto>> ListarFiltro(string filtro);

        Task<ListResultDto<SolicitacaoExameDto>> ListarTodos();

        Task<SolicitacaoExameDto> CriarOuEditar(SolicitacaoExameDto input);

        Task RequisitarSolicitacao(SolicitacaoExameInputDto input);

        Task Excluir(long id);

        Task<SolicitacaoExameDto> Obter(long id);

        Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput);
    }
}
