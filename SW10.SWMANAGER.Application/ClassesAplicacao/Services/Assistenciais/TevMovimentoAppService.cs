﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.InternacoesTev;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais
{
    public class TevMovimentoAppService : SWMANAGERAppServiceBase, ITevMovimentoAppService
    {
        private readonly IRepository<TevMovimento, long> _tevMovimentoRepository;
        private readonly IRepository<TevRisco, long> _tevRiscoRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IAtendimentoAppService _atendimentoAppService;

        public TevMovimentoAppService(
            IRepository<TevMovimento, long> tevMovimentoRepository,
            IRepository<TevRisco, long> tevRiscoRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IAtendimentoAppService atendimentoAppService
            )
        {
            _tevMovimentoRepository = tevMovimentoRepository;
            _tevRiscoRepository = tevRiscoRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _atendimentoAppService = atendimentoAppService;
        }

        [UnitOfWork]
        public async Task CriarOuEditar(TevMovimentoDto input)
        {
            try
            {
                var tevMovimento = new TevMovimento();
                tevMovimento.AtendimentoId = input.AtendimentoId;
                tevMovimento.Codigo = input.Codigo;
                tevMovimento.CreationTime = input.CreationTime;
                tevMovimento.CreatorUserId = input.CreatorUserId;
                tevMovimento.DeleterUserId = input.DeleterUserId;
                tevMovimento.DeletionTime = input.DeletionTime;
                tevMovimento.Descricao = input.Descricao;
                tevMovimento.Id = input.Id;
                tevMovimento.IsDeleted = input.IsDeleted;
                tevMovimento.IsSistema = input.IsSistema;
                tevMovimento.LastModificationTime = input.LastModificationTime;
                tevMovimento.LastModifierUserId = input.LastModifierUserId;
                tevMovimento.RiscoId = input.RiscoId;
                tevMovimento.Observacao = input.Observacao;
                tevMovimento.Data = input.Data;

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _tevMovimentoRepository.InsertAndGetIdAsync(tevMovimento);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    var entidade = await _tevMovimentoRepository.GetAsync(tevMovimento.Id);
                    entidade.AtendimentoId = tevMovimento.AtendimentoId;
                    entidade.Codigo = tevMovimento.Codigo;
                    entidade.CreationTime = tevMovimento.CreationTime;
                    entidade.CreatorUserId = tevMovimento.CreatorUserId;
                    entidade.Data = tevMovimento.Data;
                    entidade.DeleterUserId = tevMovimento.DeleterUserId;
                    entidade.DeletionTime = tevMovimento.DeletionTime;
                    entidade.Descricao = tevMovimento.Descricao;
                    //entidade.Id = prescricaoMedica.Id;
                    entidade.Observacao = tevMovimento.Observacao;
                    entidade.RiscoId = tevMovimento.RiscoId;

                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _tevMovimentoRepository.UpdateAsync(entidade);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tevMovimentoRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TevMovimentoDto>> ListarTodos()
        {
            try
            {
                var query = _tevMovimentoRepository
                    .GetAll();

                var tevMovimentoDto = await query.ToListAsync();

                return new ListResultDto<TevMovimentoDto> { Items = TevMovimentoDto.Mapear(tevMovimentoDto).ToList() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<TevMovimentoDto>> Listar(ListarInput input)
        {
            var contar = 0;
            long id = 0;
            var isAte = long.TryParse(input.PrincipalId, out id);
            List<TevMovimentoDto> tevMovimentosDto = new List<TevMovimentoDto>();
            try
            {
                var query = _tevMovimentoRepository
                    .GetAll()
                    .Include(m => m.Risco)
                    .Include(m => m.Atendimento)
                    .Where(m => m.AtendimentoId == id)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contar = await query.CountAsync();

                var tevMovimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                tevMovimentosDto = TevMovimentoDto.Mapear(tevMovimentos).ToList();

                return new PagedResultDto<TevMovimentoDto>(
                    contar,
                    tevMovimentosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TevMovimentoDto> Obter(long id)
        {
            try
            {
                var m = await _tevMovimentoRepository.GetAsync(id);
                var ate = new AtendimentoDto();
                if (m.AtendimentoId.HasValue)
                {
                    ate = await _atendimentoAppService.Obter(m.AtendimentoId.Value);
                }
                else
                {
                    ate = null;
                }
                var result = TevMovimentoDto.Mapear(m);

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TevMovimentoDto> ObterUltimo(long atendimentoId)
        {
            try
            {
                var m = await _tevMovimentoRepository
                    .GetAll()
                    .Include(i => i.Risco)
                    .Include(i => i.Atendimento)
                    .Where(w => w.AtendimentoId == atendimentoId)
                    .OrderByDescending(o => o.Data).ThenByDescending(o => o.CreationTime)
                    .FirstOrDefaultAsync();

                var result = TevMovimentoDto.Mapear(m);

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tevMovimentoRepository);
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarTevRiscoDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tevRiscoRepository);
        }

    }
}
