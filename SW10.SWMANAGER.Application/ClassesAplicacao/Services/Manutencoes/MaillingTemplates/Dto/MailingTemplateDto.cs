﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Manutencoes.MailingTemplates;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Manutencoes.MailingTemplates.Dto
{
    [AutoMap(typeof(MailingTemplate))]
    public class MailingTemplateDto : CamposPadraoCRUDDto
    {
        public string Name { get; set; }

        public string Titulo { get; set; }

        public string EmailSaida { get; set; }

        public string NomeSaida { get; set; }

        public string ContentTemplate { get; set; }

        public string CamposDisponiveis { get; set; }
    }
}
