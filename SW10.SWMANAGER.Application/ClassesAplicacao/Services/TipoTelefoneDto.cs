﻿using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(TipoTelefone))]
    public class TipoTelefoneDto : CamposPadraoCRUDDto
    {
    }
}
