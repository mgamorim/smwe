﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class SubGrupoContaAdministrativaAppService : SWMANAGERAppServiceBase, ISubGrupoContaAdministrativaAppService
    {
        private readonly IRepository<SubGrupoContaAdministrativa, long> _subGrupoContaAdministrativaRepository;

        public SubGrupoContaAdministrativaAppService(IRepository<SubGrupoContaAdministrativa, long> subGrupoContaAdministrativaRepository)
        {
            _subGrupoContaAdministrativaRepository = subGrupoContaAdministrativaRepository;
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _subGrupoContaAdministrativaRepository);
        }
    }
}
