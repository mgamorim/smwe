﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Enumeradores
{
    public enum EnumSituacaoLancamento
    {
        Aberto=1,
        Cancelado=2,
        ParcialmenteQuitado=3,
        Quitado=4
    }
}
