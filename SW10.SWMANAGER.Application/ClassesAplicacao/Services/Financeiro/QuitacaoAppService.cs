﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.Dto;
using Newtonsoft.Json;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.ServicoValidacao;
using Abp.Application.Services.Dto;
using System.Data.Entity;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Inputs;
using Newtonsoft.Json.Converters;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class QuitacaoAppService : SWMANAGERAppServiceBase, IQuitacaoAppService
    {
        private readonly IRepository<Quitacao, long> _quitacaoRepository;
        private readonly IRepository<LancamentoQuitacao, long> _lancamentoQuitacaoRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Lancamento, long> _lancamentoRepository;
        private readonly IRepository<Cheque, long> _chequeRepository;
        private readonly IRepository<MeioPagamento, long> _meioPagamentoRepository;

        public QuitacaoAppService(IRepository<Quitacao, long> quitacaoRepository
                                          , IRepository<LancamentoQuitacao, long> lancamentoQuitacaoRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Lancamento, long> lancamentoRepository
            , IRepository<Cheque, long> chequeRepository
            , IRepository<MeioPagamento, long> meioPagamentoRepository)
        {
            _quitacaoRepository = quitacaoRepository;
            _lancamentoQuitacaoRepository = lancamentoQuitacaoRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _lancamentoRepository = lancamentoRepository;
            _chequeRepository = chequeRepository;
            _meioPagamentoRepository = meioPagamentoRepository;
        }


        Task<QuitacaoDto> IQuitacaoAppService.ObterPorLancamento(List<long> ids)
        {
            throw new NotImplementedException();
        }

        public virtual DefaultReturn<QuitacaoDto> CriarOuEditar(QuitacaoDto input)
        {
            var _retornoPadrao = new DefaultReturn<QuitacaoDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {
                QuitacaoValidacaoService quitacaoValidacaoService = new QuitacaoValidacaoService(_lancamentoQuitacaoRepository
                                                                                                , _meioPagamentoRepository
                                                                                                , _chequeRepository);

                _retornoPadrao = quitacaoValidacaoService.Validar(input);


                if (_retornoPadrao.Errors.Count() == 0)
                {
                    var lancamentosQuitacoesDto = JsonConvert.DeserializeObject<List<QuitacaoIndex>>(input.LancamentosJson, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });


                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (input.Id == 0)
                        {
                            var quitacao = input.MapTo<Quitacao>();

                            var meioPagamento = _meioPagamentoRepository.GetAll()
                                                        .Where(w => w.Id == quitacao.MeioPagamentoId)
                                                        .FirstOrDefault();

                            if (meioPagamento.TipoMeioPagamentoId == (long)EnumTipoMeioPagamento.Cheque)
                            {
                                quitacao.ChequeId = input.ChequeId;

                                if(input.ChequeId!=null)
                                {
                                    var cheque = _chequeRepository.GetAll()
                                                                  .Where(w => w.Id == input.ChequeId)
                                                                  .FirstOrDefault();

                                    if(cheque !=null)
                                    {
                                        quitacao.Numero = cheque.Numero.ToString();
                                    }


                                }
                                
                            }

                            AtualizaListaLancamentoQuitacao(quitacao, lancamentosQuitacoesDto);

                            AsyncHelper.RunSync(() => _quitacaoRepository.InsertAsync(quitacao));

                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();

                            AtualizarStatusLancamentos(quitacao);
                            AtualizarUtilizacaoCheque(quitacao);

                            _retornoPadrao.ReturnObject = quitacao.MapTo<QuitacaoDto>();

                        }

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();


                    }
                }

            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }

        void AtualizaListaLancamentoQuitacao(Quitacao quitacao, List<QuitacaoIndex> lancamentosQuitacoesDto)
        {
            if (quitacao.LancamentoQuitacoes == null)
            {
                quitacao.LancamentoQuitacoes = new List<LancamentoQuitacao>();
            }
            else
            {

                quitacao.LancamentoQuitacoes.RemoveAll(r => !lancamentosQuitacoesDto.Any(a => a.Id == r.Id));

                foreach (var lancamentoQuitacao in quitacao.LancamentoQuitacoes)
                {
                    var lancamentoQuitacaoDto = lancamentosQuitacoesDto.Where(w => w.Id == lancamentoQuitacao.Id)
                                                   .First();

                    lancamentoQuitacao.ValorEfetivo = lancamentoQuitacaoDto.ValorEfetivo ?? 0;
                    lancamentoQuitacao.ValorQuitacao = lancamentoQuitacaoDto.ValorQuitacao ?? 0;
                    lancamentoQuitacao.Acrescimo = lancamentoQuitacaoDto.Acrescimo ?? 0;
                    lancamentoQuitacao.Desconto = lancamentoQuitacaoDto.Desconto ?? 0;
                    lancamentoQuitacao.MoraMulta = lancamentoQuitacaoDto.MoraMulta ?? 0;
                    lancamentoQuitacao.Juros = lancamentoQuitacaoDto.Juros ?? 0;
                    lancamentoQuitacao.LancamentoId = lancamentoQuitacaoDto.LancamentoId;

                }
            }

            foreach (var lancamentoQuitacaoDto in lancamentosQuitacoesDto.Where(w => (w.Id == 0)))
            {
                var lancamentoQuitacao = new LancamentoQuitacao();

                lancamentoQuitacao.ValorEfetivo = lancamentoQuitacaoDto.ValorEfetivo ?? 0;
                lancamentoQuitacao.ValorQuitacao = lancamentoQuitacaoDto.ValorQuitacao ?? 0;
                lancamentoQuitacao.Acrescimo = lancamentoQuitacaoDto.Acrescimo ?? 0;
                lancamentoQuitacao.Desconto = lancamentoQuitacaoDto.Desconto ?? 0;
                lancamentoQuitacao.MoraMulta = lancamentoQuitacaoDto.MoraMulta ?? 0;
                lancamentoQuitacao.Juros = lancamentoQuitacaoDto.Juros ?? 0;
                lancamentoQuitacao.LancamentoId = lancamentoQuitacaoDto.LancamentoId;

                quitacao.LancamentoQuitacoes.Add(lancamentoQuitacao);
            }


        }

        void AtualizarStatusLancamentos(Quitacao quitacao)
        {
            var lancamentosIds = quitacao.LancamentoQuitacoes.Select(s => s.LancamentoId).ToList();

            var lancamentos = _lancamentoRepository.GetAll()
                                             .Where(w => lancamentosIds.Any(a => a == w.Id))
                                             .ToList();

            foreach (var item in lancamentos)
            {
                var valorQuitacoes = _lancamentoQuitacaoRepository.GetAll()
                                                             .Where(w => w.LancamentoId == item.Id)
                                                             .Sum(s => s.ValorEfetivo);

                if (item.ValorLancamento <= valorQuitacoes)
                {
                    item.SituacaoLancamentoId = (long)EnumSituacaoLancamento.Quitado;
                }
                else
                {
                    item.SituacaoLancamentoId = (long)EnumSituacaoLancamento.ParcialmenteQuitado;
                }

                AsyncHelper.RunSync(() => _lancamentoRepository.UpdateAsync(item));

            }
        }

        void AtualizarUtilizacaoCheque(Quitacao quitacao)
        {
            var meioPagamento = _meioPagamentoRepository.GetAll()
                                                        .Where(w => w.Id == quitacao.MeioPagamentoId)
                                                        .FirstOrDefault();
            if (meioPagamento != null && meioPagamento.TipoMeioPagamentoId == (long)EnumTipoMeioPagamento.Cheque)
            {
                var cheque = _chequeRepository.GetAll()
                                              .Where(w => w.TalaoCheque.ContaCorrenteId == quitacao.ContaCorrenteId
                                                      && w.Numero.ToString() == quitacao.Numero)
                                              .FirstOrDefault();

                if (cheque != null)
                {
                    cheque.Data = DateTime.Now;
                    AsyncHelper.RunSync(() => _chequeRepository.UpdateAsync(cheque));

                }

            }
        }


        public ListResultDto<QuitacaoIndex> ListarQuitacoesPorLancamento(ListarQuitacaoLancamentoInput input)
        {
            List<QuitacaoIndex> quitacoes = new List<QuitacaoIndex>();

            long lancamentoId;

            long.TryParse(input.Filtro, out lancamentoId);


            var query = _lancamentoQuitacaoRepository.GetAll()
                                           .Where(w => w.LancamentoId == lancamentoId)
                                           .Include(i => i.Quitacao)
                                           .Include(i => i.Quitacao.MeioPagamento)
                                           .Include(i => i.Quitacao.ContaCorrente)
                                           .Include(i => i.Lancamento)
                                           .ToList();


            foreach (var item in query)
            {
                QuitacaoIndex quitacaoIndex = new QuitacaoIndex();

                quitacaoIndex.DataMovimento = item.Quitacao.DataMovimento;
                quitacaoIndex.Acrescimo = item.Acrescimo;
                quitacaoIndex.MoraMulta = item.MoraMulta;
                quitacaoIndex.ValorQuitacao = item.ValorQuitacao;
                quitacaoIndex.MeioPagamento = item.Quitacao.MeioPagamento.Descricao;
                quitacaoIndex.Numero = item.Quitacao.Numero;
                quitacaoIndex.Juros = item.Juros;
                quitacaoIndex.ContaCorrente = item.Quitacao.ContaCorrente.Descricao;
                quitacoes.Add(quitacaoIndex);
            }

            return new PagedResultDto<QuitacaoIndex>(
                 quitacoes.Count,
                 quitacoes
                 );

        }
    }
}
