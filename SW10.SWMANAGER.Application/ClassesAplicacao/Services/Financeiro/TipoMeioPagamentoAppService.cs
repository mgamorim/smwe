﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class TipoMeioPagamentoAppService : SWMANAGERAppServiceBase, ITipoMeioPagamentoAppService
    {
        private readonly IRepository<TipoMeioPagamento, long> _tipoMeioPagamentoRepository;

        public TipoMeioPagamentoAppService(IRepository<TipoMeioPagamento, long> tipoMeioPagamentoRepository)
        {
            _tipoMeioPagamentoRepository = tipoMeioPagamentoRepository;
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tipoMeioPagamentoRepository);
        }

        public async Task<TipoMeioPagamentoDto> Obter(long id)
        {
            try
            {
                var query = _tipoMeioPagamentoRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefault();

                return query.MapTo<TipoMeioPagamentoDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
