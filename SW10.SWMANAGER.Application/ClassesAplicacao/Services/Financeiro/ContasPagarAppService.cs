﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using System.Data.Entity;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Inputs;
using Abp.UI;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class ContasPagarAppService : DocumentoAppService, IContasPagarAppService
    {

        private readonly IRepository<Documento, long> _documentoRepositoy;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<SisFornecedor, long> _fornecedoreRepository;
        private readonly IRepository<Lancamento, long> _lancamentoRepository;
        private readonly IRepository<LancamentoQuitacao, long> _lancamentoQuitacaoRepository;
        private readonly IRepository<ContaAdministrativa, long> _contaAdministrativaRepository;


        public ContasPagarAppService(IRepository<Documento, long> documentoRepositoy
                                 , IUnitOfWorkManager unitOfWorkManager
            , IRepository<SisFornecedor, long> fornecedoreRepository
            , IRepository<Lancamento, long> lancamentoRepository
            , IRepository<LancamentoQuitacao, long> lancamentoQuitacaoRepository
            , IRepository<ContaAdministrativa, long> contaAdministrativaRepository) : base(documentoRepositoy, unitOfWorkManager, lancamentoRepository, lancamentoQuitacaoRepository, contaAdministrativaRepository)
        {
            _fornecedoreRepository = fornecedoreRepository;
            _documentoRepositoy = documentoRepositoy;
        }

        public override  async Task<DocumentoDto> Obter(long id)
        {
            var documentoDto = await base.Obter(id);

            var fornecedor =  _fornecedoreRepository.GetAll()
                                                   .Where(w => w.SisPessoaId == documentoDto.PessoaId)
                                                   .Include(i=>i.SisPessoa )
                                                   .FirstOrDefault();

            if (fornecedor != null)
            {
                documentoDto.ForncedorId = fornecedor.Id;

                documentoDto.Pessoa.Descricao = fornecedor.SisPessoa.FisicaJuridica == "F" ? fornecedor.SisPessoa.NomeCompleto : fornecedor.SisPessoa.NomeFantasia;

                documentoDto.Fornecedor = fornecedor.MapTo<SisFornecedorDto>();
            }


            return documentoDto;
        }

        public override DefaultReturn<DocumentoDto> CriarOuEditar(DocumentoDto input)
        {
            input.IsCredito = false;

            
            return base.CriarOuEditar(input);
        }

        public override Task<ListResultDto<LancamentoIndex>> ListarLancamento(ListarDocumentoInput input)
        {
            input.IsCredito = false;
            return base.ListarLancamento(input);
        }


        public async Task<DocumentoDto> ObterPorPessoaNumero(long pessoaId, string numero)
        {
            try
            {
                var query = _documentoRepositoy
                    .GetAll()
                    .Where(m => m.PessoaId== pessoaId  && m.Numero == numero)
                    .Include(i => i.Empresa)
                    .Include(i => i.Pessoa)
                    .Include(i => i.LancamentoDocumentos)
                    .Include(i => i.LancamentoDocumentos.Select(s => s.SituacaoLancamento))
                    .Include(i => i.TipoDocumento)
                    .FirstOrDefault();
                if (query != null)
                {

                    var documentoDto = query.MapTo<DocumentoDto>();

                    long idGrid = 0;
                    documentoDto.LancamentosDto = new List<LancamentoDto>();
                    documentoDto.DocumentosRateiosDto = new List<DocumentoRateioDto>();

                    foreach (var lancamento in query.LancamentoDocumentos)
                    {
                        var lancamentoDto = lancamento.MapTo<LancamentoDto>();

                        lancamentoDto.SituacaoDescricao = string.Concat(lancamento.SituacaoLancamento.Codigo, " - ", lancamento.SituacaoLancamento.Descricao);
                        lancamentoDto.IdGrid = idGrid++;
                        documentoDto.LancamentosDto.Add(lancamentoDto);
                    }

                    return documentoDto;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


    }
}
