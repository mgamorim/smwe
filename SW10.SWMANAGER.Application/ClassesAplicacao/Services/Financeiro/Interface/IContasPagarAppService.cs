﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public interface IContasPagarAppService: IDocumentoAppService
    {
        Task<DocumentoDto> ObterPorPessoaNumero(long pessoaId, string numero);
    }
}
