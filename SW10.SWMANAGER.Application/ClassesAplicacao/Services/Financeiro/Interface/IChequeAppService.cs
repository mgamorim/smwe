﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Interface
{
    public interface IChequeAppService : IApplicationService
    {
        Task<ResultDropdownList> ListarChequeNaoUtilziadoPorContaCorrenteDropdown(DropdownInput dropdownInput);
    }
}
