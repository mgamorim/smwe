﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto
{
    public class EmpresaIndex
    {
        public long? Id { get; set; }     
        public long EmpresaId { get; set; }
        public string EmpresaDescricao { get; set; }

        public long? IdGrid { get; set; }
    }
}
