﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto
{
    [AutoMap(typeof(SituacaoLancamento))]
    public class SituacaoLancamentoDto : CamposPadraoCRUDDto
    {
        public bool IsPermiteAlteracao { get; set; }
        public string CorLancamentoFundo { get; set; }
        public string CorLancamentoLetra { get; set; }
    }
}
