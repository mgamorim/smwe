﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto
{
    [AutoMap(typeof(Cheque))]
    public class ChequeDto : CamposPadraoCRUDDto
    {
        public long TalaoChequeId { get; set; }

        public TalaoChequeDto TalaoCheque { get; set; }

        public long Numero { get; set; }
        public string Nominal { get; set; }
        public DateTime? Data { get; set; }
    }
}
