﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto
{
    [AutoMap(typeof(GrupoContaAdministrativa))]
    public class GrupoContaAdministrativaDto : CamposPadraoCRUDDto
    {
        public bool IsValorIncideResultadoOperacionalEmpresa { get; set; }
        public long GrupoDREId { get; set; }
        public GrupoDREDto GrupoDRE { get; set; }
        public string SubGrupos { get; set; }

        
        public List<SubGrupoContaAdministrativaDto> SubGruposCntAdm { get; set; }
    }
}
