﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto
{
    [AutoMap(typeof(Agencia))]
    public class AgenciaDto : CamposPadraoCRUDDto
    {
        public long BancoId { get; set; }
        public BancoDto Banco { get; set; }
    }
}
