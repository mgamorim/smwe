﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto
{
    [AutoMap(typeof(ContaCorrente))]
    public class ContaCorrenteDto : CamposPadraoCRUDDto
    {
        public long TipoContaCorrenteId { get; set; }
        public TipoContaCorrenteDto TipoContaCorrente { get; set; }
        public long AgenciaId { get; set; }
        public AgenciaDto Agencia { get; set; }
        public long EmpresaId { get; set; }
        public EmpresaDto Empresa { get; set; }
        public DateTime DataAbertura { get; set; }
        public string NomeGerente { get; set; }
        public decimal? LimiteCredito { get; set; }
        public string Observacao { get; set; }
        public bool IsContaNaoOperacional { get; set; }
    }
}
