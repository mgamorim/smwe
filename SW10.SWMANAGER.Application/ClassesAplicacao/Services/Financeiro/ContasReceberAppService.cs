﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Inputs;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class ContasReceberAppService : DocumentoAppService, IContasReceberAppService
    {

        private readonly IRepository<Documento, long> _documentoRepositoy;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<SisPessoa, long> _sisPessoaRepository;
        private readonly IRepository<Lancamento, long> _lancamentoRepository;
        private readonly IRepository<LancamentoQuitacao, long> _lancamentoQuitacaoRepository;
        private readonly IRepository<ContaAdministrativa, long> _contaAdministrativaRepository;


        public ContasReceberAppService(IRepository<Documento, long> documentoRepositoy
                                 , IUnitOfWorkManager unitOfWorkManager
            , IRepository<SisPessoa, long> sisPessoaRepository
            , IRepository<Lancamento, long> lancamentoRepository
            , IRepository<LancamentoQuitacao, long> lancamentoQuitacaoRepository
            , IRepository<ContaAdministrativa, long> contaAdministrativaRepository) : base(documentoRepositoy, unitOfWorkManager, lancamentoRepository, lancamentoQuitacaoRepository, contaAdministrativaRepository)
        {
            _sisPessoaRepository = sisPessoaRepository;
        }

        //public override async Task<DocumentoDto> Obter(long id)
        //{
        //    var documentoDto = await base.Obter(id);

        //    var fornecedor = _fornecedoreRepository.GetAll()
        //                                           .Where(w => w.SisPessoaId == documentoDto.PessoaId)
        //                                           .Include(i => i.SisPessoa)
        //                                           .FirstOrDefault();

        //    if (fornecedor != null)
        //    {
        //        documentoDto.ForncedorId = fornecedor.Id;

        //        documentoDto.Pessoa.Descricao = fornecedor.SisPessoa.FisicaJuridica == "F" ? fornecedor.SisPessoa.NomeCompleto : fornecedor.SisPessoa.NomeFantasia;

        //        documentoDto.Fornecedor = fornecedor.MapTo<SisFornecedorDto>();
        //    }


        //    return documentoDto;
        //}

        public override Task<ListResultDto<LancamentoIndex>> ListarLancamento(ListarDocumentoInput input)
        {
            input.IsCredito = true;
            return base.ListarLancamento(input);
        }

        public override DefaultReturn<DocumentoDto> CriarOuEditar(DocumentoDto input)
        {
            input.IsCredito = true;
            return base.CriarOuEditar(input);
        }
    }
}
