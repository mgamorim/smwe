﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class ContaCorrenteAppService : SWMANAGERAppServiceBase, IContaCorrenteAppService
    {
        private readonly IRepository<ContaCorrente, long> _contaCorrenteRepository;

        public ContaCorrenteAppService(IRepository<ContaCorrente, long> contaCorrenteRepository)
        {
            _contaCorrenteRepository = contaCorrenteRepository;
        }


        public async Task<ResultDropdownList> ListarPorEmpresaDropdown(DropdownInput dropdownInput)
        {
            long empresaId;
            long.TryParse(dropdownInput.filtro, out empresaId);

            return await ListarDropdownLambda(dropdownInput
                                                     , _contaCorrenteRepository
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                     && (((ContaCorrente)m).EmpresaId == empresaId)

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );
        }
    }
}
