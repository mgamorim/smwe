﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public interface ITipoMeioPagamentoAppService : IApplicationService
    {
        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
        Task<TipoMeioPagamentoDto> Obter(long id);
    }
}
