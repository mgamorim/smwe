﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Inputs;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.ServicoValidacao;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro
{
    public class DocumentoAppService : SWMANAGERAppServiceBase, IDocumentoAppService
    {
        private readonly IRepository<Documento, long> _documentoRepositoy;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Lancamento, long> _lancamentoRepository;
        private readonly IRepository<LancamentoQuitacao, long> _lancamentoQuitacaoRepository;
        private readonly IRepository<ContaAdministrativa, long> _contaAdministrativaRepository;

        public DocumentoAppService(IRepository<Documento, long> documentoRepositoy
                                 , IUnitOfWorkManager unitOfWorkManager
                                 , IRepository<Lancamento, long> lancamentoRepository
                                 , IRepository<LancamentoQuitacao, long> lancamentoQuitacaoRepository
                                 , IRepository<ContaAdministrativa, long> contaAdministrativaRepository)
        {
            _documentoRepositoy = documentoRepositoy;
            _unitOfWorkManager = unitOfWorkManager;
            _lancamentoRepository = lancamentoRepository;
            _lancamentoQuitacaoRepository = lancamentoQuitacaoRepository;
            _contaAdministrativaRepository = contaAdministrativaRepository;
        }

        public async Task<ListResultDto<DocumentoDto>> Listar(ListarDocumentoInput input)
        {
            try
            {
                List<DocumentoDto> documentosDto = new List<DocumentoDto>();


                var query = _documentoRepositoy.GetAll()
                                                      .Where(w => (input.Filtro == "" || input.Filtro == null)
                                                                 || w.Descricao.ToString().ToUpper().Contains(input.Filtro.ToUpper()))
                                                       .Include(i => i.Pessoa)
                                                       .ToList();
                //.Select(s => new DocumentoDto
                //{
                //    Id = s.Id,
                //    Codigo = s.Codigo,
                //    Descricao = s.Descricao,
                //    DataEmissao = s.DataEmissao,
                //    PessoaId = s.PessoaId,

                //})


                foreach (var item in query)
                {
                    documentosDto.Add(item.MapTo<DocumentoDto>());
                }

                return new PagedResultDto<DocumentoDto>(
                  documentosDto.Count,
                  documentosDto
                  );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public virtual async Task<DocumentoDto> Obter(long id)
        {
            try
            {
                var query = _documentoRepositoy
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Include(i => i.Empresa)
                    .Include(i => i.Pessoa)
                    .Include(i => i.LancamentoDocumentos)
                    .Include(i => i.LancamentoDocumentos.Select(s => s.SituacaoLancamento))
                    .Include(i => i.TipoDocumento)
                    .FirstOrDefault();

                var documentoDto = query.MapTo<DocumentoDto>();

                long idGrid = 0;
                documentoDto.LancamentosDto = new List<LancamentoDto>();
                documentoDto.DocumentosRateiosDto = new List<DocumentoRateioDto>();

                foreach (var lancamento in query.LancamentoDocumentos)
                {
                    var lancamentoDto = lancamento.MapTo<LancamentoDto>();

                    lancamentoDto.SituacaoDescricao = string.Concat(lancamento.SituacaoLancamento.Codigo, " - ", lancamento.SituacaoLancamento.Descricao);
                    lancamentoDto.IdGrid = idGrid++;
                    documentoDto.LancamentosDto.Add(lancamentoDto);
                }

                foreach (var rateio in query.Rateios)
                {
                    var rateioDto = rateio.MapTo<DocumentoRateioDto>();
                    documentoDto.DocumentosRateiosDto.Add(rateioDto);
                }

                return documentoDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public virtual DefaultReturn<DocumentoDto> CriarOuEditar(DocumentoDto input)
        {
            var _retornoPadrao = new DefaultReturn<DocumentoDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {


                DocumentoValidacaoService documentoValidacaoService = new DocumentoValidacaoService(_contaAdministrativaRepository);

                _retornoPadrao = documentoValidacaoService.Validar(input);

                if (_retornoPadrao.Errors.Count() == 0)
                {


                    var lancamentosDto = JsonConvert.DeserializeObject<List<LancamentoIndex>>(input.LancamentosJson, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                    var rateiosDto = JsonConvert.DeserializeObject<List<DocumentoRateioIndex>>(input.RateioJson);

                    foreach (var item in lancamentosDto)
                    {
                        item.IsCredito = input.IsCredito;
                        if (item.SituacaoLancamentoId == 0)
                        {
                            item.SituacaoLancamentoId = 1;
                        }
                    }


                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (input.Id == 0)
                        {
                            Documento documento = input.MapTo<Documento>();
                            documento.LancamentoDocumentos = new List<Lancamento>();

                            foreach (var lancamentoDto in lancamentosDto)
                            {
                                var lancamento = new Lancamento();

                                lancamento.ValorLancamento = lancamentoDto.ValorLancamento;
                                lancamento.AnoCompetencia = lancamentoDto.AnoCompetencia;
                                lancamento.DataLancamento = lancamentoDto.DataLancamento;
                                lancamento.DataVencimento = lancamentoDto.DataVencimento;
                                lancamento.Juros = lancamentoDto.Juros;
                                lancamento.MesCompetencia = lancamentoDto.MesCompetencia;
                                lancamento.Multa = lancamentoDto.Multa;
                                lancamento.Parcela = lancamentoDto.Parcela;
                                lancamento.ValorAcrescimoDecrescimo = lancamentoDto.ValorAcrescimoDecrescimo;
                                lancamento.ValorLancamento = lancamentoDto.ValorLancamento;
                                lancamento.IsCredito = lancamentoDto.IsCredito;
                                lancamento.CodigoBarras = lancamentoDto.CodigoBarras;
                                lancamento.NossoNumero = lancamentoDto.NossoNumero;
                                lancamento.LinhaDigitavel = lancamentoDto.LinhaDigitavel;
                                lancamento.SituacaoLancamentoId = lancamentoDto.SituacaoLancamentoId;

                                documento.LancamentoDocumentos.Add(lancamento);
                            }


                            AtualizaListaRateio(documento, rateiosDto);


                            AsyncHelper.RunSync(() => _documentoRepositoy.InsertAsync(documento));
                            _retornoPadrao.ReturnObject = documento.MapTo<DocumentoDto>();
                        }
                        else
                        {
                            var documento = _documentoRepositoy.GetAll()
                                                              .Where(w => w.Id == input.Id)
                                                            .Include(i => i.Empresa)
                                                            .Include(i => i.Pessoa)
                                                            .Include(i => i.LancamentoDocumentos)
                                                            .Include(i => i.TipoDocumento)
                                                            .Include(i=> i.Rateios)
                                                             .FirstOrDefault();

                            if (documento != null)
                            {
                                documento.EmpresaId = input.EmpresaId;
                                documento.TipoDocumentoId = input.TipoDocumentoId;
                                documento.PessoaId = input.PessoaId;
                                documento.Numero = input.Numero;
                                documento.DataEmissao = input.DataEmissao;
                                documento.ValorDocumento = input.ValorDocumento;
                                documento.ValorAcrescimoDecrescimo = input.ValorAcrescimoDecrescimo;


                                AtualizaListaLancamentos(documento, lancamentosDto);
                                AtualizaListaRateio(documento, rateiosDto);


                                AsyncHelper.RunSync(() => _documentoRepositoy.UpdateAsync(documento));

                                _retornoPadrao.ReturnObject = documento.MapTo<DocumentoDto>();
                            }
                        }

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();

                    }
                }

            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }

        public async Task Excluir(DocumentoDto input)
        {
            try
            {
                await _documentoRepositoy.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        virtual public async Task<ListResultDto<LancamentoIndex>> ListarLancamento(ListarDocumentoInput input)
        {
            try
            {
                if(input.EmissaoDe!=null)
                {
                    input.EmissaoDe = ((DateTime)input.EmissaoDe).Date.AddMilliseconds(-1);
                }

                List<LancamentoIndex> lancamentosDto = new List<LancamentoIndex>();


                var query = _lancamentoRepository.GetAll()
                                                      .Where(w => ((input.Filtro == "" || input.Filtro == null)
                                                                 || w.Descricao.ToString().ToUpper().Contains(input.Filtro.ToUpper()))

                                                                 && w.IsCredito == input.IsCredito
                                                                 && (w.DocumentoId != null)

                                                                 && (input.PessoaId == null || input.PessoaId == w.Documento.PessoaId)
                                                                 && (input.EmissaoDe == null || input.IgnorarEmissao   || (((DateTime)input.EmissaoDe).Date <= w.Documento.DataEmissao && ((DateTime)input.EmissaoAte).Date >= w.Documento.DataEmissao ))
                                                                 && (input.EmpresaId == null || input.EmpresaId == w.Documento.EmpresaId)
                                                                 && (input.SituacaoLancamentoId == null || input.SituacaoLancamentoId == w.SituacaoLancamentoId)
                                                                 && ( input.Documento==null || input.Documento==string.Empty  || input.Documento == w.Documento.Numero)
                                                                 && (input.ContaAdministrativaId == null ||  w.Documento.Rateios.Any(a=> a.ContaAdministrativaId ==input.ContaAdministrativaId ))
                                                                 && (input.CentroCustoId == null || w.Documento.Rateios.Any(a => a.CentroCustoId == input.CentroCustoId))
                                                                 && (input.VencimentoDe == null || input.IgnorarVencimento ||(input.VencimentoDe <= w.DataVencimento && input.VencimentoAte >= w.DataVencimento))
                                                                 && (input.TipoDocumentoId == null || input.TipoDocumentoId == w.Documento.TipoDocumentoId)
                                                                 && (input.MeioPagamentoId == null || w.LancamentosQuitacoes.Any(a => a.Quitacao.MeioPagamentoId == input.MeioPagamentoId))

                                                                 )
                                                       .Include(i => i.Documento)
                                                       .Include(i => i.Documento.Pessoa)
                                                       .Include(i => i.Documento.TipoDocumento)
                                                       .Include(i => i.SituacaoLancamento)
                                                       .ToList();

                foreach (var item in query)
                {
                    var lancamentoIndex = new LancamentoIndex();

                    lancamentoIndex.Id = item.Id;
                    lancamentoIndex.SituacaoDescricao = string.Concat(item.SituacaoLancamento.Codigo, " - ", item.SituacaoLancamento.Descricao);
                    lancamentoIndex.CorLancamentoFundo = item.SituacaoLancamento.CorLancamentoFundo;
                    lancamentoIndex.CorLancamentoLetra = item.SituacaoLancamento.CorLancamentoLetra;
                    lancamentoIndex.Fornecedor = item.Documento.Pessoa.FisicaJuridica == "F" ? item.Documento.Pessoa.NomeCompleto : item.Documento.Pessoa.NomeFantasia;
                    lancamentoIndex.DataVencimento = item.DataVencimento;
                    lancamentoIndex.DataEmissao = item.DataLancamento;
                    lancamentoIndex.Competencia = string.Concat(item.MesCompetencia.ToString().PadLeft(2, '0'), "/", item.AnoCompetencia);
                    lancamentoIndex.Documento = item.Documento.Numero;
                    lancamentoIndex.TotalLancamento = item.ValorLancamento;
                    lancamentoIndex.TipoDocumento = string.Concat(item.Documento.TipoDocumento.Codigo, " - ", item.Documento.TipoDocumento.Descricao);

                     var quitacoes = _lancamentoQuitacaoRepository.GetAll()
                                                                  .Where(w => w.LancamentoId == item.Id)
                                                                  .ToList();

                    if (quitacoes.Count > 0)
                    {
                        lancamentoIndex.TotalQuitacao = quitacoes.Sum(s => s.ValorEfetivo);
                    }

                    lancamentosDto.Add(lancamentoIndex);
                }

                return new PagedResultDto<LancamentoIndex>(
                  lancamentosDto.Count,
                  lancamentosDto
                  );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public virtual async Task<DocumentoDto> ObterPorLancamento(long id)
        {
            try
            {
                var lancamentoBusca = _lancamentoRepository.GetAll()
                                                      .Where(w => w.Id == id)
                                                      .FirstOrDefault();

                if (lancamentoBusca != null)
                {
                    var query = _documentoRepositoy
                        .GetAll()
                        .Where(m => m.Id == lancamentoBusca.DocumentoId)
                        .Include(i => i.Empresa)
                        .Include(i => i.Pessoa)
                        .Include(i => i.LancamentoDocumentos)
                        .Include(i => i.LancamentoDocumentos.Select(s => s.SituacaoLancamento))
                        .Include(i => i.TipoDocumento)
                        .Include(i => i.Rateios)
                        .Include(i => i.Rateios.Select(s=> s.CentroCusto))
                        .Include(i => i.Rateios.Select(s => s.ContaAdministrativa))
                        .Include(i => i.Rateios.Select(s => s.Empresa))
                        .FirstOrDefault();

                    var documentoDto = query.MapTo<DocumentoDto>();
                    long idGrid = 0;
                    documentoDto.LancamentosDto = new List<LancamentoDto>();
                    foreach (var lancamento in query.LancamentoDocumentos.OrderBy(o=> o.Parcela))
                    {
                        var lancamentoDto = lancamento.MapTo<LancamentoDto>();

                        lancamentoDto.SituacaoDescricao = string.Concat(lancamento.SituacaoLancamento.Codigo, " - ", lancamento.SituacaoLancamento.Descricao);
                        lancamentoDto.CorLancamentoFundo = lancamento.SituacaoLancamento.CorLancamentoFundo;
                        lancamentoDto.CorLancamentoLetra = lancamento.SituacaoLancamento.CorLancamentoLetra;
                        lancamentoDto.IdGrid = idGrid++;

                        lancamentoDto.IsSelecionado = lancamentoDto.Id == id;

                     




                        documentoDto.LancamentosDto.Add(lancamentoDto);
                    }

                    documentoDto.DocumentosRateiosDto = new List<DocumentoRateioDto>();
                    foreach (var rateio in query.Rateios)
                    {
                        var rateioDto = rateio.MapTo<DocumentoRateioDto>();
                        documentoDto.DocumentosRateiosDto.Add(rateioDto);
                    }

                    documentoDto.ValorTotalDocumento = (documentoDto.ValorDocumento??0) + (documentoDto.ValorAcrescimoDecrescimo??0);
                    return documentoDto;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        void AtualizaListaLancamentos(Documento documento, List<LancamentoIndex> lancamentosDto)
        {
            documento.LancamentoDocumentos.RemoveAll(r => !lancamentosDto.Any(a => a.Id == r.Id));

            //atuliza
            foreach (var lancamento in documento.LancamentoDocumentos)
            {
                var lancamentoDto = lancamentosDto.Where(w => w.Id == lancamento.Id)
                                               .First();

                //lancamento.Codigo = lancamentoDto.Codigo;
                //lancamento.Descricao = lancamentoDto.Descricao;
                lancamento.ValorLancamento = lancamentoDto.ValorLancamento;
                lancamento.AnoCompetencia = lancamentoDto.AnoCompetencia;
                lancamento.DataLancamento = lancamentoDto.DataLancamento;
                lancamento.DataVencimento = lancamentoDto.DataVencimento;
                lancamento.Juros = lancamentoDto.Juros;
                lancamento.MesCompetencia = lancamentoDto.MesCompetencia;
                lancamento.Multa = lancamentoDto.Multa;
                lancamento.Parcela = lancamentoDto.Parcela;
                lancamento.ValorAcrescimoDecrescimo = lancamentoDto.ValorAcrescimoDecrescimo;
                lancamento.ValorLancamento = lancamentoDto.ValorLancamento;
                lancamento.IsCredito = lancamentoDto.IsCredito;
                lancamento.CodigoBarras = lancamentoDto.CodigoBarras;
                lancamento.NossoNumero = lancamentoDto.NossoNumero;
                lancamento.LinhaDigitavel = lancamentoDto.LinhaDigitavel;
                lancamento.SituacaoLancamentoId = lancamentoDto.SituacaoLancamentoId;

            }

            //inclui novos
            foreach (var lancamentoDto in lancamentosDto.Where(w => (w.Id == 0 || w.Id == null)))
            {
                var lancamento = new Lancamento();

                lancamento.ValorLancamento = lancamentoDto.ValorLancamento;
                lancamento.AnoCompetencia = lancamentoDto.AnoCompetencia;
                lancamento.DataLancamento = lancamentoDto.DataLancamento;
                lancamento.DataVencimento = lancamentoDto.DataVencimento;
                lancamento.Juros = lancamentoDto.Juros;
                lancamento.MesCompetencia = lancamentoDto.MesCompetencia;
                lancamento.Multa = lancamentoDto.Multa;
                lancamento.Parcela = lancamentoDto.Parcela;
                lancamento.ValorAcrescimoDecrescimo = lancamentoDto.ValorAcrescimoDecrescimo;
                lancamento.ValorLancamento = lancamentoDto.ValorLancamento;
                lancamento.IsCredito = lancamentoDto.IsCredito;
                lancamento.CodigoBarras = lancamentoDto.CodigoBarras;
                lancamento.NossoNumero = lancamentoDto.NossoNumero;
                lancamento.LinhaDigitavel = lancamentoDto.LinhaDigitavel;
                lancamento.SituacaoLancamentoId = lancamentoDto.SituacaoLancamentoId;

                documento.LancamentoDocumentos.Add(lancamento);
            }
        }

        void AtualizaListaRateio(Documento documento, List<DocumentoRateioIndex> rateiosDto)
        {
            if (documento.Rateios == null)
            {
                documento.Rateios = new List<DocumentoRateio>();
            }
            else
            {

                documento.Rateios.RemoveAll(r => !rateiosDto.Any(a => a.Id == r.Id));

                foreach (var rateio in documento.Rateios)
                {
                    var rateioDto = rateiosDto.Where(w => w.Id == rateio.Id)
                                                   .First();

                    rateio.CentroCustoId = (long)rateioDto.CentroCustoId;
                    rateio.ContaAdministrativaId = (long)rateioDto.ContaAdministrativaId;
                    rateio.EmpresaId = (long)rateioDto.EmpresaId;
                    rateio.Valor = rateioDto.Valor;
                    rateio.Observacao = rateioDto.Observacao;
                    rateio.IsImposto = rateioDto.IsImposto;
                }
            }

            foreach (var rateioDto in rateiosDto.Where(w => (w.Id == 0 || w.Id == null)))
            {
                var rateio = new DocumentoRateio();

                rateio.CentroCustoId = (long)rateioDto.CentroCustoId;
                rateio.ContaAdministrativaId = (long)rateioDto.ContaAdministrativaId;
                rateio.EmpresaId = (long)rateioDto.EmpresaId;
                rateio.Valor = rateioDto.Valor;
                rateio.Observacao = rateioDto.Observacao;
                rateio.IsImposto = rateioDto.IsImposto;

                documento.Rateios.Add(rateio);
            }


        }
    }
}
