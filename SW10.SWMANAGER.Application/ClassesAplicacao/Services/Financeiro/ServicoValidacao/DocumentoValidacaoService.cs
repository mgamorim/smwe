﻿using Abp.Domain.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Financeiros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.ServicoValidacao
{
    public class DocumentoValidacaoService
    {
        private readonly IRepository<ContaAdministrativa, long> _contaAdministrativaRepository;

        DefaultReturn<DocumentoDto> _retornoPadrao = new DefaultReturn<DocumentoDto>();
        List<LancamentoIndex> lancamentosDto;
        List<DocumentoRateioIndex> rateiosDto;

        public DocumentoValidacaoService(IRepository<ContaAdministrativa, long> contaAdministrativaRepository)
        {
            _contaAdministrativaRepository = contaAdministrativaRepository;
        }


        public DefaultReturn<DocumentoDto> Validar(DocumentoDto documento)
        {
            lancamentosDto = JsonConvert.DeserializeObject<List<LancamentoIndex>>(documento.LancamentosJson, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
            rateiosDto = JsonConvert.DeserializeObject<List<DocumentoRateioIndex>>(documento.RateioJson);

            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            ValidarTotalDocumento(documento);
            ValidarRateio(documento);

            return _retornoPadrao;
        }

        void ValidarTotalDocumento(DocumentoDto documento)
        {
            var totalParcelas = lancamentosDto.Sum(s => s.ValorLancamento);
            var totalJuros = lancamentosDto.Sum(s => s.Juros);
            var valorDocumento = (documento.ValorDocumento ?? 0) + (documento.ValorAcrescimoDecrescimo ?? 0);

            if (totalParcelas != valorDocumento)
            {
                _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "FIN0001" });
            }


        }

        void ValidarTotalRateio(DocumentoDto documento)
        {
            var totalRateio = rateiosDto.Sum(s => s.Valor);
            var valorDocumento = (documento.ValorDocumento ?? 0) + (documento.ValorAcrescimoDecrescimo ?? 0);

            if (totalRateio != valorDocumento)
            {
                _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "FIN0002" });
            }
        }

        void ValidarRateio(DocumentoDto documento)
        {
            ValidarTotalRateio(documento);

            foreach (var item in rateiosDto)
            {
                ValidarContaAdministrativa(item, documento.IsCredito);
            }

        }

        void ValidarContaAdministrativa(DocumentoRateioIndex documentoRateio, bool isCredito)
        {
            var contaAdministrativa = _contaAdministrativaRepository.GetAll()
                                          .Where(w => w.ContaAdministrativaEmpresas.Any(a => a.EmpresaId == documentoRateio.EmpresaId)
                                                  && w.Id == documentoRateio.ContaAdministrativaId
                                                  && w.IsReceita == isCredito
                                                  && w.IsDespesa == !isCredito)
                                          .FirstOrDefault();

            if (contaAdministrativa == null)
            {
                _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "FIN0003", Parametros = new List<object> { documentoRateio.ContaAdministrativaDescricao } });
            }
        }
    }
}
