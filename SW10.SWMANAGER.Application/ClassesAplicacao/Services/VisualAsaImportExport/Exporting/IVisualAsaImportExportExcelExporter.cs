﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.VisualAsaImportExportLogs.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.VisualAsaImportExportLogs.Exporting
{
	public interface IListarVisualAsaImportExportLogExcelExporter
	{
		FileDto ExportToFile (List<VisualAsaImportExportLogDto> visualAsaImportExportLogDtos);
	}
}
