﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.ViewModels;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.ViewModels
{
    [AutoMap(typeof(VWTeste))]
    public class VWTesteDto : EntityDto<long>
    {
        public long PacienteId { get; set; }
        public string NomePaciente { get; set; }
        public long CidadeId { get; set; }
        public string NomeCidade { get; set; }
        public long EstadoId { get; set; }
        public string NomeEstado { get; set; }
    }
}
