﻿using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(Escolaridade))]
    public class EscolaridadeDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
