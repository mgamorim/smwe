﻿using Abp.Application.Services;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.VisualASA
{
    public interface IVisualAppService : IApplicationService
    {
        void MigrarVisualASA(long atendimentosId);
        void MigrarSisPessoa(long pessoaId);
        
    }
}
