﻿using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(EstadoCivil))]
    public class EstadoCivilDto : CamposPadraoCRUDDto
    {
    }
}
