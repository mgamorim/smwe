﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ItensTabela;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Tabelas;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Extensions;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ItensTabela.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.ItensTabela;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Pacotes.Dtos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens
{
    public class FaturamentoContaItemAppService : SWMANAGERAppServiceBase, IFaturamentoContaItemAppService
    {
        #region Cabecalho
        private readonly IRepository<FaturamentoContaItem, long> _contaItemRepository;
        private readonly IRepository<FaturamentoItem, long> _fatItemRepository;
        private readonly IFaturamentoItemAppService _fatItemAppService;
        private readonly IFaturamentoTabelaAppService _tabelaAppService;
        private readonly IFaturamentoItemTabelaAppService _tabelaItemAppService;
        private readonly ISisMoedaAppService _moedaAppService;
        private readonly ISisMoedaCotacaoAppService _cotacaoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IFaturamentoConfigConvenioAppService _configConvenioAppService;
        private readonly IRepository<FaturamentoItemConfig, long> _itemConfigRepository;
        private readonly IRepository<FaturamentoConta, long> _faturamentoContaRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IRepository<FaturamentoItemTabela, long> _faturamentoItemTabela;




        //private readonly IContaAppService _contaAppService;
        //    private readonly IListarFaturamentoContaItensExcelExporter _listarContaItensExcelExporter;

        public FaturamentoContaItemAppService(
            IRepository<FaturamentoContaItem, long> contaItemRepository
            ,
            IRepository<FaturamentoItem, long> fatItemRepository
            ,
            //IListarContaItensExcelExporter listarContaItensExcelExporter
            //,
            IFaturamentoItemAppService fatItemAppService
            ,
            IFaturamentoTabelaAppService tabelaAppService,
            IFaturamentoItemTabelaAppService tabelaItemAppService
            ,
            ISisMoedaAppService moedaAppService,
            ISisMoedaCotacaoAppService cotacaoAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IFaturamentoConfigConvenioAppService configConvenioAppService,
            IRepository<FaturamentoItemConfig, long> itemConfigRepository,
            IRepository<FaturamentoConta, long> faturamentoContaRepository,
            IRepository<Atendimento, long> atendimentoRepository,
            IRepository<FaturamentoItemTabela, long> faturamentoItemTabela

            //,
            //IContaAppService contaAppService
            )
        {
            _contaItemRepository = contaItemRepository;
            _fatItemRepository = fatItemRepository;
            _fatItemAppService = fatItemAppService;
            _tabelaAppService = tabelaAppService;
            _moedaAppService = moedaAppService;
            _cotacaoAppService = cotacaoAppService;
            _tabelaItemAppService = tabelaItemAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _configConvenioAppService = configConvenioAppService;
            _itemConfigRepository = itemConfigRepository;
            _faturamentoContaRepository = faturamentoContaRepository;
            _atendimentoRepository = atendimentoRepository;
            _faturamentoItemTabela = faturamentoItemTabela;
            //  _contaAppService = contaAppService;
            //    _listarContaItensExcelExporter = listarContaItensExcelExporter;
        }
        #endregion cabecalho.

        public async Task<PagedResultDto<FaturamentoContaItemDto>> Listar(ListarFaturamentoContaItensInput input)
        {
            var contarContaItens = 0;
            List<FaturamentoContaItem> contaItens;
            List<FaturamentoContaItemDto> contaItensDtos = new List<FaturamentoContaItemDto>();
            try
            {
                var query = _contaItemRepository
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro)
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.FaturamentoItem.Grupo)
                    .Include(i => i.Turno)
                    ;

                contarContaItens = await query
                    .CountAsync();

                contaItens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                contaItensDtos = contaItens.MapTo<List<FaturamentoContaItemDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<FaturamentoContaItemDto>(
                contarContaItens,
                contaItensDtos
                );
        }

        public async Task<PagedResultDto<FaturamentoContaItemDto>> ListarPorConta(ListarFaturamentoContaItensInput input)
        {
            var contarContaItens = 0;
            List<FaturamentoContaItem> contaItens;
            List<FaturamentoContaItemDto> contaItensDtos = new List<FaturamentoContaItemDto>();
            try
            {
                var query = _contaItemRepository
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro && (e.FaturamentoPacoteId == null || e.FaturamentoItem.Grupo.TipoGrupoId ==4) )
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.FaturamentoItem.Grupo)
                    .Include(i => i.Turno)
                    .Include(i => i.FaturamentoConfigConvenio)
                    ;

                contarContaItens = await query
                    .CountAsync();

                contaItens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();


                foreach (var contaItem in contaItens)
                {
                    var novo = FaturamentoContaItemDto.MapearFromCore(contaItem);

                    long? tabelaId = null;

                    if (contaItem.FaturamentoConfigConvenio != null)
                    {
                        tabelaId = contaItem.FaturamentoConfigConvenio.TabelaId;
                    }
                    if (!novo.IsValorItemManual)
                    {
                        var fatItemTabela = _faturamentoItemTabela.GetAll()
                                                .Where(w => w.ItemId == contaItem.FaturamentoItemId
                                                        && w.TabelaId == tabelaId)
                                                .FirstOrDefault();
                        if (fatItemTabela != null)
                        {
                            novo.ValorItem = fatItemTabela.Preco;
                        }
                        else
                        {
                            novo.ValorItem = 0;
                        }
                    }


                    contaItensDtos.Add(novo);
                }
                //         contaItensDtos = contaItens.MapTo<List<FaturamentoContaItemDto>>();

                return new PagedResultDto<FaturamentoContaItemDto>(contarContaItens, contaItensDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public async Task<float> CalcularValorUnitarioContaItem (CalculoContaItemInput input)
        //{
        //    try
        //    {
        //        input.i.FaturamentoItem = await _fatItemAppService.Obter((long)input.i.FaturamentoItemId);

        //        long? tabelaId;

        //        // Caso haja para um plano especifico
        //        if (input.configsPorPlano.Count() > 0)
        //        {
        //            tabelaId = input.configsPorPlano/*.Where(x=>x.GrupoId == i.FaturamentoItem.GrupoId)*/.FirstOrDefault().TabelaId;

        //            // Por item especifico
        //            var configPorItem = input.configsPorPlano.FirstOrDefault(_ => _.ItemId == input.i.Id);

        //            if (configPorItem != null)
        //            {
        //                tabelaId = configPorItem.TabelaId;
        //            }
        //            else
        //            {
        //                // Por subGrupo especifico
        //                var configPorSubGrupo = input.configsPorPlano.FirstOrDefault(_ => _.SubGrupoId == input.i.FaturamentoItem.SubGrupoId);

        //                if (configPorSubGrupo != null)
        //                {
        //                    tabelaId = configPorSubGrupo.TabelaId;
        //                }
        //                else
        //                {
        //                    // Por grupo especifico
        //                    var configPorGrupo = input.configsPorPlano.FirstOrDefault(_ => _.GrupoId == input.i.FaturamentoItem.GrupoId);

        //                    if (configPorGrupo != null)
        //                    {
        //                        tabelaId = configPorGrupo.TabelaId;
        //                    }
        //                }
        //            }
        //        }
        //        // Caso seja para todos os planos
        //        else
        //        {
        //            tabelaId = input.configsPorEmpresa.First().TabelaId;

        //            // Por item
        //            var configPorItem = input.configsPorEmpresa.FirstOrDefault(_ => _.ItemId == input.i.Id);

        //            if (configPorItem != null)
        //            {
        //                tabelaId = configPorItem.TabelaId;
        //            }
        //            else
        //            {
        //                // Por subGrupo
        //                var configPorSubGrupo = input.configsPorEmpresa.FirstOrDefault(_ => _.SubGrupoId == input.i.FaturamentoItem.SubGrupoId);

        //                if (configPorSubGrupo != null)
        //                {
        //                    tabelaId = configPorSubGrupo.TabelaId;
        //                }
        //                else
        //                {
        //                    // Por grupo
        //                    var configPorGrupo = input.configsPorEmpresa.FirstOrDefault(_ => _.GrupoId == input.i.FaturamentoItem.GrupoId);

        //                    if (configPorGrupo != null)
        //                    {
        //                        tabelaId = configPorGrupo.TabelaId;
        //                    }
        //                }
        //            }
        //        }

        //        // ======================= PRECO =========================

        //        // Obter preco vigente
        //        var precosPorTabela = AsyncHelper.RunSync(() => _tabelaItemAppService.ListarParaFatTabela((long)tabelaId)).Items;
        //        var precosPorFatItem = precosPorTabela.Where(_ => _.ItemId == input.i.FaturamentoItemId);
        //        var preco = precosPorFatItem
        //            .Where(_ => _.VigenciaDataInicio <= DateTime.Now)
        //            .OrderByDescending(_ => _.VigenciaDataInicio).First();

        //        var moeda = AsyncHelper.RunSync(() => _moedaAppService.Obter((long)preco.SisMoedaId));
        //        ListarSisMoedaCotacoesInput cotacaoInput = new ListarSisMoedaCotacoesInput();
        //        cotacaoInput.Filtro = moeda.Id.ToString();

        //        // ======================= COTACAO =========================

        //        // Buscar cotacoes por convenio
        //        var cotacoesPorConvenio = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
        //            .Items
        //            .Where(_ => _.ConvenioId == input.conta.ConvenioId)
        //            ;

        //        // Cotacoes por Empresa
        //        var cotacoesPorEmpresa = cotacoesPorConvenio
        //            .Where(_ => _.EmpresaId == input.conta.EmpresaId)
        //            ;

        //        var cotacao = cotacoesPorConvenio
        //                    .Where(_ => _.DataInicio <= DateTime.Now /*&& _.DataFinal >= DateTime.Now*/)
        //                    .OrderByDescending(_ => _.DataInicio)
        //                    .FirstOrDefault();

        //        // Filtrar cotacoes por plano
        //        var cotacoesPorPlano = cotacoesPorEmpresa
        //            .Where(x => x.PlanoId != null)
        //            .Where(c => c.PlanoId == input.conta.PlanoId); // se planoId for null vai pegar todos com planoId null

        //        // Caso haja para um plano especifico
        //        if (cotacoesPorPlano.Count() > 0)
        //        {
        //            // Por subGrupo especifico
        //            var cotacaoPorSubGrupo = cotacoesPorPlano
        //                .Where(_ => _.SubGrupoId == input.i.FaturamentoItem.SubGrupoId && _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now)
        //                .OrderByDescending(_ => _.DataInicio)
        //                .FirstOrDefault();

        //            if (cotacaoPorSubGrupo != null)
        //            {
        //                cotacao = cotacaoPorSubGrupo;
        //            }
        //            else
        //            {
        //                // Por grupo especifico
        //                var cotacaoPorGrupo = cotacoesPorPlano
        //                    .Where(_ => _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now && _.GrupoId == input.i.FaturamentoItem.GrupoId)
        //                    .OrderByDescending(_ => _.DataInicio)
        //                    .FirstOrDefault();

        //                if (cotacaoPorGrupo != null)
        //                {
        //                    cotacao = cotacaoPorGrupo;
        //                }
        //            }
        //        }
        //        // Caso seja para todos os planos
        //        else
        //        {
        //            // Por subGrupo
        //            var cotacaoPorSubGrupo = cotacoesPorConvenio
        //                .Where(_ => _.DataInicio <= DateTime.Now /*&& _.DataFinal >= DateTime.Now*/)
        //                .OrderByDescending(_ => _.DataInicio)
        //                .FirstOrDefault(_ => _.SubGrupoId == input.i.FaturamentoItem.SubGrupoId);

        //            if (cotacaoPorSubGrupo != null)
        //            {
        //                cotacao = cotacaoPorSubGrupo;
        //            }
        //            else
        //            {
        //                // Por grupo
        //                var cotacaoPorGrupo = cotacoesPorConvenio
        //                    .Where(_ => _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now)
        //                    .OrderByDescending(_ => _.DataInicio)
        //                    .FirstOrDefault(_ => _.GrupoId == input.i.FaturamentoItem.GrupoId);

        //                if (cotacaoPorGrupo != null)
        //                {
        //                    cotacao = cotacaoPorGrupo;
        //                }
        //            }

        //        }

        //        // Filme
        //        cotacaoInput.Filtro = "1";//AQUI DEVE SER A ID FIXA DA MOEDA 'FILME' - CRIAR SEED NO EF PARA MOEDAS 'FIXAS' DO SISTEMA
        //        var cotacaoFilme = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
        //                                .Items
        //                                .Where(_ => _.DataInicio <= DateTime.Now)
        //                                .OrderByDescending(_ => _.DataInicio)
        //                                .FirstOrDefault();

        //        var totalFilme = cotacaoFilme.Valor * input.i.MetragemFilme;

        //        // Porte
        //        var tabela = await _tabelaAppService.Obter((long)tabelaId);
        //        float totalPorte = 0f;
        //        if (tabela.IsCBHPM)
        //        {
        //            cotacaoInput.Filtro = moeda.Id.ToString();
        //            var cotacaoPorte = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
        //                                    .Items
        //                                    .Where(_ => _.DataInicio <= DateTime.Now)
        //                                    .OrderByDescending(_ => _.DataInicio)
        //                                    .FirstOrDefault();

        //            totalPorte = cotacaoPorte.Valor * preco.Porte;
        //        }

        //        // Valor unitario
        //        var valorUnitario = ((preco.Preco * cotacao.Valor) + totalFilme + totalPorte);
        //        return valorUnitario;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return 0f;
        //    }


        //}

        public async Task<PagedResultDto<FaturamentoContaItemViewModel>> ListarVM(ListarFaturamentoContaItensInput input)
        {
            var contarContaItens = 0;
            List<FaturamentoContaItem> contaItens;
            List<FaturamentoContaItemViewModel> contaItensDtos = new List<FaturamentoContaItemViewModel>();
            try
            {
                var query = _contaItemRepository
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro)
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.FaturamentoItem.Grupo)
                    .Include(i => i.FaturamentoItem.Grupo.TipoGrupo)
                    .Include(i => i.Turno)
                    // .Include(t => t.TipoLeito)
                    .Include(t => t.TipoAcomodacao)
                    .Include(u => u.UnidadeOrganizacional)
                    ;

                contarContaItens = await query
                    .CountAsync();

                contaItens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //     var conta = _contaAppService.Obter((long)int.Parse(input.Filtro));

                foreach (var item in contaItens)
                {
                    var i = new FaturamentoContaItemViewModel();
                    input.CalculoContaItemInput.FatContaItemDto = item.MapTo<FaturamentoContaItemDto>();// DEMORANDO PRA CARALHO TOMAR NO CU

                    ///////////////////////////////////////////////
                    //     i.ValorUnitario =  await CalcularValorUnitarioContaItem(input.CalculoContaItemInput);
                    i.ValorTotal = i.ValorUnitario * i.Qtde;
                    /////////////////////////////////////////////

                    i.Id = item.Id;
                    i.Grupo = item.FaturamentoItem.Grupo != null ? item.FaturamentoItem.Grupo.Descricao : string.Empty;
                    i.Tipo = item.FaturamentoItem.Grupo != null ? (item.FaturamentoItem.Grupo.TipoGrupo != null ? item.FaturamentoItem.Grupo.TipoGrupo.Descricao : string.Empty) : string.Empty;
                    i.Descricao = item.FaturamentoItem.Descricao;
                    i.FaturamentoItemId = item.FaturamentoItemId;
                    i.FaturamentoItemDescricao = item.FaturamentoItem != null ? item.FaturamentoItem.Descricao : string.Empty;
                    i.FaturamentoContaId = item.FaturamentoContaId;
                    i.Data = item.Data;
                    i.Qtde = item.Qtde;
                    i.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                    i.UnidadeOrganizacionalDescricao = item.UnidadeOrganizacional != null ? item.UnidadeOrganizacional.Descricao : string.Empty;
                    i.TerceirizadoId = item.TerceirizadoId;
                    i.TerceirizadoDescricao = item.Terceirizado != null ? item.Terceirizado.Codigo : string.Empty;
                    i.CentroCustoId = item.CentroCustoId;
                    i.CentroCustoDescricao = item.CentroCusto != null ? item.CentroCusto.Descricao : string.Empty;
                    i.TurnoId = item.TurnoId;
                    i.TurnoDescricao = item.Turno != null ? item.Turno.Descricao : string.Empty;
                    //i.TipoLeitoId                    = item.TipoLeitoId;
                    //i.TipoLeitoDescricao             = item.TipoLeito != null ? item.TipoLeito.Descricao : string.Empty;

                    i.TipoLeitoId = item.TipoAcomodacaoId;
                    i.TipoLeitoDescricao = item.TipoAcomodacao != null ? item.TipoAcomodacao.Descricao : string.Empty;


                    i.ValorTemp = item.ValorTemp;
                    i.MedicoId = item.MedicoId;
                    i.MedicoNome = item.Medico != null ? item.Medico.NomeCompleto : string.Empty;
                    i.IsMedCredenciado = item.IsMedCredenciado;
                    i.IsGlosaMedico = item.IsGlosaMedico;
                    i.MedicoEspecialidadeId = item.MedicoEspecialidadeId;
                    i.MedicoEspecialidadeNome = item.MedicoEspecialidade != null ? item.MedicoEspecialidade.Especialidade.Nome : string.Empty;
                    i.FaturamentoContaKitId = item.FaturamentoContaKitId;
                    i.IsCirurgia = item.IsCirurgia;
                    i.ValorAprovado = item.ValorAprovado;
                    i.ValorTaxas = item.ValorTaxas;
                    i.IsValorItemManual = item.IsValorItemManual;
                    i.ValorItem = item.ValorItem;
                    i.HMCH = item.HMCH;
                    i.ValorFilme = item.ValorFilme;
                    i.ValorFilmeAprovado = item.ValorFilmeAprovado;
                    i.ValorCOCH = item.ValorCOCH;
                    i.ValorCOCHAprovado = item.ValorCOCHAprovado;
                    i.Percentual = item.Percentual;
                    i.IsInstrCredenciado = item.IsInstrCredenciado;
                    i.ValorTotalRecuperado = item.ValorTotalRecuperado;
                    i.ValorTotalRecebido = item.ValorTotalRecebido;
                    i.MetragemFilme = item.MetragemFilme;
                    i.MetragemFilmeAprovada = item.MetragemFilmeAprovada;
                    i.COCH = item.COCH;
                    i.COCHAprovado = item.COCHAprovado;
                    // STATUSNOVO ALTERAR             i.StatusEntrega                  = item.StatusEntrega;
                    i.IsRecuperaMedico = item.IsRecuperaMedico;
                    i.IsAux1Credenciado = item.IsAux1Credenciado;
                    i.IsRecebeAuxiliar1 = item.IsRecebeAuxiliar1;
                    i.IsGlosaAuxiliar1 = item.IsGlosaAuxiliar1;
                    i.IsRecuperaAuxiliar1 = item.IsRecuperaAuxiliar1;
                    i.IsAux2Credenciado = item.IsAux2Credenciado;
                    i.IsRecebeAuxiliar2 = item.IsRecebeAuxiliar2;
                    i.IsGlosaAuxiliar2 = item.IsGlosaAuxiliar2;
                    i.IsRecuperaAuxiliar2 = item.IsRecuperaAuxiliar2;
                    i.IsAux3Credenciado = item.IsAux3Credenciado;
                    i.IsRecebeAuxiliar3 = item.IsRecebeAuxiliar3;
                    i.IsGlosaAuxiliar3 = item.IsGlosaAuxiliar3;
                    i.IsRecuperaAuxiliar3 = item.IsRecuperaAuxiliar3;
                    i.IsRecebeInstrumentador = item.IsRecebeInstrumentador;
                    i.IsGlosaInstrumentador = item.IsGlosaInstrumentador;
                    i.IsRecuperaInstrumentador = item.IsRecuperaInstrumentador;
                    i.Observacao = item.Observacao;
                    i.QtdeRecuperada = item.QtdeRecuperada;
                    i.QtdeAprovada = item.QtdeAprovada;
                    i.QtdeRecebida = item.QtdeRecebida;
                    i.ValorMoedaAprovado = item.ValorMoedaAprovado;
                    i.SisMoedaId = item.SisMoedaId;
                    i.SisMoedaNome = item.SisMoeda != null ? item.SisMoeda.Descricao : string.Empty;
                    i.DataAutorizacao = item.DataAutorizacao;
                    i.SenhaAutorizacao = item.SenhaAutorizacao;
                    i.NomeAutorizacao = item.NomeAutorizacao;
                    i.ObsAutorizacao = item.ObsAutorizacao;
                    i.HoraIncio = item.HoraIncio;
                    i.HoraFim = item.HoraFim;
                    i.ViaAcesso = item.ViaAcesso;
                    i.Tecnica = item.Tecnica;
                    i.ClinicaId = item.ClinicaId;
                    i.FornecedorId = item.FornecedorId;
                    i.FornecedorNome = item.Fornecedor != null ? item.Fornecedor.Descricao : string.Empty;
                    i.NumeroNF = item.NumeroNF;
                    i.IsImportaEstoque = item.IsImportaEstoque;

                    contaItensDtos.Add(i);
                }

                return new PagedResultDto<FaturamentoContaItemViewModel>(contarContaItens, contaItensDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<FaturamentoContaItemReportModel>> ListarReportModel(ListarFaturamentoContaItensInput input)
        {
            var contarContaItens = 0;
            List<FaturamentoContaItem> contaItens;
            List<FaturamentoContaItemReportModel> contaItensDtos = new List<FaturamentoContaItemReportModel>();
            try
            {
                var query = _contaItemRepository
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro)
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.FaturamentoItem.Grupo)
                    .Include(i => i.FaturamentoItem.Grupo.TipoGrupo)
                    .Include(i => i.Turno)
                    // .Include(t => t.TipoLeito)
                    .Include(t => t.TipoAcomodacao)
                    .Include(u => u.UnidadeOrganizacional)
                    .Include(u => u.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(u => u.FaturamentoConta)
                    .Include(u => u.FaturamentoConfigConvenio)
                    ;

                contarContaItens = await query
                    .CountAsync();

                contaItens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();




                //ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();

                //if (contaItens.Count > 0)
                //{
                //    configConvenioInput.Filtro = contaItens[0].FaturamentoConta.ConvenioId.ToString();
                //    configConvenioInput.ConvenioId = contaItens[0].FaturamentoConta.ConvenioId;
                //    configConvenioInput.PlanoId = contaItens[0].FaturamentoConta.PlanoId;
                //    configConvenioInput.EmpresaId = contaItens[0].FaturamentoConta.EmpresaId;

                //    configConvenioInput.GrupoId = contaItens[0].FaturamentoItem.GrupoId;
                //    configConvenioInput.SubGrupoId = contaItens[0].FaturamentoItem.SubGrupoId;
                //    configConvenioInput.ItemId = contaItens[0].FaturamentoItemId;

                //    input.CalculoContaItemInput = new CalculoContaItemInput();
                //    input.CalculoContaItemInput.conta = new ContaCalculoItem();
                //    input.CalculoContaItemInput.conta.ConvenioId = configConvenioInput.ConvenioId ?? 0;
                //    input.CalculoContaItemInput.conta.PlanoId = configConvenioInput.PlanoId ?? 0;
                //    input.CalculoContaItemInput.conta.EmpresaId = configConvenioInput.EmpresaId ?? 0;
                //}

                //var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                // Fim - obtencao de config.convenio



                //var configsPorEmpresa = configsConvenio.Items
                //       .Where(c => c.EmpresaId == contaItens[0].FaturamentoConta.EmpresaId);

                //// Filtrar por plano
                //var configsPorPlano = configsPorEmpresa
                //    .Where(x => x.PlanoId != null)
                //    .Where(c => c.PlanoId == contaItens[0].FaturamentoConta.PlanoId);




                foreach (var item in contaItens)
                {
                    var i = new FaturamentoContaItemReportModel();



                    //input.CalculoContaItemInput.FatContaItemDto = new FaturamentoContaItemDto();
                    //input.CalculoContaItemInput.FatContaItemDto.Id = item.Id;
                    //input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem = new Itens.Dto.FaturamentoItemDto();
                    //input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.SubGrupoId = item.FaturamentoItem?.SubGrupoId;
                    //input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.GrupoId = item.FaturamentoItem?.GrupoId;
                    //input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.Id = item.FaturamentoItemId ?? 0;
                    //input.CalculoContaItemInput.FatContaItemDto.FaturamentoItemId = item.FaturamentoItemId ?? 0;
                    //input.CalculoContaItemInput.FatContaItemDto.MetragemFilme = item.MetragemFilme;

                    ///////////////////////////////////////////////////////////
                    // ============== CALCULO DE VALOR UNITARIO ===============
                    // Filtrar por empresa
                    //var configsPorEmpresa = configsConvenio.Items
                    //    .Where(c => c.EmpresaId == item.FaturamentoConta.EmpresaId);

                    //// Filtrar por plano
                    //var configsPorPlano = configsPorEmpresa
                    //    .Where(x => x.PlanoId != null)
                    //    .Where(c => c.PlanoId == item.FaturamentoConta.PlanoId);

                    //input.CalculoContaItemInput.configsPorEmpresa = configsPorEmpresa.ToArray();
                    //input.CalculoContaItemInput.configsPorPlano = configsPorPlano.ToArray();

                    // Valor manual ou calculado em tempo de execucao
                    //i.IsValorItemManual = item.IsValorItemManual;
                    //if (i.IsValorItemManual)
                    //{
                    //    i.ValorItem = item.ValorItem;
                    //    // i.ValorTotal = i.ValorItem * item.Qtde;
                    //}
                    //else
                    //{
                    //    i.ValorItem = await CalcularValorUnitarioContaItem(input.CalculoContaItemInput);
                    //    // i.ValorItem = i.ValorUnitario;
                    //    // i. .ValorTotal = i.ValorUnitario * item.Qtde;
                    //}


                    //long? tabelaId = null;

                    //if (item.FaturamentoConfigConvenio != null)
                    //{
                    //    tabelaId = item.FaturamentoConfigConvenio.TabelaId;
                    //}
                    //if (!item.IsValorItemManual)
                    //{
                    //    var fatItemTabela = _faturamentoItemTabela.GetAll()
                    //                            .Where(w => w.ItemId == item.FaturamentoItemId
                    //                                    && w.TabelaId == tabelaId)
                    //                            .FirstOrDefault();
                    //    if (fatItemTabela != null)
                    //    {
                    //        i.ValorItem = fatItemTabela.Preco;
                    //    }
                    //    else
                    //    {
                    //        i.ValorItem = 0;
                    //    }
                    //}


                    //i.ValorTotal = i.ValorItem * item.Qtde;






                    i.Id = item.Id;
                    i.Grupo = item.FaturamentoItem.Grupo != null ? item.FaturamentoItem.Grupo.Descricao : string.Empty;
                    i.Tipo = item.FaturamentoItem.Grupo != null ? (item.FaturamentoItem.Grupo.TipoGrupo != null ? item.FaturamentoItem.Grupo.TipoGrupo.Descricao : string.Empty) : string.Empty;
                    i.Descricao = item.FaturamentoItem.Descricao;
                    i.FaturamentoItemId = item.FaturamentoItemId;
                    i.FaturamentoItemCodigo = item.FaturamentoItem != null ? item.FaturamentoItem.Codigo : string.Empty;
                    i.FaturamentoItemDescricao = item.FaturamentoItem != null ? item.FaturamentoItem.Descricao : string.Empty;
                    i.FaturamentoContaId = item.FaturamentoContaId;
                    i.Data = item.Data;
                    i.Qtde = item.Qtde;
                    i.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                    i.UnidadeOrganizacionalDescricao = item.UnidadeOrganizacional != null ? item.UnidadeOrganizacional.Descricao : string.Empty;
                    i.TerceirizadoId = item.TerceirizadoId;
                    i.TerceirizadoDescricao = item.Terceirizado != null ? item.Terceirizado.Codigo : string.Empty;
                    i.CentroCustoId = item.CentroCustoId;
                    i.CentroCustoDescricao = item.CentroCusto != null ? item.CentroCusto.Descricao : string.Empty;
                    i.TurnoId = item.TurnoId;
                    i.TurnoDescricao = item.Turno != null ? item.Turno.Descricao : string.Empty;
                    //i.TipoLeitoId                    = item.TipoLeitoId;
                    //i.TipoLeitoDescricao             = item.TipoLeito != null ? item.TipoLeito.Descricao : string.Empty;

                    i.TipoLeitoId = item.TipoAcomodacaoId;
                    i.TipoLeitoDescricao = item.TipoAcomodacao != null ? item.TipoAcomodacao.Descricao : string.Empty;

                    i.ValorTemp = item.ValorTemp;
                    i.MedicoId = item.MedicoId;
                    i.MedicoNome = item.Medico != null ? item.Medico.NomeCompleto : string.Empty;
                    i.IsMedCrendenciado = item.IsMedCredenciado;
                    i.IsGlosaMedico = item.IsGlosaMedico;
                    i.MedicoEspecialidadeId = item.MedicoEspecialidadeId;
                    i.MedicoEspecialidadeNome = item.MedicoEspecialidade != null ? item.MedicoEspecialidade.Especialidade.Nome : string.Empty;
                    i.FaturamentoContaKitId = item.FaturamentoContaKitId;
                    i.IsCirurgia = item.IsCirurgia;
                    i.ValorAprovado = item.ValorAprovado;
                    i.ValorTaxas = item.ValorTaxas;
                    i.IsValorItemManual = item.IsValorItemManual;
                    // i.ValorItem = item.ValorItem;
                    i.HMCH = item.HMCH;
                    i.ValorFilme = item.ValorFilme;
                    i.ValorFilmeAprovado = item.ValorFilmeAprovado;
                    i.ValorCOCH = item.ValorCOCH;
                    i.ValorCOCHAprovado = item.ValorCOCHAprovado;
                    i.Percentual = item.Percentual;
                    i.IsInstrCredenciado = item.IsInstrCredenciado;
                    i.ValorTotalRecuperado = item.ValorTotalRecuperado;
                    i.ValorTotalRecebido = item.ValorTotalRecebido;
                    i.MetragemFilme = item.MetragemFilme;
                    i.MetragemFilmeAprovada = item.MetragemFilmeAprovada;
                    i.COCH = item.COCH;
                    i.COCHAprovado = item.COCHAprovado;
                    // STATUSNOVO ALTERAR         i.StatusEntrega                  = item.StatusEntrega;
                    i.IsRecuperaMedico = item.IsRecuperaMedico;
                    i.IsAux1Credenciado = item.IsAux1Credenciado;
                    i.IsRecebeAuxiliar1 = item.IsRecebeAuxiliar1;
                    i.IsGlosaAuxiliar1 = item.IsGlosaAuxiliar1;
                    i.IsRecuperaAuxiliar1 = item.IsRecuperaAuxiliar1;
                    i.IsAux2Credenciado = item.IsAux2Credenciado;
                    i.IsRecebeAuxiliar2 = item.IsRecebeAuxiliar2;
                    i.IsGlosaAuxiliar2 = item.IsGlosaAuxiliar2;
                    i.IsRecuperaAuxiliar2 = item.IsRecuperaAuxiliar2;
                    i.IsAux3Credenciado = item.IsAux3Credenciado;
                    i.IsRecebeAuxiliar3 = item.IsRecebeAuxiliar3;
                    i.IsGlosaAuxiliar3 = item.IsGlosaAuxiliar3;
                    i.IsRecuperaAuxiliar3 = item.IsRecuperaAuxiliar3;
                    i.IsRecebeInstrumentador = item.IsRecebeInstrumentador;
                    i.IsGlosaInstrumentador = item.IsGlosaInstrumentador;
                    i.IsRecuperaInstrumentador = item.IsRecuperaInstrumentador;
                    i.Observacao = item.Observacao;
                    i.QtdeRecuperada = item.QtdeRecuperada;
                    i.QtdeAprovada = item.QtdeAprovada;
                    i.QtdeRecebida = item.QtdeRecebida;
                    i.ValorMoedaAprovado = item.ValorMoedaAprovado;
                    i.SisMoedaId = item.SisMoedaId;
                    i.SisMoedaNome = item.SisMoeda != null ? item.SisMoeda.Descricao : string.Empty;
                    i.DataAutorizacao = item.DataAutorizacao;
                    i.SenhaAutorizacao = item.SenhaAutorizacao;
                    i.NomeAutorizacao = item.NomeAutorizacao;
                    i.ObsAutorizacao = item.ObsAutorizacao;
                    i.HoraIncio = item.HoraIncio;
                    i.HoraFim = item.HoraFim;
                    i.ViaAcesso = item.ViaAcesso;
                    i.Tecnica = item.Tecnica;
                    i.ClinicaId = item.ClinicaId;
                    i.FornecedorId = item.FornecedorId;
                    i.FornecedorNome = item.Fornecedor != null ? item.Fornecedor.Descricao : string.Empty;
                    i.NumeroNF = item.NumeroNF;
                    i.IsImportaEstoque = item.IsImportaEstoque;
                    i.ValorItem = item.ValorItem;
                  

                    contaItensDtos.Add(i);
                }

                return new PagedResultDto<FaturamentoContaItemReportModel>(contarContaItens, contaItensDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        public async Task CriarOuEditar(FaturamentoContaItemDto input)
        {
            try
            {

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var i = new FaturamentoContaItem();

                    i.Id = input.Id;
                    i.Descricao = input.FaturamentoItem != null ? input.FaturamentoItem.Descricao : string.Empty;
                    i.FaturamentoItemId = input.FaturamentoItemId;
                    i.FaturamentoContaId = input.FaturamentoContaId;
                    i.Data = input.Data;
                    i.Qtde = input.Qtde;
                    i.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                    i.TerceirizadoId = input.TerceirizadoId;
                    i.CentroCustoId = input.CentroCustoId;
                    i.TurnoId = input.TurnoId;
                    // i.TipoLeitoId                    = input.TipoLeitoId;
                    i.TipoAcomodacaoId = input.TipoLeitoId;
                    i.ValorTemp = input.ValorTemp;
                    i.MedicoId = input.MedicoId;
                    i.AnestesistaId = input.AnestesistaId;
                    i.Auxiliar1Id = input.Auxiliar1Id;
                    i.Auxiliar2Id = input.Auxiliar2Id;
                    i.Auxiliar3Id = input.Auxiliar3Id;
                    i.InstrumentadorId = input.InstrumentadorId;
                    i.IsMedCredenciado = input.IsMedCredenciado;
                    i.IsGlosaMedico = input.IsGlosaMedico;
                    i.MedicoEspecialidadeId = input.MedicoEspecialidadeId;
                    i.Auxiliar1EspecialidadeId = input.Auxiliar1EspecialidadeId;
                    i.Auxiliar2EspecialidadeId = input.Auxiliar2EspecialidadeId;
                    i.Auxiliar3EspecialidadeId = input.Auxiliar3EspecialidadeId;
                    i.AnestesistaEspecialidadeId = input.EspecialidadeAnestesistaId;
                    i.InstrumentadorEspecialidadeId = input.InstrumentadorId;
                    i.FaturamentoContaKitId = input.FaturamentoContaKitId;
                    i.IsCirurgia = input.IsCirurgia;
                    i.ValorAprovado = input.ValorAprovado;
                    i.ValorTaxas = input.ValorTaxas;
                    i.IsValorItemManual = input.IsValorItemManual;
                    i.ValorItem = input.ValorItem;
                    i.HMCH = input.HMCH;
                    i.ValorFilme = input.ValorFilme;
                    i.ValorFilmeAprovado = input.ValorFilmeAprovado;
                    i.ValorCOCH = input.ValorCOCH;
                    i.ValorCOCHAprovado = input.ValorCOCHAprovado;
                    i.Percentual = input.Percentual;
                    i.IsInstrCredenciado = input.IsInstrCredenciado;
                    i.ValorTotalRecuperado = input.ValorTotalRecuperado;
                    i.ValorTotalRecebido = input.ValorTotalRecebido;
                    i.MetragemFilme = input.MetragemFilme;
                    i.MetragemFilmeAprovada = input.MetragemFilmeAprovada;
                    i.COCH = input.COCH;
                    i.COCHAprovado = input.COCHAprovado;
                    // STATUSNOVO ALTERAR        i.StatusEntrega                  = input.StatusEntrega;
                    i.IsRecuperaMedico = input.IsRecuperaMedico;
                    i.IsAux1Credenciado = input.IsAux1Credenciado;
                    i.IsRecebeAuxiliar1 = input.IsRecebeAuxiliar1;
                    i.IsGlosaAuxiliar1 = input.IsGlosaAuxiliar1;
                    i.IsRecuperaAuxiliar1 = input.IsRecuperaAuxiliar1;
                    i.IsAux2Credenciado = input.IsAux2Credenciado;
                    i.IsRecebeAuxiliar2 = input.IsRecebeAuxiliar2;
                    i.IsGlosaAuxiliar2 = input.IsGlosaAuxiliar2;
                    i.IsRecuperaAuxiliar2 = input.IsRecuperaAuxiliar2;
                    i.IsAux3Credenciado = input.IsAux3Credenciado;
                    i.IsRecebeAuxiliar3 = input.IsRecebeAuxiliar3;
                    i.IsGlosaAuxiliar3 = input.IsGlosaAuxiliar3;
                    i.IsRecuperaAuxiliar3 = input.IsRecuperaAuxiliar3;
                    i.IsRecebeInstrumentador = input.IsRecebeInstrumentador;
                    i.IsGlosaInstrumentador = input.IsGlosaInstrumentador;
                    i.IsRecuperaInstrumentador = input.IsRecuperaInstrumentador;
                    i.Observacao = input.Observacao;
                    i.QtdeRecuperada = input.QtdeRecuperada;
                    i.QtdeAprovada = input.QtdeAprovada;
                    i.QtdeRecebida = input.QtdeRecebida;
                    i.ValorMoedaAprovado = input.ValorMoedaAprovado;
                    i.SisMoedaId = input.SisMoedaId;
                    i.DataAutorizacao = input.DataAutorizacao;
                    i.SenhaAutorizacao = input.SenhaAutorizacao;
                    i.NomeAutorizacao = input.NomeAutorizacao;
                    i.ObsAutorizacao = input.ObsAutorizacao;
                    i.HoraIncio = input.HoraIncio;
                    i.HoraFim = input.HoraFim;
                    i.ViaAcesso = input.ViaAcesso;
                    i.Tecnica = input.Tecnica;
                    i.ClinicaId = input.ClinicaId;
                    i.FornecedorId = input.FornecedorId;
                    i.NumeroNF = input.NumeroNF;
                    i.IsImportaEstoque = input.IsImportaEstoque;
                    i.FaturamentoConfigConvenioId = input.FaturamentoConfigConvenioId;
                    i.FaturamentoPacoteId = input.FaturamentoPacoteId;

                    if (input.Id.Equals(0))
                    {
                        await _contaItemRepository.InsertAsync(i);
                    }
                    else
                    {
                        var item = _contaItemRepository.GetAll()
                                                .Where(w => w.Id == input.Id)
                                                .FirstOrDefault();

                        item.Id = input.Id;
                        item.Descricao = input.FaturamentoItem != null ? input.FaturamentoItem.Descricao : string.Empty;
                        item.FaturamentoItemId = input.FaturamentoItemId;
                        //  item.FaturamentoContaId = input.FaturamentoContaId;
                        item.Data = input.Data;
                        item.Qtde = input.Qtde;
                        item.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                        item.TerceirizadoId = input.TerceirizadoId;
                        item.CentroCustoId = input.CentroCustoId;
                        item.TurnoId = input.TurnoId;
                        // item.TipoLeitoId                    = input.TipoLeitoId;
                        item.TipoAcomodacaoId = input.TipoLeitoId;
                        item.ValorTemp = input.ValorTemp;
                        item.MedicoId = input.MedicoId;
                        item.AnestesistaId = input.AnestesistaId;
                        item.Auxiliar1Id = input.Auxiliar1Id;
                        item.Auxiliar2Id = input.Auxiliar2Id;
                        item.Auxiliar3Id = input.Auxiliar3Id;
                        item.InstrumentadorId = input.InstrumentadorId;
                        item.IsMedCredenciado = input.IsMedCredenciado;
                        item.IsGlosaMedico = input.IsGlosaMedico;
                        item.MedicoEspecialidadeId = input.MedicoEspecialidadeId;
                        item.Auxiliar1EspecialidadeId = input.Auxiliar1EspecialidadeId;
                        item.Auxiliar2EspecialidadeId = input.Auxiliar2EspecialidadeId;
                        item.Auxiliar3EspecialidadeId = input.Auxiliar3EspecialidadeId;
                        item.AnestesistaEspecialidadeId = input.EspecialidadeAnestesistaId;
                        item.InstrumentadorEspecialidadeId = input.InstrumentadorEspecialidadeId;
                        item.FaturamentoContaKitId = input.FaturamentoContaKitId;
                        item.IsCirurgia = input.IsCirurgia;
                        item.ValorAprovado = input.ValorAprovado;
                        item.ValorTaxas = input.ValorTaxas;
                        item.IsValorItemManual = input.IsValorItemManual;
                        item.ValorItem = input.ValorItem;
                        item.HMCH = input.HMCH;
                        item.ValorFilme = input.ValorFilme;
                        item.ValorFilmeAprovado = input.ValorFilmeAprovado;
                        item.ValorCOCH = input.ValorCOCH;
                        item.ValorCOCHAprovado = input.ValorCOCHAprovado;
                        item.Percentual = input.Percentual;
                        item.IsInstrCredenciado = input.IsInstrCredenciado;
                        item.ValorTotalRecuperado = input.ValorTotalRecuperado;
                        item.ValorTotalRecebido = input.ValorTotalRecebido;
                        item.MetragemFilme = input.MetragemFilme;
                        item.MetragemFilmeAprovada = input.MetragemFilmeAprovada;
                        item.COCH = input.COCH;
                        item.COCHAprovado = input.COCHAprovado;
                        // STATUSNOVO ALTERAR        item.StatusEntrega                  = input.StatusEntrega;
                        item.IsRecuperaMedico = input.IsRecuperaMedico;
                        item.IsAux1Credenciado = input.IsAux1Credenciado;
                        item.IsRecebeAuxiliar1 = input.IsRecebeAuxiliar1;
                        item.IsGlosaAuxiliar1 = input.IsGlosaAuxiliar1;
                        item.IsRecuperaAuxiliar1 = input.IsRecuperaAuxiliar1;
                        item.IsAux2Credenciado = input.IsAux2Credenciado;
                        item.IsRecebeAuxiliar2 = input.IsRecebeAuxiliar2;
                        item.IsGlosaAuxiliar2 = input.IsGlosaAuxiliar2;
                        item.IsRecuperaAuxiliar2 = input.IsRecuperaAuxiliar2;
                        item.IsAux3Credenciado = input.IsAux3Credenciado;
                        item.IsRecebeAuxiliar3 = input.IsRecebeAuxiliar3;
                        item.IsGlosaAuxiliar3 = input.IsGlosaAuxiliar3;
                        item.IsRecuperaAuxiliar3 = input.IsRecuperaAuxiliar3;
                        item.IsRecebeInstrumentador = input.IsRecebeInstrumentador;
                        item.IsGlosaInstrumentador = input.IsGlosaInstrumentador;
                        item.IsRecuperaInstrumentador = input.IsRecuperaInstrumentador;
                        item.Observacao = input.Observacao;
                        item.QtdeRecuperada = input.QtdeRecuperada;
                        item.QtdeAprovada = input.QtdeAprovada;
                        item.QtdeRecebida = input.QtdeRecebida;
                        item.ValorMoedaAprovado = input.ValorMoedaAprovado;
                        item.SisMoedaId = input.SisMoedaId;
                        item.DataAutorizacao = input.DataAutorizacao;
                        item.SenhaAutorizacao = input.SenhaAutorizacao;
                        item.NomeAutorizacao = input.NomeAutorizacao;
                        item.ObsAutorizacao = input.ObsAutorizacao;
                        item.HoraIncio = input.HoraIncio;
                        item.HoraFim = input.HoraFim;
                        item.ViaAcesso = input.ViaAcesso;
                        item.Tecnica = input.Tecnica;
                        item.ClinicaId = input.ClinicaId;
                        item.FornecedorId = input.FornecedorId;
                        item.NumeroNF = input.NumeroNF;
                        item.IsImportaEstoque = input.IsImportaEstoque;
                        item.FaturamentoConfigConvenioId = input.FaturamentoConfigConvenioId;
                        item.FaturamentoPacoteId = input.FaturamentoPacoteId;

                        await _contaItemRepository.UpdateAsync(item);
                    }
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(FaturamentoContaItemDto input)
        {
            try
            {
                await _contaItemRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task ExcluirVM(long id)
        {
            try
            {
                await _contaItemRepository.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoContaItemDto> Obter(long id)
        {
            try
            {
                var query = await _contaItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Turno)
                    // .Include(m => m.TipoLeito)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.Terceirizado)
                    .Include(m => m.UnidadeOrganizacional)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var conta = query
                    .MapTo<FaturamentoContaItemDto>();

                return conta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FaturamentoContaItemDto> ObterPorCodigo(string codigo)
        {
            try
            {
                var query = await _contaItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Turno)
                    // .Include(m => m.TipoLeito)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.Terceirizado)
                    .Include(m => m.UnidadeOrganizacional)
                    .Where(m => m.Codigo == codigo)
                    .FirstOrDefaultAsync();

                var contaItem = query.MapTo<FaturamentoContaItemDto>();
                return contaItem;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FaturamentoContaItemViewModel> ObterViewModel(long id)
        {
            try
            {
                var item = await _contaItemRepository
                    .GetAll()
                    .Include(m => m.Turno)
                    //  .Include(m => m.TipoLeito)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.Terceirizado)
                    .Include(m => m.CentroCusto)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.Auxiliar1)
                    .Include(m => m.Auxiliar2)
                    .Include(m => m.Auxiliar3)
                    .Include(m => m.Anestesista)
                    .Include(m => m.Instrumentador)
                    .Include(m => m.MedicoEspecialidade)
                    .Include(m => m.Auxiliar1Especialidade.Especialidade)
                    .Include(m => m.Auxiliar2Especialidade.Especialidade)
                    .Include(m => m.Auxiliar3Especialidade.Especialidade)
                    .Include(m => m.AnestesistaEspecialidade)
                    .Include(m => m.InstrumentadorEspecialidade.Especialidade)
                    .Include(m => m.FaturamentoPacote)
                    .Include(m => m.FaturamentoPacote.FaturamentoItem)
                    .Include(m => m.FaturamentoItemCobrado)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var i = new FaturamentoContaItemViewModel();

                var fatItem = _fatItemRepository.GetAll().Include(o => o.Grupo.TipoGrupo).FirstOrDefault(y => y.Id == item.FaturamentoItemId).MapTo<FaturamentoItemDto>();

                i.Id = item.Id;
                i.Descricao = item.Descricao;
                i.FaturamentoItemId = item.FaturamentoItemId;
                i.FaturamentoItemDescricao = item.FaturamentoItem != null ? item.FaturamentoItem.Descricao : string.Empty;
                i.FaturamentoContaId = item.FaturamentoContaId;
                i.FatItem = fatItem;
                i.Data = item.Data;
                i.Qtde = item.Qtde;
                i.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                i.UnidadeOrganizacionalDescricao = item.UnidadeOrganizacional != null ? item.UnidadeOrganizacional.Descricao : string.Empty;
                i.TerceirizadoId = item.TerceirizadoId;
                i.TerceirizadoDescricao = item.Terceirizado != null ? item.Terceirizado.Codigo : string.Empty;
                i.CentroCustoId = item.CentroCustoId;
                i.CentroCustoDescricao = item.CentroCusto != null ? item.CentroCusto.Descricao : string.Empty;
                i.TurnoId = item.TurnoId;
                i.TurnoDescricao = item.Turno != null ? item.Turno.Descricao : string.Empty;
                //i.TipoLeitoId                     = item.TipoLeitoId;
                //i.TipoLeitoDescricao              = item.TipoLeito != null ? item.TipoLeito.Descricao : string.Empty;
                i.TipoLeitoId = item.TipoAcomodacaoId;
                i.TipoLeitoDescricao = item.TipoAcomodacao != null ? item.TipoAcomodacao.Descricao : string.Empty;
                i.ValorTemp = item.ValorTemp;
                i.MedicoId = item.MedicoId;
                i.MedicoNome = item.Medico != null ? item.Medico.NomeCompleto : string.Empty;
                i.IsMedCredenciado = item.IsMedCredenciado;
                i.IsGlosaMedico = item.IsGlosaMedico;
                i.MedicoId = item.MedicoId;
                i.MedicoNome = item.Medico != null ? item.Medico.NomeCompleto : string.Empty;
                i.IsMedCredenciado = item.IsMedCredenciado;
                i.MedicoEspecialidadeId = item.MedicoEspecialidadeId;
                i.MedicoEspecialidadeNome = item.MedicoEspecialidade != null ? item.MedicoEspecialidade.Especialidade.Nome : string.Empty;
                i.Auxiliar1Id = item.Auxiliar1Id;
                i.Auxiliar1Nome = item.Auxiliar1 != null ? item.Auxiliar1.NomeCompleto : string.Empty;
                i.IsAux1Credenciado = item.IsAux1Credenciado;
                i.Auxiliar1EspecialidadeId = item.Auxiliar1EspecialidadeId;
                i.Auxiliar1EspecialidadeNome = item.Auxiliar1Especialidade != null ? item.Auxiliar1Especialidade.Especialidade.Nome : string.Empty;
                i.Auxiliar2Id = item.Auxiliar2Id;
                i.Auxiliar2Nome = item.Auxiliar2 != null ? item.Auxiliar2.NomeCompleto : string.Empty;
                i.IsAux2Credenciado = item.IsAux2Credenciado;
                i.Auxiliar2EspecialidadeId = item.Auxiliar2EspecialidadeId;
                i.Auxiliar2EspecialidadeNome = item.Auxiliar2Especialidade != null ? item.Auxiliar2Especialidade.Especialidade.Nome : string.Empty;
                i.Auxiliar3Id = item.Auxiliar3Id;
                i.Auxiliar3Nome = item.Auxiliar3 != null ? item.Auxiliar3.NomeCompleto : string.Empty;
                i.IsAux3Credenciado = item.IsAux3Credenciado;
                i.Auxiliar3EspecialidadeId = item.Auxiliar3EspecialidadeId;
                i.Auxiliar3EspecialidadeNome = item.Auxiliar3Especialidade != null ? item.Auxiliar3Especialidade.Especialidade.Nome : string.Empty;
                i.AnestesistaId = item.AnestesistaId;
                i.AnestesistaNome = item.Anestesista != null ? item.Anestesista.NomeCompleto : string.Empty;
                i.AnestesistaEspecialidadeId = item.AnestesistaEspecialidadeId;
                i.InstrumentadorId = item.InstrumentadorId;
                i.InstrumentadorNome = item.Instrumentador != null ? item.Instrumentador.NomeCompleto : string.Empty;
                i.IsInstCredenciado = item.IsInstrCredenciado;
                i.InstrumentadorEspecialidadeId = item.InstrumentadorEspecialidadeId;
                i.InstrumentadorEspecialidadeNome = item.InstrumentadorEspecialidade != null ? item.InstrumentadorEspecialidade.Especialidade.Nome : string.Empty;
                i.FaturamentoContaKitId = item.FaturamentoContaKitId;
                i.IsCirurgia = item.IsCirurgia;
                i.ValorAprovado = item.ValorAprovado;
                i.ValorTaxas = item.ValorTaxas;
                i.IsValorItemManual = item.IsValorItemManual;
                i.ValorItem = item.ValorItem;
                i.HMCH = item.HMCH;
                i.ValorFilme = item.ValorFilme;
                i.ValorFilmeAprovado = item.ValorFilmeAprovado;
                i.ValorCOCH = item.ValorCOCH;
                i.ValorCOCHAprovado = item.ValorCOCHAprovado;
                i.Percentual = item.Percentual;
                i.IsInstrCredenciado = item.IsInstrCredenciado;
                i.ValorTotalRecuperado = item.ValorTotalRecuperado;
                i.ValorTotalRecebido = item.ValorTotalRecebido;
                i.MetragemFilme = item.MetragemFilme;
                i.MetragemFilmeAprovada = item.MetragemFilmeAprovada;
                i.COCH = item.COCH;
                i.COCHAprovado = item.COCHAprovado;
                // STATUSNOVO ALTERAR          i.StatusEntrega                   = item.StatusEntrega;
                i.IsRecuperaMedico = item.IsRecuperaMedico;
                i.IsAux1Credenciado = item.IsAux1Credenciado;
                i.IsRecebeAuxiliar1 = item.IsRecebeAuxiliar1;
                i.IsGlosaAuxiliar1 = item.IsGlosaAuxiliar1;
                i.IsRecuperaAuxiliar1 = item.IsRecuperaAuxiliar1;
                i.IsAux2Credenciado = item.IsAux2Credenciado;
                i.IsRecebeAuxiliar2 = item.IsRecebeAuxiliar2;
                i.IsGlosaAuxiliar2 = item.IsGlosaAuxiliar2;
                i.IsRecuperaAuxiliar2 = item.IsRecuperaAuxiliar2;
                i.IsAux3Credenciado = item.IsAux3Credenciado;
                i.IsRecebeAuxiliar3 = item.IsRecebeAuxiliar3;
                i.IsGlosaAuxiliar3 = item.IsGlosaAuxiliar3;
                i.IsRecuperaAuxiliar3 = item.IsRecuperaAuxiliar3;
                i.IsRecebeInstrumentador = item.IsRecebeInstrumentador;
                i.IsGlosaInstrumentador = item.IsGlosaInstrumentador;
                i.IsRecuperaInstrumentador = item.IsRecuperaInstrumentador;
                i.Observacao = item.Observacao;
                i.QtdeRecuperada = item.QtdeRecuperada;
                i.QtdeAprovada = item.QtdeAprovada;
                i.QtdeRecebida = item.QtdeRecebida;
                i.ValorMoedaAprovado = item.ValorMoedaAprovado;
                i.SisMoedaId = item.SisMoedaId;
                i.SisMoedaNome = item.SisMoeda != null ? item.SisMoeda.Descricao : string.Empty;
                i.DataAutorizacao = item.DataAutorizacao;
                i.SenhaAutorizacao = item.SenhaAutorizacao;
                i.NomeAutorizacao = item.NomeAutorizacao;
                i.ObsAutorizacao = item.ObsAutorizacao;
                i.HoraIncio = item.HoraIncio;
                i.HoraFim = item.HoraFim;
                i.ViaAcesso = item.ViaAcesso;
                i.Tecnica = item.Tecnica;
                i.ClinicaId = item.ClinicaId;
                i.FornecedorId = item.FornecedorId;
                i.FornecedorNome = item.Fornecedor != null ? item.Fornecedor.Descricao : string.Empty;
                i.NumeroNF = item.NumeroNF;
                i.IsImportaEstoque = item.IsImportaEstoque;
                i.FaturamentoConfigConvenioId = item.FaturamentoConfigConvenioId;
                i.FaturamentoPacoteId = item.FaturamentoPacoteId;

                if (item.FaturamentoPacote != null)
                {
                    i.FaturamentoPacoteDto = new FaturamentoPacoteDto { Id = item.FaturamentoPacote.Id, Descricao = item.FaturamentoPacote.FaturamentoItem.Descricao };
                }

                if (item.FaturamentoItemCobrado != null)
                {
                    i.FaturamentoItemCobrado = FaturamentoItemDto.Mapear(item.FaturamentoItemCobrado);
                }

                return i;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FaturamentoContaItemReportModel> ObterReportModel(long id)
        {
            try
            {
                var item = await _contaItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Turno)
                    //.Include(m => m.TipoLeito)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.Terceirizado)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var i = new FaturamentoContaItemReportModel();

                i.Id = item.Id;
                i.Descricao = item.Descricao;
                i.FaturamentoItemId = item.FaturamentoItemId;
                i.FaturamentoItemDescricao = item.FaturamentoItem != null ? item.FaturamentoItem.Descricao : string.Empty;
                i.FaturamentoContaId = item.FaturamentoContaId;
                i.Data = item.Data;
                i.Qtde = item.Qtde;
                i.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                i.UnidadeOrganizacionalDescricao = item.UnidadeOrganizacional != null ? item.UnidadeOrganizacional.Descricao : string.Empty;
                i.TerceirizadoId = item.TerceirizadoId;
                i.TerceirizadoDescricao = item.Terceirizado != null ? item.Terceirizado.Codigo : string.Empty;
                i.CentroCustoId = item.CentroCustoId;
                i.CentroCustoDescricao = item.CentroCusto != null ? item.CentroCusto.Descricao : string.Empty;
                i.TurnoId = item.TurnoId;
                i.TurnoDescricao = item.Turno != null ? item.Turno.Descricao : string.Empty;
                //i.TipoLeitoId                    = item.TipoLeitoId;
                //i.TipoLeitoDescricao             = item.TipoLeito != null ? item.TipoLeito.Descricao : string.Empty;

                i.TipoLeitoId = item.TipoAcomodacaoId;
                i.TipoLeitoDescricao = item.TipoAcomodacao != null ? item.TipoAcomodacao.Descricao : string.Empty;

                i.IsGlosaMedico = item.IsGlosaMedico;
                i.ValorTemp = item.ValorTemp;
                i.MedicoId = item.MedicoId;
                i.MedicoNome = item.Medico != null ? item.Medico.NomeCompleto : string.Empty;
                i.IsMedCrendenciado = item.IsMedCredenciado;
                i.IsGlosaMedico = item.IsGlosaMedico;
                i.MedicoEspecialidadeId = item.MedicoEspecialidadeId;
                i.MedicoEspecialidadeNome = item.MedicoEspecialidade != null ? item.MedicoEspecialidade.Especialidade.Nome : string.Empty;
                i.FaturamentoContaKitId = item.FaturamentoContaKitId;
                i.IsCirurgia = item.IsCirurgia;
                i.ValorAprovado = item.ValorAprovado;
                i.ValorTaxas = item.ValorTaxas;
                i.IsValorItemManual = item.IsValorItemManual;
                i.ValorItem = item.ValorItem;
                i.HMCH = item.HMCH;
                i.ValorFilme = item.ValorFilme;
                i.ValorFilmeAprovado = item.ValorFilmeAprovado;
                i.ValorCOCH = item.ValorCOCH;
                i.ValorCOCHAprovado = item.ValorCOCHAprovado;
                i.Percentual = item.Percentual;
                i.IsInstrCredenciado = item.IsInstrCredenciado;
                i.ValorTotalRecuperado = item.ValorTotalRecuperado;
                i.ValorTotalRecebido = item.ValorTotalRecebido;
                i.MetragemFilme = item.MetragemFilme;
                i.MetragemFilmeAprovada = item.MetragemFilmeAprovada;
                i.COCH = item.COCH;
                i.COCHAprovado = item.COCHAprovado;
                // STATUSNOVO ALTERAR         i.StatusEntrega                  = item.StatusEntrega;
                i.IsRecuperaMedico = item.IsRecuperaMedico;
                i.IsAux1Credenciado = item.IsAux1Credenciado;
                i.IsRecebeAuxiliar1 = item.IsRecebeAuxiliar1;
                i.IsGlosaAuxiliar1 = item.IsGlosaAuxiliar1;
                i.IsRecuperaAuxiliar1 = item.IsRecuperaAuxiliar1;
                i.IsAux2Credenciado = item.IsAux2Credenciado;
                i.IsRecebeAuxiliar2 = item.IsRecebeAuxiliar2;
                i.IsGlosaAuxiliar2 = item.IsGlosaAuxiliar2;
                i.IsRecuperaAuxiliar2 = item.IsRecuperaAuxiliar2;
                i.IsAux3Credenciado = item.IsAux3Credenciado;
                i.IsRecebeAuxiliar3 = item.IsRecebeAuxiliar3;
                i.IsGlosaAuxiliar3 = item.IsGlosaAuxiliar3;
                i.IsRecuperaAuxiliar3 = item.IsRecuperaAuxiliar3;
                i.IsRecebeInstrumentador = item.IsRecebeInstrumentador;
                i.IsGlosaInstrumentador = item.IsGlosaInstrumentador;
                i.IsRecuperaInstrumentador = item.IsRecuperaInstrumentador;
                i.Observacao = item.Observacao;
                i.QtdeRecuperada = item.QtdeRecuperada;
                i.QtdeAprovada = item.QtdeAprovada;
                i.QtdeRecebida = item.QtdeRecebida;
                i.ValorMoedaAprovado = item.ValorMoedaAprovado;
                i.SisMoedaId = item.SisMoedaId;
                i.SisMoedaNome = item.SisMoeda != null ? item.SisMoeda.Descricao : string.Empty;
                i.DataAutorizacao = item.DataAutorizacao;
                i.SenhaAutorizacao = item.SenhaAutorizacao;
                i.NomeAutorizacao = item.NomeAutorizacao;
                i.ObsAutorizacao = item.ObsAutorizacao;
                i.HoraIncio = item.HoraIncio;
                i.HoraFim = item.HoraFim;
                i.ViaAcesso = item.ViaAcesso;
                i.Tecnica = item.Tecnica;
                i.ClinicaId = item.ClinicaId;
                i.FornecedorId = item.FornecedorId;
                i.FornecedorNome = item.Fornecedor != null ? item.Fornecedor.Descricao : string.Empty;
                i.NumeroNF = item.NumeroNF;
                i.IsImportaEstoque = item.IsImportaEstoque;

                return i;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        //public async Task<float> CalcularValorUntario ()
        //{
        //    float valor = 0f;


        //    return valor;
        //}

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _contaItemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<float> CalcularValorUnitarioContaItemViaFront(long contaItemId)
        {
            if (contaItemId == 0)
                return 0f;

            try
            {
                var contaItem = _contaItemRepository.GetAll()
                    .Include(x => x.FaturamentoConta)
                    .Include(x => x.FaturamentoItem)
                    .Include(x => x.FaturamentoItem.Grupo)
                    .Include(x => x.FaturamentoItem.SubGrupo)
                    .FirstOrDefault(x => x.Id == contaItemId);

                CalculoContaItemInput input = new CalculoContaItemInput();
                input.FatContaItemDto = FaturamentoContaItemDto.MapearFromCore(contaItem);
                ContaCalculoItem contaCalculoItem = new ContaCalculoItem();
                contaCalculoItem.EmpresaId = (long)input.FatContaItemDto.FaturamentoConta?.EmpresaId;
                contaCalculoItem.ConvenioId = (long)input.FatContaItemDto.FaturamentoConta?.ConvenioId;

                if (input.FatContaItemDto.FaturamentoConta?.PlanoId != null)
                {
                    contaCalculoItem.PlanoId = (long)input.FatContaItemDto.FaturamentoConta?.PlanoId;
                }
                input.conta = contaCalculoItem;
                ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
                configConvenioInput.ConvenioId = contaCalculoItem.ConvenioId;
                configConvenioInput.EmpresaId = contaCalculoItem.EmpresaId;
                configConvenioInput.PlanoId = contaCalculoItem.PlanoId;

                configConvenioInput.GrupoId = input.FatContaItemDto.FaturamentoItem.GrupoId;
                configConvenioInput.SubGrupoId = input.FatContaItemDto.FaturamentoItem.SubGrupoId;
                configConvenioInput.ItemId = input.FatContaItemDto.FaturamentoItemId;


                var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);

                // por empresa
                var configsPorEmpresa = configsConvenio.Items
                    .Where(x => x.EmpresaId != null)
                    .Where(c => c.EmpresaId == contaCalculoItem.EmpresaId);

                // todas emprsas
                var configsTodasEmpresas = configsConvenio.Items
                    .Where(x => x.EmpresaId == null || x.EmpresaId == 0);

                // por plano
                var configsPorPlano = configsConvenio.Items
                    .Where(x => x.PlanoId != null)
                    .Where(c => c.PlanoId == contaCalculoItem.PlanoId);

                // Todos planos
                var configsTodosPlanos = configsConvenio.Items
                    .Where(x => x.PlanoId == null || x.PlanoId == 0);

                // por grupo
                var configsPorGrupo = configsConvenio.Items
                    .Where(x => x.GrupoId != null)
                    .Where(c => c.GrupoId == input.FatContaItemDto.FaturamentoItem.GrupoId);

                // todos grupos
                var configsTodosGrupos = configsConvenio.Items
                    .Where(x => x.GrupoId == null || x.GrupoId == 0);

                // por subGrupo
                var configsPorSubGrupo = configsConvenio.Items
                    .Where(x => x.GrupoId != null)
                    .Where(c => c.SubGrupoId == input.FatContaItemDto.FaturamentoItem.SubGrupoId);

                // todos subGrupos
                var configsTodosSubGrupos = configsConvenio.Items
                    .Where(x => x.SubGrupoId == null || x.SubGrupoId == 0);

                // por item
                var configsPorItem = configsConvenio.Items
                    .Where(x => x.ItemId != null)
                    .Where(c => c.ItemId == input.FatContaItemDto.FaturamentoItem.Id);

                // todos itens
                var configsTodosItens = configsConvenio.Items
                    .Where(x => x.ItemId == null || x.ItemId == 0);

                input.configsPorEmpresa = configsPorEmpresa.ToArray();
                input.configsTodasEmpresas = configsTodasEmpresas.ToArray();
                input.configsPorPlano = configsPorPlano.ToArray();
                input.configsTodosPlanos = configsTodosPlanos.ToArray();
                input.configsPorGrupo = configsPorGrupo.ToArray();
                input.configsTodosGrupos = configsTodosGrupos.ToArray();
                input.configsPorSubGrupo = configsPorSubGrupo.ToArray();
                input.configsTodosSubGrupos = configsTodosSubGrupos.ToArray();
                input.configsPorItem = configsPorItem.ToArray();
                input.configsTodosItens = configsTodosItens.ToArray();
                input.configsConvenio = configsConvenio.Items.ToList();

                return await CalcularValorUnitarioContaItem(input);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("ErroCalculoValorUnitario"), e);
            }
        }

        public async Task<float> CalcularValorUnitarioContaItem(CalculoContaItemInput input)
        {
            try
            {
                if (input.configsConvenio == null)
                {
                    ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
                    configConvenioInput.ConvenioId = input.conta.ConvenioId;
                    configConvenioInput.EmpresaId = input.conta.EmpresaId;
                    configConvenioInput.PlanoId = input.conta.PlanoId;

                    configConvenioInput.GrupoId = input.FatContaItemDto.FaturamentoItem.GrupoId;
                    configConvenioInput.SubGrupoId = input.FatContaItemDto.FaturamentoItem.SubGrupoId;
                    configConvenioInput.ItemId = input.FatContaItemDto.FaturamentoItemId;

                    var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                    input.configsConvenio = configsConvenio.Items.ToList();
                }

                long? tabelaId;

                // Achando tabela adequada via analise combinatoria das 'configConvenio'

                // 1 - caso mais especifico
                var caso1 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId != null && x.ItemId != 0)
                    );


                // 5
                var caso5 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId == null || x.ItemId == 0)

                          );

                // 9
                var caso9 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );


                // 13
                var caso13 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId == null || x.GrupoId == 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );


                // 14
                var caso14 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId == null || x.GrupoId == 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );





                // 2
                var caso2 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId != null && x.ItemId != 0)
                    );
                // 3
                var caso3 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId != null && x.ItemId != 0)
                    );
                // 4
                var caso4 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId != null && x.ItemId != 0)
                    );


                // 6
                var caso6 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );
                // 7
                var caso7 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );
                // 8
                var caso8 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId != null && x.SubGrupoId != 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );

                // 10
                var caso10 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId != null && x.EmpresaId != 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );
                // 11
                var caso11 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );
                // 12
                var caso12 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId != null && x.GrupoId != 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );


                // 15
                var caso15 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId != null && x.PlanoId != 0) &&
                         (x.GrupoId == null || x.GrupoId == 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );
                // 16 - caso mais generico
                var caso16 = input.configsConvenio.FirstOrDefault(
                    x => (x.EmpresaId == null || x.EmpresaId == 0) &&
                         (x.PlanoId == null || x.PlanoId == 0) &&
                         (x.GrupoId == null || x.GrupoId == 0) &&
                         (x.SubGrupoId == null || x.SubGrupoId == 0) &&
                         (x.ItemId == null || x.ItemId == 0)
                    );
                // Quanto menor o numero, maior a prioridade (ex: 1 eh mais prioritario que 20)
                CasoConfig[] casosConfig =
                {
                    new CasoConfig(1,  caso1),
                    new CasoConfig(2,  caso2),
                    new CasoConfig(3,  caso3),
                    new CasoConfig(4,  caso4),
                    new CasoConfig(5,  caso5),
                    new CasoConfig(6,  caso6),
                    new CasoConfig(7,  caso7),
                    new CasoConfig(8,  caso8),
                    new CasoConfig(9,  caso9),
                    new CasoConfig(10, caso10),
                    new CasoConfig(11, caso11),
                    new CasoConfig(12, caso12),
                    new CasoConfig(13, caso13),
                    new CasoConfig(14, caso14),
                    new CasoConfig(15, caso15),
                    new CasoConfig(16, caso16),
                };

                List<CasoConfig> casosConfigList = new List<CasoConfig>();

                foreach (var cc in casosConfig)
                {
                    if (cc.Config != null)
                        casosConfigList.Add(cc);
                }


                // Obtendo config com maior prioridade
                var config = casosConfigList.OrderByDescending(p => p.Prioridade).FirstOrDefault().Config;
                tabelaId = config?.TabelaId;

                input.FaturamentoConfigConvenioId = config?.Id;



                //var configAlternativoEmpresa = input.configsConvenio.Where(w => w.EmpresaId != null);

                //if (configAlternativoEmpresa.Count() > 0)
                //{
                //    var configAlternativoPlano = configAlternativoEmpresa.Where(w => w.PlanoId != null);

                //    if (configAlternativoPlano.Count() > 0)
                //    {

                //    }
                //}



                // fim - via analise combinatoria

                // ======================= PRECO =========================

                // Obter preco vigente
                var listarParaTabelaInput = new ListarFaturamentoItensTabelaInput();
                listarParaTabelaInput.TabelaId = tabelaId.ToString();
                var precosPorTabela = AsyncHelper.RunSync(() => _tabelaItemAppService.ListarParaFatTabela(listarParaTabelaInput)).Items;

                var itemCobranca = ObterItemASerCobrado(input.conta.ConvenioId, input.conta.PlanoId, input.FatContaItemDto.FaturamentoItemId ?? 0);


                // var precosPorFatItem = precosPorTabela.Where(_ => _.ItemId == input.FatContaItemDto.FaturamentoItemId);
                var precosPorFatItem = precosPorTabela.Where(_ => _.ItemId == itemCobranca);
                var preco = precosPorFatItem
                    .Where(_ => _.VigenciaDataInicio <= DateTime.Now)
                    .OrderByDescending(_ => _.VigenciaDataInicio).FirstOrDefault();

                if(itemCobranca != input.FatContaItemDto.FaturamentoItemId)
                {
                    input.FaturamentoItemCobradoId = itemCobranca;
                }

                // ======================= MOEDA E COTACAO =========================

                if (preco == null)
                    return 0f;

                var moeda = AsyncHelper.RunSync(() => _moedaAppService.Obter((long)(preco.SisMoedaId ?? 0)));
                ListarSisMoedaCotacoesInput cotacaoInput = new ListarSisMoedaCotacoesInput();
                cotacaoInput.Filtro = moeda.Id.ToString();

                // Buscar cotacoes por convenio
                var cotacoesPorConvenio = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
                    .Items
                    .Where(_ => _.ConvenioId == input.conta.ConvenioId)
                    ;

                // Cotacoes por Empresa
                var cotacoesPorEmpresa = cotacoesPorConvenio
                    .Where(_ => _.EmpresaId == input.conta.EmpresaId)
                    ;

                // A cotacao padrao inicial eh por Convenio, se houver configuracao especifica, sera sobrescrita no fluxo abaixo
                var cotacao = cotacoesPorConvenio
                            .Where(_ => _.DataInicio <= DateTime.Now /*&& _.DataFinal >= DateTime.Now*/)
                            .OrderByDescending(_ => _.DataInicio)
                            .FirstOrDefault();

                // Filtrar cotacoes por Plano
                var cotacoesPorPlano = cotacoesPorEmpresa //por que aqui nao eh por convenio?
                    .Where(x => x.PlanoId != null)
                    .Where(c => c.PlanoId == input.conta.PlanoId); // se planoId da conta for null (o que nao deveria existir) vai pegar todos com planoId null

                // Caso haja cotacao para um plano especifico
                if (cotacoesPorPlano.Count() > 0)
                {
                    // Por subGrupo especifico
                    var cotacaoPorSubGrupo = cotacoesPorPlano
                        .Where(_ => _.SubGrupoId == input.FatContaItemDto.FaturamentoItem.SubGrupoId && _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now)
                        //?
                        .OrderByDescending(_ => _.DataInicio)
                        .FirstOrDefault();

                    if (cotacaoPorSubGrupo != null)
                    {
                        cotacao = cotacaoPorSubGrupo;
                    }
                    else
                    {
                        // Por grupo especifico
                        var cotacaoPorGrupo = cotacoesPorPlano
                            .Where(_ => _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now && _.GrupoId == input.FatContaItemDto.FaturamentoItem.GrupoId)
                            .OrderByDescending(_ => _.DataInicio)
                            .FirstOrDefault();

                        if (cotacaoPorGrupo != null)
                        {
                            cotacao = cotacaoPorGrupo;
                        }
                    }
                }
                // Caso seja para todos os planos
                else
                {
                    // Por subGrupo
                    var cotacaoPorSubGrupo = cotacoesPorConvenio
                        .Where(_ => _.DataInicio <= DateTime.Now /*&& _.DataFinal >= DateTime.Now*/)
                        .OrderByDescending(_ => _.DataInicio)
                        .FirstOrDefault(_ => _.SubGrupoId == input.FatContaItemDto.FaturamentoItem.SubGrupoId);

                    if (cotacaoPorSubGrupo != null)
                    {
                        cotacao = cotacaoPorSubGrupo;
                    }
                    else
                    {
                        // Por grupo
                        var cotacaoPorGrupo = cotacoesPorConvenio
                            .Where(_ => _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now)
                            .OrderByDescending(_ => _.DataInicio)
                            .FirstOrDefault(_ => _.GrupoId == input.FatContaItemDto.FaturamentoItem.GrupoId);

                        if (cotacaoPorGrupo != null)
                        {
                            cotacao = cotacaoPorGrupo;
                        }
                    }

                }

                // Filme
                cotacaoInput.Filtro = "1";//AQUI DEVE SER A ID FIXA DA MOEDA 'FILME' - CRIAR SEED NO EF PARA MOEDAS 'FIXAS' DO SISTEMA
                var cotacaoFilme = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
                                        .Items
                                        .Where(_ => _.DataInicio <= DateTime.Now)
                                        .OrderByDescending(_ => _.DataInicio)
                                        .FirstOrDefault();

                var totalFilme = (cotacaoFilme?.Valor ?? 0) * input.FatContaItemDto.MetragemFilme;

                // Porte
                var tabela = await _tabelaAppService.Obter((long)tabelaId);

                if (tabela != null)
                {
                    input.TabelaUilizada = tabela.Descricao;
                }

                float totalPorte = 0f;
                tabela.IsCBHPM = true;
                if (tabela.IsCBHPM)
                {
                    cotacaoInput.Filtro = "5"; // Valor Id fixo setado pelo Seed para UCO
                    cotacaoInput.ConvenioId = input.conta.ConvenioId.ToString();
                    cotacaoInput.IsUco = true;
                    var cotacaoPorte = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
                                            .Items
                                            .Where(_ => _.DataInicio <= DateTime.Now)
                                            .OrderByDescending(_ => _.DataInicio)
                                            .FirstOrDefault();

                    if (!preco.COCH.HasValue)
                        preco.COCH = 0;

                    totalPorte = cotacaoPorte?.Valor ?? 0 * (long)preco.COCH;
                }

                // Valor unitario
                var valorUnitario = ((preco.Preco * (cotacao?.Valor ?? 1)) + totalFilme + totalPorte);
                return valorUnitario;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return 0f;
            }
        }

        public async Task<float> CalcularValorUnitarioItem(long empresaId, long convenioId, long planoId, FaturamentoContaItemDto fatContaItemDto)
        {
            CalculoContaItemInput calculoContaItemInput = new CalculoContaItemInput();
            calculoContaItemInput.conta = new ContaCalculoItem();
            calculoContaItemInput.conta.EmpresaId = empresaId;
            calculoContaItemInput.conta.ConvenioId = convenioId;
            calculoContaItemInput.conta.PlanoId = planoId;

            calculoContaItemInput.FatContaItemDto = fatContaItemDto;

            var result = await CalcularValorUnitarioContaItem(calculoContaItemInput);

            fatContaItemDto.TabelaUtilizada = calculoContaItemInput.TabelaUilizada;
            fatContaItemDto.FaturamentoConfigConvenioId = calculoContaItemInput.FaturamentoConfigConvenioId;

            return result;
        }

        public async Task<ValorCodigoTabela> CalcularValorItemFaturamento(long contaId, long faturamentoItemId)
        {
            var faturamentoItem = _fatItemRepository.GetAll()
                                                    .Where(w => w.Id == faturamentoItemId)
                                                    .FirstOrDefault();


            FaturamentoItemDto faturamentoItemDto = null;

            if (faturamentoItem != null)
            {
                faturamentoItemDto = FaturamentoItemDto.Mapear(faturamentoItem);
            }


            var conta = _faturamentoContaRepository.GetAll()
                                                   .Where(w => w.Id == contaId)
                                                   .FirstOrDefault();


            CalculoContaItemInput calculoContaItemInput = new CalculoContaItemInput();
            calculoContaItemInput.conta = new ContaCalculoItem();

            if (conta != null)
            {
                calculoContaItemInput.conta.EmpresaId = conta.EmpresaId ?? 0;
                calculoContaItemInput.conta.ConvenioId = conta.ConvenioId ?? 0;
                calculoContaItemInput.conta.PlanoId = conta.PlanoId ?? 0;
            }
            calculoContaItemInput.FatContaItemDto = new FaturamentoContaItemDto { FaturamentoItem = faturamentoItemDto, FaturamentoItemId = faturamentoItemId };

            var result = await CalcularValorUnitarioContaItem(calculoContaItemInput);

            //  fatContaItemDto.TabelaUtilizada = calculoContaItemInput.TabelaUilizada;

            ValorCodigoTabela valorCodigoTabela = new ValorCodigoTabela();

            valorCodigoTabela.Valor = result;
            valorCodigoTabela.TabelaId = calculoContaItemInput.FaturamentoConfigConvenioId;
            valorCodigoTabela.FaturamentoItemCobradoId = calculoContaItemInput.FaturamentoItemCobradoId;

            return valorCodigoTabela;
        }


        long? ObterItemASerCobrado(long convenioId, long planoId, long itemOriginalId)
        {

            var config = _itemConfigRepository.GetAll()
                                             .Where(w => (w.ConvenioId == convenioId || w.ConvenioId == null)
                                                      && (w.PlanoId == planoId || w.PlanoId == null)
                                                      && w.ItemId == itemOriginalId)
                                             .FirstOrDefault();
            if (config != null)
            {
                return config.ItemCobrarId;
            }

            return itemOriginalId;
        }

        public async Task<DefaultReturn<FaturamentoContaItemInsertDto>> InserirItensContaFaturamento(FaturamentoContaItemInsertDto itensConta)
        {
            var _retornoPadrao = new DefaultReturn<FaturamentoContaItemInsertDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();


            FaturamentoConta faturamentoConta = null;



            try
            {
                var atendimento = _atendimentoRepository.GetAll()
                                                      .Where(w => w.Id == itensConta.AtendimentoId)
                                                      .FirstOrDefault();

                if (itensConta.ContaId == null)
                {

                    var faturamentoContas = _faturamentoContaRepository.GetAll()
                                                                      .Where(w => w.AtendimentoId == itensConta.AtendimentoId)
                                                                      .ToList();

                     faturamentoConta = atendimento.IsAmbulatorioEmergencia ? faturamentoContas.FirstOrDefault() : faturamentoContas.LastOrDefault();
                }
                else
                {
                    faturamentoConta = _faturamentoContaRepository.GetAll()
                                                                  .Where(w => w.Id == itensConta.ContaId)
                                                                  .FirstOrDefault();
                }

                if (faturamentoConta != null)
                {
                    foreach (var itemFaturamento in itensConta.ItensFaturamento)
                    {
                        var faturamentoContaItem = new FaturamentoContaItem();

                        faturamentoContaItem.FaturamentoItemId = itemFaturamento.Id;
                        faturamentoContaItem.FaturamentoItem = _fatItemRepository.GetAll()
                                                                .Where(w => w.Id == itemFaturamento.Id)
                                                                .FirstOrDefault();

                        faturamentoContaItem.CentroCustoId = itensConta.CentroCustoId;
                        faturamentoContaItem.Data = (itensConta.Data == null || itensConta.Data == DateTime.MinValue) ? DateTime.Now : itensConta.Data;
                        faturamentoContaItem.FaturamentoContaId = faturamentoConta.Id;
                        faturamentoContaItem.MedicoId = itensConta.MedicoId;
                        faturamentoContaItem.Observacao = itensConta.Obs;
                        faturamentoContaItem.Qtde = itemFaturamento.Qtde;
                        // faturamentoContaItem.TipoLeitoId = laudoMovimento.TipoAcomodacaoId;
                        faturamentoContaItem.TurnoId = itensConta.TurnoId;
                        faturamentoContaItem.UnidadeOrganizacionalId = itensConta.UnidadeOrganizacionalId;
                        faturamentoContaItem.HoraIncio = itemFaturamento.HoraIncio;
                        faturamentoContaItem.HoraFim = itemFaturamento.HoraFim;
                        faturamentoContaItem.FaturamentoPacoteId = itemFaturamento.FaturamentoPacoteId;
                        faturamentoContaItem.FaturamentoContaKitId = itemFaturamento.FaturamentoContaKitId;

                        var contaCalculoItem = new ContaCalculoItem();

                        contaCalculoItem.EmpresaId = (long)atendimento.EmpresaId;
                        contaCalculoItem.ConvenioId = (long)atendimento.ConvenioId;
                        contaCalculoItem.PlanoId = (long)atendimento.PlanoId;

                        CalculoContaItemInput calculoContaItemInput = new CalculoContaItemInput();

                        calculoContaItemInput.conta = contaCalculoItem;
                        calculoContaItemInput.FatContaItemDto = FaturamentoContaItemDto.MapearFromCore(faturamentoContaItem);

                        faturamentoContaItem.ValorItem = AsyncHelper.RunSync(() => CalcularValorUnitarioContaItem(calculoContaItemInput));
                        faturamentoContaItem.FaturamentoConfigConvenioId = calculoContaItemInput.FaturamentoConfigConvenioId;
                        faturamentoContaItem.FaturamentoItem = null;
                        faturamentoContaItem.FaturamentoConta = null;
                        AsyncHelper.RunSync(() => _contaItemRepository.InsertAsync(faturamentoContaItem));

                    }
                }

            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }



        public void ExcluirPacote(long contaItemId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var contaItem = _contaItemRepository.GetAll()
                                                .Where(w => w.Id == contaItemId)
                                                .FirstOrDefault();


                contaItem.FaturamentoPacoteId = null;


                unitOfWork.Complete();
                unitOfWork.Dispose();
            }
            
        }



    }
    public class FaturamentoContaItemViewModel
    {
        public float ValorUnitario { get; set; }
        public float ValorTotal { get; set; }
        public long Id { get; set; }
        public string Descricao { get; set; }
        public long? FaturamentoItemId { get; set; }
        public string FaturamentoItemDescricao { get; set; }
        public long? FaturamentoContaId { get; set; }
        public FaturamentoItemDto FatItem { get; set; }
        public DateTime? Data { get; set; }
        public float Qtde { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public string UnidadeOrganizacionalDescricao { get; set; }
        public long? TerceirizadoId { get; set; }
        public string TerceirizadoDescricao { get; set; }
        public long? CentroCustoId { get; set; }
        public string CentroCustoDescricao { get; set; }
        public long? TurnoId { get; set; }
        public string TurnoDescricao { get; set; }
        public long? TipoLeitoId { get; set; }
        public string TipoLeitoDescricao { get; set; }
        public float ValorTemp { get; set; }
        public long? MedicoId { get; set; }
        public string MedicoNome { get; set; }
        public bool IsMedCredenciado { get; set; }
        public long? MedicoEspecialidadeId { get; set; }
        public string MedicoEspecialidadeNome { get; set; }
        public long? Auxiliar1Id { get; set; }
        public string Auxiliar1Nome { get; set; }
        public bool IsAux1Credenciado { get; set; }
        public long? Auxiliar1EspecialidadeId { get; set; }
        public string Auxiliar1EspecialidadeNome { get; set; }
        public long? Auxiliar2Id { get; set; }
        public string Auxiliar2Nome { get; set; }
        public bool IsAux2Credenciado { get; set; }
        public long? Auxiliar2EspecialidadeId { get; set; }
        public string Auxiliar2EspecialidadeNome { get; set; }
        public long? Auxiliar3Id { get; set; }
        public string Auxiliar3Nome { get; set; }
        public bool IsAux3Credenciado { get; set; }
        public long? Auxiliar3EspecialidadeId { get; set; }
        public string Auxiliar3EspecialidadeNome { get; set; }
        public long? AnestesistaId { get; set; }
        public string AnestesistaNome { get; set; }
        public bool IsAnestCredenciado { get; set; }
        public long? AnestesistaEspecialidadeId { get; set; }
        public string AnestesistaEspecialidadeNome { get; set; }
        public long? InstrumentadorId { get; set; }
        public string InstrumentadorNome { get; set; }
        public bool IsInstCredenciado { get; set; }
        public long? InstrumentadorEspecialidadeId { get; set; }
        public string InstrumentadorEspecialidadeNome { get; set; }
        public bool IsGlosaMedico { get; set; }
        public long? FaturamentoContaKitId { get; set; }
        public bool IsCirurgia { get; set; }
        public float ValorAprovado { get; set; }
        public float ValorTaxas { get; set; }
        public bool IsValorItemManual { get; set; }
        public float ValorItem { get; set; }
        public string HMCH { get; set; }
        public float ValorFilme { get; set; }
        public float ValorFilmeAprovado { get; set; }
        public float ValorCOCH { get; set; }
        public float ValorCOCHAprovado { get; set; }
        public float Percentual { get; set; }
        public bool IsInstrCredenciado { get; set; }
        public float ValorTotalRecuperado { get; set; }
        public float ValorTotalRecebido { get; set; }
        public float MetragemFilme { get; set; }
        public float MetragemFilmeAprovada { get; set; }
        public float COCH { get; set; }
        public float COCHAprovado { get; set; }
        public string StatusEntrega { get; set; }
        public bool IsRecuperaMedico { get; set; }
        public bool IsRecebeAuxiliar1 { get; set; }
        public bool IsGlosaAuxiliar1 { get; set; }
        public bool IsRecuperaAuxiliar1 { get; set; }
        public bool IsRecebeAuxiliar2 { get; set; }
        public bool IsGlosaAuxiliar2 { get; set; }
        public bool IsRecuperaAuxiliar2 { get; set; }
        public bool IsRecebeAuxiliar3 { get; set; }
        public bool IsGlosaAuxiliar3 { get; set; }
        public bool IsRecuperaAuxiliar3 { get; set; }
        public bool IsRecebeInstrumentador { get; set; }
        public bool IsGlosaInstrumentador { get; set; }
        public bool IsRecuperaInstrumentador { get; set; }
        public string Observacao { get; set; }
        public int QtdeRecuperada { get; set; }
        public int QtdeAprovada { get; set; }
        public int QtdeRecebida { get; set; }
        public float ValorMoedaAprovado { get; set; }
        public long? SisMoedaId { get; set; }
        public string SisMoedaNome { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public string SenhaAutorizacao { get; set; }
        public string NomeAutorizacao { get; set; }
        public string ObsAutorizacao { get; set; }
        public DateTime? HoraIncio { get; set; }
        public DateTime? HoraFim { get; set; }
        public string ViaAcesso { get; set; }
        public string Tecnica { get; set; }
        public string ClinicaId { get; set; }
        public long? FornecedorId { get; set; }
        public string FornecedorNome { get; set; }
        public string NumeroNF { get; set; }
        public bool IsImportaEstoque { get; set; }
        public string Tipo { get; set; }
        public string Grupo { get; set; }
        public long? FaturamentoConfigConvenioId { get; set; }
        public string Pacote { get; set; }
        public long? FaturamentoPacoteId { get; set; }
        public FaturamentoPacoteDto FaturamentoPacoteDto { get; set; }
        public bool IsPacote { get; set; }

        public long? FaturamentoItemCobradoId { get; set; }
        public FaturamentoItemDto FaturamentoItemCobrado { get; set; }

    }

    public class FaturamentoContaItemReportModel
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public long? FaturamentoItemId { get; set; }
        public string FaturamentoItemCodigo { get; set; }
        public string FaturamentoItemDescricao { get; set; }
        public long? FaturamentoContaId { get; set; }
        public DateTime? Data { get; set; }
        public float Qtde { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public string UnidadeOrganizacionalDescricao { get; set; }
        public long? TerceirizadoId { get; set; }
        public string TerceirizadoDescricao { get; set; }
        public long? CentroCustoId { get; set; }
        public string CentroCustoDescricao { get; set; }
        public long? TurnoId { get; set; }
        public string TurnoDescricao { get; set; }
        public long? TipoLeitoId { get; set; }
        public string TipoLeitoDescricao { get; set; }
        public float ValorTemp { get; set; }
        public long? MedicoId { get; set; }
        public string MedicoNome { get; set; }
        public bool IsMedCrendenciado { get; set; }
        public bool IsGlosaMedico { get; set; }
        public long? MedicoEspecialidadeId { get; set; }
        public string MedicoEspecialidadeNome { get; set; }
        public long? FaturamentoContaKitId { get; set; }
        public bool IsCirurgia { get; set; }
        public float ValorAprovado { get; set; }
        public float ValorTaxas { get; set; }
        public bool IsValorItemManual { get; set; }
        public float ValorItem { get; set; }
        public string HMCH { get; set; }
        public float ValorFilme { get; set; }
        public float ValorFilmeAprovado { get; set; }
        public float ValorCOCH { get; set; }
        public float ValorCOCHAprovado { get; set; }
        public float Percentual { get; set; }
        public bool IsInstrCredenciado { get; set; }
        public float ValorTotalRecuperado { get; set; }
        public float ValorTotalRecebido { get; set; }
        public float MetragemFilme { get; set; }
        public float MetragemFilmeAprovada { get; set; }
        public float COCH { get; set; }
        public float COCHAprovado { get; set; }
        public string StatusEntrega { get; set; }
        public bool IsRecuperaMedico { get; set; }
        public bool IsAux1Credenciado { get; set; }
        public bool IsRecebeAuxiliar1 { get; set; }
        public bool IsGlosaAuxiliar1 { get; set; }
        public bool IsRecuperaAuxiliar1 { get; set; }
        public bool IsAux2Credenciado { get; set; }
        public bool IsRecebeAuxiliar2 { get; set; }
        public bool IsGlosaAuxiliar2 { get; set; }
        public bool IsRecuperaAuxiliar2 { get; set; }
        public bool IsAux3Credenciado { get; set; }
        public bool IsRecebeAuxiliar3 { get; set; }
        public bool IsGlosaAuxiliar3 { get; set; }
        public bool IsRecuperaAuxiliar3 { get; set; }
        public bool IsRecebeInstrumentador { get; set; }
        public bool IsGlosaInstrumentador { get; set; }
        public bool IsRecuperaInstrumentador { get; set; }
        public string Observacao { get; set; }
        public int QtdeRecuperada { get; set; }
        public int QtdeAprovada { get; set; }
        public int QtdeRecebida { get; set; }
        public float ValorMoedaAprovado { get; set; }
        public long? SisMoedaId { get; set; }
        public string SisMoedaNome { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public string SenhaAutorizacao { get; set; }
        public string NomeAutorizacao { get; set; }
        public string ObsAutorizacao { get; set; }
        public DateTime? HoraIncio { get; set; }
        public DateTime? HoraFim { get; set; }
        public string ViaAcesso { get; set; }
        public string Tecnica { get; set; }
        public string ClinicaId { get; set; }
        public long? FornecedorId { get; set; }
        public string FornecedorNome { get; set; }
        public string NumeroNF { get; set; }
        public bool IsImportaEstoque { get; set; }
        public string Tipo { get; set; }
        public string Grupo { get; set; }

    }
}
