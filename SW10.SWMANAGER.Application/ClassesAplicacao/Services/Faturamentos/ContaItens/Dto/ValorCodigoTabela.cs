﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto
{
    public class ValorCodigoTabela
    {
        public float Valor { get; set; }
        public long? TabelaId { get; set; }
        public long? FaturamentoItemCobradoId { get; set; }
    }
}
