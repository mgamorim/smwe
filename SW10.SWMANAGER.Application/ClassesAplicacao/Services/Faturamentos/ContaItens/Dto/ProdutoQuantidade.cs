﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto
{
    public class ProdutoQuantidade
    {
        public long ProdutoId { get; set; }
        public float Quantidade { get; set; }
    }
}
