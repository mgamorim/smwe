﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasApresentacoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasApresentacoes.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasApresentacoes;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasApresentacoes
{
    public class FaturamentoBrasApresentacaoAppService : SWMANAGERAppServiceBase, IFaturamentoBrasApresentacaoAppService
    {
        #region Dependencias
        private readonly IRepository<FaturamentoBrasApresentacao, long> _brasApresentacaoRepository;
        private readonly IListarBrasApresentacoesExcelExporter _listarBrasApresentacoesExcelExporter;

        public FaturamentoBrasApresentacaoAppService(
            IRepository<FaturamentoBrasApresentacao, long> brasApresentacaoRepository, 
            IListarBrasApresentacoesExcelExporter listarBrasApresentacoesExcelExporter
            )
        {
            _brasApresentacaoRepository = brasApresentacaoRepository;
            _listarBrasApresentacoesExcelExporter = listarBrasApresentacoesExcelExporter;
        }
        #endregion dependencias.

        public async Task<PagedResultDto<FaturamentoBrasApresentacaoDto>> Listar(ListarFaturamentoBrasApresentacoesInput input)
        {
            var brasApresentacaorBrasApresentacoes = 0;
            List<FaturamentoBrasApresentacao> brasApresentacoes;
            List<FaturamentoBrasApresentacaoDto> brasApresentacoesDtos = new List<FaturamentoBrasApresentacaoDto>();
            try
            {
                var query = _brasApresentacaoRepository
                    .GetAll();

                brasApresentacaorBrasApresentacoes = await query.CountAsync();

                brasApresentacoes = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                brasApresentacoesDtos = brasApresentacoes.MapTo<List<FaturamentoBrasApresentacaoDto>>();

                return new PagedResultDto<FaturamentoBrasApresentacaoDto>(
                       brasApresentacaorBrasApresentacoes,
                       brasApresentacoesDtos
                   );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        
        public async Task CriarOuEditar(FaturamentoBrasApresentacaoDto input)
        {
            try
            {
                var BrasApresentacao = input.MapTo<FaturamentoBrasApresentacao>();

                if (input.Id.Equals(0))
                {
                    await _brasApresentacaoRepository.InsertAsync(BrasApresentacao);
                }
                else
                {
                    await _brasApresentacaoRepository.UpdateAsync(BrasApresentacao);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(FaturamentoBrasApresentacaoDto input)
        {
            try
            {
                await _brasApresentacaoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoBrasApresentacaoDto> Obter(long id)
        {
            try
            {
                var query = await _brasApresentacaoRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var brasApresentacao = query.MapTo<FaturamentoBrasApresentacaoDto>();

                return brasApresentacao;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FaturamentoBrasApresentacaoDto> ObterComEstado(string nome, long estadoId)
        {
            try
            {
                var query = _brasApresentacaoRepository
                    .GetAll();

                var result = await query.FirstOrDefaultAsync();

                var brasApresentacao = result.MapTo<FaturamentoBrasApresentacaoDto>();

                return brasApresentacao;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarFaturamentoBrasApresentacoesInput input)
        {
            try
            {
                var result = await Listar(input);
                var brasApresentacoes = result.Items;
                return _listarBrasApresentacoesExcelExporter.ExportToFile(brasApresentacoes.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoBrasApresentacaoDto> faturamentoItensDto = new List<FaturamentoBrasApresentacaoDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _brasApresentacaoRepository.GetAll()
                            .WhereIf(!dropdownInput.filtro.IsNullOrEmpty(), m => m.Id.ToString() == dropdownInput.filtro)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(),
                                m => m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();
                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
