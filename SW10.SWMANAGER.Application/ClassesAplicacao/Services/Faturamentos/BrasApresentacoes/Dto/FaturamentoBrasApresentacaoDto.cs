﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasApresentacoes;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasApresentacoes.Dto
{
    [AutoMap(typeof(FaturamentoBrasApresentacao))]
    public class FaturamentoBrasApresentacaoDto : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }
        
        public string Descricao { get; set; }

        public float Quantidade { get; set; }
    }
}
