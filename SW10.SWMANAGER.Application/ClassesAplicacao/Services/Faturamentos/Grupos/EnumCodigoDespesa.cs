﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos
{
    public enum EnumCodigoDespesa
    {
        GasesMedicinais=1,
        Medicamentos=2,
        Materiais=3,
        Diarias=4,
        TaxasAluguéis=5,
        OPME=6
    }
}
