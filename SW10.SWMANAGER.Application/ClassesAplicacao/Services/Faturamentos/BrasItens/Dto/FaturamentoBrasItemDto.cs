﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasItens;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasItens.Dto
{
    [AutoMap(typeof(FaturamentoBrasItem))]
    public class FaturamentoBrasItemDto : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }
    }
}
