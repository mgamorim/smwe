﻿#region Usings
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasItens.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasItens;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
#endregion usings.

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasItens
{
    public class FaturamentoBrasItemAppService : SWMANAGERAppServiceBase, IFaturamentoBrasItemAppService
    {
        #region Cabecalho
        private readonly IRepository<FaturamentoBrasItem, long> _brasItemRepository;
        private readonly IListarBrasItensExcelExporter _listarBrasItensExcelExporter;

        public FaturamentoBrasItemAppService (
            IRepository<FaturamentoBrasItem, long> brasItemRepository,
            IListarBrasItensExcelExporter listarBrasItensExcelExporter)
        {
            _brasItemRepository = brasItemRepository;
            _listarBrasItensExcelExporter = listarBrasItensExcelExporter;
        }
        #endregion cabecalho.

        public async Task<PagedResultDto<FaturamentoBrasItemDto>> Listar (ListarFaturamentoBrasItensInput input)
        {
            var brasItemrBrasItens = 0;
            List<FaturamentoBrasItem> brasItens;
            List<FaturamentoBrasItemDto> brasItensDtos = new List<FaturamentoBrasItemDto>();
            try
            {
                var query = _brasItemRepository
                    .GetAll()
                    ;

                brasItemrBrasItens = await query
                    .CountAsync();

                brasItens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                brasItensDtos = brasItens
                    .MapTo<List<FaturamentoBrasItemDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<FaturamentoBrasItemDto>(
                brasItemrBrasItens,
                brasItensDtos
                );
        }

        public async Task CriarOuEditar (FaturamentoBrasItemDto input)
        {
            try
            {
                var BrasItem = input.MapTo<FaturamentoBrasItem>();
                if (input.Id.Equals(0))
                {
                    await _brasItemRepository.InsertAsync(BrasItem);
                }
                else
                {
                    await _brasItemRepository.UpdateAsync(BrasItem);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir (FaturamentoBrasItemDto input)
        {
            try
            {
                await _brasItemRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoBrasItemDto> Obter (long id)
        {
            try
            {
                var query = await _brasItemRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var brasItem = query
                    .MapTo<FaturamentoBrasItemDto>();

                return brasItem;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FaturamentoBrasItemDto> ObterComEstado (string nome, long estadoId)
        {
            try
            {
                var query = _brasItemRepository
                    .GetAll()
                    ;

                var result = await query.FirstOrDefaultAsync();

                var brasItem = result
                    .MapTo<FaturamentoBrasItemDto>();

                return brasItem;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel (ListarFaturamentoBrasItensInput input)
        {
            try
            {
                var result = await Listar(input);
                var brasItens = result.Items;
                return _listarBrasItensExcelExporter.ExportToFile(brasItens.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoBrasItemDto> faturamentoItensDto = new List<FaturamentoBrasItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _brasItemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
