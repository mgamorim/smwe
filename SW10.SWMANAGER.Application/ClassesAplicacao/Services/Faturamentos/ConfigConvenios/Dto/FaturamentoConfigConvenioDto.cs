﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.SubGrupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Tabelas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios.Dto
{
    [AutoMap(typeof(FaturamentoConfigConvenio))]
    public class FaturamentoConfigConvenioDto : CamposPadraoCRUDDto
    {
        public long? EmpresaId { get; set; }
        public EmpresaDto Empresa { get; set; }
        
        public long? ConvenioId { get; set; }
        public ConvenioDto Convenio { get; set; }
        
        public long? PlanoId { get; set; }
        public PlanoDto Plano { get; set; }
        
        public long? GrupoId { get; set; }
        public FaturamentoGrupoDto Grupo { get; set; }
        
        public long? SubGrupoId { get; set; }
        public FaturamentoSubGrupoDto SubGrupo { get; set; }
        
        public long? TabelaId { get; set; }
        public FaturamentoTabelaDto Tabela { get; set; }
        
        public long? ItemId { get; set; }
        public FaturamentoItemDto Item { get; set; }
        
        public DateTime? DataIncio { get; set; }
        
        public DateTime? DataFim { get; set; }



        public static FaturamentoConfigConvenioDto Mapear(FaturamentoConfigConvenio faturamentoConfigConvenio)
        {
            FaturamentoConfigConvenioDto faturamentoConfigConvenioDto = new FaturamentoConfigConvenioDto();

            faturamentoConfigConvenioDto.Id = faturamentoConfigConvenio.Id;
            faturamentoConfigConvenioDto.Codigo = faturamentoConfigConvenio.Codigo;
            faturamentoConfigConvenioDto.Descricao = faturamentoConfigConvenio.Descricao;


            faturamentoConfigConvenioDto.EmpresaId = faturamentoConfigConvenio.EmpresaId;
            faturamentoConfigConvenioDto.ConvenioId = faturamentoConfigConvenio.ConvenioId;
            faturamentoConfigConvenioDto.PlanoId = faturamentoConfigConvenio.PlanoId;
            faturamentoConfigConvenioDto.GrupoId = faturamentoConfigConvenio.GrupoId;
            faturamentoConfigConvenioDto.SubGrupoId = faturamentoConfigConvenio.SubGrupoId;
            faturamentoConfigConvenioDto.TabelaId = faturamentoConfigConvenio.TabelaId;
            faturamentoConfigConvenioDto.ItemId = faturamentoConfigConvenio.ItemId;
            faturamentoConfigConvenioDto.DataIncio = faturamentoConfigConvenio.DataIncio;
            faturamentoConfigConvenioDto.DataFim = faturamentoConfigConvenio.DataFim;

            return faturamentoConfigConvenioDto;

        }

        public static FaturamentoConfigConvenio Mapear(FaturamentoConfigConvenioDto faturamentoConfigConvenioDto)
        {
            FaturamentoConfigConvenio faturamentoConfigConvenio = new FaturamentoConfigConvenio();

            faturamentoConfigConvenio.EmpresaId = faturamentoConfigConvenioDto.EmpresaId;
            faturamentoConfigConvenio.ConvenioId = faturamentoConfigConvenioDto.ConvenioId;
            faturamentoConfigConvenio.PlanoId = faturamentoConfigConvenioDto.PlanoId;
            faturamentoConfigConvenio.GrupoId = faturamentoConfigConvenioDto.GrupoId;
            faturamentoConfigConvenio.SubGrupoId = faturamentoConfigConvenioDto.SubGrupoId;
            faturamentoConfigConvenio.TabelaId = faturamentoConfigConvenioDto.TabelaId;
            faturamentoConfigConvenio.ItemId = faturamentoConfigConvenioDto.ItemId;
            faturamentoConfigConvenio.DataIncio = faturamentoConfigConvenioDto.DataIncio;
            faturamentoConfigConvenio.DataFim = faturamentoConfigConvenioDto.DataFim;

            return faturamentoConfigConvenio;

        }



    }
}
