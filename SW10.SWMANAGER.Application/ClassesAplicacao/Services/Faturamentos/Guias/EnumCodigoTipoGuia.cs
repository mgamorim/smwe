﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Guias
{
    public enum EnumCodigoTipoGuia
    {
        Consulta = 1,
        SPSADT = 2,
        ResumoInternacao = 3,
        HonorarioIndividual = 4,
        Particular = 5
    }
}
