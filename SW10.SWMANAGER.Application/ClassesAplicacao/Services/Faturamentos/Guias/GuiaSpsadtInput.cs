﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using SW10.SWMANAGER.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Guias
{
    public class GuiaSpsadtInput
    {
        public long? ContaId { get; set; }
        public long? AtendimentoId { get; set; }
    }
}
