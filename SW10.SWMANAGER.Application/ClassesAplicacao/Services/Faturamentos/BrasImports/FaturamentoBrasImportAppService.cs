﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasImports.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasImports.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasImports;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasImports
{
    public class FaturamentoBrasImportAppService : SWMANAGERAppServiceBase, IFaturamentoBrasImportAppService
    {
        #region Dependencias
        private readonly IRepository<FaturamentoBrasImport, long> _brasImportRepository;
        private readonly IListarBrasImportsExcelExporter _listarBrasImportsExcelExporter;

        public FaturamentoBrasImportAppService(
            IRepository<FaturamentoBrasImport, long> brasImportRepository,
            IListarBrasImportsExcelExporter listarBrasImportsExcelExporter
            )
        {
            _brasImportRepository = brasImportRepository;
            _listarBrasImportsExcelExporter = listarBrasImportsExcelExporter;
        }
        #endregion dependencias.

        public async Task<PagedResultDto<FaturamentoBrasImportDto>> Listar(ListarBrasImportsInput input)
        {
            var brasImportrBrasImports = 0;
            List<FaturamentoBrasImport> brasImports;
            List<FaturamentoBrasImportDto> brasImportsDtos = new List<FaturamentoBrasImportDto>();
            try
            {
                var query = _brasImportRepository
                    .GetAll();

                brasImportrBrasImports = await query.CountAsync();

                brasImports = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                brasImportsDtos = brasImports.MapTo<List<FaturamentoBrasImportDto>>();

                return new PagedResultDto<FaturamentoBrasImportDto>(
                   brasImportrBrasImports,
                   brasImportsDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        
        public async Task CriarOuEditar(FaturamentoBrasImportDto input)
        {
            try
            {
                var BrasImport = input.MapTo<FaturamentoBrasImport>();

                if (input.Id.Equals(0))
                {
                    await _brasImportRepository.InsertAsync(BrasImport);
                }
                else
                {
                    await _brasImportRepository.UpdateAsync(BrasImport);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(FaturamentoBrasImportDto input)
        {
            try
            {
                await _brasImportRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoBrasImportDto> Obter(long id)
        {
            try
            {
                var query = await _brasImportRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var brasImport = query.MapTo<FaturamentoBrasImportDto>();

                return brasImport;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FaturamentoBrasImportDto> ObterComEstado(string nome, long estadoId)
        {
            try
            {
                var query = _brasImportRepository
                    .GetAll();

                var result = await query.FirstOrDefaultAsync();

                var brasImport = result.MapTo<FaturamentoBrasImportDto>();

                return brasImport;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarBrasImportsInput input)
        {
            try
            {
                var result = await Listar(input);
                var brasImports = result.Items;
                return _listarBrasImportsExcelExporter.ExportToFile(brasImports.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }
    }
}
