﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Relatorios.Models
{
    public class GuiaResumoInternacaoModel
    {
        public string NomePaciente { get; set; }
        public string Matricula { get; set; }
        public string RegistroANS { get; set; }
        public string ValidadeCarteira { get; set; }
        public string Senha { get; set; }
        public string DataAutorizacao { get; set; }
        public string CodCNES { get; set; }
        public string NomeContratado { get; set; }
        public string ValidadeSenha { get; set; }
        public string NumeroGuia { get; set; }
        public string Cid1 { get; set; }
        public string Cid2 { get; set; }
        public string Cid3 { get; set; }
        public string Cid4 { get; set; }
        public string CodOperadora { get; set; }
        public string CaraterAtendimento { get; set; }
        public string TipoFaturamento { get; set; }
        public string DataIniFaturamento { get; set; }
        public string DataFimFaturamento { get; set; }
        public string HoraIniFaturamento { get; set; }
        public string HoraFimFaturamento { get; set; }
        public string TipoInternacao { get; set; }
        public string RegimeInternacao { get; set; }
        public string TotalProcedimentos { get; set; }
        public string TotalDiaria { get; set; }
        public string TotalTaxasAlugueis { get; set; }
        public string TotalMateriais { get; set; }
        public string TotalOpme { get; set; }
        public string TotalMedicamentos { get; set; }
        public string TotalGasesMedicinais { get; set; }
        public string TotalGeral { get; set; }
        public bool RN { get; set; }

        public List<string> Lista { get; set; }

        public GuiaResumoInternacaoModel()
        {
            Lista = new List<string>();
        }

        public static GuiaResumoInternacaoModel MapearFromAtendimento(AtendimentoDto atendimento)
        {
            var model = new GuiaResumoInternacaoModel();

            model.NomePaciente = atendimento.Paciente?.NomeCompleto;
            model.RegistroANS = atendimento.Convenio?.RegistroANS;
            model.Matricula = atendimento.Matricula;
            model.Senha = atendimento.Senha;
            model.DataAutorizacao = ((DateTime)atendimento.DataAutorizacao).ToString("dd/MM/yyyy");
            model.CodCNES = atendimento.Empresa?.Cnes.ToString();
            model.NomeContratado = atendimento.Empresa?.NomeFantasia;
            model.ValidadeSenha = ((DateTime)atendimento.ValidadeSenha).ToString("dd/MM/yyyy");
            model.NumeroGuia = atendimento.GuiaNumero;
            model.Cid1 = "";
            model.Cid2 = "";
            model.Cid3 = "";
            model.Cid4 = "";
            model.CodOperadora = "";
            model.CaraterAtendimento = "";
            model.TipoFaturamento = "";
            model.DataIniFaturamento = "";
            model.DataFimFaturamento = "";
            model.HoraIniFaturamento = "";
            model.HoraFimFaturamento = "";
            model.TipoInternacao = "";
            model.RegimeInternacao = "";
            model.TotalProcedimentos = "";
            model.TotalDiaria = "";
            model.TotalTaxasAlugueis = "";
            model.TotalMateriais = "";
            model.TotalOpme = "";
            model.TotalMedicamentos = "";
            model.TotalGasesMedicinais = "";
            model.TotalGeral = "";
            
            if (atendimento.ValidadeCarteira != null)
            {
                model.ValidadeCarteira = ((DateTime)atendimento.ValidadeCarteira).ToString("dd/MM/yyyy");
            }

            if (atendimento.Paciente.Nascimento.HasValue)
            {
                var idade = DateDifference.GetExtendedDifference((DateTime)atendimento.Paciente.Nascimento);
                model.RN = (idade.Ano == 0 && idade.Mes == 0 && idade.Dia <= 30);
            }

            return model;
        }
    }
}
