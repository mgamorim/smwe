﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens
{
    public interface IFaturamentoItemAppService : IApplicationService
    {
        Task<PagedResultDto<FaturamentoItemDto>> Listar(ListarFaturamentoItensInput input);

        Task CriarOuEditar(FaturamentoItemDto input);

        Task Excluir(FaturamentoItemDto input);

        Task<FaturamentoItemDto> Obter(long id);

        Task<FaturamentoItemDto> ObterPorCodigo (string codigo);

        Task<long> ObterTipoGrupoId (long? fatItemId);

        Task<FileDto> ListarParaExcel(ListarFaturamentoItensInput input);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownListDeluxe> ListarDropdownDeluxe (DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownCodigo (DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownExame(DropdownInput dropdownInput);

        Task<ListResultDto<FaturamentoItemDto>> ListarTodos();

        Task<ResultDropdownList> ListarFatItemDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarExameLaboratorialDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarExameImagemDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarExameDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDiagnosticoImagemDropdown (DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarFaturamentoItemPorGrupoSubGrupoDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownTodos(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownSemPacote(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownPacote(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarNaoLaudoNaoLaboratorioDropdown(DropdownInput dropdownInput);
    }
}

