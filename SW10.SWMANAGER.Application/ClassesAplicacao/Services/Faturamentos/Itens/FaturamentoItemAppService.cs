using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens
{
    public class FaturamentoItemAppService : SWMANAGERAppServiceBase, IFaturamentoItemAppService
    {
        #region Dependencias
        private readonly IRepository<FaturamentoItem, long> _itemRepository;
        private readonly IListarItensExcelExporter _listarItensExcelExporter;
        private readonly IFaturamentoGrupoAppService _faturamentoGrupoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FaturamentoItemAppService(
            IRepository<FaturamentoItem, long> itemRepository,
            IListarItensExcelExporter listarItensExcelExporter,
            IFaturamentoGrupoAppService faturamentoGrupoAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _itemRepository = itemRepository;
            _listarItensExcelExporter = listarItensExcelExporter;
            _faturamentoGrupoAppService = faturamentoGrupoAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }
        #endregion dependencias.

        public async Task<PagedResultDto<FaturamentoItemDto>> Listar(ListarFaturamentoItensInput input)
        {
            var itemrItens = 0;
            List<FaturamentoItem> itens;
            List<FaturamentoItemDto> itensDtos = new List<FaturamentoItemDto>();

            try
            {
                var query = _itemRepository
                    .GetAll()
                    .Include(i => i.Grupo)
                    .Include(i => i.SubGrupo)
                    .Include(i => i.Grupo.TipoGrupo)
                    .WhereIf(input.GrupoId != 0, m => input.GrupoId == m.Grupo.Id)
                    .WhereIf(input.SubGrupoId != 0, m => input.SubGrupoId == m.SubGrupo.Id)
                    .WhereIf(input.TipoId != 0, x => x.Grupo.TipoGrupoId == input.TipoId)

                    .WhereIf(!input.Filtro.IsNullOrEmpty(),
                        m => m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        || m.Grupo.Descricao.Contains(input.Filtro.ToUpper())
                        || m.SubGrupo.Descricao.Contains(input.Filtro.ToUpper())
                        || m.Grupo.TipoGrupo.Descricao.Contains(input.Filtro.ToUpper())
                        || m.Grupo.Descricao.Contains(input.Grupo.ToUpper())
                        || m.SubGrupo.Descricao.Contains(input.SubGrupo.ToUpper())
                        || m.Grupo.TipoGrupo.Descricao.Contains(input.Tipo.ToUpper())
                        )
                    ;

                itemrItens = await query.CountAsync();

                itens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                itensDtos = FaturamentoItemDto.Mapear(itens).ToList();

                return new PagedResultDto<FaturamentoItemDto>(
                    itemrItens,
                    itensDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task CriarOuEditar(FaturamentoItemDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var Item = FaturamentoItemDto.Mapear(input);

                    if (Item.LaudoGrupoId == 0)
                        Item.LaudoGrupoId = null;

                    if (input.Id.Equals(0))
                    {
                        await _itemRepository.InsertAsync(Item);
                    }
                    else
                    {
                        var itemEntity = _itemRepository.GetAll()
                                                        .Where(w => w.Id == input.Id)
                                                        .FirstOrDefault();

                        if (itemEntity != null)
                        {
                            itemEntity.Codigo = input.Codigo;
                            itemEntity.Descricao = input.Descricao;
                            itemEntity.BrasItemId = input.BrasItemId;
                            itemEntity.GrupoId = input.GrupoId != 0 ? input.GrupoId : null;
                            itemEntity.SubGrupoId = input.SubGrupoId != 0 ? input.SubGrupoId : null;
                            itemEntity.LaudoGrupoId = input.LaudoGrupoId != 0 ? input.LaudoGrupoId : null;
                            itemEntity.DescricaoTuss = input.DescricaoTuss;
                            itemEntity.Observacao = input.Observacao;
                            itemEntity.CodAmb = input.CodAmb;
                            itemEntity.CodTuss = input.CodTuss;
                            itemEntity.CodCbhpm = input.CodCbhpm;
                            itemEntity.DivideBrasindice = input.DivideBrasindice;
                            itemEntity.Referencia = input.Referencia;
                            itemEntity.ReferenciaSihSus = input.ReferenciaSihSus;
                            itemEntity.Sexo = input.Sexo;
                            itemEntity.QtdLaudo = input.QtdLaudo;
                            itemEntity.TipoLaudo = input.TipoLaudo;
                            itemEntity.DuracaoMinima = input.DuracaoMinima;
                            itemEntity.IsAtivo = input.IsAtivo;
                            itemEntity.IsObrigaMedico = input.IsObrigaMedico;
                            itemEntity.IsTaxaUrgencia = input.IsTaxaUrgencia;
                            itemEntity.IsPediatria = input.IsPediatria;
                            itemEntity.IsProcedimentoSerie = input.IsProcedimentoSerie;
                            itemEntity.IsRequisicaoExame = input.IsRequisicaoExame;
                            itemEntity.IsPermiteRevisao = input.IsPermiteRevisao;
                            itemEntity.IsPrecoManual = input.IsPrecoManual;
                            itemEntity.IsAutorizacao = input.IsAutorizacao;
                            itemEntity.IsInternacao = input.IsInternacao;
                            itemEntity.IsAmbulatorio = input.IsAmbulatorio;
                            itemEntity.IsCirurgia = input.IsCirurgia;
                            itemEntity.IsPorte = input.IsPorte;
                            itemEntity.IsConsultor = input.IsConsultor;
                            itemEntity.IsLaboratorio = input.IsLaboratorio;
                            itemEntity.IsPlantonista = input.IsPlantonista;
                            itemEntity.IsOpme = input.IsOpme;
                            itemEntity.IsExtraCaixa = input.IsExtraCaixa;
                            itemEntity.IsLaudo = input.IsLaudo;

                            await _itemRepository.UpdateAsync(itemEntity);
                        }

                        //await _itemRepository.UpdateAsync(Item);
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(FaturamentoItemDto input)
        {
            try
            {
                await _itemRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoItemDto> Obter(long id)
        {
            try
            {
                var query = await _itemRepository
                    .GetAll()
                    .Include(m => m.Grupo)
                    .Include(m => m.SubGrupo)
                    .Include(m => m.LaudoGrupo)
                    .Include(i => i.Material)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var item = FaturamentoItemDto.Mapear(query);

                return item;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FaturamentoItemDto> ObterPorCodigo(string codigo)
        {
            try
            {
                var query = await _itemRepository
                    .GetAll()
                    .Include(m => m.Grupo)
                    .Where(m => m.Codigo == codigo)
                    .FirstOrDefaultAsync();

                var item = FaturamentoItemDto.Mapear(query);

                return item;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<long> ObterTipoGrupoId(long? fatItemId)
        {
            if (!fatItemId.HasValue)
            {
                return 0;
            }

            try
            {
                var query = await _itemRepository
                    .GetAll()
                    .Include(m => m.Grupo.TipoGrupo)
                    .Where(m => m.Id == fatItemId)
                    .FirstOrDefaultAsync();

                var item = FaturamentoItemDto.Mapear(query);

                return item.Grupo.TipoGrupo != null ? item.Grupo.TipoGrupo.Id : 0;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }

        }

        public async Task<FaturamentoItemDto> ObterComEstado(string nome, long estadoId)
        {
            try
            {
                var query = _itemRepository
                    .GetAll();

                var result = await query.FirstOrDefaultAsync();
                var item = FaturamentoItemDto.Mapear(result);

                return item;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarFaturamentoItensInput input)
        {
            try
            {
                var result = await Listar(input);
                var itens = result.Items;
                return _listarItensExcelExporter.ExportToFile(itens.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            ).Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();
                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownListDeluxe> ListarDropdownDeluxe(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            ).Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItemsDeluxe
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao),
                                codigo = p.Codigo
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownListDeluxe() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownCodigo(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            ).Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownExame(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            .Where(m =>
                                //exame laboratorial
                                m.IsLaboratorio ||
                                m.Grupo.IsLaboratorio ||
                                m.SubGrupo.IsLaboratorio ||
                                m.IsLaudo ||
                                m.Grupo.IsLaudo ||
                                m.SubGrupo.IsLaudo
                                )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao) //string.Format("{0:D8} {1}", p.Codigo, p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FaturamentoItemDto>> ListarTodos()
        {
            try
            {
                var query = _itemRepository.GetAll();

                var faturamentoItensDto = await query.ToListAsync();

                return new ListResultDto<FaturamentoItemDto> { Items = FaturamentoItemDto.Mapear(faturamentoItensDto).ToList() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarFatItemDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                        .Where(f => !f.IsLaudo && !f.Grupo.IsLaudo && !f.IsLaboratorio && !f.Grupo.IsLaboratorio)
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarExameLaboratorialDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                        .Where(f => f.Grupo.IsLaboratorio || f.IsLaboratorio || f.SubGrupo.IsLaboratorio)
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarExameImagemDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            .Where(f => (f.Grupo.IsLaudo && !f.Grupo.IsLaboratorio) || (f.IsLaudo && !f.IsLaboratorio) || (f.SubGrupo.IsLaudo && !f.SubGrupo.IsLaboratorio))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarExameDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            .Where(f => ((f.Grupo.IsLaudo && !f.Grupo.IsLaboratorio) || (f.IsLaudo && !f.IsLaboratorio) || (f.SubGrupo.IsLaudo && !f.SubGrupo.IsLaboratorio)) ||
                                        (f.Grupo.IsLaboratorio || f.IsLaboratorio || f.SubGrupo.IsLaboratorio))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDiagnosticoImagemDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            ).Where(f => f.IsLaudo || f.Grupo.IsLaudo || f.SubGrupo.IsLaudo)
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarFaturamentoItemPorGrupoSubGrupoDropdown(DropdownInput dropdownInput)
        {
            long grupoId;
            long subGrupoId;

            long.TryParse(dropdownInput.filtros[0], out grupoId);
            long.TryParse(dropdownInput.filtros[1], out subGrupoId);

            return await ListarDropdownLambda(dropdownInput
                                              , _itemRepository
                                              , m => ((grupoId == 0 || m.GrupoId == grupoId)
                                                   && (subGrupoId == 0 || m.SubGrupoId == subGrupoId)
                                                   )
                                              , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                              , o => o.Descricao
                                              );
        }

        public async Task<ResultDropdownList> ListarDropdownTodos(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _itemRepository);

        }


        public async Task<ResultDropdownList> ListarDropdownSemPacote(DropdownInput dropdownInput)
        {

            return await ListarDropdownLambda(dropdownInput
                                              , _itemRepository
                                              , m => (m.Grupo.TipoGrupoId != 4
                                                  && (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                                       m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()))
                                                       )
                                              , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                              , o => o.Descricao
                                              );
        }

        public async Task<ResultDropdownList> ListarDropdownPacote(DropdownInput dropdownInput)
        {

            return await ListarDropdownLambda(dropdownInput
                                              , _itemRepository
                                              , m => (m.Grupo.TipoGrupoId == 4
                                                  && (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                                       m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()))
                                                       )
                                              , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                              , o => o.Descricao
                                              );
        }



        public async Task<ResultDropdownList> ListarNaoLaudoNaoLaboratorioDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _itemRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                        .Where(f => !(f.Grupo.IsLaboratorio || f.IsLaboratorio || f.SubGrupo.IsLaboratorio)
                                 && !(f.Grupo.IsLaudo || f.IsLaudo || f.SubGrupo.IsLaudo)
                                 )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }

    public class ResultDropdownListDeluxe
    {
        public List<DropdownItemsDeluxe> Items { get; set; }

        public int TotalCount { get; set; }
    }

    public class DropdownItemsDeluxe
    {
        public long id { get; set; }
        public string text { get; set; }
        public string codigo { get; set; }
    }
}
