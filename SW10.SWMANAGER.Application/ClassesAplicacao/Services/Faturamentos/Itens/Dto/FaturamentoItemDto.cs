﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Diagnosticos.Laudos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.SubGrupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Equipamentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Exames.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Setores.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Materiais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Metodos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.LaboratoriosUnidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Formatas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Mapas.Dto;
using System.Collections.Generic;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto
{
    [AutoMap(typeof(FaturamentoItem))]
    public class FaturamentoItemDto : CamposPadraoCRUDDto
    {        
        public override string Codigo { get; set; }
        public override string Descricao { get; set; }
        
        public FaturamentoBrasItemDto BrasItem { get; set; }
        public long? BrasItemId { get; set; }
        
        public FaturamentoGrupoDto Grupo { get; set; }
        public long? GrupoId { get; set; }
        
        public FaturamentoSubGrupoDto SubGrupo { get; set; }
        public long? SubGrupoId { get; set; }

        public LaudoGrupoDto LaudoGrupo { get; set; }
        public long? LaudoGrupoId { get; set; }

        public string DescricaoTuss { get; set; }
        
        public string Observacao { get; set; }
        
        public string CodAmb { get; set; }
        
        public string CodTuss { get; set; }
        
        public string CodCbhpm { get; set; }

        public float DivideBrasindice { get; set; }

        public string Referencia { get; set; }

        public string ReferenciaSihSus { get; set; }

        public int Sexo { get; set; }

        public int QtdLaudo { get; set; }

        public int TipoLaudo { get; set; }

        public int DuracaoMinima { get; set; }

        public bool IsAtivo { get; set; }

        public bool IsObrigaMedico { get; set; }

        public bool IsTaxaUrgencia { get; set; }

        public bool IsPediatria { get; set; }

        public bool IsProcedimentoSerie { get; set; }

        public bool IsRequisicaoExame { get; set; }

        public bool IsPermiteRevisao { get; set; }

        public bool IsPrecoManual { get; set; }

        public bool IsAutorizacao { get; set; }

        public bool IsInternacao { get; set; }

        public bool IsAmbulatorio { get; set; }

        public bool IsCirurgia { get; set; }

        public bool IsPorte { get; set; }

        public bool IsConsultor { get; set; }

        public bool IsLaboratorio { get; set; }

        public bool IsPlantonista { get; set; }

        public bool IsOpme { get; set; }

        public bool IsExtraCaixa { get; set; }

        public bool IsLaudo { get; set; }

        #region Exame

        public bool IsExameSimples { get; set; }
        public bool IsPeso { get; set; }
        public bool IsTesta100 { get; set; }
        public bool IsAltura { get; set; }
        public bool IsCor { get; set; }
        public bool IsMestruacao { get; set; }
        public bool IsNacionalidade { get; set; }
        public bool IsNaturalidade { get; set; }
        public bool IsImpReferencia { get; set; }
        public bool IsCultura { get; set; }
        public bool IsPendente { get; set; }
        public bool IsRepete { get; set; }
        public bool IsLibera { get; set; }
        public string Mneumonico { get; set; }
        public int? OrdemImp { get; set; }
        public int? Prazo { get; set; }
        public byte[] Interpretacao { get; set; }
        public byte[] Extra1 { get; set; }
        public byte[] Extra2 { get; set; }
        public int? QtdFatura { get; set; }
        public string MapaExame { get; set; }
        public int? OrdemResul { get; set; }
        public int? OrdemResumo { get; set; }
        public int? OrdemMapaResultado { get; set; }
        public long? EquipamentoId { get; set; }
        public long? ExameIncluiId { get; set; }
        public long? SetorId { get; set; }
        public long? MaterialId { get; set; }
        public long? MetodoId { get; set; }
        public long? UnidadeId { get; set; }
        public long? FormataId { get; set; }
        public long? MapaId { get; set; }

        public EquipamentoDto Equipamento { get; set; }
        public FaturamentoItemDto ExameInclui { get; set; }
        public SetorDto Setor { get; set; }
        public MaterialDto Material { get; set; }
        public MetodoDto Metodo { get; set; }
        public LaboratorioUnidadeDto Unidade { get; set; }
        public FormataDto Formata { get; set; }
        public MapaDto Mapa { get; set; }

        #endregion


        #region Mapeamento

        public static FaturamentoItemDto Mapear(FaturamentoItem faturamentoItem)
        {
            FaturamentoItemDto faturamentoItemDto =new  FaturamentoItemDto();

            faturamentoItemDto.Id = faturamentoItem.Id;
            faturamentoItemDto.Codigo = faturamentoItem.Codigo;
            faturamentoItemDto.Descricao = faturamentoItem.Descricao;
            faturamentoItemDto.Id = faturamentoItem.Id;
            faturamentoItemDto.MaterialId = faturamentoItem.MaterialId;
            faturamentoItemDto.QtdFatura = faturamentoItem.QtdFatura;
            faturamentoItemDto.GrupoId = faturamentoItem.GrupoId;
            faturamentoItemDto.SubGrupoId = faturamentoItem.SubGrupoId;
            faturamentoItemDto.CodTuss = faturamentoItem.CodTuss;
            faturamentoItemDto.DescricaoTuss = faturamentoItem.DescricaoTuss;
            faturamentoItemDto.ExameIncluiId = faturamentoItem.ExameIncluiId;

            if (faturamentoItem.Material !=null)
            {
                faturamentoItemDto.Material = new MaterialDto { Id = faturamentoItem.Material.Id, Codigo = faturamentoItem.Material.Codigo, Descricao = faturamentoItem.Material.Descricao };
            }


            if(faturamentoItem.Grupo!=null)
            {
                faturamentoItemDto.Grupo = FaturamentoGrupoDto.Mapear(faturamentoItem.Grupo);
            }

            if (faturamentoItem.ExameInclui != null)
            {
                faturamentoItemDto.ExameInclui = new FaturamentoItemDto
                {
                    Id = faturamentoItem.ExameInclui.Id,
                    Codigo = faturamentoItem.ExameInclui.Codigo,
                    Descricao = faturamentoItem.ExameInclui.Descricao,
                    EquipamentoId = faturamentoItem.ExameInclui.EquipamentoId,
                    ExameIncluiId = faturamentoItem.ExameInclui.ExameIncluiId,
                    Extra1 = faturamentoItem.ExameInclui.Extra1,
                    Extra2 = faturamentoItem.ExameInclui.Extra2,
                    FormataId = faturamentoItem.ExameInclui.FormataId,
                    Interpretacao = faturamentoItem.ExameInclui.Interpretacao,
                    IsAltura = faturamentoItem.ExameInclui.IsAltura,
                    IsCor = faturamentoItem.ExameInclui.IsCor,
                    IsCultura = faturamentoItem.ExameInclui.IsCultura,
                    IsExameSimples = faturamentoItem.ExameInclui.IsExameSimples,
                    IsImpReferencia = faturamentoItem.ExameInclui.IsImpReferencia,
                    IsLibera = faturamentoItem.ExameInclui.IsLibera,
                    IsMestruacao = faturamentoItem.ExameInclui.IsMestruacao,
                    IsNacionalidade = faturamentoItem.ExameInclui.IsNacionalidade,
                    IsNaturalidade = faturamentoItem.ExameInclui.IsNaturalidade,
                    IsPendente = faturamentoItem.ExameInclui.IsPendente,
                    IsPeso = faturamentoItem.ExameInclui.IsPeso,
                    IsTesta100 = faturamentoItem.ExameInclui.IsTesta100,
                    MapaExame = faturamentoItem.ExameInclui.MapaExame,
                    IsRepete = faturamentoItem.ExameInclui.IsRepete,
                    MapaId = faturamentoItem.ExameInclui.MapaId,
                    MaterialId = faturamentoItem.ExameInclui.MaterialId,
                    MetodoId = faturamentoItem.ExameInclui.MetodoId,
                    Mneumonico = faturamentoItem.ExameInclui.Mneumonico,
                    OrdemImp = faturamentoItem.ExameInclui.OrdemImp,
                    OrdemMapaResultado = faturamentoItem.ExameInclui.OrdemMapaResultado,
                    OrdemResul = faturamentoItem.ExameInclui.OrdemResul,
                    OrdemResumo = faturamentoItem.ExameInclui.OrdemResumo,
                    Prazo = faturamentoItem.ExameInclui.Prazo,
                    QtdFatura = faturamentoItem.ExameInclui.QtdFatura,
                    SetorId = faturamentoItem.ExameInclui.SetorId,
                    UnidadeId = faturamentoItem.ExameInclui.UnidadeId
                };
            }

            return faturamentoItemDto;
        }

        public static FaturamentoItem Mapear(FaturamentoItemDto faturamentoItemDto)
        {
            FaturamentoItem faturamentoItem = new FaturamentoItem();

            faturamentoItem.Id = faturamentoItemDto.Id;
            faturamentoItem.Codigo = faturamentoItemDto.Codigo;
            faturamentoItem.Descricao = faturamentoItemDto.Descricao;
            faturamentoItem.Id = faturamentoItemDto.Id;


            return faturamentoItem;
        }

        public static IEnumerable<FaturamentoItemDto> Mapear(List<FaturamentoItem> faturamentoItem)
        {
            foreach (var item in faturamentoItem)
            {
                FaturamentoItemDto faturamentoItemDto = new FaturamentoItemDto();

                faturamentoItemDto.Id = item.Id;
                faturamentoItemDto.Codigo = item.Codigo;
                faturamentoItemDto.Descricao = item.Descricao;
                faturamentoItemDto.Id = item.Id;
                faturamentoItemDto.MaterialId = item.MaterialId;
                faturamentoItemDto.QtdFatura = item.QtdFatura;
                faturamentoItemDto.GrupoId = item.GrupoId;
                faturamentoItemDto.SubGrupoId = item.SubGrupoId;
                faturamentoItemDto.CodTuss = item.CodTuss;
                faturamentoItemDto.DescricaoTuss = item.DescricaoTuss;
                faturamentoItemDto.ExameIncluiId = item.ExameIncluiId;

                if (item.Material != null)
                {
                    faturamentoItemDto.Material = new MaterialDto { Id = item.Material.Id, Codigo = item.Material.Codigo, Descricao = item.Material.Descricao };
                }


                if (item.Grupo != null)
                {
                    faturamentoItemDto.Grupo = FaturamentoGrupoDto.Mapear(item.Grupo);
                }

                if (item.ExameInclui != null)
                {
                    faturamentoItemDto.ExameInclui = new FaturamentoItemDto
                    {
                        Id = item.ExameInclui.Id,
                        Codigo = item.ExameInclui.Codigo,
                        Descricao = item.ExameInclui.Descricao,
                        EquipamentoId = item.ExameInclui.EquipamentoId,
                        ExameIncluiId = item.ExameInclui.ExameIncluiId,
                        Extra1 = item.ExameInclui.Extra1,
                        Extra2 = item.ExameInclui.Extra2,
                        FormataId = item.ExameInclui.FormataId,
                        Interpretacao = item.ExameInclui.Interpretacao,
                        IsAltura = item.ExameInclui.IsAltura,
                        IsCor = item.ExameInclui.IsCor,
                        IsCultura = item.ExameInclui.IsCultura,
                        IsExameSimples = item.ExameInclui.IsExameSimples,
                        IsImpReferencia = item.ExameInclui.IsImpReferencia,
                        IsLibera = item.ExameInclui.IsLibera,
                        IsMestruacao = item.ExameInclui.IsMestruacao,
                        IsNacionalidade = item.ExameInclui.IsNacionalidade,
                        IsNaturalidade = item.ExameInclui.IsNaturalidade,
                        IsPendente = item.ExameInclui.IsPendente,
                        IsPeso = item.ExameInclui.IsPeso,
                        IsTesta100 = item.ExameInclui.IsTesta100,
                        MapaExame = item.ExameInclui.MapaExame,
                        IsRepete = item.ExameInclui.IsRepete,
                        MapaId = item.ExameInclui.MapaId,
                        MaterialId = item.ExameInclui.MaterialId,
                        MetodoId = item.ExameInclui.MetodoId,
                        Mneumonico = item.ExameInclui.Mneumonico,
                        OrdemImp = item.ExameInclui.OrdemImp,
                        OrdemMapaResultado = item.ExameInclui.OrdemMapaResultado,
                        OrdemResul = item.ExameInclui.OrdemResul,
                        OrdemResumo = item.ExameInclui.OrdemResumo,
                        Prazo = item.ExameInclui.Prazo,
                        QtdFatura = item.ExameInclui.QtdFatura,
                        SetorId = item.ExameInclui.SetorId,
                        UnidadeId = item.ExameInclui.UnidadeId
                    };
                }

                yield return faturamentoItemDto;
            }
        }

        public static IEnumerable<FaturamentoItem> Mapear(List<FaturamentoItemDto> faturamentoItemDto)
        {
            foreach (var item in faturamentoItemDto)
            {
                FaturamentoItem result = new FaturamentoItem();

                result.Id = item.Id;
                result.Codigo = item.Codigo;
                result.Descricao = item.Descricao;
                result.Id = item.Id;
                result.MaterialId = item.MaterialId;
                result.QtdFatura = item.QtdFatura;
                result.GrupoId = item.GrupoId;
                result.SubGrupoId = item.SubGrupoId;
                result.CodTuss = item.CodTuss;
                result.DescricaoTuss = item.DescricaoTuss;
                result.ExameIncluiId = item.ExameIncluiId;

                if (item.Material != null)
                {
                    result.Material = new Material { Id = item.Material.Id, Codigo = item.Material.Codigo, Descricao = item.Material.Descricao };
                }


                if (item.Grupo != null)
                {
                    result.Grupo = FaturamentoGrupoDto.Mapear(item.Grupo);
                }

                if (item.ExameInclui != null)
                {
                    result.ExameInclui = new FaturamentoItem
                    {
                        Id = item.ExameInclui.Id,
                        Codigo = item.ExameInclui.Codigo,
                        Descricao = item.ExameInclui.Descricao,
                        EquipamentoId = item.ExameInclui.EquipamentoId,
                        ExameIncluiId = item.ExameInclui.ExameIncluiId,
                        Extra1 = item.ExameInclui.Extra1,
                        Extra2 = item.ExameInclui.Extra2,
                        FormataId = item.ExameInclui.FormataId,
                        Interpretacao = item.ExameInclui.Interpretacao,
                        IsAltura = item.ExameInclui.IsAltura,
                        IsCor = item.ExameInclui.IsCor,
                        IsCultura = item.ExameInclui.IsCultura,
                        IsExameSimples = item.ExameInclui.IsExameSimples,
                        IsImpReferencia = item.ExameInclui.IsImpReferencia,
                        IsLibera = item.ExameInclui.IsLibera,
                        IsMestruacao = item.ExameInclui.IsMestruacao,
                        IsNacionalidade = item.ExameInclui.IsNacionalidade,
                        IsNaturalidade = item.ExameInclui.IsNaturalidade,
                        IsPendente = item.ExameInclui.IsPendente,
                        IsPeso = item.ExameInclui.IsPeso,
                        IsTesta100 = item.ExameInclui.IsTesta100,
                        MapaExame = item.ExameInclui.MapaExame,
                        IsRepete = item.ExameInclui.IsRepete,
                        MapaId = item.ExameInclui.MapaId,
                        MaterialId = item.ExameInclui.MaterialId,
                        MetodoId = item.ExameInclui.MetodoId,
                        Mneumonico = item.ExameInclui.Mneumonico,
                        OrdemImp = item.ExameInclui.OrdemImp,
                        OrdemMapaResultado = item.ExameInclui.OrdemMapaResultado,
                        OrdemResul = item.ExameInclui.OrdemResul,
                        OrdemResumo = item.ExameInclui.OrdemResumo,
                        Prazo = item.ExameInclui.Prazo,
                        QtdFatura = item.ExameInclui.QtdFatura,
                        SetorId = item.ExameInclui.SetorId,
                        UnidadeId = item.ExameInclui.UnidadeId
                    };
                }

                yield return result;
            }
        }
        #endregion
    }
}
