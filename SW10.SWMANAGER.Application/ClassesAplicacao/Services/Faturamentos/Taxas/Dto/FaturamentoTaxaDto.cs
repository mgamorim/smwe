﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLeito.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Taxas;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Taxas.Dto
{
    [AutoMap(typeof(FaturamentoTaxa))]
    public class FaturamentoTaxaDto : CamposPadraoCRUDDto
    {
        public override string Codigo { get; set; }
        public override string Descricao { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public double Percentual { get; set; }
        public bool IsAmbulatorio { get; set; }
        public bool IsInternacao { get; set; }
        public bool IsIncideFilme { get; set; }
        public bool IsIncideManual { get; set; }
        public bool IsImplicita { get; set; }
        public bool IsTodosLocal { get; set; }
        public bool IsTodosTurno { get; set; }
        public bool IsTodosTipoLeito { get; set; }
        public bool IsTodosGrupo { get; set; }
        public bool IsTodosItem { get; set; }
        public bool IsTodosConvenio { get; set; }
        public bool IsTodosPlano { get; set; }
        public string LocalImpressao { get; set; }
        public string EmpresasJson { get; set; }
        public string LocaisJson { get; set; }
        public string GruposJson { get; set; }
        public string TurnosJson { get; set; }
        public string TiposLeitosJson { get; set; }
    }

    [AutoMap(typeof(FaturamentoTaxaEmpresa))]
    public class TaxaEmpresaDto : CamposPadraoCRUDDto
    {
        public long? TaxaId { get; set; }
        public FaturamentoTaxaDto FaturamentoTaxa { get; set; }
        public long? EmpresaId { get; set; }
        public EmpresaDto Empresa { get; set; }
    }

    [AutoMap(typeof(FaturamentoTaxaLocal))]
    public class TaxaLocalDto : CamposPadraoCRUDDto
    {
        public long? TaxaId { get; set; }
        public FaturamentoTaxaDto FaturamentoTaxa { get; set; }
        public long? UnidadeOrganizacaionalId { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }
    }

    [AutoMap(typeof(FaturamentoTaxaTurno))]
    public class TaxaTurnoDto : CamposPadraoCRUDDto
    {
        public long? TaxaId { get; set; }
        public FaturamentoTaxaDto FaturamentoTaxa { get; set; }
        public long? TurnoId { get; set; }
        public TurnoDto Turno { get; set; }
    }

    [AutoMap(typeof(FaturamentoTaxaTipoLeito))]
    public class TaxaTipoLeitoDto : CamposPadraoCRUDDto
    {
        public long? TaxaId { get; set; }
        public FaturamentoTaxaDto FaturamentoTaxa { get; set; }
        public long? TipoLeitoId { get; set; }
        public TipoLeitoDto TipoLeito { get; set; }
    }

    [AutoMap(typeof(FaturamentoTaxaGrupo))]
    public class TaxaGrupoDto : CamposPadraoCRUDDto
    {
        public long? TaxaId { get; set; }
        public FaturamentoTaxaDto FaturamentoTaxa { get; set; }
        public long? GrupoId { get; set; }
        public FaturamentoGrupoDto FaturamentoGrupo { get; set; }
    }
}
