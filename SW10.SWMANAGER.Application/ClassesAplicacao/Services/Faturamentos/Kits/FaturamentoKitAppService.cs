#region Usings
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Kits;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;
using Abp.Domain.Uow;
using Newtonsoft.Json;
#endregion usings.

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits
{
    public class FaturamentoKitAppService : SWMANAGERAppServiceBase, IFaturamentoKitAppService
    {
        #region Cabecalho
        private readonly IRepository<FaturamentoKit, long> _kitRepository;
        private readonly IListarKitsExcelExporter _listarKitsExcelExporter;
        private readonly IFaturamentoGrupoAppService _faturamentoGrupoAppService;
        private readonly IFaturamentoItemAppService _faturamentoItemAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;


        public FaturamentoKitAppService(
            IRepository<FaturamentoKit, long> kitRepository,
            IListarKitsExcelExporter listarKitsExcelExporter,
            IFaturamentoGrupoAppService faturamentoGrupoAppService,
            IFaturamentoItemAppService faturamentoItemAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _kitRepository = kitRepository;
            _listarKitsExcelExporter = listarKitsExcelExporter;
            _faturamentoGrupoAppService = faturamentoGrupoAppService;
            _faturamentoItemAppService = faturamentoItemAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }
        #endregion cabecalho.

        public async Task<PagedResultDto<FaturamentoKitDto>> Listar(ListarFaturamentoKitsInput input)
        {
            var itemrKits = 0;
            List<FaturamentoKit> itens;
            List<FaturamentoKitDto> itensDtos = new List<FaturamentoKitDto>();
            try
            {
                var query = _kitRepository
                    .GetAll()
                    ;

                itemrKits = await query
                    .CountAsync();

                itens = await query
                    .AsNoTracking()
                    //   .OrderBy(input.Sorting)
                    //   .PageBy(input)
                    .ToListAsync();

                itensDtos = itens
                    .MapTo<List<FaturamentoKitDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<FaturamentoKitDto>(
                itemrKits,
                itensDtos
                );
        }


        public async Task CriarOuEditar(FaturamentoKitDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var kit = FaturamentoKitDto.Mapear(input);

                    var itensQuantidades = JsonConvert.DeserializeObject<List<FaturamentoItemQuantidade>>(input.StrItensQtds);


                    if (input.Id == 0)
                    {
                        kit.FatItens = new List<FaturamentoKitItem>();

                        foreach (var item in itensQuantidades)
                        {
                            kit.FatItens.Add(new FaturamentoKitItem { FatItemId = item.ItemId, Quantidade = item.Quantidade });
                        }

                        _kitRepository.Insert(kit);
                    }
                    else
                    {

                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }


        //public async Task CriarOuEditar(FaturamentoKitDto input)
        //{
        //    try
        //    {
        //        var Kit = input.MapTo<FaturamentoKit>();

            //        if (input.Id.Equals(0))
            //        {
            //            var itens = input.itensIds.Distinct();

            //            using (var contexto = new SWMANAGERDbContext())
            //            {
            //                contexto.Entry(Kit).State = EntityState.Added;

            //                foreach (var itemId in itens)
            //                {
            //                    var itemDto = AsyncHelper.RunSync(() => _faturamentoItemAppService.Obter(itemId));
            //                    var item = itemDto.MapTo<FaturamentoItem>();

            //                    //  if (contexto.Entry(item).State == EntityState.Detached)
            //                    //contexto.FaturamentoItens.Attach(item);

            //                  //  Kit.Itens.Add(item);

            //                    contexto.Entry(item).State = EntityState.Modified;
            //                }

            //                contexto.SaveChanges();
            //            }
            //        }
            //        else
            //        {
            //            var itens = input.itensIds.Distinct();

            //            foreach (var itemId in itens)
            //            {
            //                var itemDto = AsyncHelper.RunSync(() => _faturamentoItemAppService.Obter(itemId));
            //                var item = itemDto.MapTo<FaturamentoItem>();
            //               // Kit.FatItens.Add(item);
            //            }

            //            //  using (var contexto = new SWMANAGERDbContext())
            //            // {
            //            var kitExistente = _kitRepository.GetAll()
            //                                              .Include(i => i.FatItens)
            //                                              .Where(s => s.Id == Kit.Id)
            //                                              .FirstOrDefault<FaturamentoKit>();

            //            if (kitExistente != null)
            //            {
            //                //Excluir
            //                kitExistente.FatItens.RemoveRange(0, kitExistente.FatItens.Count());



            //                //inclui
            //                foreach (var item in Kit.Itens)
            //                {
            //                    kitExistente.Itens.Add(item);
            //                }






            //                ////atuliza 
            //                //foreach (var item in kitExistente.Itens)
            //                //{
            //                //    var novoItem = Kit.Itens.Where(w => w.Id == item.Id)
            //                //                                   .First();

            //                //    item.GrupoId = novoItem.GrupoId;
            //                //    item.SubGrupoId = novoItem.SubGrupoId;
            //                //    item.QtdFatura = novoItem.QtdFatura;
            //                //    item.

            //                //}


            //                //List<FaturamentoItem> itensDeletados = new List<FaturamentoItem>();

            //                //foreach (var it in kitExistente.Itens)
            //                //{
            //                //    if (Kit.Itens.FirstOrDefault(ite => ite.Id == it.Id) == null)
            //                //        itensDeletados.Add(it);
            //                //}

            //                //List<FaturamentoItem> itensAdicionados = new List<FaturamentoItem>();

            //                //foreach (var it in Kit.Itens)
            //                //{
            //                //    if (kitExistente.Itens.FirstOrDefault(ite => ite.Id == it.Id) == null)
            //                //        itensAdicionados.Add(it);
            //                //}

            //                //itensDeletados.ForEach(c => kitExistente.Itens.Remove(c));

            //                //foreach (var c in itensAdicionados)
            //                //{
            //                //    // if (contexto.Entry(c).State == EntityState.Detached)
            //                //    //    contexto.FaturamentoItens.Attach(c);

            //                //    //   contexto.Entry(c).State = EntityState.Modified;

            //                //    kitExistente.Itens.Add(c);

            //                //    contexto.Entry(c).State = EntityState.Modified;
            //                //}

            //                kitExistente.Codigo = input.Codigo;
            //                kitExistente.Descricao = input.Descricao;

            //                await _kitRepository.UpdateAsync(kitExistente);
            //            }
            //            //  }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        throw new UserFriendlyException(L("ErroSalvar"), ex);
            //    }
            //}

        public async Task Excluir(FaturamentoKitDto input)
        {
            try
            {
                await _kitRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoKitDto> Obter(long id)
        {
            try
            {
                var query = await _kitRepository
                    .GetAll()
                    .Include(m => m.FatItens)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var item = FaturamentoKitDto.Mapear(query);

                return item;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarFaturamentoKitsInput input)
        {
            try
            {
                var result = await Listar(input);
                var itens = result.Items;
                return _listarKitsExcelExporter.ExportToFile(itens.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }
        
        public async Task<ListResultDto<FaturamentoKitDto>> ListarTodos()
        {
            try
            {
                var query = _kitRepository.GetAll();

                var faturamentoKitsDto = await query.ToListAsync();

                return new ListResultDto<FaturamentoKitDto> { Items = faturamentoKitsDto.MapTo<List<FaturamentoKitDto>>() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FaturamentoKitDto>> ListarPrevio(FaturamentoItemDto[] itens)
        {
            try
            {
                //    var query = _itemRepository.GetAll();

                //    var faturamentoKitsDto = await query.ToListAsync();

                return new ListResultDto<FaturamentoKitDto> { Items = itens.MapTo<List<FaturamentoKitDto>>() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<FaturamentoItemDto>> ListarPrevio (ListarItensKitFaturamentoInput input)
        {
            try
            {
                List<FaturamentoItemDto> listItens = new List<FaturamentoItemDto>();

                 var kit = await Obter(input.KitFaturamentoId??0);

                List<long> faturamentoItensIds = new List<long>();

                faturamentoItensIds = kit.Itens.Select(s => s.Id).ToList();


                //foreach(var id in kit.itensIds)
                //{
                //    //var novoItem = _faturamentoItemAppService.Obter(id);
                //    var novoItem = AsyncHelper.RunSync(() => _faturamentoItemAppService.Obter(id));
                //    listItens.Add(novoItem);
                //}

                // return new ListResultDto<FaturamentoItemDto> { Items = listItens.MapTo<List<FaturamentoItemDto>>() };


                //  long kitId = (input.KitFaturamentoId != null)? (long)input.KitFaturamentoId : 0;

                SWRepository<FaturamentoItem> _faturamentoItemRepository = new SWRepository<FaturamentoItem>();

                var query = _faturamentoItemRepository.GetAll()
                                          .Where(m => faturamentoItensIds.Any(a => a == m.Id))
                                          .Include(i=> i.Grupo)
                                          .Include(i => i.Grupo.TipoGrupo)
                                          .Include(i => i.SubGrupo)
                                          ;
                                          //.Select( s=> s.Itens);





               var  itens = query
                  .AsNoTracking()
                  .OrderBy(input.Sorting)
                  .PageBy(input)
               .ToList();

                var itensDto = new List<FaturamentoItemDto>();   //  itens.MapTo<List<FaturamentoItemDto>>();

                foreach (var item in itens)
                {
                    itensDto.Add(FaturamentoItemDto.Mapear(item));
                }

                return new PagedResultDto<FaturamentoItemDto>(query.Count(), itensDto );



            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _kitRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            //.Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }

    //public class teste
    //{
    //    public long[] ids { get; set; }
    //}
}
