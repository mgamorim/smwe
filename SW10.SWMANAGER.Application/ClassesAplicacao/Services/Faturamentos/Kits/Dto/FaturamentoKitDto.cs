﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Kits;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits.Dto
{
    [AutoMap(typeof(FaturamentoKit))]
    public class FaturamentoKitDto : CamposPadraoCRUDDto
    {
        public override string Codigo { get; set; }
        public override string Descricao { get; set; }

        public List<FaturamentoKitItemDto> Itens { get; set; }

        public long[] itensIds { get; set; }
        public string Observacao { get; set; }

        public List<FaturamentoItemQuantidade> ItensQuantidade { get; set; }

        public string StrItensQtds { get; set; }

        #region Mapear

        public static FaturamentoKit Mapear(FaturamentoKitDto faturamentoKitDto )
        {
            FaturamentoKit faturamentoKit = new FaturamentoKit();

            faturamentoKit.Id = faturamentoKitDto.Id;
            faturamentoKit.Codigo = faturamentoKitDto.Codigo;
            faturamentoKit.Descricao = faturamentoKitDto.Descricao;
            faturamentoKit.Observacao = faturamentoKitDto.Observacao;

            faturamentoKit.FatItens = FaturamentoKitItemDto.Mapear(faturamentoKitDto.Itens);

            return faturamentoKit;
        }

        public static FaturamentoKitDto Mapear(FaturamentoKit faturamentoKit)
        {
            FaturamentoKitDto faturamentoKitDto = new FaturamentoKitDto();

            faturamentoKitDto.Id = faturamentoKit.Id;
            faturamentoKitDto.Codigo = faturamentoKit.Codigo;
            faturamentoKitDto.Descricao = faturamentoKit.Descricao;
            faturamentoKitDto.Observacao = faturamentoKit.Observacao;

            faturamentoKitDto.Itens = FaturamentoKitItemDto.Mapear(faturamentoKit.FatItens);

            return faturamentoKitDto;
        }

        #endregion

    }
}
