﻿using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Kits;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits.Dto
{
    public class FaturamentoKitItemDto : CamposPadraoCRUDDto
    {
        public long? FatKitId { get; set; }
        public FaturamentoKitDto FatKit { get; set; }

        public long? FatItemId { get; set; }
        public FaturamentoItemDto FatItem { get; set; }

        public decimal Quantidade { get; set; }

        public static FaturamentoKitItemDto Mapear(FaturamentoKitItem faturamentoKitItem)
        {
            FaturamentoKitItemDto faturamentoKitItemDto = new FaturamentoKitItemDto();

            faturamentoKitItemDto.Id = faturamentoKitItem.Id;
            faturamentoKitItemDto.Codigo = faturamentoKitItem.Codigo;
            faturamentoKitItemDto.Descricao = faturamentoKitItem.Descricao;
            faturamentoKitItemDto.Quantidade = faturamentoKitItem.Quantidade;
            faturamentoKitItemDto.FatKitId = faturamentoKitItem.FatKitId;
            faturamentoKitItemDto.FatItemId = faturamentoKitItem.FatItemId;

            if(faturamentoKitItem.FatItem!=null)
            {
                faturamentoKitItemDto.FatItem = FaturamentoItemDto.Mapear(faturamentoKitItem.FatItem);
            }


            return faturamentoKitItemDto;
        }

        public static FaturamentoKitItem Mapear(FaturamentoKitItemDto faturamentoKitItemDto)
        {
            FaturamentoKitItem faturamentoKitItem = new FaturamentoKitItem();

            faturamentoKitItem.Id = faturamentoKitItemDto.Id;
            faturamentoKitItem.Codigo = faturamentoKitItemDto.Codigo;
            faturamentoKitItem.Descricao = faturamentoKitItemDto.Descricao;
            faturamentoKitItem.Quantidade = faturamentoKitItemDto.Quantidade;
            faturamentoKitItem.FatKitId = faturamentoKitItemDto.FatKitId;
            faturamentoKitItem.FatItemId = faturamentoKitItemDto.FatItemId;

            if (faturamentoKitItemDto.FatItem != null)
            {
                faturamentoKitItem.FatItem = FaturamentoItemDto.Mapear(faturamentoKitItemDto.FatItem);
            }

            return faturamentoKitItem;
        }

        public static List<FaturamentoKitItemDto> Mapear(List<FaturamentoKitItem> itens)
        {
            List<FaturamentoKitItemDto> itensDto = new List<FaturamentoKitItemDto>();

            if (itens != null)
            {
                foreach (var item in itens)
                {
                    itensDto.Add(Mapear(item));
                }
            }
            return itensDto;
        }

        public static List<FaturamentoKitItem> Mapear(List<FaturamentoKitItemDto> itensDto)
        {
            List<FaturamentoKitItem> itens = new List<FaturamentoKitItem>();
            if (itensDto != null)
            {
                foreach (var itemDto in itensDto)
                {
                    itens.Add(Mapear(itemDto));
                }
            }
            return itens;
        }
    }
}
