﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits.Dto
{
public    class FaturamentoItemQuantidade
    {
        public long ItemId { get; set; }
        public decimal Quantidade { get; set; }
    }
}
