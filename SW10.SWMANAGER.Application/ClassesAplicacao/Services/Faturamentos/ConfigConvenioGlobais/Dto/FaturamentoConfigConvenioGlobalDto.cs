﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ItemConfigs.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.SubGrupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Tabelas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios.Dto
{
    [AutoMap(typeof(FaturamentoConfigConvenioGlobal))]
    public class FaturamentoConfigConvenioGlobalDto : CamposPadraoCRUDDto
    {
        public long? EmpresaId { get; set; }
        public EmpresaDto Empresa { get; set; }
        
        public long? ConvenioId { get; set; }
        public ConvenioDto Convenio { get; set; }
        
        public long? PlanoId { get; set; }
        public PlanoDto Plano { get; set; }
        
        public long? GrupoId { get; set; }
        public FaturamentoGrupoDto Grupo { get; set; }
        
        public long? SubGrupoId { get; set; }
        public FaturamentoSubGrupoDto SubGrupo { get; set; }
        
        //public long? TabelaId { get; set; }
        //public FaturamentoTabelaDto Tabela { get; set; }

        public long? TabelaGlobalId { get; set; }
        public FaturamentoGlobalDto TabelaGlobal { get; set; }
        
        public long? ItemId { get; set; }
        public FaturamentoItemDto Item { get; set; }
        
        public DateTime? DataIncio { get; set; }
        
        public DateTime? DataFim { get; set; }
    }
}
