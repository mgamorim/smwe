﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.SisMoedas;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(SisMoeda))]
    public class SisMoedaDto : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }

        // 1 - fixa
        // 2 - variavel por convenio
        public int Tipo { get; set; }

        public bool IsCobraCoch { get; set; }
    }
}
