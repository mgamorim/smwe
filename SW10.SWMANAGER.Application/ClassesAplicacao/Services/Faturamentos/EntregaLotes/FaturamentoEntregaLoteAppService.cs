#region Usings
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.EntregaLotes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Interfaces;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
#endregion usings.

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas
{
    public class FaturamentoEntregaLoteAppService : SWMANAGERAppServiceBase, IFaturamentoEntregaLoteAppService
    {
        #region Dependencias
        private readonly IRepository<FaturamentoEntregaLote, long> _loteRepository;
        private readonly IRepository<FaturamentoEntregaConta, long> _entregaContaRepository;
        private readonly IRepository<FaturamentoConta, long> _contaRepository;
        private readonly IRepository<FaturamentoContaItem, long> _contaItemRepository;
        private readonly IListarFaturamentoEntregaLotesExcelExporter _listarEntregaLotesExcelExporter;
        private readonly IFaturamentoGrupoAppService _faturamentoGrupoAppService;
        private readonly IFaturamentoItemAppService _faturamentoItemAppService;
        private readonly IContaAppService _fatContaAppService;
        private readonly ITISSAppService _TISSAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FaturamentoEntregaLoteAppService(
            IRepository<FaturamentoEntregaLote, long> loteRepository,
            IRepository<FaturamentoEntregaConta, long> entregaContaRepository,
            IRepository<FaturamentoConta, long> contaRepository,
            IRepository<FaturamentoContaItem, long> contaItemRepository,
            IListarFaturamentoEntregaLotesExcelExporter listarEntregaLotesExcelExporter,
            IFaturamentoGrupoAppService faturamentoGrupoAppService,
            IFaturamentoItemAppService faturamentoItemAppService,
            IContaAppService fatContaAppService,
            ITISSAppService TISSAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _loteRepository = loteRepository;
            _entregaContaRepository = entregaContaRepository;
            _contaRepository = contaRepository;
            _contaItemRepository = contaItemRepository;
            _listarEntregaLotesExcelExporter = listarEntregaLotesExcelExporter;
            _faturamentoGrupoAppService = faturamentoGrupoAppService;
            _faturamentoItemAppService = faturamentoItemAppService;
            _fatContaAppService = fatContaAppService;
            _TISSAppService = TISSAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }
        #endregion dependencias.

        public async Task<PagedResultDto<FaturamentoEntregaLoteDto>> Listar(ListarEntregasInput input)
        {
            var itemrEntregaLotes = 0;
            List<FaturamentoEntregaLote> itens;
            List<FaturamentoEntregaLoteDto> itensDtos = new List<FaturamentoEntregaLoteDto>();
            try
            {
                var query = _loteRepository
                    .GetAll()
                    .Include(c => c.Convenio)
                    .Include(c => c.Convenio.SisPessoa)
                    .Where(c => c.EmpresaId.ToString() == input.EmpresaId)
                    .Where(c => c.ConvenioId.ToString() == input.ConvenioId)
                    ;

                itemrEntregaLotes = await query.CountAsync();

                itens = await query
                    .AsNoTracking()
                    //   .OrderBy(input.Sorting)
                    //   .PageBy(input)
                    .ToListAsync();

                itensDtos = itens.MapTo<List<FaturamentoEntregaLoteDto>>();

                return new PagedResultDto<FaturamentoEntregaLoteDto>(itemrEntregaLotes, itensDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<long> CriarOuEditar(CrudEntregaLoteContaInput input)
        {
            long loteId = 0;
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.ContasIds.Count() == 0)
                        return 0;

                    if (input.LoteId.Equals(0))
                    {
                        input.Lote = new FaturamentoEntregaLoteDto();
                        var novoLote = input.Lote.MapTo<FaturamentoEntregaLote>();
                        novoLote.ConvenioId = input.ConvenioId;
                        novoLote.CodEntregaLote = GerarSequenciaLote(input.ConvenioId);   //input.CodigoEntrega;
                        novoLote.NumeroProcesso = input.NumeroProcesso;
                        novoLote.IsInternacao = input.IsInternacao;
                        novoLote.IsAmbulatorio = input.IsAmbulatorio;
                        novoLote.EmpresaId = input.EmpresaId;

                        // Usuario que gerou o lote
                        var usuario = GetCurrentUser();
                        novoLote.UsuarioLoteId = usuario.Id;

                        loteId = await _loteRepository.InsertAndGetIdAsync(novoLote);
                        var contas = new List<FaturamentoConta>();

                        // talvez essas ids devam ser de 'EntregaConta' e nao de 'ContaMedica'
                        // fazer foreach nas 'EntergaContas' enviadas via ContasIds q deveria ser 'EntregaContasIds'
                        foreach (var contaId in input.ContasIds)
                        {
                            var entrega = _entregaContaRepository.Get(contaId);
                            var conta = _contaRepository.Get((long)entrega.ContaMedicaId);
                            conta.StatusId = 4;
                            entrega.EntregaLoteId = loteId;

                            // Atualizando Status da entrega de cada item das contas
                            var itens = _contaItemRepository.GetAll().Where(x => x.FaturamentoContaId == conta.Id).ToList();
                            foreach (var i in itens)
                            {
                                i.StatusId = 4;
                                _contaItemRepository.Update(i);
                            }

                            await _contaRepository.UpdateAsync(conta);
                            await _entregaContaRepository.UpdateAsync(entrega);
                        }
                        _unitOfWorkManager.Current.SaveChanges();
                        _TISSAppService.GerarLoteXML(loteId);

                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        _unitOfWorkManager.Current.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

            return loteId;
        }

        public async Task Excluir(FaturamentoEntregaLoteDto input)
        {
            try
            {
                var entregas = _entregaContaRepository.GetAll()
                    .Where(e => e.EntregaLoteId == input.Id)
                    .Include(c => c.ContaMedica);

                foreach (var entrega in entregas)
                {
                    //var contaId = entrega.ContaMedicaId;
                    //var conta = _contaRepository.Get((long)contaId);
                    entrega.ContaMedica.StatusId = 3; // Status 3 = 'Entregue'
                    _contaRepository.Update(entrega.ContaMedica);
                }


                await _loteRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoEntregaLoteDto> Obter(long id)
        {
            try
            {
                var query = await _loteRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var item = query
                    .MapTo<FaturamentoEntregaLoteDto>();

                return item;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarEntregasInput input)
        {
            try
            {
                var result = await Listar(input);
                var itens = result.Items;
                return _listarEntregaLotesExcelExporter.ExportToFile(itens.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ListResultDto<FaturamentoEntregaLoteDto>> ListarTodos()
        {
            try
            {
                var query = _loteRepository.GetAll();

                var faturamentoEntregaLotesDto = await query.ToListAsync();

                return new ListResultDto<FaturamentoEntregaLoteDto> { Items = faturamentoEntregaLotesDto.MapTo<List<FaturamentoEntregaLoteDto>>() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoItemDto> faturamentoItensDto = new List<FaturamentoItemDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _loteRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                                //.Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        private string GerarSequenciaLote(long convenioId)
        {
            string sequencia = string.Empty;

            var faturamentoSequenciaLoteRepository = new SWRepository<FaturamentoSequenciaLote>(AbpSession);

            var faturamentoSequenciaLote = faturamentoSequenciaLoteRepository.GetAll()
                                               .Where(w => w.ConvenioId == convenioId)
                                               .FirstOrDefault();

            if (faturamentoSequenciaLote != null)
            {
                var sequenciaLote = ++faturamentoSequenciaLote.Sequencia;

                sequencia = sequenciaLote.ToString().PadLeft(12, '0');


                faturamentoSequenciaLoteRepository.Update(faturamentoSequenciaLote);
            }
            else
            {
                sequencia = "000000000001";

                faturamentoSequenciaLoteRepository.Insert(new FaturamentoSequenciaLote { ConvenioId = convenioId, Sequencia = 1 });
            }

            return sequencia;
        }


    }

}
