﻿using Abp.AutoMapper;
using System.Collections.Generic;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using System;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.Authorization.Users;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto
{
    [AutoMap(typeof(FaturamentoEntregaLote))]
    public class FaturamentoEntregaLoteDto : CamposPadraoCRUDDto
    {
        public long? EmpresaId { get; set; }
        public EmpresaDto Empresa { get; set; }
        
        public long? ConvenioId { get; set; }
        public ConvenioDto Convenio { get; set; }

        public string CodEntregaLote { get; set; }
        public string NumeroProcesso { get; set; }
        
        public DateTime? DataInicial { get; set; }
        public DateTime? DataFinal { get; set; }
        public DateTime? DataEntrega { get; set; }

        public float ValorFatura { get; set; }
        public float ValorTaxas { get; set; }

        public bool IsAmbulatorio { get; set; }
        public bool IsInternacao { get; set; }
        public bool Desativado { get; set; }

        public long? UsuarioLoteId { get; set; }
        public User UsuarioLote { get; set; }

        public string IdentificacaoPrestadorNaOperadora { get; set; }


        public static FaturamentoEntregaLoteDto Mapear(FaturamentoEntregaLote faturamentoEntregaLote)
        {
            FaturamentoEntregaLoteDto faturamentoEntregaLoteDto = new FaturamentoEntregaLoteDto();

            faturamentoEntregaLoteDto.EmpresaId = faturamentoEntregaLote.EmpresaId;
            faturamentoEntregaLoteDto.ConvenioId = faturamentoEntregaLote.ConvenioId;
            faturamentoEntregaLoteDto.CodEntregaLote = faturamentoEntregaLote.CodEntregaLote;
            faturamentoEntregaLoteDto.NumeroProcesso = faturamentoEntregaLote.NumeroProcesso;
            faturamentoEntregaLoteDto.DataInicial = faturamentoEntregaLote.DataInicial;
            faturamentoEntregaLoteDto.DataFinal = faturamentoEntregaLote.DataFinal;
            faturamentoEntregaLoteDto.DataEntrega = faturamentoEntregaLote.DataEntrega;
            faturamentoEntregaLoteDto.ValorFatura = faturamentoEntregaLote.ValorFatura;
            faturamentoEntregaLoteDto.ValorTaxas = faturamentoEntregaLote.ValorTaxas;
            faturamentoEntregaLoteDto.IsAmbulatorio = faturamentoEntregaLote.IsAmbulatorio;
            faturamentoEntregaLoteDto.IsInternacao = faturamentoEntregaLote.IsInternacao;
            faturamentoEntregaLoteDto.Desativado = faturamentoEntregaLote.Desativado;
            faturamentoEntregaLoteDto.UsuarioLoteId = faturamentoEntregaLote.UsuarioLoteId;

            if(faturamentoEntregaLote.Convenio!=null)
            {
                faturamentoEntregaLoteDto.Convenio = ConvenioDto.Mapear(faturamentoEntregaLote.Convenio);
            }
            
            return faturamentoEntregaLoteDto;
        }

    }
}
