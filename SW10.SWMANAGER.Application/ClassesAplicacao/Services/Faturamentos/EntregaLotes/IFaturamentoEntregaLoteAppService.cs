﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.EntregaLotes
{
    public interface IFaturamentoEntregaLoteAppService : IApplicationService
    {
        Task<PagedResultDto<FaturamentoEntregaLoteDto>> Listar(ListarEntregasInput input);

        Task<long> CriarOuEditar(CrudEntregaLoteContaInput input);

        Task Excluir(FaturamentoEntregaLoteDto input);

        Task<FaturamentoEntregaLoteDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarEntregasInput input);
        
        Task<ListResultDto<FaturamentoEntregaLoteDto>> ListarTodos();

        Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput);

      
    }
}

