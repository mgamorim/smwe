﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas
{
    public interface IFaturamentoContaStatusAppService : IApplicationService
    {
        List<FaturamentoContaStatusDto> ListarTodos();
             
    }
}
