﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto
{
    [AutoMap(typeof(FaturamentoContaStatus))]
    public class FaturamentoContaStatusDto : CamposPadraoCRUDDto
    {
        public override string Codigo { get; set; }
        public override string Descricao { get; set; }
        public string Cor { get; set; }


        public static FaturamentoContaStatusDto Mapear(FaturamentoContaStatus faturamentoContaStatus)
        {
            FaturamentoContaStatusDto faturamentoContaStatusDto = new FaturamentoContaStatusDto();

            faturamentoContaStatusDto.Id = faturamentoContaStatus.Id;
            faturamentoContaStatusDto.Codigo = faturamentoContaStatus.Codigo;
            faturamentoContaStatusDto.Descricao = faturamentoContaStatus.Descricao;
            faturamentoContaStatusDto.Cor = faturamentoContaStatus.Cor;

            return faturamentoContaStatusDto;
        }
    }
}
