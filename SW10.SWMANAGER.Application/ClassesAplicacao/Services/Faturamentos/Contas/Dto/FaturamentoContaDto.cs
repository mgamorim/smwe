﻿using Abp.AutoMapper;
//using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposLeito;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLeito.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using System;
using System.Collections.Generic;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto
{
    [AutoMap(typeof(FaturamentoConta))]
    public class FaturamentoContaDto : CamposPadraoCRUDDto
    {
        public string Matricula { get; set; }
        public string CodDependente { get; set; }
        public string NumeroGuia { get; set; }
        public string Titular { get; set; }
        public string GuiaOperadora { get; set; }
        public string GuiaPrincipal { get; set; }
        public string Observacao { get; set; }
        public string SenhaAutorizacao { get; set; }
        public string IdentAcompanhante { get; set; }
        public long? PacienteId { get; set; }
        public PacienteDto Paciente { get; set; }
        public long? MedicoId { get; set; }
        public MedicoDto Medico { get; set; }
        public long? ConvenioId { get; set; }
        public ConvenioDto Convenio { get; set; }
        public long? PlanoId { get; set; }
        public PlanoDto Plano { get; set; }
        // Modelo antigo
        public long? GuiaId { get; set; }
        public GuiaDto Guia { get; set; }
        // Novo modelo
        public long? FatGuiaId { get; set; }
        public FaturamentoGuiaDto FatGuia { get; set; }
        public long? EmpresaId { get; set; }
        public EmpresaDto Empresa { get; set; }
        public long? AtendimentoId { get; set; }
        public AtendimentoDto Atendimento { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }
        public long? TipoAcomodacaoId { get; set; }
        public TipoLeitoDto TipoLeito { get; set; }
        public DateTime? DataIncio { get; set; }
        public DateTime? DataFim { get; set; }
        public DateTime? DataPagamento { get; set; }
        public DateTime? ValidadeCarteira { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public DateTime? DiaSerie1 { get; set; }
        public DateTime? DiaSerie2 { get; set; }
        public DateTime? DiaSerie3 { get; set; }
        public DateTime? DiaSerie4 { get; set; }
        public DateTime? DiaSerie5 { get; set; }
        public DateTime? DiaSerie6 { get; set; }
        public DateTime? DiaSerie7 { get; set; }
        public DateTime? DiaSerie8 { get; set; }
        public DateTime? DiaSerie9 { get; set; }
        public DateTime? DiaSerie10 { get; set; }
        public DateTime? DataEntrFolhaSala { get; set; }
        public DateTime? DataEntrDescCir { get; set; }
        public DateTime? DataEntrBolAnest { get; set; }
        public DateTime? DataEntrCDFilme { get; set; }
        public DateTime? DataValidadeSenha { get; set; }
        public bool IsAutorizador { get; set; }
        public int TipoAtendimento { get; set; }
        public long? StatusId { get; set; }
        public FaturamentoContaStatusDto Status { get; set; }
        public List<FaturamentoContaItemDto> Itens { get; set; }

        public string MotivoPendencia { get; set; }
        
        public DateTime? DataConferencia { get; set; }
        public long? UsuarioConferenciaId { get; set; }
        public User UsuarioConferencia { get; set; }

        public float ValorTotal { get; set; }

        public FaturamentoConta MapearParaBanco ()
        {
            FaturamentoConta fatConta = new FaturamentoConta();
            
            fatConta.Id = this.Id;

            fatConta.DataConferencia = this.DataConferencia;
            fatConta.UsuarioConferenciaId = this.UsuarioConferenciaId;
            fatConta.FatGuiaId = this.FatGuiaId;
            fatConta.Matricula = this.Matricula;
            fatConta.CodDependente = this.CodDependente;
            fatConta.NumeroGuia = this.NumeroGuia;
            fatConta.Titular = this.Titular;
            fatConta.GuiaOperadora = this.GuiaOperadora;
            fatConta.GuiaPrincipal = this.GuiaPrincipal;
            fatConta.Observacao = this.Observacao;
            fatConta.SenhaAutorizacao = this.SenhaAutorizacao;
            fatConta.IdentAcompanhante = this.IdentAcompanhante;
            fatConta.PacienteId = this.PacienteId == 0 ? null : this.PacienteId;
            fatConta.AtendimentoId = this.AtendimentoId == 0 ? null : this.AtendimentoId;
            fatConta.MedicoId = this.MedicoId == 0 ? null : this.MedicoId;
            fatConta.ConvenioId = this.ConvenioId == 0 ? null : this.ConvenioId;
            fatConta.PlanoId = this.PlanoId == 0 ? null : this.PlanoId;
            fatConta.GuiaId = this.GuiaId == 0 ? null : this.GuiaId;
            fatConta.EmpresaId = this.EmpresaId == 0 ? null : this.EmpresaId;
            fatConta.UnidadeOrganizacionalId = this.UnidadeOrganizacionalId == 0 ? null : this.UnidadeOrganizacionalId;
            // fatConta.TipoLeitoId = this.TipoLeitoId == 0 ? null : this.TipoLeitoId;
            fatConta.TipoAcomodacaoId = this.TipoAcomodacaoId == 0 ? null : this.TipoAcomodacaoId;
            fatConta.DataIncio = this.DataIncio;
            fatConta.DataFim = this.DataFim;
            fatConta.DataPagamento = this.DataPagamento;
            fatConta.ValidadeCarteira = this.ValidadeCarteira;
            fatConta.DataAutorizacao = this.DataAutorizacao;
            fatConta.DiaSerie1 = this.DiaSerie1;
            fatConta.DiaSerie2 = this.DiaSerie2;
            fatConta.DiaSerie3 = this.DiaSerie3;
            fatConta.DiaSerie4 = this.DiaSerie4;
            fatConta.DiaSerie5 = this.DiaSerie5;
            fatConta.DiaSerie6 = this.DiaSerie6;
            fatConta.DiaSerie7 = this.DiaSerie7;
            fatConta.DiaSerie8 = this.DiaSerie8;
            fatConta.DiaSerie9 = this.DiaSerie9;
            fatConta.DiaSerie10 = this.DiaSerie10;
            fatConta.DataEntrFolhaSala = this.DataEntrFolhaSala;
            fatConta.DataEntrDescCir = this.DataEntrDescCir;
            fatConta.DataEntrBolAnest = this.DataEntrBolAnest;
            fatConta.DataEntrCDFilme = this.DataEntrCDFilme;
            fatConta.DataValidadeSenha = this.DataValidadeSenha;
            fatConta.IsAutorizador = this.IsAutorizador;
            fatConta.TipoAtendimento = this.TipoAtendimento;
            fatConta.StatusId = this.StatusId;
            fatConta.Id = this.Id;

            

            return fatConta;
        }


        public static FaturamentoContaDto Mapear(FaturamentoConta faturamentoConta)
        {
            FaturamentoContaDto fatConta = new FaturamentoContaDto();

            fatConta.Id = faturamentoConta.Id;
            fatConta.DataConferencia = faturamentoConta.DataConferencia;
            fatConta.UsuarioConferenciaId = faturamentoConta.UsuarioConferenciaId;
            fatConta.FatGuiaId = faturamentoConta.FatGuiaId;
            fatConta.Matricula = faturamentoConta.Matricula;
            fatConta.CodDependente = faturamentoConta.CodDependente;
            fatConta.NumeroGuia = faturamentoConta.NumeroGuia;
            fatConta.Titular = faturamentoConta.Titular;
            fatConta.GuiaOperadora = faturamentoConta.GuiaOperadora;
            fatConta.GuiaPrincipal = faturamentoConta.GuiaPrincipal;
            fatConta.Observacao = faturamentoConta.Observacao;
            fatConta.SenhaAutorizacao = faturamentoConta.SenhaAutorizacao;
            fatConta.IdentAcompanhante = faturamentoConta.IdentAcompanhante;
            fatConta.PacienteId = faturamentoConta.PacienteId == 0 ? null : faturamentoConta.PacienteId;
            fatConta.AtendimentoId = faturamentoConta.AtendimentoId == 0 ? null : faturamentoConta.AtendimentoId;
            fatConta.MedicoId = faturamentoConta.MedicoId == 0 ? null : faturamentoConta.MedicoId;
            fatConta.ConvenioId = faturamentoConta.ConvenioId == 0 ? null : faturamentoConta.ConvenioId;
            fatConta.PlanoId = faturamentoConta.PlanoId == 0 ? null : faturamentoConta.PlanoId;
            fatConta.GuiaId = faturamentoConta.GuiaId == 0 ? null : faturamentoConta.GuiaId;
            fatConta.EmpresaId = faturamentoConta.EmpresaId == 0 ? null : faturamentoConta.EmpresaId;
            fatConta.UnidadeOrganizacionalId = faturamentoConta.UnidadeOrganizacionalId == 0 ? null : faturamentoConta.UnidadeOrganizacionalId;
            // fatConta.TipoLeitoId = faturamentoConta.TipoLeitoId == 0 ? null : faturamentoConta.TipoLeitoId;
            fatConta.TipoAcomodacaoId = faturamentoConta.TipoAcomodacaoId == 0 ? null : faturamentoConta.TipoAcomodacaoId;
            fatConta.DataIncio = faturamentoConta.DataIncio;
            fatConta.DataFim = faturamentoConta.DataFim;
            fatConta.DataPagamento = faturamentoConta.DataPagamento;
            fatConta.ValidadeCarteira = faturamentoConta.ValidadeCarteira;
            fatConta.DataAutorizacao = faturamentoConta.DataAutorizacao;
            fatConta.DiaSerie1 = faturamentoConta.DiaSerie1;
            fatConta.DiaSerie2 = faturamentoConta.DiaSerie2;
            fatConta.DiaSerie3 = faturamentoConta.DiaSerie3;
            fatConta.DiaSerie4 = faturamentoConta.DiaSerie4;
            fatConta.DiaSerie5 = faturamentoConta.DiaSerie5;
            fatConta.DiaSerie6 = faturamentoConta.DiaSerie6;
            fatConta.DiaSerie7 = faturamentoConta.DiaSerie7;
            fatConta.DiaSerie8 = faturamentoConta.DiaSerie8;
            fatConta.DiaSerie9 = faturamentoConta.DiaSerie9;
            fatConta.DiaSerie10 = faturamentoConta.DiaSerie10;
            fatConta.DataEntrFolhaSala = faturamentoConta.DataEntrFolhaSala;
            fatConta.DataEntrDescCir = faturamentoConta.DataEntrDescCir;
            fatConta.DataEntrBolAnest = faturamentoConta.DataEntrBolAnest;
            fatConta.DataEntrCDFilme = faturamentoConta.DataEntrCDFilme;
            fatConta.DataValidadeSenha = faturamentoConta.DataValidadeSenha;
            fatConta.IsAutorizador = faturamentoConta.IsAutorizador;
            fatConta.TipoAtendimento = faturamentoConta.TipoAtendimento;
            fatConta.StatusId = faturamentoConta.StatusId;

            if(faturamentoConta.Status !=null)
            {
                fatConta.Status = FaturamentoContaStatusDto.Mapear(faturamentoConta.Status);
            }

            if(faturamentoConta.Atendimento!=null)
            {
                fatConta.Atendimento = AtendimentoDto.Mapear(faturamentoConta.Atendimento);
            }

            if (faturamentoConta.Empresa != null)
            {
                fatConta.Empresa = EmpresaDto.Mapear(faturamentoConta.Empresa);
            }


            //fatConta.Itens = new List<FaturamentoContaItemDto>();

            //if (faturamentoConta.ContaItens != null)
            //{
            //    foreach (var item in faturamentoConta.ContaItens)
            //    {
            //        // item.FaturamentoConta = null;
            //        fatConta.Itens.Add(FaturamentoContaItemDto.MapearFromCore(item));
            //    }
            //}

            return fatConta;
        }



    }
}
