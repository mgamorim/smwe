﻿#region Usings
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ConfigConvenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ItensTabela;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Tabelas;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ItensTabela.Dto;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Planos;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.VisualASA;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.ItensTabela;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ServicosValidacoes;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos;
#endregion usings.

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas
{
    public class ContaAppService : SWMANAGERAppServiceBase, IContaAppService
    {
        #region Cabecalho
        private readonly IRepository<FaturamentoConta, long> _contaRepository;
        private readonly IRepository<FaturamentoContaItem, long> _contaItemRepository;
        private readonly IListarContasExcelExporter _listarContasExcelExporter;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IFaturamentoContaItemAppService _itemAppService;
        private readonly IFaturamentoConfigConvenioAppService _configConvenioAppService;
        private readonly IFaturamentoTabelaAppService _tabelaAppService;
        private readonly IFaturamentoItemTabelaAppService _tabelaItemAppService;
        private readonly ISisMoedaAppService _moedaAppService;
        private readonly ISisMoedaCotacaoAppService _cotacaoAppService;
        private readonly IFaturamentoItemAppService _fatItemAppService;
        private readonly IEmpresaAppService _empresaAppService;
        private readonly IConvenioAppService _convenioAppService;
        private readonly IMedicoAppService _medicoAppService;
        private readonly IPlanoAppService _planoAppService;
        private readonly IAtendimentoAppService _atendimentoAppService;
        private readonly IGuiaAppService _guiaAppService;
        private readonly IVisualAppService _visualAppService;
        private readonly IRepository<FaturamentoItemTabela, long> _faturamentoItemTabelaRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IRepository<FaturamentoPacote, long> _faturamentoPacoteRepository;

        public ContaAppService(
            IRepository<FaturamentoConta, long> contaRepository,
            IRepository<FaturamentoContaItem, long> contaItemRepository,
            IListarContasExcelExporter listarContasExcelExporter,
            IPacienteAppService pacienteAppService,
            IFaturamentoContaItemAppService itemAppService,
            IFaturamentoConfigConvenioAppService configConvenioAppService,
            IFaturamentoTabelaAppService tabelaAppService,
            IFaturamentoItemTabelaAppService tabelaItemAppService,
            ISisMoedaAppService moedaAppService,
            ISisMoedaCotacaoAppService cotacaoAppService,
            IFaturamentoItemAppService fatItemAppService,
            IEmpresaAppService empresaAppService,
            IConvenioAppService convenioAppService,
            IMedicoAppService medicoAppService,
            IPlanoAppService planoAppService,
            IAtendimentoAppService atendimentoAppService,
            IGuiaAppService guiaAppService,
            IVisualAppService visualAppService,
            IRepository<FaturamentoItemTabela, long> faturamentoItemTabela,
            IRepository<Atendimento, long> atendimentoRepository,
            IRepository<FaturamentoPacote, long> faturamentoPacoteRepository
            )
        {
            _contaRepository = contaRepository;
            _contaItemRepository = contaItemRepository;
            _listarContasExcelExporter = listarContasExcelExporter;
            _pacienteAppService = pacienteAppService;
            _itemAppService = itemAppService;
            _configConvenioAppService = configConvenioAppService;
            _tabelaAppService = tabelaAppService;
            _tabelaItemAppService = tabelaItemAppService;
            _moedaAppService = moedaAppService;
            _cotacaoAppService = cotacaoAppService;
            _fatItemAppService = fatItemAppService;
            _empresaAppService = empresaAppService;
            _convenioAppService = convenioAppService;
            _medicoAppService = medicoAppService;
            _planoAppService = planoAppService;
            _atendimentoAppService = atendimentoAppService;
            _guiaAppService = guiaAppService;
            _visualAppService = visualAppService;
            _faturamentoItemTabelaRepository = faturamentoItemTabela;
            _atendimentoRepository = atendimentoRepository;
            _faturamentoPacoteRepository = faturamentoPacoteRepository;
        }
        #endregion cabecalho.

        public Task<User> ObterUsuarioLogadoAsync()
        {
            return GetCurrentUserAsync();
        }

        public async Task CriarOuEditar(FaturamentoContaDto input)
        {
            try
            {
                using (var unitOfWork = UnitOfWorkManager.Begin())
                {
                    var conta = input.MapearParaBanco();

                    if (input.Id.Equals(0))
                    {
                        conta.StatusId = conta.StatusId ?? 1;

                        if (conta.AtendimentoId != null)
                        {
                            var atendimento = await _atendimentoAppService.Obter((long)conta.AtendimentoId);

                            if (atendimento != null)
                            {
                                conta.EmpresaId = atendimento.EmpresaId;
                                conta.PacienteId = atendimento.PacienteId;
                                conta.NumeroGuia = atendimento.NumeroGuia;
                            }
                        }

                        await _contaRepository.InsertAsync(conta);
                    }
                    else
                    {
                        var ori = await _contaRepository.GetAsync(conta.Id);
                        ori.CodDependente = conta.CodDependente;
                        ori.Codigo = conta.Codigo;
                        ori.ConvenioId = conta.ConvenioId;
                        ori.DataAutorizacao = conta.DataAutorizacao;
                        ori.DataConferencia = conta.DataConferencia;
                        ori.DataEntrBolAnest = conta.DataEntrBolAnest;
                        ori.DataEntrCDFilme = conta.DataEntrCDFilme;
                        ori.DataEntrDescCir = conta.DataEntrDescCir;
                        ori.DataEntrFolhaSala = conta.DataEntrFolhaSala;
                        ori.DataFim = conta.DataFim;
                        ori.DataIncio = conta.DataIncio;
                        ori.DataPagamento = conta.DataPagamento;
                        ori.DataValidadeSenha = conta.DataValidadeSenha;
                        ori.Descricao = conta.Descricao;
                        ori.DiaSerie1 = conta.DiaSerie1;
                        ori.DiaSerie10 = conta.DiaSerie10;
                        ori.DiaSerie2 = conta.DiaSerie2;
                        ori.DiaSerie3 = conta.DiaSerie3;
                        ori.DiaSerie4 = conta.DiaSerie4;
                        ori.DiaSerie5 = conta.DiaSerie5;
                        ori.DiaSerie6 = conta.DiaSerie6;
                        ori.DiaSerie7 = conta.DiaSerie7;
                        ori.DiaSerie8 = conta.DiaSerie8;
                        ori.DiaSerie9 = conta.DiaSerie9;
                        ori.EmpresaId = conta.EmpresaId;
                        ori.FatGuiaId = conta.FatGuiaId;
                        ori.GuiaId = conta.GuiaId;
                        ori.GuiaOperadora = conta.GuiaOperadora;
                        ori.GuiaPrincipal = conta.GuiaPrincipal;
                        ori.IdentAcompanhante = conta.IdentAcompanhante;
                        ori.IsAutorizador = conta.IsAutorizador;
                        ori.IsSistema = conta.IsSistema;
                        ori.Matricula = conta.Matricula;
                        ori.MedicoId = conta.MedicoId;
                        ori.MotivoPendencia = conta.MotivoPendencia;
                        ori.NumeroGuia = conta.NumeroGuia;
                        ori.Observacao = conta.Observacao;
                        ori.PacienteId = conta.PacienteId;
                        ori.PlanoId = conta.PlanoId;
                        ori.SenhaAutorizacao = conta.SenhaAutorizacao;
                        ori.StatusId = conta.StatusId;
                        ori.TipoAcomodacaoId = conta.TipoAcomodacaoId;
                        ori.UnidadeOrganizacionalId = conta.UnidadeOrganizacionalId;
                        ori.UsuarioConferenciaId = conta.UsuarioConferenciaId;
                        ori.ValidadeCarteira = conta.ValidadeCarteira;

                        await _contaRepository.UpdateAsync(ori);

                        await FecharConta(input);
                    }

                    UnitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        async Task FecharConta(FaturamentoContaDto input)
        {
            if (input.DataFim != null)
            {
                ValidacaoFechamentoContaAppService validacaoService = new ValidacaoFechamentoContaAppService(_atendimentoRepository);

                var retorno = validacaoService.Validar(input);

                if (retorno.Errors.Count == 0)
                {
                    var atendimento = await _atendimentoAppService.Obter((long)input.AtendimentoId);

                    if (atendimento != null && (atendimento.DataAlta == null || (atendimento.DataAlta > input.DataFim)))
                    {
                        FaturamentoConta faturamentoConta = new FaturamentoConta();

                        faturamentoConta = input.MapearParaBanco();

                        faturamentoConta.Id = 0;
                        faturamentoConta.DataFim = null;
                        faturamentoConta.DataIncio = ((DateTime)input.DataFim).Date.AddDays(1);

                        var novacontaId = _contaRepository.InsertAndGetId(faturamentoConta);

                        var itensNoPeriodo = _contaItemRepository.GetAll()
                                                                    .Include(i => i.FaturamentoPacote)
                                                                    .Include(i => i.FaturamentoContaKit)
                                                                 .Where(w => w.FaturamentoContaId == input.Id
                                                                         && w.Data > input.DataFim)
                                                                 .ToList();

                        foreach (var item in itensNoPeriodo)
                        {
                            item.FaturamentoContaId = novacontaId;
                            if (item.FaturamentoContaKit != null)
                            {
                                item.FaturamentoContaKit.FaturamentoContaId = novacontaId;
                            }

                            if (item.FaturamentoPacote != null)
                            {
                                item.FaturamentoPacote.FaturamentoContaId = novacontaId;


                                var itensPacote = _contaItemRepository.GetAll()
                                                                      .Where(w => w.FaturamentoPacoteId == item.FaturamentoPacoteId)
                                                                      .ToList();

                                foreach (var itemPacote in itensPacote)
                                {
                                    itemPacote.FaturamentoContaId = novacontaId;
                                }
                            }
                        }
                    }
                }

            }
        }

        public async Task ConferirContas(ConferirContasInput input)
        {
            try
            {
                foreach (var contaId in input.ContasIds)
                {
                    var conta = _contaRepository.Get(contaId);
                    conta.StatusId = 2; // 2 - Conta Conferida

                    var usuario = GetCurrentUser();
                    conta.UsuarioConferenciaId = usuario.Id;

                    // Atualizando Status de entrega dos itens da conta (PRECISA IMPLEMENTAR 'POR PERIODO')
                    var itens = _contaItemRepository.GetAll().Where(x => x.FaturamentoContaId == contaId);

                    foreach (var i in itens)
                    {
                        i.StatusId = 2;
                        await _contaItemRepository.UpdateAsync(i);
                    }

                    await _contaRepository.UpdateAsync(conta);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task EditarComUsuarioConferencia(FaturamentoContaDto input)
        {
            try
            {
                var Conta = input.MapearParaBanco();
                var usuarioLogado = await ObterUsuarioLogadoAsync();
                Conta.UsuarioConferenciaId = usuarioLogado.Id;

                if (input.Id.Equals(0))
                {
                    await _contaRepository.InsertAsync(Conta);
                }
                else
                {
                    await _contaRepository.UpdateAsync(Conta);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task<PagedResultDto<ContaMedicaViewModel>> Listar(ListarContasInput input)
        {
            var contarContas = 0;
            List<FaturamentoConta> contas;
            List<ContaMedicaViewModel> contasDtos = new List<ContaMedicaViewModel>();

            var dataInicial = input.StartDate != null ? ((DateTime)input.StartDate).Date : (DateTime?)null;
            var dataFinal = input.EndDate != null ? ((DateTime)input.EndDate).Date : (DateTime?)null;


            try
            {
                var query = _contaRepository
                    .GetAll()
                    .Include(i => i.Empresa)
                    .Include(i => i.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(i => i.Medico)
                    .Include(i => i.Medico.SisPessoa)
                    .Include(i => i.Plano)
                    .Include(i => i.Atendimento)
                    .Include(i => i.Atendimento.Paciente)
                    .Include(i => i.Atendimento.Paciente.SisPessoa)
                    .Include(i => i.Atendimento.Guia)
                    .Include(i => i.Status)

                    .WhereIf(input.IsEmergencia, x => x.Atendimento.IsAmbulatorioEmergencia)
                    .WhereIf(input.IsInternacao, x => x.Atendimento.IsInternacao)
                    .WhereIf(!string.IsNullOrEmpty(input.EmpresaId.ToString()), e => e.EmpresaId.ToString() == input.EmpresaId.ToString())
                    .WhereIf(!string.IsNullOrEmpty(input.ConvenioId), e => e.ConvenioId.ToString() == input.ConvenioId)
                    .WhereIf(!string.IsNullOrEmpty(input.PacienteId), e => e.Atendimento.PacienteId.ToString() == input.PacienteId)
                    .WhereIf(!string.IsNullOrEmpty(input.MedicoId), e => e.MedicoId.ToString() == input.MedicoId)

                    .Where(_ => _.DataIncio >= dataInicial && _.DataFim <= dataFinal
                       || (_.DataIncio == null && _.DataFim == null)
                       || (_.DataIncio >= dataInicial && _.DataFim == null)
                       || (_.DataFim <= dataFinal && _.DataIncio == null)
                       );

                contarContas = await query.CountAsync();

                contas = await query
                    .AsNoTracking()
                    .ToListAsync();

                foreach (var c in contas)
                {
                    var conta = new ContaMedicaViewModel();

                    conta.Id = c.Id;
                    conta.Matricula = c.Matricula;
                    conta.CodDependente = c.CodDependente;
                    conta.NumeroGuia = c.NumeroGuia;
                    conta.Titular = c.Titular;
                    conta.GuiaOperadora = c.GuiaOperadora;
                    conta.GuiaPrincipal = c.GuiaPrincipal;
                    conta.Observacao = c.Observacao;
                    conta.SenhaAutorizacao = c.SenhaAutorizacao;
                    conta.IdentAcompanhante = c.IdentAcompanhante;

                    if (c.Atendimento != null)
                    {
                        conta.AtendimentoCodigo = c.Atendimento.Codigo;
                        conta.PlanoNome = c.Atendimento.Plano != null ? c.Atendimento.Plano.Descricao : string.Empty;
                        conta.GuiaNumero = c.Atendimento.GuiaNumero;

                        if (c.Atendimento.Paciente != null)
                        {
                            conta.PacienteNome = c.Atendimento.Paciente.NomeCompleto;
                        }
                    }

                    conta.PacienteId = c.PacienteId;
                    conta.MedicoId = c.MedicoId;
                    conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                    conta.ConvenioId = c.ConvenioId;
                    conta.ConvenioNome = c.Convenio != null ? c.Convenio.NomeFantasia : string.Empty;
                    conta.PlanoId = c.PlanoId;
                    conta.GuiaId = c.GuiaId;
                    conta.EmpresaId = c.EmpresaId;
                    conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                    conta.AtendimentoId = c.AtendimentoId;
                    conta.UnidadeOrganizacionalId = c.UnidadeOrganizacionalId;
                    conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                    //conta.TipoLeitoId = c.TipoLeitoId;
                    //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;


                    conta.TipoLeitoId = c.TipoAcomodacaoId;
                    conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;

                    conta.DataIncio = c.DataIncio;
                    conta.DataFim = c.DataFim;
                    conta.DataPagamento = c.DataPagamento;
                    conta.ValidadeCarteira = c.ValidadeCarteira;
                    conta.DataAutorizacao = c.DataAutorizacao;
                    conta.DiaSerie1 = c.DiaSerie1;
                    conta.DiaSerie2 = c.DiaSerie2;
                    conta.DiaSerie3 = c.DiaSerie3;
                    conta.DiaSerie4 = c.DiaSerie4;
                    conta.DiaSerie5 = c.DiaSerie5;
                    conta.DiaSerie6 = c.DiaSerie6;
                    conta.DiaSerie7 = c.DiaSerie7;
                    conta.DiaSerie8 = c.DiaSerie8;
                    conta.DiaSerie9 = c.DiaSerie9;
                    conta.DiaSerie10 = c.DiaSerie10;
                    conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                    conta.DataEntrDescCir = c.DataEntrDescCir;
                    conta.DataEntrBolAnest = c.DataEntrBolAnest;
                    conta.DataEntrCDFilme = c.DataEntrCDFilme;
                    conta.DataValidadeSenha = c.DataValidadeSenha;
                    conta.IsAutorizador = c.IsAutorizador;
                    conta.TipoAtendimento = c.TipoAtendimento;
                    conta.StatusEntregaCodigo = c.Status?.Codigo;
                    conta.StatusEntregaDescricao = c.Status?.Descricao;
                    conta.StatusEntregaCor = c.Status?.Cor;

                    contasDtos.Add(conta);
                }

                return new PagedResultDto<ContaMedicaViewModel>(contarContas, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ContaMedicaViewModel>> ListarParaAtendimento(ListarContasInput input)
        {
            var contarContas = 0;
            List<FaturamentoConta> contas;
            List<ContaMedicaViewModel> contasDtos = new List<ContaMedicaViewModel>();

            try
            {
                var query = _contaRepository.GetAll()
                    .Where(i => i.AtendimentoId.ToString() == input.AtendimentoId)
                    .Include(i => i.Empresa)
                    .Include(i => i.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(i => i.Medico)
                    .Include(i => i.Medico.SisPessoa)
                    .Include(i => i.Plano)
                    .Include(i => i.Atendimento)
                    .Include(i => i.Atendimento.Paciente)
                    .Include(i => i.Atendimento.Plano)
                    .Include(i => i.Atendimento.Paciente.SisPessoa)
                    .Include(i => i.Atendimento.Guia)
                    .Include(i => i.Atendimento.FatGuia)
                    .Include(i => i.Paciente)
                    .Include(i => i.FatGuia)
                    .Include(i => i.Status)
                    .Include(i => i.Atendimento.TipoAcomodacao)
                    .Include(i => i.TipoAcomodacao)
                    .WhereIf(!string.IsNullOrEmpty(input.EmpresaId.ToString()), e => e.EmpresaId.ToString() == input.EmpresaId.ToString())
                    .WhereIf(!string.IsNullOrEmpty(input.ConvenioId), e => e.ConvenioId.ToString() == input.ConvenioId)
                    .WhereIf(!string.IsNullOrEmpty(input.PacienteId), e => e.Atendimento.PacienteId.ToString() == input.PacienteId)
                    .WhereIf(!string.IsNullOrEmpty(input.MedicoId), e => e.MedicoId.ToString() == input.MedicoId)

                    .WhereIf(!input.IgnoraData,
                           _ =>
                           _.DataIncio >= input.StartDate && _.DataFim <= input.EndDate
                       || (_.DataIncio == null && _.DataFim == null)
                       || (_.DataIncio >= input.StartDate && _.DataFim == null)
                       || (_.DataFim <= input.EndDate && _.DataIncio == null)
                        )
                    ;

                contarContas = await query.CountAsync();
                contas = await query.AsNoTracking().ToListAsync();

                foreach (var c in contas)
                {
                    var conta = new ContaMedicaViewModel();

                    conta.Id = c.Id;
                    conta.Matricula = c.Matricula;
                    conta.CodDependente = c.CodDependente;
                    conta.NumeroGuia = c.NumeroGuia;
                    conta.Titular = c.Titular;
                    conta.GuiaOperadora = c.GuiaOperadora;
                    conta.GuiaPrincipal = c.GuiaPrincipal;
                    conta.Observacao = c.Observacao;
                    conta.SenhaAutorizacao = c.SenhaAutorizacao;
                    conta.IdentAcompanhante = c.IdentAcompanhante;

                    if (c.Atendimento != null)
                    {
                        conta.AtendimentoCodigo = c.Atendimento.Codigo;
                        conta.PlanoNome = c.Atendimento.Plano != null ? c.Atendimento.Plano.Descricao : string.Empty;
                        conta.PlanoId = c.Atendimento.Plano != null ? c.Atendimento.Plano.Id : 0;
                        conta.GuiaNumero = c.Atendimento.GuiaNumero;
                        if (c.Atendimento.Paciente != null)
                        {
                            conta.PacienteNome = c.Atendimento.Paciente.NomeCompleto;
                        }
                    }

                    conta.PacienteId = c.PacienteId;
                    conta.MedicoId = c.MedicoId;
                    conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                    conta.ConvenioId = c.ConvenioId;
                    conta.ConvenioNome = c.Convenio != null ? c.Convenio.NomeFantasia : string.Empty;
                    conta.PlanoId = c.PlanoId;
                    conta.GuiaId = c.FatGuia?.Id;
                    conta.FatGuia = c.FatGuia != null ? c.FatGuia : null;
                    conta.FatGuiaId = c.FatGuiaId?.ToString();
                    conta.EmpresaId = c.EmpresaId;
                    conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                    conta.AtendimentoId = c.AtendimentoId;
                    conta.UnidadeOrganizacionalId = c.UnidadeOrganizacionalId;
                    conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                    //conta.TipoLeitoId = c.TipoLeitoId;
                    //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;

                    conta.TipoLeitoId = c.TipoAcomodacaoId;
                    conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;

                    conta.DataIncio = c.DataIncio;
                    conta.DataFim = c.DataFim;
                    conta.DataPagamento = c.DataPagamento;
                    conta.ValidadeCarteira = c.ValidadeCarteira;
                    conta.DataAutorizacao = c.DataAutorizacao;
                    conta.DiaSerie1 = c.DiaSerie1;
                    conta.DiaSerie2 = c.DiaSerie2;
                    conta.DiaSerie3 = c.DiaSerie3;
                    conta.DiaSerie4 = c.DiaSerie4;
                    conta.DiaSerie5 = c.DiaSerie5;
                    conta.DiaSerie6 = c.DiaSerie6;
                    conta.DiaSerie7 = c.DiaSerie7;
                    conta.DiaSerie8 = c.DiaSerie8;
                    conta.DiaSerie9 = c.DiaSerie9;
                    conta.DiaSerie10 = c.DiaSerie10;
                    conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                    conta.DataEntrDescCir = c.DataEntrDescCir;
                    conta.DataEntrBolAnest = c.DataEntrBolAnest;
                    conta.DataEntrCDFilme = c.DataEntrCDFilme;
                    conta.DataValidadeSenha = c.DataValidadeSenha;
                    conta.IsAutorizador = c.IsAutorizador;
                    conta.TipoAtendimento = c.TipoAtendimento;
                    conta.StatusEntregaCodigo = c.Status?.Codigo;
                    conta.StatusEntregaDescricao = c.Status?.Descricao;

                    contasDtos.Add(conta);
                }

                return new PagedResultDto<ContaMedicaViewModel>(contarContas, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ContaMedicaViewModel>> ListarParaExame(ListarContasInput input)
        {
            var contarContas = 0;
            List<FaturamentoConta> contas;
            List<ContaMedicaViewModel> contasDtos = new List<ContaMedicaViewModel>();
            try
            {

                var query = _contaItemRepository
                    .GetAll()
                    .Include(m => m.FaturamentoConta)
                    .Include(m => m.FaturamentoItem)
                    .Where(m => (m.FaturamentoItem.IsRequisicaoExame))
                    .WhereIf(input.EmpresaId.HasValue, e => e.FaturamentoConta.EmpresaId == input.EmpresaId)
                    .WhereIf(!string.IsNullOrEmpty(input.ConvenioId), e => e.FaturamentoConta.ConvenioId.ToString() == input.ConvenioId)
                    .WhereIf(!string.IsNullOrEmpty(input.PacienteId), e => e.FaturamentoConta.Atendimento.PacienteId.ToString() == input.PacienteId)
                    .WhereIf(!string.IsNullOrEmpty(input.MedicoId), e => e.MedicoId.ToString() == input.MedicoId)

                    .WhereIf(!input.IgnoraData,
                           _ =>
                           _.FaturamentoConta.DataIncio >= input.StartDate && _.FaturamentoConta.DataFim <= input.EndDate
                       || (_.FaturamentoConta.DataIncio == null && _.FaturamentoConta.DataFim == null)
                       || (_.FaturamentoConta.DataIncio >= input.StartDate && _.FaturamentoConta.DataFim == null)
                       || (_.FaturamentoConta.DataFim <= input.EndDate && _.FaturamentoConta.DataIncio == null)
                        )

                    .Select(m => m.FaturamentoConta)
                    .Distinct();

                contarContas = await query.CountAsync();
                contas = await query.AsNoTracking().ToListAsync();

                foreach (var c in contas)
                {
                    if (c != null)
                    {
                        if (c.EmpresaId.HasValue)
                        {
                            var empresa = await _empresaAppService.Obter(c.EmpresaId.Value);
                            c.Empresa = empresa.MapTo<Empresa>();
                        }
                        if (c.ConvenioId.HasValue)
                        {
                            var convenio = await _convenioAppService.Obter(c.ConvenioId.Value);
                            c.Convenio = convenio.MapTo<Convenio>();
                        }
                        if (c.MedicoId.HasValue)
                        {
                            var medico = await _medicoAppService.Obter(c.MedicoId.Value);
                            c.Medico = medico.MapTo<Medico>();
                        }
                        if (c.PlanoId.HasValue)
                        {
                            var plano = await _planoAppService.Obter(c.PlanoId.Value);
                            c.Plano = plano.MapTo<Plano>();
                        }
                        if (c.AtendimentoId.HasValue)
                        {
                            var atendimento = await _atendimentoAppService.Obter(c.AtendimentoId.Value);
                            c.Atendimento = atendimento.MapTo<Atendimento>();
                        }
                        if (c.PacienteId.HasValue)
                        {
                            var paciente = await _pacienteAppService.Obter(c.PacienteId.Value);
                            c.Paciente = paciente.MapTo<Paciente>();
                        }
                        if (c.GuiaId.HasValue)
                        {
                            var guia = await _guiaAppService.Obter(c.GuiaId.Value);
                            c.Guia = guia.MapTo<Guia>();
                        }

                        var conta = new ContaMedicaViewModel();

                        conta.Id = c.Id;
                        conta.Matricula = c.Matricula;
                        conta.CodDependente = c.CodDependente;
                        conta.NumeroGuia = c.NumeroGuia;
                        conta.Titular = c.Titular;
                        conta.GuiaOperadora = c.GuiaOperadora;
                        conta.GuiaPrincipal = c.GuiaPrincipal;
                        conta.Observacao = c.Observacao;
                        conta.SenhaAutorizacao = c.SenhaAutorizacao;
                        conta.IdentAcompanhante = c.IdentAcompanhante;

                        if (c.Atendimento != null)
                        {
                            conta.AtendimentoCodigo = c.Atendimento.Codigo;
                            conta.PlanoNome = c.Atendimento.Plano != null ? c.Atendimento.Plano.Descricao : string.Empty;
                            conta.PlanoId = c.Atendimento.Plano != null ? c.Atendimento.Plano.Id : 0;
                            conta.GuiaNumero = c.Atendimento.GuiaNumero;
                            if (c.Atendimento.Paciente != null)
                            {
                                conta.PacienteNome = c.Atendimento.Paciente.NomeCompleto;
                            }
                        }

                        conta.PacienteId = c.PacienteId;
                        conta.MedicoId = c.MedicoId;
                        conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                        conta.ConvenioId = c.ConvenioId;
                        conta.ConvenioNome = c.Convenio != null ? c.Convenio.NomeFantasia : string.Empty;
                        conta.GuiaId = c.FatGuia?.Id;
                        conta.FatGuia = c.FatGuia != null ? c.FatGuia : null;
                        conta.FatGuiaId = c.FatGuiaId.ToString();
                        conta.EmpresaId = c.EmpresaId;
                        conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                        conta.AtendimentoId = c.AtendimentoId;
                        conta.UnidadeOrganizacionalId = c.UnidadeOrganizacionalId;
                        conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                        //conta.TipoLeitoId = c.TipoLeitoId;
                        //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;

                        conta.TipoLeitoId = c.TipoAcomodacaoId;
                        conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;
                        conta.DataIncio = c.DataIncio;
                        conta.DataFim = c.DataFim;
                        conta.DataPagamento = c.DataPagamento;
                        conta.ValidadeCarteira = c.ValidadeCarteira;
                        conta.DataAutorizacao = c.DataAutorizacao;
                        conta.DiaSerie1 = c.DiaSerie1;
                        conta.DiaSerie2 = c.DiaSerie2;
                        conta.DiaSerie3 = c.DiaSerie3;
                        conta.DiaSerie4 = c.DiaSerie4;
                        conta.DiaSerie5 = c.DiaSerie5;
                        conta.DiaSerie6 = c.DiaSerie6;
                        conta.DiaSerie7 = c.DiaSerie7;
                        conta.DiaSerie8 = c.DiaSerie8;
                        conta.DiaSerie9 = c.DiaSerie9;
                        conta.DiaSerie10 = c.DiaSerie10;
                        conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                        conta.DataEntrDescCir = c.DataEntrDescCir;
                        conta.DataEntrBolAnest = c.DataEntrBolAnest;
                        conta.DataEntrCDFilme = c.DataEntrCDFilme;
                        conta.DataValidadeSenha = c.DataValidadeSenha;
                        conta.IsAutorizador = c.IsAutorizador;
                        conta.TipoAtendimento = c.TipoAtendimento;
                        conta.StatusEntregaCodigo = c.Status?.Codigo;
                        conta.StatusEntregaDescricao = c.Status?.Descricao;

                        contasDtos.Add(conta);
                    }
                }

                return new PagedResultDto<ContaMedicaViewModel>(contarContas, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task Excluir(FaturamentoContaDto input)
        {
            try
            {
                await _contaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoContaDto> Obter(long id)
        {
            try
            {
                var query = await _contaRepository
                    .GetAll()
                    .Include(p => p.Medico)
                    .Include(i => i.Medico.SisPessoa)
                    .Include(c => c.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(a => a.Atendimento)
                    .Include(a => a.Atendimento.Paciente)
                    .Include(a => a.Atendimento.Paciente.SisPessoa)
                    .Include(a => a.Atendimento.Guia)
                    .Include(e => e.Empresa)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var conta = FaturamentoContaDto.Mapear(query); //query.MapTo<FaturamentoContaDto>();

                return conta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<long> ObterUltimaContaAtendimentoId(long atendimentoId)
        {
            try
            {
                var query = await _contaRepository
                    .GetAll()
                    .Where(m => m.AtendimentoId == atendimentoId)
                    .FirstOrDefaultAsync();

                var id = query != null ? query.Id : 0;
                return id;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ContaMedicaViewModel> ObterViewModel(long id)
        {
            try
            {
                var c = await _contaRepository
                    .GetAll()
                    .Include(p => p.Medico)
                    .Include(p => p.Status)
                    .Include(i => i.Medico.SisPessoa)
                    .Include(d => d.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(d => d.Plano)
                    .Include(a => a.Atendimento)
                    .Include(a => a.Atendimento.Paciente)
                    .Include(a => a.Atendimento.Paciente.SisPessoa)
                    .Include(a => a.Atendimento.FatGuia)
                    .Include(a => a.Atendimento.Guia)
                    .Include(e => e.Empresa)
                    //.Include(t => t.TipoLeito)
                    .Include(t => t.TipoAcomodacao)
                    .Include(s => s.Status)
                    .Include(s => s.FatGuia)
                    .Include(s => s.ContaItens)
                    .Include(s => s.ContaItens.Select(s2 => s2.FaturamentoItem.Grupo))
                    .Include(s => s.ContaItens.Select(s2 => s2.FaturamentoConfigConvenio))
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var conta = new ContaMedicaViewModel();

                conta.Id = c.Id;
                conta.Matricula = c.Matricula;
                conta.CodDependente = c.CodDependente;
                conta.NumeroGuia = c.NumeroGuia;
                conta.Titular = c.Titular;
                conta.GuiaOperadora = c.GuiaOperadora;
                conta.GuiaPrincipal = c.GuiaPrincipal;
                conta.Observacao = c.Observacao;
                conta.SenhaAutorizacao = c.SenhaAutorizacao;
                conta.IdentAcompanhante = c.IdentAcompanhante;
                conta.PacienteId = c.Atendimento.Paciente.Id;
                conta.PacienteNome = c.Atendimento.Paciente.NomeCompleto;
                conta.AtendimentoId = c.Atendimento.Id;
                conta.MedicoId = c.Medico != null ? c.Medico.Id : 0;
                conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                conta.ConvenioId = c.Convenio != null ? c.Convenio.Id : 0;
                conta.ConvenioNome = c.Convenio != null ? c.Convenio.NomeFantasia : string.Empty;
                conta.PlanoId = c.Atendimento.Plano != null ? c.Atendimento.Plano.Id : 0;
                conta.PlanoNome = c.Atendimento.Plano != null ? c.Atendimento.Plano.Descricao : string.Empty;
                conta.GuiaId = c.Atendimento.FatGuia != null ? c.Atendimento.FatGuia.Id : 0;
                conta.GuiaNumero = c.Atendimento.GuiaNumero;
                conta.FatGuia = c.Atendimento.FatGuia;
                conta.EmpresaId = c.Empresa != null ? c.Empresa.Id : 0;
                conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                conta.AtendimentoCodigo = c.Atendimento.Codigo;
                conta.UnidadeOrganizacionalId = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Id : 0;
                conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                //conta.TipoLeitoId = c.TipoLeito != null ? c.TipoLeito.Id : 0;
                //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;

                conta.TipoLeitoId = c.TipoAcomodacao != null ? c.TipoAcomodacao.Id : 0;
                conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;

                conta.DataIncio = c.DataIncio;
                conta.DataFim = c.DataFim;
                conta.DataPagamento = c.DataPagamento;
                conta.ValidadeCarteira = c.ValidadeCarteira;
                conta.DataAutorizacao = c.DataAutorizacao;
                conta.DiaSerie1 = c.DiaSerie1;
                conta.DiaSerie2 = c.DiaSerie2;
                conta.DiaSerie3 = c.DiaSerie3;
                conta.DiaSerie4 = c.DiaSerie4;
                conta.DiaSerie5 = c.DiaSerie5;
                conta.DiaSerie6 = c.DiaSerie6;
                conta.DiaSerie7 = c.DiaSerie7;
                conta.DiaSerie8 = c.DiaSerie8;
                conta.DiaSerie9 = c.DiaSerie9;
                conta.DiaSerie10 = c.DiaSerie10;
                conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                conta.DataEntrDescCir = c.DataEntrDescCir;
                conta.DataEntrBolAnest = c.DataEntrBolAnest;
                conta.DataEntrCDFilme = c.DataEntrCDFilme;
                conta.DataValidadeSenha = c.DataValidadeSenha;
                conta.IsAutorizador = c.IsAutorizador;
                conta.TipoAtendimento = c.TipoAtendimento;
                conta.StatusEntregaCodigo = c.Status?.Codigo;
                conta.StatusEntregaDescricao = c.Status?.Descricao;
                conta.StatusEntregaId = c.Status?.Id;
                conta.Id = c.Id;


                if (c.ContaItens != null)
                {
                    conta.ContaItensDto = new List<FaturamentoContaItemDto>();

                    foreach (var item in c.ContaItens)
                    {
                        conta.ContaItensDto.Add(FaturamentoContaItemDto.MapearFromCore(item));
                    }
                }

                return conta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ContaMedicaReportModel> ObterReportModel(long id, long atendimentoId = 0)
        {
            try
            {
                var c = await _contaRepository
                    .GetAll()
                    .Include(p => p.Medico)
                    .Include(p => p.Medico.Conselho)
                    .Include(i => i.Medico.SisPessoa)
                    .Include(d => d.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(p => p.Plano)
                    .Include(a => a.Atendimento.Guia)
                    .Include(a => a.Atendimento.FatGuia)
                    .Include(a => a.Atendimento)
                    .Include(a => a.Atendimento.Paciente)
                    .Include(a => a.Atendimento.Paciente.SisPessoa)
                    .Include(a => a.Atendimento.Guia)
                    .Include(e => e.Empresa)
                    //  .Include(t => t.TipoLeito)
                    .Include(t => t.TipoAcomodacao)
                    .Include(t => t.FatGuia)
                    .Include(p => p.Paciente.SisPessoa)
                    .WhereIf(id != 0, m => m.Id == id)
                    .WhereIf(atendimentoId != 0, m => m.Atendimento.Id == atendimentoId)
                    .FirstOrDefaultAsync();

                var conta = new ContaMedicaReportModel();
                if (c != null)
                {
                    conta.Id = c.Id;
                    conta.Matricula = c.Matricula;
                    conta.CodDependente = c.CodDependente;
                    conta.NumeroGuia = c.NumeroGuia;
                    conta.Titular = c.Titular;
                    conta.GuiaOperadora = c.GuiaOperadora;
                    conta.GuiaPrincipal = c.GuiaPrincipal;
                    conta.Observacao = c.Observacao;
                    conta.SenhaAutorizacao = c.SenhaAutorizacao;
                    conta.IdentAcompanhante = c.IdentAcompanhante;
                    conta.PacienteId = c.Atendimento.Paciente.Id;
                    conta.PacienteNome = c.Atendimento.Paciente.SisPessoa.NomeCompleto;
                    conta.PacienteNascimento = string.Format("{0:dd/MM/yyyy}", c.Atendimento.Paciente.Nascimento);
                    conta.AtendimentoId = c.Atendimento.Id;
                    conta.TipoAlta = c.Atendimento?.MotivoAlta?.MotivoAltaTipoAlta?.Descricao;

                    conta.MedicoId = c.Medico != null ? c.Medico.Id : 0;
                    conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                    conta.CRM = c.Medico?.NumeroConselho.ToString();
                    conta.Conselho = c.Medico?.Conselho?.Descricao;

                    conta.ConvenioId = c.Convenio != null ? c.Convenio.Id : 0;
                    conta.ConvenioNome = c.Convenio != null ? c.Convenio.NomeFantasia : string.Empty;
                    conta.PlanoId = c.Plano != null ? c.Plano.Id : 0;
                    conta.EmpresaId = c.Empresa != null ? c.Empresa.Id : 0;
                    conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                    conta.PlanoNome = c.Plano?.Descricao;
                    conta.GuiaId = c.Atendimento.FatGuia != null ? c.Atendimento.FatGuia.Id : 0;
                    conta.GuiaNumero = c.Atendimento.GuiaNumero;
                    conta.AtendimentoCodigo = c.Atendimento.Codigo;
                    conta.UnidadeOrganizacionalId = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Id : 0;
                    conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                    //conta.TipoLeitoId = c.TipoLeito != null ? c.TipoLeito.Id : 0;
                    //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;
                    conta.TipoLeitoId = c.TipoAcomodacao != null ? c.TipoAcomodacao.Id : 0;
                    conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;
                    conta.DataIncio = c.DataIncio;
                    conta.DataFim = c.DataFim;
                    conta.DataPagamento = c.DataPagamento;
                    conta.ValidadeCarteira = c.ValidadeCarteira;
                    conta.DataAutorizacao = c.DataAutorizacao;
                    conta.DiaSerie1 = c.DiaSerie1;
                    conta.DiaSerie2 = c.DiaSerie2;
                    conta.DiaSerie3 = c.DiaSerie3;
                    conta.DiaSerie4 = c.DiaSerie4;
                    conta.DiaSerie5 = c.DiaSerie5;
                    conta.DiaSerie6 = c.DiaSerie6;
                    conta.DiaSerie7 = c.DiaSerie7;
                    conta.DiaSerie8 = c.DiaSerie8;
                    conta.DiaSerie9 = c.DiaSerie9;
                    conta.DiaSerie10 = c.DiaSerie10;
                    conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                    conta.DataEntrDescCir = c.DataEntrDescCir;
                    conta.DataEntrBolAnest = c.DataEntrBolAnest;
                    conta.DataEntrCDFilme = c.DataEntrCDFilme;
                    conta.DataValidadeSenha = c.DataValidadeSenha;
                    conta.IsAutorizador = c.IsAutorizador;
                    conta.TipoAtendimento = c.TipoAtendimento;

                    if (c.Status != null)
                    {
                        conta.StatusEntregaCodigo = c.Status.Codigo;
                        conta.StatusEntregaDescricao = c.Status.Descricao;
                    }

                    conta.Id = c.Id;
                }

                return conta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarContasInput input)
        {
            try
            {
                var result = await Listar(input);
                var contas = result.Items;
                return _listarContasExcelExporter.ExportToFile(null/*contas.ToList()*/);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        //public async Task<float> CalcularValorUnitarioContaItemViaFront(long contaItemId)
        //{
        //    if (contaItemId == 0)
        //        return 0f;

        //    try
        //    {
        //        var contaItem = _contaItemRepository.GetAll()
        //            .Include(x => x.FaturamentoConta)
        //            .Include(x => x.FaturamentoItem)
        //            .Include(x => x.FaturamentoItem.Grupo)
        //            .Include(x => x.FaturamentoItem.SubGrupo)
        //            .FirstOrDefault(x => x.Id == contaItemId);

        //        CalculoContaItemInput input = new CalculoContaItemInput();
        //        input.FatContaItemDto = FaturamentoContaItemDto.MapearFromCore(contaItem);
        //        ContaCalculoItem contaCalculoItem = new ContaCalculoItem();
        //        contaCalculoItem.EmpresaId = (long)input.FatContaItemDto.FaturamentoConta?.EmpresaId;
        //        contaCalculoItem.ConvenioId = (long)input.FatContaItemDto.FaturamentoConta?.ConvenioId;

        //        if (input.FatContaItemDto.FaturamentoConta?.PlanoId != null)
        //        {
        //            contaCalculoItem.PlanoId = (long)input.FatContaItemDto.FaturamentoConta?.PlanoId;
        //        }
        //        input.conta = contaCalculoItem;
        //        ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
        //        configConvenioInput.ConvenioId = contaCalculoItem.ConvenioId;
        //        configConvenioInput.EmpresaId = contaCalculoItem.EmpresaId;
        //        configConvenioInput.PlanoId = contaCalculoItem.PlanoId;
        //        var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);

        //        // por empresa
        //        var configsPorEmpresa = configsConvenio.Items
        //            .Where(x => x.EmpresaId != null)
        //            .Where(c => c.EmpresaId == contaCalculoItem.EmpresaId);

        //        // todas emprsas
        //        var configsTodasEmpresas = configsConvenio.Items
        //            .Where(x => x.EmpresaId == null || x.EmpresaId == 0);

        //        // por plano
        //        var configsPorPlano = configsConvenio.Items
        //            .Where(x => x.PlanoId != null)
        //            .Where(c => c.PlanoId == contaCalculoItem.PlanoId);

        //        // Todos planos
        //        var configsTodosPlanos = configsConvenio.Items
        //            .Where(x => x.PlanoId == null || x.PlanoId == 0);

        //        // por grupo
        //        var configsPorGrupo = configsConvenio.Items
        //            .Where(x => x.GrupoId != null)
        //            .Where(c => c.GrupoId == input.FatContaItemDto.FaturamentoItem.GrupoId);

        //        // todos grupos
        //        var configsTodosGrupos = configsConvenio.Items
        //            .Where(x => x.GrupoId == null || x.GrupoId == 0);

        //        // por subGrupo
        //        var configsPorSubGrupo = configsConvenio.Items
        //            .Where(x => x.GrupoId != null)
        //            .Where(c => c.SubGrupoId == input.FatContaItemDto.FaturamentoItem.SubGrupoId);

        //        // todos subGrupos
        //        var configsTodosSubGrupos = configsConvenio.Items
        //            .Where(x => x.SubGrupoId == null || x.SubGrupoId == 0);

        //        // por item
        //        var configsPorItem = configsConvenio.Items
        //            .Where(x => x.ItemId != null)
        //            .Where(c => c.ItemId == input.FatContaItemDto.FaturamentoItem.Id);

        //        // todos itens
        //        var configsTodosItens = configsConvenio.Items
        //            .Where(x => x.ItemId == null || x.ItemId == 0);

        //        input.configsPorEmpresa = configsPorEmpresa.ToArray();
        //        input.configsTodasEmpresas = configsTodasEmpresas.ToArray();
        //        input.configsPorPlano = configsPorPlano.ToArray();
        //        input.configsTodosPlanos = configsTodosPlanos.ToArray();
        //        input.configsPorGrupo = configsPorGrupo.ToArray();
        //        input.configsTodosGrupos = configsTodosGrupos.ToArray();
        //        input.configsPorSubGrupo = configsPorSubGrupo.ToArray();
        //        input.configsTodosSubGrupos = configsTodosSubGrupos.ToArray();
        //        input.configsPorItem = configsPorItem.ToArray();
        //        input.configsTodosItens = configsTodosItens.ToArray();
        //        input.configsConvenio = configsConvenio.Items.ToList();

        //        return await CalcularValorUnitarioContaItem(input);
        //    }
        //    catch (Exception e)
        //    {
        //        throw new UserFriendlyException(L("ErroCalculoValorUnitario"), e);
        //    }
        //}

        //public async Task<float> CalcularValorUnitarioContaItem(CalculoContaItemInput input)
        //{
        //    try
        //    {
        //        if (input.configsConvenio == null)
        //        {
        //            ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
        //            configConvenioInput.ConvenioId = input.conta.ConvenioId;
        //            configConvenioInput.EmpresaId = input.conta.EmpresaId;
        //            configConvenioInput.PlanoId = input.conta.PlanoId;

        //            var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
        //            input.configsConvenio = configsConvenio.Items.ToList();
        //        }

        //        long? tabelaId;

        //        // Achando tabela adequada via analise combinatoria das 'configConvenio'

        //        // 1 - caso mais especifico
        //        var caso1 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId != null && x.ItemId != 0)
        //            );


        //        // 5
        //        var caso5 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)

        //                  );

        //        // 9
        //        var caso9 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );


        //        // 13
        //        var caso13 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId == null || x.GrupoId == 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );


        //        // 14
        //        var caso14 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId == null || x.GrupoId == 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );





        //        // 2
        //        var caso2 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId != null && x.ItemId != 0)
        //            );
        //        // 3
        //        var caso3 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId != null && x.ItemId != 0)
        //            );
        //        // 4
        //        var caso4 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId != null && x.ItemId != 0)
        //            );


        //        // 6
        //        var caso6 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );
        //        // 7
        //        var caso7 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );
        //        // 8
        //        var caso8 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId != null && x.SubGrupoId != 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );

        //        // 10
        //        var caso10 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId != null && x.EmpresaId != 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );
        //        // 11
        //        var caso11 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );
        //        // 12
        //        var caso12 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId != null && x.GrupoId != 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );


        //        // 15
        //        var caso15 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId != null && x.PlanoId != 0) &&
        //                 (x.GrupoId == null || x.GrupoId == 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );
        //        // 16 - caso mais generico
        //        var caso16 = input.configsConvenio.FirstOrDefault(
        //            x => (x.EmpresaId == null || x.EmpresaId == 0) &&
        //                 (x.PlanoId == null || x.PlanoId == 0) &&
        //                 (x.GrupoId == null || x.GrupoId == 0) &&
        //                 (x.SubGrupoId == null || x.SubGrupoId == 0) &&
        //                 (x.ItemId == null || x.ItemId == 0)
        //            );
        //        // Quanto menor o numero, maior a prioridade (ex: 1 eh mais prioritario que 20)
        //        CasoConfig[] casosConfig =
        //        {
        //            new CasoConfig(1,  caso1),
        //            new CasoConfig(2,  caso2),
        //            new CasoConfig(3,  caso3),
        //            new CasoConfig(4,  caso4),
        //            new CasoConfig(5,  caso5),
        //            new CasoConfig(6,  caso6),
        //            new CasoConfig(7,  caso7),
        //            new CasoConfig(8,  caso8),
        //            new CasoConfig(9,  caso9),
        //            new CasoConfig(10, caso10),
        //            new CasoConfig(11, caso11),
        //            new CasoConfig(12, caso12),
        //            new CasoConfig(13, caso13),
        //            new CasoConfig(14, caso14),
        //            new CasoConfig(15, caso15),
        //            new CasoConfig(16, caso16),
        //        };

        //        List<CasoConfig> casosConfigList = new List<CasoConfig>();

        //        foreach (var cc in casosConfig)
        //        {
        //            if (cc.Config != null)
        //                casosConfigList.Add(cc);
        //        }


        //        // Obtendo config com maior prioridade
        //        var config = casosConfigList.OrderBy(p => p.Prioridade).FirstOrDefault().Config;
        //        tabelaId = config?.TabelaId;





        //        var configAlternativoEmpresa = input.configsConvenio.Where(w => w.EmpresaId != null);

        //        if (configAlternativoEmpresa.Count() > 0)
        //        {
        //            var configAlternativoPlano = configAlternativoEmpresa.Where(w => w.PlanoId != null);

        //            if(configAlternativoPlano.Count()>0)
        //            {

        //            }
        //        }



        //        // fim - via analise combinatoria

        //        // ======================= PRECO =========================

        //        // Obter preco vigente
        //        var listarParaTabelaInput = new ListarFaturamentoItensTabelaInput();
        //        listarParaTabelaInput.TabelaId = tabelaId.ToString();
        //        var precosPorTabela = AsyncHelper.RunSync(() => _tabelaItemAppService.ListarParaFatTabela(listarParaTabelaInput)).Items;
        //        var precosPorFatItem = precosPorTabela.Where(_ => _.ItemId == input.FatContaItemDto.FaturamentoItemId);
        //        var preco = precosPorFatItem
        //            .Where(_ => _.VigenciaDataInicio <= DateTime.Now)
        //            .OrderByDescending(_ => _.VigenciaDataInicio).FirstOrDefault();

        //        // ======================= MOEDA E COTACAO =========================

        //        if (preco == null)
        //            return 0f;

        //        var moeda = AsyncHelper.RunSync(() => _moedaAppService.Obter((long)(preco.SisMoedaId??0)));
        //        ListarSisMoedaCotacoesInput cotacaoInput = new ListarSisMoedaCotacoesInput();
        //        cotacaoInput.Filtro = moeda.Id.ToString();

        //        // Buscar cotacoes por convenio
        //        var cotacoesPorConvenio = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
        //            .Items
        //            .Where(_ => _.ConvenioId == input.conta.ConvenioId)
        //            ;

        //        // Cotacoes por Empresa
        //        var cotacoesPorEmpresa = cotacoesPorConvenio
        //            .Where(_ => _.EmpresaId == input.conta.EmpresaId)
        //            ;

        //        // A cotacao padrao inicial eh por Convenio, se houver configuracao especifica, sera sobrescrita no fluxo abaixo
        //        var cotacao = cotacoesPorConvenio
        //                    .Where(_ => _.DataInicio <= DateTime.Now /*&& _.DataFinal >= DateTime.Now*/)
        //                    .OrderByDescending(_ => _.DataInicio)
        //                    .FirstOrDefault();

        //        // Filtrar cotacoes por Plano
        //        var cotacoesPorPlano = cotacoesPorEmpresa //por que aqui nao eh por convenio?
        //            .Where(x => x.PlanoId != null)
        //            .Where(c => c.PlanoId == input.conta.PlanoId); // se planoId da conta for null (o que nao deveria existir) vai pegar todos com planoId null

        //        // Caso haja cotacao para um plano especifico
        //        if (cotacoesPorPlano.Count() > 0)
        //        {
        //            // Por subGrupo especifico
        //            var cotacaoPorSubGrupo = cotacoesPorPlano
        //                .Where(_ => _.SubGrupoId == input.FatContaItemDto.FaturamentoItem.SubGrupoId && _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now)
        //                //?
        //                .OrderByDescending(_ => _.DataInicio)
        //                .FirstOrDefault();

        //            if (cotacaoPorSubGrupo != null)
        //            {
        //                cotacao = cotacaoPorSubGrupo;
        //            }
        //            else
        //            {
        //                // Por grupo especifico
        //                var cotacaoPorGrupo = cotacoesPorPlano
        //                    .Where(_ => _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now && _.GrupoId == input.FatContaItemDto.FaturamentoItem.GrupoId)
        //                    .OrderByDescending(_ => _.DataInicio)
        //                    .FirstOrDefault();

        //                if (cotacaoPorGrupo != null)
        //                {
        //                    cotacao = cotacaoPorGrupo;
        //                }
        //            }
        //        }
        //        // Caso seja para todos os planos
        //        else
        //        {
        //            // Por subGrupo
        //            var cotacaoPorSubGrupo = cotacoesPorConvenio
        //                .Where(_ => _.DataInicio <= DateTime.Now /*&& _.DataFinal >= DateTime.Now*/)
        //                .OrderByDescending(_ => _.DataInicio)
        //                .FirstOrDefault(_ => _.SubGrupoId == input.FatContaItemDto.FaturamentoItem.SubGrupoId);

        //            if (cotacaoPorSubGrupo != null)
        //            {
        //                cotacao = cotacaoPorSubGrupo;
        //            }
        //            else
        //            {
        //                // Por grupo
        //                var cotacaoPorGrupo = cotacoesPorConvenio
        //                    .Where(_ => _.DataInicio <= DateTime.Now && _.DataFinal >= DateTime.Now)
        //                    .OrderByDescending(_ => _.DataInicio)
        //                    .FirstOrDefault(_ => _.GrupoId == input.FatContaItemDto.FaturamentoItem.GrupoId);

        //                if (cotacaoPorGrupo != null)
        //                {
        //                    cotacao = cotacaoPorGrupo;
        //                }
        //            }

        //        }

        //        // Filme
        //        cotacaoInput.Filtro = "1";//AQUI DEVE SER A ID FIXA DA MOEDA 'FILME' - CRIAR SEED NO EF PARA MOEDAS 'FIXAS' DO SISTEMA
        //        var cotacaoFilme = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
        //                                .Items
        //                                .Where(_ => _.DataInicio <= DateTime.Now)
        //                                .OrderByDescending(_ => _.DataInicio)
        //                                .FirstOrDefault();

        //        var totalFilme = cotacaoFilme?.Valor ?? 0 * input.FatContaItemDto.MetragemFilme;

        //        // Porte
        //        var tabela = await _tabelaAppService.Obter((long)tabelaId);

        //        float totalPorte = 0f;
        //        tabela.IsCBHPM = true;
        //        if (tabela.IsCBHPM)
        //        {
        //            cotacaoInput.Filtro = "5"; // Valor Id fixo setado pelo Seed para UCO
        //            cotacaoInput.ConvenioId = input.conta.ConvenioId.ToString();
        //            cotacaoInput.IsUco = true;
        //            var cotacaoPorte = AsyncHelper.RunSync(() => _cotacaoAppService.ListarPorMoeda(cotacaoInput))
        //                                    .Items
        //                                    .Where(_ => _.DataInicio <= DateTime.Now)
        //                                    .OrderByDescending(_ => _.DataInicio)
        //                                    .FirstOrDefault();

        //            if (!preco.COCH.HasValue)
        //                preco.COCH = 0;

        //            totalPorte = cotacaoPorte?.Valor ?? 0 * (long)preco.COCH;
        //        }

        //        // Valor unitario
        //        var valorUnitario = ((preco.Preco * (cotacao?.Valor ?? 1)) + totalFilme + totalPorte);
        //        return valorUnitario;
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        return 0f;
        //    }
        //}

        public async Task<bool> VerificarCadastroPrecoItem(VerificarCadastroPrecoInput input)
        {
            bool possuiCadastro = false;
            long? tabelaId;

            try
            {
                if (input.configsConvenio == null)
                {
                    ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
                    configConvenioInput.Filtro = input.conta.ConvenioId.ToString();
                    var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                    input.configsConvenio = configsConvenio.Items.ToList();
                }

                // Achando tabela adequada via analise combinatoria das 'configConvenio'

                // 1 - caso mais especifico
                var caso1 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId != null && x.ItemId != 0
                    );

                // 2
                var caso2 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId != null && x.ItemId != 0
                    );


                // 5
                var caso5 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );


                // 3
                var caso3 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId != null && x.ItemId != 0
                    );

                // 4
                var caso4 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId != null && x.ItemId != 0
                    );



                // 6
                var caso6 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 7
                var caso7 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 8
                var caso8 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId != null && x.SubGrupoId != 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 9
                var caso9 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 10
                var caso10 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 11
                var caso11 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 12
                var caso12 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId != null && x.GrupoId != 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 13
                var caso13 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId == null || x.GrupoId == 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 14
                var caso14 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId != null && x.EmpresaId != 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId == null || x.GrupoId == 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 15
                var caso15 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId != null && x.PlanoId != 0 &&
                         x.GrupoId == null || x.GrupoId == 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // 16 - caso mais generico
                var caso16 = input.configsConvenio.FirstOrDefault(
                    x => x.EmpresaId == null || x.EmpresaId == 0 &&
                         x.PlanoId == null || x.PlanoId == 0 &&
                         x.GrupoId == null || x.GrupoId == 0 &&
                         x.SubGrupoId == null || x.SubGrupoId == 0 &&
                         x.ItemId == null || x.ItemId == 0
                    );

                // Quanto menor o numero, maior a prioridade (ex: 1 eh mais prioritario que 20)
                CasoConfig[] casosConfig =
                {
                    new CasoConfig(1,  caso1),
                    new CasoConfig(2,  caso2),
                    new CasoConfig(3,  caso3),
                    new CasoConfig(4,  caso4),
                    new CasoConfig(5,  caso5),
                    new CasoConfig(6,  caso6),
                    new CasoConfig(7,  caso7),
                    new CasoConfig(8,  caso8),
                    new CasoConfig(9,  caso9),
                    new CasoConfig(10, caso10),
                    new CasoConfig(11, caso11),
                    new CasoConfig(12, caso12),
                    new CasoConfig(13, caso13),
                    new CasoConfig(14, caso14),
                    new CasoConfig(15, caso15),
                    new CasoConfig(16, caso16),
                };

                List<CasoConfig> casosConfigList = new List<CasoConfig>();

                foreach (var cc in casosConfig)
                {
                    if (cc.Config != null)
                        casosConfigList.Add(cc);
                }

                // Obtendo config com maior prioridade
                var config = casosConfigList.OrderBy(p => p.Prioridade).FirstOrDefault().Config;
                tabelaId = config?.TabelaId;

                // fim - via analise combinatoria

                // ======================= PRECO =========================

                // Obter preco vigente
                var listarParaTabelaInput = new ListarFaturamentoItensTabelaInput();
                listarParaTabelaInput.TabelaId = tabelaId.ToString();
                var precosPorTabela = AsyncHelper.RunSync(() => _tabelaItemAppService.ListarParaFatTabela(listarParaTabelaInput)).Items;
                var precosPorFatItem = precosPorTabela.Where(_ => _.ItemId == input.FatContaItemDto.FaturamentoItem.Id);
                var preco = precosPorFatItem
                    .Where(_ => _.VigenciaDataInicio <= DateTime.Now)
                    .OrderByDescending(_ => _.VigenciaDataInicio).FirstOrDefault();

                if (preco != null)
                    possuiCadastro = true;

                return possuiCadastro;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }

        public async Task<PagedResultDto<FaturamentoContaItemViewModel>> ListarItensVM(ListarFaturamentoContaItensInput input)
        {
            var contarContaItens = 0;
            List<FaturamentoContaItem> contaItens;
            List<FaturamentoContaItemViewModel> contaItensDtos = new List<FaturamentoContaItemViewModel>();
            try
            {
                var query = _contaItemRepository
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro)
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.FaturamentoConta)
                    .Include(i => i.FaturamentoItem.Grupo)
                    .Include(i => i.FaturamentoItem.Grupo.TipoGrupo)
                    .Include(i => i.Turno)
                    //.Include(t => t.TipoLeito)
                    .Include(t => t.TipoAcomodacao)
                    .Include(u => u.UnidadeOrganizacional)
                    .Include(u => u.FaturamentoConfigConvenio)
                    .Include(u => u.FaturamentoPacote.FaturamentoItem)
                    ;

                contarContaItens = await query.CountAsync();

                contaItens = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();


                ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();

                if (contaItens.Count > 0)
                {
                    configConvenioInput.Filtro = contaItens[0].FaturamentoConta.ConvenioId.ToString();
                    configConvenioInput.ConvenioId = contaItens[0].FaturamentoConta.ConvenioId;
                    configConvenioInput.PlanoId = contaItens[0].FaturamentoConta.PlanoId;
                    configConvenioInput.EmpresaId = contaItens[0].FaturamentoConta.EmpresaId;

                    //configConvenioInput.GrupoId = contaItens[0].FaturamentoItem.GrupoId;
                    //configConvenioInput.SubGrupoId = contaItens[0].FaturamentoItem.SubGrupoId;
                    //configConvenioInput.ItemId = contaItens[0].FaturamentoItemId;

                }

                var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                // Fim - obtencao de config.convenio



                var configsPorEmpresa = configsConvenio.Items
                       .Where(c => c.EmpresaId == contaItens[0].FaturamentoConta.EmpresaId);

                // Filtrar por plano
                var configsPorPlano = configsPorEmpresa
                    .Where(x => x.PlanoId != null)
                    .Where(c => c.PlanoId == contaItens[0].FaturamentoConta.PlanoId);



                foreach (var item in contaItens)
                {
                    if (item.FaturamentoItem == null)
                    {
                        continue;
                    }

                    // Obtendo configuracoes do convenio para calculo de valor dos itens
                    //ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
                    //configConvenioInput.Filtro = item.FaturamentoConta.ConvenioId.ToString();
                    //configConvenioInput.ConvenioId = item.FaturamentoConta.ConvenioId;
                    //configConvenioInput.PlanoId = item.FaturamentoConta.PlanoId;
                    //configConvenioInput.EmpresaId = item.FaturamentoConta.EmpresaId;


                    //var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                    // Fim - obtencao de config.convenio

                    var i = new FaturamentoContaItemViewModel();
                    input.CalculoContaItemInput.FatContaItemDto = new FaturamentoContaItemDto();
                    input.CalculoContaItemInput.FatContaItemDto.Id = item.Id;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem = new Itens.Dto.FaturamentoItemDto();
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.SubGrupoId = item.FaturamentoItem.SubGrupoId;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.GrupoId = item.FaturamentoItem.GrupoId;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.Id = item.FaturamentoItem.Id;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItemId = item.FaturamentoItem.Id;
                    input.CalculoContaItemInput.FatContaItemDto.MetragemFilme = item.MetragemFilme;

                    ///////////////////////////////////////////////////////////
                    // ============== CALCULO DE VALOR UNITARIO ===============
                    // Filtrar por empresa
                    //var configsPorEmpresa = configsConvenio.Items
                    //    .Where(c => c.EmpresaId == item.FaturamentoConta.EmpresaId);

                    //// Filtrar por plano
                    //var configsPorPlano = configsPorEmpresa
                    //    .Where(x => x.PlanoId != null)
                    //    .Where(c => c.PlanoId == item.FaturamentoConta.PlanoId);

                    input.CalculoContaItemInput.configsPorEmpresa = configsPorEmpresa.ToArray();
                    input.CalculoContaItemInput.configsPorPlano = configsPorPlano.ToArray();

                    // Valor manual ou calculado em tempo de execucao
                    i.IsValorItemManual = item.IsValorItemManual;
                    //if (i.IsValorItemManual)
                    //{
                    i.ValorItem = item.ValorItem;

                    //}
                    //else
                    //{
                    //    i.ValorUnitario = await _itemAppService.CalcularValorUnitarioContaItem(input.CalculoContaItemInput);
                    //    i.ValorItem = i.ValorUnitario;
                    //    i.ValorTotal = i.ValorUnitario * item.Qtde;
                    //}




                    //long? tabelaId = null;

                    //if (item.FaturamentoConfigConvenio != null)
                    //{
                    //    tabelaId = item.FaturamentoConfigConvenio.TabelaId;
                    //}
                    //if (!item.IsValorItemManual)
                    //{
                    //    var fatItemTabela = _faturamentoItemTabelaRepository.GetAll()
                    //                            .Where(w => w.ItemId == item.FaturamentoItemId
                    //                                    && w.TabelaId == tabelaId)
                    //                            .FirstOrDefault();
                    //    if (fatItemTabela != null)
                    //    {
                    //        i.ValorItem = fatItemTabela.Preco;
                    //    }
                    //    else
                    //    {
                    //        i.ValorItem = 0;
                    //    }
                    //}


                    i.ValorTotal = i.ValorItem * item.Qtde;


                    i.Id = item.Id;
                    i.Grupo = item.FaturamentoItem.Grupo != null ? item.FaturamentoItem.Grupo.Descricao : string.Empty;
                    i.Tipo = item.FaturamentoItem.Grupo != null ? (item.FaturamentoItem.Grupo.TipoGrupo != null ? item.FaturamentoItem.Grupo.TipoGrupo.Descricao : string.Empty) : string.Empty;
                    i.Descricao = item.FaturamentoItem.Descricao;
                    i.FaturamentoItemId = item.FaturamentoItemId;
                    i.FaturamentoItemDescricao = item.FaturamentoItem != null ? item.FaturamentoItem.Descricao : string.Empty;
                    i.FaturamentoContaId = item.FaturamentoContaId;
                    i.Data = item.Data;
                    i.Qtde = item.Qtde;
                    i.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                    i.UnidadeOrganizacionalDescricao = item.UnidadeOrganizacional != null ? item.UnidadeOrganizacional.Descricao : string.Empty;
                    i.TerceirizadoId = item.TerceirizadoId;
                    i.TerceirizadoDescricao = item.Terceirizado != null ? item.Terceirizado.Codigo : string.Empty;
                    i.CentroCustoId = item.CentroCustoId;
                    i.CentroCustoDescricao = item.CentroCusto != null ? item.CentroCusto.Descricao : string.Empty;
                    i.TurnoId = item.TurnoId;
                    i.TurnoDescricao = item.Turno != null ? item.Turno.Descricao : string.Empty;
                    //i.TipoLeitoId = item.TipoLeitoId;
                    //i.TipoLeitoDescricao = item.TipoLeito != null ? item.TipoLeito.Descricao : string.Empty;
                    i.TipoLeitoId = item.TipoAcomodacaoId;
                    i.TipoLeitoDescricao = item.TipoAcomodacao != null ? item.TipoAcomodacao.Descricao : string.Empty;
                    i.ValorTemp = item.ValorTemp;
                    i.MedicoId = item.MedicoId;
                    i.MedicoNome = item.Medico != null ? item.Medico.NomeCompleto : string.Empty;
                    i.IsMedCredenciado = item.IsMedCredenciado;
                    i.IsGlosaMedico = item.IsGlosaMedico;
                    i.MedicoEspecialidadeId = item.MedicoEspecialidadeId;
                    i.MedicoEspecialidadeNome = item.MedicoEspecialidade != null ? item.MedicoEspecialidade.Especialidade.Nome : string.Empty;
                    i.FaturamentoContaKitId = item.FaturamentoContaKitId;
                    i.IsCirurgia = item.IsCirurgia;
                    i.ValorAprovado = item.ValorAprovado;
                    i.ValorTaxas = item.ValorTaxas;
                    i.HMCH = item.HMCH;
                    i.ValorFilme = item.ValorFilme;
                    i.ValorFilmeAprovado = item.ValorFilmeAprovado;
                    i.ValorCOCH = item.ValorCOCH;
                    i.ValorCOCHAprovado = item.ValorCOCHAprovado;
                    i.Percentual = item.Percentual;
                    i.IsInstrCredenciado = item.IsInstrCredenciado;
                    i.ValorTotalRecuperado = item.ValorTotalRecuperado;
                    i.ValorTotalRecebido = item.ValorTotalRecebido;
                    i.MetragemFilme = item.MetragemFilme;
                    i.MetragemFilmeAprovada = item.MetragemFilmeAprovada;
                    i.COCH = item.COCH;
                    i.COCHAprovado = item.COCHAprovado;
                    // STATUSNOVO ALTERAR          i.StatusEntrega = item.StatusEntrega;
                    i.IsRecuperaMedico = item.IsRecuperaMedico;
                    i.IsAux1Credenciado = item.IsAux1Credenciado;
                    i.IsRecebeAuxiliar1 = item.IsRecebeAuxiliar1;
                    i.IsGlosaAuxiliar1 = item.IsGlosaAuxiliar1;
                    i.IsRecuperaAuxiliar1 = item.IsRecuperaAuxiliar1;
                    i.IsAux2Credenciado = item.IsAux2Credenciado;
                    i.IsRecebeAuxiliar2 = item.IsRecebeAuxiliar2;
                    i.IsGlosaAuxiliar2 = item.IsGlosaAuxiliar2;
                    i.IsRecuperaAuxiliar2 = item.IsRecuperaAuxiliar2;
                    i.IsAux3Credenciado = item.IsAux3Credenciado;
                    i.IsRecebeAuxiliar3 = item.IsRecebeAuxiliar3;
                    i.IsGlosaAuxiliar3 = item.IsGlosaAuxiliar3;
                    i.IsRecuperaAuxiliar3 = item.IsRecuperaAuxiliar3;
                    i.IsRecebeInstrumentador = item.IsRecebeInstrumentador;
                    i.IsGlosaInstrumentador = item.IsGlosaInstrumentador;
                    i.IsRecuperaInstrumentador = item.IsRecuperaInstrumentador;
                    i.Observacao = item.Observacao;
                    i.QtdeRecuperada = item.QtdeRecuperada;
                    i.QtdeAprovada = item.QtdeAprovada;
                    i.QtdeRecebida = item.QtdeRecebida;
                    i.ValorMoedaAprovado = item.ValorMoedaAprovado;
                    i.SisMoedaId = item.SisMoedaId;
                    i.SisMoedaNome = item.SisMoeda != null ? item.SisMoeda.Descricao : string.Empty;
                    i.DataAutorizacao = item.DataAutorizacao;
                    i.SenhaAutorizacao = item.SenhaAutorizacao;
                    i.NomeAutorizacao = item.NomeAutorizacao;
                    i.ObsAutorizacao = item.ObsAutorizacao;
                    i.HoraIncio = item.HoraIncio;
                    i.HoraFim = item.HoraFim;
                    i.ViaAcesso = item.ViaAcesso;
                    i.Tecnica = item.Tecnica;
                    i.ClinicaId = item.ClinicaId;
                    i.FornecedorId = item.FornecedorId;
                    i.FornecedorNome = item.Fornecedor != null ? item.Fornecedor.Descricao : string.Empty;
                    i.NumeroNF = item.NumeroNF;
                    i.IsImportaEstoque = item.IsImportaEstoque;

                    i.IsPacote = item.FaturamentoItem.Grupo.TipoGrupoId == 4;

                    if (item.FaturamentoItem.Grupo.TipoGrupoId != 4)
                    {
                        i.Pacote = item.FaturamentoPacote?.FaturamentoItem?.Descricao;
                    }
                    contaItensDtos.Add(i);
                }

                return new PagedResultDto<FaturamentoContaItemViewModel>(contarContaItens, contaItensDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<float> CalcularTotalConta(CalcularTotalContaInput input)
        {
            float total = 0f;

            foreach (var i in input.Itens)
            {
                total += i.ValorTotal;
            }

            return total;
        }

        // Entrega de contas
        public async Task<PagedResultDto<ContaMedicaViewModel>> ListarNaoConferidas(ListarContasInput input)
        {
            var contarContas = 0;
            List<FaturamentoConta> contas;
            List<ContaMedicaViewModel> contasDtos = new List<ContaMedicaViewModel>();
            try
            {
                string[] per = null;
                DateTime periodoInicio = DateTime.Now;
                DateTime periodoFim = DateTime.Now;

                if (!input.IgnoraData)
                {
                    input.Periodo = input.Periodo.Replace(" ", string.Empty);
                    per = input.Periodo.Split('-');
                    periodoInicio = Convert.ToDateTime(per[0]);
                    periodoFim = Convert.ToDateTime(per[1]);
                    periodoFim = periodoFim.AddHours(23);
                    periodoFim = periodoFim.AddMinutes(59);
                    periodoFim = periodoFim.AddSeconds(59);
                }


                if (input.IsEmergencia)
                {
                    var query = _contaRepository
                   .GetAll()

                   .WhereIf(!input.IgnoraData && input.IsEmergencia,
                       x => x.Atendimento.DataRegistro >= periodoInicio &&
                            x.Atendimento.DataRegistro <= periodoFim
                   )

                   .Include(i => i.Empresa)
                   .Include(i => i.Convenio)
                   .Include(i => i.Convenio.SisPessoa)
                   .Include(i => i.Medico)
                   .Include(i => i.Medico.SisPessoa)
                   .Include(i => i.Plano)
                   .Include(i => i.Atendimento)
                   .Include(i => i.Atendimento.Paciente)
                   .Include(a => a.Atendimento.Paciente.SisPessoa)
                   .Include(i => i.Atendimento.Guia)
                   .Include(i => i.Status)
                   .Where(i => i.Status != null && (i.Status.Codigo == 1.ToString() || i.Status.Codigo == 5.ToString())) // codigo 1 == "Status inicial (nao conferida) - atribuido no Seed"; Status 5 = Pendente
                   .WhereIf(!string.IsNullOrEmpty(input.EmpresaId.ToString()), e => e.EmpresaId.ToString() == input.EmpresaId.ToString())
                   .WhereIf(!string.IsNullOrEmpty(input.ConvenioId), e => e.ConvenioId.ToString() == input.ConvenioId)
                   .WhereIf(!string.IsNullOrEmpty(input.PacienteId), e => e.Atendimento.PacienteId.ToString() == input.PacienteId)
                   .WhereIf(!string.IsNullOrEmpty(input.MedicoId), e => e.MedicoId.ToString() == input.MedicoId)
                   .WhereIf(input.IsEmergencia == true, e => e.Atendimento.IsAmbulatorioEmergencia == true)
                   .WhereIf(input.IsInternacao == true, e => e.Atendimento.IsInternacao == true)
                   ;

                    contarContas = await query.CountAsync();

                    contas = await query
                        .AsNoTracking()
                        .ToListAsync();
                }
                else // IsINternacao
                {
                    List<FaturamentoContaItem> itens = new List<FaturamentoContaItem>();
                    if (!input.IgnoraData)
                    {
                        itens = _contaItemRepository.GetAll()
                           .Where(x => x.Data >= periodoInicio && x.Data <= periodoFim)
                           .ToList();
                    }

                    var query = _contaRepository
                   .GetAll()

                   //.WhereIf(!input.IgnoraData && input.IsEmergencia,
                   //    x => x.Atendimento.DataRegistro >= periodoInicio &&
                   //         x.Atendimento.DataRegistro <= periodoFim
                   //)

                   //.WhereIf(!input.IgnoraData && input.IsInternacao,
                   //    x => FaturamentoConta.PossuiItensNoPeriodo(x, itens, periodoInicio, periodoFim)
                   //)

                   .Include(i => i.Empresa)
                   .Include(i => i.Convenio)
                   .Include(i => i.Convenio.SisPessoa)
                   .Include(i => i.Medico)
                   .Include(i => i.Medico.SisPessoa)
                   .Include(i => i.Plano)
                   .Include(i => i.Atendimento)
                   .Include(i => i.Atendimento.Paciente)
                   .Include(a => a.Atendimento.Paciente.SisPessoa)
                   .Include(i => i.Atendimento.Guia)
                   .Include(i => i.Status)
                   .Where(i => i.Status != null && (i.Status.Codigo == 1.ToString() || i.Status.Codigo == 5.ToString())) // codigo 1 == "Status inicial (nao conferida) - atribuido no Seed"; Status 5 = Pendente
                   .WhereIf(!string.IsNullOrEmpty(input.EmpresaId.ToString()), e => e.EmpresaId.ToString() == input.EmpresaId.ToString())
                   .WhereIf(!string.IsNullOrEmpty(input.ConvenioId), e => e.ConvenioId.ToString() == input.ConvenioId)
                   .WhereIf(!string.IsNullOrEmpty(input.PacienteId), e => e.Atendimento.PacienteId.ToString() == input.PacienteId)
                   .WhereIf(!string.IsNullOrEmpty(input.MedicoId), e => e.MedicoId.ToString() == input.MedicoId)
                   .WhereIf(input.IsEmergencia == true, e => e.Atendimento.IsAmbulatorioEmergencia == true)
                   .WhereIf(input.IsInternacao == true, e => e.Atendimento.IsInternacao == true)
                   ;

                    contarContas = await query.CountAsync();

                    var contasPre = await query
                        .AsNoTracking()
                        .ToListAsync();

                    contas = new List<FaturamentoConta>();// await query.AsNoTracking().ToListAsync();

                    foreach (var c in contasPre)
                    {
                        if (!input.IgnoraData)
                        {
                            if (FaturamentoConta.PossuiItensNoPeriodo(c, itens, periodoInicio, periodoFim))
                            {
                                contas.Add(c);
                            }
                        }
                        else
                        {
                            contas.Add(c);
                        }
                    }

                }

                foreach (var c in contas)
                {
                    var conta = new ContaMedicaViewModel();

                    conta.Id = c.Id;
                    conta.PacienteNome = c.Atendimento.Paciente.SisPessoa?.NomeCompleto;
                    conta.Matricula = c.Matricula;
                    conta.CodDependente = c.CodDependente;
                    conta.NumeroGuia = c.NumeroGuia;
                    conta.Titular = c.Titular;
                    conta.GuiaOperadora = c.GuiaOperadora;
                    conta.GuiaPrincipal = c.GuiaPrincipal;
                    conta.Observacao = c.Observacao;
                    conta.SenhaAutorizacao = c.SenhaAutorizacao;
                    conta.IdentAcompanhante = c.IdentAcompanhante;

                    if (c.Atendimento != null)
                    {
                        conta.AtendimentoCodigo = c.Atendimento.Codigo;
                        conta.PlanoNome = c.Atendimento.Plano != null ? c.Atendimento.Plano.Descricao : string.Empty;
                        conta.GuiaNumero = c.Atendimento.GuiaNumero;
                        if (c.Atendimento.Paciente != null)
                        {
                            conta.PacienteNome = c.Atendimento.Paciente.NomeCompleto;
                        }
                    }

                    conta.PacienteId = c.PacienteId;
                    conta.MedicoId = c.MedicoId;
                    conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                    conta.ConvenioId = c.ConvenioId;
                    conta.ConvenioNome = c.Convenio != null ? c.Convenio.NomeFantasia : string.Empty;
                    conta.PlanoId = c.PlanoId;
                    conta.GuiaId = c.GuiaId;
                    conta.EmpresaId = c.EmpresaId;
                    conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                    conta.AtendimentoId = c.AtendimentoId;
                    conta.UnidadeOrganizacionalId = c.UnidadeOrganizacionalId;
                    conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                    //conta.TipoLeitoId = c.TipoLeitoId;
                    //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;

                    conta.TipoLeitoId = c.TipoAcomodacaoId;
                    conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;
                    conta.DataIncio = c.DataIncio;
                    conta.DataFim = c.DataFim;
                    conta.DataPagamento = c.DataPagamento;
                    conta.ValidadeCarteira = c.ValidadeCarteira;
                    conta.DataAutorizacao = c.DataAutorizacao;
                    conta.DiaSerie1 = c.DiaSerie1;
                    conta.DiaSerie2 = c.DiaSerie2;
                    conta.DiaSerie3 = c.DiaSerie3;
                    conta.DiaSerie4 = c.DiaSerie4;
                    conta.DiaSerie5 = c.DiaSerie5;
                    conta.DiaSerie6 = c.DiaSerie6;
                    conta.DiaSerie7 = c.DiaSerie7;
                    conta.DiaSerie8 = c.DiaSerie8;
                    conta.DiaSerie9 = c.DiaSerie9;
                    conta.DiaSerie10 = c.DiaSerie10;
                    conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                    conta.DataEntrDescCir = c.DataEntrDescCir;
                    conta.DataEntrBolAnest = c.DataEntrBolAnest;
                    conta.DataEntrCDFilme = c.DataEntrCDFilme;
                    conta.DataValidadeSenha = c.DataValidadeSenha;
                    conta.IsAutorizador = c.IsAutorizador;
                    conta.TipoAtendimento = c.TipoAtendimento;
                    conta.StatusEntregaCodigo = c.Status?.Codigo;
                    conta.StatusEntregaDescricao = c.Status?.Descricao;
                    conta.StatusEntregaCor = c.Status?.Cor;

                    contasDtos.Add(conta);
                }

                return new PagedResultDto<ContaMedicaViewModel>(contarContas, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


        public async Task<PagedResultDto<ContaMedicaViewModel>> ListarParaEntrega(ListarContasInput input)
        {
            var contarContas = 0;
            List<FaturamentoConta> contas;
            List<ContaMedicaViewModel> contasDtos = new List<ContaMedicaViewModel>();
            try
            {
                var query = _contaRepository
                    .GetAll()
                    .Include(i => i.Empresa)
                    .Include(i => i.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(i => i.Medico)
                    .Include(i => i.Medico.SisPessoa)
                    .Include(i => i.Plano)
                    .Include(i => i.Atendimento)
                    .Include(i => i.Atendimento.Paciente)
                    .Include(a => a.Atendimento.Paciente.SisPessoa)
                    .Include(i => i.Atendimento.Guia)
                    .Include(s => s.Status)
                    .Include(s => s.UsuarioConferencia)

                    .WhereIf(input.ApenasConferidas, e => e.StatusId == 2)
                    .Where(i => i.StatusId != 3 && i.StatusId != 4)
                    //.WhereIf(input.IsEmergencia, e => e.Atendimento.IsAmbulatorioEmergencia)
                    //.WhereIf(input.IsInternacao, e => e.Atendimento.IsInternacao)
                    .WhereIf(!string.IsNullOrEmpty(input.UsuarioId), e => e.UsuarioConferenciaId.ToString() == input.UsuarioId)
                    .WhereIf(!string.IsNullOrEmpty(input.EmpresaId.ToString()), e => e.EmpresaId.ToString() == input.EmpresaId.ToString())
                    .WhereIf(!string.IsNullOrEmpty(input.ConvenioId), e => e.ConvenioId.ToString() == input.ConvenioId)
                    .WhereIf(!string.IsNullOrEmpty(input.PacienteId), e => e.Atendimento.PacienteId.ToString() == input.PacienteId)
                    .WhereIf(!string.IsNullOrEmpty(input.MedicoId), e => e.MedicoId.ToString() == input.MedicoId)

                    .WhereIf(input.IsEmergencia == true, e => e.Atendimento.IsAmbulatorioEmergencia == true && e.Atendimento.IsInternacao == false)
                    .WhereIf(input.IsInternacao == true, e => e.Atendimento.IsInternacao == true && e.Atendimento.IsAmbulatorioEmergencia == false)
                    ;

                contarContas = await query.CountAsync();

                contas = await query
                    .AsNoTracking()
                    .ToListAsync();

                foreach (var c in contas)
                {
                    var conta = new ContaMedicaViewModel();
                    conta.Id = c.Id;
                    conta.UsuarioConferenciaNome = c.UsuarioConferencia?.Name + " " + c.UsuarioConferencia?.Surname;
                    conta.Matricula = c.Atendimento.Matricula;
                    conta.CodDependente = c.CodDependente;
                    conta.NumeroGuia = c.NumeroGuia;
                    conta.Titular = c.Titular;
                    conta.GuiaOperadora = c.GuiaOperadora;
                    conta.GuiaPrincipal = c.GuiaPrincipal;
                    conta.Observacao = c.Observacao;
                    conta.SenhaAutorizacao = c.SenhaAutorizacao;
                    conta.IdentAcompanhante = c.IdentAcompanhante;

                    if (c.Atendimento != null)
                    {
                        conta.AtendimentoCodigo = c.Atendimento.Codigo;
                        conta.PlanoNome = c.Atendimento.Plano != null ? c.Atendimento.Plano.Descricao : string.Empty;
                        conta.GuiaNumero = c.Atendimento.GuiaNumero;
                        if (c.Atendimento.Paciente != null)
                        {
                            conta.PacienteNome = c.Atendimento.Paciente.NomeCompleto;
                        }
                    }

                    conta.PacienteId = c.PacienteId;
                    conta.MedicoId = c.MedicoId;
                    conta.MedicoNome = c.Medico != null ? c.Medico.NomeCompleto : string.Empty;
                    conta.ConvenioId = c.ConvenioId;
                    conta.ConvenioNome = c.Convenio != null && c.Convenio.SisPessoa != null ? c.Convenio.SisPessoa.NomeFantasia : string.Empty;
                    conta.PlanoId = c.PlanoId;
                    conta.GuiaId = c.GuiaId;
                    conta.EmpresaId = c.EmpresaId;
                    conta.EmpresaNome = c.Empresa != null ? c.Empresa.NomeFantasia : string.Empty;
                    conta.AtendimentoId = c.AtendimentoId;
                    conta.UnidadeOrganizacionalId = c.UnidadeOrganizacionalId;
                    conta.UnidadeOrganizacionalNome = c.UnidadeOrganizacional != null ? c.UnidadeOrganizacional.Descricao : string.Empty;
                    //conta.TipoLeitoId = c.TipoLeitoId;
                    //conta.TipoLeitoDescricao = c.TipoLeito != null ? c.TipoLeito.Descricao : string.Empty;
                    conta.TipoLeitoId = c.TipoAcomodacaoId;
                    conta.TipoLeitoDescricao = c.TipoAcomodacao != null ? c.TipoAcomodacao.Descricao : string.Empty;
                    conta.DataIncio = c.DataIncio;
                    conta.DataFim = c.DataFim;
                    conta.DataPagamento = c.DataPagamento;
                    conta.ValidadeCarteira = c.ValidadeCarteira;
                    conta.DataAutorizacao = c.DataAutorizacao;
                    conta.DiaSerie1 = c.DiaSerie1;
                    conta.DiaSerie2 = c.DiaSerie2;
                    conta.DiaSerie3 = c.DiaSerie3;
                    conta.DiaSerie4 = c.DiaSerie4;
                    conta.DiaSerie5 = c.DiaSerie5;
                    conta.DiaSerie6 = c.DiaSerie6;
                    conta.DiaSerie7 = c.DiaSerie7;
                    conta.DiaSerie8 = c.DiaSerie8;
                    conta.DiaSerie9 = c.DiaSerie9;
                    conta.DiaSerie10 = c.DiaSerie10;
                    conta.DataEntrFolhaSala = c.DataEntrFolhaSala;
                    conta.DataEntrDescCir = c.DataEntrDescCir;
                    conta.DataEntrBolAnest = c.DataEntrBolAnest;
                    conta.DataEntrCDFilme = c.DataEntrCDFilme;
                    conta.DataValidadeSenha = c.DataValidadeSenha;
                    conta.IsAutorizador = c.IsAutorizador;
                    conta.TipoAtendimento = c.TipoAtendimento;
                    conta.StatusEntregaCodigo = c.Status?.Codigo;
                    conta.StatusEntregaDescricao = c.Status?.Descricao;
                    conta.StatusEntregaCor = c.Status?.Cor;
                    contasDtos.Add(conta);
                }

                return new PagedResultDto<ContaMedicaViewModel>(contarContas, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<float> ObterValorTotalConta(long contaId)
        {
            ListarFaturamentoContaItensInput input = new ListarFaturamentoContaItensInput();
            var contarContaItens = 0;
            List<FaturamentoContaItem> contaItens;
            List<FaturamentoContaItemViewModel> contaItensDtos = new List<FaturamentoContaItemViewModel>();
            try
            {
                var query = _contaItemRepository
                    .GetAll()
                    .Where(e => e.FaturamentoContaId == contaId)
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.FaturamentoConta)
                    ;

                contaItens = query.ToList();

                ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();

                if (contaItens.Count > 0)
                {
                    configConvenioInput.Filtro = contaItens[0].FaturamentoConta.ConvenioId.ToString();
                    configConvenioInput.ConvenioId = contaItens[0].FaturamentoConta.ConvenioId;
                    configConvenioInput.PlanoId = contaItens[0].FaturamentoConta.PlanoId;
                    configConvenioInput.EmpresaId = contaItens[0].FaturamentoConta.EmpresaId;

                }

                var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                // Fim - obtencao de config.convenio



                var configsPorEmpresa = configsConvenio.Items
                       .Where(c => c.EmpresaId == contaItens[0].FaturamentoConta.EmpresaId);

                // Filtrar por plano
                var configsPorPlano = configsPorEmpresa
                    .Where(x => x.PlanoId != null)
                    .Where(c => c.PlanoId == contaItens[0].FaturamentoConta.PlanoId);



                foreach (var item in contaItens)
                {
                    if (item.FaturamentoItem == null)
                    {
                        continue;
                    }

                    // Obtendo configuracoes do convenio para calculo de valor dos itens
                    //ListarFaturamentoConfigConveniosInput configConvenioInput = new ListarFaturamentoConfigConveniosInput();
                    //configConvenioInput.Filtro = item.FaturamentoConta.ConvenioId.ToString();
                    //configConvenioInput.ConvenioId = item.FaturamentoConta.ConvenioId;
                    //configConvenioInput.PlanoId = item.FaturamentoConta.PlanoId;
                    //configConvenioInput.EmpresaId = item.FaturamentoConta.EmpresaId;


                    //var configsConvenio = await _configConvenioAppService.ListarPorConvenio(configConvenioInput);
                    // Fim - obtencao de config.convenio

                    var i = new FaturamentoContaItemViewModel();

                    input.CalculoContaItemInput = new CalculoContaItemInput();

                    input.CalculoContaItemInput.FatContaItemDto = new FaturamentoContaItemDto();
                    input.CalculoContaItemInput.FatContaItemDto.Id = item.Id;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem = new Itens.Dto.FaturamentoItemDto();
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.SubGrupoId = item.FaturamentoItem.SubGrupoId;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.GrupoId = item.FaturamentoItem.GrupoId;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItem.Id = item.FaturamentoItem.Id;
                    input.CalculoContaItemInput.FatContaItemDto.FaturamentoItemId = item.FaturamentoItem.Id;
                    input.CalculoContaItemInput.FatContaItemDto.MetragemFilme = item.MetragemFilme;

                    ///////////////////////////////////////////////////////////
                    // ============== CALCULO DE VALOR UNITARIO ===============
                    // Filtrar por empresa
                    //var configsPorEmpresa = configsConvenio.Items
                    //    .Where(c => c.EmpresaId == item.FaturamentoConta.EmpresaId);

                    //// Filtrar por plano
                    //var configsPorPlano = configsPorEmpresa
                    //    .Where(x => x.PlanoId != null)
                    //    .Where(c => c.PlanoId == item.FaturamentoConta.PlanoId);

                    input.CalculoContaItemInput.configsPorEmpresa = configsPorEmpresa.ToArray();
                    input.CalculoContaItemInput.configsPorPlano = configsPorPlano.ToArray();

                    // Valor manual ou calculado em tempo de execucao
                    i.IsValorItemManual = item.IsValorItemManual;
                    if (i.IsValorItemManual)
                    {
                        i.ValorItem = item.ValorItem;
                        i.ValorTotal = i.ValorItem * item.Qtde;
                    }
                    else
                    {
                        input.CalculoContaItemInput.conta = new ContaCalculoItem();

                        input.CalculoContaItemInput.conta.EmpresaId = (long)contaItens[0].FaturamentoConta.EmpresaId;
                        input.CalculoContaItemInput.conta.ConvenioId = (long)contaItens[0].FaturamentoConta.ConvenioId;
                        input.CalculoContaItemInput.conta.PlanoId = (long)contaItens[0].FaturamentoConta.PlanoId;


                        i.ValorUnitario = await _itemAppService.CalcularValorUnitarioContaItem(input.CalculoContaItemInput);
                        i.ValorItem = i.ValorUnitario;
                        i.ValorTotal = i.ValorUnitario * item.Qtde;
                    }





                    contaItensDtos.Add(i);
                }

                return 0;// contaItensDtos.Sum(s => s.ValorTotal);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }





            return 0f;
        }

        public async Task<float> ObterValorContaRegistrado(long contaId)
        {

            float valorTotal = 0;
            try
            {

                var itensConta = _contaItemRepository.GetAll()
                                                     .Include(i => i.FaturamentoConfigConvenio)
                                                     .Where(w => w.FaturamentoContaId == contaId
                                                              && (w.FaturamentoPacoteId == null || w.FaturamentoItem.Grupo.TipoGrupoId == 4))
                                                     .ToList();


                foreach (var item in itensConta)
                {
                    //var tabelaId = item.FaturamentoConfigConvenio?.TabelaId;


                    //var tabelaItem = _faturamentoItemTabelaRepository.GetAll()
                    //                                                 .Where(w => w.TabelaId == tabelaId
                    //                                                            && w.ItemId == item.FaturamentoItemId)
                    //                                                 .FirstOrDefault();

                    //if (tabelaItem != null)
                    //{
                    //    valorTotal += tabelaItem.Preco;
                    //}

                    valorTotal += (item.ValorItem * item.Qtde);

                }
            }
            catch (Exception ex)
            {

            }

            return valorTotal;
        }

        public async Task RecalcularValores(long contaId)
        {
            var itensConta = _contaItemRepository.GetAll()
                                                 .Where(w => w.FaturamentoContaId == contaId)
                                                 .ToList();

            foreach (var item in itensConta)
            {
               var valorTabela = await _itemAppService.CalcularValorItemFaturamento(contaId, (long)item.FaturamentoItemId);
                item.ValorItem = valorTabela.Valor;
                item.FaturamentoItemCobradoId = valorTabela.FaturamentoItemCobradoId;
            }
        }

    }

    public class CalcularTotalContaInput
    {
        public FaturamentoContaItemViewModel[] Itens { get; set; }
    }

    public class ContaCalculoItem
    {
        public long ConvenioId { get; set; }
        public long EmpresaId { get; set; }
        public long PlanoId { get; set; }
    }

    public class CalculoContaItemInput
    {
        public ContaCalculoItem conta { get; set; }
        public FaturamentoContaItemDto FatContaItemDto { get; set; }
        public List<FaturamentoConfigConvenioDto> configsConvenio { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorEmpresa { get; set; }
        public FaturamentoConfigConvenioDto[] configsTodasEmpresas { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorPlano { get; set; }
        public FaturamentoConfigConvenioDto[] configsTodosPlanos { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorGrupo { get; set; }
        public FaturamentoConfigConvenioDto[] configsTodosGrupos { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorSubGrupo { get; set; }
        public FaturamentoConfigConvenioDto[] configsTodosSubGrupos { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorItem { get; set; }
        public FaturamentoConfigConvenioDto[] configsTodosItens { get; set; }

        public string TabelaUilizada { get; set; }
        public long? FaturamentoConfigConvenioId { get; set; }
        public long? FaturamentoItemCobradoId { get; set; }
    }

    public class VerificarCadastroPrecoInput
    {
        public ContaCalculoItem conta { get; set; }
        public FaturamentoContaItemDto FatContaItemDto { get; set; }
        public List<FaturamentoConfigConvenioDto> configsConvenio { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorPlano { get; set; }
        public FaturamentoConfigConvenioDto[] configsPorEmpresa { get; set; }
    }

    public class ContaMedicaViewModel
    {
        public long Id { get; set; }
        public string UsuarioConferenciaNome { get; set; }
        public string Matricula { get; set; }
        public string CodDependente { get; set; }
        public string NumeroGuia { get; set; }
        public string Titular { get; set; }
        public string GuiaOperadora { get; set; }
        public string GuiaPrincipal { get; set; }
        public string Observacao { get; set; }
        public string SenhaAutorizacao { get; set; }
        public string IdentAcompanhante { get; set; }
        public string PacienteNome { get; set; }
        public string MedicoNome { get; set; }
        public string ConvenioNome { get; set; }
        public string PlanoNome { get; set; }
        public string GuiaNumero { get; set; }
        public string EmpresaNome { get; set; }
        public string AtendimentoCodigo { get; set; }
        public string UnidadeOrganizacionalNome { get; set; }
        public string TipoLeitoDescricao { get; set; }
        public string StatusEntregaCodigo { get; set; }
        public string StatusEntregaDescricao { get; set; }
        public string StatusEntregaCor { get; set; }
        public string FatGuiaId { get; set; }
        public long? PacienteId { get; set; }
        public long? MedicoId { get; set; }
        public long? ConvenioId { get; set; }
        public long? PlanoId { get; set; }
        // public long? TipoLeitoId { get; set; }
        public long? TipoLeitoId { get; set; }
        public long? GuiaId { get; set; }
        public long? EmpresaId { get; set; }
        public long? AtendimentoId { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public long? StatusEntregaId { get; set; }
        public bool IsAutorizador { get; set; }
        public int TipoAtendimento { get; set; }
        public DateTime? DataIncio { get; set; }
        public DateTime? DataFim { get; set; }
        public DateTime? DataPagamento { get; set; }
        public DateTime? ValidadeCarteira { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public DateTime? DiaSerie1 { get; set; }
        public DateTime? DiaSerie2 { get; set; }
        public DateTime? DiaSerie3 { get; set; }
        public DateTime? DiaSerie4 { get; set; }
        public DateTime? DiaSerie5 { get; set; }
        public DateTime? DiaSerie6 { get; set; }
        public DateTime? DiaSerie7 { get; set; }
        public DateTime? DiaSerie8 { get; set; }
        public DateTime? DiaSerie9 { get; set; }
        public DateTime? DiaSerie10 { get; set; }
        public DateTime? DataEntrFolhaSala { get; set; }
        public DateTime? DataEntrDescCir { get; set; }
        public DateTime? DataEntrBolAnest { get; set; }
        public DateTime? DataEntrCDFilme { get; set; }
        public DateTime? DataValidadeSenha { get; set; }
        public FaturamentoGuia FatGuia { get; set; }

        public List<FaturamentoContaItemDto> ContaItensDto { get; set; }




    }

    public class ContaMedicaReportModel
    {
        public long Id { get; set; }
        public string Matricula { get; set; }
        public string CodDependente { get; set; }
        public string NumeroGuia { get; set; }
        public string Titular { get; set; }
        public string GuiaOperadora { get; set; }
        public string GuiaPrincipal { get; set; }
        public string Observacao { get; set; }
        public string SenhaAutorizacao { get; set; }
        public string IdentAcompanhante { get; set; }
        public string PacienteNome { get; set; }
        public string PacienteNascimento { get; set; }
        public string MedicoNome { get; set; }
        public string ConvenioNome { get; set; }
        public string GuiaNumero { get; set; }
        public string PlanoNome { get; set; }
        public string EmpresaNome { get; set; }
        public string AtendimentoCodigo { get; set; }
        public string UnidadeOrganizacionalNome { get; set; }
        public string TipoLeitoDescricao { get; set; }
        public string StatusEntregaCodigo { get; set; }
        public string StatusEntregaDescricao { get; set; }
        public long? MedicoId { get; set; }
        public long? PacienteId { get; set; }
        public long? ConvenioId { get; set; }
        public long? PlanoId { get; set; }
        public long? GuiaId { get; set; }
        public long? EmpresaId { get; set; }
        public long? AtendimentoId { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public long? TipoLeitoId { get; set; }
        public bool IsAutorizador { get; set; }
        public int TipoAtendimento { get; set; }
        public DateTime? DataIncio { get; set; }
        public DateTime? DataFim { get; set; }
        public DateTime? DataPagamento { get; set; }
        public DateTime? ValidadeCarteira { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public DateTime? DiaSerie1 { get; set; }
        public DateTime? DiaSerie2 { get; set; }
        public DateTime? DiaSerie3 { get; set; }
        public DateTime? DiaSerie4 { get; set; }
        public DateTime? DiaSerie5 { get; set; }
        public DateTime? DiaSerie6 { get; set; }
        public DateTime? DiaSerie7 { get; set; }
        public DateTime? DiaSerie8 { get; set; }
        public DateTime? DiaSerie9 { get; set; }
        public DateTime? DiaSerie10 { get; set; }
        public DateTime? DataEntrFolhaSala { get; set; }
        public DateTime? DataEntrDescCir { get; set; }
        public DateTime? DataEntrBolAnest { get; set; }
        public DateTime? DataEntrCDFilme { get; set; }
        public DateTime? DataValidadeSenha { get; set; }
        public string CRM { get; set; }
        public string TipoAlta { get; set; }
        public string Conselho { get; set; }
    }

    public class CasoConfig
    {
        public int Prioridade { get; set; }
        public FaturamentoConfigConvenioDto Config { get; set; }

        public CasoConfig(int prioridade, FaturamentoConfigConvenioDto config)
        {
            Prioridade = prioridade;
            Config = config;
        }
    }

    public class ConferirContasInput
    {
        public long[] ContasIds { get; set; }
    }
}
