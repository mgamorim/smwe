﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Interfaces;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03.GuiasLotes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03
{
    public class OperacoesV3_03_03AppService : SWMANAGERAppServiceBase, IOperacoesV3_03_03AppService
    {

        public string GerarLoteXML(long loteId)
        {
            var _faturamentoEntregaLoteRepository = new SWRepository<FaturamentoEntregaLote>();

            var entregaLote = _faturamentoEntregaLoteRepository.GetAll()
                                                                .Where(w => w.Id == loteId)
                                                                .Include(i=> i.Convenio)
                                                                .FirstOrDefault();

            var entregaLoteDto = FaturamentoEntregaLoteDto.Mapear(entregaLote);

            var _identificacaoPrestadorNaOperadoraRepository = new SWRepository<IdentificacaoPrestadorNaOperadora>();

            var identificacao  = _identificacaoPrestadorNaOperadoraRepository.GetAll()
                                                                     .Where(w => w.ConvenioId == entregaLote.ConvenioId
                                                                             && w.EmpresaId == entregaLote.EmpresaId)
                                                                     .FirstOrDefault();

            if(identificacao!=null)
            {
                entregaLoteDto.IdentificacaoPrestadorNaOperadora = identificacao.Descricao;
            }

            string xml;

            mensagemTISS mensagemTISS = new mensagemTISS();

            

            CabecalhoLotesV3_03_03Service cabecalhoLotesService = new CabecalhoLotesV3_03_03Service();
            mensagemTISS.cabecalho = cabecalhoLotesService.CriarCabecalho(entregaLoteDto);

            GuiasLotesV3_03_03Service guiasLotesService = new GuiasLotesV3_03_03Service();

            prestadorOperadora prestadorOperadora = new prestadorOperadora();


            prestadorOperadora.ItemElementName = ItemChoiceType8.loteGuias;
            prestadorOperadora.Item = guiasLotesService.GerarGuiasLote(loteId, identificacao.Descricao);
            mensagemTISS.Item = prestadorOperadora;


            var xmlserializer = new XmlSerializer(typeof(mensagemTISS));
            var stringWriter = new StringWriter();
            //using (var writer = XmlWriter.Create(stringWriter))
            //{
            //    xmlserializer.Serialize(writer, mensagemTISS);
            //    xml= stringWriter.ToString();
            //}

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("ans", "http://www.ans.gov.br/padroes/tiss/schemas");
            


            var encoding = Encoding.GetEncoding("ISO-8859-1");
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = encoding
            };

            using (var stream = new MemoryStream())
            {
                using (var xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                {
                    xmlserializer.Serialize(xmlWriter, mensagemTISS, ns);
                }
                xml= encoding.GetString(stream.ToArray());
            }


            return xml;
        }


        private prestadorOperadora GerarItemLote(long loteId)
        {
            prestadorOperadora prestadorOperadora = new prestadorOperadora();

            prestadorOperadora.Item = new ctm_guiaLote();

            ((ctm_guiaLote)prestadorOperadora.Item).numeroLote = "1234132423";

            return prestadorOperadora;
        }
        
    }
}
