﻿using Castle.Components.DictionaryAdapter;
using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.ItensTabela;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProfissionaisSaude;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03.GuiasLotes
{
    public class GuiaSPSADTV3_03_03Service : GuiaV3_03_03Service
    {
        private SWRepository<FaturamentoItemTabela> _faturamentoItemTabelaRepository;
        private SWRepository<FaturamentoContaItem> _faturamentoContaItemRepository;

        public ctm_spsadtGuia[] GerarGuiaSPSADT(List<FaturamentoEntregaContaDto> faturamentoEntregaConta, string codigoPrestadorNaOperadora)
        {
            _faturamentoContaItemRepository = new SWRepository<FaturamentoContaItem>();

            ctm_spsadtGuia[] ctm_spsadtGuia = new ctm_spsadtGuia[faturamentoEntregaConta.Count];

            int posicao = 0;

            foreach (var item in faturamentoEntregaConta)
            {
                var faturamentoConta = item.ContaMedica;


                var itensConta = _faturamentoContaItemRepository.GetAll()
                                                               .Where(w => w.FaturamentoContaId == item.ContaMedicaId)
                                                                 .Include(i => i.FaturamentoItem)
                                                                 .Include(s => s.FaturamentoItem.Grupo)
                                                                 .Include(s => s.FaturamentoItem.Grupo.FaturamentoCodigoDespesa)
                                                                 .Include(s => s.Anestesista)
                                                                 .Include(s => s.Anestesista.SisPessoa)
                                                                 .Include(s => s.Anestesista.Conselho)
                                                                 .Include(s => s.AnestesistaEspecialidade)
                                                                 .Include(s => s.AnestesistaEspecialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Auxiliar1)
                                                                 .Include(s => s.Auxiliar1.SisPessoa)
                                                                 .Include(s => s.Auxiliar1.Conselho)
                                                                 .Include(s => s.Auxiliar1Especialidade)
                                                                 .Include(s => s.Auxiliar1Especialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Auxiliar2)
                                                                 .Include(s => s.Auxiliar2.SisPessoa)
                                                                 .Include(s => s.Auxiliar2.Conselho)
                                                                 .Include(s => s.Auxiliar2Especialidade)
                                                                 .Include(s => s.Auxiliar2Especialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Auxiliar3)
                                                                 .Include(s => s.Auxiliar3.SisPessoa)
                                                                 .Include(s => s.Auxiliar3.Conselho)
                                                                 .Include(s => s.Auxiliar3Especialidade)
                                                                 .Include(s => s.Auxiliar3Especialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Instrumentador)
                                                                 .Include(s => s.Instrumentador.SisPessoa)
                                                                 .Include(s => s.Instrumentador.Conselho)
                                                                 .Include(s => s.InstrumentadorEspecialidade)
                                                                 .Include(s => s.InstrumentadorEspecialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Medico)
                                                                 .Include(s => s.Medico.SisPessoa)
                                                                 .Include(s => s.Medico.Conselho)
                                                                 .Include(s => s.MedicoEspecialidade)
                                                                 .Include(s => s.MedicoEspecialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.FaturamentoConfigConvenio);

                var itensContaList = itensConta.ToList();


                List<FaturamentoContaItemDto> itensContaListDto = new List<FaturamentoContaItemDto>();

                foreach (var itemConta in itensContaList)
                {
                    itensContaListDto.Add(FaturamentoContaItemDto.MapearFromCore(itemConta));
                }

                faturamentoConta.Itens = itensContaListDto;

                ctm_spsadtGuia spsadtGuia = new ctm_spsadtGuia();

                var _atendimentoRepository = new SWRepository<Atendimento>();

                spsadtGuia.cabecalhoGuia = (ctm_spsadtGuiaCabecalhoGuia)GerarCabecalhoGuia(faturamentoConta);
                spsadtGuia.dadosAtendimento = GerarAtendimento(faturamentoConta.Atendimento);
                spsadtGuia.dadosBeneficiario = GerarBeneficiarioDados(faturamentoConta.Atendimento);
                spsadtGuia.dadosAutorizacao = GerarAutorizacaoSADT(faturamentoConta.Atendimento);
                spsadtGuia.valorTotal = GerarValorTotal(faturamentoConta);
                spsadtGuia.outrasDespesas = GerarListaOutrasDespesas(faturamentoConta);
                spsadtGuia.procedimentosExecutados = GerarProcedimentosExecutados(faturamentoConta);

                spsadtGuia.dadosSolicitante = GerarDadosSolicitante(faturamentoConta, codigoPrestadorNaOperadora);

                ctm_spsadtGuia[posicao++] = spsadtGuia;

                //spsadtGuia.dadosSolicitante
                //    spsadtGuia.dadosSolicitacao
                //    spsadtGuia.

            }

            return ctm_spsadtGuia;
        }


        private ctm_spsadtAtendimento GerarAtendimento(AtendimentoDto atendimento)
        {
            var dadosAtendimento = new ctm_spsadtAtendimento();

            if (atendimento.IsAmbulatorioEmergencia)
            {
                dadosAtendimento.tipoAtendimento = FuncoesGlobais.ObterValueEnumType<dm_tipoAtendimento>(atendimento.AtendimentoTipo.TabelaDominio.Codigo, false);
            }


            //Verificar a possibilidade d criar seed
            if (atendimento.MotivoAlta !=null && atendimento.MotivoAlta.MotivoAltaTipoAlta!=null && atendimento.MotivoAlta.MotivoAltaTipoAlta.Codigo == "002")
            {
                dadosAtendimento.motivoEncerramentoSpecified = true;
                dadosAtendimento.motivoEncerramento = (dm_motivoSaidaObito)FuncoesGlobais.ObterValueEnum(typeof(dm_motivoSaidaObito), atendimento.MotivoAlta.Codigo, false);
            }

            return dadosAtendimento;
        }

        //private ct_beneficiarioDados GerarBeneficiarioDados(AtendimentoDto atendimento)
        //{
        //    ct_beneficiarioDados beneficiarioDados = new ct_beneficiarioDados();

        //    beneficiarioDados.nomeBeneficiario = atendimento.Paciente.NomeCompleto;
        //    beneficiarioDados.numeroCarteira = atendimento.Matricula;
        //    if (atendimento.Paciente.Nascimento != null)
        //    {
        //        beneficiarioDados.atendimentoRN = FuncoesGlobais.IsRN((DateTime)atendimento.Paciente.Nascimento) ? dm_simNao.S : dm_simNao.N;
        //    }

        //    return beneficiarioDados;
        //}

        private ct_autorizacaoSADT GerarAutorizacaoSADT(AtendimentoDto atendimento)
        {
            if (!string.IsNullOrEmpty(atendimento.Senha))
            {
                ct_autorizacaoSADT autorizacaoSADT = new ct_autorizacaoSADT();

                autorizacaoSADT.dataAutorizacao = atendimento.DataAutorizacao ?? DateTime.MinValue;

                autorizacaoSADT.dataValidadeSenhaSpecified = atendimento.ValidadeSenha != null;
                if (autorizacaoSADT.dataValidadeSenhaSpecified)
                {
                    autorizacaoSADT.dataValidadeSenha = (DateTime)atendimento.ValidadeSenha;
                }

                autorizacaoSADT.numeroGuiaOperadora = atendimento.GuiaNumero;
                autorizacaoSADT.senha = atendimento.Senha;
                autorizacaoSADT.dataValidadeSenha = atendimento.ValidadeSenha ?? DateTime.MinValue;

                return autorizacaoSADT;
            }
            else
            {
                return null;
            }
        }

        //private ct_guiaValorTotal GerarValorTotal(FaturamentoContaDto faturamentoConta)
        //{
        //    _faturamentoItemTabelaRepository = new SWRepository<FaturamentoItemTabela>();
        //    ct_guiaValorTotal guiaValorTotal = new ct_guiaValorTotal();

        //    var itensOPME = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == (long)EnumCodigoDespesa.OPME).ToList();
        //    guiaValorTotal.valorOPME = SomaValorContaItens(itensOPME);
        //    guiaValorTotal.valorOPMESpecified = guiaValorTotal.valorOPME > 0;

        //    var itensMedicamentos = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == (long)EnumCodigoDespesa.Medicamentos).ToList();
        //    guiaValorTotal.valorMedicamentos = SomaValorContaItens(itensMedicamentos);// 2
        //    guiaValorTotal.valorMedicamentosSpecified = guiaValorTotal.valorMedicamentos > 0;

        //    var itensDiarias = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == (long)EnumCodigoDespesa.Diarias).ToList();
        //    guiaValorTotal.valorDiarias = SomaValorContaItens(itensDiarias);// 5
        //    guiaValorTotal.valorDiariasSpecified = guiaValorTotal.valorDiarias > 0;

        //    var itensGasesMedicinais = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == (long)EnumCodigoDespesa.GasesMedicinais).ToList();
        //    guiaValorTotal.valorGasesMedicinais = SomaValorContaItens(itensGasesMedicinais);// 1
        //    guiaValorTotal.valorGasesMedicinaisSpecified = guiaValorTotal.valorGasesMedicinais > 0;

        //    var itensMateriais = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == (long)EnumCodigoDespesa.Materiais).ToList();
        //    guiaValorTotal.valorMateriais = SomaValorContaItens(itensMateriais);// 3
        //    guiaValorTotal.valorMateriaisSpecified = guiaValorTotal.valorMateriais > 0;

        //    var itensTaxasAlugueis = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == (long)EnumCodigoDespesa.TaxasAluguéis).ToList();
        //    guiaValorTotal.valorTaxasAlugueis = SomaValorContaItens(itensTaxasAlugueis);// 3
        //    guiaValorTotal.valorTaxasAlugueisSpecified = guiaValorTotal.valorTaxasAlugueis > 0;

        //    var itensProcedimentos = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == null).ToList();
        //    guiaValorTotal.valorProcedimentos = SomaValorContaItens(itensProcedimentos);
        //    guiaValorTotal.valorProcedimentosSpecified = guiaValorTotal.valorProcedimentos > 0;


        //    guiaValorTotal.valorTotalGeral = guiaValorTotal.valorOPME
        //                                   + guiaValorTotal.valorMedicamentos
        //                                   + guiaValorTotal.valorDiarias
        //                                   + guiaValorTotal.valorGasesMedicinais
        //                                   + guiaValorTotal.valorMateriais
        //                                   + guiaValorTotal.valorTaxasAlugueis
        //                                   + guiaValorTotal.valorProcedimentos;


        //    return guiaValorTotal;
        //}

        //private decimal SomaValorContaItens(List<FaturamentoContaItemDto> itens)
        //{
        //    decimal valorTotal = 0;


        //    foreach (var item in itens)
        //    {
        //        //var tabelaId = item.FaturamentoConfigConvenioDto?.TabelaId;

        //        //var fatItemTabela = _faturamentoItemTabelaRepository.GetAll()
        //        //                                .Where(w => w.ItemId == item.FaturamentoItemId
        //        //                                        && w.TabelaId == tabelaId)
        //        //                                .FirstOrDefault();
        //        //if (fatItemTabela != null)
        //        //{
        //        //    valorTotal += ((decimal)fatItemTabela.Preco) * (decimal)item.Qtde;
        //        //}


        //        valorTotal += ((decimal)item.ValorItem ) * (decimal)item.Qtde;
        //    }

        //    return valorTotal;
        //}

        //private ct_outrasDespesasDespesa[] GerarListaOutrasDespesas(FaturamentoContaDto faturamentoConta)
        //{
        //    var itensOutrasDespesas = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId != null).ToList();

        //    ct_outrasDespesasDespesa[] listOutrasDespesasDespes = new ct_outrasDespesasDespesa[itensOutrasDespesas.Count];

        //    int posicao = 0;

        //    foreach (var item in itensOutrasDespesas)
        //    {
        //        ct_outrasDespesasDespesa outraDespesa = new ct_outrasDespesasDespesa();

        //        outraDespesa.codigoDespesa = (dm_outrasDespesas)FuncoesGlobais.ObterValueEnum(typeof(dm_outrasDespesas), item.FaturamentoItem.Grupo.FaturamentoCodigoDespesa.Codigo, false);
        //        outraDespesa.servicosExecutados = GerarProcedimentoExecutadoOutras(item);

        //        listOutrasDespesasDespes[posicao++] = outraDespesa;
        //    }

        //    return listOutrasDespesasDespes;
        //}

        //private ct_procedimentoExecutadoOutras GerarProcedimentoExecutadoOutras(FaturamentoContaItemDto item)
        //{
        //    ct_procedimentoExecutadoOutras procedimentoExecutadoOutras = new ct_procedimentoExecutadoOutras();

        //    procedimentoExecutadoOutras.codigoRefFabricante = item.FaturamentoItem.Referencia;
        //    procedimentoExecutadoOutras.codigoTabela = (dm_tabela)FuncoesGlobais.ObterValueEnum(typeof(dm_tabela), item.FaturamentoConfigConvenioDto.Codigo, false);
        //    procedimentoExecutadoOutras.dataExecucao = item.Data ?? DateTime.MinValue;
        //    procedimentoExecutadoOutras.codigoProcedimento = item.FaturamentoItem.CodTuss;
        //    procedimentoExecutadoOutras.descricaoProcedimento = item.FaturamentoItem.DescricaoTuss;

        //    procedimentoExecutadoOutras.dataExecucao = item.Data?? DateTime.MinValue;

        //    procedimentoExecutadoOutras.horaInicialSpecified = item.HoraIncio != null;

        //    if (procedimentoExecutadoOutras.horaInicialSpecified)
        //    {
        //        procedimentoExecutadoOutras.horaInicial = (DateTime)item.HoraIncio;
        //    }

        //    procedimentoExecutadoOutras.horaFinalSpecified = item.HoraFim != null;

        //    if (procedimentoExecutadoOutras.horaFinalSpecified)
        //    {
        //        procedimentoExecutadoOutras.horaFinal = (DateTime)item.HoraFim;
        //    }

        //    procedimentoExecutadoOutras.quantidadeExecutada = (decimal)item.Qtde;
        //    //procedimentoExecutadoOutras.registroANVISA
        //    //procedimentoExecutadoOutras.unidadeMedida



        //    //var tabelaId = item.FaturamentoConfigConvenioDto.TabelaId;

        //    //var fatItemTabela = _faturamentoItemTabelaRepository.GetAll()
        //    //                                .Where(w => w.ItemId == item.FaturamentoItemId
        //    //                                        && w.TabelaId == tabelaId)
        //    //                                .FirstOrDefault();

        //    //if (fatItemTabela != null)
        //    //{
        //    //    procedimentoExecutadoOutras.valorUnitario = (decimal)(fatItemTabela.Preco);
        //    //    procedimentoExecutadoOutras.valorTotal = (procedimentoExecutadoOutras.valorUnitario * procedimentoExecutadoOutras.quantidadeExecutada);
        //    //}


        //    procedimentoExecutadoOutras.valorUnitario = (decimal)(item.ValorItem);
        //    procedimentoExecutadoOutras.valorTotal = (procedimentoExecutadoOutras.valorUnitario * procedimentoExecutadoOutras.quantidadeExecutada);

        //    return procedimentoExecutadoOutras;
        //}

        private ct_procedimentoExecutadoSadt[] GerarProcedimentosExecutados(FaturamentoContaDto faturamentoConta)
        {
            var itensProcedimentosExecutados = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == null).ToList();
            ct_procedimentoExecutadoSadt[] listProcedimentosExecutados = new ct_procedimentoExecutadoSadt[itensProcedimentosExecutados.Count];

            int posicao = 0;

            foreach (var item in itensProcedimentosExecutados)
            {

                ct_procedimentoExecutadoSadt procedimentoExecutado = new ct_procedimentoExecutadoSadt();

                procedimentoExecutado.dataExecucao = item.Data ?? DateTime.MinValue;

                procedimentoExecutado.horaInicialSpecified = item.HoraIncio != null;

                if (procedimentoExecutado.horaInicialSpecified)
                {
                    procedimentoExecutado.horaInicial = (DateTime)item.HoraIncio;
                }

                procedimentoExecutado.horaFinalSpecified = item.HoraFim != null;

                if (procedimentoExecutado.horaFinalSpecified)
                {
                    procedimentoExecutado.horaFinal = (DateTime)item.HoraFim;
                }

                procedimentoExecutado.quantidadeExecutada = item.Qtde.ToString();

                procedimentoExecutado.viaAcessoSpecified = !string.IsNullOrEmpty(item.ViaAcesso);

                if (procedimentoExecutado.viaAcessoSpecified)
                {
                    procedimentoExecutado.viaAcesso = (dm_viaDeAcesso)FuncoesGlobais.ObterValueEnum(typeof(dm_viaDeAcesso), item.ViaAcesso, false);
                }


                //var tabelaId = item.FaturamentoConfigConvenioDto.TabelaId;

                //var fatItemTabela = _faturamentoItemTabelaRepository.GetAll()
                //                                .Where(w => w.ItemId == item.FaturamentoItemId
                //                                        && w.TabelaId == tabelaId)
                //                                .FirstOrDefault();

                //if (fatItemTabela != null)
                //{
                //    procedimentoExecutado.valorUnitario = (decimal)(fatItemTabela.Preco);
                //    procedimentoExecutado.valorTotal = (procedimentoExecutado.valorUnitario * (decimal)item.Qtde);
                //}

                procedimentoExecutado.valorUnitario = (decimal)(item.ValorItem);
                procedimentoExecutado.valorTotal = (procedimentoExecutado.valorUnitario * (decimal)item.Qtde);

                procedimentoExecutado.procedimento = GerarProcecimentoExecutado(item);
                procedimentoExecutado.equipeSadt = GerarEquipeSADT(item);

                procedimentoExecutado.tecnicaUtilizadaSpecified = !string.IsNullOrEmpty(item.Tecnica);
                if (procedimentoExecutado.tecnicaUtilizadaSpecified)
                {
                    procedimentoExecutado.tecnicaUtilizada = (dm_tecnicaUtilizada)FuncoesGlobais.ObterValueEnum(typeof(dm_tecnicaUtilizada), item.Tecnica, false);
                }

                listProcedimentosExecutados[posicao++] = procedimentoExecutado;
            }

            return listProcedimentosExecutados;
        }

        //private ct_procedimentoDados GerarProcecimentoExecutado(FaturamentoContaItemDto item)
        //{
        //    ct_procedimentoDados procedimentoExecutado = new ct_procedimentoDados();

        //    procedimentoExecutado.codigoProcedimento = item.FaturamentoItem.CodTuss;
        //    procedimentoExecutado.descricaoProcedimento = item.FaturamentoItem.DescricaoTuss;
        //    procedimentoExecutado.codigoTabela = (dm_tabela)FuncoesGlobais.ObterValueEnum(typeof(dm_tabela), item.FaturamentoConfigConvenioDto.Codigo, false);

        //    return procedimentoExecutado;
        //}

        private ct_identEquipeSADT[] GerarEquipeSADT(FaturamentoContaItemDto item)
        {
            List<ct_identEquipeSADT> listIdentEquipeSADT = new List<ct_identEquipeSADT>();

            listIdentEquipeSADT.Add(GerarIntegranteEquipe(item.Anestesista, item.EspecialidadeAnestesista));
            listIdentEquipeSADT.Add(GerarIntegranteEquipe(item.Auxiliar1, item.Auxiliar1Especialidade));
            listIdentEquipeSADT.Add(GerarIntegranteEquipe(item.Auxiliar2, item.Auxiliar2Especialidade));
            listIdentEquipeSADT.Add(GerarIntegranteEquipe(item.Auxiliar2, item.Auxiliar3Especialidade));
            listIdentEquipeSADT.Add(GerarIntegranteEquipe(item.Medico, item.MedicoEspecialidade));
            listIdentEquipeSADT.Add(GerarIntegranteEquipe(item.Instrumentador, item.InstrumentadorEspecialidade));

            listIdentEquipeSADT = listIdentEquipeSADT.Where(w => w != null).ToList();

            ct_identEquipeSADT[] identEquipeSADT = new ct_identEquipeSADT[listIdentEquipeSADT.Count];

            int posicao = 0;
            foreach (var itemEquipe in listIdentEquipeSADT)
            {
                identEquipeSADT[posicao++] = itemEquipe;
            }

            return identEquipeSADT;
        }

        private ct_identEquipeSADT GerarIntegranteEquipe(MedicoDto medico, MedicoEspecialidadeDto Medicoespecialidade)
        {
            ct_identEquipeSADT integranteEquipe = null;

            if (medico != null)
            {
                integranteEquipe = new ct_identEquipeSADT();

                if (medico.Conselho != null)
                {
                    integranteEquipe.conselho = (dm_conselhoProfissional)FuncoesGlobais.ObterValueEnum(typeof(dm_conselhoProfissional), medico.Conselho.Codigo, false);
                    integranteEquipe.UF = (dm_UF)FuncoesGlobais.ObterValueEnum(typeof(dm_UF), medico.Conselho.Uf, false);
                }
                integranteEquipe.nomeProf = medico.NomeCompleto;
                integranteEquipe.numeroConselhoProfissional = medico.NumeroConselho.ToString();
                integranteEquipe.codProfissional = new ct_identEquipeSADTCodProfissional { Item = medico.SisPessoa.Cpf, ItemElementName= ItemChoiceType4.cpfContratado };

            }

            if (Medicoespecialidade != null && integranteEquipe != null)
            {
                integranteEquipe.CBOS = (dm_CBOS)FuncoesGlobais.ObterValueEnum(typeof(dm_CBOS), Medicoespecialidade.Especialidade.Codigo, false);
            }


            return integranteEquipe;
        }

        protected override ct_guiaCabecalho GerarCabecalhoGuia(FaturamentoContaDto faturamentoConta, ct_guiaCabecalho guiaCabecalho = null)
        {
            ctm_spsadtGuiaCabecalhoGuia cabecalhoSPSADT = new ctm_spsadtGuiaCabecalhoGuia();

            base.GerarCabecalhoGuia(faturamentoConta, cabecalhoSPSADT);

            cabecalhoSPSADT.guiaPrincipal = faturamentoConta.GuiaPrincipal;

            return cabecalhoSPSADT;
        }

        private ctm_spsadtGuiaDadosSolicitante GerarDadosSolicitante(FaturamentoContaDto faturamentoConta, string codigoPrestadorNaOperadora)
        {
            ctm_spsadtGuiaDadosSolicitante spsadtGuiaDadosSolicitante = new ctm_spsadtGuiaDadosSolicitante();

            ct_contratadoDados contratadoDados = new ct_contratadoDados();

            contratadoDados.nomeContratado = faturamentoConta.Empresa.NomeFantasia;
            contratadoDados.ItemElementName = ItemChoiceType1.codigoPrestadorNaOperadora;
            contratadoDados.Item = codigoPrestadorNaOperadora;

            spsadtGuiaDadosSolicitante.contratadoSolicitante = contratadoDados;



            ct_contratadoProfissionalDados contratadoProfissionalDados = new ct_contratadoProfissionalDados();

            contratadoProfissionalDados.conselhoProfissional = FuncoesGlobais.ObterValueEnumType<dm_conselhoProfissional>(faturamentoConta.Atendimento.Medico.Conselho.Codigo, false);
            contratadoProfissionalDados.nomeProfissional = faturamentoConta.Atendimento.Medico.SisPessoa.NomeCompleto;
            contratadoProfissionalDados.numeroConselhoProfissional = faturamentoConta.Atendimento.Medico.NumeroConselho.ToString();
            contratadoProfissionalDados.UF = FuncoesGlobais.ObterValueEnumType<dm_UF>(faturamentoConta.Atendimento.Medico.Conselho.Uf, false);
            if (faturamentoConta.Atendimento.Especialidade != null)
            {
                contratadoProfissionalDados.CBOS = FuncoesGlobais.ObterValueEnumType<dm_CBOS>(faturamentoConta.Atendimento.Especialidade.Codigo, false);
            }

            spsadtGuiaDadosSolicitante.profissionalSolicitante = contratadoProfissionalDados;

            return spsadtGuiaDadosSolicitante;

        }

        private ct_contratadoDados GerarDadosContratados(AtendimentoDto atendimento, string codigoPrestadorNaOperadora)
        {
            ct_contratadoDados contratadoDados = new ct_contratadoDados();

            contratadoDados.ItemElementName = ItemChoiceType1.codigoPrestadorNaOperadora;
            contratadoDados.nomeContratado = "";

            return contratadoDados;
        }

    }



}
