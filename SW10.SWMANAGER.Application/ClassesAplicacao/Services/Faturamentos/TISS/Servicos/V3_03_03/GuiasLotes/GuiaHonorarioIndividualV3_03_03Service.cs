﻿using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03.GuiasLotes
{
    public class GuiaHonorarioIndividualV3_03_03Service
    {
        public ctm_honorarioIndividualGuia[] GerarGuiaHonorarioIndividual(List<FaturamentoEntregaContaDto> faturamentoEntregaContas)
        {
            ctm_honorarioIndividualGuia[] honorarioIndividualGuias = new ctm_honorarioIndividualGuia[faturamentoEntregaContas.Count];

            int posicao = 0;

            foreach (var item in faturamentoEntregaContas)
            {
                ctm_honorarioIndividualGuia honorarioIndividualGuia = new ctm_honorarioIndividualGuia();


                honorarioIndividualGuias[posicao++] = honorarioIndividualGuia;
            }

            return honorarioIndividualGuias;
        }
    }
}
