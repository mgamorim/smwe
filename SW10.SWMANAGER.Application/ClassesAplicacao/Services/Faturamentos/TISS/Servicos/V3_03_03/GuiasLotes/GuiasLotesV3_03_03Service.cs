﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Guias;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03.GuiasLotes
{
    public class GuiasLotesV3_03_03Service
    {
        // private readonly IRepository<Lote>


        public ctm_guiaLote GerarGuiasLote(long loteId, string codigoPrestadorNaOperadora)
        {

            var _faturamentoEntregaContaRepository = new SWRepository<FaturamentoEntregaConta>();

            var contasLotes = _faturamentoEntregaContaRepository.GetAll()
                                                                .Where(w => w.EntregaLoteId == loteId)
                                                                .Include(i => i.ContaMedica)
                                                                .Include(i => i.ContaMedica.Atendimento)
                                                                .Include(i => i.ContaMedica.Atendimento.Convenio)
                                                                .Include(i => i.ContaMedica.Atendimento.Paciente)
                                                                .Include(i => i.ContaMedica.Atendimento.Paciente.SisPessoa)
                                                                .Include(i => i.ContaMedica.Atendimento.AtendimentoTipo.TabelaDominio)
                                                                .Include(i => i.ContaMedica.Atendimento.FatGuia)
                                                                .Include(i => i.ContaMedica.Atendimento.MotivoAlta)
                                                                .Include(i => i.ContaMedica.Atendimento.MotivoAlta.MotivoAltaTipoAlta)
                                                                .Include(i => i.ContaMedica.Atendimento.Empresa)
                                                                .Include(i => i.ContaMedica.Atendimento.Medico)
                                                                .Include(i => i.ContaMedica.Atendimento.Medico.SisPessoa)
                                                                .Include(i => i.ContaMedica.Atendimento.Medico.Conselho)
                                                                .Include(i => i.ContaMedica.Atendimento.Especialidade)
                                                                .Include(i => i.EntregaLote);
                        
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.FaturamentoItem))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.FaturamentoItem.Grupo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.FaturamentoItem.Grupo.FaturamentoCodigoDespesa))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Anestesista))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Anestesista.Conselho))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.AnestesistaEspecialidade))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.AnestesistaEspecialidade.Especialidade.SisCbo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar1))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar1.Conselho))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar1Especialidade))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar1Especialidade.Especialidade.SisCbo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar2))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar2.Conselho))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar2Especialidade))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar2Especialidade.Especialidade.SisCbo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar3))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar3.Conselho))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar3Especialidade))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Auxiliar3Especialidade.Especialidade.SisCbo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Instrumentador))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Instrumentador.Conselho))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.InstrumentadorEspecialidade))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.InstrumentadorEspecialidade.Especialidade.SisCbo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Medico))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.Medico.Conselho))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.MedicoEspecialidade))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.MedicoEspecialidade.Especialidade.SisCbo))
                                                                //.Include(i => i.ContaMedica.ContaItens.Select(s => s.FaturamentoConfigConvenio));
            var listaContas = contasLotes.ToList();

            ctm_guiaLote guiaLote = new ctm_guiaLote();

            guiaLote.guiasTISS = new ctm_guiaLoteGuiasTISS();

        

            guiaLote.guiasTISS.Items = new object[listaContas.Count];



            long codigoGuia = 0;

            if (listaContas.Count > 0)
            {
                long.TryParse(listaContas[0].ContaMedica.Atendimento.FatGuia.Codigo, out codigoGuia);
                guiaLote.numeroLote = listaContas[0].EntregaLote.CodEntregaLote;
            }

            int posicao = 0;

            switch (codigoGuia)
            {
                case (long)EnumCodigoTipoGuia.Consulta:

                    GuiaConsultaV3_03_03Service guiaConsulta = new GuiaConsultaV3_03_03Service();
                    guiaLote.guiasTISS.Items = guiaConsulta.GerarGuiaConsulta(FaturamentoEntregaContaDto.Mapear(listaContas));
                    break;

                case (long)EnumCodigoTipoGuia.SPSADT:

                        GuiaSPSADTV3_03_03Service guiaSPADT = new GuiaSPSADTV3_03_03Service();
                        guiaLote.guiasTISS.Items = guiaSPADT.GerarGuiaSPSADT(FaturamentoEntregaContaDto.Mapear(listaContas), codigoPrestadorNaOperadora);
                    break;

                case (long)EnumCodigoTipoGuia.ResumoInternacao:

                    GuiaResumoInternacaoV3_03_03Service guiaResumoInternacao = new GuiaResumoInternacaoV3_03_03Service();
                    guiaLote.guiasTISS.Items = guiaResumoInternacao.GerarGuiaResumoInternacoes(FaturamentoEntregaContaDto.Mapear(listaContas));
                    break;

                case (long)EnumCodigoTipoGuia.HonorarioIndividual:

                    GuiaHonorarioIndividualV3_03_03Service guiaHonorarioIndividual = new GuiaHonorarioIndividualV3_03_03Service();
                    guiaLote.guiasTISS.Items = guiaHonorarioIndividual.GerarGuiaHonorarioIndividual(FaturamentoEntregaContaDto.Mapear(listaContas));
                    break;

                case (long)EnumCodigoTipoGuia.Particular:
                    break;
            }

            return guiaLote;
        }

    }
}
