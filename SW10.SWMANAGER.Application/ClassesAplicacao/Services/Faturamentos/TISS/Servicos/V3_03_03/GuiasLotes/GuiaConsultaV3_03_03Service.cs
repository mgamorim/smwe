﻿using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03.GuiasLotes
{
    public class GuiaConsultaV3_03_03Service: GuiaV3_03_03Service
    {
        public ctm_consultaGuia[] GerarGuiaConsulta(List<FaturamentoEntregaContaDto> faturamentoEntregaConta)
        {
            ctm_consultaGuia[] consultaGuias = new ctm_consultaGuia[faturamentoEntregaConta.Count];


            int posicao = 0;

            foreach (var item in faturamentoEntregaConta)
            {
                var faturamentoConta = item.ContaMedica;

                ctm_consultaGuia consultaGuia = new ctm_consultaGuia();

                consultaGuia.cabecalhoConsulta = GerarCabecalhoGuia(faturamentoConta); //GerarCabecalho(faturamentoConta);
                consultaGuia.contratadoExecutante = GerarContratoExecutante();

                consultaGuias[posicao++] = consultaGuia;
            }

            return consultaGuias;
        }

        private ct_guiaCabecalho GerarCabecalho(FaturamentoContaDto faturamentoConta)
        {
            ct_guiaCabecalho cabecalho = new ct_guiaCabecalho();

            cabecalho.registroANS = faturamentoConta.Atendimento.Convenio.RegistroANS;

            return cabecalho;
        }

        private ctm_consultaGuiaContratadoExecutante GerarContratoExecutante()
        {
            ctm_consultaGuiaContratadoExecutante contratadoExecutante = new ctm_consultaGuiaContratadoExecutante();

            return contratadoExecutante;
        }
    }
}
