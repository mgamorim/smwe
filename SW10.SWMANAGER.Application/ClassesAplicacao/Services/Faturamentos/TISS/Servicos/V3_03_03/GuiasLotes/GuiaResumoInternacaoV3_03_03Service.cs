﻿using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03.GuiasLotes
{
    public class GuiaResumoInternacaoV3_03_03Service : GuiaV3_03_03Service
    {
        public ctm_internacaoResumoGuia[] GerarGuiaResumoInternacoes(List<FaturamentoEntregaContaDto> faturamentoEntregaConta)
        {
          var  _faturamentoContaItemRepository = new SWRepository<FaturamentoContaItem>();


            ctm_internacaoResumoGuia[] resumoInternacaoGuias = new ctm_internacaoResumoGuia[faturamentoEntregaConta.Count];

            int posicao = 0;

            foreach (var item in faturamentoEntregaConta)
            {
                var faturamentoConta = item.ContaMedica;



                var itensConta = _faturamentoContaItemRepository.GetAll()
                                                               .Where(w => w.FaturamentoContaId == item.ContaMedicaId)
                                                                 .Include(i => i.FaturamentoItem)
                                                                 .Include(s => s.FaturamentoItem.Grupo)
                                                                 .Include(s => s.FaturamentoItem.Grupo.FaturamentoCodigoDespesa)
                                                                 .Include(s => s.Anestesista)
                                                                 .Include(s => s.Anestesista.Conselho)
                                                                 .Include(s => s.AnestesistaEspecialidade)
                                                                 .Include(s => s.AnestesistaEspecialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Auxiliar1)
                                                                 .Include(s => s.Auxiliar1.Conselho)
                                                                 .Include(s => s.Auxiliar1Especialidade)
                                                                 .Include(s => s.Auxiliar1Especialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Auxiliar2)
                                                                 .Include(s => s.Auxiliar2.Conselho)
                                                                 .Include(s => s.Auxiliar2Especialidade)
                                                                 .Include(s => s.Auxiliar2Especialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Auxiliar3)
                                                                 .Include(s => s.Auxiliar3.Conselho)
                                                                 .Include(s => s.Auxiliar3Especialidade)
                                                                 .Include(s => s.Auxiliar3Especialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Instrumentador)
                                                                 .Include(s => s.Instrumentador.Conselho)
                                                                 .Include(s => s.InstrumentadorEspecialidade)
                                                                 .Include(s => s.InstrumentadorEspecialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.Medico)
                                                                 .Include(s => s.Medico.Conselho)
                                                                 .Include(s => s.MedicoEspecialidade)
                                                                 .Include(s => s.MedicoEspecialidade.Especialidade.SisCbo)
                                                                 .Include(s => s.FaturamentoConfigConvenio);

                var itensContaList = itensConta.ToList();


                List<FaturamentoContaItemDto> itensContaListDto = new List<FaturamentoContaItemDto>();

                foreach (var itemConta in itensContaList)
                {
                    itensContaListDto.Add(FaturamentoContaItemDto.MapearFromCore(itemConta));
                }

                faturamentoConta.Itens = itensContaListDto;






                ctm_internacaoResumoGuia internacaoResumoGuia = new ctm_internacaoResumoGuia();

                internacaoResumoGuia.cabecalhoGuia = GerarCabecalhoGuia(faturamentoConta);
                internacaoResumoGuia.numeroGuiaSolicitacaoInternacao = faturamentoConta.Atendimento.GuiaNumero;
                internacaoResumoGuia.outrasDespesas = GerarListaOutrasDespesas(faturamentoConta);
                internacaoResumoGuia.valorTotal = GerarValorTotal(faturamentoConta);
                internacaoResumoGuia.dadosAutorizacao = GerarAutorizacaoInternacao(faturamentoConta);
                internacaoResumoGuia.dadosBeneficiario = GerarBeneficiarioDados(faturamentoConta.Atendimento);
                internacaoResumoGuia.dadosExecutante = GerarDadosExecutante();
                internacaoResumoGuia.dadosInternacao = GerarDadosInternacao(faturamentoConta);
                internacaoResumoGuia.dadosSaidaInternacao = GerarDadosSaida();
                internacaoResumoGuia.procedimentosExecutados = GerarProcedimentosExecutadosInternacao(faturamentoConta);


                resumoInternacaoGuias[posicao++] = internacaoResumoGuia;
            }

            return resumoInternacaoGuias;
        }

        private ct_autorizacaoInternacao GerarAutorizacaoInternacao(FaturamentoContaDto faturamentoConta)
        {
            var atendimento = faturamentoConta.Atendimento;

            ct_autorizacaoInternacao autorizacaoInternacao = new ct_autorizacaoInternacao();

            autorizacaoInternacao.dataAutorizacao = atendimento.DataAutorizacao ?? DateTime.MinValue;

            autorizacaoInternacao.dataValidadeSenhaSpecified = atendimento.ValidadeSenha != null;
            if (autorizacaoInternacao.dataValidadeSenhaSpecified)
            {
                autorizacaoInternacao.dataValidadeSenha = (DateTime)atendimento.ValidadeSenha;
            }

            autorizacaoInternacao.numeroGuiaOperadora = faturamentoConta.GuiaOperadora; ;
            autorizacaoInternacao.senha = atendimento.Senha;


            return autorizacaoInternacao;
        }

        private ctm_internacaoResumoGuiaDadosExecutante GerarDadosExecutante()
        {
            ctm_internacaoResumoGuiaDadosExecutante dadosExecutante = new ctm_internacaoResumoGuiaDadosExecutante();

            return dadosExecutante;
        }

        private ctm_internacaoDados GerarDadosInternacao(FaturamentoContaDto faturamentoConta)
        {
            ctm_internacaoDados dadosInternacao = new ctm_internacaoDados();

            dadosInternacao.dataInicioFaturamento = ((DateTime)faturamentoConta.DataIncio).Date;
            dadosInternacao.dataFinalFaturamento = ((DateTime)faturamentoConta.DataFim).Date;
            dadosInternacao.horaInicioFaturamento = (DateTime)faturamentoConta.DataIncio;
            dadosInternacao.horaFinalFaturamento = (DateTime)faturamentoConta.DataFim;
            dadosInternacao.declaracoes = GerarDeclaracoes();

            return dadosInternacao;
        }

        private ctm_internacaoDadosDeclaracoes[] GerarDeclaracoes()
        {
            ctm_internacaoDadosDeclaracoes[] declaracoes = new ctm_internacaoDadosDeclaracoes[0];

            return declaracoes;
        }

        private ctm_internacaoDadosSaida GerarDadosSaida()
        {
            ctm_internacaoDadosSaida dadosSaida = new ctm_internacaoDadosSaida();


            return dadosSaida;
        }

        private ct_procedimentoExecutadoInt[] GerarProcedimentosExecutadosInternacao(FaturamentoContaDto faturamentoConta)
        {
            var itensProcedimentosExecutados = faturamentoConta.Itens.Where(w => w.FaturamentoItem.Grupo.FaturamentoCodigoDespesaId == null).ToList();
            ct_procedimentoExecutadoInt[] procedimentosExecutados = new ct_procedimentoExecutadoInt[itensProcedimentosExecutados.Count];

            int posicao = 0;

            foreach (var item in itensProcedimentosExecutados)
            {
                ct_procedimentoExecutadoInt procedimentoExecutado = new ct_procedimentoExecutadoInt();

                procedimentoExecutado.dataExecucao = item.Data ?? DateTime.MinValue;

                procedimentoExecutado.horaInicialSpecified = item.HoraIncio != null;

                if (procedimentoExecutado.horaInicialSpecified)
                {
                    procedimentoExecutado.horaInicial = (DateTime)item.HoraIncio;
                }

                procedimentoExecutado.horaFinalSpecified = item.HoraFim != null;

                if (procedimentoExecutado.horaFinalSpecified)
                {
                    procedimentoExecutado.horaFinal = (DateTime)item.HoraFim;
                }

                procedimentoExecutado.quantidadeExecutada = item.Qtde.ToString();

                procedimentoExecutado.viaAcessoSpecified = !string.IsNullOrEmpty(item.ViaAcesso);

                if (procedimentoExecutado.viaAcessoSpecified)
                {
                    procedimentoExecutado.viaAcesso = (dm_viaDeAcesso)FuncoesGlobais.ObterValueEnum(typeof(dm_viaDeAcesso), item.ViaAcesso, false);
                }

                procedimentoExecutado.valorUnitario = (decimal)(item.ValorItem);
                procedimentoExecutado.valorTotal = (procedimentoExecutado.valorUnitario * (decimal)item.Qtde);

                procedimentoExecutado.procedimento = GerarProcecimentoExecutado(item);
                procedimentoExecutado.identEquipe = GerarIdentificacaoEquipe(item);

                procedimentoExecutado.tecnicaUtilizadaSpecified = !string.IsNullOrEmpty(item.Tecnica);
                if (procedimentoExecutado.tecnicaUtilizadaSpecified)
                {
                    procedimentoExecutado.tecnicaUtilizada = (dm_tecnicaUtilizada)FuncoesGlobais.ObterValueEnum(typeof(dm_tecnicaUtilizada), item.Tecnica, false);
                }

                procedimentosExecutados[posicao++] = procedimentoExecutado;

            }

            return procedimentosExecutados;

        }

        private ct_procedimentoExecutadoIntIdentEquipe[] GerarIdentificacaoEquipe(FaturamentoContaItemDto item)
        {

            List<ct_procedimentoExecutadoIntIdentEquipe> listIdentEquipe = new List<ct_procedimentoExecutadoIntIdentEquipe>();
            listIdentEquipe.Add(GerarIntegranteEquipe(item.Anestesista, item.EspecialidadeAnestesista));
            listIdentEquipe.Add(GerarIntegranteEquipe(item.Auxiliar1, item.Auxiliar1Especialidade));
            listIdentEquipe.Add(GerarIntegranteEquipe(item.Auxiliar2, item.Auxiliar2Especialidade));
            listIdentEquipe.Add(GerarIntegranteEquipe(item.Auxiliar2, item.Auxiliar3Especialidade));
            listIdentEquipe.Add(GerarIntegranteEquipe(item.Medico, item.MedicoEspecialidade));
            listIdentEquipe.Add(GerarIntegranteEquipe(item.Instrumentador, item.InstrumentadorEspecialidade));

            ct_procedimentoExecutadoIntIdentEquipe[] equipe = new ct_procedimentoExecutadoIntIdentEquipe[listIdentEquipe.Count];

            int posicao = 0;
            foreach (var itemEquipe in listIdentEquipe)
            {
                equipe[posicao++] = itemEquipe;
            }

            return equipe;
        }

        private ct_procedimentoExecutadoIntIdentEquipe GerarIntegranteEquipe(MedicoDto medico, MedicoEspecialidadeDto Medicoespecialidade)
        {
            ct_procedimentoExecutadoIntIdentEquipe identificadorEquipe = null;
            ct_identEquipe integranteEquipe = null;

            if (medico != null)
            {
                identificadorEquipe = new ct_procedimentoExecutadoIntIdentEquipe();

                integranteEquipe = new ct_identEquipe();
                identificadorEquipe.identificacaoEquipe = integranteEquipe;

                if (medico.Conselho != null)
                {
                    integranteEquipe.conselho = (dm_conselhoProfissional)FuncoesGlobais.ObterValueEnum(typeof(dm_conselhoProfissional), medico.Conselho.Codigo, false);
                    integranteEquipe.UF = (dm_UF)FuncoesGlobais.ObterValueEnum(typeof(dm_UF), medico.Conselho.Uf, false);
                }
                integranteEquipe.nomeProf = medico.NomeCompleto;
                integranteEquipe.numeroConselhoProfissional = medico.NumeroConselho.ToString();
                integranteEquipe.codProfissional = new ct_identEquipeCodProfissional { Item = medico.SisPessoa.Cpf, ItemElementName = ItemChoiceType5.cpfContratado };

            }

            if (Medicoespecialidade != null && integranteEquipe != null)
            {
                integranteEquipe.CBOS = (dm_CBOS)FuncoesGlobais.ObterValueEnum(typeof(dm_CBOS), Medicoespecialidade.Especialidade.Codigo, false);
            }
            


            return identificadorEquipe;
        }
    }
}
