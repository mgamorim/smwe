﻿using SW10.SWMANAGER.ClassesAplicaca.Services.Faturamentos.VersoesTISS.V3_03_03;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Entregas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Servicos.V3_03_03
{
    public class CabecalhoLotesV3_03_03Service
    {
        public cabecalhoTransacao CriarCabecalho(FaturamentoEntregaLoteDto faturamentoEntregaLote)
        {
            var cabecalho = new cabecalhoTransacao();

            cabecalho.origem = CriarTrancaoOrigem(faturamentoEntregaLote);
            cabecalho.destino = CriarTransacaoDestino(faturamentoEntregaLote);
            cabecalho.identificacaoTransacao = CriarIdentificacaoTrancao(faturamentoEntregaLote);
            cabecalho.Padrao = dm_versao.Item30303;

            return cabecalho;
        }

        private cabecalhoTransacaoIdentificacaoTransacao CriarIdentificacaoTrancao(FaturamentoEntregaLoteDto faturamentoEntregaLote)
        {
            cabecalhoTransacaoIdentificacaoTransacao cabecalhoTransacaoIdentificacaoTransacao = new cabecalhoTransacaoIdentificacaoTransacao();

            var dataHoraAtual = DateTime.Now;

            var horaAtual = new DateTime(1, 1, 1, dataHoraAtual.Hour, dataHoraAtual.Minute, dataHoraAtual.Second,0);

            cabecalhoTransacaoIdentificacaoTransacao.sequencialTransacao = faturamentoEntregaLote.CodEntregaLote;
            cabecalhoTransacaoIdentificacaoTransacao.dataRegistroTransacao = dataHoraAtual.Date;
            cabecalhoTransacaoIdentificacaoTransacao.horaRegistroTransacao = horaAtual;
            cabecalhoTransacaoIdentificacaoTransacao.tipoTransacao = dm_tipoTransacao.ENVIO_LOTE_GUIAS;

            return cabecalhoTransacaoIdentificacaoTransacao;
        }

        private cabecalhoTransacaoOrigem CriarTrancaoOrigem(FaturamentoEntregaLoteDto faturamentoEntregaLote)
        {
            cabecalhoTransacaoOrigem cabecalhoTransacaoOrigem = new cabecalhoTransacaoOrigem();
            cabecalhoTransacaoOrigemIdentificacaoPrestador cabecalhoTransacaoOrigemIdentificacaoPrestador = new cabecalhoTransacaoOrigemIdentificacaoPrestador();

            cabecalhoTransacaoOrigemIdentificacaoPrestador.ItemElementName = ItemChoiceType.codigoPrestadorNaOperadora;
            cabecalhoTransacaoOrigemIdentificacaoPrestador.Item = faturamentoEntregaLote.IdentificacaoPrestadorNaOperadora;

            cabecalhoTransacaoOrigem.Item = cabecalhoTransacaoOrigemIdentificacaoPrestador;

            return cabecalhoTransacaoOrigem;
        }

        private cabecalhoTransacaoDestino CriarTransacaoDestino(FaturamentoEntregaLoteDto faturamentoEntregaLote)
        {
            cabecalhoTransacaoDestino cabecalhoTransacaoDestino = new cabecalhoTransacaoDestino();

            cabecalhoTransacaoDestino.Item = faturamentoEntregaLote.Convenio.RegistroANS;

            return cabecalhoTransacaoDestino;
        }
    }
}
