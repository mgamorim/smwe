﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.TISS.Interfaces
{
    public interface ITISSAppService: IApplicationService
    {
        void GerarLoteXML(long loteId);
    }
}
