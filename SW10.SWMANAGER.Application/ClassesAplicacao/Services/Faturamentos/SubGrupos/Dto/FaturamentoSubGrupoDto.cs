﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.SubGrupos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.SubGrupos.Dto
{
    [AutoMap(typeof(FaturamentoSubGrupo))]
    public class FaturamentoSubGrupoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public virtual FaturamentoGrupoDto Grupo { get; set; }
        public long GrupoId { get; set; }

        public bool IsLaboratorio { get; set; }
        public bool IsLaudo { get; set; }
    }
}
