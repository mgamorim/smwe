﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaKits.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Kits;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaKits
{
    public class FaturamentoContaKitAppService : SWMANAGERAppServiceBase, IFaturamentoContaKitAppService
    {
        #region Cabecalho
        private readonly IRepository<FaturamentoContaKit, long> _contaRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<FaturamentoKit, long> _faturamentoKitRepository;
        private readonly IRepository<FaturamentoConta, long> _faturamentoContaRepository;
        private readonly IFaturamentoContaItemAppService _faturamentoContaItemAppService;

        public FaturamentoContaKitAppService(
            IRepository<FaturamentoContaKit, long> contaRepository
           , IUnitOfWorkManager unitOfWorkManager
            , IRepository<FaturamentoKit, long> faturamentoKitRepository
            , IRepository<FaturamentoConta, long> faturamentoContaRepository
            , IFaturamentoContaItemAppService faturamentoContaItemAppService
            )
        {
            _contaRepository = contaRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _faturamentoKitRepository = faturamentoKitRepository;
            _faturamentoContaRepository = faturamentoContaRepository;
            _faturamentoContaItemAppService = faturamentoContaItemAppService;
        }
        #endregion cabecalho.

        public async Task<PagedResultDto<FaturamentoContaKitDto>> Listar(ListarFaturamentoContaKitsInput input)
        {
            var contarContaKits = 0;
            List<FaturamentoContaKit> contas;
            List<FaturamentoContaKitDto> contasDtos = new List<FaturamentoContaKitDto>();
            try
            {
                var query = _contaRepository
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro)
                    ;

                contarContaKits = await query
                    .CountAsync();

                contas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                contasDtos = contas
                    .MapTo<List<FaturamentoContaKitDto>>();

                return new PagedResultDto<FaturamentoContaKitDto>(contarContaKits, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<FaturamentoContaKitViewModel>> ListarVM(ListarFaturamentoContaKitsInput input)
        {
            var contarContaKits = 0;
            List<FaturamentoContaKit> contas;
            List<FaturamentoContaKitViewModel> contasDtos = new List<FaturamentoContaKitViewModel>();
            try
            {
                var query = _contaRepository
                    .GetAll()
                    .Include(i => i.FaturamentoKit)
                    .Include(i => i.Turno)
                    .Include(i => i.Medico)
                    .Include(i => i.Medico.SisPessoa)
                    .WhereIf(!string.IsNullOrEmpty(input.Filtro), e => e.FaturamentoContaId.ToString() == input.Filtro)
                    ;

                contarContaKits = await query
                    .CountAsync();

                contas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var ck in contas)
                {
                    var contaKit = new FaturamentoContaKitViewModel();
                    contaKit.Id = ck.Id;
                    contaKit.Codigo = ck.FaturamentoKit?.Codigo;
                    contaKit.Descricao = ck.FaturamentoKit?.Descricao;
                    contaKit.FaturamentoContaId = ck.FaturamentoContaId;
                    contaKit.FaturamentoKitId = ck.FaturamentoKitId;
                    contaKit.Data = ck.Data;
                    contaKit.Qtde = ck.Qtde;
                    contaKit.CentroCustoId = ck.CentroCustoId;
                    contaKit.CentroCustoDescricao = ck.CentroCusto != null ? ck.CentroCusto.Descricao : string.Empty;
                    contaKit.TurnoId = ck.TurnoId;
                    contaKit.TurnoDescricao = ck.Turno?.Descricao;
                    //contaKit.TipoLeitoId           = ck.TipoLeitoId;
                    //contaKit.TipoLeitoDescricao    = ck.TipoLeito != null ? ck.TipoLeito.Descricao : string.Empty;

                    contaKit.TipoLeitoId = ck.TipoAcomodacaoId;
                    contaKit.TipoLeitoDescricao = ck.TipoAcomodacao != null ? ck.TipoAcomodacao.Descricao : string.Empty;

                    contaKit.HoraIncio = ck.HoraIncio;
                    contaKit.HoraFim = ck.HoraFim;
                    contaKit.MedicoId = ck.MedicoId;
                    contaKit.MedicoNome = ck.Medico != null ? ck.Medico.NomeCompleto : string.Empty;

                    contasDtos.Add(contaKit);
                }

                return new PagedResultDto<FaturamentoContaKitViewModel>(contarContaKits, contasDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task CriarOuEditar(FaturamentoContaKitDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var contaKit = new FaturamentoContaKit();
                    contaKit.Id = input.Id;
                    contaKit.FaturamentoKitId = input.FaturamentoKitId;
                    contaKit.Codigo = input.Codigo;
                    contaKit.Descricao = input.Descricao;
                    contaKit.FaturamentoContaId = input.FaturamentoContaId;
                    contaKit.Data = input.Data;
                    contaKit.Qtde = input.Qtde;
                    contaKit.CentroCustoId = input.CentroCustoId;
                    contaKit.TurnoId = input.TurnoId;
                    // contaKit.TipoLeitoId           = input.TipoLeitoId;
                    contaKit.TipoAcomodacaoId = input.TipoLeitoId;
                    contaKit.HoraIncio = input.HoraIncio;
                    contaKit.HoraFim = input.HoraFim;
                    contaKit.MedicoId = input.MedicoId;

                    



                    if (input.Id.Equals(0))
                    {
                        contaKit.Id = await _contaRepository.InsertAndGetIdAsync(contaKit);
                    }
                    else
                    {
                        await _contaRepository.UpdateAsync(contaKit);
                    }





                    //var kit = _faturamentoKitRepository.GetAll()
                    //                                   .Include(i => i.Itens)
                    //                                   .Where(w => w.Id == input.FaturamentoKitId)
                    //                                   .FirstOrDefault();

                    //if (kit != null)
                    //{
                    //    var conta = _faturamentoContaRepository.GetAll()
                    //                                            .Include(i => i.ContaItens)
                    //                                            .Where(w => w.Id == input.FaturamentoContaId)
                    //                                            .FirstOrDefault();

                    //    if (conta != null)
                    //    {
                    //        if (conta.ContaItens == null)
                    //        {
                    //            conta.ContaItens = new List<FaturamentoContaItem>();
                    //        }

                    //        FaturamentoContaItemInsertDto faturamentoContaItemInsertDto = new FaturamentoContaItemInsertDto();

                    //        faturamentoContaItemInsertDto.AtendimentoId = conta.AtendimentoId ?? 0;
                    //        faturamentoContaItemInsertDto.CentroCustoId = input.CentroCustoId;
                    //        faturamentoContaItemInsertDto.Data = input.Data;
                    //        //faturamentoContaItemInsertDto.MedicoId = laudoMovimento.me

                    //        //faturamentoContaItemInsertDto.Obs = laudoMovimento.Obs;
                    //        faturamentoContaItemInsertDto.TurnoId = input.TurnoId;
                    //        // faturamentoContaItemInsertDto.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;

                    //        faturamentoContaItemInsertDto.ItensFaturamento = new List<FaturamentoContaItemDto>();
                    //        faturamentoContaItemInsertDto.ContaId = input.FaturamentoContaId;

                    //        foreach (var item in kit.Itens)
                    //        {

                    //            faturamentoContaItemInsertDto.ItensFaturamento.Add(new FaturamentoContaItemDto { Id = item.Id
                    //                                                                                           , Qtde = input.Qtde
                    //                                                                                           , FaturamentoContaKitId = contaKit.Id
                    //            });

                    //            //conta.ContaItens.Add(new FaturamentoContaItem
                    //            //{
                    //            //    Data = input.Data,
                    //            //    Qtde = input.Qtde,
                    //            //    UnidadeOrganizacionalId = input.CentroCustoId,
                    //            //    TurnoId = input.TurnoId,
                    //            //    HoraIncio = input.HoraIncio,
                    //            //    HoraFim = input.HoraFim,
                    //            //    MedicoId = input.MedicoId,
                    //            //    FaturamentoItemId = item.Id,
                    //            //    ValorItem = await _faturamentoContaItemAppService.CalcularValorUnitarioItem(conta.EmpresaId??0, conta.ConvenioId??0, conta.PlanoId??0, new FaturamentoContaItemDto { FaturamentoItemId = item.Id, FaturamentoItem = FaturamentoItemDto.Mapear(item) }



                    //            //    )
                    //            //});
                    //        }


                    //        await _faturamentoContaItemAppService.InserirItensContaFaturamento(faturamentoContaItemInsertDto);

                    //        // await _faturamentoContaRepository.UpdateAsync(conta);
                    //    }
                    //}

                  

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(FaturamentoContaKitDto input)
        {
            try
            {
                await _contaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task ExcluirVM(long id)
        {
            try
            {
                await _contaRepository.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoContaKitDto> Obter(long id)
        {
            try
            {
                var query = await _contaRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var conta = query.MapTo<FaturamentoContaKitDto>();

                return conta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FaturamentoContaKitViewModel> ObterViewModel(long id)
        {
            try
            {
                var query = await _contaRepository
                    .GetAll()
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.CentroCusto)
                    .Include(m => m.Turno)
                    //.Include(m => m.TipoLeito)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.FaturamentoKit)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var contaKit = new FaturamentoContaKitViewModel();
                contaKit.Id = query.Id;
                contaKit.Codigo = query.Codigo;
                contaKit.Descricao = query.Descricao;
                contaKit.FaturamentoContaId = query.FaturamentoContaId;
                contaKit.Data = query.Data;
                contaKit.Qtde = query.Qtde;
                contaKit.CentroCustoId = query.CentroCustoId;
                contaKit.CentroCustoDescricao = query.CentroCusto != null ? query.CentroCusto.Descricao : string.Empty;
                contaKit.TurnoId = query.TurnoId;
                contaKit.TurnoDescricao = query.Turno != null ? query.Turno.Descricao : string.Empty;
                //contaKit.TipoLeitoId             = query.TipoLeitoId;
                //contaKit.TipoLeitoDescricao      = query.TipoLeito != null ? query.TipoLeito.Descricao : string.Empty;
                contaKit.TipoLeitoId = query.TipoAcomodacaoId;
                contaKit.TipoLeitoDescricao = query.TipoAcomodacao != null ? query.TipoAcomodacao.Descricao : string.Empty;

                contaKit.HoraIncio = query.HoraIncio;
                contaKit.HoraFim = query.HoraFim;
                contaKit.MedicoId = query.MedicoId;
                contaKit.MedicoNome = query.Medico != null ? query.Medico.NomeCompleto : string.Empty;
                contaKit.FaturamentoKitId = query.FaturamentoKitId;
                contaKit.FaturamentoKitDescricao = query.FaturamentoKit != null ? query.FaturamentoKit.Descricao : string.Empty;

                return contaKit;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

    }


    public class FaturamentoContaKitViewModel
    {
        public long Id { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public long? FaturamentoContaId { get; set; }
        public DateTime? Data { get; set; }
        public float Qtde { get; set; }
        public long? CentroCustoId { get; set; }
        public string CentroCustoDescricao { get; set; }
        public long? TurnoId { get; set; }
        public string TurnoDescricao { get; set; }
        public long? TipoLeitoId { get; set; }
        public string TipoLeitoDescricao { get; set; }
        public DateTime? HoraIncio { get; set; }
        public DateTime? HoraFim { get; set; }
        public long? MedicoId { get; set; }
        public string MedicoNome { get; set; }
        public long? FaturamentoKitId { get; set; }
        public string FaturamentoKitDescricao { get; set; }
    }
}
