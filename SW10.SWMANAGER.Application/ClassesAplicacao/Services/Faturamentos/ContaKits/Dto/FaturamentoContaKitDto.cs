﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLeito.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Kits.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using System;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto
{
    [AutoMap(typeof(FaturamentoContaKit))]
    public class FaturamentoContaKitDto : CamposPadraoCRUDDto
    {
        [StringLength(100)]
        public string Descricao { get; set; }
        
        public long? FaturamentoContaId { get; set; }
        public FaturamentoContaDto FaturamentoConta { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? Data { get; set; }

        public float Qtde { get; set; }

        //[ForeignKey("LocalUtilizacao"), Column("LocalUtilizacaoId")]
        //public long? LocalUtilizacaoId { get; set; }
        //public LocalUtilizacao LocalUtilizacao { get; set; }

        //[ForeignKey("Terceirizado"), Column("TerceirizadoId")]
        //public long? TerceirizadoId { get; set; }
        //public Terceirizado Terceirizado { get; set; }
        
        public long? CentroCustoId { get; set; }
        public CentroCustoDto CentroCusto { get; set; }
        
        public long? TurnoId { get; set; }
        public Turno Turno { get; set; }
        
        public long? TipoLeitoId { get; set; }
        public TipoLeitoDto TipoLeito { get; set; }

        
        public DateTime? HoraIncio { get; set; }
        public DateTime? HoraFim { get; set; }

        public long? MedicoId { get; set; }
        public MedicoDto Medico { get; set; }

        //[ForeignKey("FaturamentoKit"), Column("FatKitId")]
        public long? FaturamentoKitId { get; set; }
        public FaturamentoKitDto FaturamentoKit { get; set; }


    }
}
