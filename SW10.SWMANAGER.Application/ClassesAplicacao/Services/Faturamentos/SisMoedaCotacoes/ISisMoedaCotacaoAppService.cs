﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    public interface ISisMoedaCotacaoAppService : IApplicationService
	{
        Task<PagedResultDto<SisMoedaCotacaoDto>> Listar(ListarSisMoedaCotacoesInput input);

        Task<PagedResultDto<SisMoedaCotacaoDto>> ListarPorMoeda (ListarSisMoedaCotacoesInput input);

        Task CriarOuEditar(SisMoedaCotacaoDto input);

        Task Excluir(SisMoedaCotacaoDto input);

        Task<SisMoedaCotacaoDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarSisMoedaCotacoesInput input);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
    }
}
