﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.SubGrupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.SisMoedas;
using System;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(SisMoedaCotacao))]
    public class SisMoedaCotacaoDto : CamposPadraoCRUDDto
    {
        public SisMoeda SisMoeda { get; set; }
        public long? SisMoedaId { get; set; }
        
        public EmpresaDto Empresa { get; set; }
        public long? EmpresaId { get; set; }
        
        public ConvenioDto Convenio { get; set; }
        public long? ConvenioId { get; set; }
        
        public PlanoDto Plano { get; set; }
        public long? PlanoId { get; set; }
        
        public FaturamentoGrupoDto Grupo { get; set; }
        public long? GrupoId { get; set; }
        
        public FaturamentoSubGrupoDto SubGrupo { get; set; }
        public long? SubGrupoId { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DataInicio { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime DataFinal { get; set; }

        public float Valor { get; set; }

        public bool IsTodosConvenio { get; set; }
        public bool IsTodosPlano { get; set; }
        public bool IsTodosItem { get; set; }
    }
}