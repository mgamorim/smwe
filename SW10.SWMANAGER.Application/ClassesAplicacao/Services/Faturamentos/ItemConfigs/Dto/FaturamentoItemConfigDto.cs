﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Grupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.SubGrupos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Tabelas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.ConfigConvenios;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ItemConfigs.Dto
{
    [AutoMap(typeof(FaturamentoItemConfig))]
    public class FaturamentoItemConfigDto : CamposPadraoCRUDDto
    {
        public long? ConvenioId { get; set; }
        public virtual ConvenioDto Convenio { get; set; }
        
        public long? PlanoId { get; set; }
        public virtual PlanoDto Plano { get; set; }
        
        public long? ItemId { get; set; }
        public virtual FaturamentoItemDto Item { get; set; }
        
        public long? ItemCobrarId { get; set; }
        public virtual FaturamentoItemDto ItemCobrar { get; set; }
    }
}
