﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasLaboratorios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasLaboratorios.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.BrasLaboratorios;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.BrasLaboratorios
{
    public class FaturamentoBrasLaboratorioAppService : SWMANAGERAppServiceBase, IFaturamentoBrasLaboratorioAppService
    {
        #region Cabecalho
        private readonly IRepository<FaturamentoBrasLaboratorio, long> _brasLaboratorioRepository;
        private readonly IListarBrasLaboratoriosExcelExporter _listarBrasLaboratoriosExcelExporter;

        public FaturamentoBrasLaboratorioAppService (IRepository<FaturamentoBrasLaboratorio, long> brasLaboratorioRepository, IListarBrasLaboratoriosExcelExporter listarBrasLaboratoriosExcelExporter)
        {
            _brasLaboratorioRepository = brasLaboratorioRepository;
            _listarBrasLaboratoriosExcelExporter = listarBrasLaboratoriosExcelExporter;
        }
        #endregion cabecalho.

        public async Task<PagedResultDto<FaturamentoBrasLaboratorioDto>> Listar (ListarFaturamentoBrasLaboratoriosInput input)
        {
            var brasLaboratoriosCount = 0;
            List<FaturamentoBrasLaboratorio> brasLaboratorios;
            List<FaturamentoBrasLaboratorioDto> brasLaboratoriosDtos = new List<FaturamentoBrasLaboratorioDto>();
            try
            {
                var query = _brasLaboratorioRepository
                    .GetAll()
                    ;

                brasLaboratoriosCount = await query
                    .CountAsync();

                brasLaboratorios = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                brasLaboratoriosDtos = brasLaboratorios
                    .MapTo<List<FaturamentoBrasLaboratorioDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<FaturamentoBrasLaboratorioDto>(
                brasLaboratoriosCount,
                brasLaboratoriosDtos
                );
        }

        public async Task CriarOuEditar (FaturamentoBrasLaboratorioDto input)
        {
            try
            {
                var BrasLaboratorio = input.MapTo<FaturamentoBrasLaboratorio>();
                if (input.Id.Equals(0))
                {
                    await _brasLaboratorioRepository.InsertAsync(BrasLaboratorio);
                }
                else
                {
                    await _brasLaboratorioRepository.UpdateAsync(BrasLaboratorio);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir (FaturamentoBrasLaboratorioDto input)
        {
            try
            {
                await _brasLaboratorioRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<FaturamentoBrasLaboratorioDto> Obter (long id)
        {
            try
            {
                var query = await _brasLaboratorioRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var brasLaboratorio = query
                    .MapTo<FaturamentoBrasLaboratorioDto>();

                return brasLaboratorio;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FaturamentoBrasLaboratorioDto> ObterComEstado (string nome, long estadoId)
        {
            try
            {
                var query = _brasLaboratorioRepository
                    .GetAll()
                    ;

                var result = await query.FirstOrDefaultAsync();

                var brasLaboratorio = result
                    .MapTo<FaturamentoBrasLaboratorioDto>();

                return brasLaboratorio;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel (ListarFaturamentoBrasLaboratoriosInput input)
        {
            try
            {
                var result = await Listar(input);
                var brasLaboratorios = result.Items;
                return _listarBrasLaboratoriosExcelExporter.ExportToFile(brasLaboratorios.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<FaturamentoBrasLaboratorioDto> faturamentoItensDto = new List<FaturamentoBrasLaboratorioDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _brasLaboratorioRepository.GetAll()
                            .WhereIf(!dropdownInput.filtro.IsNullOrEmpty(), m => m.Id.ToString() == dropdownInput.filtro)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m => m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
