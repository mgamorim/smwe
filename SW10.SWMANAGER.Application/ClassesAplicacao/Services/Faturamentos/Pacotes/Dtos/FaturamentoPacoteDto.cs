﻿using SW10.SWMANAGER.ClassesAplicacao.Faturamentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Pacotes.Dtos
{
    public class FaturamentoPacoteDto : CamposPadraoCRUDDto
    {
        public DateTime Inicio { get; set; }
        public DateTime Final { get; set; }
        public long? FaturamentoItemId { get; set; }
        public FaturamentoItemDto FaturamentoItem { get; set; }
        public long? FaturamentoContaId { get; set; }
        public FaturamentoContaDto FaturamentoConta { get; set; }

        public long? UnidadeOrganizacionalId { get; set; }
        public long? TerceirizadoId { get; set; }
        public long? TurnoId { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFim { get; set; }
        public float? Quantidade { get; set; }


        public static FaturamentoPacoteDto Mapear(FaturamentoPacote faturamentoPacote)
        {
            FaturamentoPacoteDto faturamentoPacoteDto = new FaturamentoPacoteDto();

            faturamentoPacoteDto.Id = faturamentoPacote.Id;
            faturamentoPacoteDto.Codigo = faturamentoPacote.Codigo;
            faturamentoPacoteDto.Descricao = faturamentoPacote.Descricao;

            faturamentoPacoteDto.Inicio = faturamentoPacote.Inicio;
            faturamentoPacoteDto.Final = faturamentoPacote.Final;
            faturamentoPacoteDto.FaturamentoItemId = faturamentoPacote.FaturamentoItemId;

            faturamentoPacoteDto.FaturamentoContaId = faturamentoPacote.FaturamentoContaId;

            if (faturamentoPacote.FaturamentoItem != null)
            {
                faturamentoPacoteDto.FaturamentoItem = FaturamentoItemDto.Mapear(faturamentoPacote.FaturamentoItem);
            }

            if (faturamentoPacote.FaturamentoConta != null)
            {
                faturamentoPacoteDto.FaturamentoConta = FaturamentoContaDto.Mapear(faturamentoPacote.FaturamentoConta);
            }

            return faturamentoPacoteDto;
        }

        public static FaturamentoPacote Mapear(FaturamentoPacoteDto faturamentoPacoteDto)
        {
            FaturamentoPacote faturamentoPacote = new FaturamentoPacote();

            faturamentoPacote.Id = faturamentoPacoteDto.Id;
            faturamentoPacote.Codigo = faturamentoPacoteDto.Codigo;
            faturamentoPacote.Descricao = faturamentoPacoteDto.Descricao;

            faturamentoPacote.Inicio = faturamentoPacoteDto.Inicio;
            faturamentoPacote.Final = faturamentoPacoteDto.Final;
            faturamentoPacote.FaturamentoItemId = faturamentoPacoteDto.FaturamentoItemId;

            faturamentoPacote.FaturamentoContaId = faturamentoPacoteDto.FaturamentoContaId;

            if (faturamentoPacote.FaturamentoItem != null)
            {
                faturamentoPacote.FaturamentoItem = FaturamentoItemDto.Mapear(faturamentoPacoteDto.FaturamentoItem);
            }

            return faturamentoPacote;
        }

    }
}
