﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Pacotes.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Uow;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Pacotes
{
    public class FaturamentoPacoteAppService : SWMANAGERAppServiceBase, IFaturamentoPacoteAppService
    {
        private readonly IRepository<FaturamentoPacote, long> _faturamentoPacoteRepository;
        private readonly IRepository<FaturamentoContaItem, long> _faturamentoContaItemRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<FaturamentoConta, long> _faturamentoContaRepository;
        private readonly IFaturamentoContaItemAppService _faturamentoContaItemAppService;

        public FaturamentoPacoteAppService(IRepository<FaturamentoPacote, long> faturamentoPacoteRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<FaturamentoContaItem, long> faturamentoContaItemRepository
            , IRepository<FaturamentoConta, long> faturamentoContaRepository
            , IFaturamentoContaItemAppService faturamentoContaItemAppService)
        {
            _faturamentoPacoteRepository = faturamentoPacoteRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _faturamentoContaItemRepository = faturamentoContaItemRepository;
            _faturamentoContaRepository = faturamentoContaRepository;
            _faturamentoContaItemAppService = faturamentoContaItemAppService;
        }

        public FaturamentoPacoteDto Obter(long id)
        {
            var pacote = _faturamentoPacoteRepository.GetAll()
                                                     .Where(w => w.Id == id)
                                                     .FirstOrDefault();

            if (pacote != null)
            {
                return FaturamentoPacoteDto.Mapear(pacote);
            }

            return null;
        }

        public async Task<PagedResultDto<PacoteDto>> ListarPacotesPorConta(ListarFaturamentoPacoteInput input)
        {

            var query = _faturamentoPacoteRepository.GetAll()
                                                    .Include(i => i.FaturamentoItem)
                                                    .Where(w => w.FaturamentoContaId == input.ContaId);

            var pacotesCount = await query.CountAsync();

            var pacotes = await query
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();


            List<PacoteDto> pacotesDto = new List<PacoteDto>();

            foreach (var item in pacotes)
            {
                PacoteDto pacoteDto = new PacoteDto();
                pacoteDto.Id = item.Id;
                pacoteDto.Descricao = item.FaturamentoItem.Descricao;
                pacoteDto.Inicio = item.Inicio;
                pacoteDto.Final = item.Final;

                pacotesDto.Add(pacoteDto);
            }


            return new PagedResultDto<PacoteDto>(
              pacotesCount,
              pacotesDto
              );



        }

        public DefaultReturn<FaturamentoPacoteDto> InserirPacote(FaturamentoPacoteDto pacoteDto)
        {
            var _retornoPadrao = new DefaultReturn<FaturamentoPacoteDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var pacote = new FaturamentoPacote();

                    pacote.Inicio = pacoteDto.Inicio;
                    pacote.Final = pacoteDto.Final;
                    pacote.FaturamentoItemId = pacoteDto.FaturamentoItemId;
                    pacote.FaturamentoContaId = pacoteDto.FaturamentoContaId;

                    //  long.Parse("");

                    var pacoteId = AsyncHelper.RunSync(() => _faturamentoPacoteRepository.InsertAndGetIdAsync(pacote));



                    var faturamentoContaItens = _faturamentoContaItemRepository.GetAll()
                                                                          .Where(w => w.FaturamentoContaId == pacoteDto.FaturamentoContaId
                                                                                   && w.Data >= pacoteDto.Inicio
                                                                                   && w.Data <= pacoteDto.Final
                                                                                   && w.FaturamentoPacoteId == null
                                                                                   && w.FaturamentoItem.Grupo.TipoGrupoId != 4
                                                                                   )
                                                                          .ToList();

                    foreach (var item in faturamentoContaItens)
                    {
                        item.FaturamentoPacoteId = pacoteId;
                    }


                    var conta = _faturamentoContaRepository.GetAll()
                                                           .Where(w => w.Id == pacoteDto.FaturamentoContaId)
                                                           .FirstOrDefault();

                    if (conta != null)
                    {
                        FaturamentoContaItemInsertDto faturamentoContaItemInsertDto = new FaturamentoContaItemInsertDto();

                        faturamentoContaItemInsertDto.AtendimentoId = (long)conta.AtendimentoId;
                        //faturamentoContaItemInsertDto.CentroCustoId =

                        faturamentoContaItemInsertDto.Data = DateTime.Now.Date;
                        faturamentoContaItemInsertDto.MedicoId = conta.MedicoId;
                        faturamentoContaItemInsertDto.Qtd = pacoteDto.Quantidade ?? 1;

                        List<FaturamentoContaItemDto> itensFaturamento = new List<FaturamentoContaItemDto>();

                        var faturamentoContaItemDto = new FaturamentoContaItemDto { Id = (long)pacote.FaturamentoItemId, Qtde = (float)faturamentoContaItemInsertDto.Qtd };
                        faturamentoContaItemInsertDto.UnidadeOrganizacionalId = pacoteDto.UnidadeOrganizacionalId;
                        faturamentoContaItemInsertDto.TurnoId = pacoteDto.TurnoId;
                        faturamentoContaItemDto.HoraIncio = pacoteDto.HoraInicio;
                        faturamentoContaItemDto.HoraFim = pacoteDto.HoraFim;
                        faturamentoContaItemDto.FaturamentoPacoteId = pacoteId;
                        faturamentoContaItemDto.FaturamentoContaId = pacoteDto.FaturamentoContaId;

                        itensFaturamento.Add(faturamentoContaItemDto);

                        faturamentoContaItemInsertDto.ItensFaturamento = itensFaturamento;


                        _faturamentoContaItemAppService.InserirItensContaFaturamento(faturamentoContaItemInsertDto);



                    }


                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();



                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }

                throw (ex);
            }
            return _retornoPadrao;
        }

        public async Task<ResultDropdownList> ListarDropdownPacoteConta(DropdownInput dropdownInput)
        {
            long contaId;

            long.TryParse(dropdownInput.filtro, out contaId);


            return await ListarDropdownLambda(dropdownInput
                                              , _faturamentoPacoteRepository
                                              , m => (m.FaturamentoContaId == contaId
                                                  && (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                                       m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()))
                                                       )
                                              , p => new DropdownItems { id = p.Id, text = string.Concat(p.FaturamentoItem.Codigo, " - ", p.FaturamentoItem.Descricao) }
                                              , o => o.Descricao
                                              );
        }



        public async Task ExcluirPacote(long id)
        {
            using (var unitOfWork = UnitOfWorkManager.Begin())
            {
                var itemContaFaturamentoPacote = _faturamentoContaItemRepository.GetAll()
                                                                            .Where(w => w.FaturamentoPacoteId == id
                                                                                      && w.FaturamentoItem.Grupo.TipoGrupoId == 4)
                                                                            .FirstOrDefault();


               await _faturamentoContaItemRepository.DeleteAsync(itemContaFaturamentoPacote);



                var itensContaFaturamento = _faturamentoContaItemRepository.GetAll()
                                                                            .Where(w => w.FaturamentoPacoteId == id)
                                                                            .ToList();

                foreach (var item in itensContaFaturamento)
                {
                    item.FaturamentoPacoteId = null;
                }

                await _faturamentoPacoteRepository.DeleteAsync(id);

               // await _faturamentoContaItemRepository.DeleteAsync(id);

                unitOfWork.Complete();
                unitOfWork.Dispose();

            }
        }

    }
}
