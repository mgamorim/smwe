﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Pacotes.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Pacotes
{
    public interface IFaturamentoPacoteAppService : IApplicationService
    {
        FaturamentoPacoteDto Obter(long id);
        Task<PagedResultDto<PacoteDto>> ListarPacotesPorConta(ListarFaturamentoPacoteInput input);
        DefaultReturn<FaturamentoPacoteDto> InserirPacote(FaturamentoPacoteDto pacoteDto);
        Task<ResultDropdownList> ListarDropdownPacoteConta(DropdownInput dropdownInput);
        Task ExcluirPacote(long id);
    }
}
