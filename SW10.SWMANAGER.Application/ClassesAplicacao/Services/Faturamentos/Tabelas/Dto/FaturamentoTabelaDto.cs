﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Tabelas;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Tabelas.Dto
{
    [AutoMap(typeof(FaturamentoTabela))]
    public class FaturamentoTabelaDto : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }
        
        public string Descricao { get; set; }

        public bool IsAtualizaBrasindice { get; set; }

        //[ForeignKey("TabelaTissItemId")]
        //public virtual TabelaTissItem TabelaTissItem { get; set; }
        public long? TabelaTissItemId { get; set; }

        public bool IsCBHPM { get; set; }


        public static FaturamentoTabelaDto Mapear(FaturamentoTabela faturamentoTabela)
        {
            FaturamentoTabelaDto faturamentoTabelaDto = new FaturamentoTabelaDto();

            faturamentoTabelaDto.Id = faturamentoTabela.Id;
            faturamentoTabelaDto.Codigo = faturamentoTabela.Codigo;
            faturamentoTabelaDto.Descricao = faturamentoTabela.Descricao;
            faturamentoTabelaDto.IsAtualizaBrasindice = faturamentoTabela.IsAtualizaBrasindice;
            faturamentoTabelaDto.TabelaTissItemId = faturamentoTabela.TabelaTissItemId;
            faturamentoTabelaDto.IsCBHPM = faturamentoTabela.IsCBHPM;

            return faturamentoTabelaDto;
        }

        public static FaturamentoTabela Mapear(FaturamentoTabelaDto faturamentoTabelaDto)
        {
            FaturamentoTabela faturamentoTabela = new FaturamentoTabela();

            faturamentoTabela.Id = faturamentoTabelaDto.Id;
            faturamentoTabela.Codigo = faturamentoTabelaDto.Codigo;
            faturamentoTabela.Descricao = faturamentoTabelaDto.Descricao;
            faturamentoTabela.IsAtualizaBrasindice = faturamentoTabelaDto.IsAtualizaBrasindice;
            faturamentoTabela.TabelaTissItemId = faturamentoTabelaDto.TabelaTissItemId;
            faturamentoTabela.IsCBHPM = faturamentoTabelaDto.IsCBHPM;

            return faturamentoTabela;
        }

    }
}
