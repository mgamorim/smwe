﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using SW10.SWMANAGER.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes.Dto
{
    public class ListarOperacaoInput : ListarInput
    {
        public long? ModuloId { get; set; }
    }
}
