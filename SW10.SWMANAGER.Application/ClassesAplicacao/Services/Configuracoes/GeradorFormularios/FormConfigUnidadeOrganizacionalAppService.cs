﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class FormConfigUnidadeOrganizacionalAppService : SWMANAGERAppServiceBase, IFormConfigUnidadeOrganizacionalAppService
    {
        private readonly IRepository<FormConfigUnidadeOrganizacional, long> _formConfigUnidadeOrganizacionalRepository;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FormConfigUnidadeOrganizacionalAppService(
            IRepository<FormConfigUnidadeOrganizacional, long> formConfigUnidadeOrganizacionalRepository,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _formConfigUnidadeOrganizacionalRepository = formConfigUnidadeOrganizacionalRepository;
            _unidadeOrganizacionalService = unidadeOrganizacionalService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task CriarOuEditar(FormConfigUnidadeOrganizacionalDto input)
        {
            try
            {
                var unidadesIncluidas = string.IsNullOrWhiteSpace(input.UnidadesIncluidas) ? new string[0] : input.UnidadesIncluidas.Split(',');
                var unidadesRemovidas = string.IsNullOrWhiteSpace(input.UnidadesRemovidas) ? new string[0] : input.UnidadesRemovidas.Split(',');
                //var formConfigUnidadeOrganizacional = input.MapTo<FormConfigUnidadeOrganizacional>();
                //if (input.Id.Equals(0))
                //{
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        input.Id = await _formConfigUnidadeOrganizacionalRepository.InsertAndGetIdAsync(formConfigUnidadeOrganizacional);
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}
                //else
                //{
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        await _formConfigUnidadeOrganizacionalRepository.UpdateAsync(formConfigUnidadeOrganizacional);
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}
                foreach (var unidadeOrganizacionalId in unidadesIncluidas)
                {
                    var formConfigUnidadeOrganizacional = new FormConfigUnidadeOrganizacional
                    {
                        FormConfigId = input.FormConfigId,
                        IsDeleted = false,
                        IsSistema = false,
                        CreatorUserId = AbpSession.UserId,
                        UnidadeOrganizacionalId = Convert.ToInt64(unidadeOrganizacionalId)
                    };
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formConfigUnidadeOrganizacionalRepository.InsertAsync(formConfigUnidadeOrganizacional);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                foreach (var unidadeOrganizacionalId in unidadesRemovidas)
                {
                    var formConfigUnidadeOrganizacional = await Obter(input.FormConfigId.Value, Convert.ToInt64(unidadeOrganizacionalId));

                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formConfigUnidadeOrganizacionalRepository.DeleteAsync(formConfigUnidadeOrganizacional.Id);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(FormConfigUnidadeOrganizacionalDto input)
        {
            try
            {
                await _formConfigUnidadeOrganizacionalRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FormConfigUnidadeOrganizacionalDto>> Listar(ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<FormConfigUnidadeOrganizacional> formsConfig;
            List<FormConfigUnidadeOrganizacionalDto> formsConfigDto;
            try
            {
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.FormConfig.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.UnidadeOrganizacional.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.FormConfig.DataAlteracao.ToShortTimeString().ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                formsConfigDto = formsConfig.MapTo<List<FormConfigUnidadeOrganizacionalDto>>();

                return new PagedResultDto<FormConfigUnidadeOrganizacionalDto>(
                    contarGeradorFormularios,
                    //formsConfigResult
                    formsConfigDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigUnidadeOrganizacionalDto>> ListarTodos()
        {
            try
            {
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigUnidadeOrganizacionalDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigUnidadeOrganizacionalDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfigUnidadeOrganizacionalDto> Obter(long id)
        {
            try
            {
                var formConfig = new FormConfigUnidadeOrganizacional();
                var formConfigDto = new FormConfigUnidadeOrganizacionalDto();
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .Where(m => m.Id == id);

                formConfig = await query
                     .FirstOrDefaultAsync();

                formConfigDto = formConfig.MapTo<FormConfigUnidadeOrganizacionalDto>();

                return formConfigDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfigUnidadeOrganizacionalDto> Obter(long formConfigId, long unidadeOrganizacionalId)
        {
            try
            {
                var formConfig = new FormConfigUnidadeOrganizacional();
                var formConfigDto = new FormConfigUnidadeOrganizacionalDto();
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .Where(m =>
                        m.FormConfigId == formConfigId &&
                        m.UnidadeOrganizacionalId == unidadeOrganizacionalId
                    );

                formConfig = await query
                     .FirstOrDefaultAsync();

                formConfigDto = formConfig.MapTo<FormConfigUnidadeOrganizacionalDto>();

                return formConfigDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigUnidadeOrganizacionalDto>> ListarPorForm(long id)
        {
            try
            {
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .Where(m => m.FormConfigId == id);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigUnidadeOrganizacionalDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigUnidadeOrganizacionalDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<UnidadeOrganizacionalDto>> ListarUnidadeOrganizacionalPorForm(ListarInput input)
        {
            try
            {
                long id = 0;
                var result = long.TryParse(input.PrincipalId, out id);
                if (!result)
                {
                    throw new UserFriendlyException(L("IdNaoInformado"));
                }
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.UnidadeOrganizacional.OrganizationUnit)
                    .Where(m => m.FormConfigId == id)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                         m.UnidadeOrganizacional.OrganizationUnit.Code.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.UnidadeOrganizacional.OrganizationUnit.DisplayName.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Select(m => m.UnidadeOrganizacional);

                var query1 = await query.ToListAsync();
                var unidades = query1.Select(m => m.Id).ToArray();

                //var unidades = associadas.Select(m => m.Id).ToArray();
                var disponiveis = await _unidadeOrganizacionalService.ListarTodos();
                var unidadesOrganizacionais = disponiveis.Items
                    .Where(m => m.Id.IsIn(unidades) && (m.IsAmbulatorioEmergencia || m.IsInternacao))
                    .AsQueryable();

                var count = unidadesOrganizacionais.Count();

                var unidadesOrganizacionaisDto = unidadesOrganizacionais
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();


                //var unidadesOrganizacionaisDto = unidadesOrganizacionais.MapTo<List<UnidadeOrganizacionalDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new PagedResultDto<UnidadeOrganizacionalDto>
                {
                    TotalCount = count,
                    Items = unidadesOrganizacionaisDto
                };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<UnidadeOrganizacionalDto>> ListarUnidadeOrganizacionalSemForm(ListarInput input)
        {
            try
            {
                long id = 0;
                var result = long.TryParse(input.PrincipalId, out id);
                if (!result)
                {
                    throw new UserFriendlyException(L("IdNaoInformado"));
                }
                //var associadas = await ListarUnidadeOrganizacionalPorForm(input);
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.UnidadeOrganizacional.OrganizationUnit)
                    .Where(m => m.FormConfigId == id)
                    .Select(m => m.UnidadeOrganizacional);

                var associadas = await query.ToListAsync();

                var unidades = associadas.Select(m => m.Id).ToArray();

                var disponiveis = await _unidadeOrganizacionalService.ListarTodos();

                var unidadesDisponiveis = disponiveis.Items
                    .Where(m => !m.Id.IsIn(unidades) && (m.IsAmbulatorioEmergencia || m.IsInternacao))
                    .ToList();

                var unidadesFiltradas = unidadesDisponiveis
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                         m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .AsQueryable();

                var count = unidadesFiltradas.Count();
                var unidadesDto = unidadesFiltradas
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList()
                    .MapTo<List<UnidadeOrganizacionalDto>>();

                return new PagedResultDto<UnidadeOrganizacionalDto>
                {
                    TotalCount = count,
                    Items = unidadesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigUnidadeOrganizacionalDto>> ListarPorUnidadeOrganizacional(long id)
        {
            try
            {
                var query = _formConfigUnidadeOrganizacionalRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.UnidadeOrganizacional)
                    .Where(m => m.UnidadeOrganizacionalId == id);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigUnidadeOrganizacionalDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigUnidadeOrganizacionalDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from p in _formConfigUnidadeOrganizacionalRepository
                            .GetAll()
                            .Include(m => m.FormConfig)
                            .Include(m => m.UnidadeOrganizacional)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.FormConfig.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.FormConfig.Nome.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.FormConfig.Nome ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.FormConfig.Codigo, " - ", p.FormConfig.Nome, " / ", p.UnidadeOrganizacional.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
