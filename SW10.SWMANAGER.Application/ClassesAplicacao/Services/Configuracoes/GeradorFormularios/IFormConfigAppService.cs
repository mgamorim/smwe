﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System.Linq;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public interface IFormConfigAppService : IApplicationService
    {
        Task<PagedResultDto<FormConfig>> Listar(ListarInput input);

        Task<ListResultDto<FormConfig>> ListarTodos();

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task CriarOuEditar(FormConfig input);

        Task Excluir(FormConfig input);

        Task<FormConfig> Obter(long id);

        Task<FormConfig> Clonar(long id);

        Task<ResultDropdownList> ListarRelacionadosDropdown(DropdownInput dropdownInput);

        Task<IQueryable<FormConfig>> ListarRelacionados(long operacaoId, long unidadeOrganizacionalId, long? especialidadeId);

        // Reservados
        Task<long> CriarReservadoAndGetId ();

        Task<long> ObterReservadoId ();

        Task<FormConfig> ListarReservados ();

        string ObterValorUltimoLancamento (string colConfigName, long? atendimentoId);

    }
}
