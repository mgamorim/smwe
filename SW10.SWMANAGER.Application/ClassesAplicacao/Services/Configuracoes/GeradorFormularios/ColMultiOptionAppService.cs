﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class ColMultiOptionAppService : SWMANAGERAppServiceBase, IColMultiOptionAppService
    {
        private readonly IRepository<ColMultiOption, long> _colMultiOptionRepository;

        public ColMultiOptionAppService(IRepository<ColMultiOption, long> colMultiOptionRepository)
        {
            _colMultiOptionRepository = colMultiOptionRepository;
        }

        public async Task CriarOuEditar(ColMultiOption input)
        {
            try
            {
                //var formConfig = input.MapTo<ColMultiOption>();
                if (input.Id.Equals(0))
                {
                    await _colMultiOptionRepository.InsertAsync(input);
                }
                else
                {
                    await _colMultiOptionRepository.UpdateAsync(input);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(ColMultiOption input)
        {
            try
            {
                await _colMultiOptionRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<ColMultiOption>> Listar(ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<ColMultiOption> formsConfig;
            //List<ColMultiOptionDto> formsConfigDtos = new List<ColMultiOptionDto>();
            try
            {
                var query = _colMultiOptionRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Opcao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //formsConfigDtos = formsConfig
                //    .MapTo<List<ColMultiOptionDto>>();

                return new PagedResultDto<ColMultiOption>(
                    contarGeradorFormularios,
                    formsConfig
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<ColMultiOption>> ListarTodos()
        {
            try
            {
                var query = await _colMultiOptionRepository
                    .GetAllListAsync();

                //var formsConfig = query.MapTo<List<ColMultiOptionDto>>();
                return new ListResultDto<ColMultiOption> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ColMultiOption> Obter(long id)
        {
            try
            {
                var result = await _colMultiOptionRepository
                    .GetAllListAsync(m => m.Id == id);

                var formConfig = result
                    .FirstOrDefault();
                    //.MapTo<ColMultiOptionDto>();

                return formConfig;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
