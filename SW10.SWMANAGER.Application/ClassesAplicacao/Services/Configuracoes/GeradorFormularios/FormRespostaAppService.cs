﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class FormRespostaAppService : SWMANAGERAppServiceBase, IFormRespostaAppService
    {
        private readonly IRepository<FormResposta, long> _formRespostaRepository;
        private readonly IFormConfigAppService _formConfigAppService;
        private readonly IColConfigAppService _colConfigAppService;
        private readonly IFormDataAppService _formDataAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private static List<FormData> _dados = new List<FormData>();

        public FormRespostaAppService(
            IRepository<FormResposta, long> formRespostaRepository,
            IFormConfigAppService formConfigAppService,
            IColConfigAppService colConfigAppService,
            IFormDataAppService formDataAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _formRespostaRepository = formRespostaRepository;
            _formConfigAppService = formConfigAppService;
            _colConfigAppService = colConfigAppService;
            _formDataAppService = formDataAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }
        [UnitOfWork]
        public async Task<long> CriarOuEditar(FormConfig formConfig, long idDadosResposta, string nomeClasse, string registroClasseId)
        {
            try
            {
                long result = 0;
                if (formConfig == null || formConfig.Id == 0 || formConfig.Linhas == null)
                {
                    throw new Exception();
                }
                //var resposta = new FormResposta();
                //dados = new List<FormData>();
                //var colunas1 = formConfig.Linhas.Select(s => s.Col1.Id).ToList();
                //var colunas2 = formConfig.Linhas.Select(s => s.Col2);
                //var valColunas2 = colunas2.Where(s => s != null).ToList().Select(s => s.Id).ToList();
                //var colunasId = new List<long>();
                //colunasId.AddRange(colunas1);
                //colunasId.AddRange(valColunas2);
                //colunasId = colunasId.Distinct().ToList();
                var resposta = new FormResposta();
                _dados = new List<FormData>();
                var colunasId = formConfig.Linhas.SelectMany(s => s.ColConfigs).Select(s => s.Id).ToList();
                colunasId = colunasId.Distinct().ToList();

                var idConfig = formConfig.Id;
                resposta = new FormResposta
                {
                    DataResposta = DateTime.Now,
                    NomeClasse = nomeClasse,
                    RegistroClasseId = registroClasseId,
                    FormConfig = await _formConfigAppService.Obter(idConfig)

                };
                var qColunasBase = await _colConfigAppService.ListarTodos();
                var colunasBase = qColunasBase.Items.Where(m => colunasId.Contains(m.Id)).ToList();

                formConfig.Linhas.ForEach(f =>
                {
                    //if (f.Col1 != null && f.Col2 != null)
                    //{
                    //    var col1 = colunasBase.FirstOrDefault(w => w.Id == f.Col1.Id);
                    //    var col2 = colunasBase.FirstOrDefault(w => w.Id == f.Col2.Id);
                    //    ProcessarValor(f.Col1, col1, f.Col1.MultiOption?.Where(s => s.Selecionado == true).ToList(), idDadosResposta);
                    //    ProcessarValor(f.Col2, col2, f.Col2.MultiOption?.Where(s => s.Selecionado == true).ToList(), idDadosResposta);

                    //}
                    //else if (f.Col1 != null && f.Col2 == null)
                    //{
                    //    var col1 = colunasBase.FirstOrDefault(w => w.Id == f.Col1.Id);
                    //    ProcessarValor(f.Col1, col1, f.Col1.MultiOption?.Where(s => s.Selecionado == true).ToList(), idDadosResposta);
                    //}
                    f.ColConfigs.ForEach(ff =>
                    {
                        if (ff != null)
                        {
                            ProcessarValor(ff, _dados, colunasBase.FirstOrDefault(w => w.Id == ff.Id), ff.MultiOption?.Where(s => s.Selecionado == true).ToList(), idDadosResposta);
                        }
                    });
                });

                //Inclusão
                if (idDadosResposta == 0)
                {
                    resposta.ColRespostas = _dados;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        result = await _formRespostaRepository.InsertAndGetIdAsync(resposta);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else // edição
                {
                    result = idDadosResposta;
                    var respostaBase = await ObterNoLazy(idDadosResposta);
                    respostaBase.DataResposta = DateTime.Now;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formRespostaRepository.UpdateAsync(respostaBase);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                    var resp = new List<long>();
                    resp = respostaBase.ColRespostas.Select(m => m.Id).ToList();
                    foreach (var item in resp)
                    {
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            await _formDataAppService.Excluir(item);
                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }
                    }
                    foreach (var item in _dados)
                    {
                        item.Resposta = null;
                        item.Coluna = null;
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            await _formDataAppService.CriarOuEditar(item);
                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(FormResposta input)
        {
            try
            {
                await _formRespostaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FormResposta>> Listar(ListarFormRespostaInput input)
        {
            var contarGeradorFormularios = 0;
            List<FormResposta> formsResposta;
            try
            {
                var query = _formRespostaRepository
                    .GetAll()
                    .Where(m => m.FormConfigId == input.FormId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.DataResposta.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                    )
                    //.Include(i => i.FormConfig)
                    //.Include(i => i.FormConfig.Linhas.Select(ss => ss.Col1.MultiOption))
                    //.Include(i => i.FormConfig.Linhas.Select(ss => ss.Col2.MultiOption))
                    //.Include(i => i.FormConfig.Linhas.Select(ss => ss.ColConfigs.Select(sss => sss.MultiOption)));
                    .Include(i => i.ColRespostas.Select(ss => ss.Coluna.MultiOption));

                contarGeradorFormularios = await query
                    .CountAsync();

                formsResposta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
                var formsRespostaResult = formsResposta
                    .Select(m => new FormResposta
                    {
                        Id = m.Id,
                        DataResposta = m.DataResposta,
                        LastModificationTime = m.LastModificationTime,
                        CreationTime = m.CreationTime,
                        ColRespostas = formsResposta
                        .SelectMany(f => f.ColRespostas)
                        .Where(f => f.FormRespostaId == m.Id)
                        .Select(c => new FormData
                        {
                            ColConfigId = c.ColConfigId,
                            Coluna = new ColConfig
                            {
                                Name = c.Coluna.Name,
                                Value = c.Coluna.Value,
                                CreationTime = c.Coluna.CreationTime,
                                LastModificationTime = c.Coluna.LastModificationTime
                            },
                            Id = c.Id,
                            FormRespostaId = c.FormRespostaId,
                            Valor = c.Valor,
                            CreationTime = c.CreationTime,
                            LastModificationTime = c.LastModificationTime
                        }).Distinct().ToList()
                    }).Distinct().ToList();

                return new PagedResultDto<FormResposta>(
                    contarGeradorFormularios,
                    formsRespostaResult
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormResposta>> ListarTodos()
        {
            try
            {
                //var query = await _formRespostaRepository
                //    .GetAllListAsync();
                var query = _formRespostaRepository
                    .GetAll()
                    .Include(m => m.ColRespostas)
                    .Include(m => m.FormConfig);
                //.Include(m => m.FormConfig.Linhas);

                var forms = await query
                    .ToListAsync();

                //var formsResposta = forms.MapTo<List<FormRespostaDto>>();
                return new ListResultDto<FormResposta> { Items = forms };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormResposta> Obter(long id)
        {
            try
            {
                var query = _formRespostaRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Include(i => i.FormConfig)
                    
                    //.Include(i => i.FormConfig.Linhas.Select(ss => ss.Col1.MultiOption))
                    //.Include(i => i.FormConfig.Linhas.Select(ss => ss.Col2.MultiOption))
                    .Include(i => i.FormConfig.Linhas.Select(ss => ss.ColConfigs.Select(sss => sss.MultiOption)))
                    .Include(i => i.ColRespostas);

                var result = await query
                    .FirstOrDefaultAsync();

                //var formConfig = result
                //    .MapTo<FormRespostaDto>();

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormResposta> ObterNoLazy(long id)
        {
            try
            {
                //return await Task.FromResult(_formRespostaRepository.Get(id));
                return await _formRespostaRepository
                    .GetAllIncluding(m => m.ColRespostas)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                //var query = _formRespostaRepository
                //    .Get()
                //    .GetAll()
                //    .Where(m => m.Id == id)
                //.Select(m => new FormResposta
                //{
                //    CreationTime = m.CreationTime,
                //    CreatorUserId = m.CreatorUserId,
                //    DataResposta = m.DataResposta,
                //    FormConfigId = m.FormConfigId,
                //    Id = m.Id,
                //    IsDeleted = m.IsDeleted,
                //    IsSistema = m.IsSistema,
                //    LastModificationTime = m.LastModificationTime,
                //    LastModifierUserId = m.LastModifierUserId
                //});


                //var result = await query
                //    .FirstOrDefaultAsync();
                //result.FormConfig = null;
                //result.ColRespostas = await _formDataAppService.ListarNoLazy(id);
                ////var formConfig = result
                ////    .MapTo<FormRespostaDto>();

                //return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //private static void ProcessarValor(ColConfig Col, ColConfig coluna, List<ColMultiOption> multiSelected, long formRespostaId) 
        //{
        //    if (Col.Type == "checkbox" && multiSelected != null)
        //    {
        //        for (int i = 0; i < multiSelected.Count; i++)
        //        {
        //            dados.Add(new FormData
        //            {
        //                Valor = multiSelected[i].Opcao,
        //                //Coluna = coluna,
        //                ColConfigId = coluna.Id,
        //                FormRespostaId = formRespostaId,
        //                CreatorUserId = Col.Id 
        //            });
        //        }
        //    }
        //    else
        //    {
        //        dados.Add(new FormData
        //        {
        //            Valor = Col.Value,
        //            //Coluna = coluna,
        //            ColConfigId = coluna.Id,
        //            FormRespostaId = formRespostaId,
        //            CreatorUserId = coluna.CreatorUserId

        //        });
        //    }
        //}
        private static void ProcessarValor(ColConfig Col, List<FormData> dados, ColConfig coluna, List<ColMultiOption> multiSelected, long formRespostaId)
        {

            if (Col.Type == "checkbox" && multiSelected != null)
            {
                var valor = string.Empty;
                for (int i = 0; i < multiSelected.Count; i++)
                {
                    //valor += multiSelected[i].Opcao + (i < (multiSelected.Count - 1) ? "," : "");
                    dados.Add(new FormData
                    {
                        Valor = multiSelected[i].Opcao,
                        Coluna = coluna,
                        ColConfigId = coluna.Id,
                        FormRespostaId = formRespostaId
                    });
                }
            }
            else
            {
                var valor = string.Empty;
                //if (formRespostaId > 0)
                //{
                //    valor = Col.Valores.Where(m => m.FormRespostaId == formRespostaId && m.ColConfigId == coluna.Id).FirstOrDefault().Valor;
                //}
                //else
                //{
                valor = Col.Value;
                //}
                dados.Add(new FormData
                {
                    Valor = valor,
                    Coluna = coluna,
                    ColConfigId = coluna.Id,
                    FormRespostaId = formRespostaId
                });
            }
            _dados = dados;
        }
    }
}
