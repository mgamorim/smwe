﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.EntityFramework;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes;
using Abp.Runtime.Caching;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class FormConfigAppService : SWMANAGERAppServiceBase, IFormConfigAppService
    {
        #region Cabecalho
        private readonly ICacheManager _cacheManager;

        private readonly IRepository<FormConfig, long> _formConfigRepository;
        private readonly IRepository<FormResposta, long> _formRespostaRepository;
        private readonly IRepository<RowConfig, long> _rowConfigRepository;
        private readonly IRepository<Prontuario, long> _prontuarioRepository;

        private readonly IRepository<ColConfig, long> _colConfigRepository;
        private readonly IColConfigAppService _colConfigAppService;
        List<ColConfig> _colConfigsParaUpdate = new List<ColConfig>();

        private readonly IFormConfigUnidadeOrganizacionalAppService _formConfigUnidadeOrganizacionalAppService;
        private readonly IFormConfigOperacaoAppService _formConfigOperacaoAppService;
        private readonly IOperacaoAppService _operacaoAppService;
        private readonly IFormConfigEspecialidadeAppService _formConfigEspecialidadeAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManagerAppService;

        //      private readonly SWMANAGERDbContext _dbContext;

        public FormConfigAppService (
            ICacheManager cacheManager,

            IRepository<FormConfig, long> formConfigRepository,
            IRepository<FormResposta, long> formRespostaRepository,
            IRepository<RowConfig, long> rowConfigRepository,
            IRepository<Prontuario, long> prontuarioRepository,

            IRepository<ColConfig, long> colConfigRepository,
            IColConfigAppService colConfigAppService,

            IFormConfigUnidadeOrganizacionalAppService formConfigUnidadeOrganizacionalAppService,
            IFormConfigOperacaoAppService formConfigOperacaoAppService,
            IFormConfigEspecialidadeAppService formConfigEspecialidadeAppService,
            IOperacaoAppService operacaoAppService,
            IUnitOfWorkManager unitOfWorkManagerAppService

            //  SWMANAGERDbContext dbContext
            )
        {
            _cacheManager = cacheManager;

            _formConfigRepository = formConfigRepository;
            _formRespostaRepository = formRespostaRepository;
            _rowConfigRepository = rowConfigRepository;
            _prontuarioRepository = prontuarioRepository;

            _colConfigRepository = colConfigRepository;
            _colConfigAppService = colConfigAppService;

            _formConfigUnidadeOrganizacionalAppService = formConfigUnidadeOrganizacionalAppService;
            _formConfigOperacaoAppService = formConfigOperacaoAppService;
            _formConfigEspecialidadeAppService = formConfigEspecialidadeAppService;
            _operacaoAppService = operacaoAppService;

            _unitOfWorkManagerAppService = unitOfWorkManagerAppService;
      //      _dbContext = dbContext;
        }

        #endregion cabecalho.
        [UnitOfWork]
        public async Task CriarOuEditar (FormConfig input)
        {
            try
            {
                // O codigo em FormularioApp.js verifica campos reservados atraves do Name
                // da ColConfig, sempre usando 'toUpperCase()', portanto, sempre salvar ColConfig.Name
                // usando 'ToUpper()'
                foreach (var linha in input.Linhas)
                {
                    foreach (var col in linha.ColConfigs)
                    {
                        col.Name = col.Name.ToUpper();
                    }
                }

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManagerAppService.Begin())
                    {
                        await _formConfigRepository.InsertAsync(input);
                        unitOfWork.Complete();
                        _unitOfWorkManagerAppService.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    //await _formConfigRepository.UpdateAsync(input);

                    //foreach(var lin in input.Linhas)
                    //{
                    //    foreach (var colConfig in lin.ColConfigs)
                    //    {
                    //        await _colConfigAppService.CriarOuEditar(colConfig);
                    //    }
                    //}
                    using (var unitOfWork = _unitOfWorkManagerAppService.Begin())
                    {
                        await Editar(input);
                        unitOfWork.Complete();
                        _unitOfWorkManagerAppService.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Editar (FormConfig formConfig)
        {
            try
            {
                var cache = _cacheManager.GetCache("ColConfigCache");

                using (var contexto = new SWMANAGERDbContext())
                {
                    var formConfigExistente = _formConfigRepository.GetAllIncluding(f => f.Linhas).FirstOrDefault(j => j.Id == formConfig.Id);

                    List<RowConfig> itensDeletados = new List<RowConfig>();

                    foreach (var it in formConfigExistente.Linhas)
                    {
                        if (formConfig.Linhas.FirstOrDefault(ite => ite.Id == it.Id) == null)
                            itensDeletados.Add(it);
                    }

                    List<RowConfig> itensAdicionados = new List<RowConfig>();

                    foreach (var it in formConfig.Linhas)
                    {
                        var linha = formConfigExistente.Linhas.FirstOrDefault(ite => ite.Id == it.Id);
                        if (linha == null)
                        {
                            itensAdicionados.Add(it);
                        }
                        else
                        {
                            foreach (var colConfig in it.ColConfigs)
                            {
                                //   contexto.Entry(colConfig).State = EntityState.Detached;
                                //       contexto.Entry(colConfig).State = EntityState.Modified;
                                _colConfigsParaUpdate.Add(colConfig);
                                //   contexto.Entry(colConfig).State = EntityState.Added;
                            }
                            //     cache.Set(colConfig.Id.ToString(), colConfig);
                        }
                    }

                    cache.Set("ColConfigsParaUpdate", _colConfigsParaUpdate);

                    itensDeletados.ForEach(c => formConfigExistente.Linhas.Remove(c));

                    foreach (var c in itensAdicionados)
                    {
                        contexto.Entry(c).State = EntityState.Added;
                        formConfigExistente.Linhas.Add(c);
                    }

                    await contexto.SaveChangesAsync();
                }

                //using (var contexto = new SWMANAGERDbContext())
                //{
                //    var formConfigExistente = _formConfigRepository.GetAllIncluding(f => f.Linhas).FirstOrDefault(j => j.Id == formConfig.Id);
                //    formConfigExistente.Nome = formConfig.Nome;
                //    contexto.Entry(formConfigExistente).State = EntityState.Modified;
                //    contexto.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"));
            }
        }

        public async Task Excluir (FormConfig input)
        {
            try
            {
                await _formConfigRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FormConfig>> Listar (ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<FormConfig> formsConfig;
            //List<FormConfigDto> formsConfigDtos = new List<FormConfigDto>();
            try
            {
                var query = _formConfigRepository
                    .GetAll()
                    .Where(f => f.Descricao != "RESERVADO")
                    .Include(i => i.Linhas.Select(ss => ss.ColConfigs.Select(sss => sss.MultiOption)))
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DataAlteracao.ToShortTimeString().ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
                var formsConfigResult = new List<FormConfig>();
                //Verificando se o formulário é editável (somente se não tiver nenhuma resposta)
                foreach (var formConfig in formsConfig)
                {
                    var conf = await _formRespostaRepository
                        .GetAll()
                        .Where(m => m.FormConfigId == formConfig.Id)
                        .CountAsync();
                    formConfig.IsProducao = conf > 0;
                    formsConfigResult.Add(formConfig);
                }
                //var formsConfigResult = formsConfig;
                //.Select(m => new FormConfig
                //{
                //    Id = m.Id,
                //    Nome = m.Nome,
                //    DataAlteracao = m.DataAlteracao
                //})
                //.ToList();
                //formsConfigDtos = formsConfig
                //    .MapTo<List<FormConfigDto>>();

                return new PagedResultDto<FormConfig>(
                    contarGeradorFormularios,
                    //formsConfigResult
                    formsConfigResult
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfig>> ListarTodos ()
        {
            try
            {
                var query = await _formConfigRepository
                    .GetAllListAsync();
                var formsConfigResult = new List<FormConfig>();
                //Verificando se o formulário é editável (somente se não tiver nenhuma resposta)
                foreach (var formConfig in query)
                {
                    var conf = await _formRespostaRepository
                        .GetAll()
                        .Where(m => m.FormConfigId == formConfig.Id)
                        .CountAsync();
                    formConfig.IsProducao = conf > 0;
                    formsConfigResult.Add(formConfig);
                }

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfig> { Items = formsConfigResult };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfig> Obter (long id)
        {
            try
            {
                var formConfig = new FormConfig();
                var query = _formConfigRepository
                    .GetAll()
                    //.Include(i => i.Linhas.Select(ss => ss.Col1.MultiOption))
                    //.Include(i => i.Linhas.Select(ss => ss.Col2.MultiOption))
                    //.Include(i => i.Linhas.Select(ss => ss.ColConfigs))
                    .Include(i => i.Linhas.Select(ss => ss.ColConfigs.Select(sss => sss.MultiOption)))
                    .Where(m => m.Id == id);

                formConfig = await query
                     .FirstOrDefaultAsync();

                var conf = await _formRespostaRepository
                    .GetAll()
                    .Where(m => m.FormConfigId == formConfig.Id)
                    .CountAsync();
                formConfig.IsProducao = conf > 0;

                ///* Tentando remover a propagação do relacionamento (lazy loading) */
                //var linhas = new List<RowConfig>();
                //var colunas = new List<ColConfig>();
                //var valores = new List<FormData>();
                //var resposta = formConfig.Linhas.FirstOrDefault().ColConfigs.FirstOrDefault().Valores.FirstOrDefault().Resposta;
                //resposta.FormConfig = null;
                //foreach (var linha in formConfig.Linhas)
                //{
                //    foreach (var coluna in linha.ColConfigs)
                //    {
                //        foreach (var valor in coluna.Valores)
                //        {
                //            valor.Coluna = null;
                //            valor.Resposta.FormConfig = null;
                //            valores.Add(valor);
                //        }
                //        coluna.Valores = valores;
                //        colunas.Add(coluna);
                //    }
                //    linha.ColConfigs = colunas;
                //    linhas.Add(linha);
                //}
                //var result = new FormConfig
                //{
                //    CreationTime = formConfig.CreationTime,
                //    CreatorUserId = formConfig.CreatorUserId,
                //    DataAlteracao = formConfig.DataAlteracao,
                //    DeleterUserId = formConfig.DeleterUserId,
                //    DeletionTime = formConfig.DeletionTime,
                //    Id = formConfig.Id,
                //    IsDeleted = formConfig.IsDeleted,
                //    IsSistema = formConfig.IsSistema,
                //    LastModificationTime = formConfig.LastModificationTime,
                //    LastModifierUserId = formConfig.LastModifierUserId,
                //    Linhas = linhas,
                //    Nome = formConfig.Nome
                //};
                //var result = formConfig.MapTo<FormConfig>();
                return formConfig;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfig> Clonar (long id)
        {
            try
            {

                //var formConfig = await Obter(id);
                var query = _formConfigRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Include(i => i.Linhas.Select(ss => ss.ColConfigs.Select(sss => sss.MultiOption)));
                //.FirstOrDefaultAsync();
                var formConfig = await query.FirstOrDefaultAsync();
                var result = new FormConfig();
                if (formConfig != null)
                {
                    var linhas = new List<RowConfig>();
                    result = new FormConfig
                    {
                        //Id = 0,
                        DataAlteracao = DateTime.Now,
                        //Linhas = formConfig.Linhas,
                        Nome = "Clone de " + formConfig.Nome
                    };
                    //result.Id = 0;
                    //result.Nome = "Clone de " + formConfig.Nome;
                    formConfig.Linhas.ForEach(f =>
                    {
                        var linha = new RowConfig();
                        linha.Ordem = f.Ordem;
                        var cols = new List<ColConfig>();
                        //f.Id = 0;
                        f.ColConfigs.ForEach(ff =>
                        {
                            var col = new ColConfig();
                            col.Colspan = ff.Colspan;
                            col.Label = ff.Label;
                            col.Name = ff.Name;
                            col.Ordem = ff.Ordem;
                            col.Placeholder = ff.Placeholder;
                            col.Readonly = ff.Readonly;
                            col.Type = ff.Type;
                            col.Valores = new List<FormData>();
                            col.Value = string.Empty;
                            var multiOption = new List<ColMultiOption>();
                            ff.MultiOption.ForEach(fff =>
                            {
                                multiOption.Add(new ColMultiOption
                                {
                                    Opcao = fff.Opcao,
                                    Selecionado = fff.Selecionado
                                });
                            });
                            col.MultiOption = multiOption;
                            //ff.Id = 0;
                            //ff.Valores = new List<FormData>();
                            //ff.Value = string.Empty;
                            cols.Add(col);
                        });
                        linha.ColConfigs = cols;
                        linhas.Add(linha);
                    });
                    result.Linhas = linhas;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroPesquisar");
            }
        }

        public async Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<FormConfig> solicitacaoExamePrioridadesDto = new List<FormConfig>();
            try
            {
                var query = from p in _formConfigRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Nome.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Nome ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Nome) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarRelacionadosDropdown (DropdownInput dropdownInput)
        {
            try
            {
                long unidadeOrganizacionalId = 0;

                long operacaoId = 0;

                var result1 = long.TryParse(dropdownInput.filtro, out unidadeOrganizacionalId);

                var result2 = long.TryParse(dropdownInput.id, out operacaoId);

                var result3 = dropdownInput.filtros != null && !string.IsNullOrWhiteSpace(dropdownInput.filtros[0]) ? dropdownInput.filtros[0].ToString() : string.Empty;

                long? especialidadeId = null;

                if (!result3.IsNullOrWhiteSpace())
                {
                    especialidadeId = Convert.ToInt64(result3);
                }

                //var result3 = long.TryParse(dropdownInput.filtros[0], out especialidadeId);

                var formsConfigQ = await ListarRelacionados(operacaoId, unidadeOrganizacionalId, especialidadeId);

                //var formsDist = formsConfigQ; //.ToList();

                int pageInt = int.Parse(dropdownInput.page) - 1;
                var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
                List<FormConfig> solicitacaoExamePrioridadesDto = new List<FormConfig>();
                var query = from p in formsConfigQ
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Nome.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Nome ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Nome) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = query.Count();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };

                //return new ListResultDto<FormConfig> { Items = formsConfig };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<IQueryable<FormConfig>> ListarRelacionados (long operacaoId, long unidadeOrganizacionalId, long? especialidadeId)
        {

            var operacao = await _operacaoAppService.Obter(operacaoId);

            var queryUnidadeOrganizacional = await _formConfigUnidadeOrganizacionalAppService.ListarTodos();

            var relacaoUnidadeOrganizacional = queryUnidadeOrganizacional
                .Items
                .WhereIf(unidadeOrganizacionalId > 0, m =>
                    m.UnidadeOrganizacionalId == unidadeOrganizacionalId)
                .Select(m => m.FormConfig)
                .ToList();

            var queryOperacao = await _formConfigOperacaoAppService.ListarTodos();

            var relacaoOperacao = queryOperacao
                .Items
                .Where(m => m.OperacaoId == operacaoId)
                .Select(m => m.FormConfig)
                .ToList();

            var especialidades = new List<FormConfig>();

            if (operacao.IsEspecialidade)
            {

                if (especialidadeId.HasValue)
                {
                    var queryEspecialidade = await _formConfigEspecialidadeAppService.ListarTodos();

                    var relacaoEspecialidade = queryEspecialidade
                        .Items
                        .Where(m => m.EspecialidadeId == especialidadeId)
                        .Select(m => m.FormConfig)
                        .ToList();

                    especialidades = relacaoEspecialidade;
                }
            }


            //var formsConfigQ = new List<FormConfig>();

            //formsConfigQ.AddRange(relacaoOperacao);

            //formsConfigQ.AddRange(relacaoUnidadeOrganizacional);

            var formsConfigQ = relacaoOperacao.Intersect(relacaoUnidadeOrganizacional);

            if (operacao.IsEspecialidade)
            {
                formsConfigQ = formsConfigQ.Intersect(especialidades);
            }

            // removendo duplicidades, já que o form pode estar vinculado à unidade e à operação simultaneamente
            return formsConfigQ.Distinct().ToList().AsQueryable();

        }

        // Campos reservados
        public async Task<long> CriarReservadoAndGetId ()
        {
            try
            {
                FormConfig novoForm = new FormConfig();
                novoForm.Linhas = new List<RowConfig>();
                novoForm.Nome = "Campos Reservados";
                novoForm.IsProducao = false;
                novoForm.Codigo = "COD-RESERVADO";
                novoForm.Descricao = "DESC-RESERVADO";
                novoForm.Id = 0;
                novoForm.DataAlteracao = DateTime.Now;
                novoForm.IsSistema = false;
                novoForm.IsDeleted = false;
                var id = await _formConfigRepository.InsertAndGetIdAsync(novoForm);
                return id;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task<long> ObterReservadoId ()
        {
            try
            {
                var formConfig = new FormConfig();
                var query = _formConfigRepository
                    .GetAll()
                    .Where(m => m.Descricao.Contains("RESERVADO"));

                formConfig = await query
                     .FirstOrDefaultAsync();

                if (formConfig == null)
                    return 0;
                else
                    return formConfig.Id;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfig> ListarReservados ()
        {
            try
            {
                var formConfig = new FormConfig();
                var query = _formConfigRepository
                    .GetAll()
                    .Where(m => m.Descricao.Contains("RESERVADO"));

                formConfig = await query
                     .FirstOrDefaultAsync();

                return formConfig;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public string ObterValorUltimoLancamento (string colConfigName, long? atendimentoId)
        {
            try
            {

                //var todasLinhas = new List<RowConfig>();

                //using (var contexto = new SWMANAGERDbContext())
                //{
                //    var kitExistente = contexto.FormsConfig.Include("Linhas");
                //              //.Where(s => s.For == Kit.Id)
                //              //.FirstOrDefault<FaturamentoKit>();

                    
                //}

                
                

                var prontuarios = _prontuarioRepository.GetAll()
                   .Include(f => f.FormResposta.FormConfig)
                   .Include(l => l.FormResposta.FormConfig.Linhas.Select(c => c.ColConfigs))
                   .Where(a => a.AtendimentoId == atendimentoId);

                var cols = new List<ColConfig>();

                foreach (var prontuario in prontuarios)
                {
                    var form = prontuario.FormResposta.FormConfig;

                    //var x = _rowConfigRepository
                    //    .GetAll()
                        
                    //    .Include(c => c.ColConfigs)
                    //    .Where(a=>a.Id == form.Id)
                    //    .ToList();

                    foreach (var linha in form.Linhas)
                    {
                        //   var row = _rowConfigRepository.Get(linha.Id);

                        if (linha.ColConfigs == null)
                            continue;

                        foreach (var col in linha.ColConfigs)
                        {
                            if (col.Name == colConfigName)
                                cols.Add(col);
                        }
                    }
                }

                var ultimoAlterado = cols.OrderByDescending(item => item.LastModificationTime).FirstOrDefault();

                return ultimoAlterado.Value;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
