﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class FormConfigEspecialidadeAppService : SWMANAGERAppServiceBase, IFormConfigEspecialidadeAppService
    {
        private readonly IRepository<FormConfigEspecialidade, long> _formConfigEspecialidadeRepository;
        //private readonly IRepository<FormResposta, long> _formRespostaRepository;
        private readonly IEspecialidadeAppService _especialidadeService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FormConfigEspecialidadeAppService(
            IRepository<FormConfigEspecialidade, long> formConfigEspecialidadeRepository,
            IEspecialidadeAppService especialidadeService,
            IUnitOfWorkManager unitOfWorkManager
            //IRepository<FormResposta, long> formRespostaRepository
            )
        {
            _formConfigEspecialidadeRepository = formConfigEspecialidadeRepository;
            _especialidadeService = especialidadeService;
            _unitOfWorkManager = unitOfWorkManager;
            //_formRespostaRepository = formRespostaRepository;
        }

        [UnitOfWork]
        public async Task CriarOuEditar(FormConfigEspecialidadeDto input)
        {
            try
            {
                var especialidadesIncluidas = string.IsNullOrWhiteSpace(input.EspecialidadesIncluidas) ? new string[0] : input.EspecialidadesIncluidas.Split(',');
                var especialidadesRemovidas = string.IsNullOrWhiteSpace(input.EspecialidadesRemovidas) ? new string[0] : input.EspecialidadesRemovidas.Split(',');
                //var formConfigEspecialidade = input.MapTo<FormConfigEspecialidade>();
                //if (input.Id.Equals(0))
                //{
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        input.Id = await _formConfigEspecialidadeRepository.InsertAndGetIdAsync(formConfigEspecialidade);
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}
                //else
                //{
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        await _formConfigEspecialidadeRepository.UpdateAsync(formConfigEspecialidade);
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}
                foreach (var especialidadeId in especialidadesIncluidas)
                {
                    var formConfigEspecialidade = new FormConfigEspecialidade
                    {
                        FormConfigId = input.FormConfigId,
                        IsDeleted = false,
                        IsSistema = false,
                        CreatorUserId = AbpSession.UserId,
                        EspecialidadeId = Convert.ToInt64(especialidadeId)
                    };
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formConfigEspecialidadeRepository.InsertAsync(formConfigEspecialidade);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                foreach (var especialidadeId in especialidadesRemovidas)
                {
                    var formConfigEspecialidade = await Obter(input.FormConfigId.Value, Convert.ToInt64(especialidadeId));

                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formConfigEspecialidadeRepository.DeleteAsync(formConfigEspecialidade.Id);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(FormConfigEspecialidadeDto input)
        {
            try
            {
                await _formConfigEspecialidadeRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FormConfigEspecialidadeDto>> Listar(ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<FormConfigEspecialidade> formsConfig;
            List<FormConfigEspecialidadeDto> formsConfigDto;
            try
            {
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.FormConfig.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Especialidade.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Especialidade.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.FormConfig.DataAlteracao.ToShortTimeString().ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                formsConfigDto = formsConfig.MapTo<List<FormConfigEspecialidadeDto>>();

                return new PagedResultDto<FormConfigEspecialidadeDto>(
                    contarGeradorFormularios,
                    //formsConfigResult
                    formsConfigDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigEspecialidadeDto>> ListarTodos()
        {
            try
            {
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigEspecialidadeDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigEspecialidadeDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfigEspecialidadeDto> Obter(long id)
        {
            try
            {
                var formConfig = new FormConfigEspecialidade();
                var formConfigDto = new FormConfigEspecialidadeDto();
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .Where(m => m.Id == id);

                formConfig = await query
                     .FirstOrDefaultAsync();

                formConfigDto = formConfig.MapTo<FormConfigEspecialidadeDto>();

                return formConfigDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from p in _formConfigEspecialidadeRepository
                            .GetAll()
                            .Include(m => m.FormConfig)
                            .Include(m => m.Especialidade)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.FormConfig.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.FormConfig.Nome.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.FormConfig.Nome ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.FormConfig.Codigo, " - ", p.FormConfig.Nome, " / ", p.Especialidade.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigEspecialidadeDto>> ListarPorForm(long id)
        {
            try
            {
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .Where(m => m.FormConfigId == id);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigEspecialidadeDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigEspecialidadeDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigEspecialidadeDto>> ListarPorEspecialidade(long id)
        {
            try
            {
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .Where(m => m.EspecialidadeId == id);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigEspecialidadeDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigEspecialidadeDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfigEspecialidadeDto> Obter(long formConfigId, long especialidadeId)
        {
            try
            {
                var formConfig = new FormConfigEspecialidade();
                var formConfigDto = new FormConfigEspecialidadeDto();
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .Where(m =>
                        m.FormConfigId == formConfigId &&
                        m.EspecialidadeId == especialidadeId
                    );

                formConfig = await query
                     .FirstOrDefaultAsync();

                formConfigDto = formConfig.MapTo<FormConfigEspecialidadeDto>();

                return formConfigDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<EspecialidadeDto>> ListarEspecialidadePorForm(ListarInput input)
        {
            try
            {
                long id = 0;
                var result = long.TryParse(input.PrincipalId, out id);
                if (!result)
                {
                    throw new UserFriendlyException(L("IdNaoInformado"));
                }
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .Where(m => m.FormConfigId == id)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                         m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Select(m => m.Especialidade);

                var query1 = await query.ToListAsync();
                var unidades = query1.Select(m => m.Id).ToArray();

                //var unidades = associadas.Select(m => m.Id).ToArray();
                var disponiveis = await _especialidadeService.ListarTodos();
                var especialidades = disponiveis.Items
                    .Where(m => m.Id.IsIn(unidades))
                    .AsQueryable();

                var count = especialidades.Count();

                var especialidadesDto = especialidades
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();


                //var especialidadesDto = especialidades.MapTo<List<EspecialidadeDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new PagedResultDto<EspecialidadeDto>
                {
                    TotalCount = count,
                    Items = especialidadesDto
                };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<EspecialidadeDto>> ListarEspecialidadeSemForm(ListarInput input)
        {
            try
            {
                long id = 0;
                var result = long.TryParse(input.PrincipalId, out id);
                if (!result)
                {
                    throw new UserFriendlyException(L("IdNaoInformado"));
                }
                //var associadas = await ListarEspecialidadePorForm(input);
                var query = _formConfigEspecialidadeRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Especialidade)
                    .Where(m => m.FormConfigId == id)
                    .Select(m => m.Especialidade);

                var associadas = await query.ToListAsync();

                var unidades = associadas.Select(m => m.Id).ToArray();

                var disponiveis = await _especialidadeService.ListarTodos();

                var unidadesDisponiveis = disponiveis.Items
                    .Where(m => !m.Id.IsIn(unidades))
                    .ToList();

                var unidadesFiltradas = unidadesDisponiveis
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                         m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .AsQueryable();

                var count = unidadesFiltradas.Count();
                var unidadesDto = unidadesFiltradas
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList()
                    .MapTo<List<EspecialidadeDto>>();

                return new PagedResultDto<EspecialidadeDto>
                {
                    TotalCount = count,
                    Items = unidadesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


    }
}
