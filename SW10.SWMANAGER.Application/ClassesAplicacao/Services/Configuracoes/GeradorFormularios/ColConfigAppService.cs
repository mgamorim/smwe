﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using Abp.Domain.Entities.Caching;
using Abp.Dependency;
using Abp.Runtime.Caching;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class ColConfigAppService : SWMANAGERAppServiceBase, IColConfigAppService
    {
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<ColConfig, long> _colConfigRepository;
        private readonly IRepository<ColMultiOption, long> _multiOptionRepository;

        public ColConfigAppService (
            ICacheManager cacheManager,
            IRepository<ColConfig, long> colConfigRepository,
            IRepository<ColMultiOption, long> multiOptionRepository
            )
        {
            _cacheManager = cacheManager;
            _colConfigRepository = colConfigRepository;
            _multiOptionRepository = multiOptionRepository;
        }

        public async Task CriarOuEditar (ColConfig input)
        {
            try
            {
                //var formConfig = input.MapTo<ColConfig>();
                if (input.Id.Equals(0))
                {
                    await _colConfigRepository.InsertAsync(input);
                }
                else
                {

                    if (input.Type == "checkbox" || input.Type == "radio" || input.Type == "select")
                    {
                        foreach (var multiOption in input.MultiOption)
                        {
                            if (multiOption.Id == 0)
                            {
                                await _multiOptionRepository.InsertAsync(multiOption);
                            }
                            else
                            {
                                await _multiOptionRepository.UpdateAsync(multiOption);
                            }
                        }
                    }
                    else
                    {
                        input.MultiOption = new List<ColMultiOption>();
                    }

                    await _colConfigRepository.UpdateAsync(input);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async void UpdateDesesperado (long id) // parametro id nao esta sendo usado
        {
            var cache = _cacheManager.GetCache("ColConfigCache");
            var it = cache.Get("ColConfigsParaUpdate", null) as List<ColConfig>;
            
            foreach (var colConfig in it)
            {
                await CriarOuEditar(colConfig);
            }

            cache.Clear();
        }

        public async Task Excluir (ColConfig input)
        {
            try
            {
                await _colConfigRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<ColConfig>> Listar (ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<ColConfig> formsConfig;
            //List<ColConfigDto> formsConfigDtos = new List<ColConfigDto>();
            try
            {
                var query = _colConfigRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Label.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Name.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //formsConfigDtos = formsConfig
                //    .MapTo<List<ColConfigDto>>();

                return new PagedResultDto<ColConfig>(
                    contarGeradorFormularios,
                    formsConfig
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<ColConfig>> ListarTodos ()
        {
            try
            {
                var query = await _colConfigRepository
                    .GetAllListAsync();

                var formsConfig = query.MapTo<List<ColConfig>>();
                return new ListResultDto<ColConfig> { Items = formsConfig };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ColConfig> Obter (long id)
        {
            try
            {
                var result = await _colConfigRepository
                    .GetAllListAsync(m => m.Id == id);

                var formConfig = result
                    .FirstOrDefault();
                //.MapTo<ColConfigDto>();

                return formConfig;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
