﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public interface IColConfigAppService : IApplicationService
    {
        Task<PagedResultDto<ColConfig>> Listar(ListarInput input);

        Task<ListResultDto<ColConfig>> ListarTodos();

        Task CriarOuEditar(ColConfig input);

        void UpdateDesesperado (long id);

        Task Excluir(ColConfig input);

        Task<ColConfig> Obter(long id);

    }
}
