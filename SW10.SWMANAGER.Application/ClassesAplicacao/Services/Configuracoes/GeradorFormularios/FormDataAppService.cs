﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class FormDataAppService : SWMANAGERAppServiceBase, IFormDataAppService
    {
        private readonly IRepository<FormData, long> _formDataRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;


        public FormDataAppService(
            IRepository<FormData, long> formDataRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _formDataRepository = formDataRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task CriarOuEditar(FormData input)
        {
            try
            {
                //var formConfig = input.MapTo<FormData>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formDataRepository.InsertAsync(input);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formDataRepository.UpdateAsync(input);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }
        [UnitOfWork]
        //public async Task Excluir(FormData input)
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _formDataRepository.DeleteAsync(m => m.Id == id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FormData>> Listar(ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<FormData> formsConfig;
            //List<FormDataDto> formsConfigDtos = new List<FormDataDto>();
            try
            {
                var query = _formDataRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Valor.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //formsConfigDtos = formsConfig
                //    .MapTo<List<FormDataDto>>();

                return new PagedResultDto<FormData>(
                    contarGeradorFormularios,
                    formsConfig
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormData>> ListarTodos()
        {
            try
            {
                var query = await _formDataRepository
                    .GetAllListAsync();

                //var formsConfig = query.MapTo<List<FormDataDto>>();
                return new ListResultDto<FormData> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormData> Obter(long id)
        {
            try
            {
                var result = await _formDataRepository
                    .GetAllListAsync(m => m.Id == id);

                var formConfig = result
                    .FirstOrDefault();
                //.MapTo<FormDataDto>();

                return formConfig;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<FormData>> ListarNoLazy(long id)
        {
            try
            {
                //var result = await _formDataRepository
                //    .GetAllListAsync(m => m.Id == id);

                var query = _formDataRepository
                    .GetAll()
                    .Where(m => m.FormRespostaId == id);
                //.Select(m => new FormData
                //{
                //    ColConfigId = m.ColConfigId,
                //    CreationTime = m.CreationTime,
                //    CreatorUserId = m.CreatorUserId,
                //    DeleterUserId = m.DeleterUserId,
                //    DeletionTime = m.DeletionTime,
                //    FormRespostaId = m.FormRespostaId,
                //    Id = m.Id,
                //    IsDeleted = m.IsDeleted,
                //    IsSistema = m.IsSistema,
                //    LastModificationTime = m.LastModificationTime,
                //    LastModifierUserId = m.LastModifierUserId,
                //    Valor = m.Valor
                //});
                var formsData = await query.ToListAsync();
                List<FormData> result = new List<FormData>();
                foreach (var item in formsData)
                {
                    result.Add(new FormData
                    {
                        ColConfigId = item.ColConfigId,
                        CreationTime = item.CreationTime,
                        CreatorUserId = item.CreatorUserId,
                        DeleterUserId = item.DeleterUserId,
                        DeletionTime = item.DeletionTime,
                        FormRespostaId = item.FormRespostaId,
                        Id = item.Id,
                        IsDeleted = item.IsDeleted,
                        IsSistema = item.IsSistema,
                        LastModificationTime = item.LastModificationTime,
                        LastModifierUserId = item.LastModifierUserId,
                        Valor = item.Valor
                    });
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
