﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios
{
    public class FormConfigOperacaoAppService : SWMANAGERAppServiceBase, IFormConfigOperacaoAppService
    {
        private readonly IRepository<FormConfigOperacao, long> _formConfigOperacaoRepository;
        //private readonly IRepository<FormResposta, long> _formRespostaRepository;
        private readonly IOperacaoAppService _operacaoService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FormConfigOperacaoAppService(
            IRepository<FormConfigOperacao, long> formConfigOperacaoRepository,
            IOperacaoAppService operacaoService,
            IUnitOfWorkManager unitOfWorkManager
            //IRepository<FormResposta, long> formRespostaRepository
            )
        {
            _formConfigOperacaoRepository = formConfigOperacaoRepository;
            _operacaoService = operacaoService;
            _unitOfWorkManager = unitOfWorkManager;
            //_formRespostaRepository = formRespostaRepository;
        }

        [UnitOfWork]
        public async Task CriarOuEditar(FormConfigOperacaoDto input)
        {
            try
            {
                var operacoesIncluidas = string.IsNullOrWhiteSpace(input.OperacoesIncluidas) ? new string[0] : input.OperacoesIncluidas.Split(',');
                var operacoesRemovidas = string.IsNullOrWhiteSpace(input.OperacoesRemovidas) ? new string[0] : input.OperacoesRemovidas.Split(',');
                //var formConfigOperacao = input.MapTo<FormConfigOperacao>();
                //if (input.Id.Equals(0))
                //{
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        input.Id = await _formConfigOperacaoRepository.InsertAndGetIdAsync(formConfigOperacao);
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}
                //else
                //{
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        await _formConfigOperacaoRepository.UpdateAsync(formConfigOperacao);
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}
                foreach (var operacaoId in operacoesIncluidas)
                {
                    var formConfigOperacao = new FormConfigOperacao
                    {
                        FormConfigId = input.FormConfigId,
                        IsDeleted = false,
                        IsSistema = false,
                        CreatorUserId = AbpSession.UserId,
                        OperacaoId = Convert.ToInt64(operacaoId)
                    };
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formConfigOperacaoRepository.InsertAsync(formConfigOperacao);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                foreach (var operacaoId in operacoesRemovidas)
                {
                    var formConfigOperacao = await Obter(input.FormConfigId.Value, Convert.ToInt64(operacaoId));

                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _formConfigOperacaoRepository.DeleteAsync(formConfigOperacao.Id);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(FormConfigOperacaoDto input)
        {
            try
            {
                await _formConfigOperacaoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FormConfigOperacaoDto>> Listar(ListarInput input)
        {
            var contarGeradorFormularios = 0;
            List<FormConfigOperacao> formsConfig;
            List<FormConfigOperacaoDto> formsConfigDto;
            try
            {
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.FormConfig.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Operacao.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Operacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.FormConfig.DataAlteracao.ToShortTimeString().ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGeradorFormularios = await query
                    .CountAsync();

                formsConfig = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                formsConfigDto = formsConfig.MapTo<List<FormConfigOperacaoDto>>();

                return new PagedResultDto<FormConfigOperacaoDto>(
                    contarGeradorFormularios,
                    //formsConfigResult
                    formsConfigDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigOperacaoDto>> ListarTodos()
        {
            try
            {
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigOperacaoDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigOperacaoDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfigOperacaoDto> Obter(long id)
        {
            try
            {
                var formConfig = new FormConfigOperacao();
                var formConfigDto = new FormConfigOperacaoDto();
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .Where(m => m.Id == id);

                formConfig = await query
                     .FirstOrDefaultAsync();

                formConfigDto = formConfig.MapTo<FormConfigOperacaoDto>();

                return formConfigDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from p in _formConfigOperacaoRepository
                            .GetAll()
                            .Include(m => m.FormConfig)
                            .Include(m => m.Operacao)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.FormConfig.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.FormConfig.Nome.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.FormConfig.Nome ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.FormConfig.Codigo, " - ", p.FormConfig.Nome, " / ", p.Operacao.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigOperacaoDto>> ListarPorForm(long id)
        {
            try
            {
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .Where(m => m.FormConfigId == id);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigOperacaoDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigOperacaoDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<FormConfigOperacaoDto>> ListarPorOperacao(long id)
        {
            try
            {
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .Where(m => m.OperacaoId == id);


                var formsConfig = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formsConfigDto = formsConfig.MapTo<List<FormConfigOperacaoDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new ListResultDto<FormConfigOperacaoDto> { Items = formsConfigDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FormConfigOperacaoDto> Obter(long formConfigId, long operacaoId)
        {
            try
            {
                var formConfig = new FormConfigOperacao();
                var formConfigDto = new FormConfigOperacaoDto();
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .Where(m =>
                        m.FormConfigId == formConfigId &&
                        m.OperacaoId == operacaoId
                    );

                formConfig = await query
                     .FirstOrDefaultAsync();

                formConfigDto = formConfig.MapTo<FormConfigOperacaoDto>();

                return formConfigDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<OperacaoDto>> ListarOperacaoPorForm(ListarInput input)
        {
            try
            {
                long id = 0;
                var result = long.TryParse(input.PrincipalId, out id);
                if (!result)
                {
                    throw new UserFriendlyException(L("IdNaoInformado"));
                }
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .Where(m => m.FormConfigId == id)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                         m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Select(m => m.Operacao);

                var query1 = await query.ToListAsync();
                var unidades = query1.Select(m => m.Id).ToArray();

                //var unidades = associadas.Select(m => m.Id).ToArray();
                var disponiveis = await _operacaoService.ListarTodos();
                var operacoes = disponiveis.Items
                    .Where(m => m.Id.IsIn(unidades) && m.IsFormulario)
                    .AsQueryable();

                var count = operacoes.Count();

                var operacoesDto = operacoes
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();


                //var operacoesDto = operacoes.MapTo<List<OperacaoDto>>();

                //var formsConfig = query.MapTo<List<FormConfigDto>>();
                return new PagedResultDto<OperacaoDto>
                {
                    TotalCount = count,
                    Items = operacoesDto
                };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<OperacaoDto>> ListarOperacaoSemForm(ListarInput input)
        {
            try
            {
                long id = 0;
                var result = long.TryParse(input.PrincipalId, out id);
                if (!result)
                {
                    throw new UserFriendlyException(L("IdNaoInformado"));
                }
                //var associadas = await ListarOperacaoPorForm(input);
                var query = _formConfigOperacaoRepository
                    .GetAll()
                    .Include(m => m.FormConfig)
                    .Include(m => m.Operacao)
                    .Where(m => m.FormConfigId == id)
                    .Select(m => m.Operacao);

                var associadas = await query.ToListAsync();

                var unidades = associadas.Select(m => m.Id).ToArray();

                var disponiveis = await _operacaoService.ListarTodos();

                var unidadesDisponiveis = disponiveis.Items
                    .Where(m => !m.Id.IsIn(unidades))
                    .ToList();

                var unidadesFiltradas = unidadesDisponiveis
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                         m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .WhereIf(input.EmpresaId.HasValue, m =>
                         m.ModuloId == input.EmpresaId.Value
                    )
                    .Where(m => m.IsFormulario)
                    .AsQueryable();

                var count = unidadesFiltradas.Count();
                var unidadesDto = unidadesFiltradas
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList()
                    .MapTo<List<OperacaoDto>>();

                return new PagedResultDto<OperacaoDto>
                {
                    TotalCount = count,
                    Items = unidadesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


    }
}
