﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto
{
    [AutoMap(typeof(FormConfig))]
    public class FormConfigDto : CamposPadraoCRUDDto
    {
        public string Nome { get; set; }
        public List<RowConfigDto> Linhas { get; set; }
        public DateTime DataAlteracao { get; set; }
        public bool IsProducao { get; set; }
    }

    [AutoMap(typeof(RowConfig))]
    public class RowConfigDto : CamposPadraoCRUDDto
    {
        public ColConfigDto Col1 { get; set; }
        public ColConfigDto Col2 { get; set; }
        public int Ordem { get; set; }
    }

    [AutoMap(typeof(ColConfig))]
    public class ColConfigDto : CamposPadraoCRUDDto
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string Placeholder { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public bool Colspan { get; set; }
        public bool Readonly { get; set; }
        public List<ColMultiOptionDto> MultiOption { get; set; }
        public List<FormDataDto> Valores { get; set; }
    }

    [AutoMap(typeof(ColMultiOption))]
    public class ColMultiOptionDto : CamposPadraoCRUDDto
    {
        public string Opcao { get; set; }
        public bool Selecionado { get; set; }
    }

}