﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto
{
    [AutoMap(typeof(FormResposta))]
    public class FormRespostaDto : CamposPadraoCRUDDto
    {
        public DateTime DataResposta { get; set; }

        public long FormConfigId { get; set; }

        public FormConfigDto FormConfig { get; set; }

        public string NomeClasse { get; set; }

        public string RegistroClasseId { get; set; }

        public List<FormDataDto> ColRespostas { get; set; }
    }
}