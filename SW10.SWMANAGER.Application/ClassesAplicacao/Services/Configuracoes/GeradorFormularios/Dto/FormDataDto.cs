﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto
{
    [AutoMap(typeof(FormData))]
    public class FormDataDto : CamposPadraoCRUDDto
    {
        public string Valor { get; set; }
        public ColConfigDto Coluna { get; set; }
        //public FormRespostaDto Resposta { get; set; }
    }
}