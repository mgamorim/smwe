﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.Authorization.Users;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas
{
    public class EmpresaAppService : SWMANAGERAppServiceBase, IEmpresaAppService
    {
        private readonly IRepository<Empresa, long> _empresaRepository;
        private readonly IListarEmpresasExcelExporter _listarEmpresasExcelExporter;
        private readonly UserManager _userManager;
        private readonly IUserAppService _userAppService;
        private readonly IRepository<UserEmpresa, long> _userEmpresas;

        public EmpresaAppService (
            IRepository<Empresa, long> empresaRepository,
            IListarEmpresasExcelExporter listarEmpresasExcelExporter
            ,
            UserManager userManager
            ,
            IUserAppService userAppService,
             IRepository<UserEmpresa, long> userEmpresas
            )
        {
            _empresaRepository = empresaRepository;
            _listarEmpresasExcelExporter = listarEmpresasExcelExporter;
            _userManager = userManager;
            _userAppService = userAppService;
            _userEmpresas = userEmpresas;
        }

        public async Task CriarOuEditar (EmpresaDto input)
        {
            try
            {
                var empresa = input.MapTo<Empresa>();
                if (input.Id.Equals(0))
                {
                    await _empresaRepository.InsertAsync(empresa);
                }
                else
                {
                    var ori = await _empresaRepository.GetAsync(empresa.Id);
                    ori.Cnes = empresa.Cnes;
                    ori.Codigo = empresa.Codigo;
                    ori.CodigoSus = empresa.CodigoSus;
                    ori.Convenio = empresa.Convenio;
                    ori.Descricao = empresa.Descricao;
                    ori.EstoqueId = empresa.EstoqueId;
                    ori.IsAtiva = empresa.IsAtiva;
                    ori.IsComprasUnificadas = empresa.IsComprasUnificadas;
                    ori.Logotipo = empresa.Logotipo;
                    ori.LogotipoMimeType = empresa.LogotipoMimeType;
                    ori.RazaoSocial = empresa.RazaoSocial;

                    await _empresaRepository.UpdateAsync(ori);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir (EmpresaDto input)
        {
            try
            {
                await _empresaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<EmpresaDto>> Listar (ListarEmpresasInput input)
        {
            var contarEmpresas = 0;
            List<Empresa> empresas;
            List<EmpresaDto> empresasDtos = new List<EmpresaDto>();
            try
            {
                var query = _empresaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.RazaoSocial.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cnpj.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.InscricaoEstadual.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.InscricaoMunicipal.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone1.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone2.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone3.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone4.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Email.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Logradouro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Bairro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Estado.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Estado.Uf.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarEmpresas = await query
                    .CountAsync();

                empresas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                empresasDtos = empresas
                    .MapTo<List<EmpresaDto>>();

                return new PagedResultDto<EmpresaDto>(
                    contarEmpresas,
                    empresasDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<EmpresaDto>> ListarTodos ()
        {
            try
            {
                var query = await _empresaRepository
                    .GetAllListAsync();

                var empresas = query.MapTo<List<EmpresaDto>>();
                return new ListResultDto<EmpresaDto> { Items = empresas };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel (ListarEmpresasInput input)
        {
            try
            {
                var result = await Listar(input);
                var empresas = result.Items;
                return _listarEmpresasExcelExporter.ExportToFile(empresas.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<EmpresaDto> Obter (long id)
        {
            try
            {
                var result = await _empresaRepository
                    .GetAllListAsync(m => m.Id == id);

                var empresa = result
                    .FirstOrDefault()
                    .MapTo<EmpresaDto>();

                return empresa;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _empresaRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.NomeFantasia.ToLower().Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        .Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        .Replace("ã", "a").Replace("õ", "o")
                        .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        .Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        .Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        .Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.NomeFantasia ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.NomeFantasia) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownPorUsuario (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                // Filtrando por usuario
                var user = await _userManager.GetUserByIdAsync((long)AbpSession.UserId);
                ListResultDto<EmpresaDto> empresas = await _userAppService.GetUserEmpresas(AbpSession.UserId.Value);
                List<EmpresaDto> empresasDto = empresas.Items.ToList();

                //get com filtro
                var query = from p in empresasDto
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.NomeFantasia.ToLower().Contains(dropdownInput.search.ToLower()))

                            orderby p.NomeFantasia ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.NomeFantasia) };

                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = queryResultPage.Count() };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex.InnerException);
            }
        }
    }
}
