using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Exporting
{
    public interface IExameListExcelExporter
    {
        FileDto ExportToFile(List<ExameListDto> exameListDtos);
    }
}