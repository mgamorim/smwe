﻿using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto
{
    public class CreateOrUpdateExameInput
    {
        public ExameEditDto Exame { get; set; }
    }
}