﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto
{
    public class RegistroExameIndex
    {
        public long? Id { get; set; }
        public string Codigo { get; set; }
        public string Exame { get; set; }
        public string PacienteDescricao { get; set; }
        public string ConvenioDescricao { get; set; }
        public int Status { get; set; }
        public string LoteContraste { get; set; }
        public bool IsContraste { get; set; }
        public int? QtdContraste { get; set; }
        public string InternacaoAmbulatorio { get; set; }
        public long? AtendimentoId { get; set; }
        public DateTime? Data { get; set; }
        public string Medico { get; set; }
        public string Leito { get; set; }
        public string TipoLeito { get; set; }
        public string UnidadeOrganizacional { get; set; }
        public string TipoAtendimento { get; set; }
    }
}
