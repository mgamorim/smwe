﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto
{
    public class ListarExameSolicitadosInput: PagedAndSortedInputDto, IShouldNormalize
    {
        public long? AtendimentoId { get; set; }
        public long? EmpresaId { get; set; }
        public DateTime? EmissaoDe { get; set; }
        public DateTime? EmissaoAte { get; set; }


        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Paciente";
            }
        }
    }
}
