﻿using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto
{
    public class GetExameForEditOutput
    {
        public ExameEditDto Exame { get; set; }
    }
}