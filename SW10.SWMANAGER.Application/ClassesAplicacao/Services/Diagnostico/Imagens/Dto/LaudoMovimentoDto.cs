﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Tecnicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLeito.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto
{
    [AutoMap(typeof(LaudoMovimento))]
    public class LaudoMovimentoDto : CamposPadraoCRUDDto
    {
        public long AtendimentoId { get; set; }
        public long LaudoMovimentoStatusId { get; set; }
        public long? ConvenioId { get; set; }
        public long? LeitoId { get; set; }
        public bool IsContraste { get; set; }
        public string QtdeConstraste { get; set; }
        public string Obs { get; set; }
        public AtendimentoDto Atendimento { get; set; }
        public LaudoMovimentoStatusDto LaudoMovimentoStatus { get; set; }
        public ConvenioDto Convenio { get; set; }
        public LeitoDto Leito { get; set; }
        public long AmbulatorioInternacao { get; set; }
        public DateTime? DataRegistro { get; set; }
        public DateTime? HoraRegistro { get; set; }
        public long? CentroCustoId { get; set; }
        public CentroCustoDto CentroCusto { get; set; }

        public long? TipoLeitoId { get; set; }
        public TipoLeitoDto TipoLeito { get; set; }
        public string MedicoSolicitante { get; set; }
        public List<LaudoMovimentoItemDto> LaudoMovimentoItensDto { get; set; }
        public string ExamesJson { get; set; }
        public long? PacienteId { get; set; }

        public int? VolumeContrasteTotal { get; set; }
        public int? VolumeContrasteVenoso { get; set; }
        public int? VolumeContrasteOral { get; set; }
        public int? VolumeContrasteRetal { get; set; }
        public bool IsIonico { get; set; }
        public bool IsBombaInsufora { get; set; }
        public bool IsContrasteVenoso { get; set; }
        public bool IsContrasteOral { get; set; }
        public bool IsContrasteRetal { get; set; }
        public string LoteContraste { get; set; }

        public string Crm { get; set; }

        public long? TurnoId { get; set; }
        public TurnoDto Turno { get; set; }

        public long? TipoAcomodacaoId { get; set; }
        public TipoAcomodacaoDto TipoAcomodacao { get; set; }

        public long? TecnicoId{ get; set; }
        public TecnicoDto Tecnico { get; set; }

        public long? MedicoSolicitanteId { get; set; }
        public MedicoDto Medico { get; set; }

        public long? Ionico { get; set; }
        public long? Aplicacao { get; set; }

    }
}
