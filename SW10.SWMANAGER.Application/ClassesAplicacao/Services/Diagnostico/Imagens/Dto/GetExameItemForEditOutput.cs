﻿using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Diagnostico.Imagens.Dto
{
    public class GetExameItemForEditOutput
    {
        public ExameItemEditDto ExameItem { get; set; }
    }
}