﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Religioes;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Religioes.Dto
{
    [AutoMap(typeof(Religiao))]
    public class ReligiaoDto : CamposPadraoCRUDDto
    {
    }
}
