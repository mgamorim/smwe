﻿using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(CorPele))]
    public class CorPeleDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
