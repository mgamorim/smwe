﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown
{
    public class ResultDropdownList
    {
        public List<DropdownItems> Items { get; set; }

        public int TotalCount { get; set; }
    }
}
