﻿using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    public class DefaultReturn<T> where T : class
    {
        //public TipoRetorno TipoRetorno { get; set; }
        public List<ErroDto> Errors { get; set; }
        public List<ErroDto> Warnings { get; set; }
        public T ReturnObject { get; set; }
    }
}
