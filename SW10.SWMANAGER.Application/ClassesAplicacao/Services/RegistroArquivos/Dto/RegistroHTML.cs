﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto
{
    public class RegistroHTML
    {
        public long RegistroId { get; set; }
        public string HTML { get; set; }
        public long OperacaoId { get; set; }
        public long AtendimentoId { get; set; }
    }
}
