﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto
{
    public class RegistroArquivoDto : CamposPadraoCRUDDto
    {
        public long RegistroId { get; set; }
        public long? RegistroTabelaId { get; set; }
        public string Campo { get; set; }
        public byte[] Arquivo { get; set; }
        public long? AtendimentoId { get; set; }

        public static RegistroArquivoDto Mapear(RegistroArquivo registroArquivo)
        {
            RegistroArquivoDto registroArquivoDto = new RegistroArquivoDto();
            registroArquivoDto.Id = registroArquivo.Id;
            registroArquivoDto.RegistroId = registroArquivo.RegistroId;
            registroArquivoDto.RegistroTabelaId = registroArquivo.RegistroTabelaId;
            registroArquivoDto.Campo = registroArquivo.Campo;
            registroArquivoDto.Arquivo = registroArquivo.Arquivo;
            registroArquivoDto.AtendimentoId = registroArquivo.AtendimentoId;
            registroArquivoDto.Descricao = registroArquivo.Descricao;
            registroArquivoDto.Codigo = registroArquivo.Codigo;

            return registroArquivoDto;
        }

        public static RegistroArquivo Mapear(RegistroArquivoDto registroArquivoDto)
        {
            RegistroArquivo registroArquivo = new RegistroArquivo();
            registroArquivo.Id = registroArquivoDto.Id;
            registroArquivo.RegistroId = registroArquivoDto.RegistroId;
            registroArquivo.RegistroTabelaId = registroArquivoDto.RegistroTabelaId;
            registroArquivo.Campo = registroArquivoDto.Campo;
            registroArquivo.Arquivo = registroArquivoDto.Arquivo;
            registroArquivo.AtendimentoId = registroArquivoDto.AtendimentoId;
            registroArquivo.Descricao = registroArquivoDto.Descricao;
            registroArquivo.Codigo = registroArquivoDto.Codigo;

            return registroArquivo;
        }
    }
}
