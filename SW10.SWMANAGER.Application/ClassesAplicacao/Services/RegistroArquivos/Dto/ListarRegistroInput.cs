﻿using Abp.Runtime.Validation;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto
{
    public class ListarRegistroInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public long? AtendimentoId { get; set; }

        public void Normalize()
        {
            Sorting = "Descricao";
        }
    }
}
