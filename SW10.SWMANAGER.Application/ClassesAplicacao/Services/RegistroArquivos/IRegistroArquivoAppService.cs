﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos
{
    public interface IRegistroArquivoAppService : IApplicationService
    {
        RegistroArquivoDto ObterPorId(long id);
        RegistroArquivoDto ObterPorRegistro(long registroId, long registroTabelaId);
        void GravarHTMLFormularioDinamico(RegistroHTML registroHTML);
        Task<ListResultDto<RegistroArquivoAtendimentoIndex>> ListarPorAtendimento(ListarRegistroInput input);
        void GravarImagemFormularioDinamico(RegistroHTML registroHTML);
       // RetornoArquivo VisualizarImagemRegistroArquivo(long id);
    }
}
