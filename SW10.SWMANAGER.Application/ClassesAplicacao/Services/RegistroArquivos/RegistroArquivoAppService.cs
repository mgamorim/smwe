﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Enumeradores;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using System.Text.RegularExpressions;
//using System.Web.Mvc;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Repositorios;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos
{
    public class RegistroArquivoAppService : SWMANAGERAppServiceBase, IRegistroArquivoAppService
    {
        private readonly IRepository<RegistroArquivo, long> _registroArquivoRepository;

        public RegistroArquivoAppService(IRepository<RegistroArquivo, long> registroArquivoRepository)
        {
            _registroArquivoRepository = registroArquivoRepository;
        }

        public RegistroArquivoDto ObterPorId(long id)
        {
            var registroArquivo = _registroArquivoRepository.GetAll()
                                                            .Where(w => w.Id == id)
                                                            .FirstOrDefault();

            if(registroArquivo!=null)
            {
                return RegistroArquivoDto.Mapear(registroArquivo);
            }

            return null;
        }

        public RegistroArquivoDto ObterPorRegistro(long registroId, long registroTabelaId)
        {
            var registroArquivo = _registroArquivoRepository.GetAll()
                                                            .Where(w => w.RegistroId == registroId
                                                                     && w.RegistroTabelaId == registroTabelaId)
                                                            .OrderByDescending(o=> o.CreationTime)
                                                            .FirstOrDefault();

            if (registroArquivo != null)
            {
                return RegistroArquivoDto.Mapear(registroArquivo);
            }

            return null;
        }


        long ObterTabelaRegistroFormularioDinamico(long operacaoId)
        {
            long registroTabelaId=0;
            switch(operacaoId)
            {
                case 15:
                    registroTabelaId = (long)EnumArquivoTabela.PrescricaoEnfermagem;
                    break;


                case 186:
                    registroTabelaId = (long)EnumArquivoTabela.EvolucaoEnfermagem;
                    break;

                case 13:
                    registroTabelaId = (long)EnumArquivoTabela.AdmisaoEnfermagem;
                    break;

                case 14:
                    registroTabelaId = (long)EnumArquivoTabela.PassagemPlantao;
                    break;

                case 19:
                    registroTabelaId = (long)EnumArquivoTabela.AdmissaoMedica;
                    break;

                case 20:
                    registroTabelaId = (long)EnumArquivoTabela.AltaMedica;
                    break;

                case 21:
                    registroTabelaId = (long)EnumArquivoTabela.Anamnese;
                    break;

                case 22:
                    registroTabelaId = (long)EnumArquivoTabela.EvolucaoMedica;
                    break;
            }



            return registroTabelaId;
        }

        public void GravarHTMLFormularioDinamico(RegistroHTML registroHTML)
        {
            //   Converter.ConvertHtmlString(html, @"C:\\Document.pdf");

            Document document = new Document(PageSize.A4, 10, 10, 170, 10);
            MemoryStream ms = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            HTMLWorker obj = new HTMLWorker(document);

          

            StringReader se = new StringReader(registroHTML.HTML);
            document.Open();
            obj.Parse(se);

            document.Close();

            RegistroArquivo registroArquivo = new RegistroArquivo();

            registroArquivo.Arquivo = ms.ToArray();
            registroArquivo.RegistroTabelaId = ObterTabelaRegistroFormularioDinamico(registroHTML.OperacaoId);
            registroArquivo.RegistroId = registroHTML.RegistroId;
            registroArquivo.AtendimentoId = registroHTML.AtendimentoId;

            var id = _registroArquivoRepository.InsertAndGetId(registroArquivo);

        }

        public void GravarImagemFormularioDinamico(RegistroHTML registroHTML)
        {

            var data = registroHTML.HTML;
            String baseImg = Regex.Match(data, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
            Byte[] baseBytes = Convert.FromBase64String(baseImg);

            RegistroArquivo registroArquivo = new RegistroArquivo();

            registroArquivo.Arquivo = baseBytes;
            registroArquivo.RegistroTabelaId = ObterTabelaRegistroFormularioDinamico(registroHTML.OperacaoId);
            registroArquivo.RegistroId = registroHTML.RegistroId;
            registroArquivo.AtendimentoId = registroHTML.AtendimentoId;

            var id = _registroArquivoRepository.InsertAndGetId(registroArquivo);

        }

        public async Task<ListResultDto<RegistroArquivoAtendimentoIndex>> ListarPorAtendimento(ListarRegistroInput input)
        {

       //     try
       //     {
                var registrosAgrupados = _registroArquivoRepository.GetAll()
                                                          .Where(w => w.AtendimentoId == input.AtendimentoId)
                                                          .GroupBy(g => new { g.RegistroId, g.RegistroTabelaId })
                                                          .Select(i => i.Max(m => m.Id));
                                                          //.ToList();

            //}
            //catch(Exception ex)
            //{

            //}

            var registros = _registroArquivoRepository.GetAll()
                                                       .Where(w => registrosAgrupados.Any(a=> a == w.Id))
                                                       .Include(i => i.RegistroTabela);

                                                      //.ToList();


            List<RegistroArquivoAtendimentoIndex> registrosAtendimento = new List<RegistroArquivoAtendimentoIndex>();


            var contarRegistros = registros.Count();

           var _registros = await  registros
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();


            foreach (var item in _registros)
            {
                RegistroArquivoAtendimentoIndex registroArquivoAtendimentoIndex = new RegistroArquivoAtendimentoIndex();

                registroArquivoAtendimentoIndex.RegistroId = item.Id;
                registroArquivoAtendimentoIndex.DataRegistro = item.CreationTime;
                registroArquivoAtendimentoIndex.OperacaoDescricao = item.RegistroTabela.Descricao;
                registroArquivoAtendimentoIndex.IsPDF = item.RegistroTabelaId == (long)EnumArquivoTabela.PrescricaoMedica;


                registrosAtendimento.Add(registroArquivoAtendimentoIndex);
            }


            return new PagedResultDto<RegistroArquivoAtendimentoIndex>(
                  contarRegistros,
                  registrosAtendimento
                  ); 

        }

       

       // [HttpPost]
        //public RetornoArquivo VisualizarImagemRegistroArquivo(long id)
        //{

        //    try
        //    {
        //        RetornoArquivo retornoArquivo = new RetornoArquivo();

        //        var registro = ObterPorId(id);

        //        byte[] arquivo = null;

        //        if (registro != null)
        //        {
        //            arquivo = registro.Arquivo;

        //            if (registro.RegistroTabelaId == (long)EnumArquivoTabela.PrescricaoMedica)
        //            {
        //                retornoArquivo.IsPDF = true;
        //                 retornoArquivo.FilePDF = File(arquivo, "application/pdf");
        //            }
        //            else
        //            {
        //                var base64 = Convert.ToBase64String(arquivo);
        //                var imgSrc = string.Format("data:{0};base64,{1}", "image/png", base64);

        //                retornoArquivo.ImgSrc = imgSrc;
        //            }

        //        }

        //        //   Response.Headers.Add("Content-Disposition", string.Format("inline; filename=RegistroArquivo.png"));
        //        //  return File(arquivo, "application/png");


        //        return retornoArquivo;


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return null;
        //}


    }
}
