﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Relatorios.Dto
{
    public class RelatorioMovimentacaoFiltroDto
    {
        public int GrupoId { get; set; }
        public int ClasseId { get; set; }
        public int SubClasseId { get; set; }
    }
}
