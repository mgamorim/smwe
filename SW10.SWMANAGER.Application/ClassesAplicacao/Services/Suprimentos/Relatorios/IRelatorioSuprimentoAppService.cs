﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Relatorios.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Relatorios
{
    /// <summary>
    /// Serviço que provê os dados dos filtros do relatório de suplementos
    /// </summary>
    public interface IRelatorioSuprimentoAppService : IApplicationService
    {
        /// <summary>
        /// Lista dos Grupos
        /// </summary>
        /// <returns></returns>
        Task<IList<GenericoIdNome>> Listar();

        /// <summary>
        /// Lista dos GrupoClasse
        /// </summary>
        /// <param name="filtro">Grupo Pai</param>
        /// <returns></returns>
        Task<IList<GenericoIdNome>> Listar(Grupo filtro);

        /// <summary>
        /// Lista de GrupoSubClasse
        /// </summary>
        /// <param name="filtro">GrupoClasse pai</param>
        /// <returns></returns>
        Task<IList<GenericoIdNome>> Listar(GrupoClasse filtro);

        IList<RelatorioMovimentacaoItemDto> DadosRelatorioMovimentacao(RelatorioMovimentacaoFiltroDto filtro);
    }
}