﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Relatorios.Dto;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Relatorios
{
    public class RelatorioSuprimentoAppService : SWMANAGERAppServiceBase, IRelatorioSuprimentoAppService
    {
        internal IRepository<Grupo, long> GrupoRepositorio { get; private set; }
        internal IRepository<GrupoClasse, long> GrupoClasseRepositorio { get; private set; }
        internal IRepository<GrupoSubClasse, long> GrupoSubClasseRepositorio { get; private set; }
        internal IRepository<EstoqueMovimento, long> EstoqueMovimentoRepositorio { get; private set; }
        internal IRepository<EstoqueMovimentoItem, long> EstoqueMovimentoItemRepositorio { get; private set; }

        public RelatorioSuprimentoAppService(
            IRepository<Grupo, long> _GrupoRepositorio,
            IRepository<GrupoClasse, long> _GrupoClasseRepositorio,
            IRepository<GrupoSubClasse, long> _GrupoSubClasseRepositorio
            )
        {
            GrupoClasseRepositorio = _GrupoClasseRepositorio;
            GrupoSubClasseRepositorio = _GrupoSubClasseRepositorio;
            GrupoRepositorio = _GrupoRepositorio;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public async Task<IList<GenericoIdNome>> Listar()
        {
            var grupos = await GrupoRepositorio
                .Query(q => q.Where(g => g.IsDeleted == false))
                .ToListAsync();

            if (grupos != null)
            {
                return grupos
                 .Select(s => new GenericoIdNome
                 {
                     Id = s.Id,
                     Nome = s.Descricao
                 })
                 .ToList();
            }

            return new List<GenericoIdNome>();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public async Task<IList<GenericoIdNome>> Listar(Grupo filtro)
        {
            var resultado = new List<GenericoIdNome>();
            if (filtro == null)
            {
                return resultado;
            }

            var gruposClasse = await GrupoClasseRepositorio
                .Query(q => q.Where(g => g.IsDeleted == false && g.GrupoId == filtro.Id))
                .ToListAsync();

            if (gruposClasse != null)
            {
                return gruposClasse
                 .Select(s => new GenericoIdNome
                 {
                     Id = s.Id,
                     Nome = s.Descricao
                 })
                 .ToList();
            }

            return resultado;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_SaldoProduto, AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public async Task<IList<GenericoIdNome>> Listar(GrupoClasse filtro)
        {
            var resultado = new List<GenericoIdNome>();
            if (filtro == null)
            {
                return resultado;
            }

            var gruposSubClasse = await GrupoSubClasseRepositorio
                .Query(q => q.Where(g => g.IsDeleted == false && g.GrupoClasseId == filtro.Id))
                .ToListAsync();

            if (gruposSubClasse != null)
            {
                return gruposSubClasse
                 .Select(s => new GenericoIdNome
                 {
                     Id = s.Id,
                     Nome = s.Descricao
                 })
                 .ToList();
            }

            return resultado;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Suprimentos_Relatorio_MovimentacaoProduto)]
        public IList<RelatorioMovimentacaoItemDto> DadosRelatorioMovimentacao(RelatorioMovimentacaoFiltroDto filtro)
        {
            var resultado = new List<RelatorioMovimentacaoItemDto>();
            var query = EstoqueMovimentoItemRepositorio
                .Query(q => q.Where(w => w.IsDeleted == false));

            if (filtro != null)
            {
                if (filtro.GrupoId != 0)
                {
                    query = query.Where(w => w.Produto.GrupoId == filtro.GrupoId);
                }

                if (filtro.ClasseId != 0)
                {
                    query = query.Where(w => w.Produto.GrupoClasseId == filtro.ClasseId);
                }

                if (filtro.SubClasseId != 0)
                {
                    query = query.Where(w => w.Produto.GrupoSubClasseId == filtro.SubClasseId);
                }
            }

            if (query.Any())
            {
                var dados = query.Select(s => new
                {
                    Documento = s.EstoqueMovimento.Documento,
                    Grupo = s.Produto.Grupo.Descricao,
                    Classe = s.Produto.Classe.Descricao,
                    SubClass = s.Produto.SubClasse.Descricao,
                    Produto = s.Produto.Descricao,
                    Quantidade = s.Quantidade,
                    CustoUnitario = s.CustoUnitario
                })
                .ToList();

                resultado = dados.Select(s => new RelatorioMovimentacaoItemDto
                {
                    Classe = s.Classe,
                    CustoUnitario = s.CustoUnitario,
                    Documento = s.Documento,
                    Grupo = s.Grupo,
                    Produto = s.Produto,
                    Quantidade = s.Quantidade,
                    SubClass = s.SubClass
                }).ToList();
            }

            return resultado;
        }
    }
}
