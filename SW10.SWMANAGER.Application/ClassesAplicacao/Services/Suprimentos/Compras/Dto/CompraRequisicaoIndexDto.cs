﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Compras;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Compras.Dto
{
    public class CompraRequisicaoIndexDto
    {
        public long Id { get; set; }

        public string Codigo { get; set; }

        public bool IsUrgente { get; set; }

        public bool IsEncerrada { get; set; }

        public bool IsAprovada { get; set; }

        public string Modo { get; set; }

        public string Empresa { get; set; }

        public string Estoque { get; set; }

        public string MotivoPedido { get; set; }

        public DateTime DataEmissao { get; set; }
    } 
}
