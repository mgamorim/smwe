﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Compras.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Compras.Inputs;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ComprasRequisicao
{
    public interface ICompraRequisicaoAppService : IApplicationService
    {
        #region ↓ Atributos

        #region → Basico - CRUD
        Task<CompraRequisicaoDto> CriarOuEditar(CompraRequisicaoDto input);

        Task Excluir(CompraRequisicaoDto input);

        Task<CompraRequisicaoDto> Obter(long id);

        Task<CompraRequisicaoDto> AprovarOuRecusarRequisicao(CompraRequisicaoDto input);
        #endregion

        #region → Basico - Listar
        Task<PagedResultDto<CompraRequisicaoIndexDto>> Listar(ListarRequisicoesCompraInput input);

        Task<PagedResultDto<CompraRequisicaoIndexDto>> ListarCotacao(ListarRequisicoesCompraInput input);

        Task<ListResultDto<CompraRequisicaoDto>> ListarTodos();
        #endregion

        #region → Gets
        Task<GenericoIdNome> ObterModoRequisicaoManual();

        Task<GenericoIdNome> ObterModoRequisicaoAutomatico();

        Task<GenericoIdNome> ObterMotivoPedidoReposicaoEstoque();
        #endregion

        #region → Requisicoes Itens
        Task<ListResultDto<CompraRequisicaoItemDto>> ListarRequisicaoItem(long id);

        Task<PagedResultDto<CompraRequisicaoItemDto>> ListarItensJson(List<CompraRequisicaoItemDto> list);

        Task<ListResultDto<CompraRequisicaoItemDto>> ListarRequisicaoAutomatica(ListarRequisicoesCompraInput input);

        #endregion

        #region → Dropdowns
        Task<ResultDropdownList> ListarMotivoPedidoDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarAprovacaoStatusDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarTipoRequisicaoDropdown(DropdownInput dropdownInput);
        #endregion

        #endregion
    }
}
