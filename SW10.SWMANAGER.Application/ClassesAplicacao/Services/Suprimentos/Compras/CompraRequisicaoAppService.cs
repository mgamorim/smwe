﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Compras.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Compras.Inputs;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Compras;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using Abp.Extensions;
using Newtonsoft.Json;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ComprasRequisicao
{
    /// <summary>
    /// Serviço de Requisicao de Compras
    /// As requisições de compra correspondem aos pedidos para aquisição de produtos ou serviços a serem utilizados no hospital.
    /// Elas podem ser realizadas diretamente pelo setor de Compras, não necessitando do recebimento, ou ainda por meio 
    /// dos diversos setores autorizados pelo hospital, necessitando assim do recebimento pelo setor de Compras
    /// </summary>
    public class CompraRequisicaoAppService : SWMANAGERAppServiceBase, ICompraRequisicaoAppService
    {

        #region ↓ Atributos

        #region → Services
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IProdutoAppService _produtoAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        #endregion Servicos 

        #region → Repositorios
        private readonly IRepository<CompraRequisicao, long> _compraRequisicaoRepository;
        private readonly IRepository<CompraRequisicaoItem, long> _compraRequisicaoItemRepository;
        private readonly IRepository<CompraMotivoPedido, long> _compraMotivoPedidoRepository;
        private readonly IRepository<CompraAprovacaoStatus, long> _compraAprovacaoStatusRepository;
        private readonly IRepository<CompraRequisicaoTipo, long> _compraTipoRequisicaoRepository;
        private readonly IRepository<CompraRequisicaoModo, long> _compraRequisicaoModoRepository;
        private readonly IRepository<Produto, long> _produtoRepository;
        private readonly IRepository<ProdutoEstoque, long> _ressuprimentoRepository;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepository;
        #endregion  Repositorios

        #endregion

        #region ↓ Construtores

        public CompraRequisicaoAppService(
        #region → Services
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoServicoAppService,
            IProdutoAppService produtoAppService,
            IUnidadeAppService unidadeAppService,
        #endregion

        #region → Repositorios
            IRepository<CompraRequisicao, long> compraRequisicaoRepository,
            IRepository<CompraRequisicaoItem, long> compraRequisicaoItemRepository,
            IRepository<CompraMotivoPedido, long> compraMotivoPedidoRepository,
            IRepository<CompraAprovacaoStatus, long> compraAprovacaoStatusRepository,
            IRepository<CompraRequisicaoTipo, long> compraTipoRequisicaoRepository,
            IRepository<CompraRequisicaoModo, long> compraRequisicaoModoRepository,
            IRepository<Produto, long> produtoRepository,
            IRepository<ProdutoEstoque, long> ressuprimentoRepository,
            IRepository<ProdutoSaldo, long> produtoSaldoRepository
        #endregion
        )
        {
            #region → Services
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoServicoAppService;
            _produtoAppService = produtoAppService;
            _unidadeAppService = unidadeAppService;
            #endregion

            #region → Repositorios
            _compraRequisicaoRepository = compraRequisicaoRepository;
            _compraRequisicaoItemRepository = compraRequisicaoItemRepository;
            _compraMotivoPedidoRepository = compraMotivoPedidoRepository;
            _compraAprovacaoStatusRepository = compraAprovacaoStatusRepository;
            _compraTipoRequisicaoRepository = compraTipoRequisicaoRepository;
            _compraRequisicaoModoRepository = compraRequisicaoModoRepository;
            _produtoRepository = produtoRepository;
            _ressuprimentoRepository = ressuprimentoRepository;
            _produtoSaldoRepository = produtoSaldoRepository;
            #endregion
        }

        #endregion

        #region ↓ Metodos

        #region → CRUD
        /// <summary>
        /// Inclui ou Edita uma Requisicao de Compra.
        /// Faz o mesmo nos itens da requisicao relacionados</summary>
        /// <param name="input"></param>
        /// <returns>CompraRequisicaoDto</returns>
        [UnitOfWork]
        public async Task<CompraRequisicaoDto> CriarOuEditar(CompraRequisicaoDto input)
        {
            try
            {
                var compraRequisicao = input.MapTo<CompraRequisicao>();

                var compraRequisicoesItem = new List<CompraRequisicaoItemDto>();

                //Deserializa os itens
                if (!input.RequisicoesItensJson.IsNullOrWhiteSpace())
                {
                    string requisicoesItensJson = input.RequisicoesItensJson.ToLower(); //← atJSON 

                    //compraRequisicoesItem = JsonConvert.DeserializeObject<List<CompraRequisicaoItemDto>>(input.RequisicoesItensJson, new IsoDateTimeConverter { DateTimeFormat = "dd/mm/yyyy" });
                    //compraRequisicoesItem = JsonConvert.DeserializeObject<List<CompraRequisicaoItemDto>>(input.RequisicoesItensJson);
                    compraRequisicoesItem = JsonConvert.DeserializeObject<List<CompraRequisicaoItemDto>>(requisicoesItensJson);
                }

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    #region 1 - grava requisicao
                    if (input.Id.Equals(0))
                    {
                        compraRequisicao.Codigo = _ultimoIdAppService.ObterProximoCodigo("CompraRequisicao").Result;
                        compraRequisicao.CotacaoStatusId = 1;
                        compraRequisicao.AprovacaoStatusId = 1;
                        compraRequisicao.CotacaoStatusId = 1;
                        compraRequisicao.Id = await _compraRequisicaoRepository.InsertAndGetIdAsync(compraRequisicao);
                    }
                    else
                    {
                        await _compraRequisicaoRepository.UpdateAsync(compraRequisicao);
                    }
                    #endregion 


                    #region 2 - grava itens da requisicao
                    foreach (var compraRequisicaoItem in compraRequisicoesItem)
                    {
                        compraRequisicaoItem.RequisicaoId = compraRequisicao.Id;

                        if (compraRequisicaoItem.IsDeleted)
                        {
                            await _compraRequisicaoItemRepository.DeleteAsync(compraRequisicaoItem.Id);
                        }
                        else
                        {
                            if (input.Id.Equals(0))
                            {
                                await _compraRequisicaoItemRepository.InsertAndGetIdAsync(compraRequisicaoItem.MapTo<CompraRequisicaoItem>());
                            }
                            else
                            {
                                await _compraRequisicaoItemRepository.UpdateAsync(compraRequisicaoItem.MapTo<CompraRequisicaoItem>());
                            }
                        }
                    }

                    #endregion region

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                return compraRequisicao.MapTo<CompraRequisicaoDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        [UnitOfWork]
        public async Task Excluir(CompraRequisicaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _compraRequisicaoRepository.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CompraRequisicaoDto</returns>
        public async Task<CompraRequisicaoDto> Obter(long id)
        {
            try
            {
                var result = await _compraRequisicaoRepository
                    .GetAll()
                    .Include(m => m.Estoque)
                    .Include(m => m.MotivoPedido)
                    .Include(m => m.MotivoPedido)
                    .Include(m => m.TipoRequisicao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Paciente)
                    .Include(m => m.Medico)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var requisicaoCompra = result
                    .MapTo<CompraRequisicaoDto>();

                return requisicaoCompra;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork]
        public async Task<CompraRequisicaoDto> AprovarOuRecusarRequisicao(CompraRequisicaoDto input)
        {
            try
            {
                var compraRequisicao = input.MapTo<CompraRequisicao>();

                compraRequisicao.IsEncerrada = true;

                //compraRequisicao.IsAprovada = true;

                var compraRequisicoesItem = new List<CompraRequisicaoItemDto>();

                //Deserializa os itens
                if (!input.RequisicoesItensJson.IsNullOrWhiteSpace())
                {
                    string requisicoesItensJson = input.RequisicoesItensJson.ToLower(); //← atJSON 

                    //compraRequisicoesItem = JsonConvert.DeserializeObject<List<CompraRequisicaoItemDto>>(input.RequisicoesItensJson, new IsoDateTimeConverter { DateTimeFormat = "dd/mm/yyyy" });
                    //compraRequisicoesItem = JsonConvert.DeserializeObject<List<CompraRequisicaoItemDto>>(input.RequisicoesItensJson);
                    compraRequisicoesItem = JsonConvert.DeserializeObject<List<CompraRequisicaoItemDto>>(requisicoesItensJson);
                }

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    #region 1 - grava requisicao

                        await _compraRequisicaoRepository.UpdateAsync(compraRequisicao);

                    #endregion 


                    #region 2 - grava itens da requisicao
                    foreach (var compraRequisicaoItem in compraRequisicoesItem)
                    {
                        compraRequisicaoItem.RequisicaoId = compraRequisicao.Id;

                        //if (compraRequisicaoItem.IsDeleted)
                        //{
                        //    await _compraRequisicaoItemRepository.DeleteAsync(compraRequisicaoItem.Id);
                        //}
                        //else
                        //{
                            //if (input.Id.Equals(0))
                            //{
                            //    await _compraRequisicaoItemRepository.InsertAndGetIdAsync(compraRequisicaoItem.MapTo<CompraRequisicaoItem>());
                            //}
                            //else
                            //{
                                await _compraRequisicaoItemRepository.UpdateAsync(compraRequisicaoItem.MapTo<CompraRequisicaoItem>());
                            //}
                        //}
                    }

                    #endregion region

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                return compraRequisicao.MapTo<CompraRequisicaoDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        #endregion Basico - CRUD

        #region → Basico - Listar
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<CompraRequisicaoIndexDto>> Listar(ListarRequisicoesCompraInput input)
        {
            var contarComprasRequisicao = 0;

            long StatusRequisicao;
            long.TryParse(input.StatusRequisicao, out StatusRequisicao);

            //List<CompraRequisicao> CompraRequisicoes;
            //List<CompraRequisicaoDto> ComprarequisicoesDtos = new List<CompraRequisicaoDto>();

            List<CompraRequisicaoIndexDto> compraRequisicoesIndexDtos;

            try
            {
                /* O daterangepicker não passa para o StartDate do input uma data que comece a partir de 00:00:00, mas a partir de 03:00:00
                   abaixo um ajuste para abranger todo o periodo de tempo do range de datas para que nenhum registro fique de fora do retorno
                  (00:00 da data inicial até 23:59 da data final).*/
                //PS: Quando o cadastro é carregado pela primeira vez(em que não se define range de datas manualmente)o range esta vindo com abrangencia de dois dias, mesmo que o o daterangepicker exiba "01/01/2018 - 01/01/2018". 
                #region acerta range de emissao
                DateTime startDate = ((DateTime)input.StartDate).Date;
                DateTime endDate = ((DateTime)input.EndDate).Date;
                endDate = endDate.AddDays(1);
                endDate = endDate.AddSeconds(-1);
                #endregion

                //TODO: até alinhar com o Marcio, o retorno do filtro está usando o CreationTime como emissao da requisicao.
                #region query
                var query = _compraRequisicaoRepository
                    .GetAll()
                    .Include(m => m.Empresa)
                    .Include(m => m.Estoque)
                    .Include(m => m.MotivoPedido)
                    .Include(m => m.RequisicaoModo)
                    .Include(m => m.TipoRequisicao)
                    .Include(m => m.AprovacaoStatus)
                    .WhereIf(!string.IsNullOrEmpty(input.EmpresaId), e => e.EmpresaId.ToString() == input.EmpresaId)
                    .WhereIf(!string.IsNullOrEmpty(input.EstoqueId), e => e.EstoqueId.ToString() == input.EstoqueId)
                    .WhereIf(!string.IsNullOrEmpty(input.MotivoPedidoId), e => e.MotivoPedidoId.ToString() == input.MotivoPedidoId)
                    .WhereIf(!string.IsNullOrEmpty(input.Codigo), e => e.Codigo == input.Codigo)

                    .WhereIf(StatusRequisicao == 1, m => m.IsEncerrada == true)
                    .WhereIf(StatusRequisicao == 2, m => m.IsEncerrada == false)

                    .WhereIf(!string.IsNullOrEmpty(input.AprovacaoStatusId), e => e.AprovacaoStatusId.ToString() == input.AprovacaoStatusId)

                    .WhereIf(input.IsUrgente.HasValue, e => e.IsUrgente == input.IsUrgente)
                    .Where(_ => _.DataEmissao >= startDate && _.DataEmissao <= endDate
                     )
                    .Select(m => new CompraRequisicaoIndexDto
                    {
                        Id = m.Id,
                        Codigo = m.Codigo,
                        Modo = m.RequisicaoModo.Descricao,
                        Empresa = m.Empresa.RazaoSocial,
                        Estoque = m.Estoque.Descricao,
                        MotivoPedido = m.MotivoPedido.Descricao,
                        IsUrgente = m.IsUrgente,
                        IsEncerrada = m.IsEncerrada,
                        //IsAprovada = m.IsAprovada,
                        DataEmissao = m.DataEmissao
                    });
                #endregion

                contarComprasRequisicao = await query
                    .CountAsync();

                input.Sorting = "DataEmissao";

                compraRequisicoesIndexDtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //ComprarequisicoesDtos = CompraRequisicoes
                //    .MapTo<List<CompraRequisicaoDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<CompraRequisicaoIndexDto>(
                contarComprasRequisicao,
                compraRequisicoesIndexDtos
                );
        }

        public async Task<PagedResultDto<CompraRequisicaoIndexDto>> ListarCotacao(ListarRequisicoesCompraInput input)
        {
            var contarComprasRequisicao = 0;

            long StatusRequisicao;
            long.TryParse(input.StatusRequisicao, out StatusRequisicao);

            //List<CompraRequisicao> CompraRequisicoes;
            //List<CompraRequisicaoDto> ComprarequisicoesDtos = new List<CompraRequisicaoDto>();

            List<CompraRequisicaoIndexDto> compraRequisicoesIndexDtos;

            try
            {
                /* O daterangepicker não passa para o StartDate do input uma data que comece a partir de 00:00:00, mas a partir de 03:00:00
                   abaixo um ajuste para abranger todo o periodo de tempo do range de datas para que nenhum registro fique de fora do retorno
                  (00:00 da data inicial até 23:59 da data final).*/
                //PS: Quando o cadastro é carregado pela primeira vez(em que não se define range de datas manualmente)o range esta vindo com abrangencia de dois dias, mesmo que o o daterangepicker exiba "01/01/2018 - 01/01/2018". 
                #region acerta range de emissao
                DateTime startDate = ((DateTime)input.StartDate).Date;
                DateTime endDate = ((DateTime)input.EndDate).Date;
                endDate = endDate.AddDays(1);
                endDate = endDate.AddSeconds(-1);
                #endregion

                //TODO: até alinhar com o Marcio, o retorno do filtro está usando o CreationTime como emissao da requisicao.
                #region query
                var query = _compraRequisicaoRepository
                    .GetAll()
                    .Include(m => m.Empresa)
                    .Include(m => m.Estoque)
                    .Include(m => m.MotivoPedido)
                    .Include(m => m.RequisicaoModo)
                    .Include(m => m.TipoRequisicao)
                    .Include(m => m.AprovacaoStatus)
                    .WhereIf(!string.IsNullOrEmpty(input.EmpresaId), e => e.EmpresaId.ToString() == input.EmpresaId)
                    .WhereIf(!string.IsNullOrEmpty(input.EstoqueId), e => e.EstoqueId.ToString() == input.EstoqueId)
                    .WhereIf(!string.IsNullOrEmpty(input.MotivoPedidoId), e => e.MotivoPedidoId.ToString() == input.MotivoPedidoId)
                    .WhereIf(!string.IsNullOrEmpty(input.Codigo), e => e.Codigo == input.Codigo)

                    .WhereIf(StatusRequisicao == 1, m => m.IsEncerrada == true)
                    .WhereIf(StatusRequisicao == 2, m => m.IsEncerrada == false)

                    //.WhereIf(!string.IsNullOrEmpty(input.AprovacaoStatusId), e => e.AprovacaoStatusId.ToString() == input.AprovacaoStatusId)

                    .WhereIf(input.IsUrgente.HasValue, e => e.IsUrgente == input.IsUrgente)
                    .Where(_ => _.DataEmissao >= startDate && _.DataEmissao <= endDate
                     )

                    .Where(m => m.AprovacaoStatusId == 2)

                    .Select(m => new CompraRequisicaoIndexDto
                    {
                        Id = m.Id,
                        Codigo = m.Codigo,
                        Modo = m.RequisicaoModo.Descricao,
                        Empresa = m.Empresa.RazaoSocial,
                        Estoque = m.Estoque.Descricao,
                        MotivoPedido = m.MotivoPedido.Descricao,
                        IsUrgente = m.IsUrgente,
                        IsEncerrada = m.IsEncerrada,
                        //IsAprovada = m.IsAprovada,
                        DataEmissao = m.DataEmissao
                    });
                #endregion

                contarComprasRequisicao = await query
                    .CountAsync();

                input.Sorting = "DataEmissao";

                compraRequisicoesIndexDtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //ComprarequisicoesDtos = CompraRequisicoes
                //    .MapTo<List<CompraRequisicaoDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<CompraRequisicaoIndexDto>(
                contarComprasRequisicao,
                compraRequisicoesIndexDtos
                );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<CompraRequisicaoDto>> ListarTodos()
        {
            List<CompraRequisicaoDto> requisicoesCompraDtos = new List<CompraRequisicaoDto>();
            try
            {
                var requisicoesCompra = await _compraRequisicaoRepository
                    .GetAll()
                    //.Include(m => m.Cidade)
                    .AsNoTracking()
                    .ToListAsync();

                requisicoesCompraDtos = requisicoesCompra
                    .MapTo<List<CompraRequisicaoDto>>();

                return new ListResultDto<CompraRequisicaoDto> { Items = requisicoesCompraDtos };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        #endregion

        #region → Gets

        /// <summary>
        /// Retorna um Dto de Modo de Requisicao to tipo Manual
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CompraRequisicaoDto</returns>
        public async Task<GenericoIdNome> ObterModoRequisicaoManual()
        {
            try
            {
                var result = await _compraRequisicaoModoRepository
                    .GetAll()
                    .Where(m => m.Codigo == "1")
                    .Select(m => new GenericoIdNome
                    {
                        Id = m.Id,
                        Nome = m.Descricao
                    })
                    .FirstOrDefaultAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna um Dto de Modo de Requisicao to tipo Automatico
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CompraRequisicaoDto</returns>
        public async Task<GenericoIdNome> ObterModoRequisicaoAutomatico()
        {
            try
            {
                var result = await _compraRequisicaoModoRepository
                    .GetAll()
                    .Where(m => m.Codigo == "2")
                    .Select(m => new GenericoIdNome
                    {
                        Id = m.Id,
                        Nome = m.Descricao
                    })
                    .FirstOrDefaultAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna um Dto de Motivo de Pedido para Requisicao de Estoque
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Id e Descricao(Nome) do Motivo de Pedido Resposição de Estoque</returns>
        public async Task<GenericoIdNome> ObterMotivoPedidoReposicaoEstoque()
        {
            try
            {
                var result = await _compraMotivoPedidoRepository
                    .GetAll()
                    .Where(m => m.Codigo == "1")
                    .Select(m => new GenericoIdNome
                    {
                        Id = m.Id,
                        Nome = m.Descricao
                    })
                    .FirstOrDefaultAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        #endregion

        #region → Requisicoes Itens
        /// <summary>
        /// Retorna lista(ListResultDto) de CompraRequisicaoItemDto.
        /// Carga dinamica no IdGrid para manipulacao dinamica.
        /// É Utilizado no action CriarOuEditar
        /// </summary>
        /// <param name="id">id da Requisicao</param>
        /// <returns>ListResultDto<CompraRequisicaoItemDto></returns>
        public async Task<ListResultDto<CompraRequisicaoItemDto>> ListarRequisicaoItem(long id)
        {
            try
            {
                var idGrid = 0;
                var query = _compraRequisicaoItemRepository
                            .GetAll()
                            .Where(m => m.RequisicaoId == id);

                var list = await query.ToListAsync();

                var listDto = list.MapTo<List<CompraRequisicaoItemDto>>();

                listDto.ForEach(m => m.IdGrid = ++idGrid);

                return new ListResultDto<CompraRequisicaoItemDto> { Items = listDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Recebe uma lista de objetos e devolve a mesma lista para popular um grid
        /// Utilizado na funcao RetornaLista, esta que é usada no listAction do JTable
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<CompraRequisicaoItemDto>> ListarItensJson(List<CompraRequisicaoItemDto> list)
        {
            try
            {
                var count = 0;
                if (list == null)
                {
                    list = new List<CompraRequisicaoItemDto>();
                }
                for (int i = 0; i < list.Count(); i++)
                {
                    list[i].IdGrid = i + 1;

                    //set produto e unidade requisicao
                    if (list[i].Produto == null)
                    {
                        var produto = await _produtoAppService.Obter(list[i].ProdutoId);
                        list[i].Produto = produto;
                    }

                    if (list[i].Unidade == null)
                    {
                        var unidade = await _unidadeAppService.Obter(list[i].UnidadeId);
                        list[i].Unidade = unidade;
                    }

                    //set produto e unidade aprovacao
                    if (list[i].ProdutoAprovacaoId != null)
                    {
                        var produto = await _produtoAppService.Obter((long)list[i].ProdutoAprovacaoId);
                        list[i].ProdutoAprovacao = produto;
                    }

                    if (list[i].UnidadeAprovacaoId != null)
                    {
                        var unidade = await _unidadeAppService.Obter((long)list[i].UnidadeAprovacaoId);
                        list[i].UnidadeAprovacao = unidade;
                    }
                }

                count = await Task.Run(() => list.Count());

                return new PagedResultDto<CompraRequisicaoItemDto>(
                      count,
                      list
                      );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar", ex));
            }
        }

        /// <summary>
        /// Traz uma listagem de Todos os Produtos com saldo abaixo do Estoque Minimo para ser usado em uma requisicao automatica
        /// </summary>
        /// <param name="input">Utilize a propriedade filtro para passar o id do Grupo de Produtos</param>
        /// <returns>ListResultDto<CompraRequisicaoItemDto></returns>
        //public async Task<PagedResultDto<CompraRequisicaoItemDto>> ListarRequisicaoAutomatica(ListarRequisicoesCompraInput input)
        public async Task<ListResultDto<CompraRequisicaoItemDto>> ListarRequisicaoAutomatica(ListarRequisicoesCompraInput input)
        {
            try
            {
                var idGrid = 0;
                long idGrupo = 0;
                long.TryParse(input.Filtro, out idGrupo);
                long idEstoque = 0;
                long.TryParse(input.EstoqueId, out idEstoque);

                /* ↓ Um expert em LINQ poderá simplificar a consulta abaixo ↓ */

                #region Query dos Produtos abaixo do Estoque Minimo
                var qryProdQtd = from prod in _produtoRepository.GetAll()
                            join rsup in _ressuprimentoRepository.GetAll() on prod.Id equals rsup.ProdutoId into joinedRsup
                            from ressupJoin in joinedRsup.DefaultIfEmpty()
                            join saldo in _produtoSaldoRepository.GetAll() on prod.Id equals saldo.ProdutoId into joinedSaldo
                            from saldoJoin in joinedSaldo.DefaultIfEmpty()

                                //join unidade in _produtoUnidadeRepository.GetAll() on prod.Id equals unidade.ProdutoId into joinedUnidade
                                //from saldoJoin in joinedSaldo.DefaultIfEmpty()

                            where 1 == 1
                                && (prod.GrupoId == idGrupo) // ← Grupo de Produtos passado no input
                                && (saldoJoin.EstoqueId == idEstoque) // ← Estoque passado no input

                                //&& (prod.Id == ressupJoin.ProdutoId)    //← Acho que nao precisa disso
                                //&& (prod.Id == saldoJoin.ProdutoId)

                                && (!prod.IsPrincipal) //← Não é Produto Principal
                                && (!prod.IsBloqueioCompra) //← Produto liberado para Compra
                                && (prod.IsAtivo == true) //← produto deve estar ativo
                                                       // TODO: Conferir se está retornando produtos excluidos

                                //&& (!compraReqJoin.IsEncerrada)

                                // ↓ o saldo deve estar abaixo do estoque minimo
                                // saldo disponivel + requisicao pendente de compra < estoque minimo
                                && (
                                      (saldoJoin.QuantidadeAtual + saldoJoin.QuantidadeEntradaPendente - saldoJoin.QuantidadeSaidaPendente)

                                      < ressupJoin.EstoqueMinimo
                                   )
                            //TODO: testar se a formula saldo < estoque esta funcionando

                            select new
                            {
                                ProdutoId = prod.Id,
                                Produto = prod,
                                EstoqueMinimo = ressupJoin.EstoqueMinimo,
                                EstoqueMaximo = ressupJoin.EstoqueMaximo,
                                PontoPedido = ressupJoin.PontoPedido,
                                QuantidadeAtual = saldoJoin.QuantidadeAtual,
                                QuantidadeEntradaPendente = saldoJoin.QuantidadeEntradaPendente,
                                QuantidadeSaidaPendente = saldoJoin.QuantidadeSaidaPendente
                            };
                #endregion

                input.Sorting = "ProdutoId";

                var list = await qryProdQtd.ToListAsync();

                #region Query para produtos em Requisicoes não encerradas
                var qryCmpReq = from compraReq in _compraRequisicaoRepository.GetAll()
                                join compraReqItem in _compraRequisicaoItemRepository.GetAll() on compraReq.Id equals compraReqItem.RequisicaoId into joinedRequisicao
                                from compraReqJoin in joinedRequisicao.DefaultIfEmpty()
                                where 1 == 1
                                && (!compraReq.IsEncerrada == true)
                                select new
                                {
                                    compraReqJoin
                                } into t1
                                group t1 by t1.compraReqJoin.ProdutoId into g
                                select new
                                {
                                    ProdutoId = g.FirstOrDefault().compraReqJoin.ProdutoId,
                                    QtdReq = g.Sum(m => g.FirstOrDefault().compraReqJoin.Quantidade)
                                };
                #endregion

                var list2 = await qryCmpReq.ToListAsync();

                #region final
                var joinRes = (
                                from prod in qryProdQtd
                                join req in qryCmpReq on prod.ProdutoId equals req.ProdutoId into joined
                                from req in joined.DefaultIfEmpty()
                                //from prodReqJoin in joined.DefaultIfEmpty()
                                where 1 == 1
                                && (
                                      ((prod.QuantidadeAtual + prod.QuantidadeEntradaPendente - prod.QuantidadeSaidaPendente)
                                      + (req.QtdReq == null ? default(Decimal) : req.QtdReq)
                                      )// prodReqJoin.QtdReq)
                                      < prod.EstoqueMinimo
                                   )
                                select new CompraRequisicaoItemDto
                                {
                                    ProdutoId = prod.ProdutoId,
                                    UnidadeId = 1,
                                   //↓ Ponto de Pedido - Saldo - Qtd total do Produto para o estoque selecionado em Requisicoes nao encerradas
                                    Quantidade = prod.PontoPedido - (prod.QuantidadeAtual + prod.QuantidadeEntradaPendente - prod.QuantidadeSaidaPendente) - (req.QtdReq == null ? default(Decimal) : req.QtdReq),
                                    //(req.QtdReq.ToString().IsNullOrEmpty() ? 0 : req.QtdReq),//req.QtdReq,//prodReqJoin.QtdReq,

                                    //ProdutoAprovacaoId = prod.ProdutoId,
                                    //UnidadeAprovacaoId = 1,
                                    ////↓ Ponto de Pedido - Saldo - Qtd total do Produto para o estoque selecionado em Requisicoes nao encerradas
                                    //QuantidadeAprovacao = null, //prod.PontoPedido - (prod.QuantidadeAtual + prod.QuantidadeEntradaPendente - prod.QuantidadeSaidaPendente) - prodReqJoin.QtdReq,

                                    ModoInclusao = "A" //← automatico
                                }
                              ).ToList();
                #endregion

                //var listDto = list.MapTo<List<CompraRequisicaoItemDto>>();

                var listDto = joinRes.MapTo<List<CompraRequisicaoItemDto>>();

                listDto.ForEach(m => m.IdGrid = ++idGrid);

                return new ListResultDto<CompraRequisicaoItemDto> { Items = listDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }


        }
        #endregion Requisicoes Itens

        #region Aprovacao

        #endregion

        #region → Dropdowns
        /// <summary>
        /// Retorna lista paginada de Motivo de Pedido('Reposicao de Estoque', 'Aumento de Consumo', etc) para select2
        /// </summary>
        /// <param name="dropdownInput"></param>
        /// <returns></returns>
        public async Task<ResultDropdownList> ListarMotivoPedidoDropdown(DropdownInput dropdownInput)
        {
            //long grupoId;

            //long.TryParse(dropdownInput.filtro, out grupoId);

            return await ListarDropdownLambda(dropdownInput
                                                     , _compraMotivoPedidoRepository
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );
        }

        public async Task<ResultDropdownList> ListarAprovacaoStatusDropdown(DropdownInput dropdownInput)
        {
            //long grupoId;

            //long.TryParse(dropdownInput.filtro, out grupoId);

            return await ListarDropdownLambda(dropdownInput
                                                     , _compraAprovacaoStatusRepository
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );
        }

        /// <summary>
        /// Retorna lista paginada de TipoRequisicao('Produto', 'Servico') para select2
        /// </summary>
        /// <param name="dropdownInput"></param>
        /// <returns></returns>
        public async Task<ResultDropdownList> ListarTipoRequisicaoDropdown(DropdownInput dropdownInput)
        {
            //long grupoId;
            //long.TryParse(dropdownInput.filtro, out grupoId);

            return await ListarDropdownLambda(dropdownInput
                                                     , _compraTipoRequisicaoRepository
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );
        }
        #endregion

        #endregion Metodos

    }
}
