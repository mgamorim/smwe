﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public interface ITipoMovimentoAppService : IApplicationService
    {
        Task<PagedResultDto<TipoMovimentoDto>> Listar(bool isEntrada);
        Task<PagedResultDto<TipoMovimentoDto>> ListarTipoMovimentoDevolucao();
        Task<ResultDropdownList> ListarDropdownEntrada(DropdownInput dropdownInput);
        Task<ResultDropdownList> ListarDropdownSaida(DropdownInput dropdownInput);
        Task<ResultDropdownList> ListarDropdownDevolucao(DropdownInput dropdownInput);
    }
}
