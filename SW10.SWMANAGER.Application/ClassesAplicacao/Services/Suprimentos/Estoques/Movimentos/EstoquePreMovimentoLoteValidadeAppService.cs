﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using NFe.Classes.Informacoes.Detalhe;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class EstoquePreMovimentoLoteValidadeAppService : SWMANAGERAppServiceBase, IEstoquePreMovimentoLoteValidadeAppService
    {
        private readonly IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IRepository<EstoqueImportacaoProduto, long> _estoqueImportacaoProdutoRepository;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        private readonly IRepository<EstoqueLaboratorio, long> _estoqueLaboratorioRepository;
        private readonly IRepository<LoteValidade, long> _loteValidadeRepository;


        public EstoquePreMovimentoLoteValidadeAppService(
            IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
            IRepository<EstoqueImportacaoProduto, long> estoqueImportacaoProdutoRepository,
            IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
            IRepository<EstoqueLaboratorio, long> estoqueLaboratorioRepository,
            IRepository<LoteValidade, long> loteValidadeRepository)
        {
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoqueImportacaoProdutoRepository = estoqueImportacaoProdutoRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _estoqueLaboratorioRepository = estoqueLaboratorioRepository;
            _loteValidadeRepository = loteValidadeRepository;
        }

        public async Task<decimal> ObterQuantidadeRestanteLoteValidade(long preMovimentoItemId)
        {
            try
            {
                var lista = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                            .Where(w => w.EstoquePreMovimentoItemId == preMovimentoItemId).ToList();

                var qtd = lista.Sum(s => s.Quantidade);


                var quantidadeTotal = _estoquePreMovimentoItemRepository.Get(preMovimentoItemId).Quantidade;

                return quantidadeTotal - qtd;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<InformacaoLoteValidadeTodosDto> ObterLotesValidadesPreMovimento(long preMovimentoId, List<det> NFeItens)
        {
            var queryPreMovimentoItem = from preMovimentoItem in _estoquePreMovimentoItemRepository.GetAll()

                                        join importacaoProduto in _estoqueImportacaoProdutoRepository.GetAll()
                                        on preMovimentoItem.ProdutoId equals importacaoProduto.ProdutoId
                                        into _importacaoProduto
                                        from importacaoProduto in _importacaoProduto.DefaultIfEmpty()



                                        join loteValidade in _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                        on preMovimentoItem.Id equals loteValidade.EstoquePreMovimentoItemId
                                        into _loteValidadejoin
                                        from loteValidade in _loteValidadejoin.DefaultIfEmpty()

                                        where preMovimentoItem.PreMovimentoId == preMovimentoId
                                              && (preMovimentoItem.Produto.IsValidade || preMovimentoItem.Produto.IsLote)

                                        select new InformacaoLoteValidadeTodosDto
                                        {
                                            ProdutoId = preMovimentoItem.ProdutoId,
                                            CodigoProdutoNota = importacaoProduto.CodigoProdutoNota,
                                            Lote = loteValidade.LoteValidade.Lote,
                                            Validade = loteValidade.LoteValidade.Validade,
                                            LaboratorioId = loteValidade.LoteValidade.EstLaboratorioId,
                                            Laboratorio = new ProdutoLaboratorioDto
                                            {
                                                Id = loteValidade.LoteValidade.EstoqueLaboratorio != null ? loteValidade.LoteValidade.EstoqueLaboratorio.Id : 0
                                                             ,
                                                Descricao = loteValidade.LoteValidade.EstoqueLaboratorio != null ? loteValidade.LoteValidade.EstoqueLaboratorio.Descricao : string.Empty
                                            },
                                            EstoquePreMovimentoLoteValidadeId = loteValidade.Id,
                                            DescricaoProduto = preMovimentoItem.Produto.Descricao,
                                            PreMovimentoItemId=preMovimentoItem.Id,
                                            Quantidade =preMovimentoItem.Quantidade
                                        };
            //var query = 



            //            join importacaoProduto in _estoqueImportacaoProdutoRepository.GetAll()
            //            on loteValidadejoin.EstoquePreMovimentoItem.ProdutoId equals importacaoProduto.ProdutoId

            //            where preMovimentoItem.PreMovimentoId == preMovimentoId
            //               && (preMovimentoItem.Produto.IsValidade || preMovimentoItem.Produto.IsLote)

            //            select new InformacaoLoteValidadeTodosDto
            //            {
            //                ProdutoId = preMovimentoItem.ProdutoId,
            //                CodigoProdutoNota = importacaoProduto.CodigoProdutoNota,
            //                Lote = loteValidadejoin.LoteValidade.Lote,
            //                Validade = loteValidadejoin.LoteValidade.Validade,
            //                LaboratorioId = loteValidadejoin.LoteValidade.EstLaboratorioId,
            //                Laboratorio = new ProdutoLaboratorioDto { Id = loteValidadejoin.LoteValidade.EstoqueLaboratorio!=null? loteValidadejoin.LoteValidade.EstoqueLaboratorio.Id: 0
            //                                                        , Descricao = loteValidadejoin.LoteValidade.EstoqueLaboratorio!=null? loteValidadejoin.LoteValidade.EstoqueLaboratorio.Descricao: string.Empty},
            //                EstoquePreMovimentoLoteValidadeId = loteValidadejoin.Id,
            //                DescricaoProduto = preMovimentoItem.Produto.Descricao
            //            };

            var lista = queryPreMovimentoItem.ToList();
            int i = 0;
            foreach (var item in lista)
            {
                item.Index = i++;
                if (NFeItens != null && NFeItens.Count() > 0)
                {
                    item.DescricaoProdutoNota = NFeItens.Where(w => w.prod.cProd == item.CodigoProdutoNota).FirstOrDefault().prod.xProd;
                }
            }

            return lista;
        }

        public DefaultReturn<InformacaoLoteValidadeTodosDto> AtualizarLotesValidades(List<InformacaoLoteValidadeTodosDto> lotesValidades)
        {
            var _retornoPadrao = new DefaultReturn<InformacaoLoteValidadeTodosDto>();
            _retornoPadrao.Errors = new List<ErroDto>();


            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    LoteValidade _loteValidade;

                    foreach (var item in lotesValidades)
                    {
                        var loteValidade = _loteValidadeRepository.GetAll()
                                                                  .Where(w => w.Lote == item.Lote
                                                                          && w.Validade == item.Validade
                                                                          && w.EstLaboratorioId == item.LaboratorioId
                                                                          && w.ProdutoId == item.ProdutoId)
                                                                  .FirstOrDefault();

                        if (loteValidade != null)
                        {
                            _loteValidade = loteValidade;
                        }
                        else
                        {
                            _loteValidade = new LoteValidade { Lote = item.Lote, Validade = (DateTime)item.Validade, EstLaboratorioId = item.LaboratorioId, ProdutoId = item.ProdutoId };
                            _loteValidade.Id = _loteValidadeRepository.InsertAndGetId(_loteValidade);
                        }

                        EstoquePreMovimentoLoteValidade preMovimentoLoteValidade;
                        if (item.EstoquePreMovimentoLoteValidadeId != null)
                        {
                            preMovimentoLoteValidade = _estoquePreMovimentoLoteValidadeRepository.Get((long)item.EstoquePreMovimentoLoteValidadeId);
                            preMovimentoLoteValidade.LoteValidadeId = _loteValidade.Id;

                            _estoquePreMovimentoLoteValidadeRepository.Update(preMovimentoLoteValidade);
                        }
                        else
                        {
                            preMovimentoLoteValidade = new EstoquePreMovimentoLoteValidade();

                            preMovimentoLoteValidade.LoteValidadeId = _loteValidade.Id;
                            preMovimentoLoteValidade.EstoquePreMovimentoItemId = item.PreMovimentoItemId;
                            preMovimentoLoteValidade.LoteValidadeId = _loteValidade.Id;
                            preMovimentoLoteValidade.Quantidade = item.Quantidade;

                            _estoquePreMovimentoLoteValidadeRepository.Insert(preMovimentoLoteValidade);
                        }
                        
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }



            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }
    }
}
