﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Domain.Repositories;
using System.Data.Entity;
using Abp.AutoMapper;
using Abp.UI;
using Abp.Collections.Extensions;
using Abp.Extensions;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class TipoMovimentoAppService : SWMANAGERAppServiceBase, ITipoMovimentoAppService
    {

        private readonly IRepository<TipoMovimento, long> _tpoMovimentoRepository;


        public TipoMovimentoAppService(IRepository<TipoMovimento, long> tpoMovimentoRepository)
        {
            _tpoMovimentoRepository = tpoMovimentoRepository;
        }

        public async Task<PagedResultDto<TipoMovimentoDto>> Listar(bool isEntrada)
        {
            var query = _tpoMovimentoRepository
                  .GetAll()
                  .Where(w => w.IsEntrada == isEntrada);

            var contarTipoMovimentos = await query.CountAsync();


            var tipoMovimentos = await query
                .AsNoTracking()
                .ToListAsync();

            return new PagedResultDto<TipoMovimentoDto>(
                   contarTipoMovimentos,
                   tipoMovimentos.MapTo<List<TipoMovimentoDto>>()
                   );

        }

        public async Task<PagedResultDto<TipoMovimentoDto>> ListarTipoMovimentoDevolucao()
        {
            var query = _tpoMovimentoRepository
                  .GetAll()
                  .Where(w => w.Id == (long)EnumTipoMovimento.GastoSala_Saida
                           || w.Id == (long)EnumTipoMovimento.Paciente_Saida
                           || w.Id == (long)EnumTipoMovimento.Setor_Saida);

            var contarTipoMovimentos = await query.CountAsync();


            var tipoMovimentos = await query
                .AsNoTracking()
                .ToListAsync();

            return new PagedResultDto<TipoMovimentoDto>(
                   contarTipoMovimentos,
                   tipoMovimentos.MapTo<List<TipoMovimentoDto>>()
                   );

        }

        public async Task<ResultDropdownList> ListarDropdownEntrada(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<TipoMovimentoDto> tipoMovimentoDto = new List<TipoMovimentoDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isEntrada = true;

                var query = from p in _tpoMovimentoRepository.GetAll()
              .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
             m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
             m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
             ).Where(x => x.IsEntrada.Equals(isEntrada))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = queryResultPage.ToList();

                //  int total = await query.CountAsync();

                int total = result.Count();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownSaida(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<TipoMovimentoDto> tipoMovimentoDto = new List<TipoMovimentoDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isEntrada = false;

                var query = from p in _tpoMovimentoRepository.GetAll()
              .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
             m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
             m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
             ).Where(x => x.IsEntrada.Equals(isEntrada))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = queryResultPage.ToList();

                //  int total = await query.CountAsync();

                int total = result.Count();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownDevolucao(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<TipoMovimentoDto> tipoMovimentoDto = new List<TipoMovimentoDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                bool isEntrada = false;

                var query = from p in _tpoMovimentoRepository.GetAll()
              .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
             m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
             m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
             ).Where(x => x.Id == 2|| x.Id == 3|| x.Id == 4 || x.Id==5 )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = queryResultPage.ToList();

                //  int total = await query.CountAsync();

                int total = result.Count();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
