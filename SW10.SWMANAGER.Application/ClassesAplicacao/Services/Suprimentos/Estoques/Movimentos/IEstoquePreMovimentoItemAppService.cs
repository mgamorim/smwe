﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public interface IEstoquePreMovimentoItemAppService : IApplicationService
    {
        Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditar(EstoquePreMovimentoItemDto input);

        Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditarSaida(EstoquePreMovimentoItemDto input);
        
        Task Editar(EstoquePreMovimentoItemDto input);

        Task Excluir(long id);

        Task<PagedResultDto<EstoquePreMovimentoItemDto>> Listar(long Id);

        Task<EstoquePreMovimentoItemDto> Obter(long id);

        Task<PagedResultDto<EstoquePreMovimentoItemDto>> ListarPorMovimentacaoLoteValidade(ListarEstoquePreMovimentoInput input);

        Task<PagedResultDto<EstoquePreMovimentoItemDto>> ListarPorMovimentacaoLoteValidadeProduto(ListarEstoquePreMovimentoInput input);// string preMovimentoId, string produtoId);

        Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditarTransferencia(EstoquePreMovimentoItemDto input, long transferenciaProdutoId, long transferenciaProdutoItemId);

        Task<EstoqueTransferenciaProdutoItemDto> ObterTransferenciaItem(long id);

        Task ExcluirItemTransferencia(long id);

        Task<List<string>> ObterNumerosSerieProduto(long estoqueId, long produtoId);

        Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditarDevolucao(EstoquePreMovimentoItemDto input);

        List<EstoquePreMovimentoItemDto> ObterItensPorPreMovimento(long preMovimentoId);

        List<EstoquePreMovimentoItemSolicitacaoDto> ObterItensSolicitacaoPorPreMovimento(long preMovimentoId);

        PagedResultDto<LoteValidadeGridDto> ListarLoteValidadeJson(string json);

        PagedResultDto<NumeroSerieGridDto> ListarNumeroSerieJson(string json);

        PagedResultDto<LoteValidadeGridDto> ListarLoteValidadeJsonLista(string str); //List<LoteValidadeGridDto> lista);
    }
}
