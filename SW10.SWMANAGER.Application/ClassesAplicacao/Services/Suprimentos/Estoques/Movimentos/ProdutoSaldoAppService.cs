﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class ProdutoSaldoAppService : SWMANAGERAppServiceBase, IProdutoSaldoAppService
    {
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        private readonly IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepository;
        private readonly IRepository<Produto, long> _produtoRepository;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;


        //  private readonly IUnitOfWorkManager _unitOfWorkManager;


        public ProdutoSaldoAppService(IRepository<EstoqueMovimento, long> estoqueMovimentoRepository
            , IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository
            , IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository
            , IRepository<ProdutoSaldo, long> produtoSaldoRepository
            , IRepository<Produto, long> produtoRepository
            , IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository
            , IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository
            , IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository
           //  , IUnitOfWorkManager unitOfWorkManager
           )
        {
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _produtoSaldoRepository = produtoSaldoRepository;
            _produtoRepository = produtoRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            // _unitOfWorkManager = unitOfWorkManager;
        }

        public EstoquePreMovimentoItem PreMovimentoItem { get; set; }

        public async Task AtualizarSaldoMovimento(long movimentoId)
        {



            var movimento = _estoqueMovimentoRepository.Get(movimentoId);
            if (movimento != null)
            {
                var movimentoItens = _estoqueMovimentoItemRepository.GetAll()
                    .Where(w => w.MovimentoId == movimentoId).ToList();

                foreach (var item in movimentoItens)
                {

                    var produto = _produtoRepository.Get(item.ProdutoId);
                    item.Produto = produto;

                    if (item.Produto.IsLote || item.Produto.IsValidade)
                    {
                        var preMovimentoIemLotesValidades = _estoqueMovimentoLoteValidadeRepository.GetAll()
                            .Where(w => w.EstoqueMovimentoItemId == item.Id).ToList();

                        foreach (var itemLoteValidade in preMovimentoIemLotesValidades)
                        {
                            var saldo = _produtoSaldoRepository.GetAll()
                                .Where(w => w.EstoqueId == movimento.EstoqueId
                                         && w.LoteValidadeId == itemLoteValidade.LoteValidadeId
                                         && w.ProdutoId == item.ProdutoId
                                         ).FirstOrDefault();

                            var quantidade = CalcularQuantidade(movimento.IsEntrada, itemLoteValidade.Quantidade);

                            if (saldo == null)
                            {
                                var produtoSaldo = new ProdutoSaldo();
                                produtoSaldo.EstoqueId = (long)movimento.EstoqueId;
                                produtoSaldo.LoteValidadeId = itemLoteValidade.LoteValidadeId;
                                produtoSaldo.ProdutoId = item.ProdutoId;
                                produtoSaldo.QuantidadeAtual = quantidade;

                                await _produtoSaldoRepository.InsertAsync(produtoSaldo);
                            }
                            else
                            {
                                saldo.QuantidadeAtual = saldo.QuantidadeAtual + quantidade;

                                if (movimento.IsEntrada)
                                {
                                    saldo.QuantidadeEntradaPendente = saldo.QuantidadeEntradaPendente - itemLoteValidade.Quantidade;
                                }
                                else
                                {
                                    saldo.QuantidadeSaidaPendente = saldo.QuantidadeSaidaPendente - itemLoteValidade.Quantidade;
                                }


                                await _produtoSaldoRepository.UpdateAsync(saldo);
                            }
                        }
                    }
                    else
                    {
                        var saldo = _produtoSaldoRepository.GetAll()
                               .Where(w => w.EstoqueId == (long)movimento.EstoqueId
                                        && w.ProdutoId == item.ProdutoId
                                        ).FirstOrDefault();

                        var quantidade = CalcularQuantidade(movimento.IsEntrada, item.Quantidade);

                        if (saldo == null)
                        {
                            var produtoSaldo = new ProdutoSaldo();
                            produtoSaldo.EstoqueId = (long)movimento.EstoqueId;
                            produtoSaldo.ProdutoId = item.ProdutoId;
                            produtoSaldo.QuantidadeAtual = quantidade;

                            await _produtoSaldoRepository.InsertAsync(produtoSaldo);
                        }
                        else
                        {
                            saldo.QuantidadeAtual = saldo.QuantidadeAtual + quantidade;


                            if (movimento.IsEntrada)
                            {
                                saldo.QuantidadeEntradaPendente = saldo.QuantidadeEntradaPendente - item.Quantidade;
                            }
                            else
                            {
                                saldo.QuantidadeSaidaPendente = saldo.QuantidadeSaidaPendente - item.Quantidade;
                            }



                            await _produtoSaldoRepository.UpdateAsync(saldo);
                        }
                    }


                }
            }
        }


        decimal CalcularQuantidade(bool isEntrada, decimal quantadade)
        {
            return (((isEntrada ? 1 : 0) * 2) - 1) * quantadade;
        }

        public void AtulizarSaldoPreMovimentoItemPreMovimento(EstoquePreMovimento preMovimento)//EstoquePreMovimentoItem preMovimentoItem)//, EstoquePreMovimento preMovimento)
        {

            // var preMovimento = preMovimentoItem.EstoquePreMovimento; //await _estoquePreMovimentoRepository.GetAsync(preMovimentoItem.PreMovimentoId);
            var preMovimentoItem = PreMovimentoItem;
            if (preMovimento != null)
            {

                var produtoSaldo = _produtoSaldoRepository.GetAll()
                                    .Where(w => w.ProdutoId == preMovimentoItem.ProdutoId
                                             && w.EstoqueId == preMovimento.EstoqueId
                                             && w.LoteValidadeId == null).FirstOrDefault();

                if (produtoSaldo != null)
                {
                    if (preMovimento.IsEntrada)
                    {
                        produtoSaldo.QuantidadeEntradaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }
                    else
                    {
                        produtoSaldo.QuantidadeSaidaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }
                    _produtoSaldoRepository.UpdateAsync(produtoSaldo);

                }
                else
                {
                    produtoSaldo = new ProdutoSaldo();
                    produtoSaldo.EstoqueId = (long)preMovimento.EstoqueId;
                    produtoSaldo.ProdutoId = preMovimentoItem.ProdutoId;

                    if (preMovimento.IsEntrada)
                    {
                        produtoSaldo.QuantidadeEntradaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }
                    else
                    {
                        produtoSaldo.QuantidadeSaidaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }

                    _produtoSaldoRepository.InsertAsync(produtoSaldo);

                }
            }
        }

        public async Task AtulizarSaldoPreMovimentoItem(EstoquePreMovimentoItem preMovimentoItem)
        {

            var preMovimento = await _estoquePreMovimentoRepository.GetAsync(preMovimentoItem.PreMovimentoId);
            if (preMovimento != null)
            {

                var produtoSaldo = _produtoSaldoRepository.GetAll()
                                    .Where(w => w.ProdutoId == preMovimentoItem.ProdutoId
                                             && w.EstoqueId == preMovimento.EstoqueId).FirstOrDefault();

                if (produtoSaldo != null)
                {
                    if (preMovimento.IsEntrada)
                    {
                        produtoSaldo.QuantidadeEntradaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }
                    else
                    {
                        produtoSaldo.QuantidadeSaidaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }
                    await _produtoSaldoRepository.UpdateAsync(produtoSaldo);

                }
                else
                {
                    produtoSaldo = new ProdutoSaldo();
                    produtoSaldo.EstoqueId = (long)preMovimento.EstoqueId;
                    produtoSaldo.ProdutoId = preMovimentoItem.ProdutoId;

                    if (preMovimento.IsEntrada)
                    {
                        produtoSaldo.QuantidadeEntradaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }
                    else
                    {
                        produtoSaldo.QuantidadeSaidaPendente = ObterQuantidadeProduto(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada);
                    }

                    await _produtoSaldoRepository.InsertAsync(produtoSaldo);

                }
            }

        }

        public void AtulizarSaldoPreMovimentoItemLoteValidade(EstoquePreMovimentoLoteValidade preMovimentoItemLoteValidade)
        {
            try
            { 
            var preMovimentoItem =  _estoquePreMovimentoItemRepository.Get(preMovimentoItemLoteValidade.EstoquePreMovimentoItemId);

            if (preMovimentoItem != null)
            {

                var preMovimento =  _estoquePreMovimentoRepository.Get(preMovimentoItem.PreMovimentoId);

                    if (preMovimento != null)
                    {

                        var produtoSaldo = _produtoSaldoRepository.GetAll()
                                            .Where(w => w.ProdutoId == preMovimentoItem.ProdutoId
                                                     && w.EstoqueId == (long)preMovimento.EstoqueId
                                                     && w.LoteValidadeId == preMovimentoItemLoteValidade.LoteValidadeId).FirstOrDefault();

                        var quantidade = preMovimentoItemLoteValidade.Quantidade;



                        if (produtoSaldo != null)
                        {
                            if (preMovimento.IsEntrada)
                            {
                                produtoSaldo.QuantidadeEntradaPendente += quantidade; 
                            }
                            else
                            {
                                produtoSaldo.QuantidadeSaidaPendente += quantidade; 
                            }
                            _produtoSaldoRepository.Update(produtoSaldo);

                        }
                        else
                        {
                            produtoSaldo = new ProdutoSaldo();
                            produtoSaldo.EstoqueId = (long)preMovimento.EstoqueId;
                            produtoSaldo.ProdutoId = preMovimentoItem.ProdutoId;
                            produtoSaldo.LoteValidadeId = preMovimentoItemLoteValidade.LoteValidadeId;

                            if (preMovimento.IsEntrada)
                            {
                                produtoSaldo.QuantidadeEntradaPendente = quantidade; 
                            }
                            else
                            {
                                produtoSaldo.QuantidadeSaidaPendente = quantidade;  //= ObterQuantidadeProdutoLoteValidade(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada, preMovimentoItemLoteValidade.LoteValidadeId);
                            }

                            _produtoSaldoRepository.InsertAsync(produtoSaldo);

                        }

                    }
                    
                }
            }
            catch(Exception ex)
            {

            }
        }

        public void AtulizarSaldoPreMovimentoItemLoteValidadePreMovimento(EstoquePreMovimentoLoteValidade preMovimentoItemLoteValidade)
        {
            try
            {
                var preMovimentoItem = preMovimentoItemLoteValidade.EstoquePreMovimentoItem;//await _estoquePreMovimentoItemRepository.GetAsync(preMovimentoItemLoteValidade.EstoquePreMovimentoItemId);

                if (preMovimentoItem != null)
                {
                    var preMovimento = _estoquePreMovimentoRepository.Get(preMovimentoItem.PreMovimentoId);

                    if (preMovimento != null)
                    {

                        var produtoSaldo = _produtoSaldoRepository.GetAll()
                                            .Where(w => w.ProdutoId == preMovimentoItem.ProdutoId
                                                     && w.EstoqueId == (long)preMovimento.EstoqueId
                                                     && w.LoteValidadeId == preMovimentoItemLoteValidade.LoteValidadeId).FirstOrDefault();

                        var quantidade = CalcularQuantidade(preMovimento.IsEntrada, preMovimentoItemLoteValidade.Quantidade);



                        if (produtoSaldo != null)
                        {
                            if (preMovimento.IsEntrada)
                            {
                                produtoSaldo.QuantidadeEntradaPendente = ObterQuantidadeProdutoLoteValidade(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada, preMovimentoItemLoteValidade.LoteValidadeId);
                            }
                            else
                            {
                                produtoSaldo.QuantidadeSaidaPendente = ObterQuantidadeProdutoLoteValidade(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada, preMovimentoItemLoteValidade.LoteValidadeId);
                            }
                            _produtoSaldoRepository.UpdateAsync(produtoSaldo);

                        }
                        else
                        {
                            produtoSaldo = new ProdutoSaldo();
                            produtoSaldo.EstoqueId = (long)preMovimento.EstoqueId;
                            produtoSaldo.ProdutoId = preMovimentoItem.ProdutoId;
                            produtoSaldo.LoteValidadeId = preMovimentoItemLoteValidade.LoteValidadeId;

                            if (preMovimento.IsEntrada)
                            {
                                produtoSaldo.QuantidadeEntradaPendente = ObterQuantidadeProdutoLoteValidade(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada, preMovimentoItemLoteValidade.LoteValidadeId);
                            }
                            else
                            {
                                produtoSaldo.QuantidadeSaidaPendente = ObterQuantidadeProdutoLoteValidade(preMovimentoItem.ProdutoId, (long)preMovimento.EstoqueId, preMovimento.IsEntrada, preMovimentoItemLoteValidade.LoteValidadeId);
                            }

                            _produtoSaldoRepository.InsertAsync(produtoSaldo);

                        }

                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        decimal ObterQuantidadeProduto(long produtoId, long estoqueId, bool isEntrada)
        {
            var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                                    join item in _estoquePreMovimentoItemRepository.GetAll()
                                    on mov.Id equals item.PreMovimentoId
                                    into movjoin
                                    from joinedmov in movjoin.DefaultIfEmpty()
                                    where mov.EstoqueId == estoqueId
                                       && joinedmov.ProdutoId == produtoId
                                       && mov.IsEntrada == isEntrada
                                       && mov.PreMovimentoEstadoId != 2
                                    select new { Quantidade = joinedmov.Quantidade };

            return queryPreMovimento.ToList().Sum(s => s.Quantidade);

        }


        decimal ObterQuantidadeProdutoLoteValidade(long produtoId, long estoqueId, bool isEntrada, long loteValidadeId)
        {
            var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                                    join item in _estoquePreMovimentoItemRepository.GetAll()
                                    on mov.Id equals item.PreMovimentoId

                                    join iltv in _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                  on item.Id equals iltv.EstoquePreMovimentoItemId

                                  into movjoin
                                    from joinedmov in movjoin.DefaultIfEmpty()
                                    where mov.EstoqueId == estoqueId
                                       && item.ProdutoId == produtoId
                                       && mov.IsEntrada == isEntrada
                                       && joinedmov.LoteValidadeId == loteValidadeId
                                       && mov.PreMovimentoEstadoId != 2
                                    select new { Quantidade = joinedmov.Quantidade };

            return queryPreMovimento.ToList().Sum(s => s.Quantidade);

        }


        public async Task<ResultDropdownList> ListarProdutoDropdown(DropdownInput dropdownInput)
        {

            long produtoId = long.Parse(dropdownInput.filtros[0]);
            long estoqueId = long.Parse(dropdownInput.filtros[1]);
            long laboratorioId = long.Parse(dropdownInput.filtros[2]);

            // return await ListarCodigoDescricaoDropdown<Produto>(dropdownInput, _produtoRepositorio);

            return await ListarDropdownLambda(dropdownInput
                                                     , _produtoSaldoRepository
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search)
                                                          || (m.LoteValidade.Validade.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                                                          || m.LoteValidade.Lote.ToLower().Contains(dropdownInput.search.ToLower()))
                                                          && m.ProdutoId == produtoId
                                                          && m.EstoqueId == estoqueId
                                                          && m.LoteValidade.EstLaboratorioId == laboratorioId
                                                     )

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat("Lt: " , p.LoteValidade.Lote.ToString(), " Validade: ", p.LoteValidade.Validade.ToShortDateString()) }
                                                    , o => o.Descricao
                                                    );
        }
    }
}
