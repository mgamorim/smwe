﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes
{
    public class PreMovimentoValidacaoService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        IRepository<LoteValidade, long> _loteValidadeRepository;
        IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        IRepository<Produto, long> _produtoRepository;
        IRepository<ProdutoUnidade, long> _produtoUnidadeRepository;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;



        public PreMovimentoValidacaoService(IUnitOfWorkManager unitOfWorkManager,
                                            IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
                                            IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
                                            IRepository<EstoqueMovimento, long> estoqueMovimentoRepository,
                                            IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository,
                                            IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository,
                                            IRepository<LoteValidade, long> loteValidadeRepository,
                                            IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
                                            IRepository<Produto, long> produtoRepository,
                                            IRepository<ProdutoUnidade, long> produtoUnidadeRepository,
                                            IRepository<EstoqueGrupo, long> estoqueGrupoRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _loteValidadeRepository = loteValidadeRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _produtoRepository = produtoRepository;
            _produtoUnidadeRepository = produtoUnidadeRepository;
            _estoqueGrupoRepository = estoqueGrupoRepository;
        }



        List<ErroDto> Lista = new List<ErroDto>();
        public List<ErroDto> Validar(EstoquePreMovimentoDto preMovimento, List<EstoquePreMovimentoItemDto> itens)
        {
            if (ExisteConfirmacaoEntrada(preMovimento))
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0002", Descricao = "Esta entrada já foi confirmada." });
            }

            ValidarTotalProdutos(preMovimento);
            ValidarTotalDocumento(preMovimento);

            foreach (var item in itens)
            {
                ValidarItemProduto(item);
                var produto = _produtoRepository.Get(item.ProdutoId);
                item.Produto = produto.MapTo<ProdutoDto>();

                ValidarGrupoEstoqueItem(item, (long)preMovimento.EstoqueId);

                if (!preMovimento.IsEntrada)
                {

                   
                    var validarDataVencimeneto = preMovimento.TipoDocumentoId != 4;
                    item.EstoqueId = (long)preMovimento.EstoqueId;

                    ValidarItemSaida(item, (produto != null && (produto.IsValidade || produto.IsLote)), validarDataVencimeneto);
                }
            }

            return Lista;
        }

        public void ValidarItemProduto(EstoquePreMovimentoItemDto item)
        {
            if (!String.IsNullOrEmpty(item.NumeroSerie) && item.Quantidade != 1)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0002", Descricao = "Para produtos com número de série deve ser sempre 1." });
            }

        }


        public bool ExisteLoteValidadePendente(EstoquePreMovimentoDto preMovimento)
        {

            var preMovimentosItem = _estoquePreMovimentoItemRepository
            .GetAll()
           .Where(w => w.PreMovimentoId == preMovimento.Id
                    && (w.Produto.IsLote || w.Produto.IsValidade))


           .ToList();

            foreach (var item in preMovimentosItem)//.Where(w => w.Produto.IsLote || w.Produto.IsValidade))
            {
                var queryLoteValidade = _estoquePreMovimentoLoteValidadeRepository
                    .GetAll()
                   .Where(m => m.EstoquePreMovimentoItemId == item.Id);

                var lotesValidade = queryLoteValidade.ToList();

                var somaLoteValidade = lotesValidade.Sum(s => s.Quantidade);


                if (somaLoteValidade != item.Quantidade)
                {
                    return true;
                }
            }
            return false;

        }

        bool ExisteConfirmacaoEntrada(EstoquePreMovimentoDto preMovimento)
        {
            var existeConfirmacao = _estoqueMovimentoRepository.GetAll().Where(w => w.EstoquePreMovimentoId == preMovimento.Id).FirstOrDefault();
            return existeConfirmacao != null;
        }

        public List<ErroDto> ValidarItemSaida(EstoquePreMovimentoItemDto preMovimentoItem, bool validarLoteValidade, bool validarDataVencimeneto)
        {
            ValidarItem(preMovimentoItem);
            ValidarSeExisteProdutoNoEstoque(preMovimentoItem);

            if (validarLoteValidade)
            {
                if (preMovimentoItem.LoteValidadeId != null)
                {
                    LoteValidade loteValidade = _loteValidadeRepository.Get((long)preMovimentoItem.LoteValidadeId);



                    ValidarLoteValidade(new EstoquePreMovimentoLoteValidadeDto
                    {
                        Lote = loteValidade.Lote
                                                                                                                            ,
                        Validade = loteValidade.Validade
                                                                                                                            ,
                        LaboratorioId = loteValidade.EstLaboratorioId
                                                                                                                            ,
                        EstoquePreMovimentoItemId = preMovimentoItem.Id
                    }
                    , validarDataVencimeneto);
                }
            }
            return Lista;
        }


        public List<ErroDto> ValidarItem(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            if (ExisteConfirmacaoEntrada(new EstoquePreMovimentoDto { Id = preMovimentoItem.PreMovimentoId }))
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0002", Descricao = "Esta entrada já foi confirmada." });
            }

            ValidarProduto(preMovimentoItem.ProdutoId);
            ValidarUnidade(preMovimentoItem);

            return Lista;
        }

        void ValidarProduto(long produtoId)
        {
            if (produtoId == 0)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0005", Descricao = "Produto obrigatório." });
            }
            else
            {
                var produto = _produtoRepository.Get(produtoId);
                if (produto == null)
                {
                    Lista.Add(new ErroDto { CodigoErro = "EST0006", Descricao = "Produto não cadastrado." });
                }
                else
                {
                    if (produto.IsPrincipal)
                    {
                        Lista.Add(new ErroDto { CodigoErro = "EST0004", Descricao = "Não é permitido utilizar um produto mestre em entrada de produto." });
                    }

                    if (produto.IsBloqueioCompra)
                    {
                        Lista.Add(new ErroDto { CodigoErro = "EST0009", Descricao = "Este produto não movimento estoque." });
                    }

                    if (!produto.IsAtivo)
                    {
                        Lista.Add(new ErroDto { CodigoErro = "EST0010", Descricao = "Produto inativo." });
                    }


                }
            }
        }

        void ValidarUnidade(EstoquePreMovimentoItemDto preMovimentoItem)
        {

            if (preMovimentoItem.ProdutoUnidadeId == null || preMovimentoItem.ProdutoUnidadeId == 0)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0008", Descricao = "Unidade obrigatória." });
            }
            else
            {
                var produtoUnidade = _produtoUnidadeRepository.GetAll().Where(w => w.ProdutoId == preMovimentoItem.ProdutoId
                                                                                && w.UnidadeId == preMovimentoItem.ProdutoUnidadeId);

                if (produtoUnidade == null || produtoUnidade.Count() == 0)
                {
                    Lista.Add(new ErroDto { CodigoErro = "EST0007", Descricao = "Unidade não relacionada ao produto selecionado." });
                }
            }
        }

        public List<ErroDto> ValidarLoteValidade(EstoquePreMovimentoLoteValidadeDto loteValidade, bool validarDataVencimento)
        {
            if (validarDataVencimento && loteValidade.Validade < DateTime.Now)
            {
                Lista.Add(new ErroDto { CodigoErro = "LT0001", Descricao = "Validade Vencida." });
            }

            //var preMovimentoItem = _estoquePreMovimentoItemRepository.Get(loteValidade.EstoquePreMovimentoItemId);
            //if (preMovimentoItem != null)
            //{
            //    if (ExisteConfirmacaoEntrada(new EstoquePreMovimentoDto { Id = preMovimentoItem.PreMovimentoId }))
            //    {
            //        Lista.Add(new ErroDto { CodigoErro = "EST0002", Descricao = "Está entrada já foi confirmada." });
            //    }
            //}
            return Lista;
        }

        public void ValidarSeExisteProdutoNoEstoque(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            if (preMovimentoItem.Produto.IsValidade || preMovimentoItem.Produto.IsLote)
            {

                if (preMovimentoItem.LoteValidadeId == null)
                {
                    var preMovimentoLoteValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                        .Where(w => w.EstoquePreMovimentoItemId == preMovimentoItem.Id).FirstOrDefault();

                    preMovimentoItem.LoteValidadeId = preMovimentoLoteValidade.LoteValidadeId;
                }

                ValidarSeExisteProdutoLoteValidadeNoEstoque(preMovimentoItem);
            }
            else
            {
                ValidarSeExisteProdutoSemLoteValidadeNoEstoque(preMovimentoItem);
            }

        }




        void ValidarSeExisteProdutoLoteValidadeNoEstoque(EstoquePreMovimentoItemDto preMovimentoItem)
        {

            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        join iltv in _estoqueMovimentoLoteValidadeRepository.GetAll()
                        on item.Id equals iltv.EstoqueMovimentoItemId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && item.ProdutoId == preMovimentoItem.ProdutoId
                           && joinedmov.LoteValidadeId == preMovimentoItem.LoteValidadeId
                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade) };

            //var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
            //                        join item in _estoquePreMovimentoItemRepository.GetAll()
            //                        on mov.Id equals item.PreMovimentoId
            //                        join iltv in _estoquePreMovimentoLoteValidadeRepository.GetAll()
            //                        on item.Id equals iltv.EstoquePreMovimentoItemId
            //                        into movjoin
            //                        from joinedmov in movjoin.DefaultIfEmpty()
            //                        where mov.EstoqueId == preMovimentoItem.EstoqueId
            //                           && item.ProdutoId == preMovimentoItem.ProdutoId
            //                           && joinedmov.LoteValidadeId == preMovimentoItem.LoteValidadeId

            //                           ///Todo: Verificar Regra sobre movimentações ainda não confirmadas
            //                           && mov.Id == preMovimentoItem.PreMovimentoId

            //                           && item.Id != preMovimentoItem.Id

            //                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade) };




            var queryList = query.ToList();

            var lotesValidade = queryList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade) }).ToList();

            var quantidadeEstoque = lotesValidade.Sum(s => s.Qtd);

            if (quantidadeEstoque < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0003", Descricao = string.Format( "Quantidade do produto {0} é insuficiente nesse estoque no Lote/Validade informado.", preMovimentoItem.Produto.Descricao) });
            }
        }

       void ValidarSeExisteProdutoSemLoteValidadeNoEstoque(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && joinedmov.ProdutoId == preMovimentoItem.ProdutoId
                        select new { Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade) };



            var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                                    join item in _estoquePreMovimentoItemRepository.GetAll()
                                    on mov.Id equals item.PreMovimentoId
                                    into movjoin
                                    from joinedmov in movjoin.DefaultIfEmpty()
                                    where mov.EstoqueId == preMovimentoItem.EstoqueId
                                       && joinedmov.ProdutoId == preMovimentoItem.ProdutoId
                                       && mov.Id == preMovimentoItem.PreMovimentoId
                                       && joinedmov.Id != preMovimentoItem.Id
                                    select new { Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade) };




            var queryList = query.ToList();
            var queryPreMovimentoList = queryPreMovimento.ToList();

            queryList.AddRange(queryPreMovimentoList);

            var lotesValidade = queryList.Sum(s => s.Quantidade);

            if (lotesValidade < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0003", Descricao = string.Format("Quantidade do produto {0} é insuficiente no estoque informado.", preMovimentoItem.Produto.Descricao) });
            }


        }

        //void ValidarSaidaItensEstoque(List<EstoquePreMovimentoItemDto> preMovimentoItens)
        //{
        //    foreach (var item in preMovimentoItens)
        //    {
        //        ValidarSeExisteProdutoNoEstoque(item);
        //    }
        //}

        public List<ErroDto> ValidarSaidaEstoque(EstoquePreMovimento preMovimentoSaida)
        {


            var itensSaida = _estoquePreMovimentoItemRepository.GetAll()
            .Include(i => i.Produto)
            .Include(i => i.ProdutoUnidade)
            .Where(w => w.PreMovimentoId == preMovimentoSaida.Id).ToList();

            foreach (var item in itensSaida)
            {
                var itemSaidaDto = EstoquePreMovimentoItemAppService.MapPreMovimentoItem(item);
                itemSaidaDto.EstoqueId = (long)preMovimentoSaida.EstoqueId;

                var loteValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll().Where(w => w.EstoquePreMovimentoItemId == item.Id).FirstOrDefault();
                if (loteValidade != null)
                {
                    itemSaidaDto.LoteValidadeId = loteValidade.LoteValidadeId;
                }
                ValidarSeExisteProdutoNoEstoque(itemSaidaDto);

            }

            return Lista;
        }



        public List<ErroDto> ValidarBaixaVale(EstoqueMovimento movimentoBaixa, List<long> movimentoIds)
        {
            decimal somaValores = 0;

            List<EstoqueMovimento> movimentos = new List<EstoqueMovimento>();

            foreach (var id in movimentoIds)
            {
                var movimento = _estoqueMovimentoRepository.Get(id);

                if (movimento != null)
                {
                    movimentos.Add(movimento);

                   // somaValores += movimento.TotalDocumento;
                }
            }

            if(movimentos.Sum(s=> s.TotalDocumento) != movimentoBaixa.TotalDocumento)
            {
                ErroDto erroDto = new ErroDto { CodigoErro = "BAX0001"};
                Lista.Add(erroDto);
            }


            if(movimentos.GroupBy(g=> g.SisFornecedorId).Count()>1)
            {
                ErroDto erroDto = new ErroDto { CodigoErro = "BAX0002"};
                Lista.Add(erroDto);
            }
            else if(movimentos[0].SisFornecedorId != movimentoBaixa.SisFornecedorId)
            {
                ErroDto erroDto = new ErroDto { CodigoErro = "BAX0003" };
                Lista.Add(erroDto);
            }





            return Lista;

        }

        public List<ErroDto> ValidarFornecedoresBaixaVale(string baixasIds)
        {
            List<long> ids = new List<long>();
            List<EstoqueMovimento> movimentos = new List<EstoqueMovimento>();

            baixasIds.TrimEnd('-').Split('-').ToList().ForEach(f => ids.Add(long.Parse(f)));

            foreach (var id in ids)
            {
                var movimento = _estoqueMovimentoRepository.Get(id);

                if (movimento != null)
                {
                    movimentos.Add(movimento);
                }
            }

            if (movimentos.GroupBy(g => g.SisFornecedorId).Count() > 1)
            {
                ErroDto erroDto = new ErroDto { CodigoErro = "BAX0002" };
                Lista.Add(erroDto);
            }


            return Lista;
        }



        public List<ErroDto> ValidarItemDevolucao(EstoquePreMovimentoItemDto preMovimentoItem, bool validarLoteValidade, bool validarDataVencimeneto)
        {
            ValidarItem(preMovimentoItem);
            ValidarSeExisteSaidaProduto(preMovimentoItem);
          
            return Lista;
        }

        void ValidarSeExisteSaidaProduto(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            if (preMovimentoItem.Produto.IsValidade || preMovimentoItem.Produto.IsLote)
            {
                if (preMovimentoItem.LoteValidadeId == null)
                {
                    var preMovimentoLoteValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                        .Where(w => w.EstoquePreMovimentoItemId == preMovimentoItem.Id).FirstOrDefault();

                    preMovimentoItem.LoteValidadeId = preMovimentoLoteValidade.LoteValidadeId;
                }
                ValidarSeExisteSaidaProdutoLoteValidade(preMovimentoItem);
            }
            else
            {
                ValidarSeExisteSaidaProdutoSemLoteValidade(preMovimentoItem);
            }
        }

        void ValidarSeExisteSaidaProdutoLoteValidade(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        join iltv in _estoqueMovimentoLoteValidadeRepository.GetAll()
                        on item.Id equals iltv.EstoqueMovimentoItemId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && item.ProdutoId == preMovimentoItem.ProdutoId
                           && joinedmov.LoteValidadeId == preMovimentoItem.LoteValidadeId
                           && mov.IsEntrada == false

                           && mov.EstTipoMovimentoId == preMovimentoItem.EstoquePreMovimento.EstTipoMovimentoId
                           && mov.EstoqueId == preMovimentoItem.EstoquePreMovimento.EstoqueId
                           && (preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId==null ||  mov.UnidadeOrganizacionalId == preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId)
                           && (preMovimentoItem.EstoquePreMovimento.AtendimentoId == null || mov.AtendimentoId == preMovimentoItem.EstoquePreMovimento.AtendimentoId)

                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = joinedmov.Quantidade };

            var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                                    join item in _estoquePreMovimentoItemRepository.GetAll()
                                    on mov.Id equals item.PreMovimentoId
                                    join iltv in _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                    on item.Id equals iltv.EstoquePreMovimentoItemId
                                    //into movjoin
                                   // from joinedmov in movjoin.DefaultIfEmpty()
                                    where //mov.EstoqueId == preMovimentoItem.EstoqueId
                                      // && item.ProdutoId == preMovimentoItem.ProdutoId
                                      // && joinedmov.LoteValidadeId == preMovimentoItem.LoteValidadeId
                                       //  && mov.IsEntrada == false
                                       ///Todo: Verificar Regra sobre movimentações ainda não confirmadas
                                        mov.Id == preMovimentoItem.PreMovimentoId
                                     //  && item.Id != preMovimentoItem.Id
                                    select new { LoteValidade = iltv.LoteValidade, Quantidade = iltv.Quantidade };



            var queryDevolucao = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        join iltv in _estoqueMovimentoLoteValidadeRepository.GetAll()
                        on item.Id equals iltv.EstoqueMovimentoItemId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && item.ProdutoId == preMovimentoItem.ProdutoId
                           && joinedmov.LoteValidadeId == preMovimentoItem.LoteValidadeId
                           && mov.IsEntrada == true
                           && mov.EstTipoOperacaoId == (long)EnumTipoOperacao.Devolucao

                           && mov.EstTipoMovimentoId == preMovimentoItem.EstoquePreMovimento.EstTipoMovimentoId
                           && mov.EstoqueId == preMovimentoItem.EstoquePreMovimento.EstoqueId
                           && (preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId == null || mov.UnidadeOrganizacionalId == preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId)
                           && (preMovimentoItem.EstoquePreMovimento.AtendimentoId == null || mov.AtendimentoId == preMovimentoItem.EstoquePreMovimento.AtendimentoId)

                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = joinedmov.Quantidade };






            var queryList = query.ToList();
            var lotesValidade = queryList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade) }).ToList();
            var quantidadeEstoque = lotesValidade.Sum(s => s.Qtd);


            var queryDevolucaoList = queryDevolucao.ToList();
            var lotesValidadeDevolucao = queryDevolucaoList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade) }).ToList();
            var quantidadeEstoqueDevolucao = lotesValidadeDevolucao.Sum(s => s.Qtd);


            var queryPreMovimentoList = queryPreMovimento.ToList();
            var lotesValidadePreMovimentoDevolucao = queryPreMovimentoList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade) }).ToList();
            var quantidadeEstoquePreMovimentoDevolucao = lotesValidadePreMovimentoDevolucao.Sum(s => s.Qtd);


            



            if ((quantidadeEstoque - quantidadeEstoqueDevolucao - quantidadeEstoquePreMovimentoDevolucao) < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "DEV0001", Descricao = "Quantidade de produto insuficiente nesse estoque no Lote/Validade informado." });
            }
        }

        void ValidarSeExisteSaidaProdutoSemLoteValidade(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && joinedmov.ProdutoId == preMovimentoItem.ProdutoId
                           && mov.IsEntrada == false
                           && mov.EstTipoMovimentoId == preMovimentoItem.EstoquePreMovimento.EstTipoMovimentoId
                           && (preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId == null || mov.UnidadeOrganizacionalId == preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId)
                           && (preMovimentoItem.EstoquePreMovimento.PacienteId == null || mov.PacienteId == preMovimentoItem.EstoquePreMovimento.PacienteId)

                        select new { Quantidade =  joinedmov.Quantidade };



            var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                                    join item in _estoquePreMovimentoItemRepository.GetAll()
                                    on mov.Id equals item.PreMovimentoId
                                    where 
                                        mov.Id == preMovimentoItem.PreMovimentoId
                                    select new { Quantidade = item.Quantidade};

            var queryDevolucao = from mov in _estoqueMovimentoRepository.GetAll()
                                 join item in _estoqueMovimentoItemRepository.GetAll()
                                 on mov.Id equals item.MovimentoId
                                 where mov.EstoqueId == preMovimentoItem.EstoqueId
                                    && item.ProdutoId == preMovimentoItem.ProdutoId
                                    && mov.IsEntrada == true
                                    && mov.EstTipoOperacaoId == (long)EnumTipoOperacao.Devolucao
                                    && mov.EstTipoMovimentoId == preMovimentoItem.EstoquePreMovimento.EstTipoMovimentoId
                                    && mov.EstoqueId == preMovimentoItem.EstoquePreMovimento.EstoqueId
                                    && (preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId == null || mov.UnidadeOrganizacionalId == preMovimentoItem.EstoquePreMovimento.UnidadeOrganizacionalId)
                                    && (preMovimentoItem.EstoquePreMovimento.PacienteId == null || mov.PacienteId == preMovimentoItem.EstoquePreMovimento.PacienteId)

                                 select new { Quantidade = item.Quantidade };


            var queryList = query.ToList();
            var quantidadeEstoque = queryList.Sum(s => s.Quantidade);

            var queryDevolucaoList = queryDevolucao.ToList();
            var quantidadeEstoqueDevolucao = queryDevolucaoList.Sum(s => s.Quantidade);

            var queryPreMovimentoList = queryPreMovimento.ToList();
            var quantidadeEstoquePreMovimentoDevolucao = queryPreMovimentoList.Sum(s => s.Quantidade);

            if ((quantidadeEstoque - quantidadeEstoqueDevolucao - quantidadeEstoquePreMovimentoDevolucao) < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "DEV0001", Descricao = "Quantidade de produto insuficiente no estoque informado." });
            }
        }

        void ValidarTotalProdutos(EstoquePreMovimentoDto preMovimento)
        {
            var itens = _estoquePreMovimentoItemRepository.GetAll()
                                                          .Where(w => w.PreMovimentoId == preMovimento.Id)
                                                          .ToList();

            decimal totalItens =0;

            foreach (var item in itens)
            {
                var unidade = _produtoUnidadeRepository.GetAll()
                                                       .Where(w => w.UnidadeId == item.ProdutoUnidadeId)
                                                       .Include(i => i.Unidade)
                                                       .FirstOrDefault();
                                                       
                if(unidade!=null)
                {
                    totalItens += Math.Round(item.CustoUnitario * (item.Quantidade / unidade.Unidade.Fator),2);
                }
            }

            if (totalItens != preMovimento.TotalProduto)
            {
                Lista.Add(new ErroDto { CodigoErro = "", Descricao = "Soma dos valores dos itens diferente do informado no Total Produtos." });
            }

        }

        void ValidarTotalDocumento(EstoquePreMovimentoDto preMovimento)
        {
            preMovimento.DescontoPer = preMovimento.DescontoPer != null ? preMovimento.DescontoPer : 0;

            var valorTotal = preMovimento.TotalProduto - preMovimento.DescontoPer + preMovimento.AcrescimoDecrescimo;

            if(valorTotal!= preMovimento.TotalDocumento)
            {
                Lista.Add(new ErroDto { CodigoErro = "", Descricao = "Valor total do documento inválido." });
            }
        }

        void ValidarGrupoEstoqueItem(EstoquePreMovimentoItemDto preMovimentoItem, long estoqueId)
        {
            var query = from grupoEstoque in _estoqueGrupoRepository.GetAll()
                        join produto in _produtoRepository.GetAll()
                        on grupoEstoque.GrupoId equals produto.GrupoId
                        where produto.Id == preMovimentoItem.ProdutoId
                          && grupoEstoque.EstoqueId == estoqueId
                          
                         select produto;


            if(query.Count()==0)
            {
                Lista.Add(new ErroDto { CodigoErro = "", Descricao = string.Format( "Grupo do produto {0} não esta relacionado ao estoque informado.", preMovimentoItem.Produto.Descricao) });
            }
                                                      
        }

    }
}
