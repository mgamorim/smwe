﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes
{
    public class MovimentoValidacaoAppService : SWMANAGERAppServiceBase, IMovimentoValidacaoAppService
    {
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;


        private readonly IUnitOfWorkManager _unitOfWorkManager;
        IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        IRepository<LoteValidade, long> _loteValidadeRepository;
        IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        IRepository<Produto, long> _produtoRepository;
        IRepository<ProdutoUnidade, long> _produtoUnidadeRepository;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;

        List<ErroDto> Lista = new List<ErroDto>();

        PreMovimentoValidacaoService preMovimentoValidacaoService;


        public MovimentoValidacaoAppService(IRepository<EstoqueMovimento, long> estoqueMovimentoRepository
            , IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository,


            IUnitOfWorkManager unitOfWorkManager,
                                            IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
                                            IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
                                            IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository,
                                            IRepository<LoteValidade, long> loteValidadeRepository,
                                            IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
                                            IRepository<Produto, long> produtoRepository,
                                            IRepository<ProdutoUnidade, long> produtoUnidadeRepository,
                                            IRepository<EstoqueGrupo, long> estoqueGrupoRepository

            )
        {
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _loteValidadeRepository = loteValidadeRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _produtoRepository = produtoRepository;
            _produtoUnidadeRepository = produtoUnidadeRepository;
            _estoqueGrupoRepository = estoqueGrupoRepository;
        }


        public List<ErroDto> ValidarFornecedoresBaixaVale(string baixasIds)
        {
            List<long> ids = new List<long>();
            List<EstoqueMovimento> movimentos = new List<EstoqueMovimento>();
            List<ErroDto> Lista = new List<ErroDto>();

            if (string.IsNullOrEmpty(baixasIds) || baixasIds.TrimEnd('-').Split('-').Count() == 0)
            {
                ErroDto erroDto = new ErroDto { CodigoErro = "BAX0004" };
                Lista.Add(erroDto);
            }
            else
            {
                baixasIds.TrimEnd('-').Split('-').ToList().ForEach(f => ids.Add(long.Parse(f)));

                foreach (var id in ids)
                {
                    var movimento = _estoqueMovimentoRepository.Get(id);

                    if (movimento != null)
                    {
                        movimentos.Add(movimento);
                    }
                }

                if (movimentos.GroupBy(g => g.SisFornecedorId).Count() > 1)
                {
                    ErroDto erroDto = new ErroDto { CodigoErro = "BAX0002" };
                    Lista.Add(erroDto);
                }
            }
            return Lista;
        }

        public List<ErroDto> ValidarFornecedoresMovimentosItemBaixaConsignados(string baixasItensIds)
        {
            List<long> ids = new List<long>();
            List<EstoqueMovimento> movimentos = new List<EstoqueMovimento>();
            List<ErroDto> Lista = new List<ErroDto>();

            try
            {

                if (string.IsNullOrEmpty(baixasItensIds) || baixasItensIds.TrimEnd('-').Split('-').Count() == 0)
                {
                    ErroDto erroDto = new ErroDto { CodigoErro = "BAX0004" };
                    Lista.Add(erroDto);
                }
                else
                {
                    baixasItensIds.TrimEnd('-').Split('-').ToList().ForEach(f => ids.Add(long.Parse(f)));

                    //var query2 = _estoqueMovimentoItemRepository.GetAll().Where(w=>  w.Id )

                    var query = from mov in _estoqueMovimentoRepository.GetAll()
                                join item in _estoqueMovimentoItemRepository.GetAll()
                                on mov.Id equals item.MovimentoId
                                into movjoin
                                from joinedmov in movjoin.DefaultIfEmpty()
                                where ids.Any(a => a == joinedmov.Id)
                                select mov;

                    movimentos = query.ToList();

                    if (movimentos.GroupBy(g => g.SisFornecedorId).Count() > 1)
                    {
                        ErroDto erroDto = new ErroDto { CodigoErro = "BAX0002" };
                        Lista.Add(erroDto);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Lista;
        }

        public List<ErroDto> ValidarConfirmacaoSolicitacao(EstoquePreMovimentoDto preMovimento)
        {

            preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                       , _estoquePreMovimentoLoteValidadeRepository
                                                                                                       , _estoquePreMovimentoItemRepository
                                                                                                       , _estoqueMovimentoRepository
                                                                                                       , _estoqueMovimentoItemRepository
                                                                                                       , _estoqueMovimentoLoteValidadeRepository
                                                                                                       , _loteValidadeRepository
                                                                                                       , _estoquePreMovimentoRepository
                                                                                                       , _produtoRepository
                                                                                                       , _produtoUnidadeRepository
                                                                                                       , _estoqueGrupoRepository);



            var itens = MontarListarItens(preMovimento.Itens);

            foreach (var item in itens)
            {
                item.EstoqueId = (long)preMovimento.EstoqueId;
                ValidarSeExisteProdutoNoEstoqueSolicitacao(item);

            }

            return Lista;
        }

        public void ValidarQuantidadeItemEstoque()
        {
            PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                       , _estoquePreMovimentoLoteValidadeRepository
                                                                                                       , _estoquePreMovimentoItemRepository
                                                                                                       , _estoqueMovimentoRepository
                                                                                                       , _estoqueMovimentoItemRepository
                                                                                                       , _estoqueMovimentoLoteValidadeRepository
                                                                                                       , _loteValidadeRepository
                                                                                                       , _estoquePreMovimentoRepository
                                                                                                       , _produtoRepository
                                                                                                       , _produtoUnidadeRepository
                                                                                                       , _estoqueGrupoRepository);





        }

        List<EstoquePreMovimentoItemDto> MontarListarItens(string json)
        {
            List<EstoquePreMovimentoItemDto> preMovimentoItens = new List<EstoquePreMovimentoItemDto>();

            var itens = JsonConvert.DeserializeObject<List<EstoquePreMovimentoItemSolicitacaoDto>>(json);

            foreach (var item in itens)
            {
                if (item.QuantidadeAtendida != null && item.QuantidadeAtendida != 0)
                {

                    if (!string.IsNullOrEmpty(item.NumerosSerieJson))
                    {
                        var numerosSerie = JsonConvert.DeserializeObject<List<NumeroSerieGridDto>>(item.NumerosSerieJson);

                        foreach (var numeroSerie in numerosSerie)
                        {
                            var preMovimentoItemDto = new EstoquePreMovimentoItemDto();

                            preMovimentoItemDto.ProdutoId = item.ProdutoId;
                            preMovimentoItemDto.ProdutoUnidadeId = item.ProdutoUnidadeId;
                            preMovimentoItemDto.Quantidade = 1;
                            preMovimentoItemDto.NumeroSerie = numeroSerie.NumeroSerie;
                            preMovimentoItemDto.Produto = new ProdutoDto { Descricao = item.Produto, IsSerie = true };


                            preMovimentoItens.Add(preMovimentoItemDto);
                        }
                    }
                    else
                    {
                        var preMovimentoItemDto = new EstoquePreMovimentoItemDto();

                        preMovimentoItemDto.ProdutoId = item.ProdutoId;
                        preMovimentoItemDto.ProdutoUnidadeId = item.ProdutoUnidadeId;
                        preMovimentoItemDto.Quantidade = (decimal)item.QuantidadeAtendida;
                        preMovimentoItemDto.Produto = new ProdutoDto();
                        preMovimentoItemDto.Produto = new ProdutoDto { Descricao = item.Produto };

                        preMovimentoItens.Add(preMovimentoItemDto);

                        var lotesValidades = JsonConvert.DeserializeObject<List<LoteValidadeGridDto>>(item.LotesValidadesJson, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                        if (lotesValidades != null)
                        {
                            preMovimentoItemDto.Produto.IsLote = true;
                            preMovimentoItemDto.PreMovimentoLotesValidades = new List<EstoquePreMovimentoLoteValidadeDto>();

                            foreach (var itemLoteValidade in lotesValidades)
                            {
                                //var loteValidade = _loteValidadeRepository.GetAll()
                                //                    .Where(w => w.Lote == itemLoteValidade.Lote
                                //                             && w.ProdutoId == preMovimentoItemDto.ProdutoId
                                //                             && w.Validade == itemLoteValidade.Validade
                                //                             && w.ProdutoLaboratorioId == itemLoteValidade.LaboratorioId).FirstOrDefault();

                                var loteValidade = new LoteValidadeDto();
                                loteValidade.Lote = itemLoteValidade.Lote;
                                loteValidade.ProdutoId = preMovimentoItemDto.ProdutoId;
                                loteValidade.Validade = itemLoteValidade.Validade;
                                loteValidade.ProdutoLaboratorioId = itemLoteValidade.LaboratorioId;
                                loteValidade.Id = itemLoteValidade.LoteValidadeId;

                                var preMovimentoLoteValidadeDto = new EstoquePreMovimentoLoteValidadeDto();

                                preMovimentoLoteValidadeDto.LoteValidadeId = loteValidade.Id;
                                preMovimentoLoteValidadeDto.Quantidade = (decimal)itemLoteValidade.Quantidade;
                                preMovimentoLoteValidadeDto.LoteValidade = loteValidade;

                                preMovimentoItemDto.PreMovimentoLotesValidades.Add(preMovimentoLoteValidadeDto);

                            }


                        }

                    }
                }
            }
            return preMovimentoItens;
        }


        public void ValidarSeExisteProdutoNoEstoqueSolicitacao(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            if (preMovimentoItem.Produto.IsValidade || preMovimentoItem.Produto.IsLote)
            {

                foreach (var item in preMovimentoItem.PreMovimentoLotesValidades)
                {
                    //var loteValidade = _loteValidadeRepository.GetAll()
                    //                               .Where(w => w.Lote == item.LoteValidade.Lote
                    //                                        && w.Validade == item.LoteValidade.Validade
                    //                                        && w.ProdutoId == item.LoteValidade.ProdutoId
                    //                                        && w.ProdutoLaboratorioId == item.LoteValidade.ProdutoLaboratorioId).FirstOrDefault();

                    //  if (loteValidade != null)
                    // {
                    //  preMovimentoItem.LoteValidadeId = loteValidade.Id;
                    preMovimentoItem.LoteValidadeId = item.LoteValidadeId;
                        ValidarSeExisteProdutoLoteValidadeNoEstoque(preMovimentoItem);
                    //}
                    //else
                    //{
                    //    Lista.Add(new ErroDto { CodigoErro = "EST0003", Descricao = string.Format("Quantidade do produto {0} insuficiente nesse estoque no Lote/Validade informado.", preMovimentoItem.Produto.Descricao) });
                    //}

                }
            }
            else if(preMovimentoItem.Produto.IsSerie)
            {
                ValidarSeExisteProdutoNumeroSerieNoEstoque(preMovimentoItem);
            }
            else
            {
                ValidarSeExisteProdutoSemLoteValidadeNoEstoque(preMovimentoItem);
            }

        }



        void ValidarSeExisteProdutoSemLoteValidadeNoEstoque(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && joinedmov.ProdutoId == preMovimentoItem.ProdutoId
                        select new { Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade) };



            var queryList = query.ToList();

            var lotesValidade = queryList.Sum(s => s.Quantidade);

            if (lotesValidade < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0003", Descricao = string.Format("Quantidade do produto {0} insuficiente no estoque informado.", preMovimentoItem.Produto.Descricao) });
            }


        }

        void ValidarSeExisteProdutoNumeroSerieNoEstoque(EstoquePreMovimentoItemDto preMovimentoItem)
        {
            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        //into movjoin
                        //from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && item.ProdutoId == preMovimentoItem.ProdutoId
                           && item.NumeroSerie == preMovimentoItem.NumeroSerie
                        select new { Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * item.Quantidade) };



            var queryList = query.ToList();

            var lotesValidade = queryList.Sum(s => s.Quantidade);

            if (lotesValidade < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0003", Descricao = string.Format("Produto {0} com número de série {1} não disponível no estoque informado.", preMovimentoItem.Produto.Descricao, preMovimentoItem.NumeroSerie) });
            }


        }


        void ValidarSeExisteProdutoLoteValidadeNoEstoque(EstoquePreMovimentoItemDto preMovimentoItem)
        {

            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        join iltv in _estoqueMovimentoLoteValidadeRepository.GetAll()
                        on item.Id equals iltv.EstoqueMovimentoItemId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == preMovimentoItem.EstoqueId
                           && item.ProdutoId == preMovimentoItem.ProdutoId
                           && joinedmov.LoteValidadeId == preMovimentoItem.LoteValidadeId
                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade) };


            var queryList = query.ToList();

            var lotesValidade = queryList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade) }).ToList();

            var quantidadeEstoque = lotesValidade.Sum(s => s.Qtd);

            if (quantidadeEstoque < preMovimentoItem.Quantidade)
            {
                Lista.Add(new ErroDto { CodigoErro = "EST0003", Descricao = "Quantidade de produto insuficiente nesse estoque no Lote/Validade informado." });
            }
        }



    }
}
