﻿using Abp.Application.Services;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes
{
    public interface IMovimentoValidacaoAppService : IApplicationService
    {
        List<ErroDto> ValidarFornecedoresBaixaVale(string baixasIds);
        List<ErroDto> ValidarFornecedoresMovimentosItemBaixaConsignados(string baixasItensIds);
    }
}
