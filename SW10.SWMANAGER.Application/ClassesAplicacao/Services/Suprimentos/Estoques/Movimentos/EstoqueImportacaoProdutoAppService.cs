﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using NFe.Classes;
using NFe.Classes.Informacoes.Detalhe;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class EstoqueImportacaoProdutoAppService : SWMANAGERAppServiceBase, IEstoqueImportacaoProdutoAppService
    {
        private readonly IRepository<EstoqueImportacaoProduto, long> _estoqueImportacaoProdutoRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ProdutoUnidade, long> _produtoUnidadeRepository;

        string[] lotes = { "lote:", "Lote:", "Lt:", "L:", "Lote/Serie:" };
        string[] validades = { "validade:", "Validade:", "Vl:", "V:" };
        string[] series = { "Lote/Serie", "Serie", "serie" };


        public EstoqueImportacaoProdutoAppService(IRepository<EstoqueImportacaoProduto, long> estoqueImportacaoProdutoRepository
                                                  , IUnitOfWorkManager unitOfWorkManager
                                                  , IRepository<ProdutoUnidade, long> produtoUnidadeRepository)
        {
            _estoqueImportacaoProdutoRepository = estoqueImportacaoProdutoRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _produtoUnidadeRepository = produtoUnidadeRepository;
        }


        public List<EstoqueImportacaoProdutoDto> ObterListaImportacaoProduto(nfeProc nf)
        {
            List<EstoqueImportacaoProdutoDto> listImportacaoProdutos = new List<EstoqueImportacaoProdutoDto>();

            foreach (var item in nf.NFe.infNFe.det)
            {
                var produtoImportado = _estoqueImportacaoProdutoRepository.GetAll()
                                                                             .Where(w => w.CodigoProdutoNota == item.prod.cProd
                                                                                     && w.CNPJNota == nf.NFe.infNFe.emit.CNPJ)
                                                                             .FirstOrDefault();

                var importacaoProduto = new EstoqueImportacaoProdutoDto();
                if (produtoImportado != null)
                {
                    importacaoProduto.FornecedorId = produtoImportado.FornecedorId;
                    importacaoProduto.ProdutoId = produtoImportado.ProdutoId;
                    importacaoProduto.Fator = produtoImportado.Fator;
                    importacaoProduto.UnidadeId = produtoImportado.UnidadeId;
                }
                else
                {
                    importacaoProduto.Fator = 1;
                }

                importacaoProduto.CodigoProdutoNota = item.prod.cProd;
                importacaoProduto.DescricaoProdutoNota = item.prod.xProd;
                importacaoProduto.InformacaoAdicionalNota = item.infAdProd;
                importacaoProduto.UnidadeNota = item.prod.uCom;
                importacaoProduto.Quantidade = (decimal)importacaoProduto.Fator * item.prod.qCom;
                importacaoProduto.CustoUnitario = item.prod.vUnCom / (decimal)importacaoProduto.Fator;

                var informacaoAdicional = item.infAdProd;

                if (!string.IsNullOrEmpty(informacaoAdicional))
                {
                    importacaoProduto.Lote = ObterInformacao(informacaoAdicional, lotes);
                    importacaoProduto.Validade = ObterValidade(informacaoAdicional);
                    importacaoProduto.Serie = ObterInformacao(informacaoAdicional, series);
                }
                else
                {
                    importacaoProduto.Lote = ObterInformacao(item.prod.xProd, lotes);
                    importacaoProduto.Validade = ObterValidade(item.prod.xProd);
                    importacaoProduto.Serie = ObterInformacao(item.prod.xProd, series);
                }

                listImportacaoProdutos.Add(importacaoProduto);
            }

            return listImportacaoProdutos;
        }


        public DefaultReturn<Object> RelacionarProdutos(List<EstoqueImportacaoProdutoDto> importacaoProdutos, long fornecedorId, string CNPJNota)
        {
            var _retornoPadrao = new DefaultReturn<Object>();
            _retornoPadrao.Errors = new List<ErroDto>();

            try
            {

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    foreach (var item in importacaoProdutos.Where(w => w.ProdutoId != null))
                    {
                        var importacaoProduto = _estoqueImportacaoProdutoRepository.GetAll()
                                                                                      .Where(w => w.CodigoProdutoNota == item.CodigoProdutoNota
                                                                                               && w.CNPJNota == CNPJNota)
                                                                                      .FirstOrDefault();

                        if (importacaoProduto != null)
                        {
                            importacaoProduto.ProdutoId = (long)item.ProdutoId;
                            importacaoProduto.UnidadeId = item.UnidadeId;
                            importacaoProduto.Fator = (decimal)item.Fator;

                            _estoqueImportacaoProdutoRepository.Update(importacaoProduto);
                        }
                        else
                        {
                            importacaoProduto = new EstoqueImportacaoProduto();

                            importacaoProduto.CodigoProdutoNota = item.CodigoProdutoNota;
                            importacaoProduto.ProdutoId = (long)item.ProdutoId;
                            importacaoProduto.FornecedorId = fornecedorId;
                            importacaoProduto.CNPJNota = CNPJNota;
                            importacaoProduto.UnidadeId = item.UnidadeId;
                            importacaoProduto.Fator = (decimal)item.Fator;

                            _estoqueImportacaoProdutoRepository.Insert(importacaoProduto);
                        }



                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();

                        var produtoUnidade = _produtoUnidadeRepository.GetAll()
                                                                      .Where(w => w.ProdutoId == item.ProdutoId
                                                                               && w.UnidadeId == item.UnidadeId)
                                                                      .FirstOrDefault();

                        if(produtoUnidade == null)
                        {
                            produtoUnidade = new ProdutoUnidade();
                            produtoUnidade.ProdutoId = (long)item.ProdutoId;
                            produtoUnidade.UnidadeId = item.UnidadeId;
                            produtoUnidade.UnidadeTipoId = 3; //Compras
                            produtoUnidade.IsAtivo = true;

                            _produtoUnidadeRepository.Insert(produtoUnidade);

                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();

                        }



                    }

                 
                    unitOfWork.Dispose();

                }
            }
            catch(Exception ex)
            {

            }
            return _retornoPadrao;
        }

        public string ObterInformacao(string informacaoAdicional, string[] identificador)
        {
            string retorno="";

            if (!string.IsNullOrEmpty(informacaoAdicional))
            {
                var listaInformacoes = informacaoAdicional.Split(' ');

                for (int i = 0; i < listaInformacoes.Count(); i++)
                {
                    var item = listaInformacoes[i];
                    if (identificador.Contains(item))
                    {
                        retorno = listaInformacoes[i + 1];
                    }
                }
            }



            //string identificador;

            //foreach (var item in lotes)
            //{
            //    var index = informacaoAdicional.IndexOf(string.Concat( item, ": "));
            //    if(index != -1)
            //    {
            //        var IndexLote = 
            //    }


            //}
                
                
                
                

            return retorno;
        }

        public DateTime ObterValidade(string informacaoAdicional)
        {
            DateTime validade=new DateTime();
            var strValidade = ObterInformacao(informacaoAdicional, validades);
            if (!string.IsNullOrEmpty(strValidade))
            {
                var list = strValidade.Split('/');

                if (list.Count() == 3)
                {
                    if (list[2].Count() == 2)
                    {
                        list[2] = "20" + list[2];
                    }
                    validade = new DateTime(Convert.ToInt16(list[2]), Convert.ToInt16(list[1]), Convert.ToInt16(list[0]));
                }
                else
                {
                    list[1] = "20" + list[1];
                    int dia = DateTime.DaysInMonth(Convert.ToInt16(list[1]), Convert.ToInt16(list[0]));
                    int mes = Convert.ToInt16(list[0]);
                    int ano = Convert.ToInt16(list[1]);
                    validade = new DateTime(ano, mes, dia);
                }

            }

            return validade;
        }
    }
}
