﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
//using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.UI;
using Abp.Domain.Uow;



using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Movimentos.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes;
using System.Diagnostics;
using SW10.SWMANAGER.Authorization.Users;
using Abp.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using Abp.AutoMapper;
using System.Web.Helpers;
using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.Sessions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using Abp.EntityFramework.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class ValidacaoException : Exception
    {
        public ValidacaoException(string mensagem) : base(mensagem) { }
    }

    public class EstoquePreMovimentoAppService : SWMANAGERAppServiceBase, IEstoquePreMovimentoAppService
    {
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        // private readonly IRepository<EstoqueTipoMovimento, long> _estoqueTipoMovimentoRepository;
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IListarPreMovimentosExcelExporter _listarPreMovimentosExcelExporter;
        private DefaultReturn<EstoquePreMovimentoDto> _retornoPadrao;
        private readonly IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<User, long> _usuarioRepository;
        private readonly IRepository<EstoqueLaboratorio, long> _produtoLaboratorioRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        private readonly IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        private readonly IRepository<LoteValidade, long> _loteValidadeRepository;
        private readonly IRepository<Produto, long> _produtoRepository;
        private readonly IRepository<ProdutoUnidade, long> _produtoUnidadeRepository;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IRepository<EstoqueTransferenciaProduto, long> _estoqueTransferenciaProdutoRepository;
        private readonly IRepository<EstoqueTransferenciaProdutoItem, long> _estoqueTransferenciaProdutoItemRepository;
        private readonly IEstoquePreMovimentoItemAppService _estoquePreMovimentoItemAppService;
        private readonly IRepository<Unidade, long> _unidadeRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IProdutoSaldoAppService _produtoSaldoAppService;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly IRepository<EstoqueSolicitacaoItem, long> _estoqueSolcitacaiItemRepository;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepository;
        private ISessionAppService _sessionAppService;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;
        private readonly IEstoqueMovimentoAppService _estoqueMovimentoAppService;
        private readonly IFaturamentoContaItemAppService _faturamentoContaItemAppService;
        private readonly IContasPagarAppService _contasPagarAppService;
        private readonly IRepository<SisFornecedor, long> _fornecedorRepository;

        public EstoquePreMovimentoAppService(
         IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
         IUnitOfWorkManager unitOfWorkManager,
         //   IRepository<EstoqueTipoMovimento, long> estoqueTipoMovimentoRepository,
         IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
          IListarPreMovimentosExcelExporter listarPreMovimentosExcelExporter,
          IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
          IRepository<EstoqueMovimento, long> estoqueMovimentoRepository,
          IRepository<User, long> usuarioRepository,
          IRepository<EstoqueLaboratorio, long> produtoLaboratorioRepository,
          IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository,
          IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository,
          IRepository<LoteValidade, long> loteValidadeRepository,
          IRepository<Produto, long> produtoRepository,
          IRepository<ProdutoUnidade, long> produtoUnidadeRepository,
          IUltimoIdAppService ultimoIdAppService,
          IRepository<EstoqueTransferenciaProduto, long> estoqueTransferenciaProdutoRepository,
          IRepository<EstoqueTransferenciaProdutoItem, long> estoqueTransferenciaProdutoItemRepository,
          IEstoquePreMovimentoItemAppService estoquePreMovimentoItemAppService,
          IRepository<Unidade, long> unidadeRepository,
          IRepository<Atendimento, long> atendimentoRepository,
          IProdutoSaldoAppService produtoSaldoAppService,
          IUnidadeAppService unidadeAppService,
          IRepository<EstoqueSolicitacaoItem, long> estoqueSolcitacaiItemRepository,
          IRepository<ProdutoSaldo, long> produtoSaldoRepository,
          ISessionAppService sessionAppService,
          IRepository<EstoqueGrupo, long> estoqueGrupoRepository,
          IEstoqueMovimentoAppService estoqueMovimentoAppService,
          IRepository<FaturamentoItem, long> faturamentoItemRepository,
          IFaturamentoContaItemAppService faturamentoContaItemAppService,
          IContasPagarAppService contasPagarAppService,
          IRepository<SisFornecedor, long> fornecedorRepository
         )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            //  _estoqueTipoMovimentoRepository = estoqueTipoMovimentoRepository;
            _listarPreMovimentosExcelExporter = listarPreMovimentosExcelExporter;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _usuarioRepository = usuarioRepository;
            _produtoLaboratorioRepository = produtoLaboratorioRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _loteValidadeRepository = loteValidadeRepository;
            _produtoRepository = produtoRepository;
            _produtoUnidadeRepository = produtoUnidadeRepository;
            _ultimoIdAppService = ultimoIdAppService;
            _estoqueTransferenciaProdutoRepository = estoqueTransferenciaProdutoRepository;
            _estoqueTransferenciaProdutoItemRepository = estoqueTransferenciaProdutoItemRepository;
            _estoquePreMovimentoItemAppService = estoquePreMovimentoItemAppService;
            _unidadeRepository = unidadeRepository;
            _atendimentoRepository = atendimentoRepository;
            _produtoSaldoAppService = produtoSaldoAppService;
            _unidadeAppService = unidadeAppService;
            _estoqueSolcitacaiItemRepository = estoqueSolcitacaiItemRepository;
            _produtoSaldoRepository = produtoSaldoRepository;
            _sessionAppService = sessionAppService;
            _estoqueGrupoRepository = estoqueGrupoRepository;
            _estoqueMovimentoAppService = estoqueMovimentoAppService;
            _faturamentoContaItemAppService = faturamentoContaItemAppService;
            _contasPagarAppService = contasPagarAppService;
            _fornecedorRepository = fornecedorRepository;
        }

        [UnitOfWork]
        public DefaultReturn<EstoquePreMovimentoDto> CriarOuEditarSaida(EstoquePreMovimentoDto input)
        {
            input.IsEntrada = false;
            input.Movimento = input.Emissao;
            input.EstTipoOperacaoId = 3;
            return CriarOuEditar(input);
        }

        public DefaultReturn<EstoquePreMovimentoDto> CriarOuEditar(EstoquePreMovimentoDto input)
        {
            _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();



            try
            {

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {





                    var query = _estoquePreMovimentoItemRepository
                  .GetAll()
                 .Where(m => m.PreMovimentoId == input.Id);

                    var preMovimentoItens = query.ToList();
                    var _preMovimentoItens = EstoquePreMovimentoItemAppService.MapPreMovimentoItem(preMovimentoItens);//.MapTo<List<EstoquePreMovimentoItemDto>>();

                    PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                               , _estoquePreMovimentoLoteValidadeRepository
                                                                                                               , _estoquePreMovimentoItemRepository
                                                                                                               , _estoqueMovimentoRepository
                                                                                                               , _estoqueMovimentoItemRepository
                                                                                                               , _estoqueMovimentoLoteValidadeRepository
                                                                                                               , _loteValidadeRepository
                                                                                                               , _estoquePreMovimentoRepository
                                                                                                               , _produtoRepository
                                                                                                               , _produtoUnidadeRepository
                                                                                                               , _estoqueGrupoRepository);

                    _retornoPadrao.Errors = preMovimentoValidacaoService.Validar(input, _preMovimentoItens);



                    var erroESt0003 = new List<ErroDto>();
                    erroESt0003 = _retornoPadrao.Errors.Where(w => w.CodigoErro == "EST0003").ToList();
                    if (erroESt0003.Count() > 0)
                    {
                        _retornoPadrao.Warnings = new List<ErroDto>();
                        _retornoPadrao.Warnings.Add(new ErroDto { Descricao = "Existem itens não disponiveis no estoque.  A validação será realizada na confirmação da saída." });

                        foreach (var item in erroESt0003)
                        {
                            _retornoPadrao.Errors.Remove(item);
                        }


                    }



                    if (_retornoPadrao.Errors.Count() == 0)
                    {
                        input.PossuiLoteValidade = preMovimentoValidacaoService.ExisteLoteValidadePendente(input);

                        if (input.PossuiLoteValidade)
                        {
                            input.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.NaoAutorizado;
                        }
                        else
                        {
                            input.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Autorizado;
                        }


                        EstoquePreMovimento preMovimento = MapPreMovimento(input);//.MapTo<EstoquePreMovimento>();

                        preMovimento.GrupoOperacaoId = (long)EnumGrupoOperacao.Movimentos;

                        if (input.Id.Equals(0))
                        {
                            //using (var unitOfWork = _unitOfWorkManager.Begin())
                            //{
                            input.Id = AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertAndGetIdAsync(preMovimento));


                            //    unitOfWork.Complete();
                            //    _unitOfWorkManager.Current.SaveChanges();
                            //    unitOfWork.Dispose();
                            //}

                        }
                        else
                        {
                            //using (var unitOfWork = _unitOfWorkManager.Begin())
                            //{
                            var edit = _estoquePreMovimentoRepository.Get(input.Id);
                            if (edit != null)
                            {
                                edit.OrdemId = input.OrdemId;
                                edit.Quantidade = input.Quantidade;
                                edit.Serie = input.Serie;
                                edit.EstTipoMovimentoId = input.TipoDocumentoId;
                                edit.TipoFreteId = input.TipoFreteId;
                                edit.TotalDocumento = input.TotalDocumento;
                                edit.TotalProduto = input.TotalProduto;
                                edit.ValorFrete = input.ValorFrete;
                                edit.ValorICMS = input.ValorICMS;
                                edit.EmpresaId = input.EmpresaId;
                                edit.SisFornecedorId = input.FornecedorId;
                                edit.Documento = input.Documento;
                                edit.Serie = input.Serie;
                                edit.Movimento = input.Movimento;
                                edit.EstoqueId = input.EstoqueId;
                                edit.CentroCustoId = input.CentroCustoId;
                                edit.OrdemId = input.OrdemId;
                                edit.Emissao = input.Emissao;
                                edit.CFOPId = input.CFOPId;
                                edit.ICMSPer = input.ICMSPer;
                                edit.DescontoPer = input.DescontoPer;
                                edit.AcrescimoDecrescimo = input.AcrescimoDecrescimo;
                                edit.TipoFreteId = input.TipoFreteId;
                                edit.Frete = input.Frete;
                                edit.FretePer = input.FretePer;
                                edit.ValorFrete = input.ValorFrete;
                                edit.Frete_SisFornecedorId = input.Frete_FornecedorId;

                                edit.MedicoSolcitanteId = input.MedicoSolcitanteId;
                                edit.PacienteId = input.PacienteId;
                                edit.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                                edit.AtendimentoId = input.AtendimentoId;
                                edit.Observacao = input.Observacao;

                                edit.Consiginado = input.Consiginado;
                                edit.Contabiliza = input.Contabiliza;
                                edit.AplicacaoDireta = input.AplicacaoDireta;

                                edit.PreMovimentoEstadoId = input.PreMovimentoEstadoId;
                                edit.EstTipoMovimentoId = input.EstTipoMovimentoId;

                                edit.TotalProduto = input.TotalProduto;
                                edit.DescontoPer = input.DescontoPer;

                                AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.UpdateAsync(edit));//.MapTo<EstoquePreMovimentoDto>();




                                //    unitOfWork.Complete();
                                //    _unitOfWorkManager.Current.SaveChanges();
                                //    unitOfWork.Dispose();

                                //}
                            }
                        }

                        input.PossuiLoteValidade = preMovimentoValidacaoService.ExisteLoteValidadePendente(input);
                        input.PermiteConfirmacaoEntrada = !input.PossuiLoteValidade;

                        if (input.IsEntrada)
                        {
                            var retornoContasPagar = GerarContasPagar(input);
                            _retornoPadrao.Errors.AddRange(retornoContasPagar.Errors);
                        }


                        if (_retornoPadrao.Errors.Count == 0)
                        {
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Complete();
                            unitOfWork.Dispose();

                        }
                        else
                        {
                            ((IDisposable)CurrentUnitOfWork).Dispose();

                        }

                    }



                    _retornoPadrao.ReturnObject = input;
                }
            }

            catch (Exception ex)
            {
                if (ex.TargetSite.ToString() != "Void Dispose()")
                {

                    var erro = new ErroDto();

                    if (ex.InnerException != null)
                    {
                        var inner = ex.InnerException;
                        erro = new ErroDto();
                        erro.CodigoErro = inner.HResult.ToString();
                        erro.Descricao = inner.Message.ToString();
                        _retornoPadrao.Errors.Add(erro);
                    }
                    else
                    {
                        erro.CodigoErro = ex.HResult.ToString();
                        erro.Descricao = ex.Message.ToString();
                        _retornoPadrao.Errors.Add(erro);
                    }
                }
            }

            return _retornoPadrao;
        }



        public async Task<bool> PermiteConfirmarEntrada(EstoquePreMovimentoDto preMovimento)
        {

            PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                       , _estoquePreMovimentoLoteValidadeRepository
                                                                                                       , _estoquePreMovimentoItemRepository
                                                                                                       , _estoqueMovimentoRepository
                                                                                                       , _estoqueMovimentoItemRepository
                                                                                                       , _estoqueMovimentoLoteValidadeRepository
                                                                                                       , _loteValidadeRepository
                                                                                                       , _estoquePreMovimentoRepository
                                                                                                       , _produtoRepository
                                                                                                       , _produtoUnidadeRepository
                                                                                                       , _estoqueGrupoRepository);

            var existeLoteValidadePendente = preMovimentoValidacaoService.ExisteLoteValidadePendente(preMovimento);

            return !existeLoteValidadePendente;
        }

        public async Task Excluir(EstoquePreMovimentoDto input)
        {
            try
            {

                var itensPreMovimento = _estoquePreMovimentoItemRepository.GetAll()
                                        .Where(w => w.PreMovimentoId == input.Id).ToList();

                foreach (var item in itensPreMovimento)
                {
                    await _estoquePreMovimentoItemAppService.Excluir(item.Id);
                }

                await _estoquePreMovimentoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<MovimentoIndexDto>> Listar(ListarEstoquePreMovimentoInput input)
        {
            var contarEstoquePreMovimentos = 0;
            List<MovimentoIndexDto> EstoquePreMovimentos;
            List<MovimentoIndexDto> EstoquePreMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {
                input.PeridoDe = ((DateTime)input.PeridoDe).Date;
                input.PeridoAte = ((DateTime)input.PeridoAte).Date.AddDays(1).AddSeconds(-1);

                var query = _estoquePreMovimentoRepository
                    .GetAll()
                    .Where(m => ((input.Filtro == "" || input.Filtro == null)
                     ||
                     m.Documento.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                   || m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.Emissao.ToString().Contains(input.Filtro.ToUpper())
                   || m.SisFornecedor.SisPessoa.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.SisFornecedor.SisPessoa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.TotalDocumento.ToString().Contains(input.Filtro.ToUpper())
                   || m.EstTipoMovimento.Descricao.Contains(input.Filtro.ToUpper())
                   )
                   && (input.FornecedorId == null || m.SisFornecedor.Id == input.FornecedorId)
                   && (input.PeridoDe == null || m.Emissao >= input.PeridoDe)
                   && (input.PeridoAte == null || m.Emissao <= input.PeridoAte)
                   && (input.TipoMovimentoId == null || m.EstTipoMovimento.Id == input.TipoMovimentoId)
                   && m.IsEntrada == input.IsEntrada
                   && (m.GrupoOperacaoId == null || m.GrupoOperacaoId == (long)EnumGrupoOperacao.Movimentos)
                   && (m.EstTipoOperacaoId == 1 || m.EstTipoOperacaoId == 3)

                    ).Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,
                        Fornecedor = (s.SisFornecedor != null) ? s.SisFornecedor.SisPessoa.FisicaJuridica == "F" ? s.SisFornecedor.SisPessoa.NomeCompleto : s.SisFornecedor.SisPessoa.NomeFantasia : String.Empty
                                                        ,
                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : String.Empty
                                                        ,
                        Valor = s.TotalDocumento
                        ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId,

                        Estoque = (s.Estoque != null) ? s.Estoque.Descricao : String.Empty,

                        TipoMovimento = (s.EstTipoMovimento != null) ? s.EstTipoMovimento.Descricao : String.Empty
                    });

                contarEstoquePreMovimentos = await query.CountAsync();

                EstoquePreMovimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //EstoquePreMovimentoDtos = EstoquePreMovimentos
                //    .MapTo<List<EstoquePreMovimentoDto>>();


                foreach (var preMovimento in EstoquePreMovimentos)
                {
                    if (preMovimento.UsuarioId != null)
                    {
                        var usuario = _usuarioRepository.Get((long)preMovimento.UsuarioId);
                        if (usuario != null)
                        {
                            preMovimento.Usuario = usuario.FullName;
                        }
                    }

                }




                return new PagedResultDto<MovimentoIndexDto>(
                    contarEstoquePreMovimentos,
                    EstoquePreMovimentos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<EstoquePreMovimentoDto>> ListarTodos()
        {
            var contarEntradas = 0;
            List<EstoquePreMovimento> EstoquePreMovimentos;
            List<EstoquePreMovimentoDto> EstoquePreMovimentoDtos = new List<EstoquePreMovimentoDto>();
            try
            {
                var query = _estoquePreMovimentoRepository
                    .GetAll();

                contarEntradas = await query
                    .CountAsync();

                EstoquePreMovimentos = await query
                    .AsNoTracking()
                    .ToListAsync();

                EstoquePreMovimentoDtos = MapPreMovimento(EstoquePreMovimentos);
                //.MapTo<List<EstoquePreMovimentoDto>>();






                return new ListResultDto<EstoquePreMovimentoDto> { Items = EstoquePreMovimentoDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _estoquePreMovimentoRepository
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Documento.ToString().ToUpper().Contains(input.ToUpper())
                        )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Documento.ToString() })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<EstoquePreMovimentoDto> Obter(long id)
        {
            try
            {

                var query = _estoquePreMovimentoRepository
                 .GetAll()
                 .Include(i => i.SisFornecedor)
                 .Include(i => i.CentroCusto)
                 .Include(i => i.EstTipoMovimento)
                 .Include(i => i.Atendimento)
                 .Include(i => i.MedicoSolicitante)
                 .Include(i => i.MedicoSolicitante.SisPessoa)
                 .Include(i => i.Atendimento.Paciente)
                 .Include(i => i.Atendimento.Paciente.SisPessoa)
                 .Include(i => i.SisFornecedor.SisPessoa)
                 .Include(i => i.Frete_SisForncedor)
                 .Include(i => i.Frete_SisForncedor.SisPessoa)
                 .Include(i => i.Empresa)
                 .Include(i => i.Estoque)
                 .Where(m => m.Id == id);

                var result = await query.FirstOrDefaultAsync();
                var preMovimento = MapPreMovimento(result);  //.MapTo<EstoquePreMovimentoDto>();

                PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                      , _estoquePreMovimentoLoteValidadeRepository
                                                                                                      , _estoquePreMovimentoItemRepository
                                                                                                      , _estoqueMovimentoRepository
                                                                                                      , _estoqueMovimentoItemRepository
                                                                                                     , _estoqueMovimentoLoteValidadeRepository
                                                                                                     , _loteValidadeRepository
                                                                                                     , _estoquePreMovimentoRepository
                                                                                                     , _produtoRepository
                                                                                                     , _produtoUnidadeRepository
                                                                                                     , _estoqueGrupoRepository);

                preMovimento.PossuiLoteValidade = preMovimentoValidacaoService.ExisteLoteValidadePendente(preMovimento);

                return preMovimento;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<PagedResultDto<MovimentoItemDto>> ListarItensSaida(ListarEstoquePreMovimentoInput input)
        {
            var itens = await ListarItens(input);

            foreach (var item in itens.Items)
            {
                var preMovimentoItemLoteValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                    .Include(i => i.LoteValidade)
                    .Where(w => w.EstoquePreMovimentoItemId == item.Id).FirstOrDefault();

                if (preMovimentoItemLoteValidade != null && preMovimentoItemLoteValidade.LoteValidade != null)
                {
                    item.Lote = preMovimentoItemLoteValidade.LoteValidade.Lote;
                    item.Validade = preMovimentoItemLoteValidade.LoteValidade.Validade;

                    if (preMovimentoItemLoteValidade.LoteValidade.EstLaboratorioId != null)
                    {
                        item.Laboratorio = _produtoLaboratorioRepository.Get((long)preMovimentoItemLoteValidade.LoteValidade.EstLaboratorioId).Descricao;
                    }
                }
            }


            return itens;
        }

        public async Task<PagedResultDto<MovimentoItemDto>> ListarItens(ListarEstoquePreMovimentoInput input)
        {
            var contarPreMovimentos = 0;
            List<MovimentoItemDto> preMovimentoItens = null;
            try
            {
                if (!string.IsNullOrEmpty(input.Filtro) && input.Filtro != "0")
                {
                    var id = Convert.ToInt64(input.Filtro);

                    var query = _estoquePreMovimentoItemRepository
                        .GetAll()
                       .Where(m => m.PreMovimentoId == id)
                       .Select(s => new MovimentoItemDto
                       {
                           Id = s.Id,
                           Produto = s.Produto.Descricao
                       ,
                           Quantidade = s.Quantidade / s.ProdutoUnidade.Fator
                       ,
                           IsValidade = s.Produto.IsValidade
                       ,
                           ProdutoId = s.ProdutoId
                       ,
                           Unidade = s.ProdutoUnidade.Descricao
                       ,
                           CustoUnitario = s.CustoUnitario
                       ,
                           CustoTotal = s.CustoUnitario * (s.Quantidade / s.ProdutoUnidade.Fator)
                       ,
                           IsLote = s.Produto.IsLote
                           ,
                           NumeroSerie = s.NumeroSerie
                       });
                    //var query = await Obter(Convert.ToInt64(input.Filtro));
                    contarPreMovimentos = await query.CountAsync();
                    preMovimentoItens = await query.ToListAsync();
                    //PreMovimentoItens = preMovimentoItens.MapTo<List<EstoquePreMovimentoItemDto>>();
                }
                return new PagedResultDto<MovimentoItemDto>(
                    contarPreMovimentos,
                    preMovimentoItens
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public PagedResultDto<EstoquePreMovimentoItemSolicitacaoDto> ListarItensJson(string json)
        {
            var contarPreMovimentos = 0;
            List<EstoquePreMovimentoItemSolicitacaoDto> preMovimentoItens = JsonConvert.DeserializeObject<List<EstoquePreMovimentoItemSolicitacaoDto>>(json);
            try
            {
                long idGrid = 0;
                foreach (var item in preMovimentoItens)
                {
                    if (item.ProdutoId != 0)
                    {
                        var produto = _produtoRepository.Get(item.ProdutoId);
                        if (produto != null)
                        {
                            item.Produto = produto.Descricao;
                            item.CodigoProduto = produto.Codigo;
                        }
                    }

                    if (item.ProdutoUnidadeId != null)
                    {
                        var unidade = _unidadeRepository.Get((long)item.ProdutoUnidadeId);
                        if (unidade != null)
                        {
                            item.ProdutoUnidade = unidade.Descricao;
                            item.Quantidade = item.Quantidade / unidade.Fator;
                        }
                    }

                    //  item.IdGrid = ++idGrid;
                }



                return new PagedResultDto<EstoquePreMovimentoItemSolicitacaoDto>(
                    contarPreMovimentos,
                    preMovimentoItens
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public EstoquePreMovimentoDto CriarGetIdSaida(EstoquePreMovimentoDto input)
        {
            input.IsEntrada = false;
            input.Movimento = input.Emissao;
            input.EstTipoOperacaoId = 3;

            if (string.IsNullOrEmpty(input.Documento))
            {
                input.Documento = _ultimoIdAppService.ObterProximoCodigo("SaidaProduto").Result;
            }
            return CriarGetId(input);
        }

        public EstoquePreMovimentoDto CriarGetIdEntrada(EstoquePreMovimentoDto input)
        {
            input.IsEntrada = true;
            input.EstTipoOperacaoId = 1;
            return CriarGetId(input);
        }

        private EstoquePreMovimentoDto CriarGetId(EstoquePreMovimentoDto input)
        {
            try
            {
                if (input.PacienteId == 0)
                {
                    input.PacienteId = null;
                }

                EstoquePreMovimentoDto estoquePreMovimentoDto;
                var preMovimento = MapPreMovimento(input);

                preMovimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.NaoAutorizado;
                preMovimento.GrupoOperacaoId = (long)EnumGrupoOperacao.Movimentos;


                if (input.Id.Equals(0))
                {
                    estoquePreMovimentoDto = new EstoquePreMovimentoDto { Id = AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertAndGetIdAsync(preMovimento)) };
                }
                else
                {
                    estoquePreMovimentoDto = MapPreMovimento(AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.UpdateAsync(preMovimento))); //.MapTo<EstoquePreMovimentoDto>();
                }


                estoquePreMovimentoDto.PossuiLoteValidade = _estoquePreMovimentoItemRepository.GetAll().Where(w => w.PreMovimentoId == preMovimento.Id
                                                                                                               && w.Produto.IsValidade).Count() > 0;

                estoquePreMovimentoDto.Documento = input.Documento;
                return estoquePreMovimentoDto;
            }

            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarEstoquePreMovimentoInput input)
        {
            try
            {
                var result = await Listar(input);
                var preMovimentos = result.Items;
                return _listarPreMovimentosExcelExporter.ExportToFile(preMovimentos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public static EstoquePreMovimentoDto MapPreMovimento(EstoquePreMovimento preMovimento)
        {
            var preMovimentoDto = new EstoquePreMovimentoDto();

            preMovimentoDto.AcrescimoDecrescimo = preMovimento.AcrescimoDecrescimo;
            preMovimentoDto.AplicacaoDireta = preMovimento.AplicacaoDireta;
            preMovimentoDto.AtendimentoId = preMovimento.AtendimentoId;
            preMovimentoDto.CentroCustoId = preMovimento.CentroCustoId;
            preMovimentoDto.Consiginado = preMovimento.Consiginado;
            preMovimentoDto.Contabiliza = preMovimento.Contabiliza;
            preMovimentoDto.CreationTime = preMovimento.CreationTime;
            preMovimentoDto.CreatorUserId = preMovimento.CreatorUserId;
            preMovimentoDto.DeleterUserId = preMovimento.DeleterUserId;
            preMovimentoDto.DeletionTime = preMovimento.DeletionTime;
            preMovimentoDto.DescontoPer = preMovimento.DescontoPer;
            preMovimentoDto.Documento = preMovimento.Documento;
            preMovimentoDto.Emissao = preMovimento.Emissao;
            preMovimentoDto.EmpresaId = preMovimento.EmpresaId;
            preMovimentoDto.EntragaProduto = preMovimento.EntragaProduto;
            preMovimentoDto.EstoqueId = preMovimento.EstoqueId;
            preMovimentoDto.FornecedorId = preMovimento.SisFornecedorId;
            preMovimentoDto.Paciente = preMovimento.Paciente.MapTo<PacienteDto>();

            if (preMovimento.SisFornecedor != null)
            {
                preMovimentoDto.Fornecedor = new SisFornecedorDto
                {
                    Id = preMovimento.SisFornecedor.Id
                  ,
                    Codigo = preMovimento.SisFornecedor.SisPessoa.Codigo
                  ,
                    Descricao = preMovimento.SisFornecedor.SisPessoa.FisicaJuridica == "F" ? preMovimento.SisFornecedor.SisPessoa.NomeCompleto : preMovimento.SisFornecedor.SisPessoa.NomeFantasia
                };
            }

            preMovimentoDto.Frete = preMovimento.Frete;
            preMovimentoDto.FretePer = preMovimento.FretePer;
            preMovimentoDto.Frete_FornecedorId = preMovimento.Frete_SisFornecedorId;

            if (preMovimento.Frete_SisForncedor != null)
            {
                preMovimentoDto.Frete_Fornecedor = new SisFornecedorDto
                {
                    Id = preMovimento.Frete_SisForncedor.Id
                 ,
                    Codigo = preMovimento.Frete_SisForncedor.SisPessoa.Codigo
                 ,
                    Descricao = preMovimento.Frete_SisForncedor.SisPessoa.FisicaJuridica == "F" ? preMovimento.Frete_SisForncedor.SisPessoa.NomeCompleto : preMovimento.Frete_SisForncedor.SisPessoa.NomeFantasia
                };
            }

            preMovimentoDto.ICMSPer = preMovimento.ICMSPer;
            preMovimentoDto.Id = preMovimento.Id;
            preMovimentoDto.InclusoNota = preMovimento.InclusoNota;
            preMovimentoDto.IsEntrada = preMovimento.IsEntrada;
            preMovimentoDto.MedicoSolcitanteId = preMovimento.MedicoSolcitanteId;
            preMovimentoDto.Movimento = preMovimento.Movimento;
            preMovimentoDto.Observacao = preMovimento.Observacao;
            preMovimentoDto.OrdemId = preMovimento.OrdemId;
            preMovimentoDto.PacienteId = preMovimento.PacienteId;
            preMovimentoDto.PreMovimentoEstadoId = preMovimento.PreMovimentoEstadoId;
            preMovimentoDto.Quantidade = preMovimento.Quantidade;
            preMovimentoDto.Serie = preMovimento.Serie;
            preMovimentoDto.EstTipoOperacaoId = preMovimento.EstTipoOperacaoId;
            preMovimentoDto.TipoFreteId = preMovimento.TipoFreteId;
            preMovimentoDto.EstTipoMovimentoId = preMovimento.EstTipoMovimentoId;
            preMovimentoDto.TotalDocumento = preMovimento.TotalDocumento;
            preMovimentoDto.TotalProduto = preMovimento.TotalProduto;
            preMovimentoDto.UnidadeOrganizacionalId = preMovimento.UnidadeOrganizacionalId;
            preMovimentoDto.ValorFrete = preMovimento.ValorFrete;
            preMovimentoDto.ValorICMS = preMovimento.ValorICMS;
            preMovimentoDto.MotivoPerdaProdutoId = preMovimento.MotivoPerdaProdutoId;
            preMovimentoDto.CFOPId = preMovimento.CFOPId;
            preMovimentoDto.GrupoOperacaoId = preMovimento.GrupoOperacaoId;
            preMovimentoDto.DescontoPer = preMovimento.DescontoPer;
            preMovimentoDto.HoraDiaId = preMovimento.HoraDiaId;
            preMovimentoDto.HoraPrescrita = preMovimento.HoraPrescrita;
            preMovimentoDto.PrescricaoMedicaId = preMovimento.PrescricaoMedicaId;

            if (preMovimento.EstTipoMovimento != null)
            {
                preMovimentoDto.TipoMovimento = new EstoqueTipoMovimentoDto { Id = preMovimento.EstTipoMovimento.Id, Descricao = preMovimento.EstTipoMovimento.Descricao };
            }

            if (preMovimento.CentroCusto != null)
            {
                preMovimentoDto.CentroCusto = new CentroCustoDto { Id = preMovimento.CentroCusto.Id, Descricao = preMovimento.CentroCusto.Descricao };
            }


            if (preMovimento.Empresa != null)
            {
                preMovimentoDto.Empresa = preMovimento.Empresa.MapTo<EmpresaDto>();
            }

            if (preMovimento.Atendimento != null)
            {
                preMovimentoDto.Atendimento = AtendimentoDto.Mapear(preMovimento.Atendimento);
            }

            if (preMovimento.MedicoSolicitante != null)
            {
                preMovimentoDto.MedicoSolicitante = MedicoDto.Mapear(preMovimento.MedicoSolicitante);
            }

            if (preMovimento.Estoque != null)
            {
                preMovimentoDto.Estoque = preMovimento.Estoque.MapTo<EstoqueDto>();
            }

            return preMovimentoDto;
        }

        public static EstoquePreMovimento MapPreMovimento(EstoquePreMovimentoDto preMovimentoDto)
        {
            var preMovimento = new EstoquePreMovimento();

            preMovimento.AcrescimoDecrescimo = preMovimentoDto.AcrescimoDecrescimo;
            preMovimento.AplicacaoDireta = preMovimentoDto.AplicacaoDireta;
            preMovimento.AtendimentoId = preMovimentoDto.AtendimentoId;
            preMovimento.CentroCustoId = preMovimentoDto.CentroCustoId;
            preMovimento.Consiginado = preMovimentoDto.Consiginado;
            preMovimento.Contabiliza = preMovimentoDto.Contabiliza;
            preMovimento.CreationTime = preMovimentoDto.CreationTime;
            preMovimento.CreatorUserId = preMovimentoDto.CreatorUserId;
            preMovimento.DeleterUserId = preMovimentoDto.DeleterUserId;
            preMovimento.DeletionTime = preMovimentoDto.DeletionTime;
            preMovimento.DescontoPer = preMovimentoDto.DescontoPer;
            preMovimento.Documento = preMovimentoDto.Documento;
            preMovimento.Emissao = preMovimentoDto.Emissao;
            preMovimento.EmpresaId = preMovimentoDto.EmpresaId;
            preMovimento.EntragaProduto = preMovimentoDto.EntragaProduto;
            preMovimento.EstoqueId = preMovimentoDto.EstoqueId;
            preMovimento.SisFornecedorId = preMovimentoDto.FornecedorId;
            preMovimento.Frete = preMovimentoDto.Frete;
            preMovimento.FretePer = preMovimentoDto.FretePer;
            preMovimento.Frete_SisFornecedorId = preMovimentoDto.Frete_FornecedorId;
            preMovimento.ICMSPer = preMovimentoDto.ICMSPer;
            preMovimento.Id = preMovimentoDto.Id;
            preMovimento.InclusoNota = preMovimentoDto.InclusoNota;
            preMovimento.IsEntrada = preMovimentoDto.IsEntrada;
            preMovimento.MedicoSolcitanteId = preMovimentoDto.MedicoSolcitanteId;
            preMovimento.Movimento = preMovimentoDto.Movimento;
            preMovimento.Observacao = preMovimentoDto.Observacao;
            preMovimento.OrdemId = preMovimentoDto.OrdemId;
            preMovimento.PacienteId = preMovimentoDto.PacienteId;
            preMovimento.PreMovimentoEstadoId = preMovimentoDto.PreMovimentoEstadoId;
            preMovimento.Quantidade = preMovimentoDto.Quantidade;
            preMovimento.Serie = preMovimentoDto.Serie;
            preMovimento.EstTipoOperacaoId = preMovimentoDto.EstTipoOperacaoId;
            preMovimento.TipoFreteId = preMovimentoDto.TipoFreteId;
            preMovimento.EstTipoMovimentoId = preMovimentoDto.EstTipoMovimentoId;
            preMovimento.TotalDocumento = preMovimentoDto.TotalDocumento;
            preMovimento.TotalProduto = preMovimentoDto.TotalProduto;
            preMovimento.UnidadeOrganizacionalId = preMovimentoDto.UnidadeOrganizacionalId;
            preMovimento.ValorFrete = preMovimentoDto.ValorFrete;
            preMovimento.ValorICMS = preMovimentoDto.ValorICMS;
            preMovimento.MotivoPerdaProdutoId = preMovimentoDto.MotivoPerdaProdutoId;
            preMovimento.CFOPId = preMovimentoDto.CFOPId;
            preMovimento.GrupoOperacaoId = preMovimentoDto.GrupoOperacaoId;
            preMovimento.DescontoPer = preMovimentoDto.DescontoPer;

            preMovimento.HoraDiaId = preMovimentoDto.HoraDiaId;
            preMovimento.HoraPrescrita = preMovimentoDto.HoraPrescrita;
            preMovimento.PrescricaoMedicaId = preMovimentoDto.PrescricaoMedicaId;

            return preMovimento;
        }

        public static List<EstoquePreMovimentoDto> MapPreMovimento(List<EstoquePreMovimento> preMovimentos)
        {
            var preMovimentosDto = new List<EstoquePreMovimentoDto>();
            foreach (var item in preMovimentos)
            {
                preMovimentosDto.Add(MapPreMovimento(item));
            }
            return preMovimentosDto;
        }

        public static List<EstoquePreMovimento> MapPreMovimento(List<EstoquePreMovimentoDto> preMovimentosDto)
        {
            var preMovimentos = new List<EstoquePreMovimento>();
            foreach (var item in preMovimentosDto)
            {
                preMovimentos.Add(MapPreMovimento(item));
            }
            return preMovimentos;
        }

        [UnitOfWork]
        public async Task<DefaultReturn<EstoqueTransferenciaProdutoDto>> TransferirProduto(EstoqueTransferenciaProdutoDto transferenciaProdutoDto)
        {
            var _retornoPadrao = new DefaultReturn<EstoqueTransferenciaProdutoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var documento = await _ultimoIdAppService.ObterProximoCodigo("TransferenciaProduto");

                    var preMovimentoSaida = new EstoquePreMovimento();
                    preMovimentoSaida.EstoqueId = transferenciaProdutoDto.EstoqueSaidaId;
                    preMovimentoSaida.Movimento = transferenciaProdutoDto.Movimento;
                    preMovimentoSaida.Emissao = preMovimentoSaida.Movimento;
                    preMovimentoSaida.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Autorizado;
                    preMovimentoSaida.EstTipoOperacaoId = 2;
                    preMovimentoSaida.IsEntrada = false;
                    preMovimentoSaida.Id = transferenciaProdutoDto.PreMovimentoSaidaId;
                    preMovimentoSaida.Documento = documento;

                    preMovimentoSaida.GrupoOperacaoId = (long)EnumGrupoOperacao.Movimentos;

                    var preMovimentoEntrada = new EstoquePreMovimento();
                    preMovimentoEntrada.EstoqueId = transferenciaProdutoDto.EstoqueEntradaId;
                    preMovimentoEntrada.Movimento = transferenciaProdutoDto.Movimento;
                    preMovimentoEntrada.Emissao = preMovimentoSaida.Movimento;
                    preMovimentoEntrada.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Autorizado;
                    preMovimentoEntrada.EstTipoOperacaoId = 2;
                    preMovimentoEntrada.IsEntrada = true;
                    preMovimentoEntrada.Id = transferenciaProdutoDto.PreMovimentoEntradaId;
                    preMovimentoEntrada.Documento = documento;
                    preMovimentoEntrada.GrupoOperacaoId = (long)EnumGrupoOperacao.Movimentos;

                    PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                         , _estoquePreMovimentoLoteValidadeRepository
                                                                                                         , _estoquePreMovimentoItemRepository
                                                                                                         , _estoqueMovimentoRepository
                                                                                                         , _estoqueMovimentoItemRepository
                                                                                                         , _estoqueMovimentoLoteValidadeRepository
                                                                                                         , _loteValidadeRepository
                                                                                                         , _estoquePreMovimentoRepository
                                                                                                         , _produtoRepository
                                                                                                         , _produtoUnidadeRepository
                                                                                                         , _estoqueGrupoRepository);






                    _retornoPadrao.Errors = preMovimentoValidacaoService.ValidarSaidaEstoque(preMovimentoSaida);



                    var erroESt0003 = new List<ErroDto>();
                    erroESt0003 = _retornoPadrao.Errors.Where(w => w.CodigoErro == "EST0003").ToList();
                    if (erroESt0003.Count() > 0)
                    {

                        _retornoPadrao.Warnings.Add(new ErroDto { Descricao = "Existem itens não disponiveis no estoque.  A validação será realizada na confirmação da saída." });

                        foreach (var item in erroESt0003)
                        {
                            _retornoPadrao.Errors.Remove(item);
                        }


                    }



                    if (_retornoPadrao.Errors.Count() == 0)
                    {


                        if (transferenciaProdutoDto.Id.Equals(0))
                        {
                            var transferenciaProduto = new EstoqueTransferenciaProduto();
                            transferenciaProduto.PreMovimentoEntrada = preMovimentoEntrada;
                            transferenciaProduto.PreMovimentoSaida = preMovimentoSaida;

                            transferenciaProdutoDto.Id = AsyncHelper.RunSync(() => _estoqueTransferenciaProdutoRepository.InsertAndGetIdAsync(transferenciaProduto));
                        }
                        else
                        {
                            var transferenciaProduto = _estoqueTransferenciaProdutoRepository.Get(transferenciaProdutoDto.Id);
                            if (transferenciaProduto != null)
                            {
                                // transferenciaProduto.PreMovimentoEntradaId = preMovimentoEntrada.Id;
                                transferenciaProduto.PreMovimentoEntrada = _estoquePreMovimentoRepository.Get(transferenciaProduto.PreMovimentoEntradaId);
                                transferenciaProduto.PreMovimentoEntrada.EstoqueId = preMovimentoEntrada.EstoqueId;

                                //transferenciaProduto.PreMovimentoSaidaId = preMovimentoSaida.Id;
                                transferenciaProduto.PreMovimentoSaida = _estoquePreMovimentoRepository.Get(transferenciaProduto.PreMovimentoSaidaId);
                                transferenciaProduto.PreMovimentoSaida.EstoqueId = preMovimentoSaida.EstoqueId;

                                AsyncHelper.RunSync(() => _estoqueTransferenciaProdutoRepository.UpdateAsync(transferenciaProduto));
                            }
                        }
                    }
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                    transferenciaProdutoDto.Documento = documento;
                    _retornoPadrao.ReturnObject = transferenciaProdutoDto;

                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }

        public async Task<PagedResultDto<EstoqueTransferenciaProdutoDto>> ListarTransferencia(ListarEstoquePreMovimentoInput input)
        {
            var query = _estoqueTransferenciaProdutoRepository.GetAll().Where(w => (w.PreMovimentoSaida.Movimento >= input.PeridoDe
                                                                                 && w.PreMovimentoSaida.Movimento <= input.PeridoAte)
                                                                                 && (String.IsNullOrEmpty(input.Filtro)
                                                                                    || (w.PreMovimentoSaida.Estoque.Descricao.Contains(input.Filtro))
                                                                                        || w.PreMovimentoEntrada.Estoque.Descricao.Contains(input.Filtro)
                                                                                        || w.PreMovimentoEntrada.Documento.Contains(input.Filtro)

                                                                                        )
                                                                                 ).Select(s => new EstoqueTransferenciaProdutoDto
                                                                                 {
                                                                                     Id = s.Id
                                                                                    ,
                                                                                     Documento = s.PreMovimentoSaida.Documento
                                                                                    ,
                                                                                     EstoqueEntrada = s.PreMovimentoEntrada.Estoque.Descricao
                                                                                    ,
                                                                                     EstoqueSaida = s.PreMovimentoSaida.Estoque.Descricao
                                                                                    ,
                                                                                     PreMovimentoEntradaId = s.PreMovimentoEntradaId
                                                                                    ,
                                                                                     PreMovimentoSaidaId = s.PreMovimentoSaidaId
                                                                                    ,
                                                                                     Movimento = s.PreMovimentoEntrada.Movimento
                                                                                    ,
                                                                                     CreatorUserId = s.CreatorUserId,

                                                                                     PreMovimentoEstadoId = s.PreMovimentoEntrada.PreMovimentoEstadoId
                                                                                 });


            var contarTransferencias = await query.CountAsync();
            var transferencias = await query
                                         .AsNoTracking()
                                         .OrderBy(input.Sorting)
                                         .PageBy(input)
                                         .ToListAsync();

            foreach (var item in transferencias)
            {
                if (item.CreatorUserId != null)
                {
                    var usuario = _usuarioRepository.Get((long)item.CreatorUserId);
                    if (usuario != null)
                    {
                        item.Usuario = usuario.FullName;
                    }
                }
            }

            return new PagedResultDto<EstoqueTransferenciaProdutoDto>(
                   contarTransferencias,
                   transferencias
                   );
        }

        public async Task<EstoqueTransferenciaProdutoDto> ObterTransferencia(long id)
        {
            var estoqueTransferenciaProdutoDto = new EstoqueTransferenciaProdutoDto();
            var query = _estoqueTransferenciaProdutoRepository
                  .GetAll()
                  .Include(i => i.PreMovimentoEntrada)
                  .Include(i => i.PreMovimentoSaida)
                  .Where(m => m.Id == id);

            var result = await query.FirstOrDefaultAsync();

            if (result != null)
            {
                estoqueTransferenciaProdutoDto.Id = result.Id;
                estoqueTransferenciaProdutoDto.PreMovimentoEntradaId = result.PreMovimentoEntradaId;
                estoqueTransferenciaProdutoDto.PreMovimentoEntrada = MapPreMovimento(result.PreMovimentoEntrada);

                estoqueTransferenciaProdutoDto.PreMovimentoSaidaId = result.PreMovimentoSaidaId;
                estoqueTransferenciaProdutoDto.PreMovimentoSaida = MapPreMovimento(result.PreMovimentoSaida);
                estoqueTransferenciaProdutoDto.Documento = result.PreMovimentoSaida.Documento;

                return estoqueTransferenciaProdutoDto;
            }

            return null;
        }

        public async Task<EstoqueTransferenciaProdutoDto> ObterTransferenciaPorEntradaId(long id)
        {
            var estoqueTransferenciaProdutoDto = new EstoqueTransferenciaProdutoDto();
            var query = _estoqueTransferenciaProdutoRepository
                  .GetAll()
                  .Include(i => i.PreMovimentoEntrada)
                  .Include(i => i.PreMovimentoSaida)
                  .Where(m => m.PreMovimentoEntradaId == id);

            var result = await query.FirstOrDefaultAsync();

            if (result != null)
            {
                estoqueTransferenciaProdutoDto.Id = result.Id;
                estoqueTransferenciaProdutoDto.PreMovimentoEntradaId = result.PreMovimentoEntradaId;
                estoqueTransferenciaProdutoDto.PreMovimentoEntrada = MapPreMovimento(result.PreMovimentoEntrada);

                estoqueTransferenciaProdutoDto.PreMovimentoSaidaId = result.PreMovimentoSaidaId;
                estoqueTransferenciaProdutoDto.PreMovimentoSaida = MapPreMovimento(result.PreMovimentoSaida);
                estoqueTransferenciaProdutoDto.Documento = result.PreMovimentoSaida.Documento;

                return estoqueTransferenciaProdutoDto;
            }

            return null;
        }

        public async Task<PagedResultDto<MovimentoItemDto>> ListarItensTranferencia(ListarEstoquePreMovimentoInput input)
        {
            var id = long.Parse(input.Filtro);
            var itensTransferencias = _estoqueTransferenciaProdutoItemRepository.GetAll()
                .Include(i => i.PreMovimentoSaidaItem)
                .Include(i => i.PreMovimentoSaidaItem.Produto)
                .Include(i => i.PreMovimentoSaidaItem.ProdutoUnidade)
                .Where(w => w.EstoqueTransferenciaProdutoID == id).ToList();

            // var itens = await ListarItens(input);

            List<MovimentoItemDto> itens = new List<MovimentoItemDto>();
            MovimentoItemDto item;
            foreach (var itemTransferencia in itensTransferencias)
            {
                item = new MovimentoItemDto();

                item.TransferenciaItemId = itemTransferencia.Id;

                var preMovimentoItem = itemTransferencia.PreMovimentoSaidaItem;

                item.Id = itemTransferencia.Id;
                item.ProdutoId = itemTransferencia.PreMovimentoSaidaItem.ProdutoId;
                item.Produto = itemTransferencia.PreMovimentoSaidaItem.Produto.Descricao;
                item.Quantidade = itemTransferencia.PreMovimentoSaidaItem.Quantidade / itemTransferencia.PreMovimentoSaidaItem.ProdutoUnidade.Fator;
                item.Unidade = itemTransferencia.PreMovimentoSaidaItem.ProdutoUnidade.Descricao;



                var preMovimentoItemLoteValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                    .Include(i => i.LoteValidade)
                    .Where(w => w.EstoquePreMovimentoItemId == preMovimentoItem.Id).FirstOrDefault();

                if (preMovimentoItemLoteValidade != null && preMovimentoItemLoteValidade.LoteValidade != null)
                {
                    item.Lote = preMovimentoItemLoteValidade.LoteValidade.Lote;
                    item.Validade = preMovimentoItemLoteValidade.LoteValidade.Validade;

                    if (preMovimentoItemLoteValidade.LoteValidade.EstLaboratorioId != null)
                    {
                        item.Laboratorio = _produtoLaboratorioRepository.Get((long)preMovimentoItemLoteValidade.LoteValidade.EstLaboratorioId).Descricao;
                    }
                }
                itens.Add(item);
            }

            return new PagedResultDto<MovimentoItemDto>(
                  itens.Count,
                  itens
                  );
        }

        public async Task ExcluirTransferencia(long transferenciaId)
        {
            try
            {

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var transferencia = _estoqueTransferenciaProdutoRepository.Get(transferenciaId);
                    if (transferencia != null)
                    {

                        var transferenciasItens = _estoqueTransferenciaProdutoItemRepository.GetAll().Where(w => w.EstoqueTransferenciaProdutoID == transferenciaId);

                        foreach (var itemTransferencia in transferenciasItens)
                        {
                            //var itemTransferenciaSaida = _estoquePreMovimentoItemRepository.Get(itemTransferencia.PreMovimentoSaidaItemId);

                            //var itemTransferenciaSaidaId = itemTransferencia.PreMovimentoSaidaItemId;



                            // await _estoquePreMovimentoItemRepository.DeleteAsync(itemTransferencia.PreMovimentoSaidaItem);

                            //// var itemTransferenciaEntrada = _estoquePreMovimentoItemRepository.Get(itemTransferencia.PreMovimentoEntradaItemId);

                            //var itemTransferenciaEntradaId = itemTransferencia.PreMovimentoEntradaItemId;
                            // await _estoquePreMovimentoItemRepository.DeleteAsync(itemTransferencia.PreMovimentoEntradaItem);

                            await _estoqueTransferenciaProdutoItemRepository.DeleteAsync(itemTransferencia);
                        }

                        var preMovimentoSaida = _estoquePreMovimentoRepository.Get(transferencia.PreMovimentoSaidaId);
                        //await _estoquePreMovimentoRepository.DeleteAsync(preMovimentoSaida);
                        await Excluir(MapPreMovimento(preMovimentoSaida));

                        var preMovimentoEntrada = _estoquePreMovimentoRepository.Get(transferencia.PreMovimentoEntradaId);
                        //  await _estoquePreMovimentoRepository.DeleteAsync(preMovimentoEntrada);
                        await Excluir(MapPreMovimento(preMovimentoEntrada));


                        await _estoqueTransferenciaProdutoRepository.DeleteAsync(transferencia);
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }



            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<MovimentoIndexDto>> ListarMovimentosPendente(ListarEstoquePreMovimentoInput input)
        {

            var contarEstoquePreMovimentos = 0;
            List<MovimentoIndexDto> EstoquePreMovimentos;
            List<MovimentoIndexDto> EstoquePreMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {
                DateTime PeridoDe = ((DateTime)input.PeridoDe).Date;
                DateTime PeridoAte = ((DateTime)input.PeridoAte).Date.AddDays(1).AddSeconds(-1);

                var query = _estoquePreMovimentoRepository
                    .GetAll()
                    .Where(m => ((input.Filtro == "" || input.Filtro == null)
                     || m.Documento.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                     || m.Emissao.ToString().Contains(input.Filtro.ToUpper())
                   || m.EstTipoOperacao.Descricao.Contains(input.Filtro.ToUpper())
                    || m.TotalDocumento.ToString().Contains(input.Filtro.ToUpper())
                   )
                   //  && (input.TipoMovimentoId == null || m.TipoMovimentoId == input.TipoMovimentoId)
                   && ((input.PeridoDe == null || m.Emissao >= PeridoDe)
                   && (input.PeridoAte == null || m.Emissao <= PeridoAte))
                    && (input.TipoOperacaoId == null || m.EstTipoOperacaoId == input.TipoOperacaoId)

                   && m.PreMovimentoEstadoId == 1

                   && (m.EstTipoOperacaoId != 2 || (m.EstTipoOperacaoId == 2 && m.IsEntrada))

                    ).Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,

                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : String.Empty
                                                      ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId

                       ,
                        IsEntrada = s.IsEntrada
                       ,
                        TipoMovimento = s.EstTipoOperacao.Descricao
                       ,
                        TipoOperacaoId = s.EstTipoOperacaoId
                       ,
                        ValorDocumento = s.TotalDocumento
                    });

                contarEstoquePreMovimentos = await query.CountAsync();

                EstoquePreMovimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //EstoquePreMovimentoDtos = EstoquePreMovimentos
                //    .MapTo<List<EstoquePreMovimentoDto>>();




                foreach (var preMovimento in EstoquePreMovimentos)
                {
                    if (preMovimento.UsuarioId != null)
                    {
                        var usuario = _usuarioRepository.Get((long)preMovimento.UsuarioId);
                        if (usuario != null)
                        {
                            preMovimento.Usuario = usuario.FullName;
                        }
                    }
                    // preMovimento.TipoMovimento = ObterTipoMovimentacao(preMovimento);
                }

                return new PagedResultDto<MovimentoIndexDto>(
                    contarEstoquePreMovimentos,
                    EstoquePreMovimentos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }



        }

        string ObterTipoMovimentacao(MovimentoIndexDto preMovimento)
        {
            var transferencia = _estoqueTransferenciaProdutoRepository.GetAll()
                .Where(w => (w.PreMovimentoEntradaId == preMovimento.Id || w.PreMovimentoSaidaId == preMovimento.Id)).FirstOrDefault();

            if (transferencia != null)
            {
                preMovimento.Id = transferencia.Id;
                return "Transferência";
            }
            else
            {
                return preMovimento.IsEntrada ? "Entrada" : "Saída";
            }

        }

        public DefaultReturn<EstoquePreMovimentoDto> CriarGetIdDevolucao(EstoquePreMovimentoDto input)
        {
            _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            input.IsEntrada = true;
            input.Movimento = input.Emissao;
            input.EstTipoOperacaoId = 4;
            input.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Autorizado;

            if (string.IsNullOrEmpty(input.Documento))
            {
                input.Documento = _ultimoIdAppService.ObterProximoCodigo("DevolucaoProduto").Result;
            }

            _retornoPadrao.ReturnObject = CriarGetId(input);

            return _retornoPadrao;
        }

        public async Task<PagedResultDto<MovimentoIndexDto>> ListarDevolucoes(ListarEstoquePreMovimentoInput input)
        {
            var contarEstoquePreMovimentos = 0;
            List<MovimentoIndexDto> EstoquePreMovimentos;
            List<MovimentoIndexDto> EstoquePreMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {
                input.PeridoDe = ((DateTime)input.PeridoDe).Date;
                input.PeridoAte = ((DateTime)input.PeridoAte).Date.AddDays(1).AddSeconds(-1);

                var query = _estoquePreMovimentoRepository
                    .GetAll()
                    .Where(m => ((input.Filtro == "" || input.Filtro == null)
                     ||
                     m.Documento.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                   || m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.Emissao.ToString().Contains(input.Filtro.ToUpper())
                   || m.SisFornecedor.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.TotalDocumento.ToString().Contains(input.Filtro.ToUpper())
                   || m.EstTipoMovimento.Descricao.Contains(input.Filtro.ToUpper())
                   )
                   && (input.FornecedorId == null || m.SisFornecedor.Id == input.FornecedorId)
                   && (input.PeridoDe == null || m.Emissao >= input.PeridoDe)
                   && (input.PeridoAte == null || m.Emissao <= input.PeridoAte)
                   && (input.TipoMovimentoId == null || m.EstTipoMovimento.Id == input.TipoMovimentoId)
                   && m.EstTipoOperacaoId == (long)EnumTipoOperacao.Devolucao

                    ).Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,
                        Fornecedor = (s.SisFornecedor != null) ? s.SisFornecedor.Descricao : String.Empty
                                                        ,
                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : String.Empty
                                                        ,
                        Valor = s.TotalDocumento
                        ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId,

                        Estoque = (s.Estoque != null) ? s.Estoque.Descricao : String.Empty,

                        TipoMovimento = (s.EstTipoMovimento != null) ? s.EstTipoMovimento.Descricao : String.Empty
                    });

                contarEstoquePreMovimentos = await query.CountAsync();

                EstoquePreMovimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //EstoquePreMovimentoDtos = EstoquePreMovimentos
                //    .MapTo<List<EstoquePreMovimentoDto>>();


                foreach (var preMovimento in EstoquePreMovimentos)
                {
                    if (preMovimento.UsuarioId != null)
                    {
                        var usuario = _usuarioRepository.Get((long)preMovimento.UsuarioId);
                        if (usuario != null)
                        {
                            preMovimento.Usuario = usuario.FullName;
                        }
                    }

                }




                return new PagedResultDto<MovimentoIndexDto>(
                    contarEstoquePreMovimentos,
                    EstoquePreMovimentos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public DefaultReturn<EstoquePreMovimentoDto> CriarOuEditarDevolucoes(EstoquePreMovimentoDto input)
        {
            _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {
                var query = _estoquePreMovimentoItemRepository
                  .GetAll()
                 .Where(m => m.PreMovimentoId == input.Id);

                var preMovimentoItens = query.ToList();
                var _preMovimentoItens = EstoquePreMovimentoItemAppService.MapPreMovimentoItem(preMovimentoItens);//.MapTo<List<EstoquePreMovimentoItemDto>>();

                //PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                //                                                                                           , _estoquePreMovimentoLoteValidadeRepository
                //                                                                                           , _estoquePreMovimentoItemRepository
                //                                                                                           , _estoqueMovimentoRepository
                //                                                                                           , _estoqueMovimentoItemRepository
                //                                                                                           , _estoqueMovimentoLoteValidadeRepository
                //                                                                                           , _loteValidadeRepository
                //                                                                                           , _estoquePreMovimentoRepository
                //                                                                                           , _produtoRepository
                //                                                                                           , _produtoUnidadeRepository);

                //_retornoPadrao.Errors = preMovimentoValidacaoService.Validar(input, _preMovimentoItens);


                if (_retornoPadrao.Errors.Count() == 0)
                {

                    EstoquePreMovimento preMovimento = MapPreMovimento(input);//.MapTo<EstoquePreMovimento>();


                    if (input.Id.Equals(0))
                    {
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            input.Id = AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertAndGetIdAsync(preMovimento));

                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }

                    }
                    else
                    {
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            var edit = _estoquePreMovimentoRepository.Get(input.Id);
                            if (edit != null)
                            {
                                edit.OrdemId = input.OrdemId;
                                edit.Quantidade = input.Quantidade;
                                edit.Serie = input.Serie;
                                edit.EstTipoMovimentoId = input.TipoDocumentoId;
                                edit.ValorICMS = input.ValorICMS;
                                edit.EmpresaId = input.EmpresaId;
                                edit.SisFornecedorId = input.FornecedorId;
                                edit.Documento = input.Documento;
                                edit.Serie = input.Serie;
                                edit.EstoqueId = input.EstoqueId;

                                edit.MedicoSolcitanteId = input.MedicoSolcitanteId;
                                edit.PacienteId = input.PacienteId;
                                edit.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                                edit.AtendimentoId = input.AtendimentoId;
                                edit.Observacao = input.Observacao;

                                edit.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Autorizado;
                                edit.EstTipoMovimentoId = input.EstTipoMovimentoId;

                                edit.Codigo = input.Codigo;

                                AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.UpdateAsync(edit));//.MapTo<EstoquePreMovimentoDto>();

                                unitOfWork.Complete();
                                _unitOfWorkManager.Current.SaveChanges();
                                unitOfWork.Dispose();

                            }
                        }
                    }
                    _retornoPadrao.ReturnObject = input;
                }
            }

            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }

        #region Solicitações

        public async Task<PagedResultDto<MovimentoIndexDto>> ListarSolicitacoes(ListarEstoquePreMovimentoInput input)
        {
            var contarEstoquePreMovimentos = 0;
            List<MovimentoIndexDto> EstoquePreMovimentos;
            List<MovimentoIndexDto> EstoquePreMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {
                input.PeridoDe = ((DateTime)input.PeridoDe).Date;
                input.PeridoAte = ((DateTime)input.PeridoAte).Date.AddDays(1).AddSeconds(-1);

                var query = _estoquePreMovimentoRepository
                    .GetAll()
                    .Where(m => ((input.Filtro == "" || input.Filtro == null)
                     ||
                     m.Documento.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                   || m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.Emissao.ToString().Contains(input.Filtro.ToUpper())
                   || m.Estoque.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                   || m.TotalDocumento.ToString().Contains(input.Filtro.ToUpper())
                   || m.EstTipoMovimento.Descricao.Contains(input.Filtro.ToUpper())
                   )
                   && (input.EstoqueId == null || m.EstoqueId == input.EstoqueId)
                   && (input.PeridoDe == null || m.Emissao >= input.PeridoDe)
                   && (input.PeridoAte == null || m.Emissao <= input.PeridoAte)
                   && (input.TipoMovimentoId == null || m.EstTipoMovimento.Id == input.TipoMovimentoId)

                   && m.GrupoOperacaoId == (long)EnumGrupoOperacao.Solicitacao
                  // && (m.PreMovimentoEstadoId != (long)EnumPreMovimentoEstado.TotalmenteSuspenso && m.PreMovimentoEstadoId != (long)EnumPreMovimentoEstado.ParcialmentoSuspenso)



                    ).Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,
                        Fornecedor = (s.SisFornecedor != null) ? s.SisFornecedor.Descricao : String.Empty
                                                        ,
                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : String.Empty
                                                        ,
                        Valor = s.TotalDocumento
                        ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId,

                        Estoque = (s.Estoque != null) ? s.Estoque.Descricao : String.Empty,

                        TipoMovimento = (s.EstTipoMovimento != null) ? s.EstTipoMovimento.Descricao : String.Empty,

                        HoraPrescrita =  s.HoraPrescrita

                    });

                contarEstoquePreMovimentos = await query.CountAsync();

                EstoquePreMovimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //EstoquePreMovimentoDtos = EstoquePreMovimentos
                //    .MapTo<List<EstoquePreMovimentoDto>>();


                foreach (var preMovimento in EstoquePreMovimentos)
                {
                    if (preMovimento.UsuarioId != null)
                    {
                        var usuario = _usuarioRepository.Get((long)preMovimento.UsuarioId);
                        if (usuario != null)
                        {
                            preMovimento.Usuario = usuario.FullName;
                        }
                    }

                }




                return new PagedResultDto<MovimentoIndexDto>(
                    contarEstoquePreMovimentos,
                    EstoquePreMovimentos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public DefaultReturn<EstoquePreMovimentoDto> CriarOuEditarSolicitacao(EstoquePreMovimentoDto input)
        {
            _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {
                if (_retornoPadrao.Errors.Count() == 0)
                {
                    EstoquePreMovimento preMovimento = MapPreMovimento(input);
                    preMovimento.IsEntrada = false;
                    preMovimento.Movimento = DateTime.Now;
                    preMovimento.GrupoOperacaoId = (long)EnumGrupoOperacao.Solicitacao;
                    preMovimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Pendente;

                    var itens = JsonConvert.DeserializeObject<List<EstoquePreMovimentoItemSolicitacaoDto>>(input.Itens);

                    if (preMovimento.AtendimentoId != null && preMovimento.AtendimentoId != 0)
                    {
                        var atendimento = _atendimentoRepository.Get((long)preMovimento.AtendimentoId);
                        if (atendimento != null)
                        {
                            preMovimento.MedicoSolcitanteId = atendimento.MedicoId;
                            preMovimento.PacienteId = atendimento.PacienteId;
                        }
                    }

                    if (input.Id.Equals(0))
                    {
                        var preMovimentosItens = new List<EstoquePreMovimentoItem>();
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            preMovimento.Documento = _ultimoIdAppService.ObterProximoCodigo("Solicitacao").Result;
                            //input.Id = AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertAndGetIdAsync(preMovimento));

                            input.Id = _estoquePreMovimentoRepository.InsertAndGetId(preMovimento);

                            foreach (var item in itens)
                            {
                                var preMovimentoItem = new EstoqueSolicitacaoItem
                                {
                                    SolicitacaoId = input.Id
                                                                                     ,
                                    ProdutoId = item.ProdutoId
                                                                                     ,
                                    ProdutoUnidadeId = (long)item.ProdutoUnidadeId
                                                                                     ,
                                    Quantidade = item.Quantidade
                                    ,
                                    EstadoSolicitacaoItemId = (long)EnumPreMovimentoEstado.Pendente
                                };
                                //   preMovimentosItens.Add(preMovimentoItem);
                                //AsyncHelper.RunSync(() =>
                                _estoqueSolcitacaiItemRepository.InsertAndGetId(preMovimentoItem);

                                //  _produtoSaldoAppService.PreMovimentoItem = preMovimentoItem;

                                //  _produtoSaldoAppService.AtulizarSaldoPreMovimentoItemPreMovimento(preMovimento);//, preMovimento);
                            }

                            //  foreach (var item in preMovimentosItens)
                            // {
                            //item.EstoquePreMovimento = preMovimento;
                            //_produtoSaldoAppService.AtulizarSaldoPreMovimentoItemPreMovimento(item);//, preMovimento);
                            //  }

                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }

                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            foreach (var item in preMovimentosItens)
                            {
                                //  _produtoSaldoAppService.AtulizarSaldoPreMovimentoItemPreMovimento(item, preMovimento);
                            }

                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }
                    }
                    else
                    {
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            var edit = _estoquePreMovimentoRepository.Get(input.Id);
                            if (edit != null)
                            {
                                edit.EstTipoMovimentoId = input.EstTipoMovimentoId;
                                edit.EmpresaId = input.EmpresaId;
                                edit.SisFornecedorId = input.FornecedorId;
                                edit.Documento = input.Documento;
                                edit.EstoqueId = input.EstoqueId;
                                edit.Emissao = input.Emissao;
                                edit.Observacao = input.Observacao;
                                edit.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;
                                edit.AtendimentoId = input.AtendimentoId;
                                edit.MedicoSolcitanteId = preMovimento.MedicoSolcitanteId;
                                edit.PacienteId = preMovimento.PacienteId;

                                AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.UpdateAsync(edit));


                                foreach (var item in itens)
                                {
                                    var preMovimentoItem = new EstoquePreMovimentoItem
                                    {
                                        NumeroSerie = item.NumeroSerie
                                                                                         ,
                                        PreMovimentoId = input.Id
                                                                                         ,
                                        ProdutoId = item.ProdutoId
                                                                                         ,
                                        ProdutoUnidadeId = item.ProdutoUnidadeId
                                                                                         ,
                                        Quantidade = item.Quantidade
                                        ,
                                        Id = item.Id
                                    };

                                    AsyncHelper.RunSync(() => _estoquePreMovimentoItemRepository.InsertOrUpdateAsync(preMovimentoItem));
                                }



                                var itensMantidos = itens.Where(w => w.Id != 0).ToList();

                                var itensAtuais = _estoquePreMovimentoItemRepository.GetAll().Where(w => w.PreMovimentoId == input.Id).ToList();


                                var itensExcluidos = itensAtuais.Where(w => !itensMantidos.Any(a => a.Id == w.Id));//    Except(itensMantidos);

                                foreach (var item in itensExcluidos)
                                {
                                    _estoquePreMovimentoItemRepository.DeleteAsync(item);
                                }


                                unitOfWork.Complete();
                                _unitOfWorkManager.Current.SaveChanges();
                                unitOfWork.Dispose();

                            }
                        }
                    }
                    _retornoPadrao.ReturnObject = input;
                }
            }

            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }

        public async Task<PagedResultDto<MovimentoIndexDto>> ListarSolicitacaoesPendente(ListarEstoquePreMovimentoInput input)
        {
            var contarEstoquePreMovimentos = 0;
            List<MovimentoIndexDto> EstoquePreMovimentos;
            List<MovimentoIndexDto> EstoquePreMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {
                input.PeridoDe = ((DateTime)input.PeridoDe).Date.AddSeconds(-1);
                input.PeridoAte = ((DateTime)input.PeridoAte).Date.AddDays(1).AddSeconds(-1);

                var query = _estoquePreMovimentoRepository
                    .GetAll()
                    .Where(m => ((input.Filtro == "" || input.Filtro == null)
                     || m.Documento.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                     || m.Emissao.ToString().Contains(input.Filtro.ToUpper())
                   || m.EstTipoOperacao.Descricao.Contains(input.Filtro.ToUpper())
                    || m.TotalDocumento.ToString().Contains(input.Filtro.ToUpper())
                     || m.Estoque.Descricao.Contains(input.Filtro.ToUpper())
                   )
                   //  && (input.TipoMovimentoId == null || m.TipoMovimentoId == input.TipoMovimentoId)
                   && (input.PeridoDe == null || m.Emissao >= input.PeridoDe)
                   && (input.PeridoAte == null || m.Emissao <= input.PeridoAte)
                   && (input.TipoOperacaoId == null || m.EstTipoOperacaoId == input.TipoOperacaoId)
                    && (input.EstoqueId == null || m.EstoqueId == input.EstoqueId)

                   && (m.PreMovimentoEstadoId == (long)EnumPreMovimentoEstado.Pendente
                      || m.PreMovimentoEstadoId == (long)EnumPreMovimentoEstado.ParcialmenteAtendido)

                   && m.GrupoOperacaoId == (long)EnumGrupoOperacao.Solicitacao

                    )
                    .Include(i => i.EstTipoMovimento)
                    .Include(i => i.Estoque)

                    .Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,

                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : String.Empty
                                                      ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId

                       ,
                        IsEntrada = s.IsEntrada
                       ,
                        TipoMovimento = s.EstTipoMovimento.Descricao
                       ,
                        TipoOperacaoId = s.EstTipoOperacaoId
                       ,
                        ValorDocumento = s.TotalDocumento
                        ,
                        Estoque = s.Estoque.Descricao,
                        HoraPrescrita = s.HoraPrescrita
                    });

                contarEstoquePreMovimentos = await query.CountAsync();

                EstoquePreMovimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //EstoquePreMovimentoDtos = EstoquePreMovimentos
                //    .MapTo<List<EstoquePreMovimentoDto>>();




                foreach (var preMovimento in EstoquePreMovimentos)
                {
                    if (preMovimento.UsuarioId != null)
                    {
                        var usuario = _usuarioRepository.Get((long)preMovimento.UsuarioId);
                        if (usuario != null)
                        {
                            preMovimento.Usuario = usuario.FullName;
                        }
                    }
                    // preMovimento.TipoMovimento = ObterTipoMovimentacao(preMovimento);
                }

                return new PagedResultDto<MovimentoIndexDto>(
                    contarEstoquePreMovimentos,
                    EstoquePreMovimentos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }



        }


        public DefaultReturn<EstoquePreMovimentoDto> AtenderSolicitacao(EstoquePreMovimentoDto preMovimento)
        {
            var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            List<ProdutoQuantidade> produtosQuantidades = new List<ProdutoQuantidade>();


            MovimentoValidacaoAppService movimentoValidacaoService = new MovimentoValidacaoAppService(_estoqueMovimentoRepository
                                                                                                     , _estoqueMovimentoItemRepository
                                                                                                     , _unitOfWorkManager
                                                                                                     , _estoquePreMovimentoLoteValidadeRepository
                                                                                                     , _estoquePreMovimentoItemRepository
                                                                                                     , _estoqueMovimentoLoteValidadeRepository
                                                                                                     , _loteValidadeRepository
                                                                                                     , _estoquePreMovimentoRepository
                                                                                                     , _produtoRepository
                                                                                                     , _produtoUnidadeRepository
                                                                                                     , _estoqueGrupoRepository);

            _retornoPadrao.Errors = movimentoValidacaoService.ValidarConfirmacaoSolicitacao(preMovimento);


            if (_retornoPadrao.Errors.Count == 0)
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    ProdutoSaldoAppService produtoSaldoAppService = new ProdutoSaldoAppService(_estoqueMovimentoRepository
                                                                                            , _estoqueMovimentoItemRepository
                                                                                            , _estoqueMovimentoLoteValidadeRepository
                                                                                            , _produtoSaldoRepository
                                                                                            , _produtoRepository
                                                                                            , _estoquePreMovimentoRepository
                                                                                            , _estoquePreMovimentoItemRepository
                                                                                            , _estoquePreMovimentoLoteValidadeRepository
                                                                                            //,_unitOfWorkManager

                                                                                            );

                    var preMovimentoSolicitacao = new EstoquePreMovimento();

                    preMovimentoSolicitacao.Documento = preMovimento.Documento;
                    preMovimentoSolicitacao.Emissao = preMovimento.Emissao;
                    preMovimentoSolicitacao.Movimento = preMovimento.Emissao;
                    preMovimentoSolicitacao.EmpresaId = preMovimento.EmpresaId;
                    preMovimentoSolicitacao.EstoqueId = preMovimento.EstoqueId;
                    preMovimentoSolicitacao.MedicoSolcitanteId = preMovimento.MedicoSolcitanteId;
                    preMovimentoSolicitacao.Observacao = preMovimento.Observacao;
                    preMovimentoSolicitacao.PacienteId = preMovimento.PacienteId;
                    preMovimentoSolicitacao.UnidadeOrganizacionalId = preMovimento.UnidadeOrganizacionalId;
                    preMovimentoSolicitacao.AtendimentoId = preMovimento.AtendimentoId;
                    preMovimentoSolicitacao.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Autorizado;
                    preMovimentoSolicitacao.IsEntrada = false;
                    preMovimentoSolicitacao.EstTipoOperacaoId = (long)EnumTipoOperacao.Saida;
                    preMovimentoSolicitacao.EstTipoMovimentoId = preMovimento.EstTipoMovimentoId;
                    preMovimentoSolicitacao.GrupoOperacaoId = (long)EnumGrupoOperacao.Movimentos;

                    preMovimentoSolicitacao.Documento = _ultimoIdAppService.ObterProximoCodigo("SaidaProduto").Result;

                    var preMovimentoSolicitacaoId = _estoquePreMovimentoRepository.InsertAndGetId(preMovimentoSolicitacao);

                    var itens = JsonConvert.DeserializeObject<List<EstoquePreMovimentoItemSolicitacaoDto>>(preMovimento.Itens);

                    var countCompletamenteAtendido = 0;
                    decimal somaQuantidade = 0;

                    foreach (var item in itens)
                    {
                        if (item.QuantidadeAtendida != null && item.QuantidadeAtendida != 0)
                        {
                            produtosQuantidades.Add(new ProdutoQuantidade { ProdutoId = item.ProdutoId, Quantidade = (float)item.Quantidade });

                            if (!string.IsNullOrEmpty(item.NumerosSerieJson))
                            {
                                var numerosSerie = JsonConvert.DeserializeObject<List<NumeroSerieGridDto>>(item.NumerosSerieJson);

                                foreach (var numeroSerie in numerosSerie)
                                {
                                    var movimentoItem = new EstoquePreMovimentoItem();

                                    movimentoItem.PreMovimentoId = preMovimentoSolicitacaoId;
                                    //movimentoItem.MovimentoId = movimentoId;
                                    movimentoItem.ProdutoId = item.ProdutoId;
                                    movimentoItem.ProdutoUnidadeId = item.ProdutoUnidadeId;
                                    movimentoItem.Quantidade = 1;
                                    movimentoItem.NumeroSerie = numeroSerie.NumeroSerie;

                                    movimentoItem.Id = _estoquePreMovimentoItemRepository.InsertAndGetId(movimentoItem);

                                    produtoSaldoAppService.PreMovimentoItem = movimentoItem;
                                    produtoSaldoAppService.AtulizarSaldoPreMovimentoItemPreMovimento(preMovimentoSolicitacao);
                                }
                                somaQuantidade = numerosSerie.Count();
                            }
                            else
                            {

                                var listaSomaQuantidade = _estoqueMovimentoItemRepository.GetAll()
                                                    .Where(w => w.EstoquePreMovimentoItemId == item.Id).ToList();

                                somaQuantidade = listaSomaQuantidade.Sum(s => s.Quantidade);

                                var movimentoItem = new EstoquePreMovimentoItem();

                                movimentoItem.PreMovimentoId = preMovimentoSolicitacaoId;
                                movimentoItem.ProdutoId = item.ProdutoId;
                                movimentoItem.ProdutoUnidadeId = item.ProdutoUnidadeId;
                                movimentoItem.Quantidade = _unidadeAppService.ObterQuantidadeReferencia((long)item.ProdutoUnidadeId, (decimal)item.QuantidadeAtendida);

                                movimentoItem.Id = _estoquePreMovimentoItemRepository.InsertAndGetId(movimentoItem);



                                var lotesValidades = JsonConvert.DeserializeObject<List<LoteValidadeGridDto>>(item.LotesValidadesJson, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                                if (lotesValidades != null)
                                {

                                    foreach (var itemLoteValidade in lotesValidades)
                                    {
                                        //var loteValidade = _loteValidadeRepository.GetAll()
                                        //                    .Where(w => w.Lote == itemLoteValidade.Lote
                                        //                             && w.ProdutoId == movimentoItem.ProdutoId
                                        //                             && w.Validade == itemLoteValidade.Validade
                                        //                             && w.ProdutoLaboratorioId == itemLoteValidade.LaboratorioId).FirstOrDefault();

                                        //if (loteValidade == null)
                                        //{
                                        //    loteValidade = new LoteValidade();
                                        //    loteValidade.Lote = itemLoteValidade.Lote;
                                        //    loteValidade.Validade = (DateTime)itemLoteValidade.Validade;
                                        //    loteValidade.ProdutoId = movimentoItem.ProdutoId;
                                        //    loteValidade.ProdutoLaboratorioId = itemLoteValidade.LaboratorioId;

                                        //    loteValidade.Id = _loteValidadeRepository.InsertAndGetId(loteValidade);
                                        //}

                                        var movimentoLoteValidade = new EstoquePreMovimentoLoteValidade();

                                        movimentoLoteValidade.EstoquePreMovimentoItemId = movimentoItem.Id;
                                        movimentoLoteValidade.LoteValidadeId = itemLoteValidade.LoteValidadeId;
                                        movimentoLoteValidade.Quantidade = _unidadeAppService.ObterQuantidadeReferencia((long)item.ProdutoUnidadeId, (decimal)itemLoteValidade.Quantidade);

                                        _estoquePreMovimentoLoteValidadeRepository.Insert(movimentoLoteValidade);



                                        //  produtoSaldoAppService.PreMovimentoItem = movimentoItem;

                                        produtoSaldoAppService.AtulizarSaldoPreMovimentoItemLoteValidade(movimentoLoteValidade);


                                        somaQuantidade += movimentoItem.Quantidade;

                                    }

                                }
                                else
                                {
                                    somaQuantidade += movimentoItem.Quantidade;
                                }

                            }

                            var solicitacaoItem = _estoqueSolcitacaiItemRepository.Get(item.Id);

                            solicitacaoItem.QuantidadeAtendida += somaQuantidade;

                            if (solicitacaoItem != null && solicitacaoItem.Quantidade <= solicitacaoItem.QuantidadeAtendida)
                            {
                                solicitacaoItem.EstadoSolicitacaoItemId = (long)EnumPreMovimentoEstado.TotalmenteAtendido;
                                countCompletamenteAtendido++;
                            }
                            _estoqueSolcitacaiItemRepository.Update(solicitacaoItem);
                        }







                    }



                    var estoquePreMovimento = _estoquePreMovimentoRepository.Get(preMovimento.Id);
                    if (estoquePreMovimento != null)
                    {
                        if (countCompletamenteAtendido == itens.Count)
                        {
                            estoquePreMovimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.TotalmenteAtendido;
                        }
                        else
                        {
                            estoquePreMovimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.ParcialmenteAtendido;
                        }
                        _estoquePreMovimentoRepository.Update(estoquePreMovimento);
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();

                    _estoqueMovimentoAppService.GerarMovimentoEntrada(preMovimentoSolicitacaoId);


                    //if (preMovimento.EstTipoMovimentoId == (long)EnumTipoMovimento.Paciente_Saida)
                    //{
                    //    GerarFaturamento(preMovimento, produtosQuantidades);
                    //}



                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();







                    _retornoPadrao.ReturnObject = MapPreMovimento(preMovimentoSolicitacao);
                }
            }

            return _retornoPadrao;
        }


        //void GerarFaturamento(EstoquePreMovimentoDto preMovimento, List<ProdutoQuantidade> itens)
        //{
        //    FaturamentoContaItemInsertDto faturamentoContaItemInsertDto = new FaturamentoContaItemInsertDto();

        //    faturamentoContaItemInsertDto.AtendimentoId = preMovimento.AtendimentoId ?? 0;
        //    faturamentoContaItemInsertDto.Data = preMovimento.Movimento;

        //    List<FaturamentoContaItemDto> itensFaturamentoIds = new List<FaturamentoContaItemDto>();

        //    foreach (var item in itens)
        //    {
        //        var produto = _produtoRepository.GetAll()
        //                          .Where(w => w.Id == item.ProdutoId)
        //                          .FirstOrDefault();

        //        if (produto != null && produto.FaturamentoItemId != null)
        //        {
        //            var contaItem = new FaturamentoContaItemDto();
        //            contaItem.Id = (long)produto.FaturamentoItemId;
        //            contaItem.Qtde = (float)item.Quantidade;

        //            itensFaturamentoIds.Add(contaItem);
        //        }
        //    }

        //    faturamentoContaItemInsertDto.ItensFaturamento = itensFaturamentoIds;

        //    _faturamentoContaItemAppService.InserirItensContaFaturamento(faturamentoContaItemInsertDto);

        //}


        private DefaultReturn<DocumentoDto> GerarContasPagar(EstoquePreMovimentoDto preMovimento)
        {
            DocumentoDto documento = new DocumentoDto();

            documento.DataEmissao = preMovimento.Emissao;
            documento.EmpresaId = (long)preMovimento.EmpresaId;
            documento.ForncedorId = preMovimento.FornecedorId;
            documento.Numero = preMovimento.Documento;
            documento.TipoDocumentoId = 1;
            documento.ValorDocumento = preMovimento.TotalDocumento;
            documento.ValorTotalParcelas = preMovimento.TotalDocumento;
            documento.LancamentosJson = preMovimento.LancamentosJson;


            //Informação de rateio provisória

            DocumentoRateioIndex documentoRateioIndex = new DocumentoRateioIndex();

            documentoRateioIndex.CentroCustoId = preMovimento.CentroCustoId;
            documentoRateioIndex.EmpresaId = preMovimento.EmpresaId;
            documentoRateioIndex.ContaAdministrativaId = 1;

            documentoRateioIndex.Valor = preMovimento.TotalDocumento;

            List<DocumentoRateioIndex> rateios = new List<DocumentoRateioIndex>();

            rateios.Add(documentoRateioIndex);


            documento.RateioJson = JsonConvert.SerializeObject(rateios);


            var fornecedor = _fornecedorRepository.GetAll()
                                                  .Where(w => w.Id == preMovimento.FornecedorId)
                                                  .FirstOrDefault();

            if (fornecedor != null)
            {
                documento.PessoaId = fornecedor.SisPessoaId;
                var documentoExiste = _contasPagarAppService.ObterPorPessoaNumero((long)documento.PessoaId, documento.Numero).Result;

                if (documentoExiste != null)
                {
                    documento.Id = documentoExiste.Id;
                }
            }

            var retornoContasPagar = _contasPagarAppService.CriarOuEditar(documento);

            //if (retornoContasPagar.Errors.Count > 0)
            //{
            //    throw (new Exception(retornoContasPagar.Errors[0].Descricao));
            //}

            return retornoContasPagar;
        }


        public async Task<DefaultReturn<DocumentoDto>> ExcluirSolicitacoesPrescritasNaoAtendidas (long prescricaoMedicaId)
        {

            var _retornoPadrao = new DefaultReturn<DocumentoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();

          
            try
            {
                var preMovimentosPendentes = _estoquePreMovimentoRepository.GetAll()
                                                                  .Where(w => w.PrescricaoMedicaId == prescricaoMedicaId
                                                                           && w.PreMovimentoEstadoId == (long)EnumPreMovimentoEstado.Pendente)
                                                                  .ToList();


                var preMovimentosTotais = _estoquePreMovimentoRepository.GetAll()
                                                                  .Where(w => w.PrescricaoMedicaId == prescricaoMedicaId)
                                                                  .ToList();



                // var preMovimentosDto = MapPreMovimento(preMovimentos);

                foreach (var preMovimento in preMovimentosPendentes)
                {
                    preMovimento.PreMovimentoEstadoId = (preMovimentosPendentes.Count == preMovimentosTotais.Count) ? (long)EnumPreMovimentoEstado.TotalmenteSuspenso: (long)EnumPreMovimentoEstado.ParcialmentoSuspenso;
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;

        }



        public async Task<DefaultReturn<EstoquePreMovimento>> ReAtivarSolicitacoDePrescricaoMedica(long prescricaoMedicaId)
        {
            var _retornoPadrao = new DefaultReturn<EstoquePreMovimento>();
            _retornoPadrao.Errors = new List<ErroDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();

            try
            {
                var preMovimentosPendentes = _estoquePreMovimentoRepository.GetAll()
                                                                  .Where(w => w.PrescricaoMedicaId == prescricaoMedicaId
                                                                           && (w.PreMovimentoEstadoId == (long)EnumPreMovimentoEstado.ParcialmentoSuspenso 
                                                                              || w.PreMovimentoEstadoId == (long)EnumPreMovimentoEstado.TotalmenteSuspenso))
                                                                  .ToList();


                _retornoPadrao.ReturnObject = preMovimentosPendentes.FirstOrDefault();

                foreach (var preMovimento in preMovimentosPendentes)
                {
                    preMovimento.PreMovimentoEstadoId =  (long)EnumPreMovimentoEstado.Pendente;
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;

        }

        #endregion

        #region Relatorio

        public RelatorioEntradaModelDto ObterDadosRelatorioEntrada(long preMovimentoId)
        {

            RelatorioEntradaModelDto relatorioEntradaModelDto = new RelatorioEntradaModelDto();
            relatorioEntradaModelDto.Itens = new List<RelatorioEntradaItemModalDto>();

            decimal totalItens = 0;

            var loginInformations = Task.Run(() => _sessionAppService.GetCurrentLoginInformations()).Result;


            var itens = _estoquePreMovimentoItemRepository.GetAll()
                                                          .Where(w => w.PreMovimentoId == preMovimentoId)
                                                          .Include(i => i.Produto)
                                                          .Include(i => i.ProdutoUnidade)
                                                          .Include(i => i.EstoquePreMovimento.Empresa)
                                                          .Include(i => i.EstoquePreMovimento.SisFornecedor)
                                                          .Include(i => i.EstoquePreMovimento.SisFornecedor.SisPessoa)
                                                          //.Include(i => i.EstoquePreMovimento.Fornecedor.FornecedorPessoaJuridica)
                                                          // .Include(i => i.EstoquePreMovimento.Fornecedor.FornecedorPessoaFisica)
                                                          .Include(i => i.EstoquePreMovimento.Estoque)
                                                          .Include(i => i.EstoquePreMovimento.EstTipoMovimento)
                                                          .Include(i => i.EstoquePreMovimento)
                                                          .Include(i => i.EstoquePreMovimento.Paciente)
                                                          .Include(i => i.EstoquePreMovimento.Paciente.SisPessoa)
                                                          .Include(i => i.EstoquePreMovimento.UnidadeOrganizacional)
                                                          .ToList();

            relatorioEntradaModelDto.IsEntrada = itens[0].EstoquePreMovimento.IsEntrada;
            if (relatorioEntradaModelDto.IsEntrada)
            {
                relatorioEntradaModelDto.Titulo = string.Concat("Entrada de Produtos");
            }
            else
            {
                relatorioEntradaModelDto.Titulo = string.Concat("Saída de Produtos");
            }
            //_filtro.NomeHospital = "Lipp";

            relatorioEntradaModelDto.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
            relatorioEntradaModelDto.DataHora = Convert.ToString(DateTime.Now);

            if (itens.Count > 0)
            {
                relatorioEntradaModelDto.NomeHospital = itens[0].EstoquePreMovimento != null ? itens[0].EstoquePreMovimento.Empresa.NomeFantasia.ToString() : string.Empty;

                relatorioEntradaModelDto.Fornecedor = itens[0].EstoquePreMovimento.SisFornecedor != null ? itens[0].EstoquePreMovimento.SisFornecedor.SisPessoa.FisicaJuridica == "F" ? itens[0].EstoquePreMovimento.SisFornecedor.SisPessoa.NomeCompleto : itens[0].EstoquePreMovimento.SisFornecedor.SisPessoa.NomeFantasia : string.Empty;
                relatorioEntradaModelDto.Documento = itens[0].EstoquePreMovimento.Documento;
                relatorioEntradaModelDto.TipoEntrada = itens[0].EstoquePreMovimento.EstTipoMovimento.Descricao;

                relatorioEntradaModelDto.ValorFrete = string.Format("{0:C}", itens[0].EstoquePreMovimento.ValorFrete);
                relatorioEntradaModelDto.ValorTotal = string.Format("{0:C}", itens[0].EstoquePreMovimento.TotalDocumento);

                if (itens[0].EstoquePreMovimento.SisFornecedor != null)
                {
                    if (itens[0].EstoquePreMovimento.SisFornecedor.SisPessoa.FisicaJuridica == "F")
                    {
                        relatorioEntradaModelDto.CNPJFornecedor = itens[0].EstoquePreMovimento.SisFornecedor.SisPessoa.Cpf;
                    }
                    else
                    {
                        relatorioEntradaModelDto.CNPJFornecedor = itens[0].EstoquePreMovimento.SisFornecedor.SisPessoa.Cnpj;
                    }
                }

                relatorioEntradaModelDto.Estoque = itens[0].EstoquePreMovimento.Estoque.Descricao;
                relatorioEntradaModelDto.DataEntrada = string.Format("{0:dd/MM/yyyy}", itens[0].EstoquePreMovimento.Movimento);


                if (itens[0].EstoquePreMovimento.CreatorUserId != null && itens[0].EstoquePreMovimento.CreatorUserId != 0)
                {
                    var usuario = _usuarioRepository.Get((long)itens[0].EstoquePreMovimento.CreatorUserId);
                    if (usuario != null)
                    {
                        relatorioEntradaModelDto.UsuarioEntrada = string.Concat(usuario.Name, " ", usuario.Surname);
                    }
                }

                if (itens[0].EstoquePreMovimento.Paciente != null)
                {
                    relatorioEntradaModelDto.Paciente = itens[0].EstoquePreMovimento.Paciente.NomeCompleto;
                }

                if (itens[0].EstoquePreMovimento.UnidadeOrganizacional != null)
                {
                    relatorioEntradaModelDto.Setor = itens[0].EstoquePreMovimento.UnidadeOrganizacional.Descricao;
                }

                // relatorioEntradaModelDto.TotalItens = string.Format("{0:C}", itens.Sum(s => s.CustoUnitario * s.Quantidade));
            }

            foreach (var item in itens)
            {

                totalItens += (item.CustoUnitario * (item.Quantidade / item.ProdutoUnidade.Fator));

                if (item.Produto.IsLote || item.Produto.IsValidade)
                {
                    var lotesValidades = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                                                                   .Where(w => w.EstoquePreMovimentoItemId == item.Id)
                                                                                   .Include(i => i.LoteValidade)
                                                                                   .ToList();

                    foreach (var loteValidade in lotesValidades)
                    {
                        relatorioEntradaModelDto.Itens.Add(new RelatorioEntradaItemModalDto
                        {
                            CodigoProduto = item.Produto.Codigo
                                                                                            ,
                            DescricaoProduto = item.Produto.Descricao
                                                                                            ,
                            Lote = loteValidade.LoteValidade.Lote
                                                                                            ,
                            Validade = String.Format("{0:dd/MM/yyyy}", loteValidade.LoteValidade.Validade)
                        ,
                            Quantidade = string.Format("{0:0.0000}", (loteValidade.Quantidade / item.ProdutoUnidade.Fator))
                        ,
                            Sigla = item.ProdutoUnidade.Sigla
                        ,
                            ValorUnitario = item.CustoUnitario.ToString()
                        ,
                            ValorIPI = ((item.CustoUnitario * (item.Quantidade / item.ProdutoUnidade.Fator) / 100) * item.PerIPI).ToString(),
                            ValorTotal = (item.CustoUnitario * (item.Quantidade / item.ProdutoUnidade.Fator)).ToString()
                        });
                    }



                }
                else
                {
                    relatorioEntradaModelDto.Itens.Add(new RelatorioEntradaItemModalDto
                    {
                        CodigoProduto = item.Produto.Codigo
                        ,
                        DescricaoProduto = item.Produto.Descricao
                     ,
                        Quantidade = string.Format("{0:0.00}", (item.Quantidade / item.ProdutoUnidade.Fator))
                        ,
                        Sigla = item.ProdutoUnidade.Sigla
                        ,
                        ValorUnitario = item.CustoUnitario.ToString()
                        ,
                        ValorIPI = ((item.CustoUnitario * (item.Quantidade / item.ProdutoUnidade.Fator) / 100) * item.PerIPI).ToString()
                        ,
                        ValorTotal = (item.CustoUnitario * (item.Quantidade / item.ProdutoUnidade.Fator)).ToString()
                    });
                }
            }

            relatorioEntradaModelDto.TotalItens = string.Format("{0:C}", totalItens);


            return relatorioEntradaModelDto;
        }


        public RelatorioSolicitacaoSaidaModelDto ObterDadosRelatorioSolicitacao(long solicitacaoId)
        {
            RelatorioSolicitacaoSaidaModelDto relatorioSolicitacaoSaidaModelDto = new RelatorioSolicitacaoSaidaModelDto();
            relatorioSolicitacaoSaidaModelDto.Itens = new List<RelatorioSolicitacaoSaidaItemModelDto>();

            var loginInformations = Task.Run(() => _sessionAppService.GetCurrentLoginInformations()).Result;

            var itens = _estoqueSolcitacaiItemRepository.GetAll()
                                                          .Where(w => w.SolicitacaoId == solicitacaoId)
                                                          .Include(i => i.Produto)
                                                          .Include(i => i.ProdutoUnidade)
                                                          .Include(i => i.Solicitacao.Empresa)
                                                          .Include(i => i.Solicitacao.Estoque)
                                                          .Include(i => i.Solicitacao)
                                                          .Include(i => i.Solicitacao.Paciente)
                                                          .Include(i => i.Solicitacao.Paciente.SisPessoa)
                                                          .Include(i => i.Solicitacao.UnidadeOrganizacional)
                                                           .Include(i => i.Solicitacao.MedicoSolicitante)
                                                          .Include(i => i.Solicitacao.MedicoSolicitante.SisPessoa)
                                                          .ToList();

            relatorioSolicitacaoSaidaModelDto.Titulo = "Solicitação de Saída";

            relatorioSolicitacaoSaidaModelDto.NomeUsuario = string.Concat(loginInformations.User.Name, " ", loginInformations.User.Surname);
            relatorioSolicitacaoSaidaModelDto.DataHora = Convert.ToString(DateTime.Now);
            relatorioSolicitacaoSaidaModelDto.PreMovimentoId = solicitacaoId;

            if (itens.Count() > 0)
            {
                relatorioSolicitacaoSaidaModelDto.Documento = itens[0].Solicitacao.Documento;
                relatorioSolicitacaoSaidaModelDto.Estoque = itens[0].Solicitacao.Estoque.Descricao;
                relatorioSolicitacaoSaidaModelDto.DataHoraSolicitacao = string.Format("{0:dd/MM/yyyy}", itens[0].Solicitacao.Emissao);

                relatorioSolicitacaoSaidaModelDto.Paciente = (itens[0].Solicitacao.Paciente != null) ? itens[0].Solicitacao.Paciente.NomeCompleto : null;
                relatorioSolicitacaoSaidaModelDto.Medico = (itens[0].Solicitacao.MedicoSolicitante != null) ? itens[0].Solicitacao.MedicoSolicitante.NomeCompleto : null;
                relatorioSolicitacaoSaidaModelDto.Setor = (itens[0].Solicitacao.UnidadeOrganizacional != null) ? itens[0].Solicitacao.UnidadeOrganizacional.Descricao : null;

                if (itens[0].Solicitacao.CreatorUserId != null && itens[0].Solicitacao.CreatorUserId != 0)
                {
                    var usuario = _usuarioRepository.Get((long)itens[0].Solicitacao.CreatorUserId);
                    if (usuario != null)
                    {
                        relatorioSolicitacaoSaidaModelDto.UsuarioSolicitacao = string.Concat(usuario.Name, " ", usuario.Surname);
                    }
                }

            }


            foreach (var item in itens)
            {
                relatorioSolicitacaoSaidaModelDto.Itens.Add(new RelatorioSolicitacaoSaidaItemModelDto
                {
                    CodigoProduto = item.Produto.Codigo
                                                                                                        ,
                    DescricaoProduto = item.Produto.Descricao
                                                                                                        ,
                    Quantidade = item.Quantidade.ToString()
                                                                                                        ,
                    Sigla = item.ProdutoUnidade.Sigla
                });
            }


            return relatorioSolicitacaoSaidaModelDto;
        }

        #endregion

    }
}
