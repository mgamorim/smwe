﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class EstMovimentoBaixaAppService : SWMANAGERAppServiceBase, IEstMovimentoBaixaAppService
    {
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        private readonly IRepository<EstMovimentoBaixa, long> _estMovimentoBaixaRepository;
        private readonly IRepository<EstMovimentoBaixaItem, long> _estMovimentoBaixaItemRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IRepository<Unidade, long> _unidadeRepository;

        public EstMovimentoBaixaAppService(IRepository<EstoqueMovimento, long> estoqueMovimentoRepository
                                         , IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository
                                         , IRepository<EstMovimentoBaixa, long> estMovimentoBaixaRepository
                                         , IRepository<EstMovimentoBaixaItem, long> estMovimentoBaixaItemRepository
                                         , IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository
                                         , IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository
                                         , IRepository<Unidade, long> unidadeRepository)
        {
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _estMovimentoBaixaRepository = estMovimentoBaixaRepository;
            _estMovimentoBaixaItemRepository = estMovimentoBaixaItemRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _unidadeRepository = unidadeRepository;
        }


        public bool PossuiVales(long preMovimentoId)
        {
            var query = from movimento in _estoqueMovimentoRepository.GetAll()
                        join baixa in _estMovimentoBaixaRepository.GetAll()
                        on movimento.Id equals baixa.MovimentoBaixaPrincipalId
                        where movimento.EstoquePreMovimentoId == preMovimentoId
                        select movimento.Id;

            return query.Count()>0;

        }

        public bool PossuiNota(long preMovimentoId)
        {
            var query = from movimento in _estoqueMovimentoRepository.GetAll()
                        join baixa in _estMovimentoBaixaRepository.GetAll()
                        on movimento.Id equals baixa.MovimentoBaixaId
                        where movimento.EstoquePreMovimentoId == preMovimentoId
                        select movimento.Id;

            return query.Count() > 0;

        }

        public async Task<decimal> QuantidadeBaixaItemPendente(long movimentoItemId)
        {
            var query = _estMovimentoBaixaItemRepository.GetAll()
            .Where(w => w.EstoqueMovimentoItemId == movimentoItemId);

            var movimentoItem = _estoqueMovimentoItemRepository.Get(movimentoItemId);

            var quantidadeBaixada = query.ToList().Sum(s => s.Quantidade);

            return movimentoItem.Quantidade - quantidadeBaixada;
        }

        public async Task<EstMovimentoBaixaItemDto> ObterItemBaixa(long baixaItemid)
        {
            var movimentoBaixaItem = await _estMovimentoBaixaItemRepository.GetAsync(baixaItemid);

            return movimentoBaixaItem.MapTo<EstMovimentoBaixaItemDto>();
        }

        public bool PossuiItemConsignados(long preMovimentoId)
        {
            var query = from movimento in _estoqueMovimentoRepository.GetAll()
                        join baixaItem in _estMovimentoBaixaItemRepository.GetAll()
                        on movimento.Id equals baixaItem.EstoqueMovimentoBaixaId
                        where movimento.EstoquePreMovimentoId == preMovimentoId
                        select movimento.Id;

            return query.Count() > 0;
        }

        public async Task<decimal> Editar(MovimentoItemDto input)
        {
            try
            {
                decimal totalDocumento = 0;

                var movimentoItem = _estoqueMovimentoItemRepository.Get(input.Id);
                if(movimentoItem!=null)
                {
                    var unidade = _unidadeRepository.Get((long)movimentoItem.ProdutoUnidadeId);

                   var valorAntigo = movimentoItem.CustoUnitario * (movimentoItem.Quantidade / unidade.Fator);
                   var ValorAtual = input.CustoUnitario * (movimentoItem.Quantidade / unidade.Fator);
                    
                    movimentoItem.CustoUnitario = input.CustoUnitario;
                    movimentoItem.PerIPI = input.PerIPI;

                    await _estoqueMovimentoItemRepository.UpdateAsync(movimentoItem);

                    var movimento = _estoqueMovimentoRepository.Get(movimentoItem.MovimentoId);
                    if (movimento != null)
                    {
                        movimento.TotalDocumento += (ValorAtual - valorAntigo);
                        await _estoqueMovimentoRepository.UpdateAsync(movimento);
                    }

                    if (movimentoItem.EstoquePreMovimentoItemId != null)
                    {
                        var preMovimentoItem = _estoquePreMovimentoItemRepository.Get((long)movimentoItem.EstoquePreMovimentoItemId);

                        preMovimentoItem.CustoUnitario = input.CustoUnitario;
                        preMovimentoItem.PerIPI = input.PerIPI;

                        await _estoquePreMovimentoItemRepository.UpdateAsync(preMovimentoItem);

                        var preMovimento = _estoquePreMovimentoRepository.Get(preMovimentoItem.PreMovimentoId);
                        if(preMovimento!=null)
                        {
                            preMovimento.TotalDocumento += (ValorAtual - valorAntigo);
                            await _estoquePreMovimentoRepository.UpdateAsync(preMovimento);
                        }
                    }

                    var baixa = _estMovimentoBaixaRepository.GetAll()
                                .Where(w => w.MovimentoBaixaId == movimento.Id)
                                .FirstOrDefault();

                    if(baixa!=null)
                    {
                        var baixas = from movimentoBaixa in _estMovimentoBaixaRepository.GetAll()
                                     join movimentoDetalhe in _estoqueMovimentoRepository.GetAll()
                                     on movimentoBaixa.MovimentoBaixaId equals movimentoDetalhe.Id
                                     where movimentoBaixa.MovimentoBaixaPrincipalId == baixa.MovimentoBaixaPrincipalId
                                     select (movimentoDetalhe);


                         totalDocumento = baixas.ToList().Sum(s => s.TotalDocumento);


                        var movimentoNotaFiscal = _estoqueMovimentoRepository.Get(baixa.MovimentoBaixaPrincipalId);
                        movimentoNotaFiscal.TotalDocumento = totalDocumento;
                        await _estoqueMovimentoRepository.UpdateAsync(movimentoNotaFiscal);


                        var preMovimentoNotaFiscal = _estoquePreMovimentoRepository.Get(movimentoNotaFiscal.EstoquePreMovimentoId);
                        preMovimentoNotaFiscal.TotalDocumento = totalDocumento;
                        await _estoquePreMovimentoRepository.UpdateAsync(preMovimentoNotaFiscal);

                    }
                }


                return totalDocumento;


            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }
    }
}
