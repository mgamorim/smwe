﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Financeiro.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public interface IEstoquePreMovimentoAppService : IApplicationService
    {
        Task<PagedResultDto<MovimentoIndexDto>> Listar(ListarEstoquePreMovimentoInput input);

        Task<ListResultDto<EstoquePreMovimentoDto>> ListarTodos();

        DefaultReturn<EstoquePreMovimentoDto> CriarOuEditar(EstoquePreMovimentoDto input);

        Task Excluir(EstoquePreMovimentoDto input);

        Task<EstoquePreMovimentoDto> Obter(long id);

     //   Task<FileDto> ListarParaExcel(ListarEstoquePreMovimentoInput input);

        Task<PagedResultDto<MovimentoItemDto>> ListarItens(ListarEstoquePreMovimentoInput input);

        EstoquePreMovimentoDto CriarGetIdEntrada(EstoquePreMovimentoDto input);

        Task<FileDto> ListarParaExcel(ListarEstoquePreMovimentoInput input);

       Task< bool> PermiteConfirmarEntrada(EstoquePreMovimentoDto preMovimento);

        EstoquePreMovimentoDto CriarGetIdSaida(EstoquePreMovimentoDto input);

        DefaultReturn<EstoquePreMovimentoDto> CriarOuEditarSaida(EstoquePreMovimentoDto input);

        Task<PagedResultDto<MovimentoItemDto>> ListarItensSaida(ListarEstoquePreMovimentoInput input);

        Task<DefaultReturn<EstoqueTransferenciaProdutoDto>> TransferirProduto(EstoqueTransferenciaProdutoDto transferenciaProdutoDto);

        Task<PagedResultDto<EstoqueTransferenciaProdutoDto>> ListarTransferencia(ListarEstoquePreMovimentoInput input);

        Task<EstoqueTransferenciaProdutoDto> ObterTransferencia(long id);

        Task<PagedResultDto<MovimentoItemDto>> ListarItensTranferencia(ListarEstoquePreMovimentoInput input);

        Task ExcluirTransferencia(long transferenciaId);

        Task<PagedResultDto<MovimentoIndexDto>> ListarMovimentosPendente(ListarEstoquePreMovimentoInput input);

        DefaultReturn<EstoquePreMovimentoDto> CriarGetIdDevolucao(EstoquePreMovimentoDto input);
        Task<PagedResultDto<MovimentoIndexDto>> ListarDevolucoes(ListarEstoquePreMovimentoInput input);
        DefaultReturn<EstoquePreMovimentoDto> CriarOuEditarDevolucoes(EstoquePreMovimentoDto input);

        Task<PagedResultDto<MovimentoIndexDto>> ListarSolicitacoes(ListarEstoquePreMovimentoInput input);

        DefaultReturn<EstoquePreMovimentoDto> CriarOuEditarSolicitacao(EstoquePreMovimentoDto input);

        PagedResultDto<EstoquePreMovimentoItemSolicitacaoDto> ListarItensJson(string json);

        Task<PagedResultDto<MovimentoIndexDto>> ListarSolicitacaoesPendente(ListarEstoquePreMovimentoInput input);

        DefaultReturn<EstoquePreMovimentoDto> AtenderSolicitacao(EstoquePreMovimentoDto preMovimento);

        RelatorioEntradaModelDto ObterDadosRelatorioEntrada(long preMovimentoId);

        Task<EstoqueTransferenciaProdutoDto> ObterTransferenciaPorEntradaId(long id);

        RelatorioSolicitacaoSaidaModelDto ObterDadosRelatorioSolicitacao(long solicitacaoId);

        Task<DefaultReturn<DocumentoDto>> ExcluirSolicitacoesPrescritasNaoAtendidas(long prescricaoMedicaId);

        Task<DefaultReturn<EstoquePreMovimento>> ReAtivarSolicitacoDePrescricaoMedica(long prescricaoMedicaId);
    }
}
