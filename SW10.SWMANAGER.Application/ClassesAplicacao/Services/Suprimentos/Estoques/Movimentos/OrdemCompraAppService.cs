﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.UI;
using Abp.Domain.Uow;



using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class OrdemCompraAppService : SWMANAGERAppServiceBase, IOrdemCompraAppService
    {
        private readonly IRepository<OrdemCompra, long> _ordemCompraRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public OrdemCompraAppService(
         IRepository<OrdemCompra, long> ordemCompraRepository,
         IUnitOfWorkManager unitOfWorkManager
       
         )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _ordemCompraRepository = ordemCompraRepository;
        }
        
        public async Task<ListResultDto<OrdemCompraDto>> ListarTodos()
        {
            var contarOrdemCompra = 0;
            List<OrdemCompra> OrdemCompras;
            List<OrdemCompraDto> OrdemCompraDtos = new List<OrdemCompraDto>();
            try
            {
                var query = _ordemCompraRepository
                    .GetAll();

                contarOrdemCompra = await query
                    .CountAsync();

                OrdemCompras = await query
                    .AsNoTracking()
                    .ToListAsync();

                OrdemCompraDtos = OrdemCompras
                    .MapTo<List<OrdemCompraDto>>();

                return new ListResultDto<OrdemCompraDto> { Items = OrdemCompraDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<OrdemCompraDto> tipoMovimentoDto = new List<OrdemCompraDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

               
                var query = from p in _ordemCompraRepository.GetAll()
              .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
             m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
             m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
             )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = queryResultPage.ToList();

                //  int total = await query.CountAsync();

                int total = result.Count();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
