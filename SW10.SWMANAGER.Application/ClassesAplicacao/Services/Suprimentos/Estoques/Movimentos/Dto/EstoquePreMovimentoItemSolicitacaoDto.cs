﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    public class EstoquePreMovimentoItemSolicitacaoDto
    {
        public long Id { get; set; }
        public long PreMovimentoId { get; set; }
        public long ProdutoId { get; set; }
        public decimal Quantidade { get; set; }
        public string NumeroSerie { get; set; }
        public long? ProdutoUnidadeId { get; set; }

        public string Produto { get; set; }
        public string ProdutoUnidade { get; set; }
        public string CodigoProduto { get; set; }

        public long? IdGrid { get; set; }
        public decimal? QuantidadeAtendida { get; set; }
        public string LotesValidadesJson { get; set; }
        public string NumerosSerieJson { get; set; }
       // public bool NaoCalcularFator { get; set; }
    }
}
