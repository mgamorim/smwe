﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    public class NumeroSerieGridDto
    {
        public long Id { get; set; }
        public string NumeroSerie { get; set; }
        public long IdGridNumeroSerie { get; set; }
    }
}
