﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(EstoquePreMovimentoLoteValidade))]
    public class EstoquePreMovimentoLoteValidadeDto : CamposPadraoCRUDDto
    {
        public long EstoquePreMovimentoItemId { get; set; }
        public long LoteValidadeId { get; set; }
        public EstoquePreMovimentoItemDto EstoquePreMovimentoItem { get; set; }
        public LoteValidadeDto LoteValidade { get; set; }
        public long? LaboratorioId { get; set; }
        public string Lote { get; set; }
        public DateTime Validade { get; set; }
        public long ProdutoId { get; set; }
        public decimal Quantidade { get; set; }
        public bool EntradaConfirmada { get; set; }
        public string ProdutoDescricao { get; set; }
    }
}
