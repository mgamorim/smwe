﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(LoteValidade))]
    public class LoteValidadeDto : CamposPadraoCRUDDto
    {
        public string Lote { get; set; }
        public DateTime Validade { get; set; }
        public long ProdutoId { get; set; }
        public long? ProdutoLaboratorioId { get; set; }

        public ProdutoLaboratorioDto ProdutoLaboratorio { get; set; }
        public ProdutoDto Produto { get; set; }
    }
}
