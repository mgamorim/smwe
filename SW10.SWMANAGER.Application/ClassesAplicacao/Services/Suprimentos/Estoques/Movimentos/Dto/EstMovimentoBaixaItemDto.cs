﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(EstMovimentoBaixaItem))]
    public class EstMovimentoBaixaItemDto: CamposPadraoCRUDDto
    {
        public long EstoqueMovimentoBaixaId { get; set; }
        public long EstoqueMovimentoItemId { get; set; }
        public decimal Quantidade { get; set; }
       
    }
}
