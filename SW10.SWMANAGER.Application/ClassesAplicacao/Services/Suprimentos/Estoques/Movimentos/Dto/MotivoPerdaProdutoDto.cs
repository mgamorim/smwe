﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.MotivosPerdaProdutos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(MotivoPerdaProduto))]
    public class MotivoPerdaProdutoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
