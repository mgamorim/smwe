﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(EstoquePreMovimentoItem))]
    public class EstoquePreMovimentoItemDto :CamposPadraoCRUDDto
    {
        public long PreMovimentoId { get; set; }
        public long ProdutoId { get; set; }
        public decimal Quantidade { get; set; }
        public string NumeroSerie { get; set; }
        public decimal CustoUnitario { get; set; }
        public long? ProdutoUnidadeId { get; set; }
        public string CodigoBarra { get; set; }
        public decimal PerIPI { get; set; }
        public decimal ValorIPI { get; set; }

        public long? LoteValidadeId { get; set; }
        public long? LaboratorioId { get; set; }
        public string Lote { get; set; }
        public DateTime? Validade { get; set; }
        public EstoqueLaboratorio Laboratorio { get; set; }
        public LoteValidade LoteValidade { get; set; }

        public virtual ProdutoDto Produto { get; set; }
        public virtual EstoquePreMovimentoDto EstoquePreMovimento { get; set; }
        public virtual UnidadeDto ProdutoUnidade { get; set; }


        public long EstoquePreMovimentoLoteValidadeId { get; set; }
        public long EstoqueId { get; set; }
        public long? IdGrid { get; set; }
        public decimal? QuantidadeAtendida { get; set; }

        public string ItensLoteValidade { get; set; }
        public string ItensNumeroSerie { get; set; }

        public List<EstoquePreMovimentoLoteValidadeDto> PreMovimentoLotesValidades { get; set; }

        public List<LoteValidadeGridDto> ItensDtos { get; set; }
    }
}
