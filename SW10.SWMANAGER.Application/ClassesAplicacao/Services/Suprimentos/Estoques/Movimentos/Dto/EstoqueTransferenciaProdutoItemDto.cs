﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    public class EstoqueTransferenciaProdutoItemDto : CamposPadraoCRUDDto
    {
        public long PreMovimentoEntradaItemId { get; set; }
        public long PreMovimentoSaidaItemId { get; set; }
        public long EstoqueTransferenciaProdutoID { get; set; }
    }
}
