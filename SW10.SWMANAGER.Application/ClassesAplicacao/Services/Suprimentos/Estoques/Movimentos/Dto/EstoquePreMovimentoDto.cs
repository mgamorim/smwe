﻿using Abp.Authorization.Users;
using Abp.AutoMapper;
using NFe.Classes.Informacoes.Detalhe;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.CentrosCustos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Globais.HorasDias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposDocumento.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(EstoquePreMovimento))]
    public class EstoquePreMovimentoDto : CamposPadraoCRUDDto
    {
        public string Documento { get; set; }
        public DateTime Movimento { get; set; }
        public long? EstoqueId { get; set; }
        public long TipoMovimentoId { get; set; }
        public long? FornecedorId { get; set; }
        public long? EmpresaId { get; set; }
        public decimal Quantidade { get; set; }
        public long PreMovimentoEstadoId { get; set; }
        public decimal TotalProduto { get; set; }
        public decimal Frete { get; set; }
        public decimal AcrescimoDecrescimo { get; set; }
        public decimal TotalDocumento { get; set; }
        public long? CentroCustoId { get; set; }
        public bool IsEntrada { get; set; }
        public long? MotivoPerdaProdutoId { get; set; }

        public bool PossuiLoteValidade { get; set; }

        public EstoqueDto Estoque { get; set; }
        public EstoqueTipoMovimentoDto TipoMovimento { get; set; }
        public SisFornecedorDto Fornecedor { get; set; }
        public SisFornecedorDto Frete_Fornecedor { get; set; }
        public EmpresaDto Empresa { get; set; }
        public EstoquePreMovimentoEstadoDto PreMovimentoEstado { get; set; }
        public CentroCustoDto CentroCusto { get; set; }

        //  public virtual ICollection<EstoquePreMovimentoItemDto> PreMovimentosItem { get; set; }



        public bool Contabiliza { get; set; }
        public bool Consiginado { get; set; }
        public bool AplicacaoDireta { get; set; }
        public bool EntragaProduto { get; set; }
        public string Serie { get; set; }
        public long? TipoDocumentoId { get; set; }
        public long? OrdemId { get; set; }

        public long? EstTipoMovimentoId { get; set; }
        public long? EstTipoOperacaoId { get; set; }


        // public TipoDocumentoDto TipoDocumento { get; set; }
        public OrdemCompraDto OrdemCompra { get; set; }


        public long? CFOPId { get; set; }
        public CfopDto CFOP { get; set; }
        public decimal? ICMSPer { get; set; }
        public decimal? ValorICMS { get; set; }
        public decimal? DescontoPer { get; set; }

        public decimal ValorDesconto { get; set; }
        public decimal ValorAcrescimo { get; set; }


        public long? TipoFreteId { get; set; }
        public TipoFreteDto TipoFrete { get; set; }

        public bool InclusoNota { get; set; }
        public decimal? FretePer { get; set; }
        public decimal? ValorFrete { get; set; }
        public long? Frete_FornecedorId { get; set; }
        public FornecedorDto Frete_Forncedor { get; set; }

        public DateTime Emissao { get; set; }

        public bool PermiteConfirmacaoEntrada { get; set; }

        public String NomUsuario { get; set; }


        public long? PacienteId { get; set; }

        public PacienteDto Paciente { get; set; }

        public long? MedicoSolcitanteId { get; set; }

        public MedicoDto MedicoSolicitante { get; set; }

        public long? UnidadeOrganizacionalId { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }


        public string Observacao { get; set; }

        public long? AtendimentoId { get; set; }

        public AtendimentoDto Atendimento { get; set; }

        public bool IsSaidaPaciente { get; set; }
        public bool IsSaidaSetor { get; set; }

        public int SaidaPorId { get; set; }

        public long? GrupoOperacaoId { get; set; }

        public string Itens { get; set; }

        public long ProdutoUnidadeId { get; set; }

        public long? TipoAtendimentoId { get; set; }
        public string TipoAtendimento { get; set; }

        public List<EstoqueImportacaoProdutoDto> ImportacaoProdutos { get; set; }
        public string CNPJNota { get; set; }
        public string NFeItens { get; set; }

        public string LancamentosJson { get; set; }
        public string RateioJson { get; set; }

        public DateTime? HoraPrescrita { get; set; }

        public long? HoraDiaId { get; set; }
        public HoraDiaDto HoraDiaDto { get; set; }

        public long? PrescricaoMedicaId { get; set; }
        public PrescricaoMedicaDto PrescricaoMedicaDto { get; set; }

    }
}
