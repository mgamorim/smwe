﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
   // [AutoMap(typeof(EstoqueTipoMovimento))]
    public class EstoqueTipoMovimentoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
