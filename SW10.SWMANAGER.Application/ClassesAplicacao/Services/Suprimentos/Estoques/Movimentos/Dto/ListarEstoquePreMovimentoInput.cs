﻿using Abp.Runtime.Validation;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    public class ListarEstoquePreMovimentoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filtro { get; set; }
        public long? FornecedorId { get; set; }
        public DateTime? PeridoDe { get; set; }
        public DateTime? PeridoAte { get; set; }
        public bool EntradaConfirmada { get; set; }
        public long? TipoMovimentoId { get; set; }
        public long? TipoOperacaoId { get; set; }
        public long? MovimentoId { get; set; }
        public long? EstoqueId { get; set; }

        [System.ComponentModel.DefaultValue(true)]
        public bool IsEntrada { get; set; }

        public void Normalize()
        {
            Sorting = "Documento";
        }
    }
}
