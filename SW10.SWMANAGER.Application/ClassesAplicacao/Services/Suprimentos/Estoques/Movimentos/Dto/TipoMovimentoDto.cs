﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto
{
    [AutoMap(typeof(TipoMovimento))]
    public class TipoMovimentoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
        public bool IsEntrada { get; set; }
    }
}
