﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class EstoquePreMovimentoItemAppService : SWMANAGERAppServiceBase, IEstoquePreMovimentoItemAppService
    {
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<LoteValidade, long> _loteValidadeRepository;
        private readonly IEstoqueLoteValidadeAppService _estoqueLoteValidadeAppService;
        private readonly IRepository<Produto, long> _produtoRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        private readonly IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        private readonly IRepository<ProdutoUnidade, long> _produtoUnidadeRepository;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly IRepository<EstoqueTransferenciaProdutoItem, long> _estoqueTransferenciaProdutoItemRepository;
        private readonly IRepository<EstoqueTransferenciaProduto, long> _estoqueTransferenciaProdutoRepository;
        private readonly IProdutoSaldoAppService _produtoSaldoAppService;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepository;
        private readonly IRepository<Unidade, long> _unidadeRepository;
        private readonly IRepository<EstoqueLaboratorio, long> _produtoLaboratorioRepositorio;
        private readonly IRepository<EstoqueSolicitacaoItem, long> _estoqueSolicitacaoItemRepositorio;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;


        public EstoquePreMovimentoItemAppService(
         IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
         IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
         IRepository<EstoqueMovimento, long> estoqueMovimentoRepository,
         IUnitOfWorkManager unitOfWorkManager,
         IRepository<LoteValidade, long> loteValidadeRepository,
         IEstoqueLoteValidadeAppService estoqueLoteValidadeAppService,
         IRepository<Produto, long> produtoRepository,
         IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository,
         IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository,
         IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
         IRepository<ProdutoUnidade, long> produtoUnidadeRepository,
         IUnidadeAppService unidadeAppService,
         IRepository<EstoqueTransferenciaProdutoItem, long> estoqueTransferenciaProdutoItemRepository,
         IRepository<EstoqueTransferenciaProduto, long> estoqueTransferenciaProdutoRepository,
         IProdutoSaldoAppService produtoSaldoAppService,
         IRepository<ProdutoSaldo, long> produtoSaldoRepository,
         IRepository<Unidade, long> unidadeRepository,
         IRepository<EstoqueLaboratorio, long> produtoLaboratorioRepositorio,
         IRepository<EstoqueSolicitacaoItem, long> estoqueSolicitacaoItemRepositorio,
         IRepository<EstoqueGrupo, long> estoqueGrupoRepository
         )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _loteValidadeRepository = loteValidadeRepository;
            _estoqueLoteValidadeAppService = estoqueLoteValidadeAppService;
            _produtoRepository = produtoRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _produtoUnidadeRepository = produtoUnidadeRepository;
            _unidadeAppService = unidadeAppService;
            _estoqueTransferenciaProdutoItemRepository = estoqueTransferenciaProdutoItemRepository;
            _estoqueTransferenciaProdutoRepository = estoqueTransferenciaProdutoRepository;
            _produtoSaldoAppService = produtoSaldoAppService;
            _produtoSaldoRepository = produtoSaldoRepository;
            _unidadeRepository = unidadeRepository;
            _produtoLaboratorioRepositorio = produtoLaboratorioRepositorio;
            _estoqueSolicitacaoItemRepositorio = estoqueSolicitacaoItemRepositorio;
            _estoqueGrupoRepository = estoqueGrupoRepository;
        }

        public async Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditarSaida(EstoquePreMovimentoItemDto input)
        {
            var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoItemDto>();

            try
            {
                var estoquePreMovimentoItem = new EstoquePreMovimentoItem();


                PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                          , _estoquePreMovimentoLoteValidadeRepository
                                                                                                          , _estoquePreMovimentoItemRepository
                                                                                                          , _estoqueMovimentoRepository
                                                                                                          , _estoqueMovimentoItemRepository
                                                                                                          , _estoqueMovimentoLoteValidadeRepository
                                                                                                          , _loteValidadeRepository
                                                                                                          , _estoquePreMovimentoRepository
                                                                                                          , _produtoRepository
                                                                                                          , _produtoUnidadeRepository
                                                                                                          , _estoqueGrupoRepository);
                var produto = _produtoRepository.Get(input.ProdutoId);
                input.Produto = produto.MapTo<ProdutoDto>();

                var preMovimento = _estoquePreMovimentoRepository.Get(input.PreMovimentoId);

                var validarDataVencimeneto = preMovimento.EstTipoMovimentoId != (long)EnumTipoMovimento.Perda_Saida;

                input.Quantidade = _unidadeAppService.ObterQuantidadeReferencia((long)input.ProdutoUnidadeId, input.Quantidade);


                _retornoPadrao.Errors = preMovimentoValidacaoService.ValidarItemSaida(input, (produto != null && (produto.IsValidade || produto.IsLote)), validarDataVencimeneto);

                var erroESt0003 = _retornoPadrao.Errors.Where(w => w.CodigoErro == "EST0003").FirstOrDefault();
                if (erroESt0003 != null)
                {
                    _retornoPadrao.Warnings = new List<ErroDto>();
                    _retornoPadrao.Warnings.Add(erroESt0003);
                    _retornoPadrao.Errors.Remove(erroESt0003);
                }

                if (_retornoPadrao.Errors.Count() == 0)
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {

                        estoquePreMovimentoItem = MapPreMovimentoItem(input);
                        if (input.Id.Equals(0))
                        {
                            input.Id = await _estoquePreMovimentoItemRepository.InsertAndGetIdAsync(estoquePreMovimentoItem);
                        }
                        else
                        {
                            await _estoquePreMovimentoItemRepository.UpdateAsync(estoquePreMovimentoItem);
                        }


                        if (produto != null && (produto.IsValidade || produto.IsLote))
                        {


                            var estoquePreMovimentoLoteValidadeDto = new EstoquePreMovimentoLoteValidadeDto
                            {
                                Id = input.EstoquePreMovimentoLoteValidadeId
                                                                                                            ,
                                LaboratorioId = input.LaboratorioId != null ? (long)input.LaboratorioId : 0
                                                                                                            ,
                                Lote = input.Lote
                                                                                                            ,
                                Validade = input.Validade != null ? (DateTime)input.Validade : DateTime.MinValue
                                                                                                            ,
                                ProdutoId = input.ProdutoId
                                                                                                            ,
                                EstoquePreMovimentoItemId = input.Id
                                                                                                            ,
                                Quantidade = input.Quantidade
                                                                                                            ,
                                LoteValidadeId = input.LoteValidadeId != null ? (long)input.LoteValidadeId : 0
                            };

                            var retorno = await _estoqueLoteValidadeAppService.CriarOuEditar(estoquePreMovimentoLoteValidadeDto);

                            _retornoPadrao.Errors.AddRange(retorno.Errors);
                        }

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();

                        if (produto != null && !(produto.IsValidade || produto.IsLote))
                        {
                            await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);
                        }

                        var query = await Obter(input.Id);
                        var newObj = MapPreMovimentoItem(query);

                        var estoquePreMovimentoItemDto = MapPreMovimentoItem(newObj);

                        _retornoPadrao.ReturnObject = estoquePreMovimentoItemDto;
                    }
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }

        public async Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditar(EstoquePreMovimentoItemDto input)
        {
            var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoItemDto>();

            try
            {
                PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                         , _estoquePreMovimentoLoteValidadeRepository
                                                                                                         , _estoquePreMovimentoItemRepository
                                                                                                         , _estoqueMovimentoRepository
                                                                                                         , _estoqueMovimentoItemRepository
                                                                                                         , _estoqueMovimentoLoteValidadeRepository
                                                                                                         , _loteValidadeRepository
                                                                                                         , _estoquePreMovimentoRepository
                                                                                                         , _produtoRepository
                                                                                                         , _produtoUnidadeRepository
                                                                                                         , _estoqueGrupoRepository);
                _retornoPadrao.Errors = preMovimentoValidacaoService.ValidarItem(input);

                if (_retornoPadrao.Errors.Count() == 0)
                {
                    var estoquePreMovimentoItem = new EstoquePreMovimentoItem();
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {


                        estoquePreMovimentoItem = MapPreMovimentoItem(input);//.MapTo<EstoquePreMovimentoItem>();
                        estoquePreMovimentoItem.Quantidade = _unidadeAppService.ObterQuantidadeReferencia((long)estoquePreMovimentoItem.ProdutoUnidadeId, estoquePreMovimentoItem.Quantidade);
                        if (input.Id.Equals(0))
                        {
                            input.Id = await _estoquePreMovimentoItemRepository.InsertAndGetIdAsync(estoquePreMovimentoItem);
                        }
                        else
                        {
                            await _estoquePreMovimentoItemRepository.UpdateAsync(estoquePreMovimentoItem);
                        }
                        var query = await Obter(input.Id);
                        var newObj = MapPreMovimentoItem(query);//.MapTo<EstoquePreMovimentoItem>();

                        var estoquePreMovimentoItemDto = MapPreMovimentoItem(newObj);//.MapTo<EstoquePreMovimentoItemDto>();



                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();

                        //  await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);

                        _retornoPadrao.ReturnObject = estoquePreMovimentoItemDto;
                    }

                    var produto = _produtoRepository.Get(estoquePreMovimentoItem.ProdutoId);

                    if (produto != null && !(produto.IsValidade || produto.IsLote))
                    {
                        await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);
                    }
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;

        }

        public async Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditarTransferencia(EstoquePreMovimentoItemDto input, long transferenciaProdutoId, long transferenciaProdutoItemId)
        {
            var transferencia = _estoqueTransferenciaProdutoRepository.Get(transferenciaProdutoId);
            var itemTransferencia = new EstoqueTransferenciaProdutoItem();


            EstoquePreMovimentoItemDto entradaItemDto = new EstoquePreMovimentoItemDto();

            entradaItemDto.EstoquePreMovimentoLoteValidadeId = input.EstoquePreMovimentoLoteValidadeId;
            entradaItemDto.NumeroSerie = input.NumeroSerie;
            entradaItemDto.PreMovimentoId = transferencia.PreMovimentoEntradaId;
            entradaItemDto.ProdutoId = input.ProdutoId;
            entradaItemDto.ProdutoUnidadeId = input.ProdutoUnidadeId;
            entradaItemDto.Quantidade = input.Quantidade;



            if (transferenciaProdutoItemId != 0)
            {
                itemTransferencia = _estoqueTransferenciaProdutoItemRepository.Get(transferenciaProdutoItemId);
                entradaItemDto.Id = itemTransferencia.PreMovimentoEntradaItemId;
                input.Id = itemTransferencia.PreMovimentoSaidaItemId;
            }


            input.PreMovimentoId = transferencia.PreMovimentoSaidaId;

            var retornoSaida = await CriarOuEditarSaida(input);


            var retornoEntrada = await CriarOuEditar(entradaItemDto);


            if (input.Produto != null && (input.Produto.IsValidade || input.Produto.IsLote))
            {


                var estoquePreMovimentoLoteValidadeDto = new EstoquePreMovimentoLoteValidadeDto
                {
                    Id = input.EstoquePreMovimentoLoteValidadeId
                                                                                                ,
                    LaboratorioId = input.LaboratorioId != null ? (long)input.LaboratorioId : 0
                                                                                                ,
                    Lote = input.Lote
                                                                                                ,
                    Validade = input.Validade != null ? (DateTime)input.Validade : DateTime.MinValue
                                                                                                ,
                    ProdutoId = input.ProdutoId
                                                                                                ,
                    EstoquePreMovimentoItemId = entradaItemDto.Id
                                                                                                ,
                    Quantidade = input.Quantidade
                                                                                                ,
                    LoteValidadeId = input.LoteValidadeId != null ? (long)input.LoteValidadeId : 0
                };

                var retornoLoteValidade = await _estoqueLoteValidadeAppService.CriarOuEditar(estoquePreMovimentoLoteValidadeDto);

                retornoEntrada.Errors.AddRange(retornoLoteValidade.Errors);


            }

            retornoEntrada.Errors.AddRange(retornoSaida.Errors);

            if (retornoEntrada.Errors.Count == 0)
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (transferenciaProdutoItemId == 0)
                    {
                        EstoqueTransferenciaProdutoItem estoqueTransferenciaProdutoItem = new EstoqueTransferenciaProdutoItem();

                        estoqueTransferenciaProdutoItem.EstoqueTransferenciaProdutoID = transferenciaProdutoId;
                        estoqueTransferenciaProdutoItem.PreMovimentoSaidaItemId = retornoSaida.ReturnObject.Id;
                        estoqueTransferenciaProdutoItem.PreMovimentoEntradaItemId = retornoEntrada.ReturnObject.Id;

                        await _estoqueTransferenciaProdutoItemRepository.InsertAndGetIdAsync(estoqueTransferenciaProdutoItem);
                    }
                    else
                    {
                        EstoqueTransferenciaProdutoItem estoqueTransferenciaProdutoItem = _estoqueTransferenciaProdutoItemRepository.Get(transferenciaProdutoItemId);

                        estoqueTransferenciaProdutoItem.EstoqueTransferenciaProdutoID = transferenciaProdutoId;
                        estoqueTransferenciaProdutoItem.PreMovimentoSaidaItemId = retornoSaida.ReturnObject.Id;
                        estoqueTransferenciaProdutoItem.PreMovimentoEntradaItemId = retornoEntrada.ReturnObject.Id;

                        await _estoqueTransferenciaProdutoItemRepository.UpdateAsync(estoqueTransferenciaProdutoItem);
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();

                }
            }

            return retornoEntrada;
        }

        public async Task<EstoquePreMovimentoItemDto> Obter(long id)
        {
            try
            {
                var query = _estoquePreMovimentoItemRepository.GetAll()
                    .Include(i => i.Produto)
                    .Where(w => w.Id == id).FirstOrDefault();

                var entrada = MapPreMovimentoItem(query);//.MapTo<EstoquePreMovimentoItemDto>();


                return entrada;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task Editar(EstoquePreMovimentoItemDto input)
        {
            try
            {
                await _estoquePreMovimentoItemRepository.UpdateAsync(MapPreMovimentoItem(input));// MapTo<EstoquePreMovimentoItem>());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(long id)
        {
            try
            {
                var estoquePreMovimentoItem = _estoquePreMovimentoItemRepository.Get(id);

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var itemLotesValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                            .Where(w => w.EstoquePreMovimentoItemId == id).ToList();

                    foreach (var item in itemLotesValidade)
                    {
                        await _estoqueLoteValidadeAppService.Excluir(EstoqueLoteValidadeAppService.MapLoteValidade(item));
                    }


                    await _estoquePreMovimentoItemRepository.DeleteAsync(id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();

                }

                await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public Task<PagedResultDto<EstoquePreMovimentoItemDto>> Listar(long Id)
        {
            throw new NotImplementedException();
        }

        public async Task<PagedResultDto<EstoquePreMovimentoItemDto>> ListarPorMovimentacaoLoteValidade(ListarEstoquePreMovimentoInput input)
        {
            try
            {
                var contarPreMovimentos = 0;
                Int64 _filtro;
                List<EstoquePreMovimentoItemDto> PreMovimentoItens = null;

                if (Int64.TryParse(input.Filtro, out _filtro))
                {
                    var query = _estoquePreMovimentoItemRepository
                        .GetAll().Where(w => w.PreMovimentoId == _filtro
                                          && (w.Produto.IsValidade || w.Produto.IsLote))
                        .Include(i => i.Produto);

                    contarPreMovimentos = await query.CountAsync();

                    var preMovimentoItens = await query.ToListAsync();

                    

                    PreMovimentoItens = MapPreMovimentoItem(preMovimentoItens);//.MapTo<List<EstoquePreMovimentoItemDto>>();
                    foreach (var item in PreMovimentoItens)
                    {
                        var qtdAtendida = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                                  .Where(w => w.EstoquePreMovimentoItemId == item.Id)
                                                  .ToList()
                                                  .Sum(s => s.Quantidade);


                        item.QuantidadeAtendida = qtdAtendida;
                    }
                }

                return new PagedResultDto<EstoquePreMovimentoItemDto>(
                    contarPreMovimentos,
                    PreMovimentoItens
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<EstoquePreMovimentoItemDto>> ListarPorMovimentacaoLoteValidadeProduto(ListarEstoquePreMovimentoInput input)
        {
            try
            {
                var contarPreMovimentos = 0;
                List<EstoquePreMovimentoItemDto> PreMovimentoItens = null;

                long preMovimentoId = long.Parse(input.Filtro);

                var query = _estoquePreMovimentoItemRepository
                    .GetAll().Where(w => w.Id == preMovimentoId  // preMovimentoId)
                                                                 //&& w.Produto.Id == long.Parse(produtoId )
                                     );

                contarPreMovimentos = await query.CountAsync();
                var preMovimentoItens = await query.ToListAsync();
                PreMovimentoItens = MapPreMovimentoItem(preMovimentoItens);//.MapTo<List<EstoquePreMovimentoItemDto>>();


                return new PagedResultDto<EstoquePreMovimentoItemDto>(
                    contarPreMovimentos,
                    PreMovimentoItens
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public static EstoquePreMovimentoItemDto MapPreMovimentoItem(EstoquePreMovimentoItem preMovimentoItem)
        {
            EstoquePreMovimentoItemDto preMovimentoItemDto = new EstoquePreMovimentoItemDto();

            preMovimentoItemDto.CreationTime = preMovimentoItem.CreationTime;
            preMovimentoItemDto.CreatorUserId = preMovimentoItem.CreatorUserId;
            preMovimentoItemDto.CustoUnitario = preMovimentoItem.CustoUnitario;
            preMovimentoItemDto.DeleterUserId = preMovimentoItem.DeleterUserId;
            preMovimentoItemDto.DeletionTime = preMovimentoItem.DeletionTime;
            preMovimentoItemDto.Id = preMovimentoItem.Id;
            preMovimentoItemDto.IsDeleted = preMovimentoItem.IsDeleted;
            preMovimentoItemDto.IsSistema = preMovimentoItem.IsSistema;
            preMovimentoItemDto.NumeroSerie = preMovimentoItem.NumeroSerie;
            preMovimentoItemDto.PreMovimentoId = preMovimentoItem.PreMovimentoId;
            preMovimentoItemDto.ProdutoId = preMovimentoItem.ProdutoId;
            preMovimentoItemDto.ProdutoUnidadeId = preMovimentoItem.ProdutoUnidadeId;
            preMovimentoItemDto.Quantidade = preMovimentoItem.Quantidade;
            preMovimentoItemDto.PerIPI = preMovimentoItem.PerIPI;

            if (preMovimentoItem.Produto != null)
            {
                preMovimentoItemDto.Produto = new Cadastros.Produtos.Dto.ProdutoDto { Id = preMovimentoItem.Produto.Id, IsValidade = preMovimentoItem.Produto.IsValidade, Descricao = preMovimentoItem.Produto.Descricao };
            }
            return preMovimentoItemDto;
        }

        public static EstoquePreMovimentoItem MapPreMovimentoItem(EstoquePreMovimentoItemDto preMovimentoItemDto)
        {
            EstoquePreMovimentoItem preMovimentoItem = new EstoquePreMovimentoItem();

            preMovimentoItem.CreationTime = preMovimentoItemDto.CreationTime;
            preMovimentoItem.CreatorUserId = preMovimentoItemDto.CreatorUserId;
            preMovimentoItem.CustoUnitario = preMovimentoItemDto.CustoUnitario;
            preMovimentoItem.DeleterUserId = preMovimentoItemDto.DeleterUserId;
            preMovimentoItem.DeletionTime = preMovimentoItemDto.DeletionTime;
            preMovimentoItem.Id = preMovimentoItemDto.Id;
            preMovimentoItem.IsDeleted = preMovimentoItemDto.IsDeleted;
            preMovimentoItem.IsSistema = preMovimentoItemDto.IsSistema;
            preMovimentoItem.NumeroSerie = preMovimentoItemDto.NumeroSerie;
            preMovimentoItem.PreMovimentoId = preMovimentoItemDto.PreMovimentoId;
            preMovimentoItem.ProdutoId = preMovimentoItemDto.ProdutoId;
            preMovimentoItem.ProdutoUnidadeId = preMovimentoItemDto.ProdutoUnidadeId;
            preMovimentoItem.Quantidade = preMovimentoItemDto.Quantidade;
            preMovimentoItem.PerIPI = preMovimentoItemDto.PerIPI;

            return preMovimentoItem;
        }

        public static List<EstoquePreMovimentoItem> MapPreMovimentoItem(List<EstoquePreMovimentoItemDto> preMovimentoItensDto)
        {
            List<EstoquePreMovimentoItem> preMovimentoItens = new List<EstoquePreMovimentoItem>();

            foreach (var item in preMovimentoItensDto)
            {
                preMovimentoItens.Add(MapPreMovimentoItem(item));
            }

            return preMovimentoItens;
        }

        public static List<EstoquePreMovimentoItemDto> MapPreMovimentoItem(List<EstoquePreMovimentoItem> preMovimentoItens)
        {
            List<EstoquePreMovimentoItemDto> preMovimentoItensDto = new List<EstoquePreMovimentoItemDto>();

            foreach (var item in preMovimentoItens)
            {
                preMovimentoItensDto.Add(MapPreMovimentoItem(item));
            }

            return preMovimentoItensDto;
        }

        public async Task<EstoqueTransferenciaProdutoItemDto> ObterTransferenciaItem(long id)
        {
            try
            {
                var query = _estoqueTransferenciaProdutoItemRepository.GetAll().Where(w => w.Id == id).FirstOrDefault();

                var transferenciaItemDto = new EstoqueTransferenciaProdutoItemDto();

                transferenciaItemDto.Id = query.Id;
                transferenciaItemDto.EstoqueTransferenciaProdutoID = query.EstoqueTransferenciaProdutoID;
                transferenciaItemDto.PreMovimentoEntradaItemId = query.PreMovimentoEntradaItemId;
                transferenciaItemDto.PreMovimentoSaidaItemId = query.PreMovimentoSaidaItemId;

                return transferenciaItemDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task ExcluirItemTransferencia(long id)
        {
            try
            {
                var transferenciaItem = _estoqueTransferenciaProdutoItemRepository.Get(id);
                if (transferenciaItem != null)
                {

                    EstoquePreMovimentoItem saidaItem = new EstoquePreMovimentoItem();
                    EstoquePreMovimentoItem entradaItem = new EstoquePreMovimentoItem();

                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        var saidaItemId = transferenciaItem.PreMovimentoSaidaItemId;
                        var entradaItemId = transferenciaItem.PreMovimentoEntradaItemId;

                        saidaItem = _estoquePreMovimentoItemRepository.Get(saidaItemId);
                        entradaItem = _estoquePreMovimentoItemRepository.Get(entradaItemId);

                        await _estoquePreMovimentoItemRepository.DeleteAsync(saidaItemId);
                        await _estoquePreMovimentoItemRepository.DeleteAsync(entradaItemId);

                        await _estoqueTransferenciaProdutoItemRepository.DeleteAsync(id);

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }


                    await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(saidaItem);
                    await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(entradaItem);

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<List<string>> ObterNumerosSerieProduto(long estoqueId, long produtoId)
        {
            var query = from mov in _estoqueMovimentoRepository.GetAll()
                        join item in _estoqueMovimentoItemRepository.GetAll()
                        on mov.Id equals item.MovimentoId
                        into movjoin
                        from joinedmov in movjoin.DefaultIfEmpty()
                        where mov.EstoqueId == estoqueId
                           && joinedmov.ProdutoId == produtoId
                           && !String.IsNullOrEmpty(joinedmov.NumeroSerie)

                        //   group joinedmov by joinedmov.NumeroSerie  into g

                        select new
                        {
                            NumeroSerie = joinedmov.NumeroSerie,

                            Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade),

                            //(((joinedmov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade)
                        };


            var numerosSeries = query.GroupBy(g => g.NumeroSerie).Select(s => new { Quantidade = s.Sum(su => su.Quantidade), numerosSeries = s.Select(s2 => s2.NumeroSerie) });


            return query.Where(w => w.Quantidade > 0).Select(s => s.NumeroSerie).ToList();

        }




        public async Task<DefaultReturn<EstoquePreMovimentoItemDto>> CriarOuEditarDevolucao(EstoquePreMovimentoItemDto input)
        {
            var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoItemDto>();

            try
            {
                var estoquePreMovimentoItem = new EstoquePreMovimentoItem();


                PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                          , _estoquePreMovimentoLoteValidadeRepository
                                                                                                          , _estoquePreMovimentoItemRepository
                                                                                                          , _estoqueMovimentoRepository
                                                                                                          , _estoqueMovimentoItemRepository
                                                                                                          , _estoqueMovimentoLoteValidadeRepository
                                                                                                          , _loteValidadeRepository
                                                                                                          , _estoquePreMovimentoRepository
                                                                                                          , _produtoRepository
                                                                                                          , _produtoUnidadeRepository
                                                                                                          , _estoqueGrupoRepository);
                var produto = _produtoRepository.Get(input.ProdutoId);
                input.Produto = produto.MapTo<ProdutoDto>();

                var preMovimento = _estoquePreMovimentoRepository.Get(input.PreMovimentoId);
                input.EstoquePreMovimento = EstoquePreMovimentoAppService.MapPreMovimento(preMovimento);
                var validarDataVencimeneto = preMovimento.EstTipoMovimentoId != 4;

                input.Quantidade = _unidadeAppService.ObterQuantidadeReferencia((long)input.ProdutoUnidadeId, input.Quantidade);


                _retornoPadrao.Errors = preMovimentoValidacaoService.ValidarItemDevolucao(input, (produto != null && (produto.IsValidade || produto.IsLote)), validarDataVencimeneto);

                //var erroESt0003 = _retornoPadrao.Errors.Where(w => w.CodigoErro == "EST0003").FirstOrDefault();
                //if (erroESt0003 != null)
                //{
                //    _retornoPadrao.Warnings = new List<ErroDto>();
                //    _retornoPadrao.Warnings.Add(erroESt0003);
                //    _retornoPadrao.Errors.Remove(erroESt0003);
                //}

                if (_retornoPadrao.Errors.Count() == 0)
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {

                        estoquePreMovimentoItem = MapPreMovimentoItem(input);
                        if (input.Id.Equals(0))
                        {
                            input.Id = await _estoquePreMovimentoItemRepository.InsertAndGetIdAsync(estoquePreMovimentoItem);
                        }
                        else
                        {
                            await _estoquePreMovimentoItemRepository.UpdateAsync(estoquePreMovimentoItem);
                        }


                        if (produto != null && (produto.IsValidade || produto.IsLote))
                        {


                            var estoquePreMovimentoLoteValidadeDto = new EstoquePreMovimentoLoteValidadeDto
                            {
                                Id = input.EstoquePreMovimentoLoteValidadeId
                                                                                                            ,
                                LaboratorioId = input.LaboratorioId != null ? (long)input.LaboratorioId : 0
                                                                                                            ,
                                Lote = input.Lote
                                                                                                            ,
                                Validade = input.Validade != null ? (DateTime)input.Validade : DateTime.MinValue
                                                                                                            ,
                                ProdutoId = input.ProdutoId
                                                                                                            ,
                                EstoquePreMovimentoItemId = input.Id
                                                                                                            ,
                                Quantidade = input.Quantidade
                                                                                                            ,
                                LoteValidadeId = input.LoteValidadeId != null ? (long)input.LoteValidadeId : 0
                            };

                            var retorno = await _estoqueLoteValidadeAppService.CriarOuEditar(estoquePreMovimentoLoteValidadeDto);

                            _retornoPadrao.Errors.AddRange(retorno.Errors);
                        }

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();

                        if (produto != null && !(produto.IsValidade || produto.IsLote))
                        {
                            await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);
                        }

                        var query = await Obter(input.Id);
                        var newObj = MapPreMovimentoItem(query);

                        var estoquePreMovimentoItemDto = MapPreMovimentoItem(newObj);

                        _retornoPadrao.ReturnObject = estoquePreMovimentoItemDto;
                    }
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }


        public List<EstoquePreMovimentoItemDto> ObterItensPorPreMovimento(long preMovimentoId)
        {
            var itens = _estoquePreMovimentoItemRepository.GetAll().Where(w => w.PreMovimentoId == preMovimentoId).ToList();

            List<EstoquePreMovimentoItemDto> itensDto = new List<EstoquePreMovimentoItemDto>();
            EstoquePreMovimentoItemDto itemDto;

            long idGrid = 0;
            foreach (var item in itens)
            {
                itemDto = MapPreMovimentoItem(item);
                itemDto.IdGrid = ++idGrid;
                itensDto.Add(itemDto);
            }

            return itensDto;
        }

        public List<EstoquePreMovimentoItemSolicitacaoDto> ObterItensSolicitacaoPorPreMovimento(long preMovimentoId)
        {
            var itens = _estoqueSolicitacaoItemRepositorio.GetAll()
                         .Where(w => w.SolicitacaoId == preMovimentoId
                                  && w.EstadoSolicitacaoItemId != (long)EnumPreMovimentoEstado.TotalmenteAtendido  ).ToList();

            List<EstoquePreMovimentoItemSolicitacaoDto> itensDto = new List<EstoquePreMovimentoItemSolicitacaoDto>();
            EstoquePreMovimentoItemSolicitacaoDto itemDto;

            long idGrid = 0;
            foreach (var item in itens)
            {
                itemDto = new EstoquePreMovimentoItemSolicitacaoDto();

                itemDto.Id = item.Id;
               // itemDto.NumeroSerie = item.NumeroSerie;
                itemDto.ProdutoId = item.ProdutoId;

                if (item.ProdutoId != 0)
                {
                    var produto = _produtoRepository.Get(item.ProdutoId);
                    if (produto != null)
                    {
                        itemDto.Produto = produto.Descricao;
                    }
                }

                itemDto.ProdutoUnidadeId = item.ProdutoUnidadeId;

                if (item.ProdutoUnidadeId != 0)
                {
                    var unidade = _unidadeRepository.Get((long)item.ProdutoUnidadeId);
                    if (unidade != null)
                    {
                        itemDto.ProdutoUnidade = unidade.Descricao;
                    }
                }

                //var itensAtendidos = _estoqueMovimentoItemRepository.GetAll()
                //                         .Where(w => w.EstoquePreMovimentoItemId == itemDto.Id).ToList();


                //var quantidadeAtendida = itensAtendidos.Sum(s => s.Quantidade);


                itemDto.Quantidade = item.Quantidade - item.QuantidadeAtendida;

                itemDto.IdGrid = ++idGrid;
                itensDto.Add(itemDto);
            }

            return itensDto;
        }

        public PagedResultDto<LoteValidadeGridDto> ListarLoteValidadeJson(string json)
        {
            try
            {
                var contarlotesValidades = 0;
                List<LoteValidadeGridDto> lotesValidades = new List<LoteValidadeGridDto>();
                if (!string.IsNullOrEmpty(json))
                {
                    lotesValidades = JsonConvert.DeserializeObject<List<LoteValidadeGridDto>>(json, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });


                    foreach (var item in lotesValidades)
                    {
                        if (item.LaboratorioId != null && item.LaboratorioId != 0)
                        {
                            var laboratorio = _produtoLaboratorioRepositorio.Get((long)item.LaboratorioId);
                            if (laboratorio != null)
                            {
                                item.Laboratorio = laboratorio.Descricao;
                            }
                        }

                        if (item.LoteValidadeId != 0)
                        {
                            var loteValidade = _loteValidadeRepository.Get(item.LoteValidadeId);
                            if (loteValidade != null)
                            {
                                item.Lote = loteValidade.Lote;
                                item.Validade = loteValidade.Validade;
                            }
                        }

                    }

                }
                contarlotesValidades = lotesValidades.Count;

                return new PagedResultDto<LoteValidadeGridDto>(
                      contarlotesValidades,
                      lotesValidades
                      );
            }
            catch (Exception ex)
            {

            }

            return null;
        }


        public PagedResultDto<NumeroSerieGridDto> ListarNumeroSerieJson(string json)
        {
            try
            {
                var contarNumeroSerie = 0;
                List<NumeroSerieGridDto> numerosSerie = new List<NumeroSerieGridDto>();
                if (!string.IsNullOrEmpty(json))
                {
                    numerosSerie = JsonConvert.DeserializeObject<List<NumeroSerieGridDto>>(json);

                }
                contarNumeroSerie = numerosSerie.Count;

                return new PagedResultDto<NumeroSerieGridDto>(
                      contarNumeroSerie,
                      numerosSerie
                      );
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        public PagedResultDto<LoteValidadeGridDto> ListarLoteValidadeJsonLista(string str)//List<LoteValidadeGridDto> lista)
        {

            List<LoteValidadeGridDto> lista = new List<LoteValidadeGridDto>();

            return new PagedResultDto<LoteValidadeGridDto>(
                     lista.Count,
                     lista
                     );
        }
    }
}
