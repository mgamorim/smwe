﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class EstoqueLoteValidadeAppService : SWMANAGERAppServiceBase, IEstoqueLoteValidadeAppService
    {
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IRepository<LoteValidade, long> _loteValidadeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        private readonly IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        private readonly IRepository<EstoqueLaboratorio, long> _produtoLaboratorioRepository;
        private readonly IProdutoSaldoAppService _produtoSaldoAppService;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepository;


        public EstoqueLoteValidadeAppService(
            IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
            IRepository<LoteValidade, long> loteValidadeRepository,
            IRepository<EstoqueMovimento, long> estoqueMovimentoRepository,
            IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository,
            IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository,
            IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
            IRepository<EstoqueLaboratorio, long> produtoLaboratorioRepository,
            IProdutoSaldoAppService produtoSaldoAppService,
            IRepository<ProdutoSaldo, long> produtoSaldoRepository
        )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _loteValidadeRepository = loteValidadeRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _produtoLaboratorioRepository = produtoLaboratorioRepository;
            _produtoSaldoAppService = produtoSaldoAppService;
            _produtoSaldoRepository = produtoSaldoRepository;
        }



        [UnitOfWork]
        public async Task<DefaultReturn<EstoquePreMovimentoLoteValidadeDto>> CriarOuEditar(EstoquePreMovimentoLoteValidadeDto input)
        {

            var _retornoPadrao = new DefaultReturn<EstoquePreMovimentoLoteValidadeDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            try
            {
                var estoqueLoteValidade = new EstoquePreMovimentoLoteValidade();
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    estoqueLoteValidade = _estoquePreMovimentoLoteValidadeRepository.GetAll().Where(w => w.Id == input.Id).FirstOrDefault();
                    if (estoqueLoteValidade == null)
                    {
                        estoqueLoteValidade = new EstoquePreMovimentoLoteValidade();
                        estoqueLoteValidade.EstoquePreMovimentoItemId = input.EstoquePreMovimentoItemId;
                    }

                    var loteValidade = new LoteValidadeDto();
                    if (input.LoteValidadeId == 0)
                    {
                        loteValidade = ObterPorLaboratorioLoteValidade(input.LaboratorioId, input.Lote, input.Validade).Result;

                        if (loteValidade != null)
                        {
                            estoqueLoteValidade.LoteValidadeId = loteValidade.Id;
                        }
                        else
                        {
                            estoqueLoteValidade.LoteValidade = new LoteValidade { EstLaboratorioId = input.LaboratorioId, Lote = input.Lote, Validade = input.Validade, ProdutoId = input.ProdutoId };
                        }
                    }
                    else
                    {
                        estoqueLoteValidade.LoteValidadeId = input.LoteValidadeId;
                    }

                    estoqueLoteValidade.Quantidade = input.Quantidade;

                    if (input.Id.Equals(0))
                    {
                        // using (var unitOfWork = _unitOfWorkManager.Begin())
                        // {
                        await _estoquePreMovimentoLoteValidadeRepository.InsertAsync(estoqueLoteValidade);

                        //   unitOfWork.Complete();
                        //  _unitOfWorkManager.Current.SaveChanges();
                        // unitOfWork.Dispose();
                        //}
                    }
                    else
                    {
                        //using (var unitOfWork = _unitOfWorkManager.Begin())
                        //{
                        await _estoquePreMovimentoLoteValidadeRepository.UpdateAsync(estoqueLoteValidade);

                        //  unitOfWork.Complete();
                        //  _unitOfWorkManager.Current.SaveChanges();
                        //  unitOfWork.Dispose();
                        //}
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
                _retornoPadrao.ReturnObject = MapLoteValidade(estoqueLoteValidade);//.MapTo<EstoquePreMovimentoLoteValidadeDto>();
                                                                                   //  }

                _produtoSaldoAppService.AtulizarSaldoPreMovimentoItemLoteValidade(estoqueLoteValidade);
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }

        public async Task<EstoquePreMovimentoLoteValidadeDto> Obter(long id)
        {
            try
            {
                var query = _estoquePreMovimentoLoteValidadeRepository.GetAll()
                    .Include(i => i.LoteValidade)
                    .Include(i => i.EstoquePreMovimentoItem)
                    .Where(w => w.Id == id).FirstOrDefault();

                var entrada = MapLoteValidade(query);//.MapTo<EstoquePreMovimentoLoteValidadeDto>();

                return entrada;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<PagedResultDto<EstoquePreMovimentoLoteValidadeDto>> ListarPorPreMovimentoItem(ListarEstoquePreMovimentoInput input)
        {
            try
            {
                long preMovimentoItemId;

                if (long.TryParse(input.Filtro, out preMovimentoItemId))
                {
                    var contarLoteValidades = 0;
                    List<EstoquePreMovimentoLoteValidadeDto> lotesValidadesDto = null;

                    var query = _estoquePreMovimentoLoteValidadeRepository
                      .GetAll()
                      .Include(i => i.LoteValidade)
                      .Include(i => i.EstoquePreMovimentoItem)
                      .Where(w => w.EstoquePreMovimentoItemId == preMovimentoItemId);


                    contarLoteValidades = await query.CountAsync();
                    var lotesValidades = await query.ToListAsync();
                    lotesValidadesDto = MapLoteValidade(lotesValidades);//.MapTo<List<EstoquePreMovimentoLoteValidadeDto>>();

                    lotesValidadesDto.ForEach(f => f.EntradaConfirmada = input.EntradaConfirmada);


                    return new PagedResultDto<EstoquePreMovimentoLoteValidadeDto>(
                        contarLoteValidades,
                        lotesValidadesDto
                        );
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<LoteValidadeDto> ObterPorLaboratorioLoteValidade(long? laboratorioId, string lote, DateTime validade)
        {
            try
            {
                var query =  Task.Run(()=> _loteValidadeRepository.GetAll().Where(w => ((laboratorioId == 0 || laboratorioId==null) || w.EstLaboratorioId == laboratorioId)
                                                                     && w.Lote == lote
                                                                     && w.Validade == validade).FirstOrDefault()).Result;

                var loteValidade = query.MapTo<LoteValidadeDto>();

                return loteValidade;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public static EstoquePreMovimentoLoteValidadeDto MapLoteValidade(EstoquePreMovimentoLoteValidade preMovimentoLoteValidade)
        {
            EstoquePreMovimentoLoteValidadeDto preMovimentoLoteValidadeDto = new EstoquePreMovimentoLoteValidadeDto();

            preMovimentoLoteValidadeDto.CreationTime = preMovimentoLoteValidade.CreationTime;
            preMovimentoLoteValidadeDto.CreatorUserId = preMovimentoLoteValidade.CreatorUserId;
            preMovimentoLoteValidadeDto.DeleterUserId = preMovimentoLoteValidade.DeleterUserId;
            preMovimentoLoteValidadeDto.DeletionTime = preMovimentoLoteValidade.DeletionTime;
            preMovimentoLoteValidadeDto.EstoquePreMovimentoItemId = preMovimentoLoteValidade.EstoquePreMovimentoItemId;
            preMovimentoLoteValidadeDto.Id = preMovimentoLoteValidade.Id;
            preMovimentoLoteValidadeDto.IsDeleted = preMovimentoLoteValidade.IsDeleted;
            preMovimentoLoteValidadeDto.IsSistema = preMovimentoLoteValidade.IsSistema;
            preMovimentoLoteValidadeDto.LastModificationTime = preMovimentoLoteValidade.LastModificationTime;
            preMovimentoLoteValidadeDto.LastModifierUserId = preMovimentoLoteValidade.LastModifierUserId;
            preMovimentoLoteValidadeDto.Quantidade = preMovimentoLoteValidade.Quantidade;
            preMovimentoLoteValidadeDto.LoteValidadeId = preMovimentoLoteValidade.LoteValidadeId;

            if (preMovimentoLoteValidade.LoteValidade != null)
            {
                preMovimentoLoteValidadeDto.LoteValidade = new LoteValidadeDto
                {
                    Id = preMovimentoLoteValidade.LoteValidade.Id,
                    Lote = preMovimentoLoteValidade.LoteValidade.Lote,
                    Validade = preMovimentoLoteValidade.LoteValidade.Validade,
                    ProdutoLaboratorioId = preMovimentoLoteValidade.LoteValidade.EstLaboratorioId,
                    Produto = CarregarProdutoDto(preMovimentoLoteValidade.LoteValidade.Produto)
                };
            }

            return preMovimentoLoteValidadeDto;
        }

        public static EstoquePreMovimentoLoteValidade MapLoteValidade(EstoquePreMovimentoLoteValidadeDto preMovimentoLoteValidadeDto)
        {
            EstoquePreMovimentoLoteValidade preMovimentoLoteValidade = new EstoquePreMovimentoLoteValidade();

            preMovimentoLoteValidade.CreationTime = preMovimentoLoteValidadeDto.CreationTime;
            preMovimentoLoteValidade.CreatorUserId = preMovimentoLoteValidadeDto.CreatorUserId;
            preMovimentoLoteValidade.DeleterUserId = preMovimentoLoteValidadeDto.DeleterUserId;
            preMovimentoLoteValidade.DeletionTime = preMovimentoLoteValidadeDto.DeletionTime;
            preMovimentoLoteValidade.EstoquePreMovimentoItemId = preMovimentoLoteValidadeDto.EstoquePreMovimentoItemId;
            preMovimentoLoteValidade.Id = preMovimentoLoteValidadeDto.Id;
            preMovimentoLoteValidade.IsDeleted = preMovimentoLoteValidadeDto.IsDeleted;
            preMovimentoLoteValidade.IsSistema = preMovimentoLoteValidadeDto.IsSistema;
            preMovimentoLoteValidade.LastModificationTime = preMovimentoLoteValidadeDto.LastModificationTime;
            preMovimentoLoteValidade.LastModifierUserId = preMovimentoLoteValidadeDto.LastModifierUserId;
            preMovimentoLoteValidade.Quantidade = preMovimentoLoteValidadeDto.Quantidade;
            preMovimentoLoteValidade.LoteValidadeId = preMovimentoLoteValidadeDto.LoteValidadeId;

            return preMovimentoLoteValidade;
        }

        public static List<EstoquePreMovimentoLoteValidadeDto> MapLoteValidade(List<EstoquePreMovimentoLoteValidade> preMovimentoLotesValidades)
        {
            List<EstoquePreMovimentoLoteValidadeDto> preMovimentoLotesValidadesDto = new List<EstoquePreMovimentoLoteValidadeDto>();

            foreach (var item in preMovimentoLotesValidades)
            {
                preMovimentoLotesValidadesDto.Add(MapLoteValidade(item));
            }

            return preMovimentoLotesValidadesDto;
        }

        public static List<EstoquePreMovimentoLoteValidade> MapLoteValidade(List<EstoquePreMovimentoLoteValidadeDto> preMovimentoLotesValidadesDto)
        {
            List<EstoquePreMovimentoLoteValidade> preMovimentoLotesValidades = new List<EstoquePreMovimentoLoteValidade>();

            foreach (var item in preMovimentoLotesValidadesDto)
            {
                preMovimentoLotesValidades.Add(MapLoteValidade(item));
            }

            return preMovimentoLotesValidades;
        }

        static ProdutoDto CarregarProdutoDto(Produto produto)
        {
            ProdutoDto produtoDto = new ProdutoDto();

            if (produto != null)
            {
                produtoDto.Id = produto.Id;
                produtoDto.Descricao = produto.Descricao;
                produtoDto.IsValidade = produto.IsValidade;
            }


            return produtoDto;

        }

        public async Task<List<GenericoIdNome>> ObterPorProdutoEstoque(long produtoId, long estoqueId, long preMovimentoId)
        {
            try
            {
                var query = from mov in _estoqueMovimentoRepository.GetAll()
                            join item in _estoqueMovimentoItemRepository.GetAll()
                            on mov.Id equals item.MovimentoId



                            join iltv in _estoqueMovimentoLoteValidadeRepository.GetAll()
                            on item.Id equals iltv.EstoqueMovimentoItemId
                            into movjoin



                            from joinedmov in movjoin.DefaultIfEmpty()

                            join produtoLaboratorio in _produtoLaboratorioRepository.GetAll()
                           on joinedmov.LoteValidade.EstLaboratorioId equals produtoLaboratorio.Id

                             into laboratoriojoin



                            from joinLaboratorio in laboratoriojoin.DefaultIfEmpty()

                            where mov.EstoqueId == estoqueId
                               && item.ProdutoId == produtoId
                            select new { LoteValidade = joinedmov.LoteValidade, Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade), ProdutoLaboratorio = joinLaboratorio };



                var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                                        join item in _estoquePreMovimentoItemRepository.GetAll()
                                        on mov.Id equals item.PreMovimentoId
                                        join iltv in _estoquePreMovimentoLoteValidadeRepository.GetAll()
                                        on item.Id equals iltv.EstoquePreMovimentoItemId
                                        into movjoin
                                        from joinedmov in movjoin.DefaultIfEmpty()

                                        join produtoLaboratorio in _produtoLaboratorioRepository.GetAll()
                        on joinedmov.LoteValidade.EstLaboratorioId equals produtoLaboratorio.Id

                                        where mov.EstoqueId == estoqueId
                                           && item.ProdutoId == produtoId
                                           && mov.Id == preMovimentoId
                                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade), ProdutoLaboratorio = produtoLaboratorio };

                var queryList = query.ToList();
                var queryPreMovimentoList = queryPreMovimento.ToList();

                queryList.AddRange(queryPreMovimentoList);

                var lotesValidade = queryList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade), ProdutoLaboratorio = s.Key.EstoqueLaboratorio }).ToList();

                var listaGenericoIdNome = new List<GenericoIdNome>();

                foreach (var item in lotesValidade.OrderBy(o => o.LoteValidade.Validade))
                {
                    var nome = String.Concat("Lt:", item.LoteValidade.Lote
                                            , "     Validade: ", String.Format("{0:dd/MM/yyyy}", item.LoteValidade.Validade)
                                            , item.ProdutoLaboratorio!=null? string.Concat( "     Laboratório: ", item.ProdutoLaboratorio.Descricao): string.Empty
                                            , "     Qtd: ", item.Qtd);

                    listaGenericoIdNome.Add(new GenericoIdNome { Id = item.LoteValidade.Id, Nome = nome });
                }

                return listaGenericoIdNome;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task Excluir(EstoquePreMovimentoLoteValidadeDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _estoquePreMovimentoLoteValidadeRepository.DeleteAsync(input.Id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                _produtoSaldoAppService.AtulizarSaldoPreMovimentoItemLoteValidade(MapLoteValidade(input));
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }


        public async Task<List<GenericoIdNome>> ObterPorProdutoEstoqueComSaida(long produtoId
                                                                             , long estoqueId
                                                                             , long tipoMovimentoId
                                                                             , long? unidadeOrganizacionalId
                                                                             , long? atendimentoId)
        {
            try
            {
                var query = from mov in _estoqueMovimentoRepository.GetAll()
                            join item in _estoqueMovimentoItemRepository.GetAll()
                            on mov.Id equals item.MovimentoId

                            join iltv in _estoqueMovimentoLoteValidadeRepository.GetAll()
                            on item.Id equals iltv.EstoqueMovimentoItemId


                            join produtoLaboratorio in _produtoLaboratorioRepository.GetAll()
                           on iltv.LoteValidade.EstLaboratorioId equals produtoLaboratorio.Id
                              into laboratoriojoin

                            from joinLaboratorio in laboratoriojoin.DefaultIfEmpty()


                            where mov.EstoqueId == estoqueId
                               && item.ProdutoId == produtoId
                              && mov.EstTipoOperacaoId == (long)EnumTipoOperacao.Saida

                              && mov.EstTipoMovimentoId == tipoMovimentoId

                              && (
                                  ((tipoMovimentoId == (long)EnumTipoMovimento.Setor_Saida
                                 || tipoMovimentoId == (long)EnumTipoMovimento.GastoSala_Saida)
                                 && mov.UnidadeOrganizacionalId == unidadeOrganizacionalId)

                                 || (tipoMovimentoId == (long)EnumTipoMovimento.Paciente_Saida
                                    && mov.AtendimentoId == atendimentoId)
                                 )

                            //(((mov.IsEntrada ? 1 : 0) * 2) - 1)
                            select new { LoteValidade = iltv.LoteValidade, Quantidade = iltv.Quantidade, ProdutoLaboratorio = joinLaboratorio };



                //var queryPreMovimento = from mov in _estoquePreMovimentoRepository.GetAll()
                //                        join item in _estoquePreMovimentoItemRepository.GetAll()
                //                        on mov.Id equals item.PreMovimentoId
                //                        join iltv in _estoquePreMovimentoLoteValidadeRepository.GetAll()
                //                        on item.Id equals iltv.EstoquePreMovimentoItemId
                //                        into movjoin
                //                        from joinedmov in movjoin.DefaultIfEmpty()

                //                        join produtoLaboratorio in _produtoLaboratorioRepository.GetAll()
                //        on joinedmov.LoteValidade.ProdutoLaboratorioId equals produtoLaboratorio.Id

                //                        where mov.EstoqueId == estoqueId
                //                           && item.ProdutoId == produtoId
                //                           && mov.Id == preMovimentoId
                //                        select new { LoteValidade = joinedmov.LoteValidade, Quantidade = ((((mov.IsEntrada ? 1 : 0) * 2) - 1) * joinedmov.Quantidade), ProdutoLaboratorio = produtoLaboratorio };

                var queryList = query.ToList();
                //  var queryPreMovimentoList = queryPreMovimento.ToList();

                //   queryList.AddRange(queryPreMovimentoList);

                var lotesValidade = queryList.GroupBy(g => g.LoteValidade).Select(s => new { LoteValidade = s.Key, Qtd = s.Sum(soma => soma.Quantidade), ProdutoLaboratorio = s.Key.EstoqueLaboratorio }).ToList();

                var listaGenericoIdNome = new List<GenericoIdNome>();

                foreach (var item in lotesValidade.OrderBy(o => o.LoteValidade.Validade))
                {
                    var nome = String.Concat("Lt:", item.LoteValidade.Lote
                                            , "     Validade: ", String.Format("{0:dd/MM/yyyy}", item.LoteValidade.Validade)
                                            , item.ProdutoLaboratorio!=null? string.Concat("     Laboratório: ", item.ProdutoLaboratorio.Descricao): string.Empty
                                            , "     Qtd: ", item.Qtd);
                    listaGenericoIdNome.Add(new GenericoIdNome { Id = item.LoteValidade.Id, Nome = nome });
                }

                return listaGenericoIdNome;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


        public List<GenericoIdNome> ObterPorProdutoEstoqueComSaida(long produtoId, long estoqueId, long laboratorioId)
        {
            var listaGenericoIdNome = new List<GenericoIdNome>();

            var query = _produtoSaldoRepository.GetAll()
                        .Where(w => w.LoteValidade.EstLaboratorioId == laboratorioId
                                 && w.ProdutoId == produtoId
                                 && w.EstoqueId == estoqueId
                                 && w.QuantidadeAtual > 0).ToList();

            foreach (var item in query)
            {
                var generico = new GenericoIdNome();

                generico.Id = item.Id;
                generico.Nome = String.Concat("Lt: ", item.LoteValidade.Lote, "     Validade: ", String.Format("{ 0:dd / MM / yyyy}", item.LoteValidade.Validade));

                listaGenericoIdNome.Add(generico);
            }

            return listaGenericoIdNome;
        }



        public async Task<ResultDropdownList> ListarProdutoDropdownPorLaboratorio(DropdownInput dropdownInput)
        {

            long produtoId = dropdownInput.filtros[0] != null ? long.Parse(dropdownInput.filtros[0]) : 0;
            long estoqueId = dropdownInput.filtros[1] != null ? long.Parse(dropdownInput.filtros[1]) : 0;
            long laboratorioId = dropdownInput.filtros[2] != null ? long.Parse(dropdownInput.filtros[2]) : 0;

            // return await ListarCodigoDescricaoDropdown<Produto>(dropdownInput, _produtoRepositorio);



            return await ListarDropdownLambda(dropdownInput
                                                     , _loteValidadeRepository
                                                     , m => ((string.IsNullOrEmpty(dropdownInput.search) 
                                                          || (m.Validade.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                                                          || m.Lote.ToLower().Contains(dropdownInput.search.ToLower())))
                                                          && m.ProdutoId == produtoId
                                                          && (laboratorioId==0 || m.EstLaboratorioId == laboratorioId)

                                                          //&& _produtoSaldoRepository.GetAll().Any(w=> w.LoteValidadeId == m.Id 
                                                          //                                             && w.EstoqueId == estoqueId)

                                                     )
                                                    , p => new DropdownItems { id = p.Id, text = string.Concat("Lt: ", p.Lote.ToString(), " Validade: ", p.Validade.Day, "/", p.Validade.Month, "/", p.Validade.Year  ) }
                                                    , o => o.Descricao
                                                    );

        }

        public LoteValidadeDto Obter(long produtoId, string lote, DateTime validade, long? laboratorioId)
        {
            var loteValidade = _loteValidadeRepository.GetAll()
                                                      .Where(w => w.ProdutoId == produtoId
                                                              && w.Lote == lote
                                                              && w.Validade == validade
                                                              && (laboratorioId == null || w.EstLaboratorioId == laboratorioId))
                                                      .FirstOrDefault();

            if(loteValidade !=null)
            {
                return loteValidade.MapTo<LoteValidadeDto>();
            }

            return null;
        }
    }
}
