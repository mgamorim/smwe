﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public interface IProdutoSaldoAppService : IApplicationService
    {
        Task AtualizarSaldoMovimento(long movimentoId);
        Task AtulizarSaldoPreMovimentoItem(EstoquePreMovimentoItem preMovimentoItem);
        void AtulizarSaldoPreMovimentoItemLoteValidade(EstoquePreMovimentoLoteValidade preMovimentoItemLoteValidade);
        void AtulizarSaldoPreMovimentoItemPreMovimento(EstoquePreMovimento preMovimento);//EstoquePreMovimentoItem preMovimentoItem);//, EstoquePreMovimento preMovimento);
        void AtulizarSaldoPreMovimentoItemLoteValidadePreMovimento(EstoquePreMovimentoLoteValidade preMovimentoItemLoteValidade);
        EstoquePreMovimentoItem PreMovimentoItem { get; set; }
    }
}
