﻿using Abp.Application.Services;
using NFe.Classes;
using NFe.Classes.Informacoes.Detalhe;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public interface IEstoqueImportacaoProdutoAppService : IApplicationService
    {
        List<EstoqueImportacaoProdutoDto> ObterListaImportacaoProduto(nfeProc nf);
        DefaultReturn<Object> RelacionarProdutos(List<EstoqueImportacaoProdutoDto> importacaoProdutos, long fornecedorId, string CNPJNota);
        string ObterInformacao(string informacaoAdicional, string[] identificador);
        DateTime ObterValidade(string informacaoAdicional);
    }
}
