﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.UI;
using Abp.Domain.Uow;



using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.MotivosPerdaProdutos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class MotivoPerdaProdutoAppService : SWMANAGERAppServiceBase, IMotivoPerdaProdutoAppService
    {
        private readonly IRepository<MotivoPerdaProduto, long> _motivoPerdaProdutoRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public MotivoPerdaProdutoAppService(
         IRepository<MotivoPerdaProduto, long> motivoPerdaProdutoRepository,
         IUnitOfWorkManager unitOfWorkManager
       
         )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _motivoPerdaProdutoRepository = motivoPerdaProdutoRepository;
        }
        
        public async Task<ListResultDto<MotivoPerdaProdutoDto>> ListarTodos()
        {
            var contarMotivoPerdadProduto = 0;
            List<MotivoPerdaProduto> motivosPerdadProdutos;
            List<MotivoPerdaProdutoDto> motivosPerdadProdutosDtos = new List<MotivoPerdaProdutoDto>();
            try
            {
                var query = _motivoPerdaProdutoRepository
                    .GetAll();

                contarMotivoPerdadProduto = await query
                    .CountAsync();

                motivosPerdadProdutos = await query
                    .AsNoTracking()
                    .ToListAsync();

                motivosPerdadProdutosDtos = motivosPerdadProdutos
                    .MapTo<List<MotivoPerdaProdutoDto>>();

                return new ListResultDto<MotivoPerdaProdutoDto> { Items = motivosPerdadProdutosDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
      
    }
}
