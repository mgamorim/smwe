﻿//using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Validacoes;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class EstoqueMovimentoAppService : SWMANAGERAppServiceBase, IEstoqueMovimentoAppService
    {
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepository;
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepository;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepository;
        private readonly IRepository<EstoquePreMovimentoLoteValidade, long> _estoquePreMovimentoLoteValidadeRepository;
        private readonly IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepository;
        private readonly IRepository<LoteValidade, long> _loteValidadeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Produto, long> _produtoRepository;
        private readonly IRepository<ProdutoUnidade, long> _produtoUnidadeRepository;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepository;
        private readonly IRepository<EstoqueTransferenciaProduto, long> _estoqueTransferenciaProduto;
        private readonly IRepository<EstMovimentoBaixa, long> _estMovimentoBaixaRepository;
        private readonly IRepository<EstMovimentoBaixaItem, long> _estMovimentoBaixaItemRepository;
        private readonly IUnidadeAppService _unidadeAppService;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;
        private readonly IFaturamentoContaItemAppService _faturamentoContaItemAppService;

        public object PreMovimentoItem { get; private set; }

        public EstoqueMovimentoAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<EstoqueMovimento, long> estoqueMovimentoRepository,
            IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepository,
            IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepository,
             IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepository,
              IRepository<EstoquePreMovimentoLoteValidade, long> estoquePreMovimentoLoteValidadeRepository,
        IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepository,
        IRepository<LoteValidade, long> loteValidadeRepository,
        IRepository<Produto, long> produtoRepository,
        IRepository<ProdutoUnidade, long> produtoUnidadeRepository,
        IRepository<ProdutoSaldo, long> produtoSaldoRepository,
        IRepository<EstoqueTransferenciaProduto, long> estoqueTransferenciaProduto,
            IRepository<EstMovimentoBaixa, long> estMovimentoBaixaRepository,
            IRepository<EstMovimentoBaixaItem, long> estMovimentoBaixaItemRepository,
           IUnidadeAppService unidadeAppService,
           IRepository<EstoqueGrupo, long> estoqueGrupoRepository,
           IFaturamentoContaItemAppService faturamentoContaItemAppService
      )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _estoquePreMovimentoRepository = estoquePreMovimentoRepository;
            _estoquePreMovimentoItemRepository = estoquePreMovimentoItemRepository;
            _estoqueMovimentoItemRepository = estoqueMovimentoItemRepository;
            _estoquePreMovimentoLoteValidadeRepository = estoquePreMovimentoLoteValidadeRepository;
            _estoqueMovimentoLoteValidadeRepository = estoqueMovimentoLoteValidadeRepository;
            _loteValidadeRepository = loteValidadeRepository;
            _produtoRepository = produtoRepository;
            _produtoUnidadeRepository = produtoUnidadeRepository;
            _produtoSaldoRepository = produtoSaldoRepository;
            _estoqueTransferenciaProduto = estoqueTransferenciaProduto;
            _estMovimentoBaixaRepository = estMovimentoBaixaRepository;
            _estMovimentoBaixaItemRepository = estMovimentoBaixaItemRepository;
            _unidadeAppService = unidadeAppService;
            _estoqueGrupoRepository = estoqueGrupoRepository;
            _faturamentoContaItemAppService = faturamentoContaItemAppService;
        }


        public async Task<DefaultReturn<Object>> GerarMovimentoEntrada(long preMovimentoId)
        {
            var _retornoPadrao = new DefaultReturn<Object>();
            try
            {
                var preMovimento = _estoquePreMovimentoRepository.Get(preMovimentoId);
                if (preMovimento != null)
                {

                    var preMovimentosItens = _estoquePreMovimentoItemRepository.GetAll().Where(w => w.PreMovimentoId == preMovimento.Id).ToList();

                    var _preMovimentoItens = EstoquePreMovimentoItemAppService.MapPreMovimentoItem(preMovimentosItens);//.MapTo<List<EstoquePreMovimentoItemDto>>();

                    PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                               , _estoquePreMovimentoLoteValidadeRepository
                                                                                                               , _estoquePreMovimentoItemRepository
                                                                                                               , _estoqueMovimentoRepository
                                                                                                               , _estoqueMovimentoItemRepository
                                                                                                               , _estoqueMovimentoLoteValidadeRepository
                                                                                                               , _loteValidadeRepository
                                                                                                               , _estoquePreMovimentoRepository
                                                                                                               , _produtoRepository
                                                                                                               , _produtoUnidadeRepository
                                                                                                               , _estoqueGrupoRepository);

                    _retornoPadrao.Errors = preMovimentoValidacaoService.Validar(EstoquePreMovimentoAppService.MapPreMovimento(preMovimento), _preMovimentoItens);

                    if (preMovimento.EstTipoOperacaoId == (long)EnumTipoOperacao.Entrada)
                    {
                        var existeLoteValidadePendente = preMovimentoValidacaoService.ExisteLoteValidadePendente(EstoquePreMovimentoAppService.MapPreMovimento(preMovimento));//.MapTo<EstoquePreMovimentoDto>());
                        if (existeLoteValidadePendente)
                        {
                            _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "EST003", Descricao = "Existe Produtos sem Informação de Lote/Validade" });
                        }
                    }
                    if (_retornoPadrao.Errors.Count() == 0)
                    {
                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            var movimento = new EstoqueMovimento();

                            movimento.AcrescimoDecrescimo = preMovimento.AcrescimoDecrescimo;
                            movimento.AplicacaoDireta = preMovimento.AplicacaoDireta;
                            movimento.CentroCustoId = preMovimento.CentroCustoId;
                            movimento.CFOPId = preMovimento.CFOPId;
                            movimento.Consiginado = preMovimento.Consiginado;
                            movimento.Contabiliza = preMovimento.Contabiliza;
                            movimento.DescontoPer = preMovimento.DescontoPer;
                            movimento.Documento = preMovimento.Documento;
                            movimento.Emissao = preMovimento.Emissao;
                            movimento.EmpresaId = preMovimento.EmpresaId;
                            movimento.EntragaProduto = preMovimento.EntragaProduto;
                            movimento.EstoqueId = preMovimento.EstoqueId;
                            movimento.EstoquePreMovimentoId = preMovimento.Id;
                            movimento.SisFornecedorId = preMovimento.SisFornecedorId;
                            movimento.Frete = preMovimento.Frete;
                            movimento.FretePer = preMovimento.FretePer;
                            movimento.Frete_SisFornecedorId = preMovimento.Frete_SisFornecedorId;
                            movimento.ICMSPer = preMovimento.ICMSPer;
                            movimento.InclusoNota = preMovimento.InclusoNota;
                            movimento.IsEntrada = preMovimento.IsEntrada;
                            movimento.Movimento = preMovimento.Movimento;
                            movimento.OrdemId = preMovimento.OrdemId;
                            movimento.PreMovimentoEstadoId = 2;
                            movimento.Quantidade = preMovimento.Quantidade;
                            movimento.Serie = preMovimento.Serie;
                            movimento.TipoFreteId = preMovimento.TipoFreteId;
                            movimento.TotalDocumento = preMovimento.TotalDocumento;
                            movimento.TotalProduto = preMovimento.TotalProduto;
                            movimento.ValorFrete = preMovimento.ValorFrete;
                            movimento.ValorICMS = preMovimento.ValorICMS;

                            movimento.PacienteId = preMovimento.PacienteId;
                            movimento.MedicoSolcitanteId = preMovimento.MedicoSolcitanteId;
                            movimento.UnidadeOrganizacionalId = preMovimento.UnidadeOrganizacionalId;
                            movimento.Observacao = preMovimento.Observacao;
                            movimento.AtendimentoId = preMovimento.AtendimentoId;
                            movimento.EstTipoMovimentoId = preMovimento.EstTipoMovimentoId;
                            movimento.EstTipoOperacaoId = preMovimento.EstTipoOperacaoId;

                            var movimentoId = AsyncHelper.RunSync(() => _estoqueMovimentoRepository.InsertAndGetIdAsync(movimento));

                            EstoqueMovimentoItem movimentoItem;
                            foreach (var item in preMovimentosItens)
                            {
                                movimentoItem = new EstoqueMovimentoItem();
                                movimentoItem.NumeroSerie = item.NumeroSerie;
                                movimentoItem.ProdutoId = item.ProdutoId;
                                movimentoItem.Quantidade = item.Quantidade;
                                movimentoItem.CustoUnitario = item.CustoUnitario;
                                movimentoItem.MovimentoId = movimentoId;
                                movimentoItem.PerIPI = item.PerIPI;
                                movimentoItem.ProdutoUnidadeId = item.ProdutoUnidadeId;
                                movimentoItem.EstoquePreMovimentoItemId = item.Id;

                                var movimentoItemId = AsyncHelper.RunSync(() => _estoqueMovimentoItemRepository.InsertAndGetIdAsync(movimentoItem));

                                var movimentoLoteValidades = _estoquePreMovimentoLoteValidadeRepository.GetAll().Where(w => w.EstoquePreMovimentoItemId == item.Id).ToList();

                                EstoqueMovimentoLoteValidade loteValidade;
                                foreach (var itemLoteValidade in movimentoLoteValidades)
                                {
                                    loteValidade = new EstoqueMovimentoLoteValidade();

                                    loteValidade.Quantidade = itemLoteValidade.Quantidade;
                                    loteValidade.EstoqueMovimentoItemId = movimentoItemId;
                                    loteValidade.LoteValidadeId = itemLoteValidade.LoteValidadeId;

                                    var loteValidadeId = AsyncHelper.RunSync(() => _estoqueMovimentoLoteValidadeRepository.InsertAndGetIdAsync(loteValidade));
                                }
                            }


                            preMovimento.PreMovimentoEstadoId = 2;

                            AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertOrUpdateAsync(preMovimento));



                            ProdutoSaldoAppService produtoSaldoAppService = new ProdutoSaldoAppService(_estoqueMovimentoRepository
                                                                                                   , _estoqueMovimentoItemRepository
                                                                                                   , _estoqueMovimentoLoteValidadeRepository
                                                                                                   , _produtoSaldoRepository
                                                                                                   , _produtoRepository
                                                                                                   , _estoquePreMovimentoRepository
                                                                                                   , _estoquePreMovimentoItemRepository
                                                                                                   , _estoquePreMovimentoLoteValidadeRepository
                                                                                                   //,_unitOfWorkManager

                                                                                                   );

                            await produtoSaldoAppService.AtualizarSaldoMovimento(movimento.Id);


                            if (preMovimento.EstTipoMovimentoId == (long)EnumTipoMovimento.Paciente_Saida)
                            {
                                GerarFaturamento(preMovimento, _preMovimentoItens);
                            }



                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }
                    }



                }

            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }


            return _retornoPadrao;
        }

        public async Task<DefaultReturn<Object>> GerarMovimentoTransferencia(long transferenciaId)
        {
            DefaultReturn<Object> retornoValidacao = new DefaultReturn<object>();

            var transferencia = _estoqueTransferenciaProduto.Get(transferenciaId);
            if (transferencia != null)
            {
                retornoValidacao = await ValidarMovimento(transferencia.PreMovimentoSaidaId);
                retornoValidacao.Errors.AddRange((await ValidarMovimento(transferencia.PreMovimentoEntradaId)).Errors);

                if (retornoValidacao.Errors.Count() == 0)
                {
                    var retornoSaida = await GerarMovimentoEntrada(transferencia.PreMovimentoSaidaId);
                    var retornoEntrada = await GerarMovimentoEntrada(transferencia.PreMovimentoEntradaId);

                    retornoSaida.Errors.AddRange(retornoEntrada.Errors);

                    return retornoSaida;
                }
            }

            return retornoValidacao;
        }


        async Task<DefaultReturn<Object>> ValidarMovimento(long preMovimentoId)
        {

            var _retornoPadrao = new DefaultReturn<Object>();
            try
            {
                var preMovimento = _estoquePreMovimentoRepository.Get(preMovimentoId);
                if (preMovimento != null)
                {

                    var preMovimentosItens = _estoquePreMovimentoItemRepository.GetAll().Where(w => w.PreMovimentoId == preMovimento.Id).ToList();

                    var _preMovimentoItens = EstoquePreMovimentoItemAppService.MapPreMovimentoItem(preMovimentosItens);//.MapTo<List<EstoquePreMovimentoItemDto>>();

                    PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                               , _estoquePreMovimentoLoteValidadeRepository
                                                                                                               , _estoquePreMovimentoItemRepository
                                                                                                               , _estoqueMovimentoRepository
                                                                                                               , _estoqueMovimentoItemRepository
                                                                                                               , _estoqueMovimentoLoteValidadeRepository
                                                                                                               , _loteValidadeRepository
                                                                                                               , _estoquePreMovimentoRepository
                                                                                                               , _produtoRepository
                                                                                                               , _produtoUnidadeRepository
                                                                                                               , _estoqueGrupoRepository);

                    _retornoPadrao.Errors = preMovimentoValidacaoService.Validar(EstoquePreMovimentoAppService.MapPreMovimento(preMovimento), _preMovimentoItens);






                    


                }


            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }


        void GerarFaturamento(EstoquePreMovimento preMovimento, List<EstoquePreMovimentoItemDto> itens)
        {
            FaturamentoContaItemInsertDto faturamentoContaItemInsertDto = new FaturamentoContaItemInsertDto();

            faturamentoContaItemInsertDto.AtendimentoId = preMovimento.AtendimentoId ?? 0;
            faturamentoContaItemInsertDto.Data = preMovimento.Movimento;

            List<FaturamentoContaItemDto> itensFaturamentoIds = new List<FaturamentoContaItemDto>();

            foreach (var item in itens)
            {
                var produto = _produtoRepository.GetAll()
                                  .Where(w => w.Id == item.ProdutoId)
                                  .FirstOrDefault();

                if (produto != null && produto.FaturamentoItemId != null)
                {
                    var contaItem = new FaturamentoContaItemDto();
                    contaItem.Id = (long)produto.FaturamentoItemId;
                    contaItem.Qtde = (float)item.Quantidade;

                    itensFaturamentoIds.Add(contaItem);
                }
            }

            faturamentoContaItemInsertDto.ItensFaturamento = itensFaturamentoIds;

            _faturamentoContaItemAppService.InserirItensContaFaturamento(faturamentoContaItemInsertDto);

        }


        public async Task<ListResultDto<MovimentoIndexDto>> ListarBaixaValePendente(ListarEstoquePreMovimentoInput input)
        {
            var contarEntradas = 0;
            List<EstoqueMovimento> estoqueMovimentos;
            List<MovimentoIndexDto> estoqueMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {

                var queryBaixa = _estMovimentoBaixaRepository.GetAll();

                var query = _estoqueMovimentoRepository
                    .GetAll()
                    .Where(w => !queryBaixa.Any(a => a.MovimentoBaixaId == w.Id)

                       &&

                    w.EstTipoMovimentoId == (long)EnumTipoMovimento.Vale_Entrada
                           && (input.FornecedorId == null || w.SisFornecedorId == input.FornecedorId)

                           && (input.PeridoDe == null || input.PeridoDe <= w.Emissao)
                           && (input.PeridoAte == null || input.PeridoAte >= w.Emissao)

                           && (string.IsNullOrEmpty(input.Filtro) ||
                               w.SisFornecedor.Descricao.ToLower().Contains(input.Filtro.ToLower()) ||
                               w.Empresa.NomeFantasia.ToLower().Contains(input.Filtro.ToLower()) ||
                               w.Documento.ToLower().Contains(input.Filtro.ToLower()) ||
                               w.TotalDocumento.ToString().ToLower().Contains(input.Filtro.ToLower())
                           )


                           )
                    .Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,

                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : string.Empty
                                                      ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId

                       ,
                        IsEntrada = s.IsEntrada
                       ,
                        TipoMovimento = s.EstTipoOperacao.Descricao
                       ,
                        TipoOperacaoId = s.EstTipoOperacaoId
                       ,
                        ValorDocumento = s.TotalDocumento,
                        Fornecedor = (s.SisFornecedor != null) ? s.SisFornecedor.Descricao : string.Empty,
                        FornecedorId = (s.SisFornecedor != null) ? s.SisFornecedor.Id : (long?)null
                    });

                contarEntradas = await query
                    .CountAsync();

                estoqueMovimentoDtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                // estoqueMovimentoDtos = MapPreMovimento(estoqueMovimentos);
                //.MapTo<List<EstoquePreMovimentoDto>>();

                return new ListResultDto<MovimentoIndexDto> { Items = estoqueMovimentoDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<EstoqueMovimentoDto> Obter(long id)
        {
            try
            {
                var query = _estoqueMovimentoRepository
                    .GetAll()
                    .Where(m => m.Id == id);

                var result = await query.FirstOrDefaultAsync();
                var preMovimento = MapMovimento(result);

                return preMovimento;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public static EstoqueMovimentoDto MapMovimento(EstoqueMovimento Movimento)
        {
            var MovimentoDto = new EstoqueMovimentoDto();

            MovimentoDto.AcrescimoDecrescimo = Movimento.AcrescimoDecrescimo;
            MovimentoDto.AplicacaoDireta = Movimento.AplicacaoDireta;
            MovimentoDto.AtendimentoId = Movimento.AtendimentoId;
            MovimentoDto.CentroCustoId = Movimento.CentroCustoId;
            MovimentoDto.Consiginado = Movimento.Consiginado;
            MovimentoDto.Contabiliza = Movimento.Contabiliza;
            MovimentoDto.CreationTime = Movimento.CreationTime;
            MovimentoDto.CreatorUserId = Movimento.CreatorUserId;
            MovimentoDto.DeleterUserId = Movimento.DeleterUserId;
            MovimentoDto.DeletionTime = Movimento.DeletionTime;
            MovimentoDto.DescontoPer = Movimento.DescontoPer;
            MovimentoDto.Documento = Movimento.Documento;
            MovimentoDto.Emissao = Movimento.Emissao;
            MovimentoDto.EmpresaId = Movimento.EmpresaId;
            MovimentoDto.EntragaProduto = Movimento.EntragaProduto;
            MovimentoDto.EstoqueId = Movimento.EstoqueId;
            MovimentoDto.FornecedorId = Movimento.SisFornecedorId;
            MovimentoDto.Frete = Movimento.Frete;
            MovimentoDto.FretePer = Movimento.FretePer;
            MovimentoDto.Frete_FornecedorId = Movimento.Frete_SisFornecedorId;
            MovimentoDto.ICMSPer = Movimento.ICMSPer;
            MovimentoDto.Id = Movimento.Id;
            MovimentoDto.InclusoNota = Movimento.InclusoNota;
            MovimentoDto.IsEntrada = Movimento.IsEntrada;
            MovimentoDto.MedicoSolcitanteId = Movimento.MedicoSolcitanteId;
            MovimentoDto.Movimento = Movimento.Movimento;
            MovimentoDto.Observacao = Movimento.Observacao;
            MovimentoDto.OrdemId = Movimento.OrdemId;
            MovimentoDto.PacienteId = Movimento.PacienteId;
            //  MovimentoDto.MovimentoEstadoId = Movimento.MovimentoEstadoId;
            MovimentoDto.Quantidade = Movimento.Quantidade;
            MovimentoDto.Serie = Movimento.Serie;
            MovimentoDto.EstTipoOperacaoId = Movimento.EstTipoOperacaoId;
            MovimentoDto.TipoFreteId = Movimento.TipoFreteId;
            MovimentoDto.EstTipoMovimentoId = Movimento.EstTipoMovimentoId;
            MovimentoDto.TotalDocumento = Movimento.TotalDocumento;
            MovimentoDto.TotalProduto = Movimento.TotalProduto;
            MovimentoDto.UnidadeOrganizacionalId = Movimento.UnidadeOrganizacionalId;
            MovimentoDto.ValorFrete = Movimento.ValorFrete;
            MovimentoDto.ValorICMS = Movimento.ValorICMS;
            //MovimentoDto.MotivoPerdaProdutoId = Movimento.MotivoPerdaProdutoId;
            MovimentoDto.CFOPId = Movimento.CFOPId;
            MovimentoDto.EstoquePreMovimentoId = Movimento.EstoquePreMovimentoId;
            return MovimentoDto;
        }

        public static EstoqueMovimento MapMovimento(EstoqueMovimentoDto MovimentoDto)
        {
            var Movimento = new EstoqueMovimento();

            Movimento.AcrescimoDecrescimo = MovimentoDto.AcrescimoDecrescimo;
            Movimento.AplicacaoDireta = MovimentoDto.AplicacaoDireta;
            Movimento.AtendimentoId = MovimentoDto.AtendimentoId;
            Movimento.CentroCustoId = MovimentoDto.CentroCustoId;
            Movimento.Consiginado = MovimentoDto.Consiginado;
            Movimento.Contabiliza = MovimentoDto.Contabiliza;
            Movimento.CreationTime = MovimentoDto.CreationTime;
            Movimento.CreatorUserId = MovimentoDto.CreatorUserId;
            Movimento.DeleterUserId = MovimentoDto.DeleterUserId;
            Movimento.DeletionTime = MovimentoDto.DeletionTime;
            Movimento.DescontoPer = MovimentoDto.DescontoPer;
            Movimento.Documento = MovimentoDto.Documento;
            Movimento.Emissao = MovimentoDto.Emissao;
            Movimento.EmpresaId = MovimentoDto.EmpresaId;
            Movimento.EntragaProduto = MovimentoDto.EntragaProduto;
            Movimento.EstoqueId = MovimentoDto.EstoqueId;
            Movimento.SisFornecedorId = MovimentoDto.FornecedorId;
            Movimento.Frete = MovimentoDto.Frete;
            Movimento.FretePer = MovimentoDto.FretePer;
            Movimento.Frete_SisFornecedorId = MovimentoDto.Frete_FornecedorId;
            Movimento.ICMSPer = MovimentoDto.ICMSPer;
            Movimento.Id = MovimentoDto.Id;
            Movimento.InclusoNota = MovimentoDto.InclusoNota;
            Movimento.IsEntrada = MovimentoDto.IsEntrada;
            Movimento.MedicoSolcitanteId = MovimentoDto.MedicoSolcitanteId;
            Movimento.Movimento = MovimentoDto.Movimento;
            Movimento.Observacao = MovimentoDto.Observacao;
            Movimento.OrdemId = MovimentoDto.OrdemId;
            Movimento.PacienteId = MovimentoDto.PacienteId;
            //   Movimento.MovimentoEstadoId = MovimentoDto.MovimentoEstadoId;
            Movimento.Quantidade = MovimentoDto.Quantidade;
            Movimento.Serie = MovimentoDto.Serie;
            Movimento.EstTipoOperacaoId = MovimentoDto.EstTipoOperacaoId;
            Movimento.TipoFreteId = MovimentoDto.TipoFreteId;
            Movimento.EstTipoMovimentoId = MovimentoDto.EstTipoMovimentoId;
            Movimento.TotalDocumento = MovimentoDto.TotalDocumento;
            Movimento.TotalProduto = MovimentoDto.TotalProduto;
            Movimento.UnidadeOrganizacionalId = MovimentoDto.UnidadeOrganizacionalId;
            Movimento.ValorFrete = MovimentoDto.ValorFrete;
            Movimento.ValorICMS = MovimentoDto.ValorICMS;
            //  Movimento.MotivoPerdaProdutoId = MovimentoDto.MotivoPerdaProdutoId;
            Movimento.CFOPId = MovimentoDto.CFOPId;
            Movimento.EstoquePreMovimentoId = MovimentoDto.EstoquePreMovimentoId;

            return Movimento;
        }

        public async Task<ListResultDto<MovimentoIndexDto>> listarMovimentosValeSelecionados(ListarEstoquePreMovimentoInput input)
        {
            List<long> listaIds = new List<long>();

            string[] listaIdsString = input.Filtro.TrimEnd('-').Split('-');

            foreach (var item in listaIdsString)
            {
                listaIds.Add(long.Parse(item));
            }



            var contarEntradas = 0;
            List<EstoqueMovimento> estoqueMovimentos;
            List<MovimentoIndexDto> estoqueMovimentoDtos = new List<MovimentoIndexDto>();
            try
            {
                var query = _estoqueMovimentoRepository
                    .GetAll()
                    .Where(w => listaIds.Any(a => a == w.Id)
                           )
                    .Select(s => new MovimentoIndexDto
                    {
                        Id = s.Id
                                                        ,

                        DataEmissaoSaida = s.Emissao
                                                        ,
                        Documento = s.Documento
                                                        ,
                        Empresa = (s.Empresa != null) ? s.Empresa.NomeFantasia : string.Empty
                                                      ,
                        UsuarioId = s.CreatorUserId
                        ,
                        PreMovimentoEstadoId = s.PreMovimentoEstadoId

                       ,
                        IsEntrada = s.IsEntrada
                       ,
                        TipoMovimento = s.EstTipoOperacao.Descricao
                       ,
                        TipoOperacaoId = s.EstTipoOperacaoId
                       ,
                        ValorDocumento = s.TotalDocumento,
                        Fornecedor = (s.SisFornecedor != null) ? s.SisFornecedor.Descricao : string.Empty
                    });

                contarEntradas = await query
                    .CountAsync();

                estoqueMovimentoDtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                // estoqueMovimentoDtos = MapPreMovimento(estoqueMovimentos);
                //.MapTo<List<EstoquePreMovimentoDto>>();

                return new ListResultDto<MovimentoIndexDto> { Items = estoqueMovimentoDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<MovimentoItemDto>> ListarItensValesSelecionados(ListarEstoquePreMovimentoInput input)
        {
            List<long> listaIds = new List<long>();

            string[] listaIdsString = input.Filtro.TrimEnd('-').Split('-');

            foreach (var item in listaIdsString)
            {
                listaIds.Add(long.Parse(item));
            }


            var query = _estoqueMovimentoItemRepository.GetAll()
                       .Where(w => listaIds.Any(a => a == w.MovimentoId))
                       .Select(s => new MovimentoItemDto
                       {
                           Id = s.Id,
                           Produto = s.Produto.Descricao
                               ,
                           Quantidade = s.Quantidade
                               ,
                           ProdutoId = s.ProdutoId
                               ,

                           CustoUnitario = s.CustoUnitario
                                   ,

                           NumeroSerie = s.NumeroSerie,
                           ProdutoUnidadeId = s.ProdutoUnidadeId
                       }
                       );


            var estoqueMovimentoItemDtos = await query
                 .AsNoTracking()
                 .ToListAsync();


            foreach (var item in estoqueMovimentoItemDtos)
            {
                var unidade = await _unidadeAppService.Obter((long)item.ProdutoUnidadeId);
                if (unidade != null)
                {
                    item.Quantidade = item.Quantidade / unidade.Fator;
                    item.CustoTotal = item.CustoUnitario * item.Quantidade;
                    item.Unidade = unidade.Descricao;
                }
            }

            return new ListResultDto<MovimentoItemDto> { Items = estoqueMovimentoItemDtos };

        }

        public DefaultReturn<EstoqueMovimentoDto> CriarOuEditar(EstoqueMovimentoDto input, string valesIds)
        {
            var _retornoPadrao = new DefaultReturn<EstoqueMovimentoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            input.EstTipoOperacaoId = 1;
            var movimento = MapMovimento(input);

            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                                , _estoquePreMovimentoLoteValidadeRepository
                                                                                                                , _estoquePreMovimentoItemRepository
                                                                                                                , _estoqueMovimentoRepository
                                                                                                                , _estoqueMovimentoItemRepository
                                                                                                                , _estoqueMovimentoLoteValidadeRepository
                                                                                                                , _loteValidadeRepository
                                                                                                                , _estoquePreMovimentoRepository
                                                                                                                , _produtoRepository
                                                                                                                , _produtoUnidadeRepository
                                                                                                                , _estoqueGrupoRepository);
                    List<long> ids = new List<long>();

                    valesIds.TrimEnd('-').Split('-').ToList().ForEach(f => ids.Add(long.Parse(f)));
                    _retornoPadrao.Errors = preMovimentoValidacaoService.ValidarBaixaVale(movimento, ids);


                    if (_retornoPadrao.Errors.Count() == 0)
                    {
                        var preMovimento = CarregarPreMovimento(input);
                        preMovimento.Id = movimento.EstoquePreMovimentoId;

                        preMovimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Conferencia;

                        long estoquePreMovimentoId = 0;

                        if (preMovimento.Id == 0)
                        {
                            estoquePreMovimentoId = AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertAndGetIdAsync(preMovimento));
                        }
                        else
                        {
                            AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.UpdateAsync(preMovimento));
                            estoquePreMovimentoId = preMovimento.Id;
                        }

                        input.EstoquePreMovimentoId = estoquePreMovimentoId;
                        movimento.EstoquePreMovimentoId = estoquePreMovimentoId;
                        movimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Conferencia;

                        long movimentoId = 0;
                        if (movimento.Id == 0)
                        {
                            input.Id = AsyncHelper.RunSync(() => _estoqueMovimentoRepository.InsertAndGetIdAsync(movimento));

                            foreach (var item in ids)
                            {
                                var baixaVale = new EstMovimentoBaixa();
                                baixaVale.MovimentoBaixaPrincipalId = input.Id;
                                baixaVale.MovimentoBaixaId = item;

                                _estMovimentoBaixaRepository.InsertAsync(baixaVale);

                            }


                        }
                        else
                        {
                            AsyncHelper.RunSync(() => _estoqueMovimentoRepository.UpdateAsync(movimento));
                        }

                        _retornoPadrao.ReturnObject = input;
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }

        public async Task<ListResultDto<MovimentoItemDto>> ListarBaixaConsignadosPendente(ListarEstoquePreMovimentoInput input)
        {
            var contarEntradas = 0;



            List<MovimentoItemDto> estoqueMovimentoItemDtos = new List<MovimentoItemDto>();
            try
            {
                var queryBaixaItem = _estMovimentoBaixaItemRepository.GetAll()
                    .GroupBy(g => g.EstoqueMovimentoItemId)
                    .Select(s => new { EstoqueMovimentoItemId = s.Key, Quantidade = s.Sum(soma => soma.Quantidade) });


                var query =
                from mov in _estoqueMovimentoRepository.GetAll()
                join item in _estoqueMovimentoItemRepository.GetAll()
                on mov.Id equals item.MovimentoId
                into movjoin
                from joinedmov in movjoin.DefaultIfEmpty()

                where mov.EstTipoMovimentoId == (long)EnumTipoMovimento.Consignado_Entrada
                    && !queryBaixaItem.Any(a => a.EstoqueMovimentoItemId == joinedmov.Id && a.Quantidade >= joinedmov.Quantidade)

                  && (input.FornecedorId == null || mov.SisFornecedorId == input.FornecedorId)
                  && (input.PeridoDe == null || input.PeridoDe <= mov.Emissao)
                  && (input.PeridoAte == null || input.PeridoAte >= mov.Emissao)
                  && (string.IsNullOrEmpty(input.Filtro) ||
                    mov.SisFornecedor.Descricao.ToLower().Contains(input.Filtro.ToLower()) ||
                    mov.Empresa.NomeFantasia.ToLower().Contains(input.Filtro.ToLower()) ||
                    joinedmov.Produto.Descricao.ToLower().Contains(input.Filtro.ToLower()) ||
                    joinedmov.NumeroSerie.ToLower().Contains(input.Filtro.ToLower())
                    )

                select new MovimentoItemDto
                {
                    Id = joinedmov.Id,
                    Produto = joinedmov.Produto.Descricao
                       ,
                    Quantidade = joinedmov.Quantidade - (queryBaixaItem.Where(w => w.EstoqueMovimentoItemId == joinedmov.Id).FirstOrDefault() != null ?
                                                          queryBaixaItem.Where(w => w.EstoqueMovimentoItemId == joinedmov.Id).FirstOrDefault().Quantidade : 0)
                       //Quantidade = 

                       ,
                    ProdutoId = joinedmov.ProdutoId
                       ,

                    CustoUnitario = joinedmov.CustoUnitario


                           ,
                    NumeroSerie = joinedmov.NumeroSerie,
                    Fornecedor = mov.SisFornecedor.Descricao,
                    Unidade = joinedmov.ProdutoUnidade.Unidade.Descricao,
                    ProdutoUnidadeId = joinedmov.ProdutoUnidadeId
                };

                contarEntradas = await query
                    .CountAsync();

                estoqueMovimentoItemDtos = await query
                    .AsNoTracking()
                    .ToListAsync();


                foreach (var item in estoqueMovimentoItemDtos)
                {
                    var unidade = await _unidadeAppService.Obter((long)item.ProdutoUnidadeId);
                    if (unidade != null)
                    {
                        item.Quantidade = item.Quantidade / unidade.Fator;
                        item.CustoTotal = item.CustoUnitario * item.Quantidade;
                    }
                }

                return new ListResultDto<MovimentoItemDto> { Items = estoqueMovimentoItemDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<MovimentoItemDto>> ListarMovimentosItemConsinagdoSelecionados(ListarEstoquePreMovimentoInput input)
        {
            List<long> listaIds = new List<long>();

            string[] listaIdsString = input.Filtro.TrimEnd('-').Split('-');

            foreach (var item in listaIdsString.ToList())
            {
                if (!string.IsNullOrEmpty(item))
                {
                    listaIds.Add(long.Parse(item));
                }
            }

            var contarEntradas = 0;
            List<MovimentoItemDto> estoqueMovimentoItemDtos = new List<MovimentoItemDto>();
            try
            {
                var queryBaixaItem = _estMovimentoBaixaItemRepository.GetAll()
                                      .Where(w => listaIds.Any(a => a == w.EstoqueMovimentoItemId) && w.EstoqueMovimentoBaixaId == input.MovimentoId)
                                      .GroupBy(g => g.EstoqueMovimentoItemId)
                                      .Select(s => new { EstoqueMovimentoItemId = s.Key, Quantidade = s.Sum(soma => soma.Quantidade) });


                var queryBaixaItemRestante = _estMovimentoBaixaItemRepository.GetAll()
                                      .Where(w => listaIds.Any(a => a == w.EstoqueMovimentoItemId))
                                      .GroupBy(g => g.EstoqueMovimentoItemId)
                                      .Select(s => new { EstoqueMovimentoItemId = s.Key, Quantidade = s.Sum(soma => soma.Quantidade) });


                var query = from movimentoItem in _estoqueMovimentoItemRepository.GetAll()
                            join estMovimentoBaixa in _estMovimentoBaixaItemRepository.GetAll().Where(w => w.EstoqueMovimentoBaixaId == input.MovimentoId)
                            on movimentoItem.Id equals estMovimentoBaixa.EstoqueMovimentoItemId
                            into estBaixa
                            from est in estBaixa.DefaultIfEmpty()

                            join baixaItem in queryBaixaItem//_estMovimentoBaixaItemRepository.GetAll().Where(w => w.EstoqueMovimentoBaixaId==0)
                            on movimentoItem.Id equals baixaItem.EstoqueMovimentoItemId
                            into relacionamento
                            from m in relacionamento.DefaultIfEmpty()



                            join baixaItemRestante in queryBaixaItemRestante
                            on movimentoItem.Id equals baixaItemRestante.EstoqueMovimentoItemId
                             into relacionamentoRestante
                            from restante in relacionamentoRestante.DefaultIfEmpty()

                            where listaIds.Any(a => a == movimentoItem.Id)
                            // && (input.MovimentoId == null || input.MovimentoId == 0 || input.MovimentoId == movimentoItem.MovimentoId)



                            //&& queryBaixaItem.Any(a => a.EstoqueMovimentoItemId == movimentoItem.Id
                            //                        && a.Quantidade >= movimentoItem.Quantidade)


                            //  .Where(w => listaIds.Any(a => a == w.Id)  )

                            select (new MovimentoItemDto
                            {
                                Id = movimentoItem.Id,
                                Produto = movimentoItem.Produto.Descricao
                                ,
                                Quantidade = movimentoItem.Quantidade - ((restante.Quantidade != null) ? restante.Quantidade : 0)
                                ,
                                ProdutoId = movimentoItem.ProdutoId
                                ,

                                CustoUnitario = movimentoItem.CustoUnitario
                                    ,

                                NumeroSerie = movimentoItem.NumeroSerie,
                                Unidade = movimentoItem.ProdutoUnidade.Unidade.Descricao,
                                QuantidadeBaixa = m.Quantidade,
                                BaixaItemId = est.Id,
                                ProdutoUnidadeId = movimentoItem.ProdutoUnidadeId
                            });


                contarEntradas = await query
                    .CountAsync();

                estoqueMovimentoItemDtos = await query
                    .AsNoTracking()
                    .ToListAsync();


                foreach (var item in estoqueMovimentoItemDtos)
                {
                    item.Quantidade = _unidadeAppService.ObterQuantidadePorFator((long)item.ProdutoUnidadeId, item.Quantidade);
                    if (item.QuantidadeBaixa != null)
                    {
                        item.QuantidadeBaixa = _unidadeAppService.ObterQuantidadePorFator((long)item.ProdutoUnidadeId, (decimal)item.QuantidadeBaixa);
                        item.CustoTotal = item.CustoUnitario * (decimal)item.QuantidadeBaixa;
                    }

                }




                return new ListResultDto<MovimentoItemDto> { Items = estoqueMovimentoItemDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<MovimentoItemDto> ObterItem(long id)
        {
            try
            {
                var query = _estoqueMovimentoItemRepository
                    .GetAll()
                    .Where(m => m.Id == id);

                var result = await query.FirstOrDefaultAsync();

                var movimentoItemDto = new MovimentoItemDto();

                movimentoItemDto.CustoUnitario = result.CustoUnitario;
                movimentoItemDto.Id = result.Id;
                movimentoItemDto.NumeroSerie = result.NumeroSerie;
                movimentoItemDto.ProdutoId = result.ProdutoId;
                movimentoItemDto.Quantidade = result.Quantidade;
                movimentoItemDto.ProdutoUnidadeId = result.ProdutoUnidadeId;
                movimentoItemDto.MovimentoId = result.MovimentoId;

                return movimentoItemDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public DefaultReturn<EstoqueMovimentoDto> CriarOuEditarBaixaConsignado(EstoqueMovimentoDto input)
        {
            var _retornoPadrao = new DefaultReturn<EstoqueMovimentoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            input.EstTipoOperacaoId = 1;
            var movimento = MapMovimento(input);

            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    PreMovimentoValidacaoService preMovimentoValidacaoService = new PreMovimentoValidacaoService(_unitOfWorkManager
                                                                                                                , _estoquePreMovimentoLoteValidadeRepository
                                                                                                                , _estoquePreMovimentoItemRepository
                                                                                                                , _estoqueMovimentoRepository
                                                                                                                , _estoqueMovimentoItemRepository
                                                                                                                , _estoqueMovimentoLoteValidadeRepository
                                                                                                                , _loteValidadeRepository
                                                                                                                , _estoquePreMovimentoRepository
                                                                                                                , _produtoRepository
                                                                                                                , _produtoUnidadeRepository
                                                                                                                , _estoqueGrupoRepository);
                    List<long> ids = new List<long>();

                    //   valesIds.TrimEnd('-').Split('-').ToList().ForEach(f => ids.Add(long.Parse(f)));
                    //   _retornoPadrao.Errors = preMovimentoValidacaoService.ValidarBaixaVale(movimento, ids);


                    if (_retornoPadrao.Errors.Count() == 0)
                    {
                        long estoquePreMovimentoId = 0;

                        var preMovimento = CarregarPreMovimento(input);
                        preMovimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Conferencia;

                        if (input.Id == 0)
                        {
                            estoquePreMovimentoId = AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.InsertAndGetIdAsync(preMovimento));
                        }
                        else
                        {
                            estoquePreMovimentoId = movimento.EstoquePreMovimentoId;
                            preMovimento.Id = estoquePreMovimentoId;
                            AsyncHelper.RunSync(() => _estoquePreMovimentoRepository.UpdateAsync(preMovimento));
                        }


                        movimento.EstoquePreMovimentoId = estoquePreMovimentoId;
                        movimento.PreMovimentoEstadoId = (long)EnumPreMovimentoEstado.Conferencia;

                        if (input.Id == 0)
                        {
                            movimento.Id = AsyncHelper.RunSync(() => _estoqueMovimentoRepository.InsertAndGetIdAsync(movimento));
                        }
                        else
                        {
                            AsyncHelper.RunSync(() => _estoqueMovimentoRepository.UpdateAsync(movimento));
                        }

                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();

                    _retornoPadrao.ReturnObject = MapMovimento(movimento);
                }

                return _retornoPadrao;

            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;
        }

        public DefaultReturn<EstoqueMovimentoDto> CriarOuEditarBaixaItemConsignado(MovimentoItemDto input)
        {

            var _retornoPadrao = new DefaultReturn<EstoqueMovimentoDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    EstMovimentoBaixaItem estMovimentoBaixaItem = new EstMovimentoBaixaItem();

                    estMovimentoBaixaItem.EstoqueMovimentoBaixaId = input.MovimentoId;
                    estMovimentoBaixaItem.EstoqueMovimentoItemId = input.Id;
                    estMovimentoBaixaItem.Quantidade = input.Quantidade;

                    AsyncHelper.RunSync(() => _estMovimentoBaixaItemRepository.InsertAsync(estMovimentoBaixaItem));


                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }


            //

            return _retornoPadrao;


        }

        public async Task<DefaultReturn<MovimentoItemDto>> CriarOuEditarBaixaItem(MovimentoItemDto input)
        {
            var _retornoPadrao = new DefaultReturn<MovimentoItemDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            var baixaItem = new EstMovimentoBaixaItem();

            try
            {
                if (_retornoPadrao.Errors.Count() == 0)
                {
                    var movimentoItemDto = new MovimentoItemDto();
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {

                        // estoquePreMovimentoItem = MapPreMovimentoItem(input);//.MapTo<EstoquePreMovimentoItem>();
                        var quantidadeBaixaReferencia = _unidadeAppService.ObterQuantidadeReferencia((long)input.ProdutoUnidadeId, (decimal)input.QuantidadeBaixa);
                        if (input.BaixaItemId == null || input.BaixaItemId.Equals((long?)0))
                        {
                            baixaItem = new EstMovimentoBaixaItem
                            {
                                EstoqueMovimentoBaixaId = input.MovimentoId
                                                                      ,
                                EstoqueMovimentoItemId = input.Id
                                                                      ,
                                Quantidade = quantidadeBaixaReferencia
                            };


                            input.Id = await _estMovimentoBaixaItemRepository.InsertAndGetIdAsync(baixaItem);
                        }
                        else
                        {
                            baixaItem = _estMovimentoBaixaItemRepository.Get((long)input.BaixaItemId);

                            if (baixaItem != null)
                            {

                                baixaItem.Quantidade = quantidadeBaixaReferencia;

                                await _estMovimentoBaixaItemRepository.UpdateAsync(baixaItem);
                            }
                        }
                        //  var query = await Obter(input.Id);
                        //  var newObj = MapPreMovimentoItem(query);//.MapTo<EstoquePreMovimentoItem>();

                        //  var estoquePreMovimentoItemDto = MapPreMovimentoItem(newObj);//.MapTo<EstoquePreMovimentoItemDto>();



                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();

                        //  await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);

                        // _retornoPadrao.ReturnObject = baixaItem;
                    }

                    //var produto = _produtoRepository.Get(estoquePreMovimentoItem.ProdutoId);

                    //if (produto != null && !(produto.IsValidade || produto.IsLote))
                    //{
                    //    await _produtoSaldoAppService.AtulizarSaldoPreMovimentoItem(estoquePreMovimentoItem);
                    //}
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }

            return _retornoPadrao;

        }

        EstoquePreMovimento CarregarPreMovimento(EstoqueMovimentoDto movimentoDto)
        {
            EstoquePreMovimento estoquePreMovimento = new EstoquePreMovimento();

            estoquePreMovimento.Id = movimentoDto.EstoquePreMovimentoId;
            estoquePreMovimento.AcrescimoDecrescimo = movimentoDto.AcrescimoDecrescimo;
            estoquePreMovimento.AplicacaoDireta = movimentoDto.AplicacaoDireta;
            estoquePreMovimento.AtendimentoId = movimentoDto.AtendimentoId;
            estoquePreMovimento.CentroCustoId = movimentoDto.CentroCustoId;
            estoquePreMovimento.CFOPId = movimentoDto.CFOPId;
            estoquePreMovimento.Consiginado = movimentoDto.Consiginado;
            estoquePreMovimento.Contabiliza = movimentoDto.Contabiliza;
            estoquePreMovimento.DescontoPer = movimentoDto.DescontoPer;
            estoquePreMovimento.Documento = movimentoDto.Documento;
            estoquePreMovimento.Emissao = movimentoDto.Emissao;
            estoquePreMovimento.EmpresaId = movimentoDto.EmpresaId;
            estoquePreMovimento.EntragaProduto = movimentoDto.EntragaProduto;
            estoquePreMovimento.EstoqueId = movimentoDto.EstoqueId;
            estoquePreMovimento.EstTipoMovimentoId = movimentoDto.EstTipoMovimentoId;
            estoquePreMovimento.EstTipoOperacaoId = movimentoDto.EstTipoOperacaoId;
            estoquePreMovimento.SisFornecedorId = movimentoDto.FornecedorId;
            estoquePreMovimento.FretePer = movimentoDto.FretePer;
            estoquePreMovimento.Frete_SisFornecedorId = movimentoDto.Frete_FornecedorId;
            estoquePreMovimento.ICMSPer = movimentoDto.ICMSPer;
            estoquePreMovimento.IsEntrada = movimentoDto.IsEntrada;
            estoquePreMovimento.MedicoSolcitanteId = movimentoDto.MedicoSolcitanteId;
            estoquePreMovimento.MotivoPerdaProdutoId = movimentoDto.MotivoPerdaProdutoId;
            estoquePreMovimento.Movimento = movimentoDto.Movimento;
            estoquePreMovimento.Observacao = movimentoDto.Observacao;
            estoquePreMovimento.OrdemId = movimentoDto.OrdemId;
            estoquePreMovimento.PacienteId = movimentoDto.PacienteId;
            estoquePreMovimento.PreMovimentoEstadoId = movimentoDto.PreMovimentoEstadoId;
            estoquePreMovimento.Quantidade = movimentoDto.Quantidade;
            estoquePreMovimento.Serie = movimentoDto.Serie;
            estoquePreMovimento.TipoFreteId = movimentoDto.TipoFreteId;
            estoquePreMovimento.TotalDocumento = movimentoDto.TotalDocumento;
            estoquePreMovimento.TotalProduto = movimentoDto.TotalProduto;
            estoquePreMovimento.UnidadeOrganizacionalId = movimentoDto.UnidadeOrganizacionalId;
            estoquePreMovimento.ValorFrete = movimentoDto.ValorFrete;
            estoquePreMovimento.ValorICMS = movimentoDto.ValorICMS;

            return estoquePreMovimento;
        }

        public async Task<ListResultDto<MovimentoIndexDto>> ListarVales(ListarEstoquePreMovimentoInput input)
        {

            var contarEntradas = 0;
            List<EstoqueMovimento> estoqueMovimentos;
            List<MovimentoIndexDto> estoqueMovimentoDtos = new List<MovimentoIndexDto>();


            var movimentoId = long.Parse(input.Filtro);
            try
            {
                var query = from movimento in _estoqueMovimentoRepository.GetAll()
                            join baixa in _estMovimentoBaixaRepository.GetAll()
                            on movimento.Id equals baixa.MovimentoBaixaId

                            join movimentoPrincipal in _estoqueMovimentoRepository.GetAll()
                            on baixa.MovimentoBaixaPrincipalId equals movimentoPrincipal.Id

                            join preMovimento in _estoquePreMovimentoRepository.GetAll()
                            on movimentoPrincipal.EstoquePreMovimentoId equals preMovimento.Id



                            where (preMovimento.Id == movimentoId)
                            select (new MovimentoIndexDto
                            {
                                Id = movimento.Id
                                                                ,

                                DataEmissaoSaida = movimento.Emissao
                                                                ,
                                Documento = movimento.Documento
                                                                ,
                                Empresa = (movimento.Empresa != null) ? movimento.Empresa.NomeFantasia : string.Empty
                                                              ,
                                UsuarioId = movimento.CreatorUserId
                                ,
                                PreMovimentoEstadoId = movimento.PreMovimentoEstadoId

                               ,
                                IsEntrada = movimento.IsEntrada
                               ,
                                TipoMovimento = movimento.EstTipoOperacao.Descricao
                               ,
                                TipoOperacaoId = movimento.EstTipoOperacaoId
                               ,
                                ValorDocumento = movimento.TotalDocumento,
                                Fornecedor = (movimento.SisFornecedor != null) ? movimento.SisFornecedor.Descricao : string.Empty
                            });

                contarEntradas = await query
                    .CountAsync();

                estoqueMovimentoDtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                // estoqueMovimentoDtos = MapPreMovimento(estoqueMovimentos);
                //.MapTo<List<EstoquePreMovimentoDto>>();

                return new ListResultDto<MovimentoIndexDto> { Items = estoqueMovimentoDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<MovimentoIndexDto>> ListarNota(ListarEstoquePreMovimentoInput input)
        {

            var contarEntradas = 0;
            List<EstoqueMovimento> estoqueMovimentos;
            List<MovimentoIndexDto> estoqueMovimentoDtos = new List<MovimentoIndexDto>();


            var movimentoId = long.Parse(input.Filtro);
            try
            {
                var query = from movimento in _estoqueMovimentoRepository.GetAll()
                            join baixa in _estMovimentoBaixaRepository.GetAll()
                            on movimento.Id equals baixa.MovimentoBaixaPrincipalId

                            join movimentoPrincipal in _estoqueMovimentoRepository.GetAll()
                            on baixa.MovimentoBaixaId equals movimentoPrincipal.Id

                            join preMovimento in _estoquePreMovimentoRepository.GetAll()
                            on movimentoPrincipal.EstoquePreMovimentoId equals preMovimento.Id



                            where (preMovimento.Id == movimentoId)
                            select (new MovimentoIndexDto
                            {
                                Id = movimento.Id
                                                                ,

                                DataEmissaoSaida = movimento.Emissao
                                                                ,
                                Documento = movimento.Documento
                                                                ,
                                Empresa = (movimento.Empresa != null) ? movimento.Empresa.NomeFantasia : string.Empty
                                                              ,
                                UsuarioId = movimento.CreatorUserId
                                ,
                                PreMovimentoEstadoId = movimento.PreMovimentoEstadoId

                               ,
                                IsEntrada = movimento.IsEntrada
                               ,
                                TipoMovimento = movimento.EstTipoOperacao.Descricao
                               ,
                                TipoOperacaoId = movimento.EstTipoOperacaoId
                               ,
                                ValorDocumento = movimento.TotalDocumento,
                                Fornecedor = (movimento.SisFornecedor != null) ? movimento.SisFornecedor.Descricao : string.Empty
                            });

                contarEntradas = await query
                    .CountAsync();

                estoqueMovimentoDtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                // estoqueMovimentoDtos = MapPreMovimento(estoqueMovimentos);
                //.MapTo<List<EstoquePreMovimentoDto>>();

                return new ListResultDto<MovimentoIndexDto> { Items = estoqueMovimentoDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<MovimentoItemDto>> ListarItensConsignados(ListarEstoquePreMovimentoInput input)
        {
            var preMovimentoId = long.Parse(input.Filtro);
            List<MovimentoItemDto> estoqueMovimentoDtos = new List<MovimentoItemDto>();

            try
            {
                var query = from movimento in _estoqueMovimentoRepository.GetAll()
                            join baixaItem in _estMovimentoBaixaItemRepository.GetAll()
                            on movimento.Id equals baixaItem.EstoqueMovimentoBaixaId

                            join movimentoItem in _estoqueMovimentoItemRepository.GetAll()
                            on baixaItem.EstoqueMovimentoItemId equals movimentoItem.Id

                            where movimento.EstoquePreMovimentoId == preMovimentoId

                            select (new MovimentoItemDto
                            {
                                Id = movimentoItem.Id,
                                Produto = movimentoItem.Produto.Descricao
                                        ,
                                Quantidade = baixaItem.Quantidade
                                        ,
                                ProdutoId = movimentoItem.ProdutoId
                                        ,

                                CustoUnitario = movimentoItem.CustoUnitario
                                            ,
                                // CustoTotal = movimentoItem.CustoUnitario * movimentoItem.Quantidade
                                //                ,
                                NumeroSerie = movimentoItem.NumeroSerie,
                                Unidade = movimentoItem.ProdutoUnidade.Unidade.Descricao,
                                ProdutoUnidadeId = movimentoItem.ProdutoUnidadeId
                            });

                var contarEntradas = await query
                      .CountAsync();

                estoqueMovimentoDtos = await query
                     .AsNoTracking()
                     .ToListAsync();


                foreach (var item in estoqueMovimentoDtos)
                {
                    var unidade = await _unidadeAppService.Obter((long)item.ProdutoUnidadeId);
                    if (unidade != null)
                    {
                        item.Quantidade = item.Quantidade / unidade.Fator;
                        item.CustoTotal = item.Quantidade * item.CustoUnitario;
                        item.Unidade = unidade.Descricao;
                    }
                }

                return new ListResultDto<MovimentoItemDto> { Items = estoqueMovimentoDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}

