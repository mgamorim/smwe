﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Entradas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas.Exporting;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas
{
    public class EntradaAppService : SWMANAGERAppServiceBase, IEntradaAppService
    {
        private readonly IRepository<Entrada, long> _entradaRepository;
        private readonly IListarEntradasExcelExporter _listarEntradasExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public EntradaAppService(
            IRepository<Entrada, long> entradaRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IListarEntradasExcelExporter listarEntradasExcelExporter
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entradaRepository = entradaRepository;
            _listarEntradasExcelExporter = listarEntradasExcelExporter;
        }

        [UnitOfWork]
        public async Task CriarOuEditar(CriarOuEditarEntrada input)
        {
            try
            {
                var objEntrada = input.MapTo<Entrada>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _entradaRepository.InsertAsync(objEntrada);

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _entradaRepository.UpdateAsync(objEntrada);

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }


               // var entrada = input.MapTo<Entrada>();
                //if (input.Id.Equals(0))
                //{
                //    await _entradaRepository.InsertAsync(entrada);
                //}
                //else
                //{
                //    await _entradaRepository.UpdateAsync(entrada);
                //}
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(CriarOuEditarEntrada input)
        {
            try
            {
                await _entradaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<EntradaDto>> Listar(ListarEntradasInput input)
        {
            var contarEntradas = 0;
            List<Entrada> Entradas;
            List<EntradaDto> EntradasDtos = new List<EntradaDto>();
            try
            {
                var query = _entradaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.NumeroDocumento.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarEntradas = await query
                    .CountAsync();

                Entradas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                EntradasDtos = Entradas
                    .MapTo<List<EntradaDto>>();

                return new PagedResultDto<EntradaDto>(
                    contarEntradas,
                    EntradasDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<EntradaDto>> ListarTodos()
        {
            var contarEntradas = 0;
            List<Entrada> Entradas;
            List<EntradaDto> EntradasDtos = new List<EntradaDto>();
            try
            {
                var query = _entradaRepository
                    .GetAll();

                contarEntradas = await query
                    .CountAsync();

                Entradas = await query
                    .AsNoTracking()
                    .ToListAsync();

                EntradasDtos = Entradas
                    .MapTo<List<EntradaDto>>();

                return new ListResultDto<EntradaDto> { Items = EntradasDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _entradaRepository
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.NumeroDocumento.ToString().ToUpper().Contains(input.ToUpper())
                        )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.NumeroDocumento.ToString() })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarEntradasInput input)
        {
            try
            {
                var result = await Listar(input);
                var Entradas = result.Items;
                return _listarEntradasExcelExporter.ExportToFile(Entradas.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<CriarOuEditarEntrada> Obter(long id)
        {
            try
            {
                var query = _entradaRepository
                    .GetAll()
                    .Where(m => m.Id == id);

                var result = await query.FirstOrDefaultAsync();
                var entrada = result.MapTo<CriarOuEditarEntrada>();

                return entrada;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<PagedResultDto<CriarOuEditarEntradaItem>> ListarItens(ListarEntradasInput input)
        {
            var contarEntradas = 0;
            List<CriarOuEditarEntradaItem> iEntradasItem = null;
            //List<CriarOuEditarEntradaItem> EntradasItemDtos = new List<CriarOuEditarEntradaItem>();
            try
            {
                if (input.Filtro != "0")
                {
                    var query = await Obter(Convert.ToInt64(input.Filtro));
                    //var entrada = query.MapTo<Entrada>();
                    contarEntradas = query.EntradaItem.Count();//entrada.EntradaItem.Count();


                    iEntradasItem = query.EntradaItem.ToList();
                    //.AsNoTracking()
                    //.ToListAsync();

                    //EntradasItemDtos = iEntradasItem
                    //    .MapTo<List<CriarOuEditarEntradaItem>>();
                }
                return new PagedResultDto<CriarOuEditarEntradaItem>(
                    contarEntradas,
                    iEntradasItem
                    );

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
