﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Entradas.Exporting
{
    public interface IListarEntradasExcelExporter
    {
        FileDto ExportToFile(List<EntradaDto> list);
    }
}
