﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque.Dto;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Exporting;
using Abp.Domain.Uow;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques
{
    public class EstoqueAppService : SWMANAGERAppServiceBase, IEstoqueAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Estoque, long> _EstoqueRepositorio;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;
        //private readonly IListarEstoqueExcelExporter _listarEstoqueExcelExporter;

        public IRepository<Estoque, long> EstoqueRepositorio
        {
            get
            {
                return _EstoqueRepositorio;
            }
        }

        public EstoqueAppService(
            IRepository<Estoque, long> EstoqueRepositorio,
            IRepository<EstoqueGrupo, long> estoqueGrupoRepository,
            IUnitOfWorkManager unitOfWorkManager
            //IListarEstoqueExcelExporter listarEstoqueExcelExporter
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _EstoqueRepositorio = EstoqueRepositorio;
            _estoqueGrupoRepository = estoqueGrupoRepository;
            //_listarEstoqueExcelExporter = listarEstoqueExcelExporter;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Estoque_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Estoque_Edit)]
        [UnitOfWork]
        public async Task CriarOuEditar(EstoqueDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var Estoque = input.MapTo<Estoque>();

                    if (input.Id.Equals(0))
                    {
                        var IdEstoque = AsyncHelper.RunSync(() => EstoqueRepositorio.InsertAndGetIdAsync(Estoque));

                        foreach (var item in input.EstoquesGrupo)
                        {
                            EstoqueGrupoDto EstoqueGrupo = new EstoqueGrupoDto();

                            EstoqueGrupo.EstoqueId = IdEstoque;
                            EstoqueGrupo.GrupoId = item.id;
                            EstoqueGrupo.IsTodosItens = item.checado;

                            await _estoqueGrupoRepository.InsertOrUpdateAsync(EstoqueGrupo.MapTo<EstoqueGrupo>());
                        };
                    }
                    else
                    {
                        //Atualiza o estoque
                        await EstoqueRepositorio.UpdateAsync(Estoque);

                        //------------------------------------------------------

                        //Atualiza os estoquesGrupos
                        var aListaEstoquesGrupoBD =
                            from details in _estoqueGrupoRepository.GetAll()
                            where details.EstoqueId == input.Id
                            //select details.Id;
                            select ( new MultiSelectItem {id = (long)details.GrupoId, checado = (bool)details.IsTodosItens });

                        List < MultiSelectItem > aListaIdEstoquesGrupoBD = new List<MultiSelectItem>();
                        List<MultiSelectItem> aListaIdEstoquesGrupoFront = new List<MultiSelectItem>();

                        aListaIdEstoquesGrupoBD = aListaEstoquesGrupoBD.ToList();
                        //aListaIdEstoquesGrupoFront = input.EstoquesGrupo.Select(m => m.id).ToList();
                        aListaIdEstoquesGrupoFront = input.EstoquesGrupo.Select(m => new MultiSelectItem { id = m.id, checado = m.checado}).ToList();

                        //excluindo grupos relacionados
                        var aListaDiferencaExcluir = aListaIdEstoquesGrupoBD.Except(aListaIdEstoquesGrupoFront);

                        foreach (var itemLoop in aListaDiferencaExcluir)
                        {
                            await _estoqueGrupoRepository.DeleteAsync(m => m.GrupoId == itemLoop.id && m.EstoqueId == input.Id);
                        }

                        //incluindo grupos selecionados
                        var aListaDiferencaAdd = aListaIdEstoquesGrupoFront.Except(aListaIdEstoquesGrupoBD);

                        foreach (var itemLoop in aListaDiferencaAdd)
                        {
                            EstoqueGrupoDto EstoqueGrupo = new EstoqueGrupoDto();

                            EstoqueGrupo.EstoqueId = input.Id;
                            EstoqueGrupo.GrupoId = itemLoop.id;
                            EstoqueGrupo.IsTodosItens = itemLoop.checado;

                            await _estoqueGrupoRepository.InsertOrUpdateAsync(EstoqueGrupo.MapTo<EstoqueGrupo>());
                        }
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(EstoqueDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await EstoqueRepositorio.DeleteAsync(input.Id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<EstoqueDto>> Listar(ListarProdutosEstoqueInput input)
        {
            var contarProdutosEstoque = 0;
            List<Estoque> produtosEstoque;
            List<EstoqueDto> produtosEstoqueDtos = new List<EstoqueDto>();
            try
            {
                var query = EstoqueRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarProdutosEstoque = await query
                    .CountAsync();

                produtosEstoque = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                produtosEstoqueDtos = produtosEstoque
                    .MapTo<List<EstoqueDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<EstoqueDto>(
                contarProdutosEstoque,
                produtosEstoqueDtos
                );
        }

        public async Task<FileDto> ListarParaExcel(ListarProdutosEstoqueInput input)
        {
            try
            {
                var query = await Listar(input);

                var produtosEstoqueDtos = query.Items;

                return null;//return _listarEstoqueExcelExporter.ExportToFile(produtosEstoqueDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<EstoqueDto> Obter(long id)
        {
            try
            {
                var result = await EstoqueRepositorio.GetAsync(id);
                var Estoque = result.MapTo<EstoqueDto>();

                //-------------------------------------------------------------------
                var query = _estoqueGrupoRepository
                    .GetAll()
                    .Where(m => m.EstoqueId == id);

                var contarEstoqueGrupo = await query
                    .CountAsync();

                var estoquesGrupo = await query
                    .AsNoTracking()
                    .ToListAsync();

                //-------------------------------------------------------------------

                // VERSAO ANTIGA ARRAY DE IDS

                //long[] idsGrupos;

                //idsGrupos = new long[contarEstoqueGrupo];

                //long i = 0;

                //foreach (var item in estoquesGrupo)
                //{
                //    idsGrupos[i] = (long)item.GrupoId;
                //    i = i + 1;
                //}

                //Estoque.aEstoquesGrupo = idsGrupos;

                //-------------------------------------------------------------------

                // VERSAO NOVA - CHECKS FUNCIONANDOS
                
                Estoque.EstoquesGrupo = new List<MultiSelectItem>();
                foreach (var item in estoquesGrupo)
                {
                    var msi = new MultiSelectItem();
                    msi.id = (long)item.GrupoId;
                    msi.checado = (bool)item.IsTodosItens;
                    Estoque.EstoquesGrupo.Add(msi);
                }   

                return Estoque;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ListResultDto<EstoqueDto>> ListarTodos()
        {
            try
            {
                List<EstoqueDto> objListaDto = new List<EstoqueDto>();

                var query = await EstoqueRepositorio
                    .GetAllListAsync();

                objListaDto = query.MapTo<List<EstoqueDto>>();

                return new ListResultDto<EstoqueDto> { Items = objListaDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await EstoqueRepositorio
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper())
                        )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public async Task<PagedResultDto<EstoqueGrupoDto>> ListarEstoqueGrupo(ListarEstoqueGrupoInput input)
        //public async Task<PagedResultDto<EstoqueDto>> ListarEstoqueGrupo()
        {
            var contarEstoquesGrupo = 0;
            //List<EstoqueGrupo> estoquesGrupo;
            List<EstoqueGrupo> estoquesGrupo = new List<EstoqueGrupo>();
            List<EstoqueGrupoDto> estoquesGruposDtos = new List<EstoqueGrupoDto>();
            try
            {
                var query = _estoqueGrupoRepository
                    .GetAll()
                    .Include(m => m.Grupo);
                //.WhereIf(!input.Filtro.IsNullOrEmpty(), m => m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()));

                contarEstoquesGrupo = await query
                    .CountAsync();

                List<EstoqueGrupo> lista = null;

                //if (contarEstoquesGrupo == 0) {
                if (contarEstoquesGrupo == 100) {

                    for (int i = 0; i < 10; i++)
                    {
                        EstoqueGrupo estqoueGrupo_loop = new EstoqueGrupo();
                        //lista.Add(estqoueGrupo_loop);
                        estqoueGrupo_loop.Codigo = null;
                        estoquesGrupo.Add(estqoueGrupo_loop);
                    }
                }
                else {
                    estoquesGrupo = await query
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();
                };

                estoquesGruposDtos = estoquesGrupo
                    .MapTo<List<EstoqueGrupoDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<EstoqueGrupoDto>(
                contarEstoquesGrupo,
                estoquesGruposDtos
                );
        }

        public async Task<ResultDropdownList> ResultDropdownList(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _EstoqueRepositorio);
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _EstoqueRepositorio);
        }
    }
}
