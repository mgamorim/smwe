﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques
{
    public interface IEstoqueKitAppService : IApplicationService
    {
        EstoqueKitDto Obter(long id);
        List<EstoqueKitDto> ListarTodos();
        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
        List<EstoqueKitItemDto> ObterItensKit(long id);
    }
}
