﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Exporting
{
    public interface IListarEstoqueExcelExporter
    {
        FileDto ExportToFile(List<EstoqueDto> produtoEstoqueDtos);
    }
}