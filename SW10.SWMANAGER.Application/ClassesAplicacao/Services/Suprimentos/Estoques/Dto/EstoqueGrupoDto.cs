﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto
{
    [AutoMap(typeof(EstoqueGrupo))]
    public class EstoqueGrupoDto : CamposPadraoCRUDDto
    {
        /// <summary>
        /// Estoque.
        /// </summary>
        public Estoque Estoque { get; set; }
        public long? EstoqueId { get; set; }

        /// <summary>
        /// Grupo de Produtos.
        /// </summary>
        public Grupo Grupo { get; set; }
        public long? GrupoId { get; set; }

        /// <summary>
        /// IsTodosItens.
        /// </summary>
        public bool? IsTodosItens { get; set; }
    }
}