﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto
{
    [AutoMap(typeof(Estoque))]
    public class EstoqueDto : CamposPadraoCRUDDto
    {
        /// <summary>
        /// Descrição Estoque
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Lista
        /// </summary>
        //public ICollection<EstoqueGrupo> EstoquesGrupo { get; set; }

        public ICollection<MultiSelectItem> EstoquesGrupo { get; set; }

        public long[] aEstoquesGrupo { get; set; }

}

    public class MultiSelectItem
    {
        public long id { get; set; }
        public bool checado { get; set; }
    }
}
