﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques
{
    public class EstoqueKitAppService : SWMANAGERAppServiceBase, IEstoqueKitAppService
    {
        private readonly IRepository<EstoqueKit, long> _estoqueKitRepository;
        private readonly IRepository<EstoqueKitItem, long> _estoqueKitItemRepository;

        public EstoqueKitAppService(IRepository<EstoqueKit, long> estoqueKitRepository
                                  , IRepository<EstoqueKitItem, long> estoqueKitItemRepository)
        {
            _estoqueKitRepository = estoqueKitRepository;
            _estoqueKitItemRepository = estoqueKitItemRepository;
        }

        public EstoqueKitDto Obter(long id)
        {
            var estoqueKit = _estoqueKitRepository.GetAll()
                                                  .Include(i => i.Itens)
                                                  .Include(i => i.Itens.Select(s => s.EstoqueKit))
                                                  .Include(i => i.Itens.Select(s => s.Produto))
                                                  .Where(w => w.Id == id)
                                                  .FirstOrDefault();

            var estoqueKitDto = EstoqueKitDto.Mapear(estoqueKit);

            estoqueKitDto.ItensDto = new List<EstoqueKitItemDto>();

            foreach (var item in estoqueKit.Itens)
            {
                estoqueKitDto.ItensDto.Add(EstoqueKitItemDto.Mapear(item));
            }
            return estoqueKitDto;
        }

        public List<EstoqueKitDto> ListarTodos()
        {
            var estoquesKits = _estoqueKitRepository.GetAll()
                                                    .ToList();

            return EstoqueKitDto.Mapear(estoquesKits);
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _estoqueKitRepository);
        }

        public List<EstoqueKitItemDto> ObterItensKit(long id)
        {
            var itens = _estoqueKitItemRepository.GetAll()
                                                 .Include(i=> i.Produto)
                                                 .Include(i => i.EstoqueKit)
                                                 .Include(i => i.Unidade)
                                                 .Where(w => w.EstoqueKitId == id)
                                                 .ToList();

            return EstoqueKitItemDto.Mapear(itens);
        }

    }
}
