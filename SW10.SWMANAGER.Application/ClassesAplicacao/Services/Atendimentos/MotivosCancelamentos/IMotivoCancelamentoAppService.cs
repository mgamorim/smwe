﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.MotivosCancelamentos
{
    public interface IMotivoCancelamentoAppService : IApplicationService
    {
        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
    }
}
