﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
    public class FilaTerminalIndex
    {
        public long FilaId { get; set; }
        public string DescricaoFila { get; set; }
        public string Cor { get; set; }

    }
}
