﻿using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
   public  class TipoLocalChamadaDto: CamposPadraoCRUDDto
    {
        public static TipoLocalChamadaDto Mapear(TipoLocalChamada tipoLocalChamada)
        {
            TipoLocalChamadaDto tipoLocalChamadaDto = new TipoLocalChamadaDto();

            tipoLocalChamadaDto.Id = tipoLocalChamada.Id;
            tipoLocalChamadaDto.Codigo = tipoLocalChamada.Codigo;
            tipoLocalChamadaDto.Descricao = tipoLocalChamada.Descricao;
            return tipoLocalChamadaDto;
        }

        public static TipoLocalChamada Mapear(TipoLocalChamadaDto tipoLocalChamadaDto)
        {
            TipoLocalChamada tipoLocalChamada = new TipoLocalChamada();

            tipoLocalChamada.Id = tipoLocalChamadaDto.Id;
            tipoLocalChamada.Codigo = tipoLocalChamadaDto.Codigo;
            tipoLocalChamada.Descricao = tipoLocalChamadaDto.Descricao;
            return tipoLocalChamada;
        }
    }
}
