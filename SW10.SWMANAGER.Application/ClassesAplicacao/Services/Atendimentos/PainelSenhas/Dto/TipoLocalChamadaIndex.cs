﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
    public class TipoLocalChamadaIndex
    {
        public long? Id { get; set; }
        public long TipoLocalChamdaId { get; set; }
        public string TipoLocalChamadaDescricao { get; set; }
        public long GridId { get; set; }
    }
}
