﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
    public class MonitorChamadaIndex
    {

        public string LocalChamadaAtual { get; set; }
        public int SenhaAtual { get; set; }
        public string NomePaciente { get; set; }
       


    }
}
