﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
    [AutoMap(typeof(SenhaMovimentacao))]
    public class SenhaMovimentacaoDto : CamposPadraoCRUDDto
    {
        public long SenhaId { get; set; }
        public long? LocalChamadaId { get; set; }
        public long? TipoLocalChamadaId { get; set; }
        public DateTime DataHora { get; set; }
        public DateTime? DataHoraInicial { get; set; }
        public DateTime? DataHoraFinal { get; set; }

        public Senha Senha { get; set; }

        public LocalChamada LocalChamada { get; set; }

        public TipoLocalChamada TipoLocalChamada { get; set; }
    }
}
