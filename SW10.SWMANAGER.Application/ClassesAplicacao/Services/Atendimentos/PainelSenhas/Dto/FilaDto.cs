﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.Empresas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
    [AutoMap(typeof(Fila))]
    public class FilaDto : CamposPadraoCRUDDto
    {
        public int NumeroInicial { get; set; }
        public int NumeroFinal { get; set; }
        public bool IsZera { get; set; }
        public DateTime? HoraZera { get; set; }
        public bool IsAtivo { get; set; }
        public bool IsDomingo { get; set; }
        public bool IsSegunda { get; set; }
        public bool IsTerca { get; set; }
        public bool IsQuarta { get; set; }
        public bool IsQuinta { get; set; }
        public bool IsSexta { get; set; }
        public bool IsSabado { get; set; }
        public string Cor { get; set; }
        public bool IsNaoImprimeSenha { get; set; }
        public long? TipoLocalChamadaInicialId { get; set; }

        public TipoLocalChamadaDto TipoLocalChamadaInicial { get; set; }

        public long? EmpresaId { get; set; }

        public EmpresaDto Empresa { get; set; }

        #region Mapear

        public static FilaDto Mapear(Fila fila)
        {
            FilaDto filaDto = new FilaDto();
            filaDto.Id = fila.Id;
            filaDto.Codigo = fila.Codigo;
            filaDto.Descricao = fila.Descricao;
            filaDto.NumeroInicial = fila.NumeroInicial;
            filaDto.NumeroFinal = fila.NumeroFinal;
            filaDto.IsZera = fila.IsZera;
            filaDto.HoraZera = fila.HoraZera;
            filaDto.IsAtivo = fila.IsAtivo;
            filaDto.IsDomingo = fila.IsDomingo;
            filaDto.IsSegunda = fila.IsSegunda;
            filaDto.IsTerca = fila.IsTerca;
            filaDto.IsQuarta = fila.IsQuarta;
            filaDto.IsQuinta = fila.IsQuinta;
            filaDto.IsSexta = fila.IsSexta;
            filaDto.IsSabado = fila.IsSabado;
            filaDto.Cor = fila.Cor;
            filaDto.IsNaoImprimeSenha = fila.IsNaoImprimeSenha;
            filaDto.TipoLocalChamadaInicialId = fila.TipoLocalChamadaInicialId;
            filaDto.EmpresaId = fila.EmpresaId;

            if (fila.TipoLocalChamadaInicial != null)
            {
                filaDto.TipoLocalChamadaInicial = TipoLocalChamadaDto.Mapear(fila.TipoLocalChamadaInicial);
            }

            if (fila.Empresa != null)
            {
                filaDto.Empresa = new EmpresaDto { Id = fila.Empresa.Id, NomeFantasia = fila.Empresa.NomeFantasia };
            }

            return filaDto;
        }

        public static Fila Mapear(FilaDto filaDto)
        {
            Fila fila = new Fila();

            fila.Id = filaDto.Id;
            fila.Codigo = filaDto.Codigo;
            fila.Descricao = filaDto.Descricao;
            fila.NumeroInicial = filaDto.NumeroInicial;
            fila.NumeroFinal = filaDto.NumeroFinal;
            fila.IsZera = filaDto.IsZera;
            fila.HoraZera = filaDto.HoraZera;
            fila.IsAtivo = filaDto.IsAtivo;
            fila.IsDomingo = filaDto.IsDomingo;
            fila.IsSegunda = filaDto.IsSegunda;
            fila.IsTerca = filaDto.IsTerca;
            fila.IsQuarta = filaDto.IsQuarta;
            fila.IsQuinta = filaDto.IsQuinta;
            fila.IsSexta = filaDto.IsSexta;
            fila.IsSabado = filaDto.IsSabado;
            fila.Cor = filaDto.Cor;
            fila.IsNaoImprimeSenha = filaDto.IsNaoImprimeSenha;
            fila.TipoLocalChamadaInicialId = filaDto.TipoLocalChamadaInicialId;

            if (filaDto.TipoLocalChamadaInicial != null)
            {
                fila.TipoLocalChamadaInicial = TipoLocalChamadaDto.Mapear(filaDto.TipoLocalChamadaInicial);
            }


            if (filaDto.Empresa != null)
            {
                fila.Empresa = new Empresa { Id = filaDto.Empresa.Id, NomeFantasia = filaDto.Empresa.NomeFantasia };
            }


            return fila;
        }

        #endregion

    }

}
