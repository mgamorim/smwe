﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto
{
    [AutoMap(typeof(Senha))]
    public class SenhaDto : CamposPadraoCRUDDto
    {
        public DateTime DataHora { get; set; }
        public int Numero { get; set; }
        public long FilaId { get; set; }
        public long? AtendimentoId { get; set; }

        public FilaDto Fila { get; set; }

        public AtendimentoDto Atendimento { get; set; }
    }
}
