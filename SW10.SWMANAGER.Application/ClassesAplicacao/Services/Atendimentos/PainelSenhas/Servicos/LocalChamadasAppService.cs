﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using Abp.UI;
using Abp.Domain.Uow;
using Abp.Threading;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Servicos
{
    public class LocalChamadasAppService : SWMANAGERAppServiceBase, ILocalChamadasAppService
    {
        private readonly IRepository<LocalChamada, long> _localChamadaRepository;
        private readonly IRepository<SenhaMovimentacao, long> _senhaMovimentacaoRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public LocalChamadasAppService(IRepository<LocalChamada, long> localChamadaRepository
                                      , IRepository<SenhaMovimentacao, long> senhaMovimentacaoRepository
            , IUnitOfWorkManager unitOfWorkManager)
        {
            _localChamadaRepository = localChamadaRepository;
            _senhaMovimentacaoRepository = senhaMovimentacaoRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<ResultDropdownList> ListarLocalChamadaDropdown(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _localChamadaRepository);

        }

        public async Task<ResultDropdownList> ListarLocalChamadaPorTipoDropdown(DropdownInput dropdownInput)
        {


            long tipoLocalChamadaId;

            long.TryParse(dropdownInput.filtro, out tipoLocalChamadaId);


            //var localChamada = _localChamadaRepository.GetAll()
            //                                   .Include(i => i.TipoLocalChamada)
            //                                   .Where(w => w.Id == localChamadaId)
            //                                   .FirstOrDefault();


            return await ListarDropdownLambda(dropdownInput
                                                     , _localChamadaRepository
                                                     , m =>
                                                      (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                     || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                      &&
                                                     (m.TipoLocalChamadaId == tipoLocalChamadaId)


                                                     , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );

        }

        public async Task<PagedResultDto<SenhaIndex>> ListarSenhasNaoChamadasIndex(ListarPainelSenhaInput input)
        {
            var contarSenhas = 0;
            List<SenhaIndex> senhasIndex = new List<SenhaIndex>();
            try
            {
                var query = _senhaMovimentacaoRepository
                    .GetAll()
                    .Include(i => i.TipoLocalChamada)
                    .Include(i => i.Senha)
                    .Include(i => i.Senha.Atendimento.Paciente.SisPessoa)
                    .Where(w => (input.TipoLocalChamadaId == null || w.TipoLocalChamadaId == input.TipoLocalChamadaId)
                            && w.LocalChamadaId == null
                              );

                contarSenhas = await query
                    .CountAsync();

                var movimentosSenhas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in movimentosSenhas)
                {
                    var senha = new SenhaIndex();

                    senha.SenhaMovimentoId = item.Id;
                    senha.NumeroSenha = item.Senha.Numero;
                    senha.NomePaciente = item.Senha.Atendimento?.Paciente.NomeCompleto;
                    senha.TipoLocalChamada = item.TipoLocalChamada?.Descricao;
                    senha.TipoLocalChamadaId = item.TipoLocalChamadaId;

                    senhasIndex.Add(senha);
                }

                return new PagedResultDto<SenhaIndex>(
                contarSenhas,
                senhasIndex
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task AlterarTipoLocalChamadaSenha(long senhaMovAtualId, long tipoLocalChamadaNovoId)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var senhaMovimentacao = _senhaMovimentacaoRepository.GetAll()
                                                                    .Where(w => w.Id == senhaMovAtualId)
                                                                    .FirstOrDefault();

                if (senhaMovimentacao != null)
                {
                    senhaMovimentacao.DataHoraFinal = DateTime.Now;

                    //Qual deve ser o local d chamada correto????
                    senhaMovimentacao.LocalChamadaId = 1;

                    SenhaMovimentacao senhaMovimentacaoNova = new SenhaMovimentacao();

                    senhaMovimentacaoNova.DataHora = DateTime.Now;
                    senhaMovimentacaoNova.SenhaId = senhaMovimentacao.SenhaId;
                    senhaMovimentacaoNova.TipoLocalChamadaId = tipoLocalChamadaNovoId;

                    AsyncHelper.RunSync(() => _senhaMovimentacaoRepository.InsertAsync(senhaMovimentacaoNova));

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();


                }
            }
        }

        public async Task<LocalChamadaDto> Obter(long id)
        {
            var localChamada = _localChamadaRepository.GetAll()
                                                      .Include(i=> i.TipoLocalChamada)
                                                      .Where(w => w.Id == id)
                                                      .FirstOrDefault();

            if(localChamada!=null)
            {
                return LocalChamadaDto.Mapear(localChamada);
            }
            else
            {
                return null;
            }
        }

    }
}
