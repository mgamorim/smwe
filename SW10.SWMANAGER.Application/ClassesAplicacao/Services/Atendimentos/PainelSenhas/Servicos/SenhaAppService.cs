﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Servicos
{
    public class SenhaAppService : SWMANAGERAppServiceBase, ISenhaAppService
    {
        private readonly IRepository<Senha, long> _senhaRepository;
        private readonly IRepository<SenhaMovimentacao, long> _senhaMovimentacaoRepository;
        private readonly IRepository<LocalChamada, long> _localChamadaRepository;
        private readonly IRepository<SenhaMovimentacaoPainel, long> _senhaMovimentacaoPainelRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public SenhaAppService(IRepository<Senha, long> senhaRepository
                             , IRepository<SenhaMovimentacao, long> senhaMovimentacaoRepository
                             , IRepository<LocalChamada, long> localChamadaRepository
                             , IRepository<SenhaMovimentacaoPainel, long> senhaMovimentacaoPainelRepository
                             , IUnitOfWorkManager unitOfWorkManager
            )
        {
            _senhaRepository = senhaRepository;
            _senhaMovimentacaoRepository = senhaMovimentacaoRepository;
            _localChamadaRepository = localChamadaRepository;
            _senhaMovimentacaoPainelRepository = senhaMovimentacaoPainelRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }


        public async Task<ResultDropdownList> ListarSenhasPorlocalChamadaDropdown(DropdownInput dropdownInput)
        {
                long localChamadaId;

                long.TryParse(dropdownInput.filtro, out localChamadaId);


                var localChamada = _localChamadaRepository.GetAll()
                                                   .Include(i => i.TipoLocalChamada)
                                                   .Where(w => w.Id == localChamadaId)
                                                   .FirstOrDefault();


                return await ListarDropdownLambda(dropdownInput
                                                         , _senhaMovimentacaoRepository
                                                         , m =>
                                                         // (string.IsNullOrEmpty(dropdownInput.search) || m.Senha.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                         //|| m.Senha.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                         // && 
                                                         (m.TipoLocalChamadaId == localChamada.TipoLocalChamadaId
                                                          && m.LocalChamadaId == null)


                                                        , p => new DropdownItems { id = p.Id, text = p.Senha.Atendimento != null ? p.Senha.Atendimento.Paciente.NomeCompleto : p.Senha.Numero.ToString() }
                                                        , o => o.Descricao
                                                        );
        }

        public MonitorChamadaIndex CarregarPainelSenha(long painelId)
        {
            var movimentacaoPaineis = _senhaMovimentacaoPainelRepository.GetAll()
                                                                        .Include(i => i.SenhaMovimentacao)
                                                                        .Include(i => i.SenhaMovimentacao.LocalChamada)
                                                                        .Include(i => i.SenhaMovimentacao.Senha)
                                                                        .Include(i => i.SenhaMovimentacao.Senha.Atendimento)
                                                                        .Include(i => i.SenhaMovimentacao.Senha.Atendimento.Paciente)
                                                                        .Include(i => i.SenhaMovimentacao.Senha.Atendimento.Paciente.SisPessoa)
                                                                         .Where(w => w.IsMostra && w.PainelId == painelId)
                                                                         .OrderBy(o => o.Id)
                                                                         .FirstOrDefault();

            MonitorChamadaIndex monitorChamadaIndex = new MonitorChamadaIndex();

            if (movimentacaoPaineis != null)
            {
                monitorChamadaIndex.LocalChamadaAtual = movimentacaoPaineis.SenhaMovimentacao.LocalChamada.Descricao;
                monitorChamadaIndex.SenhaAtual = movimentacaoPaineis.SenhaMovimentacao.Senha.Numero;

                int? length = movimentacaoPaineis.SenhaMovimentacao.Senha.Atendimento?.Paciente.NomeCompleto.Length;

                int tamanho = (int)(length < 30 ? length : 30);

                monitorChamadaIndex.NomePaciente = movimentacaoPaineis.SenhaMovimentacao.Senha.Atendimento?.Paciente.NomeCompleto?.Substring(0, tamanho );

                movimentacaoPaineis.IsMostra = false;

                return monitorChamadaIndex;
            }
            else
            {
                return null;
            }

        }

        public async Task<SenhaDto> Obter(long id)
        {
            try
            {
                var senhaDto = await _senhaRepository.GetAsync(id);

                return senhaDto.MapTo<SenhaDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<SenhaMovimentacaoDto> ObterMovimento(long id)
        {
            try
            {
                var senha = await _senhaMovimentacaoRepository.GetAsync(id);

                return senha.MapTo<SenhaMovimentacaoDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork]
        public async Task CriarMovimento(long atendimentoId, long tipoLocalChamadaId)
        {
            try
            {
                using (var _unitOfWork = _unitOfWorkManager.Begin())
                {

                    var movimentacaoSenha = _senhaMovimentacaoRepository.GetAll()
                                            .Include(i => i.Senha)
                                            .Include(i => i.TipoLocalChamada)
                                            .Where(w => w.Senha.AtendimentoId == atendimentoId)
                                            .ToList()
                                            .FirstOrDefault();

                    if (movimentacaoSenha != null)
                    {
                        //movimentacaoSenha.Senha.AtendimentoId = atendimentoId;
                        if (!movimentacaoSenha.DataHoraFinal.HasValue)
                        {
                            movimentacaoSenha.DataHoraFinal = DateTime.Now;
                        }

                        //if (movimentacaoSenha.TipoLocalChamada != null && movimentacaoSenha.TipoLocalChamada.TipoLocalChamadaProximoId != null)
                        //{
                        var senhaMovimentacao = new SenhaMovimentacao();

                        senhaMovimentacao.SenhaId = movimentacaoSenha.Senha.Id;
                        senhaMovimentacao.TipoLocalChamadaId = tipoLocalChamadaId;
                        senhaMovimentacao.DataHora = DateTime.Now;

                        await _senhaMovimentacaoRepository.InsertAsync(senhaMovimentacao);
                        //}

                    }
                    _unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    _unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }
    }
}
