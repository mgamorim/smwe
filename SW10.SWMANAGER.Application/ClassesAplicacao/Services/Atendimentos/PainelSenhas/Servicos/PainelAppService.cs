﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.Dto;
using Newtonsoft.Json;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Servicos
{
    public class PainelAppService : SWMANAGERAppServiceBase, IPainelAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Painel, long> _painelRepository;

        public PainelAppService(IRepository<Painel, long> painelRepository
                               , IUnitOfWorkManager unitOfWorkManager)
        {
            _painelRepository = painelRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }


        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _painelRepository.DeleteAsync(id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PainelDto> Obter(long id)
        {
            try
            {
                var result = _painelRepository.GetAll()
                                              .Include(i => i.PaineisTipoLocaisChamadas)
                                              .Include(i => i.PaineisTipoLocaisChamadas.Select(s => s.TipoLocalChamada))
                                              .Where(w => w.Id == id)
                                              .FirstOrDefault();

                return PainelDto.Mapear(result);

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<PainelDto>> Listar(ListarPainelSenhaInput input)
        {
            var contarPaineis = 0;
            List<Painel> paineis;
            List<PainelDto> paineisDtos = new List<PainelDto>();
            try
            {
                var query = _painelRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarPaineis = await query
                    .CountAsync();

                paineis = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in paineis)
                {
                    paineisDtos.Add(PainelDto.Mapear(item));
                }


            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<PainelDto>(
                contarPaineis,
                paineisDtos
                );
        }

        public DefaultReturn<PainelDto> CriarOuEditar(PainelDto input)
        {
            var _retornoPadrao = new DefaultReturn<PainelDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var tiposLoaisChamadas = JsonConvert.DeserializeObject<List<TipoLocalChamadaIndex>>(input.TipoLocalChamadas);
                  


                 //   ContaAdministrativaValidacaoService contaAdministrativaValidacaoService = new ContaAdministrativaValidacaoService();

                   // _retornoPadrao = contaAdministrativaValidacaoService.Validar(input);

                    if (_retornoPadrao.Errors.Count == 0)
                    {
                        if (input.Id == 0)
                        {
                            Painel painel = PainelDto.Mapear(input);

                            painel.PaineisTipoLocaisChamadas = new List<PainelTipoLocalChamada>();

                            foreach (var item in tiposLoaisChamadas)
                            {
                                var painelTipoLocalChamada = new PainelTipoLocalChamada();
                                painelTipoLocalChamada.TipoLocalChamadaId = item.TipoLocalChamdaId;

                                painel.PaineisTipoLocaisChamadas.Add(painelTipoLocalChamada);
                            }

                            AsyncHelper.RunSync(() => _painelRepository.InsertAsync(painel));
                            _retornoPadrao.ReturnObject = PainelDto.Mapear(painel);
                        }
                        else
                        {
                            var painel = _painelRepository.GetAll()
                                .Include(i=> i.PaineisTipoLocaisChamadas)
                                                                         .Where(w => w.Id == input.Id)
                                                                         .FirstOrDefault();

                            if (painel != null)
                            {
                                painel.Codigo = input.Codigo;
                                painel.Descricao = input.Descricao;

                                #region Tipo local chamada
                                //Exclui 
                                painel.PaineisTipoLocaisChamadas.RemoveAll(r => !tiposLoaisChamadas.Any(a => a.Id == r.Id));
                               

                                //inclui
                                foreach (var tipoLocalChamada in tiposLoaisChamadas.Where(w => w.Id == 0 || w.Id == null))
                                {
                                    painel.PaineisTipoLocaisChamadas.Add(new PainelTipoLocalChamada
                                    {
                                        TipoLocalChamadaId = tipoLocalChamada.TipoLocalChamdaId
                                    });
                                }


                                #endregion


                                AsyncHelper.RunSync(() => _painelRepository.UpdateAsync(painel));
                                _retornoPadrao.ReturnObject = PainelDto.Mapear(painel);
                            }
                        }
                    }
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _painelRepository);
        }

    }

}
