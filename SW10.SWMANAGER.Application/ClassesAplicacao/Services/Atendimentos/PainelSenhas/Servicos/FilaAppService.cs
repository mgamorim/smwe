﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Servicos
{
    public class FilaAppService : SWMANAGERAppServiceBase, IFilaAppService
    {
        private readonly IRepository<Fila, long> _filaRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FilaAppService(IRepository<Fila, long> filaRepository
            , IUnitOfWorkManager unitOfWorkManager)
        {
            _filaRepository = filaRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultDto<FilaIndex>> Listar(ListaFilaInput input)
        {
            try
            {
                List<FilaIndex> filasDto = new List<FilaIndex>();

                filasDto = _filaRepository.GetAll()
                                          .Include(i => i.TipoLocalChamadaInicial)
                                                     .Where(w => (input.Filtro == "" || input.Filtro == null)
                                                                || w.Descricao.ToString().ToUpper().Contains(input.Filtro.ToUpper()))
                                                      .Select(s => new FilaIndex
                                                      {
                                                          Id = s.Id,
                                                          Codigo = s.Codigo,
                                                          Descricao = s.Descricao,
                                                          TipoLocalChamadaInicial = s.TipoLocalChamadaInicial.Descricao
                                                      }).ToList();

                return new PagedResultDto<FilaIndex>(
                  filasDto.Count,
                  filasDto
                  );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FilaDto> Obter(long id)
        {
            try
            {
                var query = _filaRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Include(i => i.TipoLocalChamadaInicial)
                    .Include(i => i.Empresa)
                    .FirstOrDefault();

                var filaDto = FilaDto.Mapear(query);

                return filaDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task Excluir(long id)
        {
            try
            {
                await _filaRepository.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }


        public DefaultReturn<FilaDto> CriarOuEditar(FilaDto input)
        {
            var _retornoPadrao = new DefaultReturn<FilaDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    // ContaAdministrativaValidacaoService contaAdministrativaValidacaoService = new ContaAdministrativaValidacaoService();

                    // _retornoPadrao = contaAdministrativaValidacaoService.Validar(input);

                    if (_retornoPadrao.Errors.Count == 0)
                    {
                        if (input.Id == 0)
                        {
                            Fila fila = FilaDto.Mapear(input);

                            fila.UltimaZera = DateTime.Now;
                            AsyncHelper.RunSync(() => _filaRepository.InsertAsync(fila));
                            _retornoPadrao.ReturnObject = FilaDto.Mapear(fila);
                        }
                        else
                        {
                            var fila = _filaRepository.GetAll()
                                                      .Where(w => w.Id == input.Id)
                                                      .FirstOrDefault();

                            if (fila != null)
                            {
                                fila.Codigo = input.Codigo;
                                fila.Descricao = input.Descricao;
                                fila.NumeroInicial = input.NumeroInicial;
                                fila.NumeroFinal = input.NumeroFinal;
                                fila.TipoLocalChamadaInicialId = input.TipoLocalChamadaInicialId;
                                fila.IsZera = input.IsZera;
                                if (input.HoraZera != null)
                                {
                                    fila.HoraZera = new DateTime(1900, 1, 1, ((DateTime)input.HoraZera).Hour, ((DateTime)input.HoraZera).Minute, 0);
                                }
                                else
                                {
                                    fila.HoraZera = null;
                                }
                                fila.Cor = input.Cor;
                                fila.IsNaoImprimeSenha = input.IsNaoImprimeSenha;

                                fila.IsAtivo = input.IsAtivo;
                                fila.IsDomingo = input.IsDomingo;
                                fila.IsSegunda = input.IsSegunda;
                                fila.IsTerca = input.IsTerca;
                                fila.IsQuarta = input.IsQuarta;
                                fila.IsQuinta = input.IsQuinta;
                                fila.IsSexta = input.IsSexta;
                                fila.IsSabado = input.IsSabado;

                                fila.EmpresaId = input.EmpresaId;


                                AsyncHelper.RunSync(() => _filaRepository.UpdateAsync(fila));
                                _retornoPadrao.ReturnObject = FilaDto.Mapear(fila);
                            }
                        }
                    }
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                var erro = new ErroDto();

                if (ex.InnerException != null)
                {
                    var inner = ex.InnerException;
                    erro = new ErroDto();
                    erro.CodigoErro = inner.HResult.ToString();
                    erro.Descricao = inner.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
                else
                {
                    erro.CodigoErro = ex.HResult.ToString();
                    erro.Descricao = ex.Message.ToString();
                    _retornoPadrao.Errors.Add(erro);
                }
            }
            return _retornoPadrao;
        }


    }
}
