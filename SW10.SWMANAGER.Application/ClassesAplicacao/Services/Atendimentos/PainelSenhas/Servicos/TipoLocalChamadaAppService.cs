﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Servicos
{
   public  class TipoLocalChamadaAppService: SWMANAGERAppServiceBase, ITipoLocalChamadaAppService
    {
        private readonly IRepository<TipoLocalChamada, long> _tipoLocalChamadaRepository;

        public TipoLocalChamadaAppService(IRepository<TipoLocalChamada, long> tipoLocalChamadaRepository)
        {
            _tipoLocalChamadaRepository = tipoLocalChamadaRepository;
        }

        public async Task<ResultDropdownList> ListarTipoLocalChamadaDropdown(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _tipoLocalChamadaRepository);

        }





    }
}
