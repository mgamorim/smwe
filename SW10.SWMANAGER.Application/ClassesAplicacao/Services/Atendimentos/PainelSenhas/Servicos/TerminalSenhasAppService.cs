﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Servicos
{
    public class TerminalSenhasAppService : SWMANAGERAppServiceBase, ITerminalSenhasAppService
    {
        private readonly IRepository<Fila, long> _filaRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Senha, long> _senhaRepository;
        private readonly IRepository<SenhaMovimentacao, long> _senhaMovimentacaoRepository;
        private readonly IRepository<PainelTipoLocalChamada, long> _painelTipoLocalChamadaRepository;
        private readonly IRepository<SenhaMovimentacaoPainel, long> _senhaMovimentacaoPainelRepository;

        public TerminalSenhasAppService(IRepository<Fila, long> filaRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<Senha, long> senhaRepository
            , IRepository<SenhaMovimentacao, long> senhaMovimentacaoRepository
            , IRepository<PainelTipoLocalChamada, long> painelTipoLocalChamadaRepository
            , IRepository<SenhaMovimentacaoPainel, long> senhaMovimentacaoPainelRepository)
        {
            _filaRepository = filaRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _senhaRepository = senhaRepository;
            _senhaMovimentacaoRepository = senhaMovimentacaoRepository;
            _painelTipoLocalChamadaRepository = painelTipoLocalChamadaRepository;
            _senhaMovimentacaoPainelRepository = senhaMovimentacaoPainelRepository;
        }

        public async Task<SenhaIndex> GerarSenha(long filaId)
        {
            SenhaIndex senhaIndex = null;
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var fila = _filaRepository.GetAll()
                                      .Include(i=> i.Empresa)
                                      .Where(w => w.Id == filaId)
                                      .FirstOrDefault();

                //var ultimaSenha = _senhaRepository.GetAll()
                //                                  .Where(w => w.FilaId == filaId)
                //                                  .ToList()
                //                                  .LastOrDefault();

                var numeroSenha = 0;
                if(fila.UltimaSenha==0)
                {
                    numeroSenha = fila.UltimaSenha = fila.NumeroInicial;
                    fila.UltimaZera = DateTime.Now;
                }
                else
                {
                    if(fila.UltimaSenha < fila.NumeroFinal)//   && !(((DateTime)fila.HoraZera).TimeOfDay < DateTime.Now.TimeOfDay && (fila.UltimaZera.Date < DateTime.Now.Date  ) ))
                    {
                        numeroSenha = ++fila.UltimaSenha ;
                    }
                    else
                    {
                        numeroSenha = fila.UltimaSenha = fila.NumeroInicial;
                        fila.UltimaZera = DateTime.Now;
                    }
                }

                var senha = new Senha();

                senha.Numero = numeroSenha;
                senha.DataHora = DateTime.Now;
                senha.FilaId = filaId;

                var senhaId = AsyncHelper.RunSync(() => _senhaRepository.InsertAndGetIdAsync(senha));

                SenhaMovimentacao senhaMov = new SenhaMovimentacao();

                senhaMov.SenhaId = senhaId;
                senhaMov.TipoLocalChamadaId = fila.TipoLocalChamadaInicialId;
                senhaMov.DataHora = DateTime.Now;

                AsyncHelper.RunSync(() => _senhaMovimentacaoRepository.InsertAndGetIdAsync(senhaMov));
                

                unitOfWork.Complete();
                _unitOfWorkManager.Current.SaveChanges();
                unitOfWork.Dispose();

                senhaIndex = new SenhaIndex();

                senhaIndex.NumeroSenha = senha.Numero;
                senhaIndex.TipoLocalChamada = fila.Descricao;
                senhaIndex.Data = senhaMov.DataHora;
                senhaIndex.Hospital = fila.Empresa?.NomeFantasia;



                //Criar Impressão da senha

            }
            return senhaIndex;
        }

        public List<FilaTerminalIndex> ListarFilasDisponiveis()
        {
            List<FilaTerminalIndex> filasIndex = new List<FilaTerminalIndex>();

            var diaSemana = DateTime.Now.DayOfWeek;

            var filas = _filaRepository.GetAll()
                                       .Where(w => w.IsAtivo
                                               && !w.IsNaoImprimeSenha
                                               &&( (diaSemana == DayOfWeek.Sunday && w.IsDomingo)
                                               || (diaSemana == DayOfWeek.Monday && w.IsSegunda)
                                               || (diaSemana == DayOfWeek.Tuesday && w.IsTerca)
                                               || (diaSemana == DayOfWeek.Wednesday && w.IsQuarta)
                                               || (diaSemana == DayOfWeek.Thursday && w.IsQuinta)
                                               || (diaSemana == DayOfWeek.Friday && w.IsSexta)
                                               || (diaSemana == DayOfWeek.Saturday && w.IsSabado))
                                               ).ToList();

            foreach (var fila in filas)
            {
                FilaTerminalIndex filaIndex = new FilaTerminalIndex();

                filaIndex.FilaId = fila.Id;
                filaIndex.DescricaoFila = fila.Descricao;
                filaIndex.Cor = fila.Cor;

                filasIndex.Add(filaIndex);
            }

            return filasIndex;
        }

        public async Task ChamarSenha(long tipoLocalChamada, long localChamadaId, long senhaMovimentacaoId)
        {

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {


                var senhaMovimentacao = _senhaMovimentacaoRepository.GetAll()
                                                                .Where(w => w.Id == senhaMovimentacaoId)
                                                                .FirstOrDefault();


                var paineisTipoLocalChamadas = _painelTipoLocalChamadaRepository.GetAll()
                                                                              .Where(w => w.TipoLocalChamadaId == tipoLocalChamada
                                                                                       && w.PainelId !=null
                                                                                       && !w.Painel.IsDeleted)
                                                                              .ToList();


                foreach (var item in paineisTipoLocalChamadas)
                {
                    SenhaMovimentacaoPainel senhaMovimentacaoPainel = new SenhaMovimentacaoPainel();

                    senhaMovimentacaoPainel.IsMostra = true;
                    senhaMovimentacaoPainel.PainelId = (long)item.PainelId;
                    senhaMovimentacaoPainel.SenhaMovimentacaoId = senhaMovimentacaoId;

                    AsyncHelper.RunSync(() => _senhaMovimentacaoPainelRepository.InsertAndGetIdAsync(senhaMovimentacaoPainel));

                }

                senhaMovimentacao.LocalChamadaId = localChamadaId;
                senhaMovimentacao.DataHoraInicial = DateTime.Now;


                unitOfWork.Complete();
                _unitOfWorkManager.Current.SaveChanges();
                unitOfWork.Dispose();

            }
        }
    }
}
