﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Interfaces
{
   public interface ITerminalSenhasAppService: IApplicationService
    {
        Task<SenhaIndex> GerarSenha(long filaId);
        List<FilaTerminalIndex> ListarFilasDisponiveis();
        Task ChamarSenha(long tipoLocalChamada, long localChamadaId, long senhaMovimentacaoId);
    }
}
