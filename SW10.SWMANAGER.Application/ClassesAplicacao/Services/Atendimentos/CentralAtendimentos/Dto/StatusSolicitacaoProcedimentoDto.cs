﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.CentralAutorizacoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos.Dto
{
    [AutoMap(typeof(StatusSolicitacaoProcedimento))]
    public class StatusSolicitacaoProcedimentoDto: CamposPadraoCRUDDto
    {
    }
}
