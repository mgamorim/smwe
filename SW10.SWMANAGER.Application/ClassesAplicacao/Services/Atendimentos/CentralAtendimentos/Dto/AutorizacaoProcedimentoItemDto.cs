﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.CentralAutorizacoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos.Dto
{
    [AutoMap(typeof(AutorizacaoProcedimentoItem))]
    public class AutorizacaoProcedimentoItemDto: CamposPadraoCRUDDto
    {
        public long FaturamentoItemId { get; set; }
        public string Senha { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public bool IsOrtese { get; set; }
        public string AutorizadoPor { get; set; }
        public int? QuantidadeSolicitada { get; set; }
        public int? QuantidadeAutorizada { get; set; }
        public long? StatusId { get; set; }
        public string Observacao { get; set; }
        public long AutorizacaoProcedimentoId { get; set; }
        public AutorizacaoProcedimentoDto AutorizacaoProcedimento { get; set; }
        public FaturamentoItemDto FaturamentoItem { get; set; }
        public int IdGrid { get; set; }
        public string FaturamentoItemStr { get; set; }
        public bool ItemSelecionado { get; set; }
        public string StatusDescricao { get; set; }
        public string FaturamentoItemDescricao { get; set; }
    }
}
