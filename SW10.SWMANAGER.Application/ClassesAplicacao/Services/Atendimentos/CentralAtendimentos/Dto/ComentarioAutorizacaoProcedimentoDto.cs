﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.CentralAutorizacoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos.Dto
{
    [AutoMap(typeof(ComentarioAutorizacaoProcedimento))]
    public class ComentarioAutorizacaoProcedimentoDto : CamposPadraoCRUDDto
    {
        public long AutorizacaoProcedimentoId { get; set; }
        public DateTime DataRegistro { get; set; }
        public string Conteudo { get; set; }
        public long? UsuarioId { get; set; }
        public string NomeUsuario { get; set; }

        public AutorizacaoProcedimentoDto AutorizacaoProcedimento { get; set; }
        
    }
}
