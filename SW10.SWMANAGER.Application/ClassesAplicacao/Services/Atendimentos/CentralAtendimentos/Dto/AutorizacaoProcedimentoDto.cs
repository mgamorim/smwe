﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.CentralAutorizacoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos.Dto
{
    [AutoMap(typeof(AutorizacaoProcedimento))]
    public class AutorizacaoProcedimentoDto : CamposPadraoCRUDDto
    {
        public long? SolicitanteId { get; set; }
        public long? PacienteId { get; set; }
        public long ConvenioId { get; set; }
        public long FaturamentoItemId { get; set; }
        public string Autorizacao { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public string AutorizadoPor { get; set; }
        public string Observacao { get; set; }
        public string NumeroGuia { get; set; }
        public bool IsOstese { get; set; }

        public MedicoDto Solicitante { get; set; }
        public PacienteDto Paciente { get; set; }
        public ConvenioDto Convenio { get; set; }
        public FaturamentoItemDto FaturamentoItem { get; set; }
        public string Itens { get; set; }
       

        public List<ComentarioAutorizacaoProcedimentoDto> Comentarios { get; set; }

    }
}
