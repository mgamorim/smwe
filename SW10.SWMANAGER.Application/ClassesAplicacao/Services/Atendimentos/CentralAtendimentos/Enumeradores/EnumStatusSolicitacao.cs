﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos.Enumeradores
{
    public enum EnumStatusSolicitacao
    {
        Autorizado=1,
        EmAnalise=2,
        Negado=3,
        AguardandoJustificativaTecnicaDoSolicitante=4,
        AguardandoDocumentacaoPrestador=5,
        SolicitaçãoCancelada=6,
        AutorizadoParcialmente=7
    }
}
