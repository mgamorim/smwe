﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.CentralAtendimentos
{
    public interface IAutorizacaoProcedimentoAppService : IApplicationService
    {
        Task<PagedResultDto<AutorizacaoProcedimentoIndexDto>> Listar(ListarAutorizacaoProcedimentoInput input);
        PagedResultDto<AutorizacaoProcedimentoItemDto> ListarItensJson(string json);
        DefaultReturn<AutorizacaoProcedimentoDto> CriarOuEditarAutorizacaoProcedimento(AutorizacaoProcedimentoDto input);
        Task<AutorizacaoProcedimentoDto> Obter(long id);
        Task<List<AutorizacaoProcedimentoItemDto>> ObterItens(long id);
    }
}
