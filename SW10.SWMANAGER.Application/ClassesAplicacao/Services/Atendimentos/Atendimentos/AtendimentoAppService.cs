﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.PainelSenha;
using SW10.SWMANAGER.ClassesAplicacao.AtendimentosLeitosMov;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.ServicosMedicosPrestados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Nacionalidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.VisualASA;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos
{

    public class AtendimentoAppService : SWMANAGERAppServiceBase, IAtendimentoAppService
    {
        #region Injecao e Construtor

        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IRepository<Leito, long> _leitoRepository;
        private readonly IListarAtendimentosExcelExporter _listarAtendimentosExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IPacienteAppService _pacienteAppService;
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepository;
        private readonly IRepository<FaturamentoConta, long> _fatContaRepository;
        private readonly IRepository<AtendimentoLeitoMov, long> _atendimentoLeitoMovRepository;
        private readonly IVisualAppService _visualAppService;
        private readonly IRepository<Senha, long> _senhaRepository;
        private readonly IRepository<SenhaMovimentacao, long> _senhaMovimentacaoRepository;

        public AtendimentoAppService(
            IRepository<Atendimento, long> atendimentoRepository,
            IRepository<Leito, long> leitoRepository,
            IListarAtendimentosExcelExporter listarAtendimentosExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoIdAppService,
           IPacienteAppService pacienteAppService,
           IRepository<EstoqueMovimento, long> estoqueMovimentoRepository,
           IRepository<FaturamentoConta, long> fatContaRepository,
           IRepository<AtendimentoLeitoMov, long> atendimentoLeitoMovRepository,
           IVisualAppService visualAppService,
           IRepository<Senha, long> senhaRepository,
           IRepository<SenhaMovimentacao, long> senhaMovimentacaoRepository
            )
        {
            _atendimentoRepository = atendimentoRepository;
            _leitoRepository = leitoRepository;
            _listarAtendimentosExcelExporter = listarAtendimentosExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoIdAppService;
            _pacienteAppService = pacienteAppService;
            _estoqueMovimentoRepository = estoqueMovimentoRepository;
            _fatContaRepository = fatContaRepository;
            _atendimentoLeitoMovRepository = atendimentoLeitoMovRepository;
            _visualAppService = visualAppService;
            _senhaRepository = senhaRepository;
            _senhaMovimentacaoRepository = senhaMovimentacaoRepository;
        }

        #endregion injecao e construtor.

        [UnitOfWork] //Atualizado (pablo 08/08/2017)
        public async Task<long> CriarOuEditar(AtendimentoDto input)
        {
            try
            {
                var atendimento = AtendimentoDto.Mapear(input);

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    //=========CRIAR MÉTODO PARA TRATAMENTO DE PRE-ATENDIMENTO===============
                    if (input.NomePreAtendimento != null)
                    {
                        PacienteDto paciente = new PacienteDto();
                        //pesquisa paciente
                        paciente = await _pacienteAppService.ObterPorCpf(input.CpfPreAtendimento);

                        if (paciente == null)
                        {
                            //CriarOuEditarPaciente paciente2 = new CriarOuEditarPaciente();
                            paciente = new PacienteDto();
                            paciente.NomeCompleto = input.NomePreAtendimento;
                            paciente.Telefone1 = input.TelefonePreAtendimento;
                            paciente.Nascimento = input.DataNascPreAtendimento;
                            paciente.Cpf = input.CpfPreAtendimento;
                            paciente.Rg = input.IdentPreAtendimento;
                            //salva paciente
                            atendimento.PacienteId = await _pacienteAppService.CriarOuEditarOriginal(paciente);
                        }
                        else
                        {
                            atendimento.PacienteId = paciente.Id;
                        }
                        atendimento.IsPreatendimento = true;
                        atendimento.AtendimentoTipoId = input.AtendimentoTipoId;
                        atendimento.DataRegistro = input.DataRegistro;
                        atendimento.DataPreatendimento = input.DataRegistro;
                        atendimento.Observacao = input.Observacao;

                    }  //=======FIM TRATAMENTO P/ PRE-ATENDIMENTO===============

                    //========Cria método depois geração do codigo caso ñ seja passado============
                    if (atendimento.Codigo.IsNullOrWhiteSpace())
                    {
                        // Buscando 'UltimoId'
                        var codigos = await _ultimoIdAppService.ListarTodos();
                        var codigosList = codigos.Items.ToList();

                        var ultimo = codigosList.FirstOrDefault(c => c.NomeTabela == "Atendimento");

                        // Atribuindo 'Codigo' de 'Atendimento'
                        atendimento.Codigo = ultimo.Codigo;

                        // Incrementando 'UltimoId'
                        var codigoNumero = Convert.ToInt64(ultimo.Codigo);
                        codigoNumero++;
                        ultimo.Codigo = codigoNumero.ToString();
                        await _ultimoIdAppService.CriarOuEditar(ultimo);

                        // Precisa reverter este incremento caso ocorra erro na insercao do Atendimento?
                    }
                    //========fim geração do codigo caso ñ seja passado============


                    if (input.Id.Equals(0))
                    {
                        atendimento.Id = await _atendimentoRepository.InsertAndGetIdAsync(atendimento);

                        // Nova conta medica
                        // Todo atendimento salvo deve gerar automaticamente uma nova conta associada a este atendimento
                        FaturamentoConta novaConta = new FaturamentoConta();

                        // Todos os campos de FatConta abaixo, devem ser preenchidos conforme possível
                        // obtendo dados do Atendimento que gera esta nova conta
                        novaConta.AtendimentoId = atendimento.Id;
                        novaConta.EmpresaId = atendimento.EmpresaId;
                        novaConta.ConvenioId = atendimento.ConvenioId;
                        novaConta.PlanoId = atendimento.PlanoId;
                        novaConta.PacienteId = atendimento.PacienteId;
                        novaConta.MedicoId = atendimento.MedicoId;
                        novaConta.Matricula = atendimento.Matricula;
                        novaConta.CodDependente = input.CodDependente; //campo foi inserido apenas no dto, pois ele pertence à conta médica (fatconta)
                        novaConta.NumeroGuia = atendimento.GuiaNumero;
                        novaConta.Titular = atendimento.Titular;
                        novaConta.FatGuiaId = atendimento.FatGuiaId;
                        novaConta.UnidadeOrganizacionalId = atendimento.UnidadeOrganizacionalId;
                        novaConta.StatusId = 1; // StatusId 1 = 'Inicial'
                        novaConta.DataIncio = DateTime.Now;
                        novaConta.DataFim = null;
                        novaConta.ValidadeCarteira = atendimento.ValidadeCarteira; //null;
                        novaConta.DataAutorizacao = atendimento.DataAutorizacao; //null;
                        novaConta.DiaSerie1 = null;
                        novaConta.DiaSerie2 = null;
                        novaConta.DiaSerie3 = null;
                        novaConta.DiaSerie4 = null;
                        novaConta.DiaSerie5 = null;
                        novaConta.DiaSerie6 = null;
                        novaConta.DiaSerie7 = null;
                        novaConta.DiaSerie8 = null;
                        novaConta.DiaSerie9 = null;
                        novaConta.DiaSerie10 = null;
                        novaConta.DataEntrFolhaSala = null;
                        novaConta.DataEntrDescCir = null;
                        novaConta.DataEntrBolAnest = null;
                        novaConta.DataEntrCDFilme = null;
                        novaConta.DataValidadeSenha = atendimento.ValidadeSenha; //null;
                        novaConta.GuiaOperadora = null;
                        novaConta.GuiaPrincipal = null;
                        novaConta.TipoAtendimento = atendimento.IsAmbulatorioEmergencia ? 1 : 2;
                        novaConta.IsAutorizador = false;
                        //if(atendimento.LeitoId!=null)
                        //{
                        //    var leito = _leitoRepository.GetAll()
                        //                                .Where(w => w.Id == atendimento.LeitoId)
                        //                                .FirstOrDefault();

                        //    if (leito !=null)
                        //    {
                        //        novaConta.TipoAcomodacaoId = leito.TipoAcomodacaoId; // tipo de leito eh tipo de acomdacao
                        //    }
                        //}

                        novaConta.Observacao = atendimento.Observacao;
                        novaConta.SenhaAutorizacao = atendimento.Senha; //null;
                        novaConta.IdentAcompanhante = null;
                        novaConta.DataPagamento = atendimento.DataUltimoPagamento;

                        if (atendimento.IsInternacao)
                        {
                            novaConta.DataIncio = atendimento.DataRegistro;

                            #region Movimento Leito

                            AtendimentoLeitoMov atendimentoLeitoMov = new AtendimentoLeitoMov();
                            atendimentoLeitoMov.AtendimentoId = atendimento.Id;
                            atendimentoLeitoMov.LeitoId = atendimento.LeitoId;
                            atendimentoLeitoMov.UserId = atendimento.CreatorUserId;
                            atendimentoLeitoMov.DataInclusao = atendimentoLeitoMov.DataInicial = atendimento.DataRegistro;

                            await _atendimentoLeitoMovRepository.InsertAsync(atendimentoLeitoMov);

                            var leito = _leitoRepository.GetAll()
                                                        .Where(w => w.Id == atendimento.LeitoId)
                                                        .FirstOrDefault();

                            if (leito != null)
                            {
                                leito.LeitoStatusId = 2;

                                novaConta.TipoAcomodacaoId = leito.TipoAcomodacaoId;

                                await _leitoRepository.UpdateAsync(leito);
                            }

                            #endregion

                        }

                        await _fatContaRepository.InsertAndGetIdAsync(novaConta);
                        // Fim - nova conta medica
                    }
                    else
                    {
                        //var ori = _atendimentoRepository.Get(atendimento.Id);
                        //ori = atendimento;
                        //ori.AltaGrupoCID = null;
                        //ori.AtendimentoMotivoCancelamento = null;
                        //ori.AtendimentoStatus = null;
                        //ori.AtendimentoTipo = null;
                        //ori.Convenio = null;
                        //ori.Empresa = null;
                        //ori.EspecialidadeId = null;
                        //ori.FatGuia = null;
                        //ori.Guia = null;
                        //ori.Leito = null;
                        //ori.Medico = null;
                        //ori.MotivoAlta = null;
                        //ori.Nacionalidade = null;
                        //ori.Origem = null;
                        //ori.Paciente = null;
                        //ori.Plano = null;
                        //ori.ServicoMedicoPrestado = null;
                        //ori.TipoAcomodacao = null;
                        //ori.UnidadeOrganizacional = null;

                        await _atendimentoRepository.UpdateAsync(atendimento);
                    }


                    var movimentacaoSenha = _senhaMovimentacaoRepository.GetAll()
                                                .Include(i => i.Senha)
                                                .Include(i => i.TipoLocalChamada)
                                                .Where(w => w.Id == input.MovimentacaoSenhalId)
                                                .FirstOrDefault();

                    if (movimentacaoSenha != null)
                    {
                        movimentacaoSenha.Senha.AtendimentoId = atendimento.Id;
                        movimentacaoSenha.DataHoraFinal = DateTime.Now;

                        if (movimentacaoSenha.TipoLocalChamada != null && movimentacaoSenha.TipoLocalChamada.TipoLocalChamadaProximoId != null)
                        {
                            var senhaMovimentacao = new SenhaMovimentacao();

                            senhaMovimentacao.SenhaId = movimentacaoSenha.Senha.Id;
                            senhaMovimentacao.TipoLocalChamadaId = movimentacaoSenha.TipoLocalChamada.TipoLocalChamadaProximoId; //input.ProximoTipoLocalChamdaId;
                            senhaMovimentacao.DataHora = DateTime.Now;

                            await _senhaMovimentacaoRepository.InsertAsync(senhaMovimentacao);
                        }

                    }



                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                    _visualAppService.MigrarVisualASA(atendimento.Id);




                    return atendimento.Id;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task SetAlta(SetAltaInput input)
        {
            try
            {
                if (!input.atendimentoId.HasValue)
                {
                    throw new Exception("InformarAtendimento");
                }
                var atendimento = _atendimentoRepository.Get(input.atendimentoId.Value);
                atendimento.DataAlta = input.dataAlta;

                if (input.motivoAltaId == 0)
                    input.motivoAltaId = null;

                if (input.altaGrupoCidId == 0)
                    input.altaGrupoCidId = null;

                atendimento.MotivoAltaId = input.motivoAltaId;
                atendimento.AltaGrupoCIDId = input.altaGrupoCidId;
                atendimento.NumeroObito = input.NumeroObito;

                if (atendimento.LeitoId.HasValue)
                {
                    var leito = _leitoRepository.Get(atendimento.LeitoId.Value);
                    leito.LeitoStatusId = 1; // status 1 -> VAGO (default Seed)
                    _leitoRepository.Update(leito);
                    atendimento.LeitoId = null;

                    //movimento de leito... inserindo a data final do movimento
                    var mov = _atendimentoLeitoMovRepository
                        .GetAll()
                        .Where(w => w.AtendimentoId == atendimento.Id && w.LeitoId == leito.Id && !w.DataFinal.HasValue)
                        .FirstOrDefault();

                    if(mov != null)
                    {
                        mov.DataFinal = DateTime.Now;
                        await _atendimentoLeitoMovRepository.UpdateAsync(mov);
                    }
                    else
                    {
                        mov = new AtendimentoLeitoMov();
                        mov.AtendimentoId = atendimento.Id;
                        mov.LeitoId = leito.Id;
                        mov.DataInclusao = DateTime.Now;
                        mov.DataInicial = DateTime.Now;
                        mov.DataFinal = DateTime.Now;
                        mov.UserId = AbpSession.UserId.HasValue? AbpSession.UserId.Value : 2;
                        await _atendimentoLeitoMovRepository.InsertAsync(mov);
                    }
                }
                await _atendimentoRepository.UpdateAsync(atendimento);
                //await CriarOuEditar(AtendimentoDto.MapearFromCore(atendimento));
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("ErroAtribuirAlta"), e);
            }
        }

        public async Task SetAltaMedica(SetAltaInput input)
        {
            try
            {
                var atendimento = _atendimentoRepository.Get(input.atendimentoId.Value);
                atendimento.DataAltaMedica = input.dataAltaMedica;
                await _atendimentoRepository.UpdateAsync(atendimento);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("ErroAtribuirAlta"), e);
            }
        }

        public async Task SetDataPrevistaAlta(SetAltaInput input)
        {
            try
            {
                var atendimento = _atendimentoRepository.Get(input.atendimentoId.Value);
                atendimento.DataPrevistaAlta = input.dataPrevisaoAlta;
                await _atendimentoRepository.UpdateAsync(atendimento);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("ErroAtribuirAlta"), e);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id, long? motivoCancelamentoId = null)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    //await _atendimentoRepository.DeleteAsync(id);

                    var atendimento = _atendimentoRepository.GetAll()
                                                            .Where(w => w.Id == id)
                                                            .FirstOrDefault();

                    if (atendimento != null)
                    {

                        atendimento.AtendimentoMotivoCancelamentoId = motivoCancelamentoId ?? 1;

                        if (atendimento.LeitoId.HasValue)
                        {
                            var leito = _leitoRepository.Get(atendimento.LeitoId.Value);
                            leito.LeitoStatusId = 1; // status 1 -> VAGO (default Seed)
                            _leitoRepository.Update(leito);


                            var movimentoLeito = _atendimentoLeitoMovRepository.GetAll()
                                                                      .Where(w => w.LeitoId == atendimento.LeitoId
                                                                              && w.AtendimentoId == atendimento.Id
                                                                              && w.DataFinal == null)
                                                                      .FirstOrDefault();

                            if (movimentoLeito != null)
                            {
                                movimentoLeito.DataFinal = DateTime.Now;
                            }
                            atendimento.LeitoId = null;
                        }



                    }



                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork]
        public async Task Reativar(long id, long? leitoId)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var atendimento = _atendimentoRepository.GetAll()
                                                            .Where(w => w.Id == id)
                                                            .FirstOrDefault();

                    if (atendimento != null)
                    {

                        atendimento.AtendimentoMotivoCancelamentoId = null;
                      

                        if (leitoId.HasValue)
                        {
                            atendimento.LeitoId = leitoId;
                            var leito = _leitoRepository.Get(leitoId ?? 0);
                            leito.LeitoStatusId = 2; // status 2 -> OCUPADO (default Seed)
                            _leitoRepository.Update(leito);

                            var movimentoLeito = new AtendimentoLeitoMov();

                            movimentoLeito.AtendimentoId = id;
                            movimentoLeito.DataInclusao = DateTime.Now;
                            movimentoLeito.DataInicial = DateTime.Now;
                            movimentoLeito.LeitoId = leitoId;

                            _atendimentoLeitoMovRepository.Insert(movimentoLeito);
                        }
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoDto>> ListarTodos()
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            List<AtendimentoDto> atendimentosDtos = new List<AtendimentoDto>();
            try
            {
                var query = _atendimentoRepository
                  .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.Medico)
                  .Include(m => m.Medico.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional);


                contarAtendimentos = await query
                    .CountAsync();

                atendimentos = await query
                    .AsNoTracking()

                    .ToListAsync();

                atendimentosDtos = AtendimentoDto.Mapear(atendimentos).ToList();

                return new PagedResultDto<AtendimentoDto>(
                contarAtendimentos,
                atendimentosDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoDto>> ListarParaInternacao()
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            List<AtendimentoDto> atendimentosDtos = new List<AtendimentoDto>();
            try
            {
                var query = _atendimentoRepository
                    .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.Medico)
                  .Include(m => m.Medico.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional)
                    .Where(a => a.IsInternacao == true);

                contarAtendimentos = await query
                    .CountAsync();

                atendimentos = await query
                    .AsNoTracking()

                    .ToListAsync();

                atendimentosDtos = AtendimentoDto.Mapear(atendimentos).ToList();

                return new PagedResultDto<AtendimentoDto>(
                contarAtendimentos,
                atendimentosDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoIndexDto>> ListarFiltro(ListarAtendimentosInput input)
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            try
            {
                //var query = _atendimentoRepository
                //    .GetAll()
                //    .Where(a => a.IsAmbulatorioEmergencia == true)
                //    .Where(m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
                //    .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
                //    .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
                //    .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
                //    .WhereIf(input.MedicoId > 0, m => m.MedicoId == input.MedicoId)
                //    .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId
                //    );

                //input.endDate2 = input.endDate2.AddDays(1);

                var query = _atendimentoRepository
                  .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.Medico)
                  .Include(m => m.Medico.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.Leito.TipoAcomodacao)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional)
                  //=== filtro generico pablo 
                  .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Codigo.Contains(input.Filtro.ToUpper()) ||
                    m.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Leito.TipoAcomodacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Leito.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper())
                    )
                  //===
                  .Where(a => a.IsAmbulatorioEmergencia == true)
                  .WhereIf(input.FiltroData == "Alta", m => (m.DataAlta >= input.StartDate && m.DataRegistro <= input.EndDate) && m.AtendimentoMotivoCancelamentoId == null)
                  .WhereIf((input.FiltroData == "Atendimento"), m => (m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate) && m.AtendimentoMotivoCancelamentoId == null)
                  .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
                  .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
                  .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
                  .WhereIf(input.MedicoId > 0 && input.MedicoId != input.UserMedicoId, m => m.MedicoId == input.MedicoId)
                  .WhereIf(input.UserMedicoId > 0 && input.MedicoId == input.UserMedicoId, m => m.MedicoId == input.UserMedicoId || m.MedicoId == null)
                  .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId)
                  .WhereIf(input.NacionalidadeResponsavelId > 0, m => m.NacionalidadeResponsavelId == input.NacionalidadeResponsavelId)
                  .WhereIf(input.IsAmbulatorioEmergencia.HasValue, m => m.IsAmbulatorioEmergencia == input.IsAmbulatorioEmergencia.Value)
                  .WhereIf(input.IsInternacao.HasValue, m => m.IsInternacao == input.IsInternacao.Value)
                   //.WhereIf(!input.Filtro.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()))
                   .WhereIf(!input.NomePaciente.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.NomePaciente))
                  //.WhereIf(input.FiltroData == "Internado", m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate &&  m.DataAlta == null)
                  .WhereIf(input.FiltroData == "Internado", m => m.DataAlta == null)
                   .WhereIf(input.FiltroData == "Cancelado", m => m.AtendimentoMotivoCancelamentoId != null)
                //.WhereIf(input.Internados, m => m.DataAlta == null)
                //.WhereIf(input.Internados, a => DateTime.Compare((DateTime)a.DataAlta, DateTime.Now) >= 0)
                ;

                contarAtendimentos = await query
                    .CountAsync();

                atendimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();


                var atendimentosDto = new List<AtendimentoIndexDto>();
                foreach (var m in atendimentos)
                {
                    atendimentosDto.Add(new AtendimentoIndexDto
                    {
                        CodigoAtendimento = m.Codigo,
                        CodigoPaciente = m.Paciente?.CodigoPaciente.ToString(),
                        Convenio = m.Convenio?.NomeFantasia,
                        DataAlta = m.DataAlta,
                        Empresa = m.Empresa?.NomeFantasia,
                        DataRegistro = m.DataRegistro,
                        Id = m.Id,
                        Medico = m.Medico?.NomeCompleto,
                        Paciente = m.Paciente?.NomeCompleto,
                        TipoLeito = m.Leito?.TipoAcomodacao?.Descricao,
                        Leito = m.Leito?.Descricao,
                        Unidade = m.UnidadeOrganizacional?.Descricao,
                        IsControlaTev = m.IsControlaTev,
                        AtendimentoMotivoCancelamentoId = m.AtendimentoMotivoCancelamentoId
                    });
                }

                return new PagedResultDto<AtendimentoIndexDto>(
                    contarAtendimentos,
                    atendimentosDto
                    );


                //var atendimentosDto = new List<AtendimentoDto>();
                ////atendimentos.MapTo<List<AtendimentoDto>>();
                //AtendimentoDto atendimentoDto;// = new AtendimentoDto();
                //foreach (var item in atendimentos)
                //{
                //    atendimentoDto = new AtendimentoDto();

                //    atendimentoDto.Id = item.Id;
                //    atendimentoDto.UnidadeOrganizacional = item.UnidadeOrganizacional.MapTo<UnidadeOrganizacionalDto>();
                //    atendimentoDto.Codigo = item.Codigo;
                //    atendimentoDto.Paciente = item.Paciente.MapTo<PacienteDto>();
                //    atendimentoDto.DataRegistro = item.DataRegistro;
                //    atendimentoDto.DataAlta = item.DataAlta;
                //    atendimentoDto.Convenio = item.Convenio.MapTo<ConvenioDto>();
                //    atendimentoDto.Medico = item.Medico.MapTo<MedicoDto>();
                //    atendimentoDto.Leito = item.Leito.MapTo<LeitoDto>();
                //    atendimentoDto.Empresa = item.Empresa.MapTo<EmpresaDto>();

                //    atendimentosDto.Add(atendimentoDto);

                //}



                //return new PagedResultDto<AtendimentoDto>(
                //    contarAtendimentos,
                //    atendimentosDto
                //    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        private IQueryable<Atendimento> ConsultaListarFiltro(ListarAtendimentosInput input)
        {
            return _atendimentoRepository
              .GetAll()
              .Include(m => m.Paciente)
              .Include(m => m.Paciente.SisPessoa)
              .Include(m => m.Medico)
              .Include(m => m.Medico.SisPessoa)
              .Include(m => m.AtendimentoTipo)
              .Include(m => m.Convenio)
              .Include(m => m.Convenio.SisPessoa)
              .Include(m => m.Empresa)
              .Include(m => m.Especialidade)
              .Include(m => m.Guia)
              .Include(m => m.Leito)
              .Include(m => m.Leito.TipoAcomodacao)
              .Include(m => m.MotivoAlta)
              .Include(m => m.Nacionalidade)
              .Include(m => m.Origem)
              .Include(m => m.Plano)
              .Include(m => m.ServicoMedicoPrestado)
              .Include(m => m.UnidadeOrganizacional)
              //=== filtro generico pablo 
              .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Codigo.Contains(input.Filtro.ToUpper()) ||
                m.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Leito.TipoAcomodacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Leito.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper())
                )
              //===
              .Where(a => a.IsAmbulatorioEmergencia == true)
              .WhereIf(input.FiltroData == "Alta", m => m.DataAlta >= input.StartDate && m.DataAlta <= input.EndDate)
              .WhereIf((input.FiltroData == "Atendimento"), m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
              .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
              .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
              .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
              .WhereIf(input.MedicoId > 0, m => m.MedicoId == input.MedicoId)
              .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId)
              .WhereIf(input.NacionalidadeResponsavelId > 0, m => m.NacionalidadeResponsavelId == input.NacionalidadeResponsavelId)
               //.WhereIf(input.IsAmbulatorioEmergencia.HasValue, m => m.IsAmbulatorioEmergencia == input.IsAmbulatorioEmergencia.Value)
               //.WhereIf(input.IsInternacao.HasValue, m => m.IsInternacao == input.IsInternacao.Value)
               //.WhereIf(!input.Filtro.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()))
               .WhereIf(!input.NomePaciente.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.NomePaciente))
              //.WhereIf(input.FiltroData == "Internado", m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate &&  m.DataAlta == null)
              .WhereIf(input.FiltroData == "Internado", m => m.DataAlta == null)
            //.WhereIf(input.Internados, m => m.DataAlta == null)
            //.WhereIf(input.Internados, a => DateTime.Compare((DateTime)a.DataAlta, DateTime.Now) >= 0)
            ;

        }

        [UnitOfWork(false)]
        private IQueryable<Atendimento> ConsultaListarFiltroInternacao(ListarAtendimentosInput input)
        {
            return _atendimentoRepository
                .GetAll()
              .Include(m => m.Paciente)
              .Include(m => m.Paciente.SisPessoa)
              .Include(m => m.Medico)
              .Include(m => m.Medico.SisPessoa)
              .Include(m => m.AtendimentoTipo)
              .Include(m => m.Convenio)
              .Include(m => m.Convenio.SisPessoa)
              .Include(m => m.Empresa)
              .Include(m => m.Especialidade)
              .Include(m => m.Guia)
              .Include(m => m.Leito)
              .Include(m => m.Leito.TipoAcomodacao)
              .Include(m => m.MotivoAlta)
              .Include(m => m.Nacionalidade)
              .Include(m => m.Origem)
              .Include(m => m.Plano)
              .Include(m => m.ServicoMedicoPrestado)
              .Include(m => m.UnidadeOrganizacional)
              //=== filtro generico pablo 
              .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Codigo.Contains(input.Filtro.ToUpper()) ||
                m.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Leito.TipoAcomodacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Leito.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()))
                //===
                .Where(a => a.IsInternacao == true)
                //.WhereIf((input.FiltroData == "Internacao"), m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
                //.Where(m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
                .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
                .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
                .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
                .WhereIf(input.MedicoId > 0, m => m.MedicoId == input.MedicoId)
                .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId)
                .WhereIf(input.NacionalidadeResponsavelId > 0, m => m.NacionalidadeResponsavelId == input.NacionalidadeResponsavelId)
                //.WhereIf(input.IsAmbulatorioEmergencia.HasValue, m => m.IsAmbulatorioEmergencia == input.IsAmbulatorioEmergencia.Value)
                //.WhereIf(input.IsInternacao.HasValue, m => m.IsInternacao == input.IsInternacao.Value)
                // .WhereIf(!input.Filtro.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()))
                .WhereIf(!input.NomePaciente.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.NomePaciente))
                //.WhereIf(input.Internados, a => DateTime.Compare((DateTime)a.DataAlta, DateTime.Now) >= 0) Internação
                .WhereIf(input.FiltroData == "Alta", m => m.DataAlta >= input.StartDate && m.DataAlta <= input.EndDate)
                .WhereIf(input.FiltroData == "Internado", m => m.DataAlta == null)
                .WhereIf(input.FiltroData == "Atendimento", m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
                .WhereIf(input.FiltroData == "Cancelado", m => m.AtendimentoMotivoCancelamentoId != null);
            //.OrderByDescending(m => input.Sorting);
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoIndexDto>> ListarAtendimentos(ListarAtendimentosInput input)
        {
            var contarAtendimentos = 0;
            List<AtendimentoIndexDto> atendimentos = new List<AtendimentoIndexDto>();

            try
            {
                switch (input.TipoAtendimento)
                {
                    case "AMB":
                        var queryAmb = ConsultaListarFiltro(input);
                        var amb = await queryAmb.ToListAsync(); // ListarFiltro(input);
                        foreach (var item in amb)
                        {
                            atendimentos.Add(new AtendimentoIndexDto
                            {
                                CodigoAtendimento = item.Codigo,
                                CodigoPaciente = item.Paciente != null ? item.Paciente.Codigo : string.Empty,
                                Convenio = item.Convenio != null ? item.Convenio.NomeFantasia : string.Empty,
                                DataAlta = item.DataAlta,
                                //DataFim=item.da
                                //DataInicioConta=item.DataAlta
                                DataRegistro = item.DataRegistro,
                                Empresa = item.Empresa != null ? item.Empresa.NomeFantasia : string.Empty,
                                Guia = item.Guia != null ? item.Guia.Descricao : string.Empty,
                                LeitoAtual = item.Leito != null ? item.Leito.Descricao : string.Empty,
                                Matricula = item.Matricula,
                                NumeroGuia = item.NumeroGuia,
                                Paciente = item.Paciente != null ? item.Paciente.NomeCompleto : string.Empty,
                                Plano = item.Plano != null ? item.Plano.Descricao : string.Empty,
                                TipoLeito = item.Leito != null ? item.Leito.TipoAcomodacao.Descricao : string.Empty,
                                Unidade = item.Empresa != null ? item.Empresa.Codigo : string.Empty,
                                Id = item.Id,
                            });
                        }
                        break;
                    case "INT":
                        var queryInt = ConsultaListarFiltroInternacao(input);
                        var intern = await queryInt.ToListAsync();
                        foreach (var item in intern)
                        {
                            atendimentos.Add(new AtendimentoIndexDto
                            {
                                CodigoAtendimento = item.Codigo,
                                CodigoPaciente = item.Paciente != null ? item.Paciente.Codigo : string.Empty,
                                Convenio = item.Convenio != null ? item.Convenio.NomeFantasia : string.Empty,
                                DataAlta = item.DataAlta,
                                //DataFim=item.da
                                //DataInicioConta=item.DataAlta
                                DataRegistro = item.DataRegistro,
                                Empresa = item.Empresa != null ? item.Empresa.NomeFantasia : string.Empty,
                                Guia = item.Guia != null ? item.Guia.Descricao : string.Empty,
                                LeitoAtual = item.Leito != null ? item.Leito.Descricao : string.Empty,
                                Matricula = item.Matricula,
                                NumeroGuia = item.NumeroGuia,
                                Paciente = item.Paciente != null ? item.Paciente.NomeCompleto : string.Empty,
                                Plano = item.Plano != null ? item.Plano.Descricao : string.Empty,
                                TipoLeito = item.Leito != null ? item.Leito.TipoAcomodacao.Descricao : string.Empty,
                                Unidade = item.Empresa != null ? item.Empresa.Codigo : string.Empty,
                                Id = item.Id,
                            });
                        }
                        break;
                    default:
                        var queryAmbAll = ConsultaListarFiltro(input);
                        var ambAll = await queryAmbAll.ToListAsync();
                        foreach (var item in ambAll)
                        {
                            atendimentos.Add(new AtendimentoIndexDto
                            {
                                CodigoAtendimento = item.Codigo,
                                CodigoPaciente = item.Paciente != null ? item.Paciente.Codigo : string.Empty,
                                Convenio = item.Convenio != null ? item.Convenio.NomeFantasia : string.Empty,
                                DataAlta = item.DataAlta,
                                //DataFim=item.da
                                //DataInicioConta=item.DataAlta
                                DataRegistro = item.DataRegistro,
                                Empresa = item.Empresa != null ? item.Empresa.NomeFantasia : string.Empty,
                                Guia = item.Guia != null ? item.Guia.Descricao : string.Empty,
                                LeitoAtual = item.Leito != null ? item.Leito.Descricao : string.Empty,
                                Matricula = item.Matricula,
                                NumeroGuia = item.NumeroGuia,
                                Paciente = item.Paciente != null ? item.Paciente.NomeCompleto : string.Empty,
                                Plano = item.Plano != null ? item.Plano.Descricao : string.Empty,
                                TipoLeito = item.Leito != null ? item.Leito.TipoAcomodacao.Descricao : string.Empty,
                                Unidade = item.Empresa != null ? item.Empresa.Codigo : string.Empty,
                                Id = item.Id,
                            });
                        }
                        var queryIntAll = ConsultaListarFiltroInternacao(input);
                        var internAll = await queryIntAll.ToListAsync();
                        foreach (var item in internAll)
                        {
                            atendimentos.Add(new AtendimentoIndexDto
                            {
                                CodigoAtendimento = item.Codigo,
                                CodigoPaciente = item.Paciente != null ? item.Paciente.Codigo : string.Empty,
                                Convenio = item.Convenio != null ? item.Convenio.NomeFantasia : string.Empty,
                                DataAlta = item.DataAlta,
                                //DataFim=item.da
                                //DataInicioConta=item.DataAlta
                                DataRegistro = item.DataRegistro,
                                Empresa = item.Empresa != null ? item.Empresa.NomeFantasia : string.Empty,
                                Guia = item.Guia != null ? item.Guia.Descricao : string.Empty,
                                LeitoAtual = item.Leito != null ? item.Leito.Descricao : string.Empty,
                                Matricula = item.Matricula,
                                NumeroGuia = item.NumeroGuia,
                                Paciente = item.Paciente != null ? item.Paciente.NomeCompleto : string.Empty,
                                Plano = item.Plano != null ? item.Plano.Descricao : string.Empty,
                                TipoLeito = item.Leito != null ? item.Leito.TipoAcomodacao.Descricao : string.Empty,
                                Unidade = item.Empresa != null ? item.Empresa.Codigo : string.Empty,
                                Id = item.Id,
                            });
                        }
                        break;
                }

                contarAtendimentos = atendimentos.Distinct().Count();

                var atendimentosDto = atendimentos
                    .AsQueryable()
                    .AsNoTracking()
                    .PageBy(input)
                    .OrderBy(input.Sorting)
                    .Distinct()
                    .ToList();

                return new PagedResultDto<AtendimentoIndexDto>(
                    contarAtendimentos,
                    atendimentosDto
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoIndexDto>> ListarFiltroInternacao(ListarAtendimentosInput input)
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            try
            {
                var query = _atendimentoRepository
                    .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.Medico)
                  .Include(m => m.Medico.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.Leito.TipoAcomodacao)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional)
                  //=== filtro generico pablo 
                  .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Codigo.Contains(input.Filtro.ToUpper()) ||
                    m.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Leito.TipoAcomodacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Leito.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()))
                    //===
                    .Where(a => a.IsInternacao == true )
                     .WhereIf((input.FiltroData == "Internacao"), m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
                    //.Where(m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
                    .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
                    .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
                    .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
                    .WhereIf(input.MedicoId > 0, m => m.MedicoId == input.MedicoId)
                    .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId)
                    .WhereIf(input.NacionalidadeResponsavelId > 0, m => m.NacionalidadeResponsavelId == input.NacionalidadeResponsavelId)
                    .WhereIf(input.IsAmbulatorioEmergencia.HasValue, m => m.IsAmbulatorioEmergencia == input.IsAmbulatorioEmergencia.Value)
                    .WhereIf(input.IsInternacao.HasValue, m => m.IsInternacao == input.IsInternacao.Value)
                    // .WhereIf(!input.Filtro.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()))
                    .WhereIf(!input.NomePaciente.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.NomePaciente))
                    //.WhereIf(input.Internados, a => DateTime.Compare((DateTime)a.DataAlta, DateTime.Now) >= 0) Internação
                    .WhereIf(input.FiltroData == "Alta", m => (m.DataAlta >= input.StartDate && m.DataRegistro <= input.EndDate) && m.AtendimentoMotivoCancelamentoId == null)
                    .WhereIf(input.FiltroData == "Internado", m => (m.DataAlta == null && m.AtendimentoMotivoCancelamentoId == null))
                    .WhereIf(input.FiltroData == "Cancelado", m => m.AtendimentoMotivoCancelamentoId != null)
                    .OrderByDescending(m => input.Sorting);

                contarAtendimentos = await query.CountAsync();

                atendimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //var atendimentosDto = atendimentos
                //  .MapTo<List<AtendimentoDto>>();
                var atendimentosDto = new List<AtendimentoIndexDto>();
                foreach (var m in atendimentos)
                {
                    atendimentosDto.Add(new AtendimentoIndexDto
                    {
                        CodigoAtendimento = m.Codigo,
                        CodigoPaciente = m.Paciente?.Codigo,
                        Convenio = m.Convenio?.NomeFantasia,
                        DataAlta = m.DataAlta,
                        Empresa = m.Empresa?.NomeFantasia,
                        DataRegistro = m.DataRegistro,
                        Id = m.Id,
                        Medico = m.Medico?.NomeCompleto,
                        Paciente = m.Paciente?.NomeCompleto,
                        TipoLeito = m.Leito?.TipoAcomodacao?.Descricao,
                        Leito = m.Leito?.Descricao,
                        Unidade = m.UnidadeOrganizacional?.Descricao,
                        IsControlaTev = m.IsControlaTev,
                        AtendimentoMotivoCancelamentoId = m.AtendimentoMotivoCancelamentoId
                    });
                }

                return new PagedResultDto<AtendimentoIndexDto>(
                    contarAtendimentos,
                    atendimentosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoDto>> ListarFiltroPreAtendimento(ListarAtendimentosInput input)
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            try
            {
                var query = _atendimentoRepository
                  .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.Medico)
                  .Include(m => m.Medico.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.Leito.TipoAcomodacao)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional)
                  .Where(a => a.IsPreatendimento == true)
                  .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Codigo.Contains(input.Filtro.ToUpper()) ||
                    m.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Leito.TipoAcomodacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Leito.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.AtendimentoTipo.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                     m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarAtendimentos = await query
                    .CountAsync();

                atendimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var atendimentosDto = AtendimentoDto.Mapear(atendimentos).ToList();

                return new PagedResultDto<AtendimentoDto>(
                    contarAtendimentos,
                    atendimentosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoDto>> ListarPorPaciente(ListarInput input)
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            try
            {
                var query = _atendimentoRepository
                    .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.Medico)
                  .Include(m => m.Medico.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional)
                    .Where(m => m.PacienteId.Equals(input.Filtro))
                    .OrderByDescending(m => m.DataRegistro);

                contarAtendimentos = await query
                    .CountAsync();

                atendimentos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var atendimentosDto = AtendimentoDto.Mapear(atendimentos).ToList();

                return new PagedResultDto<AtendimentoDto>(
                    contarAtendimentos,
                    atendimentosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<FileDto> ListarParaExcel(ListarAtendimentosInput input)
        {
            try
            {
                //var result = await Listar(input);
                var result = await ListarTodos();
                var atendimentos = result.Items;
                return _listarAtendimentosExcelExporter.ExportToFile(atendimentos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        [UnitOfWork(false)]
        public async Task<AtendimentoDto> Obter(long id)
        {
            try
            {
                var m = await _atendimentoRepository
                    .GetAll()
                  .Include(a => a.Paciente)
                  .Include(a => a.Paciente.SisPessoa)
                  .Include(a => a.Paciente.SisPessoa.Sexo)
                  .Include(a => a.Paciente.SisPessoa.Nacionalidade)
                  .Include(a => a.Paciente.Sexo)
                  .Include(a => a.Paciente.SisPessoa.Enderecos)
                  .Include(a => a.Paciente.Pais)
                  .Include(a => a.Paciente.Estado)
                  .Include(a => a.Paciente.Cidade)
                  .Include(a => a.Paciente.EstadoCivil)
                  .Include(a => a.Paciente.Profissao)
                  .Include(a => a.Medico)
                  .Include(a => a.Medico.Conselho)
                  .Include(a => a.Medico.SisPessoa)
                  .Include(a => a.AtendimentoTipo)
                  .Include(a => a.Convenio)
                  .Include(a => a.Convenio.SisPessoa)
                  .Include(a => a.Empresa)
                  .Include(a => a.Especialidade)
                  .Include(a => a.Guia) // modelo antigo
                  .Include(a => a.FatGuia) // novo modelo FatGuia
                  .Include(a => a.Leito)
                  .Include(a => a.Leito.TipoAcomodacao)
                  .Include(a => a.MotivoAlta)
                  .Include(a => a.Nacionalidade)
                  .Include(a => a.Origem)
                  .Include(a => a.Plano)
                  .Include(a => a.ServicoMedicoPrestado)
                  .Include(a => a.UnidadeOrganizacional)
                  .Include(a => a.TipoAcomodacao)
                  .Where(a => a.Id == id)
                    .FirstOrDefaultAsync();

                var atendimento = AtendimentoDto.Mapear(m);

                return atendimento;
            }
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<AtendimentoDto> ObterAssistencial(long id)
        {
            try
            {
                var m = await _atendimentoRepository
                    .GetAll()
                  .Include(a => a.Paciente)
                  .Include(a => a.Paciente.SisPessoa)
                  .Include(a => a.Paciente.SisPessoa.Sexo)
                  //.Include(a => a.Paciente.SisPessoa.Nacionalidade)
                  .Include(a => a.Paciente.Sexo)
                  //.Include(a => a.Paciente.SisPessoa.Enderecos)
                  //.Include(a => a.Paciente.Pais)
                  //.Include(a => a.Paciente.Estado)
                  //.Include(a => a.Paciente.Cidade)
                  //.Include(a => a.Paciente.EstadoCivil)
                  //.Include(a => a.Paciente.Profissao)
                  .Include(a => a.Medico)
                  .Include(a => a.Medico.Conselho)
                  .Include(a => a.Medico.SisPessoa)
                  //.Include(a => a.AtendimentoTipo)
                  .Include(a => a.Convenio)
                  .Include(a => a.Convenio.SisPessoa)
                  .Include(a => a.Empresa)
                  .Include(a => a.Especialidade)
                  //.Include(a => a.Guia) // modelo antigo
                  //.Include(a => a.FatGuia) // novo modelo FatGuia
                  .Include(a => a.Leito)
                  .Include(a => a.Leito.TipoAcomodacao)
                  .Include(a => a.MotivoAlta)
                  //.Include(a => a.Nacionalidade)
                  //.Include(a => a.Origem)
                  .Include(a => a.Plano)
                  //.Include(a => a.ServicoMedicoPrestado)
                  .Include(a => a.UnidadeOrganizacional)
                  .Where(a => a.Id == id)
                    .FirstOrDefaultAsync();

                var atendimento = AtendimentoDto.Mapear(m);

                return atendimento;
            }
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


        [UnitOfWork(false)]
        public AtendimentoDto ObterPorLeito(long id)
        {
            try
            {
                var result = _atendimentoRepository.GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa)
                  .Include(m => m.AtendimentoTipo)
                  .Include(m => m.Convenio)
                  .Include(m => m.Convenio.SisPessoa)
                  .Include(m => m.Empresa)
                  .Include(m => m.Especialidade)
                  .Include(m => m.Guia)
                  .Include(m => m.Leito)
                  .Include(m => m.MotivoAlta)
                  .Include(m => m.Nacionalidade)
                  .Include(m => m.Origem)
                  .Include(m => m.Plano)
                  .Include(m => m.ServicoMedicoPrestado)
                  .Include(m => m.UnidadeOrganizacional)
                  .Where(m => m.LeitoId == id)
                  .FirstOrDefault();

                AtendimentoDto atendimento = new AtendimentoDto();

                if (result != null)
                {
                    atendimento = AtendimentoDto.MapearFromCore(result);
                    return atendimento;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork]
        public async Task<long> CriarNovoAtendimento()
        {
            var novoAtendimento = new Atendimento();

            novoAtendimento.Codigo = String.Empty;
            novoAtendimento.IsSistema = false;
            novoAtendimento.IsDeleted = false;
            novoAtendimento.CreationTime = DateTime.Now;
            novoAtendimento.DataRegistro = DateTime.Now;

            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var result = await _atendimentoRepository.InsertAndGetIdAsync(novoAtendimento);
                    novoAtendimento.Codigo = _ultimoIdAppService.ObterProximoCodigo("Atendimento").Result;
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<GenericoIdNome>> ListarAtendimentoPaciente()
        {
            var contarAtendimentos = 0;
            List<Atendimento> atendimentos;
            List<GenericoIdNome> atendimentosDtos = new List<GenericoIdNome>();
            try
            {
                var query = _atendimentoRepository
                  .GetAll()
                  .Include(m => m.Paciente)
                  .Include(m => m.Paciente.SisPessoa);

                contarAtendimentos = await query
                    .CountAsync();

                atendimentos = await query
                    .AsNoTracking()
                    .ToListAsync();

                foreach (var item in atendimentos)
                {
                    atendimentosDtos.Add(new GenericoIdNome { Id = item.Id, Nome = String.Concat(item.Codigo, " - ", item.Paciente.NomeCompleto) });
                }


                return new PagedResultDto<GenericoIdNome>(
                contarAtendimentos,
                atendimentosDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<AtendimentoDto> atendimentosDto = new List<AtendimentoDto>();
            try
            {
                //get com filtro
                var query = from p in _atendimentoRepository
                            .GetAll()
                            .Include(m => m.Paciente)
                            .Include(m => m.Paciente.SisPessoa)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Paciente.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby p.Paciente.NomeCompleto ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Paciente.NomeCompleto) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarAtendimentosEmAbertoDropdown(DropdownInput dropdownInput)
        {

            DateTime dataAtual = DateTime.Now.Date.AddDays(-1);
            bool isAtendimentoEmgergicia = dropdownInput.filtros[0] == "0";
            bool isInternacao = dropdownInput.filtros[0] == "1";

            return await ListarDropdownLambda(dropdownInput
                                                     , _atendimentoRepository
                                                    , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Paciente.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                                                   || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                   &&
                                                    (m.DataAlta == null || m.DataAlta >= dataAtual)
                                                    && m.IsAmbulatorioEmergencia == isAtendimentoEmgergicia
                                                    && m.IsInternacao == isInternacao
                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Paciente.NomeCompleto, p.DataAlta != null ? string.Concat(" - Dt. alta: ", ((DateTime)p.DataAlta).Day, "/", ((DateTime)p.DataAlta).Month, "/", ((DateTime)p.DataAlta).Year) : "") }
                                                    , o => o.Paciente.NomeCompleto
                                                    );


        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarAtendimentosComSaidaDropdown(DropdownInput dropdownInput)
        {

            //DateTime dataAtual = DateTime.Now.Date.AddDays(-1);
            //bool isAtendimentoEmgergicia = dropdownInput.filtros[0] == "0";
            var estoque = dropdownInput.filtros[0];

            long estoqueId;

            long.TryParse(estoque, out estoqueId);

            var saidas = _estoqueMovimentoRepository.GetAll()
                                                    .Where(w => !w.IsEntrada
                                                              && w.EstoqueId == estoqueId);

            return await ListarDropdownLambda(dropdownInput
                                                     , _atendimentoRepository
                                                    , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                   || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))

                                                   && saidas.Any(a => a.AtendimentoId == m.Id)

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Paciente.NomeCompleto, p.DataAlta != null ? string.Concat(" - Dt. alta: ", ((DateTime)p.DataAlta).Day, "/", ((DateTime)p.DataAlta).Month, "/", ((DateTime)p.DataAlta).Year) : "") }
                                                    , o => o.Paciente.NomeCompleto
                                                    );


        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarAtendimentosAmbulatorioInternacao(DropdownInput dropdownInput)
        {

            //DateTime dataAtual = DateTime.Now.Date.AddDays(-1);
            //bool isAtendimentoEmgergicia = dropdownInput.filtros[0] == "0";
            var isAmbulatorio = dropdownInput.filtros[0] == "true";
            var isInternacao = dropdownInput.filtros[1] == "true";




            return await ListarDropdownLambda(dropdownInput
                                                     , _atendimentoRepository
                                                    , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Paciente.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                                                   || m.Paciente.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))

                                                   && m.IsAmbulatorioEmergencia == isAmbulatorio
                                                   && m.IsInternacao == isInternacao
                                                   && m.DataAlta == null
                                                   && m.AtendimentoMotivoCancelamentoId == null

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Paciente.Codigo.ToString(), " - ", p.Paciente.NomeCompleto, " - Dt.:", p.DataRegistro.Day, "/", p.DataRegistro.Month, "/", p.DataRegistro.Year, p.DataAlta != null ? string.Concat(" - Dt. alta: ", ((DateTime)p.DataAlta).Day, "/", ((DateTime)p.DataAlta).Month, "/", ((DateTime)p.DataAlta).Year) : "") }
                                                    , o => o.Paciente.NomeCompleto
                                                    );


        }
        public async Task<ResultDropdownList> ListarAtendimentosSemAlta(DropdownInput dropdownInput)
        {

            return await ListarDropdownLambda(dropdownInput
                                                     , _atendimentoRepository
                                                    , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Paciente.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                                                   || m.Paciente.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                   && m.DataAlta == null
                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Paciente.Codigo.ToString(), " - ", p.Paciente.NomeCompleto, " - Dt.:", p.DataRegistro.Day, "/", p.DataRegistro.Month, "/", p.DataRegistro.Year, p.DataAlta != null ? string.Concat(" - Dt. alta: ", ((DateTime)p.DataAlta).Day, "/", ((DateTime)p.DataAlta).Month, "/", ((DateTime)p.DataAlta).Year) : "") }
                                                    , o => o.Paciente.NomeCompleto
                                                    );


        }

    }

    public class SetAltaInput
    {
        public long? atendimentoId { get; set; }
        public long? altaMedicaLeitoId { get; set; }
        public long? altaGrupoCidId { get; set; }
        public long? motivoAltaId { get; set; }
        public DateTime dataAltaMedica { get; set; }
        public DateTime dataAlta { get; set; }
        public DateTime? dataPrevisaoAlta { get; set; }
        public string NumeroObito { get; set; }
    }
}
