﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto
{
    [AutoMap(typeof(AtendimentoStatus))]
    public class AtendimentoStatusDto : CamposPadraoCRUDDto
    {
    }
}
