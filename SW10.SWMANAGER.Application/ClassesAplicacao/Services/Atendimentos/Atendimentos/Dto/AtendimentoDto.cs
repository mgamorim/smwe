﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.PainelSenhas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.ServicosMedicosPrestados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposCID.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Nacionalidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Origens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Empresas.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto
{
    [AutoMap(typeof(Atendimento))]
    public class AtendimentoDto : CamposPadraoCRUDDto
    {
        #region campos
        public string Titular { get; set; }
        public string GuiaNumero { get; set; }
        public string Matricula { get; set; }
        public string Responsavel { get; set; }
        public string RgResponsavel { get; set; }
        public string CpfResponsavel { get; set; }
        public string NumeroGuia { get; set; }
        public int? QtdSessoes { get; set; }
        public string Senha { get; set; }
        public string Parentesco { get; set; }
        public string Observacao { get; set; }
        public long? PacienteId { get; set; }
        public long? OrigemId { get; set; }
        public long? MedicoId { get; set; }
        public long? EspecialidadeId { get; set; }
        public long? EmpresaId { get; set; }
        public long? ConvenioId { get; set; }
        public long? PlanoId { get; set; }
        public long? AtendimentoStatusId { get; set; }
        public long? UnidadeOrganizacionalId { get; set; }
        public long? AtendimentoTipoId { get; set; }
        public long? GuiaId { get; set; }
        public long? FatGuiaId { get; set; }
        public long? LeitoId { get; set; }
        public long? MotivoAltaId { get; set; }
        public long? NacionalidadeResponsavelId { get; set; }
        public long? TipoAcomodacaoId { get; set; }
        public bool IsAmbulatorioEmergencia { get; set; }
        public bool IsInternacao { get; set; }
        public bool IsHomeCare { get; set; }
        public bool IsPreatendimento { get; set; }
        public bool IsControlaTev { get; set; }
        public long? ServicoMedicoPrestadoId { get; set; }
        public string TelefonePreAtendimento { get; set; }
        public string IdentPreAtendimento { get; set; }
        public string CpfPreAtendimento { get; set; }
        public string NomePreAtendimento { get; set; }
        public long? AltaGrupoCIDId { get; set; }
        public string CodDependente { get; set; }

        public DateTime? DataRetorno { get; set; }
        public DateTime? DataRevisao { get; set; }
        public DateTime? DataPreatendimento { get; set; }
        public DateTime? DataPrevistaAtendimento { get; set; }
        public DateTime DataRegistro { get; set; }
        public DateTime? DataAlta { get; set; }
        public DateTime? DataAltaMedica { get; set; }
        public DateTime? ValidadeCarteira { get; set; }
        public DateTime? ValidadeSenha { get; set; }
        public DateTime? DataAutorizacao { get; set; }
        public DateTime? DataNascPreAtendimento { get; set; }
        public DateTime? DataPrevistaAlta { get; set; }
        public long? DiasAutorizacao { get; set; }
        public DateTime? DataUltimoPagamento { get; set; }

        public NacionalidadeDto Nacionalidade { get; set; }
        public PacienteDto Paciente { get; set; }
        public OrigemDto Origem { get; set; }
        public MedicoDto Medico { get; set; }
        public EspecialidadeDto Especialidade { get; set; }
        public EmpresaDto Empresa { get; set; }
        public ConvenioDto Convenio { get; set; }
        public PlanoDto Plano { get; set; }
        public TipoAtendimentoDto AtendimentoTipo { get; set; }
        public GuiaDto Guia { get; set; }
        public FaturamentoGuiaDto FatGuia { get; set; }
        public ServicoMedicoPrestadoDto ServicoMedicoPrestado { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }
        public LeitoDto Leito { get; set; }
        public TipoAcomodacaoDto TipoAcomodacao { get; set; }
        public MotivoAltaDto MotivoAlta { get; set; }
        public GrupoCIDDto AltaGrupoCID { get; set; }
        public string NumeroObito { get; set; }

        public long? MovimentacaoSenhalId { get; set; }
        public long? ProximoTipoLocalChamdaId { get; set; }
        public long? LocalChamadaId { get; set; }
        public long? TipoLocalChamadaId { get; set; }
        public LocalChamadaDto LocalChamada { get; set; }
        // public TipoLocalChamadaDto TipoLocalChamada { get; set; }

        public List<PrescricaoStatusDto> ListaStatus { get; set; }

        public long? AtendimentoMotivoCancelamentoId { get; set; }

        #endregion

        #region Mapeamento

        public static Atendimento Mapear(AtendimentoDto atendimentoDto)
        {
            Atendimento atendimento = new Atendimento();

            atendimento.GuiaNumero = atendimentoDto.GuiaNumero;
            atendimento.Matricula = atendimentoDto.Matricula;
            atendimento.Responsavel = atendimentoDto.Responsavel;
            atendimento.RgResponsavel = atendimentoDto.RgResponsavel;
            atendimento.CpfResponsavel = atendimentoDto.CpfResponsavel;
            atendimento.NumeroGuia = atendimentoDto.NumeroGuia;
            atendimento.QtdSessoes = atendimentoDto.QtdSessoes;
            atendimento.Senha = atendimentoDto.Senha;
            atendimento.Parentesco = atendimentoDto.Parentesco;
            atendimento.Observacao = atendimentoDto.Observacao;
            atendimento.PacienteId = atendimentoDto.PacienteId;
            atendimento.OrigemId = atendimentoDto.OrigemId;
            atendimento.MedicoId = atendimentoDto.MedicoId;
            atendimento.EspecialidadeId = atendimentoDto.EspecialidadeId;
            atendimento.EmpresaId = atendimentoDto.EmpresaId;
            atendimento.ConvenioId = atendimentoDto.ConvenioId;
            atendimento.PlanoId = atendimentoDto.PlanoId;
            atendimento.AtendimentoStatusId = atendimentoDto.AtendimentoStatusId;
            atendimento.UnidadeOrganizacionalId = atendimentoDto.UnidadeOrganizacionalId;
            atendimento.AtendimentoTipoId = atendimentoDto.AtendimentoTipoId;
            atendimento.GuiaId = atendimentoDto.GuiaId;
            atendimento.FatGuiaId = atendimentoDto.FatGuiaId;
            atendimento.LeitoId = atendimentoDto.LeitoId;
            atendimento.MotivoAltaId = atendimentoDto.MotivoAltaId;
            atendimento.NacionalidadeResponsavelId = atendimentoDto.NacionalidadeResponsavelId;
            atendimento.IsAmbulatorioEmergencia = atendimentoDto.IsAmbulatorioEmergencia;
            atendimento.IsInternacao = atendimentoDto.IsInternacao;
            atendimento.IsHomeCare = atendimentoDto.IsHomeCare;
            atendimento.IsPreatendimento = atendimentoDto.IsPreatendimento;
            atendimento.ServicoMedicoPrestadoId = atendimentoDto.ServicoMedicoPrestadoId;
            atendimento.AltaGrupoCIDId = atendimentoDto.AltaGrupoCIDId;
            atendimento.DataRetorno = atendimentoDto.DataRetorno;
            atendimento.DataRevisao = atendimentoDto.DataRevisao;
            atendimento.DataPreatendimento = atendimentoDto.DataPreatendimento;
            atendimento.DataPrevistaAtendimento = atendimentoDto.DataPrevistaAtendimento;
            atendimento.DataRegistro = atendimentoDto.DataRegistro;
            atendimento.DataAlta = atendimentoDto.DataAlta;
            atendimento.DataAltaMedica = atendimentoDto.DataAltaMedica;
            atendimento.ValidadeCarteira = atendimentoDto.ValidadeCarteira;
            atendimento.ValidadeSenha = atendimentoDto.ValidadeSenha;
            atendimento.DataAutorizacao = atendimentoDto.DataAutorizacao;
            atendimento.DiasAutorizacao = atendimentoDto.DiasAutorizacao;
            atendimento.DataPrevistaAlta = atendimentoDto.DataPrevistaAlta;
            atendimento.TipoAcomodacaoId = atendimentoDto.TipoAcomodacaoId;

            return atendimento;
        }

        public static AtendimentoDto Mapear(Atendimento atendimento)
        {
            AtendimentoDto atendimentoDto = new AtendimentoDto();

            atendimentoDto.Id = atendimento.Id;
            atendimentoDto.GuiaNumero = atendimento.GuiaNumero;
            atendimentoDto.Matricula = atendimento.Matricula;
            atendimentoDto.Responsavel = atendimento.Responsavel;
            atendimentoDto.RgResponsavel = atendimento.RgResponsavel;
            atendimentoDto.CpfResponsavel = atendimento.CpfResponsavel;
            atendimentoDto.NumeroGuia = atendimento.NumeroGuia;
            atendimentoDto.QtdSessoes = atendimento.QtdSessoes;
            atendimentoDto.Senha = atendimento.Senha;
            atendimentoDto.Parentesco = atendimento.Parentesco;
            atendimentoDto.Observacao = atendimento.Observacao;
            atendimentoDto.PacienteId = atendimento.PacienteId;
            atendimentoDto.OrigemId = atendimento.OrigemId;
            atendimentoDto.MedicoId = atendimento.MedicoId;
            atendimentoDto.EspecialidadeId = atendimento.EspecialidadeId;
            atendimentoDto.EmpresaId = atendimento.EmpresaId;
            atendimentoDto.ConvenioId = atendimento.ConvenioId;
            atendimentoDto.PlanoId = atendimento.PlanoId;
            atendimentoDto.AtendimentoStatusId = atendimento.AtendimentoStatusId;
            atendimentoDto.UnidadeOrganizacionalId = atendimento.UnidadeOrganizacionalId;
            atendimentoDto.AtendimentoTipoId = atendimento.AtendimentoTipoId;
            atendimentoDto.GuiaId = atendimento.GuiaId;
            atendimentoDto.FatGuiaId = atendimento.FatGuiaId;
            atendimentoDto.LeitoId = atendimento.LeitoId;
            atendimentoDto.MotivoAltaId = atendimento.MotivoAltaId;
            atendimentoDto.NacionalidadeResponsavelId = atendimento.NacionalidadeResponsavelId;
            atendimentoDto.IsAmbulatorioEmergencia = atendimento.IsAmbulatorioEmergencia;
            atendimentoDto.IsInternacao = atendimento.IsInternacao;
            atendimentoDto.IsHomeCare = atendimento.IsHomeCare;
            atendimentoDto.IsPreatendimento = atendimento.IsPreatendimento;
            atendimentoDto.ServicoMedicoPrestadoId = atendimento.ServicoMedicoPrestadoId;
            atendimentoDto.AltaGrupoCIDId = atendimento.AltaGrupoCIDId;
            atendimentoDto.DataRetorno = atendimento.DataRetorno;
            atendimentoDto.DataRevisao = atendimento.DataRevisao;
            atendimentoDto.DataPreatendimento = atendimento.DataPreatendimento;
            atendimentoDto.DataPrevistaAtendimento = atendimento.DataPrevistaAtendimento;
            atendimentoDto.DataRegistro = atendimento.DataRegistro;
            atendimentoDto.DataAlta = atendimento.DataAlta;
            atendimentoDto.DataAltaMedica = atendimento.DataAltaMedica;
            atendimentoDto.ValidadeCarteira = atendimento.ValidadeCarteira;
            atendimentoDto.ValidadeSenha = atendimento.ValidadeSenha;
            atendimentoDto.DataAutorizacao = atendimento.DataAutorizacao;
            atendimentoDto.DiasAutorizacao = atendimento.DiasAutorizacao;
            atendimentoDto.DataPrevistaAlta = atendimento.DataPrevistaAlta;
            atendimentoDto.TipoAcomodacaoId = atendimento.TipoAcomodacaoId;

            if (atendimento.Paciente != null)
            {
                atendimentoDto.Paciente = PacienteDto.Mapear(atendimento.Paciente);
            }

            if (atendimento.Medico != null)
            {
                atendimentoDto.Medico = MedicoDto.Mapear(atendimento.Medico);
            }

            if (atendimento.Leito != null)
            {
                atendimentoDto.Leito = LeitoDto.Mapear(atendimento.Leito);
            }

            if (atendimento.Convenio != null)
            {
                atendimentoDto.Convenio = ConvenioDto.Mapear(atendimento.Convenio);
            }

            if (atendimento.FatGuia != null)
            {
                atendimentoDto.FatGuia = FaturamentoGuiaDto.Mapear(atendimento.FatGuia);
            }

            if (atendimento.AtendimentoTipo != null)
            {
                atendimentoDto.AtendimentoTipo = TipoAtendimentoDto.Mapear(atendimento.AtendimentoTipo);
            }

            if (atendimento.UnidadeOrganizacional != null)
            {
                atendimentoDto.UnidadeOrganizacional = UnidadeOrganizacionalDto.Mapear(atendimento.UnidadeOrganizacional);
            }

            if (atendimento.TipoAcomodacao != null)
            {
                atendimentoDto.TipoAcomodacao = TipoAcomodacaoDto.Mapear(atendimento.TipoAcomodacao);
            }

            if (atendimento.Plano != null)
            {
                atendimentoDto.Plano = PlanoDto.Mapear(atendimento.Plano);
            }

            if (atendimento.Origem != null)
            {
                atendimentoDto.Origem = OrigemDto.Mapear(atendimento.Origem);
            }

            return atendimentoDto;
        }

        public static IEnumerable<Atendimento> Mapear(List<AtendimentoDto> atendimentoDto)
        {
            foreach (var item in atendimentoDto)
            {
                Atendimento atendimento = new Atendimento();

                atendimento.GuiaNumero = item.GuiaNumero;
                atendimento.Matricula = item.Matricula;
                atendimento.Responsavel = item.Responsavel;
                atendimento.RgResponsavel = item.RgResponsavel;
                atendimento.CpfResponsavel = item.CpfResponsavel;
                atendimento.NumeroGuia = item.NumeroGuia;
                atendimento.QtdSessoes = item.QtdSessoes;
                atendimento.Senha = item.Senha;
                atendimento.Parentesco = item.Parentesco;
                atendimento.Observacao = item.Observacao;
                atendimento.PacienteId = item.PacienteId;
                atendimento.OrigemId = item.OrigemId;
                atendimento.MedicoId = item.MedicoId;
                atendimento.EspecialidadeId = item.EspecialidadeId;
                atendimento.EmpresaId = item.EmpresaId;
                atendimento.ConvenioId = item.ConvenioId;
                atendimento.PlanoId = item.PlanoId;
                atendimento.AtendimentoStatusId = item.AtendimentoStatusId;
                atendimento.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                atendimento.AtendimentoTipoId = item.AtendimentoTipoId;
                atendimento.GuiaId = item.GuiaId;
                atendimento.FatGuiaId = item.FatGuiaId;
                atendimento.LeitoId = item.LeitoId;
                atendimento.MotivoAltaId = item.MotivoAltaId;
                atendimento.NacionalidadeResponsavelId = item.NacionalidadeResponsavelId;
                atendimento.IsAmbulatorioEmergencia = item.IsAmbulatorioEmergencia;
                atendimento.IsInternacao = item.IsInternacao;
                atendimento.IsHomeCare = item.IsHomeCare;
                atendimento.IsPreatendimento = item.IsPreatendimento;
                atendimento.ServicoMedicoPrestadoId = item.ServicoMedicoPrestadoId;
                atendimento.AltaGrupoCIDId = item.AltaGrupoCIDId;
                atendimento.DataRetorno = item.DataRetorno;
                atendimento.DataRevisao = item.DataRevisao;
                atendimento.DataPreatendimento = item.DataPreatendimento;
                atendimento.DataPrevistaAtendimento = item.DataPrevistaAtendimento;
                atendimento.DataRegistro = item.DataRegistro;
                atendimento.DataAlta = item.DataAlta;
                atendimento.DataAltaMedica = item.DataAltaMedica;
                atendimento.ValidadeCarteira = item.ValidadeCarteira;
                atendimento.ValidadeSenha = item.ValidadeSenha;
                atendimento.DataAutorizacao = item.DataAutorizacao;
                atendimento.DiasAutorizacao = item.DiasAutorizacao;
                atendimento.DataPrevistaAlta = item.DataPrevistaAlta;
                atendimento.TipoAcomodacaoId = item.TipoAcomodacaoId;

                yield return atendimento;
            }
        }

        public static IEnumerable<AtendimentoDto> Mapear(List<Atendimento> atendimento)
        {
            foreach (var item in atendimento)
            {
                AtendimentoDto atendimentoDto = new AtendimentoDto();

                atendimentoDto.Id = item.Id;
                atendimentoDto.GuiaNumero = item.GuiaNumero;
                atendimentoDto.Matricula = item.Matricula;
                atendimentoDto.Responsavel = item.Responsavel;
                atendimentoDto.RgResponsavel = item.RgResponsavel;
                atendimentoDto.CpfResponsavel = item.CpfResponsavel;
                atendimentoDto.NumeroGuia = item.NumeroGuia;
                atendimentoDto.QtdSessoes = item.QtdSessoes;
                atendimentoDto.Senha = item.Senha;
                atendimentoDto.Parentesco = item.Parentesco;
                atendimentoDto.Observacao = item.Observacao;
                atendimentoDto.PacienteId = item.PacienteId;
                atendimentoDto.OrigemId = item.OrigemId;
                atendimentoDto.MedicoId = item.MedicoId;
                atendimentoDto.EspecialidadeId = item.EspecialidadeId;
                atendimentoDto.EmpresaId = item.EmpresaId;
                atendimentoDto.ConvenioId = item.ConvenioId;
                atendimentoDto.PlanoId = item.PlanoId;
                atendimentoDto.AtendimentoStatusId = item.AtendimentoStatusId;
                atendimentoDto.UnidadeOrganizacionalId = item.UnidadeOrganizacionalId;
                atendimentoDto.AtendimentoTipoId = item.AtendimentoTipoId;
                atendimentoDto.GuiaId = item.GuiaId;
                atendimentoDto.FatGuiaId = item.FatGuiaId;
                atendimentoDto.LeitoId = item.LeitoId;
                atendimentoDto.MotivoAltaId = item.MotivoAltaId;
                atendimentoDto.NacionalidadeResponsavelId = item.NacionalidadeResponsavelId;
                atendimentoDto.IsAmbulatorioEmergencia = item.IsAmbulatorioEmergencia;
                atendimentoDto.IsInternacao = item.IsInternacao;
                atendimentoDto.IsHomeCare = item.IsHomeCare;
                atendimentoDto.IsPreatendimento = item.IsPreatendimento;
                atendimentoDto.ServicoMedicoPrestadoId = item.ServicoMedicoPrestadoId;
                atendimentoDto.AltaGrupoCIDId = item.AltaGrupoCIDId;
                atendimentoDto.DataRetorno = item.DataRetorno;
                atendimentoDto.DataRevisao = item.DataRevisao;
                atendimentoDto.DataPreatendimento = item.DataPreatendimento;
                atendimentoDto.DataPrevistaAtendimento = item.DataPrevistaAtendimento;
                atendimentoDto.DataRegistro = item.DataRegistro;
                atendimentoDto.DataAlta = item.DataAlta;
                atendimentoDto.DataAltaMedica = item.DataAltaMedica;
                atendimentoDto.ValidadeCarteira = item.ValidadeCarteira;
                atendimentoDto.ValidadeSenha = item.ValidadeSenha;
                atendimentoDto.DataAutorizacao = item.DataAutorizacao;
                atendimentoDto.DiasAutorizacao = item.DiasAutorizacao;
                atendimentoDto.DataPrevistaAlta = item.DataPrevistaAlta;
                atendimentoDto.TipoAcomodacaoId = item.TipoAcomodacaoId;

                if (item.Paciente != null)
                {
                    atendimentoDto.Paciente = PacienteDto.Mapear(item.Paciente);
                }

                if (item.Medico != null)
                {
                    atendimentoDto.Medico = MedicoDto.Mapear(item.Medico);
                }

                if (item.Leito != null)
                {
                    atendimentoDto.Leito = LeitoDto.Mapear(item.Leito);
                }

                if (item.Convenio != null)
                {
                    atendimentoDto.Convenio = ConvenioDto.Mapear(item.Convenio);
                }

                if (item.FatGuia != null)
                {
                    atendimentoDto.FatGuia = FaturamentoGuiaDto.Mapear(item.FatGuia);
                }

                if (item.AtendimentoTipo != null)
                {
                    atendimentoDto.AtendimentoTipo = TipoAtendimentoDto.Mapear(item.AtendimentoTipo);
                }

                if (item.UnidadeOrganizacional != null)
                {
                    atendimentoDto.UnidadeOrganizacional = UnidadeOrganizacionalDto.Mapear(item.UnidadeOrganizacional);
                }

                if (item.TipoAcomodacao != null)
                {
                    atendimentoDto.TipoAcomodacao = TipoAcomodacaoDto.Mapear(item.TipoAcomodacao);
                }

                if (item.Plano != null)
                {
                    atendimentoDto.Plano = PlanoDto.Mapear(item.Plano);
                }

                if (item.Origem != null)
                {
                    atendimentoDto.Origem = OrigemDto.Mapear(item.Origem);
                }

                yield return atendimentoDto;
            }
        }

        public static AtendimentoDto MapearFromCore(Atendimento atendimento)
        {
            var atendimentoDto = new AtendimentoDto();

            atendimentoDto.Id = atendimento.Id;
            atendimentoDto.Titular = atendimento.Titular;
            atendimentoDto.GuiaNumero = atendimento.GuiaNumero;
            atendimentoDto.Matricula = atendimento.Matricula;
            atendimentoDto.Responsavel = atendimento.Responsavel;
            atendimentoDto.RgResponsavel = atendimento.RgResponsavel;
            atendimentoDto.CpfResponsavel = atendimento.CpfResponsavel;
            atendimentoDto.NumeroGuia = atendimento.NumeroGuia;
            atendimentoDto.QtdSessoes = atendimento.QtdSessoes;
            atendimentoDto.Senha = atendimento.Senha;
            atendimentoDto.Parentesco = atendimento.Parentesco;
            atendimentoDto.Observacao = atendimento.Observacao;
            atendimentoDto.PacienteId = atendimento.PacienteId;
            atendimentoDto.OrigemId = atendimento.OrigemId;
            atendimentoDto.MedicoId = atendimento.MedicoId;
            atendimentoDto.EspecialidadeId = atendimento.EspecialidadeId;
            atendimentoDto.EmpresaId = atendimento.EmpresaId;
            atendimentoDto.ConvenioId = atendimento.ConvenioId;
            atendimentoDto.PlanoId = atendimento.PlanoId;
            atendimentoDto.AtendimentoStatusId = atendimento.AtendimentoStatusId;
            atendimentoDto.UnidadeOrganizacionalId = atendimento.UnidadeOrganizacionalId;
            atendimentoDto.AtendimentoTipoId = atendimento.AtendimentoTipoId;
            atendimentoDto.GuiaId = atendimento.GuiaId;
            atendimentoDto.FatGuiaId = atendimento.FatGuiaId;
            atendimentoDto.LeitoId = atendimento.LeitoId;
            atendimentoDto.MotivoAltaId = atendimento.MotivoAltaId;
            atendimentoDto.NacionalidadeResponsavelId = atendimento.NacionalidadeResponsavelId;
            atendimentoDto.IsAmbulatorioEmergencia = atendimento.IsAmbulatorioEmergencia;
            atendimentoDto.IsInternacao = atendimento.IsInternacao;
            atendimentoDto.IsHomeCare = atendimento.IsHomeCare;
            atendimentoDto.IsPreatendimento = atendimento.IsPreatendimento;
            atendimentoDto.ServicoMedicoPrestadoId = atendimento.ServicoMedicoPrestadoId;
            atendimentoDto.TelefonePreAtendimento = "";
            atendimentoDto.IdentPreAtendimento = "";
            atendimentoDto.CpfPreAtendimento = "";
            atendimentoDto.NomePreAtendimento = "";
            atendimentoDto.AltaGrupoCIDId = atendimento.AltaGrupoCIDId;
            atendimentoDto.DataRetorno = atendimento.DataRetorno;
            atendimentoDto.DataRevisao = atendimento.DataRevisao;
            atendimentoDto.DataPreatendimento = atendimento.DataPreatendimento;
            atendimentoDto.DataPrevistaAtendimento = atendimento.DataPrevistaAtendimento;
            atendimentoDto.DataRegistro = atendimento.DataRegistro;
            atendimentoDto.DataAlta = atendimento.DataAlta;
            atendimentoDto.DataAltaMedica = atendimento.DataAltaMedica;
            atendimentoDto.ValidadeCarteira = atendimento.ValidadeCarteira;
            atendimentoDto.ValidadeSenha = atendimento.ValidadeSenha;
            atendimentoDto.DataAutorizacao = atendimento.DataAutorizacao;
            atendimentoDto.DiasAutorizacao = atendimento.DiasAutorizacao;
            atendimentoDto.DataNascPreAtendimento = null;
            atendimentoDto.DataPrevistaAlta = atendimento.DataPrevistaAlta;
            atendimentoDto.Nacionalidade = atendimento.Nacionalidade?.MapTo<NacionalidadeDto>();
            atendimentoDto.Paciente = atendimento.Paciente?.MapTo<PacienteDto>();
            atendimentoDto.Origem = atendimento.Origem?.MapTo<OrigemDto>();
            atendimentoDto.Medico = atendimento.Medico?.MapTo<MedicoDto>();
            atendimentoDto.Especialidade = atendimento.Especialidade?.MapTo<EspecialidadeDto>();
            atendimentoDto.Empresa = atendimento.Empresa?.MapTo<EmpresaDto>();
            atendimentoDto.Convenio = atendimento.Convenio?.MapTo<ConvenioDto>();
            atendimentoDto.Plano = atendimento.Plano?.MapTo<PlanoDto>();
            atendimentoDto.AtendimentoTipo = null;
            atendimentoDto.Guia = null;
            atendimentoDto.FatGuia = atendimento.FatGuia?.MapTo<FaturamentoGuiaDto>();
            atendimentoDto.ServicoMedicoPrestado = null;
            atendimentoDto.UnidadeOrganizacional = atendimento.UnidadeOrganizacional?.MapTo<UnidadeOrganizacionalDto>();

            if (atendimento.Leito != null)
            {
                atendimentoDto.Leito = LeitoDto.MapearFromCore(atendimento.Leito);
            }

            atendimentoDto.MotivoAlta = atendimento.MotivoAlta?.MapTo<MotivoAltaDto>();
            atendimentoDto.AltaGrupoCID = null;

            return atendimentoDto;
        }

        #endregion




    }
}
