﻿using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto
{
    public class AtendimentoIndexDto : CamposPadraoCRUDDto
    {
        public string Unidade { get; set; }
        public string CodigoAtendimento { get; set; }
        public string Paciente { get; set; }
        public string TipoLeito { get; set; }
        public string LeitoAtual { get; set; }
        public DateTime? DataRegistro { get; set; }
        public DateTime? DataInicioConta { get; set; }
        public DateTime? DataFim { get; set; }
        public string Convenio { get; set; }
        public string Matricula { get; set; }
        public string Guia { get; set; }
        public string NumeroGuia { get; set; }
        public string Empresa { get; set; }
        public DateTime? DataAlta { get; set; }
        public string Plano { get; set; }
        public string CodigoPaciente { get; set; }
        public bool IsControlaTev { get; set; }
        public string Medico { get; set; }
        public string Leito { get; set; }
        public long? AtendimentoMotivoCancelamentoId { get; set; }

    }
}
