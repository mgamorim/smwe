﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos
{
    public interface IAtendimentoAppService : IApplicationService
    {
        Task<long>CriarOuEditar(AtendimentoDto input);

        Task SetAlta(SetAltaInput input);

        Task SetAltaMedica(SetAltaInput input);

        Task Excluir(long id, long? motivoCancelamentoId=null);

        Task<AtendimentoDto> Obter(long id);

        Task<AtendimentoDto> ObterAssistencial(long id);

        //Task<AtendimentoDto> ObterPorLeito(long id);

        AtendimentoDto ObterPorLeito(long id);

        Task<long> CriarNovoAtendimento();

        Task<FileDto> ListarParaExcel(ListarAtendimentosInput input);
        
        Task<PagedResultDto<AtendimentoDto>> ListarTodos();

        Task<PagedResultDto<AtendimentoDto>> ListarParaInternacao();

        Task<PagedResultDto<AtendimentoIndexDto>> ListarAtendimentos(ListarAtendimentosInput input);

        Task<PagedResultDto<AtendimentoDto>> ListarFiltroPreAtendimento(ListarAtendimentosInput input);

        Task<PagedResultDto<AtendimentoDto>> ListarPorPaciente(ListarInput input);

        Task<PagedResultDto<AtendimentoIndexDto>> ListarFiltro(ListarAtendimentosInput input);

        Task<PagedResultDto<AtendimentoIndexDto>> ListarFiltroInternacao(ListarAtendimentosInput input);

        Task<PagedResultDto<GenericoIdNome>> ListarAtendimentoPaciente();

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarAtendimentosEmAbertoDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarAtendimentosComSaidaDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarAtendimentosAmbulatorioInternacao(DropdownInput dropdownInput);

        Task SetDataPrevistaAlta(SetAltaInput input);

        Task<ResultDropdownList> ListarAtendimentosSemAlta(DropdownInput dropdownInput);

        Task Reativar(long id, long? leitoId);
    }
}
