﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.AtendimentosLeitosMov.Dto;
using System.Threading.Tasks;
using System;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.AtendimentosLeitosMov;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AtendimentosLeitosMov.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AtendimentosLeitosMov
{
    public interface IAtendimentoLeitoMovAppService : IApplicationService
    {
       // Task Editar(AtendimentoLeitoMovDto input);

        Task Criar(AtendimentoLeitoMovDto input);

        Task Excluir(long id);

        AtendimentoLeitoMovDto Obter(long? id, long? atendimentoId = null);

        AtendimentoLeitoMovDto Obter(long atendimentoId);

        Task<PagedResultDto<AtendimentoLeitoMovIndexDto>> ListarFiltro(ListarAtendimentosLeitosMovInput input);

        Task Editar(AtendimentoLeitoMovDto atendimentoLeitoMovDto);

        Task ObterEditarAtendimento(AtendimentoDto atendimentoDto);

        Task<DefaultReturn<AtendimentoLeitoMovDto>> TransferirLeito(long leitoOrigemId, long leitoDestinoId, long atendimentoId, DateTime dataHoraTransferencia);

        Task<DefaultReturn<AtendimentoLeitoMovDto>> ExcluirMovimentoLeito(long id);

        Task<DefaultReturn<AtendimentoLeitoMovDto>> AltarDataMovimentoLeito(long id, DateTime novaData);
    }
}
