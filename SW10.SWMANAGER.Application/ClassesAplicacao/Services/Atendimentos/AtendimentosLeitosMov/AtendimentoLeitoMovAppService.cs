﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using Castle.Core.Internal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.AtendimentosLeitosMov.Dto;
using SW10.SWMANAGER.ClassesAplicacao.AtendimentosLeitosMov;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AtendimentosLeitosMov.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AtendimentosLeitosMov
{

    public class AtendimentoLeitoMovAppService : SWMANAGERAppServiceBase, IAtendimentoLeitoMovAppService
    {
        #region Injecao e Construtor

        private readonly IRepository<AtendimentoLeitoMov, long> _atendimentoLeitoMovRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IAtendimentoAppService _atendimentoAppService;
        //private readonly IListarAtendimentosExcelExporter _listarAtendimentosExcelExporter;
        private readonly IRepository<Leito, long> _leitoRepository;

        public AtendimentoLeitoMovAppService(
            IRepository<AtendimentoLeitoMov, long> atendimentoLeitoMovRepository,
            IUnitOfWorkManager unitOfWorkManager,
             IRepository<Atendimento, long> atendimentoRepository,
               IAtendimentoAppService atendimentoAppService,
               IRepository<Leito, long> leitoRepository
            //IListarAtendimentosExcelExporter listarAtendimentosExcelExporter,
            )
        {
            _atendimentoLeitoMovRepository = atendimentoLeitoMovRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _atendimentoRepository = atendimentoRepository;
            _atendimentoAppService = atendimentoAppService;
            _leitoRepository = leitoRepository;
            //_listarAtendimentosExcelExporter = listarAtendimentosExcelExporter;
        }

        #endregion injecao e construtor.

        [UnitOfWork]
        public async Task Criar(AtendimentoLeitoMovDto input)
        {
            try
            {
                var atendimentoLeitoMov = input.MapTo<AtendimentoLeitoMov>();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _atendimentoLeitoMovRepository.InsertAsync(atendimentoLeitoMov);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                //ALTERANDO ATENDOMENTO
                if (input.AtendimentoId.HasValue && input.AtendimentoId.Value > 0)
                {
                    AtendimentoDto atendimento = new AtendimentoDto();
                    atendimento.Id = Convert.ToInt64(input.AtendimentoId);
                    atendimento.LeitoId = Convert.ToInt64(input.LeitoId);

                    atendimento.UnidadeOrganizacionalId = Convert.ToInt64(input.UnidadeOrganizacionalId);

                    await ObterEditarAtendimento(atendimento);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"));
            }
        }

        [UnitOfWork]
        //public async Task<AtendimentoLeitoMovDto> Editar(AtendimentoLeitoMovDto atendimentoLeitoMovDto)
        public async Task Editar(AtendimentoLeitoMovDto atendimentoLeitoMovDto)
        {
            try
            {
                var result = await _atendimentoLeitoMovRepository
                    .GetAll()
                    .Include(m => m.Atendimento)
                    .Include(m => m.Leito)
                    .Include(m => m.User)
                    .WhereIf(atendimentoLeitoMovDto.LeitoId != null, m => m.LeitoId == atendimentoLeitoMovDto.LeitoId)
                    .Where(m => m.AtendimentoId == atendimentoLeitoMovDto.AtendimentoId)
                    .FirstOrDefaultAsync();

                // UNITY OF WORK ESTAVA CAINDO EM EXCEPTION
                //using (var unitOfWork = _unitOfWorkManager.Begin())
                //{
                if (result != null)
                {
                    result.DataFinal = atendimentoLeitoMovDto.DataFinal;

                    AsyncHelper.RunSync(() => _atendimentoLeitoMovRepository.UpdateAsync(result));//.MapTo<EstoquePreMovimentoDto>();

                    //unitOfWork.Complete();
                    //_unitOfWorkManager.Current.SaveChanges();
                    //unitOfWork.Dispose();

                }
                //    }


                var atendimentoLeitoMov = result.MapTo<AtendimentoLeitoMovDto>();

                //return atendimentoLeitoMov;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroEditar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _atendimentoLeitoMovRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"));
            }
        }

        [UnitOfWork(false)]
        public AtendimentoLeitoMovDto Obter(long? id, long? atendimentoId = null)
        {
            try
            {
                var result = _atendimentoLeitoMovRepository
                    .GetAll()
                      .Include(m => m.Atendimento)
                      .Include(m => m.Leito)
                      .Include(m => m.Atendimento.Paciente)
                      .Include(m => m.Atendimento.Paciente.SisPessoa)
                    //  .Include(m => m.User)
                    //        .Where(m => m.Atendimento.DataAlta == null)
                    .Where(m => m.LeitoId == id)
                    .Where(m => m.AtendimentoId == atendimentoId)
                    .FirstOrDefault();

                AtendimentoLeitoMovDto atendimentoLeitoMovDto = new AtendimentoLeitoMovDto();

                if (result != null)
                {
                    atendimentoLeitoMovDto.Id = result.Id;
                    atendimentoLeitoMovDto.DataInicial = result.DataInicial;
                    atendimentoLeitoMovDto.DataFinal = result.DataFinal;
                    atendimentoLeitoMovDto.DataInclusao = result.DataInclusao;
                    atendimentoLeitoMovDto.Atendimento = AtendimentoDto.MapearFromCore(result.Atendimento);
                    atendimentoLeitoMovDto.Leito = LeitoDto.MapearFromCore(result.Leito);
                }
                return atendimentoLeitoMovDto;

                //var atendimento = result.Atendimento
                //    .MapTo<AtendimentoLeitoMovDto>();

                //return atendimento;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        [UnitOfWork(false)]
        public AtendimentoLeitoMovDto Obter(long atendimentoId)
        {
            try
            {
                var result = _atendimentoLeitoMovRepository
                    .GetAll()
                      .Include(m => m.Atendimento)
                      .Include(m => m.Leito)
                      .Include(m => m.Atendimento.Paciente)
                      .Include(m => m.Atendimento.Paciente.SisPessoa)
                    //  .Include(m => m.User)
                    //        .Where(m => m.Atendimento.DataAlta == null)
                    .Where(m => m.AtendimentoId == atendimentoId)
                    .FirstOrDefault();

                AtendimentoLeitoMovDto atendimentoLeitoMovDto = new AtendimentoLeitoMovDto();

                if (result != null)
                {
                    atendimentoLeitoMovDto.Id = result.Id;
                    atendimentoLeitoMovDto.DataInicial = result.DataInicial;
                    atendimentoLeitoMovDto.DataFinal = result.DataFinal;
                    atendimentoLeitoMovDto.DataInclusao = result.DataInclusao;
                    atendimentoLeitoMovDto.Atendimento = AtendimentoDto.MapearFromCore(result.Atendimento);
                    atendimentoLeitoMovDto.Leito = LeitoDto.MapearFromCore(result.Leito);
                }
                return atendimentoLeitoMovDto;

                //var atendimento = result.Atendimento
                //    .MapTo<AtendimentoLeitoMovDto>();

                //return atendimento;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<AtendimentoLeitoMovIndexDto>> ListarFiltro(ListarAtendimentosLeitosMovInput input)
        {

            var contarVisitantes = 0;
            List<AtendimentoLeitoMovIndexDto> atendimentoLeitoMovDto = new List<AtendimentoLeitoMovIndexDto>();
            // List<AtendimentoLeitoMovDto> atendimentoLeitoMovDtos = new List<AtendimentoLeitoMovDto>();
            long atendimentoId = 0;
            var IdAtendimento = long.TryParse(input.Id, out atendimentoId);
            if (atendimentoId == 0)
            {
                throw new UserFriendlyException(L("SelecioneAtendimento"));
            }
            try
            {
                var query = _atendimentoLeitoMovRepository
                 .GetAll()
                .Include(m => m.Atendimento)
                .Include(m => m.Leito.TipoAcomodacao)
                //.Include(m => m.Leito)
                .Include(m => m.User)
                .Include(m => m.Atendimento.Paciente)
                .Include(m => m.Atendimento.Paciente.SisPessoa)
                .Include(m => m.Leito)
                .Where(m => m.AtendimentoId == atendimentoId)
                //.WhereIf(input.Paciente != null, m => m.Atendimento.Paciente.NomeCompleto == input.Paciente)
                //.WhereIf(input.Fornecedor > 0, m => m.FornecedorId == input.Fornecedor)
                //.WhereIf(input.Documento != null, m => m.Documento == input.Documento)
                .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper())
                )
                .Select(s => new AtendimentoLeitoMovIndexDto
                {
                    CodigoAtendimento = s.Atendimento.Codigo,
                    Leito = s.Leito.Descricao,
                    Id = s.Id,
                    TipoLeito = s.Leito.TipoAcomodacao.Descricao,
                    Paciente = s.Atendimento.Paciente.NomeCompleto,
                    DataInicial = s.DataInicial,
                    DataFinal = s.DataFinal,
                    DataInclusao = s.DataInclusao,
                    DataAlta = s.Atendimento.DataAlta
                })
                ;

                contarVisitantes = await query
                    .CountAsync();

                atendimentoLeitoMovDto = await query
                    .AsNoTracking()
                    .OrderBy("Id")
                    .PageBy(input)
                    .ToListAsync();


                var ultimoMovimento = atendimentoLeitoMovDto.LastOrDefault();

                if (ultimoMovimento != null)
                {
                    ultimoMovimento.IsUltimoHistorico = true;
                }

                //atendimentoLeitoMovDtos = atendimentoLeitoMov
                //    .MapTo<List<AtendimentoLeitoMovDto>>();

                return new PagedResultDto<AtendimentoLeitoMovIndexDto>(
                contarVisitantes,
                atendimentoLeitoMovDto
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork]
        public async Task ObterEditarAtendimento(AtendimentoDto atendimentoDto)
        {
            try
            {
                var result = await _atendimentoRepository
                    .GetAll()
                    .Where(m => m.Id == atendimentoDto.Id)
                    .FirstOrDefaultAsync();

                if (result != null)
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        result.LeitoId = atendimentoDto.LeitoId;
                        result.UnidadeOrganizacionalId = atendimentoDto.UnidadeOrganizacionalId;

                        await _atendimentoRepository.UpdateAsync(result);//.MapTo<EstoquePreMovimentoDto>();

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();

                    }
                }

                var atendimento = result
                    .MapTo<AtendimentoDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


        public async Task<DefaultReturn<AtendimentoLeitoMovDto>> TransferirLeito(long leitoOrigemId, long leitoDestinoId, long atendimentoId, DateTime dataHoraTransferencia)
        {

            var _retornoPadrao = new DefaultReturn<AtendimentoLeitoMovDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            AtendimentoLeitoMov ultimoMovimento = null;

            var ultimoMovimentoLista = _atendimentoLeitoMovRepository.GetAll()
                                                                       .Where(w => w.LeitoId == leitoOrigemId
                                                                               && w.AtendimentoId == atendimentoId
                                                                               && w.DataFinal == null)
                                                                               .OrderBy(o => o.Id)
                                                                       .ToList();

            if (ultimoMovimentoLista != null && ultimoMovimentoLista.Count > 0)
            {
                ultimoMovimento = ultimoMovimentoLista.LastOrDefault();

            }

            if (ultimoMovimento != null && (dataHoraTransferencia <= ultimoMovimento.DataInicial))
            {
                _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "MOVL001" });
            }

            else
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var atendimento = _atendimentoRepository.GetAll()
                                                        .Where(w => w.Id == atendimentoId)
                                                        .FirstOrDefault();

                    if (atendimento != null)
                    {
                        atendimento.LeitoId = leitoDestinoId;
                        AsyncHelper.RunSync(() => _atendimentoRepository.UpdateAsync(atendimento));
                    }


                    if (ultimoMovimento != null)
                    {
                        ultimoMovimento.DataFinal = dataHoraTransferencia;
                        AsyncHelper.RunSync(() => _atendimentoLeitoMovRepository.UpdateAsync(ultimoMovimento));
                    }

                    AtendimentoLeitoMov atendimentoLeitoMov = new AtendimentoLeitoMov();

                    atendimentoLeitoMov.AtendimentoId = atendimentoId;
                    atendimentoLeitoMov.DataInicial = dataHoraTransferencia;
                    atendimentoLeitoMov.DataInclusao = DateTime.Now;
                    atendimentoLeitoMov.LeitoId = leitoDestinoId;
                    atendimentoLeitoMov.UserId = AbpSession.UserId;

                    AsyncHelper.RunSync(() => _atendimentoLeitoMovRepository.InsertAsync(atendimentoLeitoMov));


                    var leitoOrigem = _leitoRepository.GetAll()
                                                      .Where(w => w.Id == leitoOrigemId)
                                                      .FirstOrDefault();
                    if (leitoOrigem != null)
                    {

                        leitoOrigem.LeitoStatusId = 1;

                        AsyncHelper.RunSync(() => _leitoRepository.UpdateAsync(leitoOrigem));
                    }


                    var leitoDestino = _leitoRepository.GetAll()
                                                     .Where(w => w.Id == leitoDestinoId)
                                                     .FirstOrDefault();

                    if (leitoDestino != null)
                    {

                        leitoDestino.LeitoStatusId = 2;

                        AsyncHelper.RunSync(() => _leitoRepository.UpdateAsync(leitoDestino));
                    }


                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            return _retornoPadrao;
        }


        [UnitOfWork]
        public async Task<DefaultReturn<AtendimentoLeitoMovDto>> ExcluirMovimentoLeito(long id)
        {
            var _retornoPadrao = new DefaultReturn<AtendimentoLeitoMovDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();


            AtendimentoLeitoMov penultimoMovimento = null;
            Leito leito = null;

            try
            {


                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var movimentoLeito = _atendimentoLeitoMovRepository.GetAll()
                                                                       .Where(w => w.Id == id)
                                                                       .FirstOrDefault();

                    if (movimentoLeito != null)
                    {
                        var historioAtendimentoMovimentoLeito = _atendimentoLeitoMovRepository.GetAll()
                                                                                              .Where(w => w.AtendimentoId == movimentoLeito.AtendimentoId)
                                                                                              .OrderBy(o => o.Id)
                                                                                              .ToList();

                        if (historioAtendimentoMovimentoLeito.Count <= 1)
                        {
                            _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "MOVL002" });
                        }
                        else
                        {
                            penultimoMovimento = historioAtendimentoMovimentoLeito[historioAtendimentoMovimentoLeito.Count - 2];

                           // penultimoMovimento.DataFinal = null;

                            leito = _leitoRepository.GetAll()
                                                        .Where(w => w.Id == penultimoMovimento.LeitoId)
                                                        .FirstOrDefault();

                            if(leito!=null)
                            {
                                if(leito.LeitoStatusId !=1)
                                {
                                    _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "MOVL004", Parametros = new List<object> { leito.Descricao} });
                                }
                            }
                        }



                        if (_retornoPadrao.Errors.Count == 0)
                        {
                            if (historioAtendimentoMovimentoLeito.Count > 1)
                            {
                                // penultimoMovimento = historioAtendimentoMovimentoLeito[historioAtendimentoMovimentoLeito.Count - 2];

                                penultimoMovimento.DataFinal = null;

                                //var leito = _leitoRepository.GetAll()
                                //                            .Where(w => w.Id == penultimoMovimento.LeitoId)
                                //                            .FirstOrDefault();

                                if (leito != null)
                                {
                                    //ocupado
                                    leito.LeitoStatusId = 2;

                                    var atendimento = _atendimentoRepository.GetAll()
                                                                            .Where(w => w.Id == movimentoLeito.AtendimentoId)
                                                                            .FirstOrDefault();

                                    if (atendimento != null)
                                    {
                                        atendimento.LeitoId = leito.Id;
                                    }
                                }

                                var ultimoMovimento = historioAtendimentoMovimentoLeito[historioAtendimentoMovimentoLeito.Count - 1];

                                var ultimoLeito = _leitoRepository.GetAll()
                                                            .Where(w => w.Id == ultimoMovimento.LeitoId)
                                                            .FirstOrDefault();

                                if (ultimoLeito != null)
                                {
                                    //vago
                                    ultimoLeito.LeitoStatusId = 1;
                                }


                                await _atendimentoLeitoMovRepository.DeleteAsync(ultimoMovimento.Id);
                            }
                        }
                    }






                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"));
            }

            return _retornoPadrao;
        }

        public async Task<DefaultReturn<AtendimentoLeitoMovDto>> AltarDataMovimentoLeito(long id, DateTime novaData)
        {

            var _retornoPadrao = new DefaultReturn<AtendimentoLeitoMovDto>();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();


            AtendimentoLeitoMov penultimoMovimento = null;


            var movimentoLeito = _atendimentoLeitoMovRepository.GetAll()
                                                          .Where(w => w.Id == id)
                                                          .FirstOrDefault();



            if (movimentoLeito != null)
            {
                var historioAtendimentoMovimentoLeito = _atendimentoLeitoMovRepository.GetAll()
                                                                                      .Where(w => w.AtendimentoId == movimentoLeito.AtendimentoId)
                                                                                      .ToList();




                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    if (historioAtendimentoMovimentoLeito.Count > 1)
                    {
                        penultimoMovimento = historioAtendimentoMovimentoLeito[historioAtendimentoMovimentoLeito.Count - 2];


                    }


                    if (penultimoMovimento != null && penultimoMovimento.DataInicial > novaData)
                    {
                        _retornoPadrao.Errors.Add(new ErroDto { CodigoErro = "MOVL003" });
                    }

                    if (_retornoPadrao.Errors.Count() == 0)
                    {
                        if (penultimoMovimento != null)
                        {
                            penultimoMovimento.DataFinal = novaData;
                        }
                        movimentoLeito.DataInicial = novaData;
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }

            return _retornoPadrao;


        }
    }
}
