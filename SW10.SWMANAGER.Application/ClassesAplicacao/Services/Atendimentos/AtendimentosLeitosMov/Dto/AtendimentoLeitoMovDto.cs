﻿using Abp.AutoMapper;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.AtendimentosLeitosMov;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.AtendimentosLeitosMov.Dto
{
    [AutoMap(typeof(AtendimentoLeitoMov))]
    public class AtendimentoLeitoMovDto : CamposPadraoCRUDDto
    {
        public DateTime? DataInicial { get; set; }

        public DateTime? DataFinal { get; set; }

        public DateTime? DataInclusao { get; set; }

        public long? UserId { get; set; }
        public User User { get; set; }

        public long? AtendimentoLeitoMovId { get; set; }
        public AtendimentoLeitoMovDto AtendimentoLeitoMov { get; set; }

        public long? AtendimentoId { get; set; }
        public AtendimentoDto Atendimento { get; set; }

        public long? LeitoId { get; set; }
        public LeitoDto Leito { get; set; }

        public long? UnidadeOrganizacionalId { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }

    }
}


