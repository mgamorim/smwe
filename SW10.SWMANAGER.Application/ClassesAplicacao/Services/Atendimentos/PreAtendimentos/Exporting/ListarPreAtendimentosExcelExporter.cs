﻿using System;
using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.DataExporting.Excel.EpPlus;
using Abp.Timing.Timezone;
using Abp.Runtime.Session;
using SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.PreAtendimentos.Exporting
{
    public class ListarPreAtendimentosExcelExporter : EpPlusExcelExporterBase, IListarPreAtendimentosExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ListarPreAtendimentosExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<PreAtendimentoDto> preAtendimentosDto)
        {
            return CreateExcelPackage(
                string.Format("PreAtendimentos_{0:yyyyMMdd_hhmmss}.xlsx", DateTime.Now),
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("PreAtendimentos"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Descricao")

                    );

                    AddObjects(
                        sheet, 2, preAtendimentosDto,
                        _ => _.Id,
                        _ => _.Descricao
                            );
                });
        }
    }
}
