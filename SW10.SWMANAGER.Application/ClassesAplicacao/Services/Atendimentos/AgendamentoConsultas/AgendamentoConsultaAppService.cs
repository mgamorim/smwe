﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.AgendamentoConsultas;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas.Exporting;
using Abp.UI;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas
{
    public class AgendamentoConsultaAppService : SWMANAGERAppServiceBase, IAgendamentoConsultaAppService
    {
        private readonly IRepository<AgendamentoConsulta, long> _agendamentoConsultaRepository;
        private readonly IListarAgendamentoConsultasExcelExporter _listarAgendamentoConsultasExcelExporter;

            public AgendamentoConsultaAppService(IRepository<AgendamentoConsulta, long> agendamentoConsultaRepository, IListarAgendamentoConsultasExcelExporter listarAgendamentoConsultasExcelExporter)
            {
                _agendamentoConsultaRepository = agendamentoConsultaRepository;
                _listarAgendamentoConsultasExcelExporter = listarAgendamentoConsultasExcelExporter;
            }

        public async Task CriarOuEditar(CriarOuEditarAgendamentoConsulta input)
        {
            try
            {
                var agendamentoConsulta = input.MapTo<AgendamentoConsulta>();
                if (input.Id.Equals(0))
                {
                    await _agendamentoConsultaRepository.InsertAsync(agendamentoConsulta);
                }
                else
                {
                    await _agendamentoConsultaRepository.UpdateAsync(agendamentoConsulta);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(long id)
        {
            try
            {
                await _agendamentoConsultaRepository.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<ListResultDto<AgendamentoConsultaDto>> ListarTodos()
        {
            try
            {
                var query = await _agendamentoConsultaRepository
                    .GetAll()
                    .Include(m => m.AgendamentoConsultaMedicoDisponibilidade.Intervalo)
                    .Include(m => m.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.Plano)
                    .ToListAsync();

                var agendamentoConsultasDto = query.MapTo<List<AgendamentoConsultaDto>>();

                return new ListResultDto<AgendamentoConsultaDto> { Items = agendamentoConsultasDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<AgendamentoConsultaDto>> Listar(ListarAgendamentoConsultasInput input)
        {
            var contarAgendamentoConsultas = 0;
            List<AgendamentoConsulta> agendamentoConsultas;
            List<AgendamentoConsultaDto> agendamentoConsultasDtos = new List<AgendamentoConsultaDto>();
            try
            {
                var query = _agendamentoConsultaRepository
                    .GetAll()
                    .Include(m => m.AgendamentoConsultaMedicoDisponibilidade.Intervalo)
                    .Include(m => m.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.Plano)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        //m.Especialidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarAgendamentoConsultas = await query
                    .CountAsync();

                agendamentoConsultas = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                agendamentoConsultasDtos = agendamentoConsultas
                    .MapTo<List<AgendamentoConsultaDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<AgendamentoConsultaDto>(
                contarAgendamentoConsultas,
                agendamentoConsultasDtos
                );
        }

        public async Task<FileDto> ListarParaExcel(ListarAgendamentoConsultasInput input)
        {
            try
            {
                var result = await Listar(input);
                var agendamentoConsultas = result.Items;
                return _listarAgendamentoConsultasExcelExporter.ExportToFile(agendamentoConsultas.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ICollection<AgendamentoConsulta>> ListarPorMedico(long? medicoId, long? medicoEspecialidadeId, DateTime start, DateTime end)
        {
            try
            {
                //var query = await _agendamentoConsultaRepository
                return await _agendamentoConsultaRepository
                    .GetAll()
                    .Include(m => m.AgendamentoConsultaMedicoDisponibilidade)
                    .Include(m => m.AgendamentoConsultaMedicoDisponibilidade.Intervalo)
                    .Include(m => m.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.Plano)
                    .WhereIf(medicoId.HasValue && medicoId >= 0, m => m.MedicoId == medicoId.Value)
                    .WhereIf(medicoEspecialidadeId.HasValue && medicoEspecialidadeId >= 0, m => m.MedicoEspecialidade.EspecialidadeId == medicoEspecialidadeId.Value)
                    .Where(m => m.DataAgendamento >= start && m.DataAgendamento <= end) // start <= m.DataAgendamento && end >= m.DataAgendamento)
                    .ToListAsync();
                //return query.MapTo<List<AgendamentoConsultaDto>>();
                //var agendamentoConsultas = await query
                ////.AsNoTracking()
                //.ToListAsync();
                //var agendamentoConsultasDtos = agendamentoConsultas.MapTo<List<AgendamentoConsultaDto>>();
                //return agendamentoConsultasDtos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ICollection<AgendamentoConsulta>> ListarPorData(DateTime start, DateTime end)
        {
            return await _agendamentoConsultaRepository
                .GetAll()
                .Include(m => m.AgendamentoConsultaMedicoDisponibilidade.Intervalo)
                .Include(m => m.Convenio)
                .Include(i => i.Convenio.SisPessoa)
                .Include(m => m.Medico)
                .Include(m => m.Medico.SisPessoa)
                .Include(m => m.MedicoEspecialidade.Especialidade)
                .Include(m => m.Paciente)
                .Include(m => m.Paciente.SisPessoa)
                .Include(m => m.Plano)
                .Where(m => m.DataAgendamento >= start && m.DataAgendamento <= end)
                .ToListAsync();
        }

        public async Task<CriarOuEditarAgendamentoConsulta> Obter(long id)
        {
            try
            {
                var result = await _agendamentoConsultaRepository
                    .GetAll()
                    .Include(m => m.AgendamentoConsultaMedicoDisponibilidade.Intervalo)
                    .Include(m => m.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.Plano)
                    .FirstOrDefaultAsync(m => m.Id == id);

                var agendamentoConsulta = result
                    .MapTo<CriarOuEditarAgendamentoConsulta>();

                return agendamentoConsulta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<bool> ChecarDisponibilidade(long medicoDisponibilidadeId, DateTime hora, long id = 0)
        {
            var query = await _agendamentoConsultaRepository
                .GetAll()
                    .Include(m => m.AgendamentoConsultaMedicoDisponibilidade.Intervalo)
                    .Include(m => m.Convenio)
                    .Include(i => i.Convenio.SisPessoa)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.Plano)
                .Where(m => m.AgendamentoConsultaMedicoDisponibilidadeId == medicoDisponibilidadeId && m.HoraAgendamento == hora)
                .ToListAsync();
            if (query.Count() > 0)
            {
                //no caso de edição, se o horário for o próprio do registro, ele deve ser incluído.
                if (id > 0)
                {
                    var agendamento = query.FirstOrDefault().Id == id;
                    if (agendamento)
                    {
                        return true;
                    }
                }
                return false; //Não está disponível
            }
            else
            {
                return true; //Está disponível
            }
        }

    }
}
