﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos.AgendamentoConsultas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas.Dto;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AgendamentoConsultas
{
    public interface IAgendamentoConsultaAppService : IApplicationService
    {
        Task<PagedResultDto<AgendamentoConsultaDto>> Listar(ListarAgendamentoConsultasInput input);

        Task CriarOuEditar(CriarOuEditarAgendamentoConsulta input);

        Task Excluir(long id);

        Task<CriarOuEditarAgendamentoConsulta> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarAgendamentoConsultasInput input);

        Task<ICollection<AgendamentoConsulta>> ListarPorMedico(long? medicoId, long? medicoEspecialidadeId, DateTime start, DateTime end);

        Task<ListResultDto<AgendamentoConsultaDto>> ListarTodos();

        Task<bool> ChecarDisponibilidade(long medicoDisponibiliadeId, DateTime hora, long id = 0);

        Task<ICollection<AgendamentoConsulta>> ListarPorData(DateTime start, DateTime end);

    }
}
