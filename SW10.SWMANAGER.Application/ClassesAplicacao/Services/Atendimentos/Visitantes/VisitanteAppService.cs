﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using Castle.Core.Internal;
using SW10.SWMANAGER.ClassesAplicacao.Services.Visitantes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Vistantes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Visitantes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Visitantes
{

    public class VisitanteAppService : SWMANAGERAppServiceBase, IVisitanteAppService
    {
        #region Dependencias

        private readonly IRepository<Visitante, long> _visitanteRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        //    private readonly IAtendimentoAppService         _atendimentoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public VisitanteAppService(
            IRepository<Visitante, long> visitanteRepository,
            IRepository<Atendimento, long> atendimentoRepository,
        //    IAtendimentoAppService         atendimentoAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _visitanteRepository = visitanteRepository;
            _atendimentoRepository = atendimentoRepository;
            //   _atendimentoAppService = atendimentoAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        #endregion dependencias.

        [UnitOfWork]
        public async Task CriarOuEditar(VisitanteDto input)
        {
            try
            {
                //var visitante = input.MapTo<Visitante>();
                var visitante = VisitanteDto.MapearParaCore(input);

                visitante.AtendimentoId = input.AteId;

                if (visitante.AtendimentoId.HasValue && input.AteId != 0)
                {
                    visitante.AtendimentoId = input.AteId;
                }
                else
                {
                    visitante.AtendimentoId = input.AtendimentoId;
                }

                if (visitante.AtendimentoId.HasValue && visitante.AtendimentoId != 0)
                {
                    AtendimentoDto atendimentodto = new AtendimentoDto();
                    var atendimento = _atendimentoRepository.Get((long)visitante.AtendimentoId);//.MapTo<AtendimentoDto>(); // await _atendimentoAppService.Obter(Convert.ToInt64(visitante.AtendimentoId));
                    atendimentodto = AtendimentoDto.MapearFromCore(atendimento);
                    visitante.UnidadeOrganizacionalId = atendimento.UnidadeOrganizacionalId;
                    visitante.LeitoId = atendimento.LeitoId;
                }

                if (visitante.AtendimentoId == 0)
                {
                    visitante.AtendimentoId = null;
                }

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.Id.Equals(0))
                    {
                        await _visitanteRepository.InsertAsync(visitante);
                    }
                    else
                    {
                        visitante.Atendimento = null;
                        visitante.Leito = null;
                        visitante.UnidadeOrganizacional = null;

                        await _visitanteRepository.UpdateAsync(visitante);
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _visitanteRepository.DeleteAsync(id);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<VisitanteDto>> ListarFiltro(ListarVisitantesInput input)
        {

            var contarVisitantes = 0;
            List<Visitante> visitantes;
            List<VisitanteDto> visitantesDtos = new List<VisitanteDto>();
            try
            {
                DateTime data = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                //data = moment.data
                if (input.Nome == "") { input.Nome = null; }
                if (input.Paciente == "") { input.Paciente = null; }
                if (input.Documento == "") { input.Documento = null; }
                if (input.StartDate == null) { input.StartDate = data; }
                data = data.AddDays(1);
                if (input.EndDate == null) { input.EndDate = data; }

                var query = _visitanteRepository
                 .GetAll()
                .Include(m => m.Atendimento)
                .Include(m => m.UnidadeOrganizacional)
                .Include(m => m.Leito)
                .Include(m => m.Atendimento.Leito)
                .Include(m => m.Atendimento.Paciente)
                .Include(m => m.Atendimento.Paciente.SisPessoa)
                .Where(m => m.DataEntrada >= input.StartDate && m.DataEntrada <= input.EndDate)
                .WhereIf(!input.Nome.IsNullOrWhiteSpace(), m => m.Nome.ToUpper().Contains(input.Nome))
                .WhereIf(!input.Paciente.IsNullOrWhiteSpace(), m => m.Atendimento.Paciente.NomeCompleto.ToUpper().Contains(input.Paciente))
                .WhereIf(input.Fornecedor > 0, m => m.FornecedorId == input.Fornecedor)
                .WhereIf(!input.Documento.IsNullOrWhiteSpace(), m => m.Documento.ToUpper().Contains(input.Documento));

                contarVisitantes = await query.CountAsync();

                visitantes = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //visitantesDtos = visitantes
                //    .MapTo<List<VisitanteDto>>();

                foreach (var v in visitantes)
                {
                    var visitanteDto = VisitanteDto.MapearFormCore(v);
                    visitantesDtos.Add(visitanteDto);
                }

                return new PagedResultDto<VisitanteDto>(
                    contarVisitantes,
                       visitantesDtos
              );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

            //var contarVisitantes = 0;
            //List<Visitante> visitantes;
            //List<VisitanteDto> visitantesDto = new List<VisitanteDto>();
            //try
            //{
            //    var query = _visitanteRepository
            //     .GetAll()
            //    .Include(m => m.Atendimento)
            //    .Include(m => m.Fornedcedor)
            //    .Include(m => m.UnidadeOrganizacional)
            //    .Include(m => m.Atendimento.UnidadeOrganizacional)
            //    .Include(m => m.Atendimento.Leito)
            //     .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
            //        m.Nome.ToUpper().Contains(input.Filtro.ToUpper())
            //        );
            //    //.Where(m => m.Id == m.Atendimento.Id);


            //    contarVisitantes = await query
            //        .CountAsync();

            //    visitantes = await query
            //        .AsNoTracking()
            //        .OrderBy(input.Sorting)
            //        .PageBy(input)
            //        .ToListAsync();


            //    visitantesDto = visitantes
            //        .MapTo<List<VisitanteDto>>();

            //    return new PagedResultDto<VisitanteDto>(
            //   contarVisitantes,
            //   visitantes
            //   );
            //}
            //catch (Exception ex)
            //{
            //    throw new UserFriendlyException(L("ErroPesquisar"), ex);
            //}
        }

        public async Task<VisitanteDto> Obter(long id)
        {
            try
            {
                var result = await _visitanteRepository
                    .GetAll()
                    .Include(m => m.Atendimento)
                    .Include(m => m.Fornecedor)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.Atendimento.UnidadeOrganizacional)
                    .Include(m => m.Atendimento.Leito)
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    //.Include(m => m.AtendimentoId)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();


                // TROCAR PARA MAPEAMENTO MANUAL. ESTA MUITO LENTO

                VisitanteDto visitante = new VisitanteDto();

                if (result != null)
                {
                    visitante = VisitanteDto.MapearFormCore(result);
                }

                return visitante;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownModalVisitantePaciente(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            // List<AtendimentoDto> atendimentosDto = new List<AtendimentoDto>();
            try
            {
                //get com filtro
                var query = from p in _atendimentoRepository
                            .GetAll()
                            .Include(m => m.Paciente)
                            .Include(m => m.Paciente.SisPessoa)
                            .Where(m => m.DataAlta == null)
                            .Where(m => m.IsInternacao == true)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.Id.ToString().ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Paciente.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby p.Codigo ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Id, " - ", p.Paciente.NomeCompleto) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        public async Task<ResultDropdownList> ListarDropdownModalVisitantePaciente2(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            // List<AtendimentoDto> atendimentosDto = new List<AtendimentoDto>();
            try
            {
                //get com filtro
                var query = from p in _atendimentoRepository
                            .GetAll()
                            .Include(m => m.Paciente)
                            .Include(m => m.Paciente.SisPessoa)
                            .Where(m => m.DataAlta == null)
                            .Where(m => m.IsAmbulatorioEmergencia == true)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.Id.ToString().ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Paciente.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby p.Codigo ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Paciente.Id, " - ", p.Paciente.NomeCompleto) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}

//public async Task<PagedResultDto<VisitanteDto>> ListarFiltro(ListarVisitantesInput input)
//{
//    var contarAtendimentos = 0;
//    List<Atendimento> atendimentos;
//    try
//    {
//        //var query = _atendimentoRepository
//        //    .GetAll()
//        //    .Where(a => a.IsAmbulatorioEmergencia == true)
//        //    .Where(m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
//        //    .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
//        //    .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
//        //    .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
//        //    .WhereIf(input.MedicoId > 0, m => m.MedicoId == input.MedicoId)
//        //    .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId
//        //    );

//        //input.endDate2 = input.endDate2.AddDays(1);

//        var query = _atendimentoRepository
//          .GetAll()
//          .Include(m => m.Paciente)
//          .Include(m => m.Medico)
//          .Include(m => m.AtendimentoTipo)
//          .Include(m => m.Convenio)
//          .Include(m => m.Empresa)
//          .Include(m => m.Especialidade)
//          .Include(m => m.Guia)
//          .Include(m => m.Leito)
//          .Include(m => m.Leito.TipoAcomodacao)
//          .Include(m => m.MotivoAlta)
//          .Include(m => m.Nacionalidade)
//          .Include(m => m.Origem)
//          .Include(m => m.Plano)
//          .Include(m => m.ServicoMedicoPrestado)
//          .Include(m => m.UnidadeOrganizacional)
//          //=== filtro generico pablo 
//          .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
//            m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.UnidadeOrganizacional.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.Codigo.Contains(input.Filtro.ToUpper()) ||
//            m.Paciente.CodigoPaciente.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.Leito.TipoAcomodacao.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.Leito.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
//            m.Empresa.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()))
//        //  //===
//        //  .Where(a => a.IsAmbulatorioEmergencia == true)
//        //  .WhereIf(input. == "Alta", m => m.DataAlta >= input.StartDate && m.DataRegistro <= input.EndDate)
//        //  .WhereIf((input.FiltroData == "Atendimento"), m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate)
//        //  .WhereIf(input.EmpresaId > 0, m => m.EmpresaId == input.EmpresaId)
//        //  .WhereIf(input.UnidadeOrganizacionalId > 0, m => m.UnidadeOrganizacionalId == input.UnidadeOrganizacionalId)
//        //  .WhereIf(input.ConvenioId > 0, m => m.ConvenioId == input.ConvenioId)
//        //  .WhereIf(input.MedicoId > 0, m => m.MedicoId == input.MedicoId)
//        //  .WhereIf(input.PacienteId > 0, m => m.PacienteId == input.PacienteId)
//        //  .WhereIf(input.NacionalidadeResponsavelId > 0, m => m.NacionalidadeResponsavelId == input.NacionalidadeResponsavelId)
//        //  .WhereIf(input.IsAmbulatorioEmergencia.HasValue, m => m.IsAmbulatorioEmergencia == input.IsAmbulatorioEmergencia.Value)
//        //  .WhereIf(input.IsInternacao.HasValue, m => m.IsInternacao == input.IsInternacao.Value)
//        //  //.WhereIf(!input.Filtro.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()))
//        //   .WhereIf(!input.NomePaciente.IsNullOrEmpty(), m => m.Paciente.NomeCompleto.ToUpper().Contains(input.NomePaciente))
//        //  //.WhereIf(input.FiltroData == "Internado", m => m.DataRegistro >= input.StartDate && m.DataRegistro <= input.EndDate &&  m.DataAlta == null)
//        //  .WhereIf(input.FiltroData == "Internado", m => m.DataAlta == null)
//        ////.WhereIf(input.Internados, m => m.DataAlta == null)
//        ////.WhereIf(input.Internados, a => DateTime.Compare((DateTime)a.DataAlta, DateTime.Now) >= 0)
//        ;

//        contarAtendimentos = await query
//            .CountAsync();

//        atendimentos = await query
//            .AsNoTracking()
//            .OrderBy(input.Sorting)
//            .PageBy(input)
//            .ToListAsync();

//        var atendimentosDto = atendimentos
//          .MapTo<List<AtendimentoDto>>();

//        //return new PagedResultDto<AtendimentoDto>(
//        //    contarAtendimentos,
//        //    atendimentosDto
//        //    );

//        return new PagedResultDto<VisitanteDto>();
//    }
//    catch (Exception ex)
//    {
//        throw new UserFriendlyException(L("ErroPesquisar"), ex);
//    }
//}

//public async Task<FileDto> ListarParaExcel(ListarVisitantesInput input)
//{
//    try
//    {
//        //var result = await Listar(input);
//        var result = await ListarTodos();
//        var atendimentos = result.Items;
//        return _listarAtendimentosExcelExporter.ExportToFile(atendimentos.ToList());
//    }
//    catch (Exception ex)
//    {
//        throw new UserFriendlyException(L("ErroExportar"));
//    }
//}