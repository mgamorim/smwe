﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Fornecedores;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Visitantes;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Visitantes.Dto
{
    [AutoMap(typeof(Visitante))]
    public class VisitanteDto : CamposPadraoCRUDDto
    {

        public string Nome { get; set; }
        public string Documento { get; set; }
        public bool IsAcompanhante { get; set; }
        public bool IsVisitante { get; set; }
        public bool IsMedico { get; set; }
        public bool IsEmergencia { get; set; }
        public bool IsInternado { get; set; }
        public bool IsFornecedor { get; set; }
        public bool IsSetor { get; set; }
        public byte[] Foto { get; set; }
        public string FotoMimeType { get; set; }
        public DateTime? DataEntrada { get; set; }
        public DateTime? DataSaida { get; set; }
        public long AteId { get; set; }
                public long? UnidadeOrganizacionalId { get; set; }
        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }
        public long? AtendimentoId { get; set; }
        public AtendimentoDto Atendimento { get; set; }
        public long? LeitoId { get; set; }
        public LeitoDto Leito { get; set; }
        public long? FornecedorId { get; set; }
        public SisFornecedorDto Fornecedor { get; set; }
        public bool IsFinalizar { get; set; }

        public static VisitanteDto MapearFormCore(Visitante visitante)
        {
            var visitanteDto = new VisitanteDto();
            visitanteDto.Id = visitante.Id;
            visitanteDto.Nome = visitante.Nome;
            visitanteDto.Documento = visitante.Documento;
            visitanteDto.IsAcompanhante = visitante.IsAcompanhante;
            visitanteDto.IsVisitante = visitante.IsVisitante;
            visitanteDto.IsMedico = visitante.IsMedico;
            visitanteDto.IsEmergencia = visitante.IsEmergencia;
            visitanteDto.IsInternado = visitante.IsInternado;
            visitanteDto.IsFornecedor = visitante.IsFornecedor;
            visitanteDto.IsSetor = visitante.IsSetor;
            visitanteDto.Foto = visitante.Foto;
            visitanteDto.FotoMimeType = visitante.FotoMimeType;
            visitanteDto.DataEntrada = visitante.DataEntrada;
            visitanteDto.DataSaida = visitante.DataSaida;
            //visitanteDto.AteId = null;
            visitanteDto.UnidadeOrganizacionalId = visitante.UnidadeOrganizacionalId;
            visitanteDto.UnidadeOrganizacional = visitante.UnidadeOrganizacional?.MapTo<UnidadeOrganizacionalDto>();
            visitanteDto.AtendimentoId = visitante.AtendimentoId;
            visitanteDto.Atendimento = visitante.Atendimento?.MapTo<AtendimentoDto>();
            //visitanteDto.Atendimento.Paciente = visitante.Atendimento?.Paciente?.MapTo<PacienteDto>();
            //visitanteDto.Atendimento.Paciente.SisPessoa = visitante.Atendimento?.Paciente?.SisPessoa?.MapTo<SisPessoaDto>();
            visitanteDto.LeitoId = visitante.LeitoId;

            if (visitante.Leito != null)
            {
                visitanteDto.Leito = LeitoDto.MapearFromCore(visitante.Leito);
            }

            visitanteDto.FornecedorId = visitante.FornecedorId;
            visitanteDto.Fornecedor = visitante.Fornecedor?.MapTo<SisFornecedorDto>();
            visitanteDto.IsFinalizar = false;

            return visitanteDto;
        }

        public static Visitante MapearParaCore(VisitanteDto visitanteDto)
        {
            var visitante = new Visitante();

            visitante.Id = visitanteDto.Id;
            visitante.Nome = visitanteDto.Nome;
            visitante.Documento = visitanteDto.Documento;
            visitante.IsAcompanhante = visitanteDto.IsAcompanhante;
            visitante.IsVisitante = visitanteDto.IsVisitante;
            visitante.IsMedico = visitanteDto.IsMedico;
            visitante.IsEmergencia = visitanteDto.IsEmergencia;
            visitante.IsInternado = visitanteDto.IsInternado;
            visitante.IsFornecedor = visitanteDto.IsFornecedor;
            visitante.IsSetor = visitanteDto.IsSetor;
            visitante.Foto = visitanteDto.Foto;
            visitante.FotoMimeType = visitanteDto.FotoMimeType;
            visitante.DataEntrada = visitanteDto.DataEntrada;
            visitante.DataSaida = visitanteDto.DataSaida;
            //visitante.AteId = 
            visitante.UnidadeOrganizacionalId = visitanteDto.UnidadeOrganizacionalId;
            //visitante.UnidadeOrganizacional = visitanteDto.UnidadeOrganizacional.MapTo<UnidadeOrganizacional>();
            visitante.AtendimentoId = visitanteDto.AtendimentoId;
            //visitante.Atendimento = visitanteDto.Atendimento.MapTo<Atendimento>();
            visitante.LeitoId = visitanteDto.LeitoId;
            //visitante.Leito = visitanteDto.Leito.MapTo<Leito>();
            visitante.FornecedorId = visitanteDto.FornecedorId;
            //visitante.Fornecedor = visitanteDto.Fornecedor.MapTo<SisFornecedorDto>();
            //visitante.IsFinalizar = false;

            return visitante;
        }
    }

}
