﻿using System;
using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.DataExporting.Excel.EpPlus;
using Abp.Timing.Timezone;
using Abp.Runtime.Session;
using SW10.SWMANAGER.ClassesAplicacao.Services.Eventos.Eventos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Eventos.EventosMov.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Eventos.EventosMov.Exporting
{
    public class ListarEventosMovExcelExporter : EpPlusExcelExporterBase, IListarEventosMovExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ListarEventosMovExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<EventoMovDto> agendamentoConsultasDto)
        {
            return CreateExcelPackage(
                string.Format("Atendimentos_{0:yyyyMMdd_hhmmss}.xlsx", DateTime.Now),
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Atendimentos"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Descricao")

                    );

                    AddObjects(
                        sheet, 2, agendamentoConsultasDto,
                        _ => _.Id
                        
                            );
                });
        }
    }
}
