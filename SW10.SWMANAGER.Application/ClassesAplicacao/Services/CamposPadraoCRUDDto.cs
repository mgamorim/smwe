﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(CamposPadraoCRUD))]
    public abstract class CamposPadraoCRUDDto : FullAuditedEntityDto<long>
    {
        public bool IsSistema { get; set; }

        public virtual string Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual int? ImportaId { get; set; }
    }
}
