﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Ferramentas.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Dto
{
    public class ReboqueOutput
    {
        public string placa { get; set; }
        public string UF { get; set; }
        public string RNTC { get; set; }
    }
}
