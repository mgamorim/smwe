﻿using System.Collections.Generic;
using DFe.Classes.Flags;
using DFe.Classes.Entidades;
using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Dto
{
    [AutoMap(typeof(NFe.Classes.Servicos.Evento.retEnvEvento))]
    public class RetEnvEventoOutput
    {
        public RetEnvEventoOutput()
        {
            //this.retEvento = new List<retEvento>();
        }
        public string versao { get; set; }

        public int idLote { get; set; }

        public TipoAmbiente tpAmb { get; set; }

        public string verAplic { get; set; }

        public EstadoNfe cOrgao { get; set; }

        public int cStat { get; set; }

        public string xMotivo { get; set; }

        //public List<NFe.Classes.Servicos.Evento.retEvento> retEvento { get; set; }
    }
}