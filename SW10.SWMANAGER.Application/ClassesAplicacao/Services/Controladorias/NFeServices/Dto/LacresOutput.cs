﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Ferramentas.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Dto
{
    public class LacresOutput
    {
        public string nLacre { get; set; }
    }
}
