﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Ferramentas.Enums
{
    /// <summary>
    /// Tipo de Operação da NF-e: 
    /// 0=Entrada;
    /// 1=Saída
    /// </summary>
    public enum TpNFSW
    {
        Entrada = 0,
        Saida = 1
    }
}
