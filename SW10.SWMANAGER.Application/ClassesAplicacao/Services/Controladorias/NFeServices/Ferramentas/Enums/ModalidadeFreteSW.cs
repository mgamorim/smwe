﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Ferramentas.Enums
{
    public enum ModalidadeFreteSW
    {
        mfContaEmitente,
        mfContaDestinatario,
        mfContaTerceiros,
        mfSemFrete
    }
}
