﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Ferramentas.Enums
{
    public enum ModeloDocumentoSW
    {
        NFe = 55,
        MDFe = 58,
        NFCe = 65
    }
}
