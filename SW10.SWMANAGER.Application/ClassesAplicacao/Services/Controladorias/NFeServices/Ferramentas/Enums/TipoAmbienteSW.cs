﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Controladorias.NFeServices.Ferramentas.Enums
{
    public enum TipoAmbienteSW
    {
        Producao = 1,
        Homologacao = 2
    }
}
