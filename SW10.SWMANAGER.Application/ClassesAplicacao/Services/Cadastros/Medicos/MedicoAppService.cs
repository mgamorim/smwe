﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Medicos;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Exporting;
using Abp.UI;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Enderecos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos
{
    public class MedicoAppService : SWMANAGERAppServiceBase, IMedicoAppService
    {
        private readonly IRepository<Medico, long> _medicoRepository;
        private readonly IListarMedicosExcelExporter _listarMedicosExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IMedicoEspecialidadeAppService _medicoEspecialidadeAppService;
        private readonly IRepository<SisPessoa, long> _sisPessoa;

        public MedicoAppService(
            IRepository<Medico, long> medicoRepository,
            IListarMedicosExcelExporter listarMedicosExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoServicoAppService,
            IMedicoEspecialidadeAppService medicoEspecialidadeAppService,
            IRepository<SisPessoa, long> sisPessoa
            )
        {
            _medicoRepository = medicoRepository;
            _listarMedicosExcelExporter = listarMedicosExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoServicoAppService;
            _medicoEspecialidadeAppService = medicoEspecialidadeAppService;
            _sisPessoa = sisPessoa;
        }

        [UnitOfWork]
        public async Task<MedicoDto> CriarOuEditar(MedicoDto input)
        {
            try
            {
                var medico = input.MapTo<Medico>();

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Codigo = _ultimoIdAppService.ObterProximoCodigo("Medico").Result;
                        medico.Id = await _medicoRepository.InsertAndGetIdAsync(CarregarDadosMedico(input));
                        unitOfWork.Complete();

                        _unitOfWorkManager.Current.SaveChanges();

                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    var ori = _medicoRepository.Get(input.Id);
                    ori = CarregarDadosMedico(input);
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _medicoRepository.UpdateAsync(ori);
                        unitOfWork.Complete();

                        _unitOfWorkManager.Current.SaveChanges();

                        unitOfWork.Dispose();
                    }
                }




                //foreach (var medicoEspecialidade in medicosEspecialidade)
                //{
                //    medicoEspecialidade.IdEspecialidade = input.Id;
                //    using (var unitOfWork = _unitOfWorkManager.Begin())
                //    {
                //        if (medicoEspecialidade.IsDeleted)
                //        {
                //            await _medicoEspecialidadeAppService.Excluir(medicoEspecialidade);
                //        }
                //        else
                //        {
                //            await _medicoEspecialidadeAppService.CriarOuEditar(medicoEspecialidade);
                //        }
                //        unitOfWork.Complete();
                //        _unitOfWorkManager.Current.SaveChanges();
                //        unitOfWork.Dispose();
                //    }
                //}

                return medico.MapTo<MedicoDto>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(MedicoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _medicoRepository.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<ListarMedicoIndex>> Listar(ListarMedicosInput input)
        {
            var contarMedicos = 0;
            List<Medico> medicos;
            List<ListarMedicoIndex> medicosDtos = new List<ListarMedicoIndex>();
            try
            {
                var query = _medicoRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.SisPessoa)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Rg.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Nascimento.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.NomeMae.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.NomePai.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Logradouro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Numero.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Complemento.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Bairro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Estado.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Estado.Uf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Pais.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Pais.Sigla.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cep.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cns.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Email.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone1.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone2.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone3.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone4.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarMedicos = await query
                    .CountAsync();

                medicos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //medicosDtos = medicos
                //    .MapTo<List<MedicoDto>>();


                ListarMedicoIndex listarMedicoIndex;
                foreach (var item in medicos)
                {
                    listarMedicoIndex = new ListarMedicoIndex();

                    listarMedicoIndex.Id = item.Id;
                    listarMedicoIndex.Cpf = item.Cpf;
                    listarMedicoIndex.Rg = item.Rg;
                    listarMedicoIndex.Nascimento = item.Nascimento;
                    listarMedicoIndex.NomeCompleto = item.NomeCompleto;
                    listarMedicoIndex.NumeroConselho = item.NumeroConselho.ToString();

                    medicosDtos.Add(listarMedicoIndex);
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ListarMedicoIndex>(
                contarMedicos,
                medicosDtos
                );
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            List<GenericoIdNome> medicos;
            try
            {
                var query = _medicoRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.SisPessoa)
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                    m.NomeCompleto.ToUpper().Contains(input.ToUpper()) ||
                    m.Rg.ToUpper().Contains(input.ToUpper()) ||
                    m.Cpf.ToUpper().Contains(input.ToUpper()) ||
                    m.Nascimento.ToString().ToUpper().Contains(input.ToUpper()) ||
                    m.NomeMae.ToUpper().Contains(input.ToUpper()) ||
                    m.NomePai.ToUpper().Contains(input.ToUpper()) ||
                    m.Logradouro.ToUpper().Contains(input.ToUpper()) ||
                    m.Numero.ToUpper().Contains(input.ToUpper()) ||
                    m.Complemento.ToUpper().Contains(input.ToUpper()) ||
                    m.Bairro.ToUpper().Contains(input.ToUpper()) ||
                    m.Cidade.Nome.ToUpper().Contains(input.ToUpper()) ||
                    m.Estado.Nome.ToUpper().Contains(input.ToUpper()) ||
                    m.Estado.Uf.ToUpper().Contains(input.ToUpper()) ||
                    m.Pais.Nome.ToUpper().Contains(input.ToUpper()) ||
                    m.Pais.Sigla.ToUpper().Contains(input.ToUpper()) ||
                    m.Cep.ToUpper().Contains(input.ToUpper()) ||
                    m.Cns.ToString().ToUpper().Contains(input.ToUpper()) ||
                    m.Email.ToUpper().Contains(input.ToUpper()) ||
                    m.Telefone1.ToUpper().Contains(input.ToUpper()) ||
                    m.Telefone2.ToUpper().Contains(input.ToUpper()) ||
                    m.Telefone3.ToUpper().Contains(input.ToUpper()) ||
                    m.Telefone4.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.NomeCompleto })
                    .OrderBy(r => r.Nome);

                medicos = await query
                    .AsNoTracking()
                    .Take(10)
                    .ToListAsync();

                //medicosDtos = medicos
                //    .MapTo<List<GenericoIdNome>>();

                return new ListResultDto<GenericoIdNome> { Items = medicos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<MedicoDto>> ListarTodos()
        {
            List<MedicoDto> medicosDtos = new List<MedicoDto>();
            try
            {
                var medicos = await _medicoRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.SisPessoa)
                    .AsNoTracking()
                    .ToListAsync();

                medicosDtos = medicos
                    .MapTo<List<MedicoDto>>();

                return new ListResultDto<MedicoDto> { Items = medicosDtos };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarMedicosInput input)
        {
            try
            {
                var result = await Listar(input);
                var medicos = result.Items;
                return _listarMedicosExcelExporter.ExportToFile(medicos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ICollection<MedicoDto>> ListarPorEspecialidade(long id)
        {
            List<Medico> medicos;
            List<MedicoDto> medicosDtos = new List<MedicoDto>();
            try
            {
                var query = from m in _medicoRepository.GetAll()
                            from e in m.MedicoEspecialidades
                            where e.EspecialidadeId == id
                            select m;

                medicos = await query
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.SisPessoa)
                    .AsNoTracking()
                    .ToListAsync();

                medicosDtos = medicos
                    .MapTo<List<MedicoDto>>();

                return medicosDtos;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<MedicoDto> Obter(long id)
        {
            try
            {
                var result = await _medicoRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.Conselho)
                    .Include(m => m.Profissao)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s=> s.TipoEndereco))
                     .Include(m => m.SisPessoa.Enderecos.Select(s => s.Pais))
                     .Include(m => m.SisPessoa.Enderecos.Select(s => s.Cidade))
                     .Include(m => m.SisPessoa.Enderecos.Select(s => s.Estado))
                     .Include(m => m.SisPessoa.TipoTelefone1)
                     .Include(m => m.SisPessoa.TipoTelefone2)
                     .Include(m => m.SisPessoa.TipoTelefone3)
                     .Include(m => m.SisPessoa.TipoTelefone4)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                //.GetAllIncluding(
                //    m => m.Cidade,
                //    //m => m.Cidade.Estado,
                //    m => m.Estado,
                //    m => m.Pais,
                //    m => m.Profissao,
                //    m => m.Naturalidade,
                //    m => m.MedicoEspecialidades
                //)
                //.Where(m => m.Id == id)
                //.FirstOrDefaultAsync();

                var medico = MedicoDto.Mapear(result);

                return medico;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<MedicoDto> medicosDto = new List<MedicoDto>();
            try
            {
              //  dropdownInput.search = dropdownInput.search.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("é", "c");

                var query = from p in _medicoRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.NomeCompleto.ToLower().Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        .Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        .Replace("ã", "a").Replace("õ", "o")
                        .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        .Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        .Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        .Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.NomeCompleto ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.NomeCompleto) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownFatContaItem (DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<MedicoDto> medicosDto = new List<MedicoDto>();

            try
            {
                var query = from p in _medicoRepository.GetAll()

                            .WhereIf(dropdownInput.filtros.Length>0, 
                            
                            x=> !dropdownInput.filtros.Contains(x.Id.ToString())

                            )

                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.NomeCompleto ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.NomeCompleto) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        Medico CarregarDadosMedico(MedicoDto input)
        {
            Medico medico = null;

            if (input.Id == 0)
            {
                medico = new Medico();
            }
            else
            {
                var medicoAtual = _medicoRepository.GetAll()
                                                .Where(w => w.Id == input.Id)
                                                .Include(i => i.SisPessoa)
                                                .Include(i => i.SisPessoa.Enderecos)
                                                .Include(i => i.MedicoEspecialidades)
                                                .FirstOrDefault();

                medico = medicoAtual ?? new Medico();
            }

            medico.Id = input.Id;
            medico.Codigo = input.Codigo;
            medico.ConselhoId = input.ConselhoId;
            medico.NumeroConselho = input.NumeroConselho;
           // medico.Religiao = Convert.ToInt32(input.Religiao);  
            medico.IsAgendaConsulta = input.IsAgendaConsulta;
            medico.IsAtendimentoCirurgia = input.IsAtendimentoCirurgia;
            medico.IsEspecialista = input.IsEspecialista;
            medico.IsAgendaCirurgia = input.IsAgendaCirurgia;
            medico.IsAtendimentoInternacao = input.IsAtendimentoInternacao;
            medico.IsAtendimentoConsulta = input.IsAtendimentoConsulta;
            medico.IsExame = input.IsExame;
            medico.CorAgendamentoConsulta = input.CorAgendamentoConsulta;

            medico.IsAtivo = input.IsAtivo;

            medico.AssinaturaDigital = input.AssinaturaDigital;
            medico.AssinaturaDigitalMimeType = input.AssinaturaDigitalMimeType;

            medico.Nascimento = input.Nascimento;
            medico.Cpf = input.Cpf;


            SisPessoa sisPessoa = medico.SisPessoa;

            if (medico.SisPessoa == null)
            {
                sisPessoa = _sisPessoa.GetAll()
                                               .Where(w => w.Cpf == input.Cpf)
                                               .FirstOrDefault();

                medico.SisPessoa = sisPessoa;
            }



            if (sisPessoa != null)
            {
                medico.SisPessoaId = sisPessoa.Id;

                if (sisPessoa.Enderecos != null && sisPessoa.Enderecos.Count() > 0)
                {
                    CarregarEndereco(input, sisPessoa.Enderecos[0]);
                }
                else
                {
                    var endereco = new Endereco();
                    CarregarEndereco(input, endereco);

                    sisPessoa.Enderecos = new List<Endereco> { endereco };
                }


                //  convenio.SisPessoa = sisPessoa;
                
            }
            else
            {
                sisPessoa = new SisPessoa();
                medico.SisPessoa = sisPessoa;
                //sisPessoa.NomeCompleto = input.NomeCompleto; 
                //sisPessoa.Cpf = input.Cpf;
                //sisPessoa.Escolaridade = input.Escolaridade;
                //sisPessoa.NacionalidadeId = input.NacionalidadeId;
                //sisPessoa.Nascimento = input.Nascimento;
                //sisPessoa.NomeMae = input.NomeMae;
                //sisPessoa.NomePai = input.NomePai;
                //sisPessoa.ProfissaoId = input.ProfissaoId;
                //sisPessoa.Rg = input.Rg;
                //// sisPessoa.Nascimento = DateTime.Now;
                //sisPessoa.TipoPessoaId = 2;
                //sisPessoa.FisicaJuridica = "F";

                var endereco = new Endereco();
                sisPessoa.Enderecos = new List<Endereco>();

                CarregarEndereco(input, endereco);
                sisPessoa.Enderecos.Add(endereco);

            }




            sisPessoa.NomeCompleto = input.NomeCompleto;
            sisPessoa.Cpf = input.Cpf;
            //sisPessoa.Escolaridade = Convert.ToInt32(input.Escolaridade);
            sisPessoa.NacionalidadeId = input.NacionalidadeId;
            sisPessoa.Nascimento = input.Nascimento;
            sisPessoa.NomeMae = input.NomeMae;
            sisPessoa.NomePai = input.NomePai;
            sisPessoa.ProfissaoId = input.ProfissaoId;
            sisPessoa.Rg = input.Rg;
            sisPessoa.EmissaoRg = input.Emissao;
            sisPessoa.Emissor = input.Emissor;
            //sisPessoa.CorPele = Convert.ToInt32(input.CorPele);
            //sisPessoa.EstadoCivil = Convert.ToInt32(input.EstadoCivil);
            sisPessoa.SexoId = input.SexoId;

            //sisPessoa.TipoTelefone1 = input.TipoTelefone1;
            sisPessoa.Telefone1 = input.Telefone1;

            //sisPessoa.TipoTelefone2 = input.TipoTelefone2;
            sisPessoa.Telefone2 = input.Telefone2;

            //sisPessoa.TipoTelefone3 = input.TipoTelefone3;
            sisPessoa.Telefone3 = input.Telefone3;

            //sisPessoa.TipoTelefone4 = input.TipoTelefone4;
            sisPessoa.Telefone4 = input.Telefone4;


            sisPessoa.Email = input.Email;


            // sisPessoa.Nascimento = DateTime.Now;
            sisPessoa.TipoPessoaId = 1;
            sisPessoa.FisicaJuridica = "F";
            sisPessoa.IsDebito = true;


            var medicosEspecialidade = new List<EspecialidadeMedicoDto>();

            if (!input.MedicoEspecialidadeList.IsNullOrWhiteSpace())
            {
                medicosEspecialidade = JsonConvert.DeserializeObject<List<EspecialidadeMedicoDto>>(input.MedicoEspecialidadeList);
            }

            if(medico.MedicoEspecialidades==null)
            {
                medico.MedicoEspecialidades = new List<MedicoEspecialidade>();
            }

            medico.MedicoEspecialidades.RemoveAll(r => !medicosEspecialidade.Any(a => a.Id == r.Id));


            //Incluir Especialidade.
            foreach (var medicoEspecialidade in medicosEspecialidade.Where(w => w.Id == 0 || w.Id == 0))
            {
                medico.MedicoEspecialidades.Add(new MedicoEspecialidade
                {
                    EspecialidadeId = medicoEspecialidade.IdEspecialidade
                });
            }



            return medico;
        }

        void CarregarEndereco(MedicoDto input, Endereco endereco)
        {
            endereco.TipoLogradouroId = input.TipoLogradouroId;
            endereco.Bairro = input.Bairro;
            endereco.Cep = input.Cep;
            endereco.CidadeId = input.CidadeId;
            endereco.Complemento = input.Complemento;
            endereco.EstadoId = input.EstadoId;
            endereco.Logradouro = input.Logradouro;
            endereco.Numero = input.Numero;
            endereco.PaisId = input.PaisId;
        }

        public async Task<MedicoDto> ObterPorCPF(string cpf)
        {
            try
            {
                var result = await _medicoRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.Conselho)
                    .Include(m => m.Profissao)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .Where(m => m.SisPessoa.Cpf == cpf)
                    .FirstOrDefaultAsync();
                //.GetAllIncluding(
                //    m => m.Cidade,
                //    //m => m.Cidade.Estado,
                //    m => m.Estado,
                //    m => m.Pais,
                //    m => m.Profissao,
                //    m => m.Naturalidade,
                //    m => m.MedicoEspecialidades
                //)
                //.Where(m => m.Id == id)
                //.FirstOrDefaultAsync();

                var medico = result
                    //.FirstOrDefault()
                    .MapTo<MedicoDto>();

                return medico;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
