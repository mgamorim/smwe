﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Conselhos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto
{
    [AutoMap(typeof(Medico))]
    public class MedicoDto : PessoaFisicaDto
    {
        public long NumeroConselho { get; set; }

        public byte[] AssinaturaDigital { get; set; }

        public string AssinaturaDigitalMimeType { get; set; }

        public string Cns { get; set; }

        public bool IsAgendaConsulta { get; set; }

        public bool IsAgendaCirurgia { get; set; }

        public bool IsAtendimentoConsulta { get; set; }

        public bool IsAtendimentoCirurgia { get; set; }

        public bool IsAtendimentoInternacao { get; set; }

        public bool IsEspecialista { get; set; }

        public bool IsExame { get; set; }

        public string CorAgendamentoConsulta { get; set; }

        public long? ConselhoId { get; set; }
        public virtual ConselhoDto Conselho { get; set; }

        public string CodigoCredenciamentoConvenio { get; set; }

        public bool IsAtivo { get; set; }

        public bool IsCorpoClinico { get; set; }

        public string Apelido { get; set; }
        public long? IdGridMedicoEspecialidade { get; set; }

        //public virtual ICollection<MedicoEspecialidadeDto> MedicoEspecialidades { get; set; }

        public string MedicoEspecialidadeList { get; set; }

        public long? SisPessoaId { get; set; }
        public SisPessoaDto SisPessoa { get; set; }



        #region Mapeamento

        public static Medico Mapear(MedicoDto medicoDto)
        {
            Medico medico = new Medico();


            medico.NumeroConselho = medicoDto.NumeroConselho;
            medico.AssinaturaDigital = medicoDto.AssinaturaDigital;
            medico.AssinaturaDigitalMimeType = medicoDto.AssinaturaDigitalMimeType;
            medico.Cns = medicoDto.Cns;
            medico.IsAgendaConsulta = medicoDto.IsAgendaConsulta;
            medico.IsAgendaCirurgia = medicoDto.IsAgendaCirurgia;
            medico.IsAtendimentoConsulta = medicoDto.IsAtendimentoConsulta;
            medico.IsAtendimentoCirurgia = medicoDto.IsAtendimentoCirurgia;
            medico.IsAtendimentoInternacao = medicoDto.IsAtendimentoInternacao;
            medico.IsEspecialista = medicoDto.IsEspecialista;
            medico.IsExame = medicoDto.IsExame;
            medico.CorAgendamentoConsulta = medicoDto.CorAgendamentoConsulta;
            medico.ConselhoId = medicoDto.ConselhoId;
            medico.CodigoCredenciamentoConvenio = medicoDto.CodigoCredenciamentoConvenio;
            medico.IsAtivo = medicoDto.IsAtivo;
            medico.IsCorpoClinico = medicoDto.IsCorpoClinico;
            medico.Apelido = medicoDto.Apelido;
            medico.SisPessoaId = medicoDto.SisPessoaId;
            medico.Id = medicoDto.Id;

            if (medicoDto.SisPessoa != null)
            {
                medico.SisPessoa = SisPessoaDto.Mapear(medicoDto.SisPessoa);
            }


            if (medicoDto.Conselho != null)
            {
                medico.Conselho = ConselhoDto.Mapear(medicoDto.Conselho);
            }

            //public virtual ICollection<MedicoEspecialidadeDto> MedicoEspecialidades { get; set; }

            return medico;
        }

        public static MedicoDto Mapear(Medico medico)
        {
            MedicoDto medicoDto = new MedicoDto();

            medicoDto.NumeroConselho = medico.NumeroConselho;
            medicoDto.AssinaturaDigital = medico.AssinaturaDigital;
            medicoDto.AssinaturaDigitalMimeType = medico.AssinaturaDigitalMimeType;
            medicoDto.Cns = medico.Cns;
            medicoDto.IsAgendaConsulta = medico.IsAgendaConsulta;
            medicoDto.IsAgendaCirurgia = medico.IsAgendaCirurgia;
            medicoDto.IsAtendimentoConsulta = medico.IsAtendimentoConsulta;
            medicoDto.IsAtendimentoCirurgia = medico.IsAtendimentoCirurgia;
            medicoDto.IsAtendimentoInternacao = medico.IsAtendimentoInternacao;
            medicoDto.IsEspecialista = medico.IsEspecialista;
            medicoDto.IsExame = medico.IsExame;
            medicoDto.CorAgendamentoConsulta = medico.CorAgendamentoConsulta;
            medicoDto.ConselhoId = medico.ConselhoId;
            medicoDto.CodigoCredenciamentoConvenio = medico.CodigoCredenciamentoConvenio;
            medicoDto.IsAtivo = medico.IsAtivo;
            medicoDto.IsCorpoClinico = medico.IsCorpoClinico;
            medicoDto.Apelido = medico.Apelido;
            medicoDto.SisPessoaId = medico.SisPessoaId;
            medicoDto.Id = medico.Id;

            if (medico.SisPessoa != null)
            {
                medicoDto.SisPessoa = SisPessoaDto.Mapear(medico.SisPessoa);
            }


            if (medico.Conselho != null)
            {
                medicoDto.Conselho = ConselhoDto.Mapear(medico.Conselho);
            }

            //public virtual ICollection<MedicoEspecialidadeDto> MedicoEspecialidades { get; set; }

            return medicoDto;
        }

        #endregion



    }
}
