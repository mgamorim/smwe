﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposEntrada.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposEntrada.Exporting
{
    public interface IListarTipoEntradaExcelExporter
    {
        FileDto ExportToFile(List<TipoEntradaDto> unidadeDtos);
    }
}
