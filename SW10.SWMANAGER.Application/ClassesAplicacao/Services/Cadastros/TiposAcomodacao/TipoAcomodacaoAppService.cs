﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposAcomodacao;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao
{
    public class TipoAcomodacaoAppService : SWMANAGERAppServiceBase, ITipoAcomodacaoAppService
    {
        private readonly IRepository<TipoAcomodacao, long> _tipoAcomodacaoRepositorio;
        private readonly IListarTipoAcomodacaoExcelExporter _listarTipoAcomodacaoExcelExporter;
        private readonly IRepository<Leito, long> _leitoRepositorio;

        public TipoAcomodacaoAppService(IRepository<TipoAcomodacao, long> tipoAcomodacaoRepositorio,
            IListarTipoAcomodacaoExcelExporter listarTipoAcomodacaoExcelExporter,
            IRepository<Leito, long> leitoRepositorio)
        {
            _tipoAcomodacaoRepositorio = tipoAcomodacaoRepositorio;
            _listarTipoAcomodacaoExcelExporter = listarTipoAcomodacaoExcelExporter;
            _leitoRepositorio = leitoRepositorio;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_TiposAcomodacao_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_TiposAcomodacao_Edit)]
        public async Task CriarOuEditar(TipoAcomodacaoDto input)
        {
            try
            {
                var tipoAcomodacao = input.MapTo<TipoAcomodacao>();
                if (input.Id.Equals(0))
                {
                    await _tipoAcomodacaoRepositorio.InsertOrUpdateAsync(tipoAcomodacao);
                }
                else
                {
                    var tipoAcomodacaoEntity = _tipoAcomodacaoRepositorio.GetAll()
                                                                         .Where(w => w.Id == input.Id)
                                                                         .FirstOrDefault();

                    if (tipoAcomodacaoEntity != null)
                    {
                        tipoAcomodacaoEntity.Codigo = input.Codigo;
                        tipoAcomodacaoEntity.Descricao = input.Descricao;
                        tipoAcomodacaoEntity.TabelaItemTissId = input.TabelaItemTissId;

                        await _tipoAcomodacaoRepositorio.UpdateAsync(tipoAcomodacaoEntity);
                    }

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"));
            }

        }

        public async Task Excluir(TipoAcomodacaoDto input)
        {
            try
            {
                await _tipoAcomodacaoRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"));
            }

        }

        public async Task<PagedResultDto<TipoAcomodacaoDto>> Listar(ListarTiposAcomodacaoInput input)
        {
            var contarTiposAcomodacao = 0;
            List<TipoAcomodacao> tiposAcomodacao;
            List<TipoAcomodacaoDto> tiposAcomodacaoDtos = new List<TipoAcomodacaoDto>();
            try
            {
                var query = _tipoAcomodacaoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarTiposAcomodacao = await query
                    .CountAsync();

                tiposAcomodacao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                tiposAcomodacaoDtos = tiposAcomodacao
                    .MapTo<List<TipoAcomodacaoDto>>();

                return new PagedResultDto<TipoAcomodacaoDto>(
                    contarTiposAcomodacao,
                    tiposAcomodacaoDtos
                    );

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarTiposAcomodacaoInput input)
        {
            try
            {
                var query = await Listar(input);

                var tiposAcomodacaoDtos = query.Items;

                return _listarTipoAcomodacaoExcelExporter.ExportToFile(tiposAcomodacaoDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<TipoAcomodacaoDto> Obter(long id)
        {
            try
            {
                var result = await _tipoAcomodacaoRepositorio.GetAsync(id);
                var tipoAcomodacao = result.MapTo<TipoAcomodacaoDto>();
                return tipoAcomodacao;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }

        }


        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _tipoAcomodacaoRepositorio);
        }



        public async Task<PagedResultDto<TipoAcomodacaoDto>> ListarComLeito(ListarTiposAcomodacaoInput input)
        {
            var contarTiposAcomodacao = 0;
            List<TipoAcomodacao> tiposAcomodacao;
            List<TipoAcomodacaoDto> tiposAcomodacaoDtos = new List<TipoAcomodacaoDto>();
            try
            {
                var queryLeito = _leitoRepositorio.GetAll();


                var query = _tipoAcomodacaoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    )
                    .Where(m => queryLeito.Any(a => a.TipoAcomodacaoId == m.Id));

                contarTiposAcomodacao = await query
                    .CountAsync();

                tiposAcomodacao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                tiposAcomodacaoDtos = tiposAcomodacao
                    .MapTo<List<TipoAcomodacaoDto>>();

                return new PagedResultDto<TipoAcomodacaoDto>(
                    contarTiposAcomodacao,
                    tiposAcomodacaoDtos
                    );

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }



    }
}
