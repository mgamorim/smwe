﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Exporting
{
	public interface IListarTipoAcomodacaoExcelExporter
	{
		FileDto ExportToFile (List<TipoAcomodacaoDto> tipoAcomodacaoDtos);
	}
}
