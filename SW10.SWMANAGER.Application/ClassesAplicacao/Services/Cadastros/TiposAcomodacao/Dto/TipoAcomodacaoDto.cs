﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposAcomodacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TabelasDominio.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto
{
    [AutoMap(typeof(TipoAcomodacao))]
    public class TipoAcomodacaoDto : CamposPadraoCRUDDto
    {
        public long? TabelaItemTissId { get; set; }

        public virtual TabelaDominioDto TabelaDominio { get; set; }

        public static TipoAcomodacao Mapear(TipoAcomodacaoDto tipoAcomodacaoDto)
        {
            TipoAcomodacao tipoAcomodacao = new TipoAcomodacao();

            tipoAcomodacao.Id = tipoAcomodacaoDto.Id;
            tipoAcomodacao.Codigo = tipoAcomodacaoDto.Codigo;
            tipoAcomodacao.Descricao = tipoAcomodacaoDto.Descricao;

            return tipoAcomodacao;
        }

        public static TipoAcomodacaoDto Mapear(TipoAcomodacao tipoAcomodacao)
        {
            TipoAcomodacaoDto tipoAcomodacaoDto = new TipoAcomodacaoDto();

            tipoAcomodacaoDto.Id = tipoAcomodacao.Id;
            tipoAcomodacaoDto.Codigo = tipoAcomodacao.Codigo;
            tipoAcomodacaoDto.Descricao = tipoAcomodacao.Descricao;

            return tipoAcomodacaoDto;
        }

    }
}
