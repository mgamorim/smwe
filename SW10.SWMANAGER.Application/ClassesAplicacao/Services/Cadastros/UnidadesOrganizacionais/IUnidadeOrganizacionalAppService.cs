﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais
{
	public interface IUnidadeOrganizacionalAppService : IApplicationService
    {
        Task<PagedResultDto<UnidadeOrganizacionalDto>> Listar(ListarUnidadesOrganizacionaisInput input);

        Task<ListResultDto<UnidadeOrganizacionalDto>> ListarTodos();

        Task<ListResultDto<UnidadeOrganizacionalDto>> ListarParaAmbulatorioEmergencia();

        Task<ListResultDto<UnidadeOrganizacionalDto>> ListarParaInternacao();

        Task<ResultDropdownList> ListarDropdownLocalUtilizacao (DropdownInput dropdownInput);

        Task CriarOuEditar(UnidadeOrganizacionalDto input);

        Task Excluir (long id);

        Task<UnidadeOrganizacionalDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarUnidadesOrganizacionaisInput input);

        // Atendimento
        Task<string> ChecarControlaAlta(long id);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownUnidadeAtual(DropdownInput dropdownInput);

        Task<UnidadeOrganizacionalDto> ObterPorId(long id);
    }
}
