﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Exporting
{
    public interface IListarUnidadesOrganizacionaisExcelExporter
    {
        FileDto ExportToFile(List<UnidadeOrganizacionalDto> unidadesDtos);
    }
}
