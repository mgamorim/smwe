﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.UI;
using SW10.SWMANAGER.Authorization;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Exporting;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.Organizations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;


namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais
{
    public class UnidadeOrganizacionalAppService : SWMANAGERAppServiceBase, IUnidadeOrganizacionalAppService
    {
        #region Dependencias

        private readonly IRepository<UnidadeOrganizacional, long> _unidadeOrganizacionalRepositorio;
        private readonly IRepository<Leito, long> _leitoRepositorio;
        private readonly IListarUnidadesOrganizacionaisExcelExporter _listarUnidadesOrganizacionaisExcelExporter;
        private readonly IOrganizationUnitAppService _organizationUnitAppService;
        
        public UnidadeOrganizacionalAppService(
            IRepository<UnidadeOrganizacional, long> unidadeOrganizacionalRepositorio,
            IRepository<Leito, long> leitoRepositorio,
            IOrganizationUnitAppService organizationUnitAppService,
            IListarUnidadesOrganizacionaisExcelExporter listarUnidadesOrganizacionaisExcelExporter
            )
        {
            _unidadeOrganizacionalRepositorio = unidadeOrganizacionalRepositorio;
            _leitoRepositorio = leitoRepositorio;
            _organizationUnitAppService = organizationUnitAppService;
            _listarUnidadesOrganizacionaisExcelExporter = listarUnidadesOrganizacionaisExcelExporter;
        }

        #endregion dependencias.

        public async Task<PagedResultDto<UnidadeOrganizacionalDto>> Listar(ListarUnidadesOrganizacionaisInput input)
        {
            var contarUnidadesOrganizacionais = 0;
            List<UnidadeOrganizacional> unidades;
            List<UnidadeOrganizacionalDto> unidadesOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                var query = _unidadeOrganizacionalRepositorio
                    .GetAll()
                    .Include(m => m.OrganizationUnit)
                    .Include(m => m.UnidadeInternacaoTipo)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.Contains(input.Filtro)
                    );

                contarUnidadesOrganizacionais = await query
                    .CountAsync();

                unidades = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                unidadesOrganizacionaisDtos = unidades
                    .MapTo<List<UnidadeOrganizacionalDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<UnidadeOrganizacionalDto>(
                contarUnidadesOrganizacionais,
                unidadesOrganizacionaisDtos
                );
        }

        public async Task<ListResultDto<UnidadeOrganizacionalDto>> ListarTodos()
        {
            List<UnidadeOrganizacional> unidadesOrganizacionais;
            List<UnidadeOrganizacionalDto> unidadesOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                unidadesOrganizacionais = await _unidadeOrganizacionalRepositorio
                  .GetAll()
                    .Include(m => m.OrganizationUnit)
                    .Include(m => m.UnidadeInternacaoTipo)
                  .AsNoTracking()
                  .ToListAsync();

                foreach (var unidade in unidadesOrganizacionais)
                {
                    var ous = await _organizationUnitAppService.GetOrganizationUnits();
                    var ou = ous.Items.FirstOrDefault(o => o.Id == unidade.OrganizationUnitId);

                    if (ou != null)
                    {
                        unidade.OrganizationUnit = ou.MapTo<OrganizationUnit>();
                    }
                }

                unidadesOrganizacionaisDtos = unidadesOrganizacionais
                    .MapTo<List<UnidadeOrganizacionalDto>>();

                return new ListResultDto<UnidadeOrganizacionalDto> { Items = unidadesOrganizacionaisDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<UnidadeOrganizacionalDto>> ListarParaAmbulatorioEmergencia()
        {
            List<UnidadeOrganizacional> unidadesOrganizacionais;
            List<UnidadeOrganizacionalDto> unidadesOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                unidadesOrganizacionais = await _unidadeOrganizacionalRepositorio
                 .GetAll()
                 .Include(m => m.OrganizationUnit)
                 //.Include(m => m.UnidadeInternacaoTipo)
                 .Where(u => u.ControlaAlta && u.IsAmbulatorioEmergencia)
                 .AsNoTracking()
                 .ToListAsync();

                //unidadesOrganizacionais = await _unidadeOrganizacionalRepositorio
                //  .GetAll()
                //  .Include(m => m.OrganizationUnit)
                // //.Include(m => m.UnidadeInternacaoTipo)
                //  .Where(u => u.ControlaAlta && u.IsAmbulatorioEmergencia)
                // //.Where(u => u.IsAmbulatorioEmergencia)
                //  .AsNoTracking()
                //  .ToListAsync();

                //foreach (var unidade in unidadesOrganizacionais)
                //{
                //    var ous = await _organizationUnitAppService.GetOrganizationUnits();
                //    var ou = ous.Items.FirstOrDefault(o => o.Id == unidade.OrganizationUnitId);

                //    if (ou != null)
                //    {
                //        unidade.OrganizationUnit = ou.MapTo<OrganizationUnit>();
                //    }
                //}

                unidadesOrganizacionaisDtos = unidadesOrganizacionais
                    .MapTo<List<UnidadeOrganizacionalDto>>();

                return new ListResultDto<UnidadeOrganizacionalDto> { Items = unidadesOrganizacionaisDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<UnidadeOrganizacionalDto>> ListarParaInternacao()
        {
            List<UnidadeOrganizacional> unidadesOrganizacionais;
            List<UnidadeOrganizacionalDto> unidadesOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                   unidadesOrganizacionais = await _unidadeOrganizacionalRepositorio
                  .GetAll()
                  .Include(m => m.OrganizationUnit)
                //.Include(m => m.UnidadeInternacaoTipo)
                  .Where(u => u.ControlaAlta && u.IsInternacao)
                  .AsNoTracking()
                  .ToListAsync();

                //foreach (var unidade in unidadesOrganizacionais)
                //{
                //    var ous = await _organizationUnitAppService.GetOrganizationUnits();
                //    var ou = ous.Items.FirstOrDefault(o => o.Id == unidade.OrganizationUnitId);

                //    if (ou != null)
                //    {
                //        unidade.OrganizationUnit = ou.MapTo<UnidadeOrganizacional>();
                //    }
                //}

                unidadesOrganizacionaisDtos = unidadesOrganizacionais
                    .MapTo<List<UnidadeOrganizacionalDto>>();

                return new ListResultDto<UnidadeOrganizacionalDto> { Items = unidadesOrganizacionaisDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_UnidadeOrganizacional_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_UnidadeOrganizacional_Edit)]
        public async Task CriarOuEditar(UnidadeOrganizacionalDto input)
        {
            try
            {
                var unidadeOrganizacional = input.MapTo<UnidadeOrganizacional>();
                if (input.Id.Equals(0))
                {
                    await _unidadeOrganizacionalRepositorio.InsertOrUpdateAsync(unidadeOrganizacional);
                }
                else
                {
                    await _unidadeOrganizacionalRepositorio.UpdateAsync(unidadeOrganizacional);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosGlobais_UnidadeOrganizacional_Delete)]
        public async Task Excluir(long id)
        {
            try
            {
                var unidadeOrganizacional = _unidadeOrganizacionalRepositorio.GetAll().FirstOrDefault(u => u.OrganizationUnitId == id);

                if (unidadeOrganizacional == null)
                    return;

                long unidadeOrganizacionalId = unidadeOrganizacional.MapTo<UnidadeOrganizacionalDto>().Id;

                await _unidadeOrganizacionalRepositorio.DeleteAsync(unidadeOrganizacionalId);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<UnidadeOrganizacionalDto> Obter(long id)
        {
            try
            {
                var result = await _unidadeOrganizacionalRepositorio
                    .GetAll()
                    .Include(m => m.OrganizationUnit)
                    .Include(m => m.UnidadeInternacaoTipo)
                    .Include(m => m.Estoque)
                    .FirstOrDefaultAsync(u => u.OrganizationUnitId == id);
                var unidadeOrganizacional = UnidadeOrganizacionalDto.MapearFromCore(result);
                return unidadeOrganizacional;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<UnidadeOrganizacionalDto> ObterPorId(long id)
        {
            try
            {
                var result = await _unidadeOrganizacionalRepositorio
                    .GetAll()
                    .Where(w=> w.Id == id)
                    .Include(m => m.OrganizationUnit)
                    .Include(m => m.UnidadeInternacaoTipo)
                    .FirstOrDefaultAsync();
                var unidadeOrganizacional = result.MapTo<UnidadeOrganizacionalDto>();
                return unidadeOrganizacional;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarUnidadesOrganizacionaisInput input)
        {
            try
            {
                var query = await Listar(input);

                var unidadesOrganizacionaisDtos = query.Items;

                return _listarUnidadesOrganizacionaisExcelExporter.ExportToFile(unidadesOrganizacionaisDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<string> ChecarControlaAlta(long id)
        {
            var uo = await Obter(id);

            if (uo.ControlaAlta)
            {
                return true.ToString().ToLower();
            }
            else
            {
                return false.ToString().ToLower();
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {

            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<UnidadeOrganizacionalDto> unidadeOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                //get com filtro
                var query = from p in _unidadeOrganizacionalRepositorio.GetAll()
                         .WhereIf(dropdownInput.filtro == "inter", m => m.IsInternacao == true)
                         .WhereIf(dropdownInput.filtro == "ambEmr", m => m.IsAmbulatorioEmergencia == true)
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownUnidadeAtual(DropdownInput dropdownInput)
        {

            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<UnidadeOrganizacionalDto> unidadeOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                var leitos = _leitoRepositorio.GetAll();
                var uoIds = leitos.Select(x => x.UnidadeOrganizacionalId).ToList();
                
                //get com filtro
                var query = from p in _unidadeOrganizacionalRepositorio.GetAll()
                            .Where(u => uoIds.Contains(u.Id))
                            .WhereIf(dropdownInput.filtro == "inter", m => m.IsInternacao == true)
                            .WhereIf(dropdownInput.filtro == "ambEmr", m => m.IsAmbulatorioEmergencia == true)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownLocalUtilizacao (DropdownInput dropdownInput)
        {

            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<UnidadeOrganizacionalDto> unidadeOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                var query = from p in _unidadeOrganizacionalRepositorio.GetAll()
                            .Where(uo=>uo.IsLocalUtilizacao)
                   //      .WhereIf(dropdownInput.filtro == "inter", m => m.IsInternacao == true)
                     //    .WhereIf(dropdownInput.filtro == "ambEmr", m => m.IsAmbulatorioEmergencia == true)
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex.InnerException);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownCentroCusto (DropdownInput dropdownInput)
        {

            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<UnidadeOrganizacionalDto> unidadeOrganizacionaisDtos = new List<UnidadeOrganizacionalDto>();
            try
            {
                var query = from p in _unidadeOrganizacionalRepositorio.GetAll()
                         //   .Where(uo => uo.IsCentroCusto)
                        //      .WhereIf(dropdownInput.filtro == "inter", m => m.IsInternacao == true)
                        //    .WhereIf(dropdownInput.filtro == "ambEmr", m => m.IsAmbulatorioEmergencia == true)
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        .Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        .Replace("ã", "a").Replace("õ", "o")
                        .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        .Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        .Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        .Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex.InnerException);
            }
        }
    }
}
