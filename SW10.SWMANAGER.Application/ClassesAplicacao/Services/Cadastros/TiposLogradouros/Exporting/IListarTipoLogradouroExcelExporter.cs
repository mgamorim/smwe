﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Exporting
{
    public interface IListarTipoLogradouroExcelExporter
    {
        FileDto ExportToFile(List<TipoLogradouroDto> tipoLogradouroDtos);
    }
}
