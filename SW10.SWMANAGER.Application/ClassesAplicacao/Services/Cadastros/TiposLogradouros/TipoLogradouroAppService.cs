﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
//using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouro.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposLogradouro;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Exporting;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros
{
    public class TipoLogradouroAppService : SWMANAGERAppServiceBase, ITipoLogradouroAppService
    {
        private readonly IRepository<TipoLogradouro, long> _tipoLogradouroRepositorio;
        private readonly IListarTipoLogradouroExcelExporter _listarTiposLogradouroExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TipoLogradouroAppService(
            IRepository<TipoLogradouro, long> tipoLogradouroRepositorio,
            IUnitOfWorkManager unitOfWorkManager,
            IListarTipoLogradouroExcelExporter listarTipoLogradouroExcelExporter
            )
        {
            _tipoLogradouroRepositorio = tipoLogradouroRepositorio;
            _listarTiposLogradouroExcelExporter = listarTipoLogradouroExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<CriarOuEditarTipoLogradouroDto> CriarOuEditar(CriarOuEditarTipoLogradouroDto input)
        {
            try
            {
                var tipoLogradouro = input.MapTo<TipoLogradouro>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        var result = await _tipoLogradouroRepositorio.InsertAndGetIdAsync(tipoLogradouro);
                        //var tipoLogradouroDto = result.MapTo<CriarOuEditarTipoLogradouroDto>();
                        input.Id = result;
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        var result = await _tipoLogradouroRepositorio.UpdateAsync(tipoLogradouro);
                        var tipoLogradouroDto = result.MapTo<CriarOuEditarTipoLogradouroDto>();
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return tipoLogradouroDto;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }
        [UnitOfWork]
        public async Task Excluir(CriarOuEditarTipoLogradouroDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tipoLogradouroRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<TipoLogradouroDto>> Listar(ListarTiposLogradouroInput input)
        {
            var contarTiposLogradouro = 0;
            List<TipoLogradouro> tiposLogradouro;
            List<TipoLogradouroDto> tiposLogradouroDtos = new List<TipoLogradouroDto>();
            try
            {
                var query = _tipoLogradouroRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Abreviacao.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarTiposLogradouro = await query
                    .CountAsync();

                tiposLogradouro = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                tiposLogradouroDtos = tiposLogradouro
                    .MapTo<List<TipoLogradouroDto>>();

                return new PagedResultDto<TipoLogradouroDto>(
                    contarTiposLogradouro,
                    tiposLogradouroDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<TipoLogradouroDto>> ListarTodos()
        {
            try
            {
                var tiposLogradouro = await _tipoLogradouroRepositorio
                   .GetAll()
                   .AsNoTracking()
                   .ToListAsync();

                var tiposLograouroDto = tiposLogradouro
                    .MapTo<List<TipoLogradouroDto>>();

                return new ListResultDto<TipoLogradouroDto> { Items = tiposLograouroDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarTiposLogradouroInput input)
        {
            try
            {
                var result = await Listar(input);
                var TiposLogradouro = result.Items;
                return _listarTiposLogradouroExcelExporter.ExportToFile(TiposLogradouro.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<CriarOuEditarTipoLogradouroDto> Obter(long id)
        {
            try
            {
                var result = await _tipoLogradouroRepositorio.GetAsync(id);
                var tipoAtendimento = result.MapTo<CriarOuEditarTipoLogradouroDto>();

                return tipoAtendimento;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<CriarOuEditarTipoLogradouroDto> Obter(string logradouro)
        {
            try
            {
                var query = await _tipoLogradouroRepositorio.FirstOrDefaultAsync(m => m.Descricao.ToUpper().Equals(logradouro.ToUpper()));
                var result = query.MapTo<CriarOuEditarTipoLogradouroDto>();

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tipoLogradouroRepositorio);
        }

    }
}
