﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Exporting
{
    public interface IListarGrupoClasseExcelExporter
    {
        FileDto ExportToFile(List<GrupoClasseDto> grupoClasseDtos);
    }
}