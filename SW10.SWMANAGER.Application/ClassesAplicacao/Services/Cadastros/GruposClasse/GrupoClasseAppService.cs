﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse
{
    public class GrupoClasseAppService : SWMANAGERAppServiceBase, IGrupoClasseAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IRepository<GrupoClasse, long> _grupoClasseRepositorio;
        private readonly IListarGrupoClasseExcelExporter _listarGrupoClasseExcelExporter;

        public GrupoClasseAppService(IRepository<GrupoClasse, long> grupoClasseRepositorio,
            IListarGrupoClasseExcelExporter listarGrupoClasseExcelExporter,
            IUnitOfWorkManager UnitOfWorkManager,
            IUltimoIdAppService ultimoServicoAppService
            )
        {
            _unitOfWorkManager = UnitOfWorkManager;
            _grupoClasseRepositorio = grupoClasseRepositorio;
            _listarGrupoClasseExcelExporter = listarGrupoClasseExcelExporter;
            _ultimoIdAppService = ultimoServicoAppService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Classe_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Classe_Edit)]
        [UnitOfWork]
        public async Task<GrupoClasseDto> CriarOuEditar(GrupoClasseDto input)
        {
            try
            {
                var grupoClasse = input.MapTo<GrupoClasse>();

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        grupoClasse.Codigo = _ultimoIdAppService.ObterProximoCodigo("grupoClasse").Result;
                        grupoClasse.Id = await _grupoClasseRepositorio.InsertAndGetIdAsync(grupoClasse);

                        unitOfWork.Complete();

                        _unitOfWorkManager.Current.SaveChanges();

                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _grupoClasseRepositorio.UpdateAsync(grupoClasse);
                        unitOfWork.Complete();

                        _unitOfWorkManager.Current.SaveChanges();

                        unitOfWork.Dispose();
                    }
                }

                return grupoClasse.MapTo<GrupoClasseDto>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork]
        public async Task Excluir(GrupoClasseDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _grupoClasseRepositorio.DeleteAsync(input.Id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<GrupoClasseDto>> ListarTodos()
        {
            List<GrupoClasse> listCore;
            List<GrupoClasseDto> listDtos = new List<GrupoClasseDto>();
            try
            {
                listCore = await _grupoClasseRepositorio
                    .GetAll()
                    .ToListAsync();

                listDtos = listCore
                    .MapTo<List<GrupoClasseDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new ListResultDto<GrupoClasseDto> { Items = listDtos };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ListResultDto<GrupoClasseDto>> ListarPorGrupo(long id)
        {
            try
            {
                var idGrid = 0;
                var query = _grupoClasseRepositorio
                    .GetAll()
                    .Where(m => m.GrupoId == id)
                    //.Where(m=>m.Id)
                    ;

                var list = await query.ToListAsync();

                var listDto = list.MapTo<List<GrupoClasseDto>>();

                listDto.ForEach(m => m.IdGridClasse = ++idGrid);

                return new ListResultDto<GrupoClasseDto> { Items = listDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Recebe uma lista de objetos e devolve a mesma lista para popular um grid
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GrupoClasseDto>> ListarJson(List<GrupoClasseDto> list)
        {
            try
            {
                var count = 0;
                if (list == null)
                {
                    list = new List<GrupoClasseDto>();
                }
                for (int i = 0; i < list.Count(); i++)
                {
                    list[i].IdGridClasse = i + 1;
                }

                count = await Task.Run(() => list.Count());

                return new PagedResultDto<GrupoClasseDto>(
                      count,
                      list
                      );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GrupoClasseDto>> Listar(ListarGruposClasseInput input)
        {
            var contarGruposClasse = 0;
            List<GrupoClasse> gruposClasse;
            List<GrupoClasseDto> gruposClasseDtos = new List<GrupoClasseDto>();
            try
            {
                //string id; 
                //long.TryParse(input.Filtro.ToString(), out id);

                var query = _grupoClasseRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    || m.Id.ToString() == input.Filtro.ToString()
                    //m.Palavra.Contains(input.Filtro) ||
                    //m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGruposClasse = await query
                    .CountAsync();

                gruposClasse = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                gruposClasseDtos = gruposClasse
                    .MapTo<List<GrupoClasseDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<GrupoClasseDto>(
                contarGruposClasse,
                gruposClasseDtos
                );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<CriarOuEditarGrupoClasse> Obter(long id)
        {
            try
            {
                var result = await _grupoClasseRepositorio.GetAsync(id);
                var grupoClasse = result.MapTo<CriarOuEditarGrupoClasse>();
                return grupoClasse;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ListResultDto<GrupoClasseDto>> ObterPorGrupo(long id)
        {
            List<GrupoClasseDto> listDtos = new List<GrupoClasseDto>();
            try
            {
                var result = await _grupoClasseRepositorio
                    .GetAll()
                    .Where(w => w.GrupoId == id)
                    .FirstOrDefaultAsync();

                listDtos = result.MapTo<List<GrupoClasseDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

            return new ListResultDto<GrupoClasseDto> { Items = listDtos };

        }
    }
}
