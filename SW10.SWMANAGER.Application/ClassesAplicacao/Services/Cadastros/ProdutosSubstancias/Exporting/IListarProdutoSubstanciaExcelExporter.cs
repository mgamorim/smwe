﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosSubstancia.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosSubstancia.Exporting
{
    public interface IListarProdutoSubstanciaExcelExporter
    {
        FileDto ExportToFile(List<ProdutoSubstanciaDto> produtoSubstanciaDtos);
    }
}