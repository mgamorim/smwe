﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TabelasDominio.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TabelasDominio.Exporting
{
	public interface IListarTabelaDominioExcelExporter
	{
		FileDto ExportToFile(List<TabelaDominioDto> tabelaDominioDtos);
	}
}