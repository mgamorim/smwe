﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Cbos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Especialidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cbos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Especialidades.Dto
{
    [AutoMap(typeof(Especialidade))]
    public class EspecialidadeDto : CamposPadraoCRUDDto
    {
        public string Nome { get; set; }
        public string Cbo { get; set; }
        public string CboSus { get; set; }

        public bool IsAtivo { get; set; }

        public long CboId { get; set; }
        public CboDto SisCbo { get; set; }

        //public virtual ICollection<MedicoEspecialidadeDto> MedicoEspecialidades { get; set; }
        //public ICollection<MedicoEspecialidadeDto> MedicoEspecialidades { get; set; }

        public static EspecialidadeDto Mapear(Especialidade especialidade)
        {
            EspecialidadeDto especialidadeDto = new EspecialidadeDto();

            especialidadeDto.Id = especialidade.Id;
            especialidadeDto.Codigo = especialidade.Codigo;
            especialidadeDto.Descricao = especialidade.Descricao;
            especialidadeDto.Nome = especialidade.Nome;
            especialidadeDto.Cbo = especialidade.Cbo;
            especialidadeDto.CboSus = especialidade.CboSus;
            especialidadeDto.IsAtivo = especialidade.IsAtivo;
            especialidadeDto.CboId = especialidade.CboId ?? 0;

            if(especialidade.SisCbo!=null)
            {
                especialidadeDto.SisCbo = CboDto.Mapear(especialidade.SisCbo);
            }


            return especialidadeDto;
        }
    }
}