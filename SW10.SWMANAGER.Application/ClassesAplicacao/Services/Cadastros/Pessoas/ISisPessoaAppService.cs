﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas
{
    public interface ISisPessoaAppService : IApplicationService
    {
        Task<SisPessoaDto> ObterPorCPF(string cpf);
        Task<ResultDropdownList> ListarDropdownSisIsPagar(DropdownInput dropdownInput);
        Task<SisPessoaDto> ObterPorCnpj(string cnpj);
    }
}
