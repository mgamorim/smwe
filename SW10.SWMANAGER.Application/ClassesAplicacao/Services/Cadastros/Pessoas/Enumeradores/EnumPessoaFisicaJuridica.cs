﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Enumeradores
{
    public enum EnumPessoaFisicaJuridica
    {
        [Description("F")]
        Ficica ,
        [Description("J")]
        Juridica
    }
}
