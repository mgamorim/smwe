﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto
{
    [AutoMap(typeof(SisTipoPessoa))]
    public class SisTipoPessoaDto : CamposPadraoCRUDDto
    {
        public bool IsReceber { get; set; }
        public bool IsPagar { get; set; }
        public bool IsAtivo { get; set; }
    }
}
