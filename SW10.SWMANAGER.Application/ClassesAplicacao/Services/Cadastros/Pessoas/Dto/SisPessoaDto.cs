﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Enderecos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Nacionalidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Naturalidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Profissoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Religioes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposSanguineos.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto
{
    [AutoMap(typeof(SisPessoa))]
    public class SisPessoaDto : CamposPadraoCRUDDto
    {
        public override string Descricao
        {
            get
            {
                return this.FisicaJuridica == "F" ? this.NomeCompleto : this.NomeFantasia;
            }

            set
            {
                base.Descricao = value;
            }
        }

        public string NomeCompleto { get; set; }

        public long? TipoPessoaId { get; set; }

        public SisTipoPessoaDto TipoPessoa { get; set; }

        #region colunas Pessoa Física

        public string Rg { get; set; }

        public string Emissor { get; set; }

        public DateTime? EmissaoRg { get; set; }

        public DateTime? Nascimento { get; set; }

        public SexoDto Sexo { get; set; }
        public long? SexoId { get; set; }

        // public int? Sexo { get; set; }

        public CorPeleDto CorPele { get; set; }
        public long? CorPeleId { get; set; }

        public ProfissaoDto Profissao { get; set; }
        public long? ProfissaoId { get; set; }

        public EscolaridadeDto Escolaridade { get; set; }
        public long? EscolaridadeId { get; set; }

        public string Cpf { get; set; }

        public NaturalidadeDto Naturalidade { get; set; }
        public long? NaturalidadeId { get; set; }

        public long? NacionalidadeId { get; set; }

        public NacionalidadeDto Nacionalidade { get; set; }

        public EstadoCivilDto EstadoCivil { get; set; }
        public long? EstadoCivilId { get; set; }

        public string NomeMae { get; set; }

        public string NomePai { get; set; }

        public long? ReligiaoId { get; set; }

        public ReligiaoDto Religiao { get; set; }

        #endregion

        #region Colunas Pessoa Jurídica

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Cnpj { get; set; }
        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }

        #endregion

        #region Telefones



        //RELACIONAMENTOS TELEFONES 1-4
        public TipoTelefoneDto TipoTelefone1 { get; set; }
        public long? TipoTelefone1Id { get; set; }

        public TipoTelefoneDto TipoTelefone2 { get; set; }
        public long? TipoTelefone2Id { get; set; }

        public TipoTelefoneDto TipoTelefone3 { get; set; }
        public long? TipoTelefone3Id { get; set; }

        public TipoTelefoneDto TipoTelefone4 { get; set; }
        public long? TipoTelefone4Id { get; set; }


        public string Telefone1 { get; set; }

        //public TipoTelefoneDto TipoTelefone1 { get; set; }
        //public long? TipoTelefone1Id { get; set; }


        public int? DddTelefone1 { get; set; }

        public string Telefone2 { get; set; }

        //public TipoTelefoneDto TipoTelefone2 { get; set; }
        //public long? TipoTelefone2Id { get; set; }

        public int? DddTelefone2 { get; set; }

        public string Telefone3 { get; set; }

        //public TipoTelefoneDto TipoTelefone3 { get; set; }
        //public long? TipoTelefone3Id { get; set; }

        public int? DddTelefone3 { get; set; }

        public string Telefone4 { get; set; }

        //public TipoTelefoneDto TipoTelefone4 { get; set; }
        //public long? TipoTelefone4Id { get; set; }

        public int? DddTelefone4 { get; set; }

        #endregion

        #region Emails

        public string Email { get; set; }

        public string Email2 { get; set; }

        public string Email3 { get; set; }

        public string Email4 { get; set; }

        #endregion

        public byte[] Foto { get; set; }

        public string FotoMimeType { get; set; }

        public string FisicaJuridica { get; set; }

        public bool IsAtivo { get; set; }

        public List<EnderecoDto> Enderecos { get; set; }

        public string EnderecosJson { get; set; }


        public TipoSanguineoDto TipoSanguineo { get; set; }
        public long? TipoSanguineoId { get; set; }



        #region Mapeamento


        public static SisPessoa Mapear(SisPessoaDto sisPessoaDto)
        {
            SisPessoa sisPessoa = new SisPessoa();


            sisPessoa.Id = sisPessoaDto.Id;
            sisPessoa.Codigo = sisPessoaDto.Codigo;
            sisPessoa.Descricao = sisPessoaDto.Descricao;
            sisPessoa.NomeCompleto = sisPessoaDto.NomeCompleto;
            sisPessoa.TipoPessoaId = sisPessoaDto.TipoPessoaId;


            return sisPessoa;

        }

        public static SisPessoaDto Mapear(SisPessoa sisPessoa)
        {
            SisPessoaDto sisPessoaDto = new SisPessoaDto();


            sisPessoaDto.Id = sisPessoa.Id;
            sisPessoaDto.Codigo = sisPessoa.Codigo;
            sisPessoaDto.Descricao = sisPessoa.Descricao;
            sisPessoaDto.NomeCompleto = sisPessoa.NomeCompleto;
            sisPessoaDto.TipoPessoaId = sisPessoa.TipoPessoaId;
            sisPessoaDto.Nascimento = sisPessoa.Nascimento;

            return sisPessoaDto;

        }




        #endregion

    }
}
