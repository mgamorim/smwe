﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosTransferenciaLeito.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosTransferenciaLeito.Exporting
{
    public interface IListarMotivosTransferenciaLeitoExcelExporter
    {
        FileDto ExportToFile(List<MotivoTransferenciaLeitoDto> MotivoTransferenciaLeitoDtos);
    }
}
