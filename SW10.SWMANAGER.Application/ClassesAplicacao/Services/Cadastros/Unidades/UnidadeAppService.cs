﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using Abp.Domain.Uow;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades
{
    public class UnidadeAppService : SWMANAGERAppServiceBase, IUnidadeAppService
    {
        private readonly IRepository<Unidade, long> _unidadeRepositorio;
        private readonly IRepository<ProdutoUnidade, long> _produtoUnidadeRepositorio;
        private readonly IListarUnidadeExcelExporter _listarUnidadeExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public UnidadeAppService(
            IRepository<Unidade, long> unidadeRepositorio,
            IRepository<ProdutoUnidade, long> produtoUnidadeRepositorio,
            IUnitOfWorkManager unitOfWorkManager,
            IListarUnidadeExcelExporter listarUnidadeExcelExporter)
        {
            _unidadeRepositorio = unidadeRepositorio;
            _produtoUnidadeRepositorio = produtoUnidadeRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
            _listarUnidadeExcelExporter = listarUnidadeExcelExporter;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Unidade_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Unidade_Edit)]
        public async Task CriarOuEditar(UnidadeDto input)
        {
            try
            {
                var unidade = input.MapTo<Unidade>();
                if (input.Id.Equals(0))
                {
                    await _unidadeRepositorio.InsertOrUpdateAsync(unidade);
                }
                else
                {
                    await _unidadeRepositorio.UpdateAsync(unidade);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        /// <summary>
        /// Cria um novo produto e retorna um ProdutoDto com seu Id
        /// </summary>
        [UnitOfWork]
        public UnidadeDto CriarGetId(UnidadeDto input)
        {
            try
            {
                //input.Codigo = ObterProximoNumero(input);
                UnidadeDto unidadeDto;
                var unidade = input.MapTo<Unidade>();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.Id.Equals(0))
                    {
                        //Inclui o unidade e retorna unidadeDto com o Id
                        unidadeDto = new UnidadeDto { Id = AsyncHelper.RunSync(() => _unidadeRepositorio.InsertAndGetIdAsync(unidade)) };
                    }
                    else
                    {
                        unidadeDto = AsyncHelper.RunSync(() => _unidadeRepositorio.UpdateAsync(unidade)).MapTo<UnidadeDto>();
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                //unidadeDto.Codigo = input.Codigo;
                return unidadeDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }


        public async Task Excluir(UnidadeDto input)
        {
            try
            {
                await _unidadeRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<UnidadeDto>> Listar(ListarUnidadesInput input)
        {
            var contarUnidades = 0;
            List<Unidade> unidades;
            List<UnidadeDto> unidadesDtos = new List<UnidadeDto>();
            try
            {
                var query = _unidadeRepositorio
                    .GetAll()
                    .Where(u => u.UnidadeReferenciaId == null)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Id.ToString().Contains(input.Filtro) ||
                        m.Sigla.Contains(input.Filtro) ||
                        m.Fator.ToString().Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarUnidades = await query
                    .CountAsync();

                unidades = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                unidadesDtos = unidades.MapTo<List<UnidadeDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<UnidadeDto>(
                contarUnidades,
                unidadesDtos
                );
        }

        public async Task<FileDto> ListarParaExcel(ListarUnidadesInput input)
        {
            try
            {
                var query = await Listar(input);

                var unidadesDtos = query.Items;

                return _listarUnidadeExcelExporter.ExportToFile(unidadesDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<UnidadeDto> Obter(long id)
        {
            try
            {
                var result = await _unidadeRepositorio
                    .GetAll()
                    .Include(m => m.UnidadeReferencia)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var unidade = result.MapTo<UnidadeDto>();
                return unidade;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<UnidadeDto> ObterUnidadeDto(long id)
        {
            try
            {
                var result = await _unidadeRepositorio
                    .GetAll()
                    .Include(m => m.UnidadeReferencia)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var unidade = result.MapTo<UnidadeDto>();
                return unidade;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                ///TODO: NOVO PRODUTO
                //var query = await _unidadeRepositorio
                //    .GetAll()
                //    .WhereIf(!input.IsNullOrEmpty(), m =>
                //        m.Descricao.ToUpper().Contains(input.ToUpper())
                //        )
                //    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                //    .ToListAsync();

                //return new ListResultDto<GenericoIdNome> { Items = query };
                return new ListResultDto<GenericoIdNome> { Items = null };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoCompleteNaoUtilizado(string input, long produtoId)
        {
            try
            {
                ///TODO: NOVO PRODUTO
                //var query = await _unidadeRepositorio
                //    .GetAll()
                //    .WhereIf(!input.IsNullOrEmpty(), m =>
                //        m.Descricao.ToUpper().Contains(input.ToUpper()
                //         )
                //        )
                //    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                //    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = null };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna todas as Unidades disponiveis
        /// </summary>
        public async Task<ListResultDto<UnidadeDto>> ListarTodos()
        {
            List<UnidadeDto> unidadesDtos = new List<UnidadeDto>();
            try
            {
                var query = await _unidadeRepositorio
                    .GetAll()
                    .Include(m => m.UnidadeReferencia)
                    .ToListAsync();

                var unidadesDto = query.MapTo<List<UnidadeDto>>();

                return new ListResultDto<UnidadeDto> { Items = unidadesDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna todas as Unidades Referenciais disponiveis
        /// </summary>
        public async Task<ListResultDto<UnidadeDto>> ListarUnidadesReferenciais()
        {
            List<UnidadeDto> unidadesDtos = new List<UnidadeDto>();
            try
            {
                var query = await _unidadeRepositorio
                    .GetAll()
                    .Include(m => m.UnidadeReferencia)
                    .Where(a => a.UnidadeReferenciaId == null)
                    .ToListAsync();

                var unidadesDto = query.MapTo<List<UnidadeDto>>();

                return new ListResultDto<UnidadeDto> { Items = unidadesDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna unidades vinculadas a uma Unidade Referencial. 
        /// Pode trazer a propria unidade referencial juntamente com suas "filhas"
        /// </summary>
        public async Task<ListResultDto<UnidadeDto>> ListarPorReferencial(long? id, bool addPai = false)
        {
            List<UnidadeDto> unidadesDtos = new List<UnidadeDto>();
            try
            {
                if (id.HasValue)
                {
                    List<Unidade> query = null;

                    if (addPai)
                        query = await _unidadeRepositorio
                            .GetAll()
                            .Include(m => m.UnidadeReferencia)
                            .Where(a => a.Id == id || a.UnidadeReferenciaId == id)
                            .ToListAsync();
                    else
                        query = await _unidadeRepositorio
                            .GetAll()
                            .Include(m => m.UnidadeReferencia)
                            .Where(a => a.UnidadeReferenciaId == id)
                            .ToListAsync();

                    var unidadesDto = query.MapTo<List<UnidadeDto>>();

                    return new ListResultDto<UnidadeDto> { Items = unidadesDto };
                }
                else
                {
                    return new ListResultDto<UnidadeDto> { Items = new List<UnidadeDto>() };
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<UnidadeDto>> ListarPorReferencial(ListarUnidadesInput input)
        {
            var contarUnidades = 0;
            List<Unidade> unidades;
            List<UnidadeDto> unidadesDtos = new List<UnidadeDto>();
            try
            {
                var query = _unidadeRepositorio
                    .GetAll()
                    //.WhereIf(!input.Filtro.IsNullOrEmpty(), a => a.UnidadeReferenciaId.ToString().Contains(input.Filtro));
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), a => a.UnidadeReferenciaId.ToString() == input.Filtro);

                contarUnidades = await query.CountAsync();

                unidades = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                //var unidadesDto = query.MapTo<List<UnidadeDto>>();
                var unidadesDto = unidades.MapTo<List<UnidadeDto>>();

                //return new PagedResultDto<UnidadeDto> { Items = unidadesDto };

                return new PagedResultDto<UnidadeDto>(contarUnidades, unidadesDto);

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna todos os Ids de Unidade para uma Sigla especifica
        /// </summary>
        public async Task<List<long>> ListarIdsPorSigla(string sigla)
        {
            try
            {
                var query = await _unidadeRepositorio
                    .GetAll()
                    .Include(m => m.UnidadeReferencia)
                    .Where(a => a.Sigla == sigla)
                    //.Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .Select(m => m.Id)
                    .ToListAsync();

                var Ids = query.MapTo<List<long>>();

                return Ids;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna o Id de uma Unidade pela sua Sigla
        /// </summary>
        //public async Task<long> getIdUnidadelPorSigla(string sigla, bool? isReferencia = null)
        public async Task<long> GetIdUnidadelPorSigla(string sigla, bool? isReferencia = null, long? idRef=null)
        {
            try
            {
                List<long> query;

                if (isReferencia.HasValue && isReferencia == true)
                {
                    query = await _unidadeRepositorio
                        .GetAll()
                        .Include(m => m.UnidadeReferencia)
                        .Where(a => a.Sigla == sigla && a.UnidadeReferenciaId == null)
                        .Select(m => m.Id)
                        .ToListAsync();

                }
                else
                {
                    query = await _unidadeRepositorio
                        .GetAll()
                        .Include(m => m.UnidadeReferencia)
                        .Where(a => a.Sigla == sigla && ((a.UnidadeReferenciaId == idRef) || (a.UnidadeReferenciaId == null)))
                        .Select(m => m.Id)
                        .ToListAsync();
                };

                long Id;

                if (query.Count <= 0)
                {
                    Id = -1;
                }
                else
                {
                    Id = query.First();
                };

                return Id;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna a Sigla de uma Unidade pelo seu Id
        /// </summary>
        public async Task<string> GetSiglaUnidadePeloId(long id)
        {
            try
            {
                var query = await _unidadeRepositorio
                    .GetAll()
                    .Include(m => m.UnidadeReferencia)
                    .Where(a => a.Id == id)
                    .Select(m => m.Sigla)
                    .ToListAsync();


                string Sigla;

                if (query.Count <= 0)
                {
                    Sigla = "";
                }
                else
                {
                    Sigla = query.First();
                };

                return Sigla;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ObterQuantidadeReferencia(long unidadeId, decimal quantidade)
        {
            var unidade = _unidadeRepositorio
                .GetAll()
                .Where(m => m.Id == unidadeId)
                .FirstOrDefault();
            if (unidade != null)
            {
                return unidade.Fator * quantidade;
            }

            return 0;
        }

        public decimal ObterQuantidadePorFator(long unidadeId, decimal quantidade)
        {
            var unidade = _unidadeRepositorio
                .GetAll()
                .Where(m => m.Id == unidadeId)
                .FirstOrDefault();
            if (unidade != null)
            {
                return quantidade/ unidade.Fator;
            }

            return 0;
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from p in _unidadeRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m => m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                                          || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                                                                      )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Sigla.ToString(), " - ", p.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarUnidadePorProduto2Dropdown(DropdownInput dropdownInput)
        {
            long unidadeId;

            long.TryParse(dropdownInput.filtro, out unidadeId);

            var produtosUnidade = _produtoUnidadeRepositorio.GetAll();

            return await ListarDropdownLambda(dropdownInput
                                              , _unidadeRepositorio
                                              , m => produtosUnidade.Any(a => a.UnidadeId == m.Id && a.UnidadeId == unidadeId)
                                              , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                              , o => o.Descricao
                                              );

        }
    }
}
