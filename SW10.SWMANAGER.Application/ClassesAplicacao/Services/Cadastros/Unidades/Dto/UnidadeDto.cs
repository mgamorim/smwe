﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto
{
    [AutoMap(typeof(Unidade))]
    public class UnidadeDto : CamposPadraoCRUDDto
    {

        /// </summary>
        public string Sigla { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Fator de multiplicação (1cx = 10un = 1000gr -> 1 comprimido possui 100g cada)
        /// </summary>
        public Decimal Fator { get; set; }

        /// <summary>
        /// Normalmente a unidade de referência é a menor unidade de dispensação do estoque/farmácia
        /// </summary>
        
        public bool IsReferencia
        {
            get
            {
                return !UnidadeReferenciaId.HasValue;
            }
        }

        //public virtual Unidade UnidadeReferencia { get; set; }

        ///// <summary>
        ///// Quando for unidade de referência, esta é a lista de unidades vinculadas
        ///// </summary>
        //public virtual ICollection<Unidade> Unidades { get; set; }
        public Nullable<long> UnidadeReferenciaId { get; set; }


        public static UnidadeDto Mapear(Unidade unidade)
        {
            UnidadeDto unidadeDto = new UnidadeDto();

            unidadeDto.Id = unidade.Id;
            unidadeDto.Codigo = unidade.Codigo;
            unidadeDto.Descricao = unidade.Descricao;
            unidadeDto.Sigla = unidade.Sigla;
            unidadeDto.Fator = unidade.Fator;
            unidadeDto.UnidadeReferenciaId = unidade.UnidadeReferenciaId;

            return unidadeDto;

        }


        public static Unidade Mapear(UnidadeDto unidadeDto)
        {
            Unidade unidade = new Unidade();

            unidade.Id = unidadeDto.Id;
            unidade.Codigo = unidadeDto.Codigo;
            unidade.Descricao = unidadeDto.Descricao;
            unidade.Sigla = unidadeDto.Sigla;
            unidade.Fator = unidadeDto.Fator;
            unidade.UnidadeReferenciaId = unidadeDto.UnidadeReferenciaId;

            return unidade;

        }
    }
}
