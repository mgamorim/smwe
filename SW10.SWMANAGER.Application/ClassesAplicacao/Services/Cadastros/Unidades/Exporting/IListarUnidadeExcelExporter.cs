﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Exporting
{
    public interface IListarUnidadeExcelExporter
    {
        FileDto ExportToFile(List<UnidadeDto> unidadeDtos);
    }
}
