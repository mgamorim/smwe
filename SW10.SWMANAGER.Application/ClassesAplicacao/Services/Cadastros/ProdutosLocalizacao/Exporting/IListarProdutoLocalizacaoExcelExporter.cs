﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLocalizacao.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLocalizacao.Exporting
{
    public interface IListarProdutoLocalizacaoExcelExporter
    {
        FileDto ExportToFile(List<ProdutoLocalizacaoDto> ProdutoLocalizacaoDtos);
    }
}
