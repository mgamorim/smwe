﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposTabelaDominio.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposTabelaDominio.Exporting
{
	public interface IListarTipoTabelaDominioExcelExporter
	{
		FileDto ExportToFile(List<TipoTabelaDominioDto> tipoTabelaDominioDtos);
	}
}