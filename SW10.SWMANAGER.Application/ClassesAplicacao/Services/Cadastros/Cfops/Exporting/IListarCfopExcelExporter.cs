﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cfops.Exporting
{
    public interface IListarCfopExcelExporter
    {
        FileDto ExportToFile(List<CfopDto> unidadeDtos);
    }
}
