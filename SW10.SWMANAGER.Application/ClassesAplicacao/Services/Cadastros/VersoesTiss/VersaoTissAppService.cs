﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.VersoesTiss.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.VersoesTiss.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.VersoesTiss;
using Abp.UI;
using Abp.Domain.Uow;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.VersoesTiss
{
	public class VersaoTissAppService : SWMANAGERAppServiceBase, IVersaoTissAppService
	{
		private readonly IRepository<VersaoTiss,long> _versaoTissRepository;
		private readonly IListarVersoesTissExcelExporter _listarVersaoTissExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public VersaoTissAppService(IRepository<VersaoTiss,long> versaoTissRepository,
			IListarVersoesTissExcelExporter listarVersaoTissExcelExporter,
            IUnitOfWorkManager unitOfWorkManager)
		{
			_versaoTissRepository = versaoTissRepository;
			_listarVersaoTissExcelExporter = listarVersaoTissExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;

        }

		[AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_DominioTiss_VersoesTiss_Create,AppPermissions.Pages_Tenant_Cadastros_DominioTiss_VersoesTiss_Edit)]
		public async Task CriarOuEditar(CriarOuEditarVersaoTiss input)
		{
			try
			{
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var versaoTiss = input.MapTo<VersaoTiss>();
                    if (input.Id.Equals(0))
                    {
                        await _versaoTissRepository.InsertAsync(versaoTiss);
                    }
                    else
                    {
                        await _versaoTissRepository.UpdateAsync(versaoTiss);
                    }
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroSalvar"), ex);
			}

		}

		public async Task Excluir(CriarOuEditarVersaoTiss input)
		{
			try
			{
				await _versaoTissRepository.DeleteAsync(input.Id);
			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroExcluir"), ex);
			}

		}

		public async Task<ListResultDto<VersaoTissDto>> ListarTodos()
		{
			try
			{
				var query = await _versaoTissRepository
					.GetAllListAsync();

				var versoesTissDto = query.MapTo<List<VersaoTissDto>>();

				return new ListResultDto<VersaoTissDto> { Items = versoesTissDto };
			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroPesquisar"), ex);
			}
		}

		public async Task<PagedResultDto<VersaoTissDto>> Listar(ListarVersoesTissInput input)
		{
			var contarVersoesTiss = 0;
			List<VersaoTiss> versoesTiss;
			List<VersaoTissDto> versoesTissDtos = new List<VersaoTissDto>();
			try
			{
				var query = _versaoTissRepository
					.GetAll()
					.WhereIf(!input.Filtro.IsNullOrEmpty(),m =>
					   m.Codigo.ToUpper().Contains(input.Filtro.ToUpper())
					);

				contarVersoesTiss = await query
					.CountAsync();

				versoesTiss = await query
					.AsNoTracking()
					.OrderBy(input.Sorting)
					.PageBy(input)
					.ToListAsync();

				versoesTissDtos = versoesTiss
					.MapTo<List<VersaoTissDto>>();

			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroPesquisar"), ex);
			}
			return new PagedResultDto<VersaoTissDto>(
				contarVersoesTiss,
				versoesTissDtos
				);
		}

		public async Task<FileDto> ListarParaExcel(ListarVersoesTissInput input)
		{
			try
			{
				var query = await Listar(input);

				var versoesTissDtos = query.Items;

				return _listarVersaoTissExcelExporter.ExportToFile(versoesTissDtos.ToList());
			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroExportar"));
			}

		}

		public async Task<CriarOuEditarVersaoTiss> Obter(long id)
		{
			try
			{
				var result = await _versaoTissRepository.GetAsync(id);
				var versaoTiss = result.MapTo<CriarOuEditarVersaoTiss>();
				return versaoTiss;
			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroPesquisar"), ex);
			}
		}

		public async Task<ICollection<VersaoTissDto>> ListarPorTabelaDominio(long id)
		{
			List<VersaoTiss> versoesTiss;
			List<VersaoTissDto> versoesTissDtos = new List<VersaoTissDto>();
			try
			{
				var query = from m in _versaoTissRepository.GetAll()
							from e in m.TabelaDominioVersoesTiss
							where e.TabelaDominioId == id
							select m;

				versoesTiss = await query
					.AsNoTracking()
					.ToListAsync();

				versoesTissDtos = versoesTiss
					.MapTo<List<VersaoTissDto>>();

				return versoesTissDtos;
			}
			catch(Exception ex)
			{
				throw new UserFriendlyException(L("ErroPesquisar"), ex);
			}
			//return especialidadesDtos.MapTo<ListResultDto<VersaoTissDto>>();
		}
	}
}