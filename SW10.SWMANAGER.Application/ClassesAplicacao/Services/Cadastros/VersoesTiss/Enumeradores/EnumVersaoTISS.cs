﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.VersoesTiss.Enumeradores
{
    public enum EnumVersaoTISS
    {
        V03_03_00 = 1,
        V03_03_01 = 2,
        V03_03_02 = 3,
        V03_03_03 = 4,
    }
}
