﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosCancelamento.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosCancelamento.Exporting
{
    public interface IListarMotivosCancelamentoExcelExporter
    {
        FileDto ExportToFile(List<MotivoCancelamentoDto> MotivoCancelamentoDtos);
    }
}
