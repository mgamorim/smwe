﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEmpresa.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEmpresa.Exporting
{
    public interface IListarProdutoEmpresaExcelExporter
    {
        FileDto ExportToFile(List<ProdutoEmpresaDto> produtoEmpresaDtos);
    }
}
