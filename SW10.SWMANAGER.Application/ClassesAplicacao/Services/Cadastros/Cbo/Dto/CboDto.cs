﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Cbos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cbos.Dto
{
    [AutoMap(typeof(Cbo))]
    public class CboDto : CamposPadraoCRUDDto
    {
        public static CboDto Mapear(Cbo cbo)
        {
            CboDto cboDto = new CboDto();

            cboDto.Id = cbo.Id;
            cboDto.Codigo = cbo.Codigo;
            cboDto.Descricao = cbo.Descricao;

            return cboDto;
        }
    }
}
