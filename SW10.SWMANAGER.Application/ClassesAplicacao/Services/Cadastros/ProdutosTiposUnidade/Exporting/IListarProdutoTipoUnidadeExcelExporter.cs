﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosTiposUnidade.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosTiposUnidade.Exporting
{
    public interface IListarProdutoTipoUnidadeExcelExporter
    {
        FileDto ExportToFile(List<ProdutoTipoUnidadeDto> produtoTipoUnidadeDtos);
    }
}
