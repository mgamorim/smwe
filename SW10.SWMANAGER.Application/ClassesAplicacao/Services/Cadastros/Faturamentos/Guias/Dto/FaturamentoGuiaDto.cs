﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Faturamentos.Grupos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Paises;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Guias.Dto
{
    [AutoMap(typeof(FaturamentoGuia))]
    public class FaturamentoGuiaDto : CamposPadraoCRUDDto
    {
        public override string Codigo { get; set; }
        public override string Descricao { get; set; }
        public bool IsAmbulatorio { get; set; }
        public bool IsInternacao { get; set; }

        public bool IsEditMode ()
        {
            if (this.Id != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static FaturamentoGuiaDto Mapear(FaturamentoGuia faturamentoGuia)
        {
            FaturamentoGuiaDto faturamentoGuiaDto = new FaturamentoGuiaDto();

            faturamentoGuiaDto.Id = faturamentoGuia.Id;
            faturamentoGuiaDto.Codigo = faturamentoGuia.Codigo;
            faturamentoGuiaDto.Descricao = faturamentoGuia.Descricao;
            faturamentoGuiaDto.IsAmbulatorio = faturamentoGuia.IsAmbulatorio;
            faturamentoGuiaDto.IsInternacao = faturamentoGuia.IsInternacao;

            return faturamentoGuiaDto;
        }
    }
}
