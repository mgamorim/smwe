﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Autorizacoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Autorizacoes.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Autorizacoes
{
    public class FaturamentoAutorizacaoAppService : SWMANAGERAppServiceBase, IFaturamentoAutorizacaoAppService
    {
        private readonly IRepository<FaturamentoAutorizacao, long> _autorizacaoRepository;
        private readonly IRepository<FaturamentoAutorizacaoDetalhe, long> _autorizacaoDetalheRepository;

        public FaturamentoAutorizacaoAppService(
            IRepository<FaturamentoAutorizacao, long> autorizacaoRepository,
            IRepository<FaturamentoAutorizacaoDetalhe, long> autorizacaoDetalheRepository
            )
        {
            _autorizacaoRepository = autorizacaoRepository;
            _autorizacaoDetalheRepository = autorizacaoDetalheRepository;
        }

        public async Task CriarOuEditar(FaturamentoAutorizacaoDto input)
        {
            try
            {
                var autorizacao = input.MapTo<FaturamentoAutorizacao>();
                if (input.Id.Equals(0))
                {
                    await _autorizacaoRepository.InsertAsync(autorizacao);
                }
                else
                {
                    await _autorizacaoRepository.UpdateAsync(autorizacao);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task SalvarDetalhe(FaturamentoAutorizacaoDetalhe input)
        {
            try
            {
                if (input.Id.Equals(0))
                {
                    await _autorizacaoDetalheRepository.InsertAsync(input);
                }
                else
                {
                    await _autorizacaoDetalheRepository.UpdateAsync(input);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(FaturamentoAutorizacaoDto input)
        {
            try
            {
                await _autorizacaoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<FaturamentoAutorizacaoDto>> Listar(ListarAutorizacoesInput input)
        {
            var contarFaturamentoAutorizacaoes = 0;
            List<FaturamentoAutorizacao> autorizacaoes;
            List<FaturamentoAutorizacaoDto> autorizacaoesDtos = new List<FaturamentoAutorizacaoDto>();
            try
            {
                var query = _autorizacaoRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarFaturamentoAutorizacaoes = await query.CountAsync();

                autorizacaoes = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                autorizacaoesDtos = autorizacaoes.MapTo<List<FaturamentoAutorizacaoDto>>();

                return new PagedResultDto<FaturamentoAutorizacaoDto>(
                    contarFaturamentoAutorizacaoes,
                    autorizacaoesDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<FaturamentoAutorizacaoDetalhe>> ListarDetalhes(ListarAutorizacoesInput input)
        {
            var contarFaturamentoAutorizacaoes = 0;
            
            List<FaturamentoAutorizacaoDetalhe> autorizacaoesDtos = new List<FaturamentoAutorizacaoDetalhe>();
            try
            {
                var query = _autorizacaoDetalheRepository
                    .GetAll()
                    .Include(x=>x.Convenio)
                    .Include(x => x.Convenio.SisPessoa)
                    .Include(x => x.Plano)
                    .Include(x => x.Unidade)
                    .Include(x => x.Item)
                    .Include(x => x.Grupo)
                    .Include(x => x.SubGrupo)
                    .Where(x=>x.Id.ToString() == input.Filtro);

                contarFaturamentoAutorizacaoes = await query.CountAsync();

                autorizacaoesDtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<FaturamentoAutorizacaoDetalhe>(
                    contarFaturamentoAutorizacaoes,
                    autorizacaoesDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FaturamentoAutorizacaoDto> Obter(long id)
        {
            try
            {
                var query = await _autorizacaoRepository.GetAsync(id);
                var autorizacao = query.MapTo<FaturamentoAutorizacaoDto>();
                return autorizacao;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<FaturamentoAutorizacaoDto> pacientesDtos = new List<FaturamentoAutorizacaoDto>();
            try
            {
                //get com filtro
                var query = from p in _autorizacaoRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                        orderby p.Descricao ascending
                        select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
