﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Autorizacoes;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Faturamentos.Autorizacoes.Dto
{
    [AutoMap(typeof(FaturamentoAutorizacao))]
    public class FaturamentoAutorizacaoDto : CamposPadraoCRUDDto
    {
        public string Mensagem { get; set; }
        public DateTime DataInicial { get; set; }
        public DateTime? DataFinal { get; set; }
        public bool IsAmbulatorio { get; set; }
        public bool IsInternacao { get; set; }
        public bool IsAutorizacao { get; set; }
        public bool IsLiberacao { get; set; }
        public bool IsJustificativa { get; set; }
        public bool IsBloqueio { get; set; }

        // View
        public FaturamentoAutorizacaoDetalhe Detalhe { get; set; }
        public string Combo { get; set; }

        public bool IsEditMode()
        {
            if (this.Id != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
