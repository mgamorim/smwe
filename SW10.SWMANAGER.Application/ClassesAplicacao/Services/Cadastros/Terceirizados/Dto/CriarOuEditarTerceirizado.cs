﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Terceirizados.Dto
{
   public class CriarOuEditarTerceirizado : CamposPadraoCRUDDto
    {

        public string CodTerceirizado { get; set; }
    }
}
