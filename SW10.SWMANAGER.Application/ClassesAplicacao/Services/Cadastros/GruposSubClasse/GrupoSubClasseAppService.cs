﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse
{
    public class GrupoSubClasseAppService : SWMANAGERAppServiceBase, IGrupoSubClasseAppService
    {
        private readonly IRepository<GrupoSubClasse, long> _grupoSubClasseRepositorio;
        private readonly IListarGrupoSubClasseExcelExporter _listarGrupoSubClasseExcelExporter;

        public GrupoSubClasseAppService(IRepository<GrupoSubClasse, long> grupoSubClasseRepositorio,
            IListarGrupoSubClasseExcelExporter listarGrupoSubClasseExcelExporter)
        {
            _grupoSubClasseRepositorio = grupoSubClasseRepositorio;
            _listarGrupoSubClasseExcelExporter = listarGrupoSubClasseExcelExporter;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_SubClasse_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_SubClasse_Edit)]
        public async Task CriarOuEditar(CriarOuEditarGrupoSubClasse input)
        {
            try
            {
                var classe = input.MapTo<GrupoSubClasse>();
                if (input.Id.Equals(0))
                {
                    await _grupoSubClasseRepositorio.InsertOrUpdateAsync(classe);
                }
                else
                {
                    await _grupoSubClasseRepositorio.UpdateAsync(classe);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input, long idGrupo)
        {
            try
            {
                var query = await _grupoSubClasseRepositorio
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper()) && m.GrupoClasseId == idGrupo
                        )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarGrupoSubClasse input)
        {
            try
            {
                await _grupoSubClasseRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<ListResultDto<GrupoSubClasseDto>> ListarTodos()
        {
            List<GrupoSubClasse> listCore;
            List<GrupoSubClasseDto> listDtos = new List<GrupoSubClasseDto>();
            try
            {
                listCore = await _grupoSubClasseRepositorio
                    .GetAll()
                    .ToListAsync();

                listDtos = listCore
                    .MapTo<List<GrupoSubClasseDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new ListResultDto<GrupoSubClasseDto> { Items = listDtos };
        }

        public async Task<PagedResultDto<GrupoSubClasseDto>> Listar(ListarGruposSubClasseInput input)
        {
            var contarGruposSubClasse = 0;
            List<GrupoSubClasse> GruposSubClasse;
            List<GrupoSubClasseDto> GruposSubClasseDtos = new List<GrupoSubClasseDto>();
            try
            {
                var query = _grupoSubClasseRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    //m.Palavra.Contains(input.Filtro) ||
                    //m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGruposSubClasse = await query
                    .CountAsync();

                GruposSubClasse = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                GruposSubClasseDtos = GruposSubClasse
                    .MapTo<List<GrupoSubClasseDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

            return new PagedResultDto<GrupoSubClasseDto>(
                contarGruposSubClasse,
                GruposSubClasseDtos
                );
        }

        public async Task<FileDto> ListarParaExcel(ListarGruposSubClasseInput input)
        {
            try
            {
                var query = await Listar(input);

                var GruposSubClasseDtos = query.Items;

                return _listarGrupoSubClasseExcelExporter.ExportToFile(GruposSubClasseDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<CriarOuEditarGrupoSubClasse> Obter(long id)
        {
            try
            {
                var result = await _grupoSubClasseRepositorio.GetAsync(id);
                var classe = result.MapTo<CriarOuEditarGrupoSubClasse>();
                return classe;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ListResultDto<GrupoSubClasseDto>> ObterPorClasse(long id)
        {
            try
            {
                var result = await _grupoSubClasseRepositorio
                    .GetAll()
                    .Where(w => w.GrupoClasseId == id)
                    .FirstOrDefaultAsync();

                var classe = result.MapTo<List<GrupoSubClasseDto>>();

                return new ListResultDto<GrupoSubClasseDto> { Items = classe };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ListResultDto<GrupoSubClasseDto>> ListarPorClasse(long id)
        {
            List<GrupoSubClasse> listCore;
            List<GrupoSubClasseDto> listDtos = new List<GrupoSubClasseDto>();
            try
            {
                listCore = await _grupoSubClasseRepositorio
                    .GetAll()
                    .Where(w => w.GrupoClasseId == id)
                    .ToListAsync();

                listDtos = listCore
                    .MapTo<List<GrupoSubClasseDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new ListResultDto<GrupoSubClasseDto> { Items = listDtos };
        }

    }
}
