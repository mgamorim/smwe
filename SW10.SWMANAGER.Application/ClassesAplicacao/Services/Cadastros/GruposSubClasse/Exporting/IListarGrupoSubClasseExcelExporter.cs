﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposSubClasse.Exporting
{
    public interface IListarGrupoSubClasseExcelExporter
    {
        FileDto ExportToFile(List<GrupoSubClasseDto> GrupoSubclasseDtos);
    }
}