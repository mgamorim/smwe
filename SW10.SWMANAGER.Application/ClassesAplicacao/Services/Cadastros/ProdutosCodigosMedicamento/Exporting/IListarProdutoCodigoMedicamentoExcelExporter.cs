﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosCodigosMedicamento.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosCodigosMedicamento.Exporting
{
    public interface IListarProdutoCodigoMedicamentoExcelExporter
    {
        FileDto ExportToFile(List<ProdutoCodigoMedicamentoDto> produtoCodigoMedicamentoDtos);
    }
}
