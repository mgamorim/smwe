﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ConsultorTabelas.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ConsultorTabelas.Exporting
{
	public interface IListarConsultorTabelaExcelExporter
	{
		FileDto ExportToFile(List<ConsultorTabelaDto> consultorTabelaDtos);
	}
}