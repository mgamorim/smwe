﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosPortaria.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosPortaria.Exporting
{
    public interface IListarProdutoPortariaExcelExporter
    {
        FileDto ExportToFile(List<ProdutoPortariaDto> produtoPortariaDtos);
    }
}