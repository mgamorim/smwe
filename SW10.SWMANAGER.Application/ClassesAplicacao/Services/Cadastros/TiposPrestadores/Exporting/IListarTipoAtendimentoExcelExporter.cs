﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposPrestadores.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposPrestadores.Exporting
{
    public interface IListarTipoPrestadorExcelExporter
    {
        FileDto ExportToFile(List<TipoPrestadorDto> tipoAtendimentoDtos);
    }
}
