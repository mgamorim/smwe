﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Globais.HorasDia;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Globais.HorasDias.Dto
{
    [AutoMap(typeof(HoraDia))]
    public class HoraDiaDto : CamposPadraoCRUDDto
    {
    }
}
