﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Collections.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto;
using Ghostscript.NET.Rasterizer;
using System.IO;
using System.Drawing.Imaging;
using System.Web.Script.Serialization;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias.Dto;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias
{
    public class GuiaAppService : SWMANAGERAppServiceBase, IGuiaAppService
    {
        #region Injecao e Construtor

        private readonly IRepository<Guia, long> _guiaRepository;
        private readonly IGuiaCampoAppService _guiaCampoAppService;
        private readonly IRelacaoGuiaCampoAppService _relacaoGuiaCampoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public GuiaAppService(
            IRepository<Guia, long> guiaRepository,
            IGuiaCampoAppService guiaCampoAppService,
            IRelacaoGuiaCampoAppService relacaoGuiaCampoAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _guiaRepository = guiaRepository;
            _guiaCampoAppService = guiaCampoAppService;
            _relacaoGuiaCampoAppService = relacaoGuiaCampoAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        #endregion

        #region Metodos padrao

        public async Task<CriarOuEditarGuia> CriarOuEditar(CriarOuEditarGuia input)
        {
            try
            {
                var guia = input.MapTo<Guia>();

                // Gerar PNG
                int xDpi = 100;
                int yDpi = 100;
                int pagina = 1;

                using (GhostscriptRasterizer rasterizer = new GhostscriptRasterizer())
                {
                    Stream pdfStream = new MemoryStream(guia.ModeloPDF);

                    rasterizer.Open(pdfStream);
                    var imagemPDF = rasterizer.GetPage(xDpi, yDpi, pagina);

                    using (var memoryStream = new MemoryStream())
                    {
                        imagemPDF.Save(memoryStream, ImageFormat.Png);
                        var imagemPNG = memoryStream.ToArray();
                        guia.ModeloPNG = imagemPNG;
                        guia.ModeloPNGMimeType = "image/png";
                    }
                }

                if (input.Id.Equals(0))
                {
                    guia.Id = await _guiaRepository.InsertAndGetIdAsync(guia);
                }
                else
                {
                    await _guiaRepository.UpdateAsync(guia);
                }

                return guia.MapTo<CriarOuEditarGuia>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task<CriarOuEditarGuia> AtualizarCoordenadas(CriarOuEditarGuia guia, string camposAlterados)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer(new SimpleTypeResolver());
                    GuiaCampoDto[] camposAlteradosDto = jsonSerializer.Deserialize<GuiaCampoDto[]>(camposAlterados);
                    GuiaCampoDto[] camposAntigos = jsonSerializer.Deserialize<GuiaCampoDto[]>(guia.CamposJson);

                    //             GuiaCampoDto guiaCampoDto = new GuiaCampoDto();

                    foreach (var campo in camposAntigos)
                    {
                        var campoAlterado = camposAlteradosDto.Where(ca => ca.Descricao == campo.Descricao).FirstOrDefault();

                        if (campoAlterado != null)
                        {
                            campo.CoordenadaX = campoAlterado.CoordenadaX;
                            campo.CoordenadaY = campoAlterado.CoordenadaY;
                        }

                        foreach (var subCampo in campo.SubConjuntos)
                        {
                            var subCampoAlterado = camposAlteradosDto.Where(sca => sca.Descricao == subCampo.Descricao).FirstOrDefault();

                            if (subCampoAlterado != null)
                            {
                                subCampo.CoordenadaX = subCampoAlterado.CoordenadaX;
                                subCampo.CoordenadaY = subCampoAlterado.CoordenadaY;
                            }
                        }
                        //else
                        //{
                        //    foreach (var subCampo in campo.SubConjuntos)
                        //    {
                        //        var subCampoAlterado = camposAlteradosDto.Where(sca => sca.Descricao == subCampo.Descricao).FirstOrDefault();

                        //        if (subCampoAlterado != null)
                        //        {
                        //            subCampo.CoordenadaX = subCampoAlterado.CoordenadaX;
                        //            subCampo.CoordenadaY = subCampoAlterado.CoordenadaY;
                        //        }
                        //    }
                        //}
                    }

                    //for (int i = 0; i < camposAlteradosDto.Length; i++)
                    //{
                    //    GuiaCampoDto guiaCampoDto = camposAntigos.Where(c => c.Descricao == camposAlteradosDto[i].Descricao).FirstOrDefault();
                    //    if (guiaCampoDto != null)
                    //    {
                    //        guiaCampoDto.CoordenadaX = camposAlteradosDto[i].CoordenadaX;
                    //        guiaCampoDto.CoordenadaY = camposAlteradosDto[i].CoordenadaY;
                    //    }
                    //    else
                    //    {

                    //    }
                    //}

                    guia.CamposJson = jsonSerializer.Serialize(camposAntigos);
                    var guiaCore = guia.MapTo<Guia>();
                    await _guiaRepository.UpdateAsync(guiaCore);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                    return guia;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarGuia input)
        {
            try
            {
                await _guiaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<GuiaDto>> Listar(ListarGuiasInput input)
        {
            var contarGuias = 0;
            List<Guia> guias;
            List<GuiaDto> guiasDtos = new List<GuiaDto>();
            try
            {
                var query = _guiaRepository
                    .GetAll()
                    .Include(m => m.Originaria)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGuias = await query
                    .CountAsync();

                guias = await query
                    //.AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                guiasDtos = guias
                    .MapTo<List<GuiaDto>>();

                return new PagedResultDto<GuiaDto>(
                contarGuias,
                guiasDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<GuiaDto>> ListarTodos()
        {
            try
            {
                var guias = await _guiaRepository
                    .GetAll()
                    .Include(m => m.Originaria)
                    .AsNoTracking()
                    .ToListAsync();

                var guiasDtos = guias
                    .MapTo<List<GuiaDto>>();

                return new PagedResultDto<GuiaDto> { Items = guiasDtos };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<CriarOuEditarGuia> Obter(long id)
        {
            try
            {
                var query = _guiaRepository
                   .GetAll()
                   .Include(m => m.Originaria);

                Guia g = await query.FirstOrDefaultAsync(x => x.Id == id);
                var guia = g.MapTo<CriarOuEditarGuia>();

                return guia;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public JsonResult ObterGuiaJson(long id)
        //{
        //    try
        //    {
        //        var query = _guiaRepository
        //           .GetAll();

        //        Guia g = await query.FirstOrDefaultAsync(x => x.Id == id);
        //        var guia = g.MapTo<CriarOuEditarGuia>();


        //        return guiaJson;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}

        public async Task<bool> SalvarCampos(string camposJson, long guiaId)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer(new SimpleTypeResolver());
            GuiaCampoDto[] campos = new GuiaCampoDto[] { };

            try
            {
                campos = jsonSerializer.Deserialize<GuiaCampoDto[]>(camposJson);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            foreach (var campo in campos)
            {
                if (campo.IsConjunto)
                {
                    CriarOuEditarGuiaCampo guiaCampo = new CriarOuEditarGuiaCampo();
                    try
                    {
                        var subs = campo.SubConjuntos;
                        var temp = campo.MapTo<GuiaCampo>();
                        guiaCampo = temp.MapTo<CriarOuEditarGuiaCampo>();
                        guiaCampo.SubConjuntos = subs;
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }

                    var campoSalvo = await _guiaCampoAppService.CriarOuEditar(guiaCampo);

                    if (campoSalvo.Value == true)
                    {
                        CriarOuEditarRelacaoGuiaCampo relacao = new CriarOuEditarRelacaoGuiaCampo();
                        var campoSalvo2 = campoSalvo.Key;

                        relacao.GuiaId = guiaId;
                        relacao.GuiaCampoId = campoSalvo2.Id;
                        relacao.CoordenadaX = campoSalvo2.CoordenadaX;
                        relacao.CoordenadaY = campoSalvo2.CoordenadaY;

                        await _relacaoGuiaCampoAppService.CriarOuEditar(relacao);
                    }

                    if (guiaCampo.SubConjuntos != null)
                    {
                        foreach (var sub in guiaCampo.SubConjuntos)
                        {
                            var subs = sub.SubConjuntos;//talvez desnecessario
                            var temp = sub.MapTo<GuiaCampo>();
                            var subMapeado = temp.MapTo<CriarOuEditarGuiaCampo>();
                            subMapeado.ConjuntoId = campoSalvo.Key.Id;
                            subMapeado.SubConjuntos = subs;//talvez desnecessario
                            await _guiaCampoAppService.CriarOuEditar(subMapeado);
                        }
                    }
                }
                else
                {
                    CriarOuEditarGuiaCampo guiaCampo = new CriarOuEditarGuiaCampo();
                    try
                    {
                        // teoricamentre nao precisa de subConjuntos
                        var temp = campo.MapTo<GuiaCampo>();
                        guiaCampo = temp.MapTo<CriarOuEditarGuiaCampo>();
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }

                    var campoSalvo = await _guiaCampoAppService.CriarOuEditar(guiaCampo);
                }
            }

            return true;
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _guiaRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        .Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        .Replace("ã", "a").Replace("õ", "o")
                        .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        .Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        .Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        .Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
    #endregion
}

