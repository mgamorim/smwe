﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos
{
    public class GuiaCampoAppService : SWMANAGERAppServiceBase, IGuiaCampoAppService
    {
        #region Injecao e Construtor

        private readonly IRepository<GuiaCampo, long> _guiaCampoRepository;

        public GuiaCampoAppService(
            IRepository<GuiaCampo, long> motivoAltaRepository
       )
        {
            _guiaCampoRepository = motivoAltaRepository;
        }

        #endregion

        //public async Task<CriarOuEditarGuiaCampo> CriarOuEditar(CriarOuEditarGuiaCampo input)
        //{
        //    try
        //    {
        //        var guiaCampo = _guiaCampoRepository.GetAllList().FirstOrDefault(
        //            c => c.IsConjunto == input.IsConjunto &&
        //            c.Descricao == input.Descricao &&
        //            c.MaximoElementos == input.MaximoElementos &&
        //            c.IsSubItem == input.IsSubItem
        //            );

        //        if (guiaCampo == null)
        //        {
        //            guiaCampo = input.MapTo<GuiaCampo>();
        //            guiaCampo.Id = await _guiaCampoRepository.InsertAndGetIdAsync(input.MapTo<GuiaCampo>());
        //        }
        //        else
        //        {
        //            guiaCampo.CoordenadaX = input.CoordenadaX;
        //            guiaCampo.CoordenadaY = input.CoordenadaY;
        //            guiaCampo = await _guiaCampoRepository.UpdateAsync(guiaCampo);
        //        }

        //        return guiaCampo.MapTo<CriarOuEditarGuiaCampo>();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroSalvar"), ex);
        //    }
        //}

        public async Task<KeyValuePair<CriarOuEditarGuiaCampo,bool>> CriarOuEditar(CriarOuEditarGuiaCampo input)
        {
            try
            {
                bool novo = true;
                var guiaCampo = _guiaCampoRepository.GetAllList().FirstOrDefault(
                    c => c.IsConjunto == input.IsConjunto &&
                    c.Descricao == input.Descricao &&
                    c.MaximoElementos == input.MaximoElementos &&
                    c.IsSubItem == input.IsSubItem
                    );

                if (guiaCampo == null)
                {
                    guiaCampo = input.MapTo<GuiaCampo>();
                    guiaCampo.Id = await _guiaCampoRepository.InsertAndGetIdAsync(guiaCampo);
                }
                else
                {
                 //   guiaCampo = input.MapTo<GuiaCampo>();

                    guiaCampo.ConjuntoId = input.ConjuntoId;
                    guiaCampo.CoordenadaX = input.CoordenadaX;
                    guiaCampo.CoordenadaY = input.CoordenadaY;
                    guiaCampo.Descricao = input.Descricao;
                    guiaCampo.IsConjunto = input.IsConjunto;
                    guiaCampo.IsSubItem = input.IsSubItem;
                //    guiaCampo.MaximoElementos = input.MaximoElementos;

                    guiaCampo = await _guiaCampoRepository.UpdateAsync(guiaCampo);
                    novo = false;
                }

                var gc = guiaCampo.MapTo<CriarOuEditarGuiaCampo>();

                return new KeyValuePair<CriarOuEditarGuiaCampo, bool>(gc, novo);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarGuiaCampo input)
        {
            try
            {
                await _guiaCampoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<GuiaCampoDto>> Listar(ListarGuiaCamposInput input)
        {
            var contarGuiaCampos = 0;
            List<GuiaCampo> motivosAlta;
            List<GuiaCampoDto> motivosAltaDtos = new List<GuiaCampoDto>();
            try
            {
                var query = _guiaCampoRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGuiaCampos = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<GuiaCampoDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<GuiaCampoDto>(
                contarGuiaCampos,
                motivosAltaDtos
                );
        }

        public PagedResultDto<GuiaCampoDto> ListarParaConjunto(long conjuntoId)
        {
            var contarGuiaCampos = 0;
            List<GuiaCampo> motivosAlta;
            List<GuiaCampoDto> motivosAltaDtos = new List<GuiaCampoDto>();
            try
            {
                var query = _guiaCampoRepository
                    .GetAll()
                    .Where(
                        c => c.ConjuntoId == conjuntoId
                    );

                contarGuiaCampos = query
                    .Count();

                motivosAlta = query
                    .ToList();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<GuiaCampoDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<GuiaCampoDto>(
                contarGuiaCampos,
                motivosAltaDtos
                );
        }

        public async Task<CriarOuEditarGuiaCampo> Obter(long id)
        {
            try
            {
                var query = await _guiaCampoRepository
                    .GetAsync(id);

                var motivoAlta = query
                    .MapTo<CriarOuEditarGuiaCampo>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public GuiaCampoDto ObterDto(long id)
        {
            try
            {
                var query = _guiaCampoRepository
                    .Get(id);

                var motivoAlta = query
                    .MapTo<GuiaCampoDto>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
