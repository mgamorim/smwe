﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias
{
    public class RelacaoGuiaCampoAppService : SWMANAGERAppServiceBase, IRelacaoGuiaCampoAppService
    {
        #region Injecao e Construtor

        private readonly IRepository<RelacaoGuiaCampo, long> _relacaoGuiaCampoRepository;
        private readonly IRepository<GuiaCampo, long> _guiaCampoRepository;

        public RelacaoGuiaCampoAppService(
            IRepository<RelacaoGuiaCampo, long> relacaoGuiaCampoRepository,
            IRepository<GuiaCampo, long> guiaCampoRepository
            )
        {
            _relacaoGuiaCampoRepository = relacaoGuiaCampoRepository;
            _guiaCampoRepository = guiaCampoRepository;
        }

        #endregion

        public async Task<CriarOuEditarRelacaoGuiaCampo> CriarOuEditar(CriarOuEditarRelacaoGuiaCampo input)
        {
            try
            {
                var relacaoGuiaCampo = _relacaoGuiaCampoRepository.GetAllList().FirstOrDefault(
                    c => c.GuiaId == input.GuiaId &&
                         c.GuiaCampoId == input.GuiaCampoId
                    );

                if (relacaoGuiaCampo == null)
                {
                    relacaoGuiaCampo = input.MapTo<RelacaoGuiaCampo>();
                    relacaoGuiaCampo.Id = await _relacaoGuiaCampoRepository.InsertAndGetIdAsync(input.MapTo<RelacaoGuiaCampo>());
                }
                else
                {
                    relacaoGuiaCampo.CoordenadaX = input.CoordenadaX;
                    relacaoGuiaCampo.CoordenadaY = input.CoordenadaY;
                    await _relacaoGuiaCampoRepository.UpdateAsync(relacaoGuiaCampo);
                }

                return relacaoGuiaCampo.MapTo<CriarOuEditarRelacaoGuiaCampo>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarRelacaoGuiaCampo input)
        {
            try
            {
                await _relacaoGuiaCampoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<CriarOuEditarRelacaoGuiaCampo> Obter(long id)
        {
            try
            {
                var query = await _relacaoGuiaCampoRepository
                    .GetAll()
                    .Include(m => m.Guia)
                    .Include(m => m.GuiaCampo)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var relacaoGuiaCampo = query.MapTo<CriarOuEditarRelacaoGuiaCampo>();

                return relacaoGuiaCampo;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public GuiaCampoDto ObterGuiaCampo(long id)
        //{
        //    try
        //    {
        //        var query = _relacaoGuiaCampoRepository
        //            .GetAll()
        //            .Where(g => g.GuiaCampo.Id == id
        //            );

        //        var guiaCampo = query.MapTo<GuiaCampoDto>();

        //        return guiaCampo;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}

        //public RelacaoGuiaCampoDto ObterDto(long id)
        //{
        //    try
        //    {
        //        var query = _relacaoGuiaCampoRepository
        //            .Get(id);

        //        var relacaoGuiaCampo = query.MapTo<RelacaoGuiaCampoDto>();

        //        return relacaoGuiaCampo;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}

        //public GuiaCampoDto ObterGuiaCampo(long id)
        //{
        //    try
        //    {
        //        var query = _relacaoGuiaCampoRepository
        //            .Get(id);

        //        var guiaCampo = query.MapTo<RelacaoGuiaCampoDto>().GuiaCampo;

        //        return guiaCampo;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}

        //public PagedResultDto<RelacaoGuiaCampoDto> ListarParaGuia(long guiaId)
        //{
        //    var contarGuias = 0;
        //    List<RelacaoGuiaCampo> guias;
        //    List<RelacaoGuiaCampoDto> guiasDtos = new List<RelacaoGuiaCampoDto>();
        //    try
        //    {
        //        var query = _relacaoGuiaCampoRepository
        //            .GetAll()
        //            .Where(
        //                r => r.GuiaId == guiaId
        //            );

        //        contarGuias = 0;

        //        guias = query
        //            .ToList();

        //        guiasDtos = guias
        //            .MapTo<List<RelacaoGuiaCampoDto>>();

        //        return new PagedResultDto<RelacaoGuiaCampoDto>(
        //        contarGuias,
        //        guiasDtos
        //        );
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}


        //public async Task<PagedResultDto<RelacaoGuiaCampoDto>> ListarParaConjunto(long conjuntoId)

        //public PagedResultDto<RelacaoGuiaCampoDto> ListarParaConjunto(long conjuntoId, long guiaId)

        public PagedResultDto<GuiaCampoDto> ListarParaConjunto(long conjuntoId, long guiaId)
        {
            //var contarGuias = 0;
            //List<RelacaoGuiaCampo> guias;
            //List<RelacaoGuiaCampoDto> guiasDtos = new List<RelacaoGuiaCampoDto>();
            try
            {
                //    var campo = _relacaoGuiaCampoRepository.Get(conjuntoId);

                var subCampos = _guiaCampoRepository
                    .GetAll()
                    .Where(s => s.ConjuntoId == conjuntoId && s.IsSubItem == true)
                    ;

                //      contarGuias = 0;

                var subCamposDto = new List<GuiaCampoDto>();

                foreach (var sub in subCampos)
                {
                    subCamposDto.Add(sub.MapTo<GuiaCampoDto>());
                }

                return new PagedResultDto<GuiaCampoDto>(
                0,
                subCamposDto
                );


                //    .ToList();

                //guiasDtos = guias
                //    .MapTo<List<RelacaoGuiaCampoDto>>();

                //return new PagedResultDto<RelacaoGuiaCampoDto>(
                //contarGuias,
                //guiasDtos
                //);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
