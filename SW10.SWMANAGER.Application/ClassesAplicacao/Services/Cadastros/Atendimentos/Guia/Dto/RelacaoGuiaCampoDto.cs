﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias.Dto;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto
{
    [AutoMap(typeof(RelacaoGuiaCampo))]
    public class RelacaoGuiaCampoDto : CamposPadraoCRUDDto
    {
        public float CoordenadaX { get; set; }

        public float CoordenadaY { get; set; }

        public long GuiaId { get; set; }
        [ForeignKey("GuiaId")]
        public virtual GuiaDto Guia { get; set; }

        public long GuiaCampoId { get; set; }
        [ForeignKey("GuiaCampoId")]
        public virtual GuiaCampoDto GuiaCampo { get; set; }
    }
}
