﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias.Dto
{
    [AutoMap(typeof(GuiaCampo))]
    public class CriarOuEditarGuiaCampo : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public float CoordenadaX { get; set; }

        public float CoordenadaY { get; set; }

        public bool IsConjunto { get; set; }

        public bool IsSubItem { get; set; }

        public long? ConjuntoId { get; set; }

        public int? MaximoElementos { get; set; }

        public GuiaCampoDto[] SubConjuntos { get; set; }

   //     public virtual ICollection<RelacaoGuiaCampo> Campos { get; set; }
    }
}