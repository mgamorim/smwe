﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Guias;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Guias.Dto
{
    [AutoMap(typeof(Guia))]
    public class CriarOuEditarGuia : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public long? OriginariaId { get; set; }

        public byte[] ModeloPDF { get; set; }

        public string ModeloPDFMimeType { get; set; }

        public byte[] ModeloPNG { get; set; }

        public string ModeloPNGMimeType { get; set; }

        public string CamposJson { get; set; }

        public CriarOuEditarGuia()
        {
            
        }

        // esta sendo usado?
        public IEnumerable<Type> PegarClassesBase(Type tipo)
        {
            var result = new List<Type>();
            var atual = tipo.BaseType;
            while (atual != null)
            {
                //yield return atual;
                result.Add(atual);
                atual = atual.BaseType;
            }
            return result;
        }
    }
}