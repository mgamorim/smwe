﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Intervalos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades.Dto
{
    [AutoMap(typeof(AgendamentoConsultaMedicoDisponibilidade))]
    public class CriarOuEditarAgendamentoConsultaMedicoDisponibilidade : CamposPadraoCRUDDto
    {
        public long MedicoId { get; set; }

        public long MedicoEspecialidadeId { get; set; }

        public DateTime DataInicio { get; set; }

        public DateTime DataFim { get; set; }

        public DateTime HoraInicio { get; set; }

        public DateTime HoraFim { get; set; }

        public long IntervaloId { get; set; }

        public bool Domingo { get; set; }

        public bool Segunda { get; set; }

        public bool Terca { get; set; }

        public bool Quarta { get; set; }

        public bool Quinta { get; set; }

        public bool Sexta { get; set; }

        public bool Sabado { get; set; }

        [ForeignKey("MedicoId")]
        public virtual MedicoDto Medico { get; set; }

        [ForeignKey("MedicoEspecialidadeId")]
        public virtual MedicoEspecialidadeDto MedicoEspecialidade { get; set; }

        [ForeignKey("IntervaloId")]
        public virtual IntervaloDto Intervalo { get; set; }

    }
}
