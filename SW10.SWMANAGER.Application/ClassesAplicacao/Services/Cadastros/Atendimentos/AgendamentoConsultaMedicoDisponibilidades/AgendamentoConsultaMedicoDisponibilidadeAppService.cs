﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades;
using Abp.AutoMapper;
using System.Data.Entity;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Extensions;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.AgendamentoConsultaMedicoDisponibilidades
{
    public class AgendamentoConsultaMedicoDisponibilidadeAppService : SWMANAGERAppServiceBase, IAgendamentoConsultaMedicoDisponibilidadeAppService
    {
        private readonly IRepository<AgendamentoConsultaMedicoDisponibilidade, long> _agendamentoConsultaMedicoDisponibilidadeRepository;
        private readonly IListarAgendamentoConsultaMedicoDisponibilidadesExcelExporter _listarAgendamentoConsultaMedicoDisponibilidadesExcelExporter;

        public AgendamentoConsultaMedicoDisponibilidadeAppService(IRepository<AgendamentoConsultaMedicoDisponibilidade, long> agendamentoConsultaMedicoDisponibilidadeRepository, IListarAgendamentoConsultaMedicoDisponibilidadesExcelExporter listarAgendamentoConsultaMedicoDisponibilidadesExcelExporter)
        {
            _agendamentoConsultaMedicoDisponibilidadeRepository = agendamentoConsultaMedicoDisponibilidadeRepository;
            _listarAgendamentoConsultaMedicoDisponibilidadesExcelExporter = listarAgendamentoConsultaMedicoDisponibilidadesExcelExporter;
        }

        public async Task CriarOuEditar(CriarOuEditarAgendamentoConsultaMedicoDisponibilidade input)
        {
            try
            {
                var agendamentoConsultaMedicoDisponibilidade = input.MapTo<AgendamentoConsultaMedicoDisponibilidade>();
                if (input.Id.Equals(0))
                {
                    await _agendamentoConsultaMedicoDisponibilidadeRepository.InsertAsync(agendamentoConsultaMedicoDisponibilidade);
                }
                else
                {
                    await _agendamentoConsultaMedicoDisponibilidadeRepository.UpdateAsync(agendamentoConsultaMedicoDisponibilidade);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(CriarOuEditarAgendamentoConsultaMedicoDisponibilidade input)
        {
            try
            {
                await _agendamentoConsultaMedicoDisponibilidadeRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<ListResultDto<AgendamentoConsultaMedicoDisponibilidadeDto>> ListarTodos()
        {
            try
            {
                var query = await _agendamentoConsultaMedicoDisponibilidadeRepository
                    .GetAll()
                    .Include(m => m.Intervalo)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .ToListAsync();

                var agendamentoConsultaMedicoDisponibilidadesDto = query.MapTo<List<AgendamentoConsultaMedicoDisponibilidadeDto>>();

                return new ListResultDto<AgendamentoConsultaMedicoDisponibilidadeDto> { Items = agendamentoConsultaMedicoDisponibilidadesDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<AgendamentoConsultaMedicoDisponibilidade>> ListarAtivos(long? medicoId, long? especialidadeId)
        {
            try
            {
                return await _agendamentoConsultaMedicoDisponibilidadeRepository
                    .GetAll()
                    .Include(m => m.Intervalo)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .WhereIf(medicoId.HasValue && medicoId > 0, m => m.MedicoId == medicoId)
                    .WhereIf(especialidadeId.HasValue && especialidadeId > 0, m => m.MedicoEspecialidade.EspecialidadeId == especialidadeId)
                    //.GetAllListAsync(m => m.DataFim >= DateTime.Now)
                    .ToListAsync();

                //var agendamentoConsultaMedicoDisponibilidadesDto = query.MapTo<List<AgendamentoConsultaMedicoDisponibilidadeDto>>();

                //return new ListResultDto<AgendamentoConsultaMedicoDisponibilidadeDto> { Items = agendamentoConsultaMedicoDisponibilidadesDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<AgendamentoConsultaMedicoDisponibilidadeDto>> Listar(ListarAgendamentoConsultaMedicoDisponibilidadesInput input)
        {
            var contarAgendamentoConsultaMedicoDisponibilidades = 0;
            List<AgendamentoConsultaMedicoDisponibilidade> agendamentoConsultaMedicoDisponibilidades;
            List<AgendamentoConsultaMedicoDisponibilidadeDto> agendamentoConsultaMedicoDisponibilidadesDtos = new List<AgendamentoConsultaMedicoDisponibilidadeDto>();
            try
            {
                var query = _agendamentoConsultaMedicoDisponibilidadeRepository
                    .GetAll()
                    .Include(m => m.Intervalo)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade);
                //.WhereIf(!input..IsNullOrEmpty(), m =>
                //    m.Medico.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                //    m.MedicoEspecialidade.Especialidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                //    m.DataInicio.ToString("dd/MM/yyyy").ToUpper().Contains(input.Filtro.ToUpper()) ||
                //    m.DataFim.ToString("dd/MM/yyyy").ToUpper().Contains(input.Filtro.ToUpper()) ||
                //    m.HoraFim.ToString("hh:mm:ss").ToUpper().Contains(input.Filtro.ToUpper()) ||
                //    m.HoraInicio.ToString("hh:mm:ss").ToUpper().Contains(input.Filtro.ToUpper())
                //);

                contarAgendamentoConsultaMedicoDisponibilidades = await query
                    .CountAsync();

                agendamentoConsultaMedicoDisponibilidades = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                agendamentoConsultaMedicoDisponibilidadesDtos = agendamentoConsultaMedicoDisponibilidades
                    .MapTo<List<AgendamentoConsultaMedicoDisponibilidadeDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<AgendamentoConsultaMedicoDisponibilidadeDto>(
                contarAgendamentoConsultaMedicoDisponibilidades,
                agendamentoConsultaMedicoDisponibilidadesDtos
                );
        }

        public async Task<FileDto> ListarParaExcel(ListarAgendamentoConsultaMedicoDisponibilidadesInput input)
        {
            try
            {
                var result = await Listar(input);
                var agendamentoConsultaMedicoDisponibilidades = result.Items;
                return _listarAgendamentoConsultaMedicoDisponibilidadesExcelExporter.ExportToFile(agendamentoConsultaMedicoDisponibilidades.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ICollection<AgendamentoConsultaMedicoDisponibilidade>> ListarPorData(DateTime date)
        {
            try
            {
                var dayOfWeek = (int)date.DayOfWeek;

                return await _agendamentoConsultaMedicoDisponibilidadeRepository
                    .GetAll()
                    .Include(m => m.Intervalo)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .Where(m =>
                    m.DataInicio <= date
                    && m.DataFim >= date
                    && (dayOfWeek == 0
                    ? m.Domingo == true
                    : dayOfWeek == 1
                    ? m.Segunda == true
                    : dayOfWeek == 2
                    ? m.Terca == true
                    : dayOfWeek == 3
                    ? m.Quarta == true
                    : dayOfWeek == 4
                    ? m.Quinta == true
                    : dayOfWeek == 5
                    ? m.Sexta == true
                    : m.Sabado == true)
                    )
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<CriarOuEditarAgendamentoConsultaMedicoDisponibilidade> Obter(long id)
        {
            try
            {
                var result = await _agendamentoConsultaMedicoDisponibilidadeRepository
                    .GetAll()
                    .Include(m => m.Intervalo)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var agendamentoConsultaMedicoDisponibilidade = result
                    //.FirstOrDefault()
                    .MapTo<CriarOuEditarAgendamentoConsultaMedicoDisponibilidade>();

                return agendamentoConsultaMedicoDisponibilidade;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<AgendamentoConsultaMedicoDisponibilidadeDto> Obter(long medicoId, long especialidadeId, DateTime horaAgendamento)
        {
            try
            {
                var query = await _agendamentoConsultaMedicoDisponibilidadeRepository
                    .GetAll()
                    .Include(m => m.Intervalo)
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.MedicoEspecialidade.Especialidade)
                    .Where(m =>
                        m.MedicoId == medicoId
                        && m.MedicoEspecialidade.EspecialidadeId == especialidadeId
                    )
                    .ToListAsync();

                var results = new List<AgendamentoConsultaMedicoDisponibilidadeDto>();
                foreach (var item in query)
                {

                    var dataIni = new DateTime(01, 01, 01, item.HoraInicio.Hour, item.HoraInicio.Minute, item.HoraInicio.Second);
                    var dataFim = new DateTime(01, 01, 01, item.HoraFim.Hour, item.HoraFim.Minute, item.HoraFim.Second);
                    var agendamento = new DateTime(01, 01, 01, horaAgendamento.Hour, horaAgendamento.Minute, horaAgendamento.Second);
                    if (dataIni <= agendamento && dataFim >= agendamento)
                    {
                        results.Add(new AgendamentoConsultaMedicoDisponibilidadeDto
                        {
                            CreationTime = item.CreationTime,
                            CreatorUserId = item.CreatorUserId,
                            DataFim = item.DataFim,
                            DataInicio = item.DataInicio,
                            DeleterUserId = item.DeleterUserId,
                            DeletionTime = item.DeletionTime,
                            Domingo = item.Domingo,
                            HoraFim = item.HoraFim,
                            HoraInicio = item.HoraInicio,
                            Id = item.Id,
                            IntervaloId = item.IntervaloId,
                            IsDeleted = item.IsDeleted,
                            IsSistema = item.IsSistema,
                            LastModificationTime = item.LastModificationTime,
                            LastModifierUserId = item.LastModifierUserId,
                            MedicoEspecialidadeId = item.MedicoEspecialidadeId,
                            MedicoId = item.MedicoId,
                            Quarta = item.Quarta,
                            Quinta = item.Quinta,
                            Sexta = item.Sexta,
                            Sabado = item.Sabado,
                            Segunda = item.Segunda,
                            Terca = item.Terca
                        });
                    }
                }

                //var agendamentoConsultaMedicoDisponibilidade = results
                //    .FirstOrDefault()
                //    .MapTo<CriarOuEditarAgendamentoConsultaMedicoDisponibilidade>();

                return results.FirstOrDefault(); //agendamentoConsultaMedicoDisponibilidade;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarMedicosDisponiveisDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _agendamentoConsultaMedicoDisponibilidadeRepository
                            .GetAll()
                            .Include(m => m.Medico)
                            .Include(m => m.Medico.SisPessoa)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.Medico.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Medico.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby p.Medico.NomeCompleto ascending
                            select new DropdownItems { id = p.Medico.Id, text = string.Concat(p.Medico.Codigo, " - ", p.Medico.NomeCompleto) };
                //paginação 
                var queryResultPage = query
                    .Skip(numberOfObjectsPerPage * pageInt)
                    .Take(numberOfObjectsPerPage)
                    ;

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.Distinct().ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarEspecialidadesMedicoDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);

            try
            {
                long medicoId = 0;
                var isMedico = long.TryParse(dropdownInput.filtro, out medicoId);
                if (medicoId == 0)
                {
                    throw new Exception(L("InformarMedico"));
                }
                //get com filtro
                var query = from p in _agendamentoConsultaMedicoDisponibilidadeRepository
                            .GetAll()
                            .Include(m => m.MedicoEspecialidade)
                            .Include(m => m.MedicoEspecialidade.Especialidade)
                            .Where(m => m.MedicoId == medicoId)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.MedicoEspecialidade.Especialidade.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.MedicoEspecialidade.Especialidade.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                            )

                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.MedicoEspecialidade.Id, text = string.Concat(p.MedicoEspecialidade.Especialidade.Codigo, " - ", p.MedicoEspecialidade.Especialidade.Descricao) };
                //paginação 
                var queryResultPage = query
                    .Skip(numberOfObjectsPerPage * pageInt)
                    .Take(numberOfObjectsPerPage)
                    ;

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.Distinct().ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
