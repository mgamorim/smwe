﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.ServicosMedicosPrestados;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Especialidades;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.ServicosMedicosPrestados.Dto
{
    [AutoMapTo(typeof(ServicoMedicoPrestado))]
    public class CriarOuEditarServicoMedicoPrestado : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }

        public string ModeloAnamnese { get; set; }

        public bool Caucao { get; set; }

        public long? EspecialidadeId { get; set; }

        //[ForeignKey("EspecialidadeId")]
        //public virtual Especialidade Especialidade { get; set; }
    }
}