﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosAlta.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosAlta
{
    public class MotivoAltaTipoAltaAppService : SWMANAGERAppServiceBase, IMotivoAltaTipoAltaAppService
    {
        private readonly IRepository<MotivoAltaTipoAlta, long> _motivoAltaRepository;

        public MotivoAltaTipoAltaAppService(
            IRepository<MotivoAltaTipoAlta, long> motivoAltaRepository
            )
        {
            _motivoAltaRepository = motivoAltaRepository;
        }

        public async Task CriarOuEditar(CriarOuEditarMotivoAltaTipoAlta input)
        {
            try
            {
                var MotivoAltaTipoAlta = input.MapTo<MotivoAltaTipoAlta>();
                if (input.Id.Equals(0))
                {
                    await _motivoAltaRepository.InsertAsync(MotivoAltaTipoAlta);
                }
                else
                {
                    await _motivoAltaRepository.UpdateAsync(MotivoAltaTipoAlta);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarMotivoAltaTipoAlta input)
        {
            try
            {
                await _motivoAltaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<MotivoAltaTipoAltaDto>> Listar(ListarMotivosAltaInput input)
        {
            var contarMotivosAlta = 0;
            List<MotivoAltaTipoAlta> motivosAlta;
            List<MotivoAltaTipoAltaDto> motivosAltaDtos = new List<MotivoAltaTipoAltaDto>();
            try
            {
                var query = _motivoAltaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarMotivosAlta = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<MotivoAltaTipoAltaDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<MotivoAltaTipoAltaDto>(
                contarMotivosAlta,
                motivosAltaDtos
                );
        }


        public async Task<CriarOuEditarMotivoAltaTipoAlta> Obter(long id)
        {
            try
            {
                var query = await _motivoAltaRepository
                    .GetAsync(id);

                var motivoAlta = query
                    .MapTo<CriarOuEditarMotivoAltaTipoAlta>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<MotivoAltaTipoAltaDto> motivoaltatipoaltadto = new List<MotivoAltaTipoAltaDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                //   bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _motivoAltaRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                                //.Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = queryResultPage.ToList();

                int total = await Task.Run(() => query.Count());

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


    }
}
