﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosAlta.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta.Dto
{
    [AutoMap(typeof(MotivoAlta))]
    public class MotivoAltaDto : CamposPadraoCRUDDto
	{
        public long MotivoAltaTipoAltaId { get; set; }

        public MotivoAltaTipoAltaDto MotivoAltaTipoAlta { get; set; }
    }
}
