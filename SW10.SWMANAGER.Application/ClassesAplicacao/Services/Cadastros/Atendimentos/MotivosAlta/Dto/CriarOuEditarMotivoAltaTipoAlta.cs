﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta.Dto
{
    [AutoMap(typeof(MotivoAltaTipoAlta))]
    public class CriarOuEditarMotivoAltaTipoAlta : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
