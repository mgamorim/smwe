﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosAlta.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosAlta.Exporting;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.MotivosAlta.Dto;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosAlta
{
    public class MotivoAltaAppService : SWMANAGERAppServiceBase, IMotivoAltaAppService
    {
        private readonly IRepository<MotivoAlta, long> _motivoAltaRepository;
        private readonly IListarMotivosAltaExcelExporter _listarMotivosAltaExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public MotivoAltaAppService(
            IRepository<MotivoAlta, long> motivoAltaRepository,
            IListarMotivosAltaExcelExporter listarMotivosAltaExcelExporter,
             IUnitOfWorkManager unitOfWorkManager)
        {
            _motivoAltaRepository = motivoAltaRepository;
            _listarMotivosAltaExcelExporter = listarMotivosAltaExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]//Atualizado (Pablo 08/08/2017)
        public async Task CriarOuEditar(CriarOuEditarMotivoAlta input)
        {
            try
            {
                var MotivoAlta = input.MapTo<MotivoAlta>();
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.Id.Equals(0))
                    {
                        await _motivoAltaRepository.InsertAsync(MotivoAlta);
                    }
                    else
                    {
                        await _motivoAltaRepository.UpdateAsync(MotivoAlta);
                    }
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarMotivoAlta input)
        {
            try
            {
                await _motivoAltaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<MotivoAltaDto>> Listar(ListarMotivosAltaInput input)
        {
            var contarMotivosAlta = 0;
            List<MotivoAlta> motivosAlta;
            List<MotivoAltaDto> motivosAltaDtos = new List<MotivoAltaDto>();
            try
            {
                var query = _motivoAltaRepository
                    .GetAllIncluding(
                        m => m.MotivoAltaTipoAlta
                    )
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarMotivosAlta = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<MotivoAltaDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<MotivoAltaDto>(
                contarMotivosAlta,
                motivosAltaDtos
                );
        }

        public async Task<FileDto> ListarParaExcel(ListarMotivosAltaInput input)
        {
            try
            {
                var result = await Listar(input);
                var motivosAlta = result.Items;
                return _listarMotivosAltaExcelExporter.ExportToFile(motivosAlta.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<CriarOuEditarMotivoAlta> Obter(long id)
        {
            try
            {
                var query = await _motivoAltaRepository
                    .GetAll()
                    .Include(m => m.MotivoAltaTipoAlta)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var motivoAlta = query
                    .MapTo<CriarOuEditarMotivoAlta>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<MotivoAltaDto> modalidadeDto = new List<MotivoAltaDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                //   bool isLaudo = (!dropdownInput.filtro.IsNullOrEmpty()) ? dropdownInput.filtro.Equals("IsLaudo") : false;

                var query = from p in _motivoAltaRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToLower().Contains(dropdownInput.search.ToLower())
                            )
                                //.Where(f => f.IsLaudo.Equals(isLaudo))
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = queryResultPage.ToList();

                int total = await Task.Run(() => query.Count());

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
