﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.UnidadesInternacao.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.UnidadesInternacao
{
    public class UnidadeInternacaoTipoAppService : SWMANAGERAppServiceBase, IUnidadeInternacaoTipoAppService
    {
        private readonly IRepository<UnidadeInternacaoTipo, long> _motivoAltaRepository;

        public UnidadeInternacaoTipoAppService(
            IRepository<UnidadeInternacaoTipo, long> motivoAltaRepository
            )
        {
            _motivoAltaRepository = motivoAltaRepository;
        }

        public async Task CriarOuEditar(CriarOuEditarUnidadeInternacaoTipo input)
        {
            try
            {
                var UnidadeInternacaoTipo = input.MapTo<UnidadeInternacaoTipo>();
                if (input.Id.Equals(0))
                {
                    await _motivoAltaRepository.InsertAsync(UnidadeInternacaoTipo);
                }
                else
                {
                    await _motivoAltaRepository.UpdateAsync(UnidadeInternacaoTipo);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarUnidadeInternacaoTipo input)
        {
            try
            {
                await _motivoAltaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<UnidadeInternacaoTipoDto>> Listar(ListarUnidadesInternacaoInput input)
        {
            var contarUnidadesInternacao = 0;
            List<UnidadeInternacaoTipo> motivosAlta;
            List<UnidadeInternacaoTipoDto> motivosAltaDtos = new List<UnidadeInternacaoTipoDto>();
            try
            {
                var query = _motivoAltaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarUnidadesInternacao = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<UnidadeInternacaoTipoDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<UnidadeInternacaoTipoDto>(
                contarUnidadesInternacao,
                motivosAltaDtos
                );
        }


        public async Task<CriarOuEditarUnidadeInternacaoTipo> Obter(long id)
        {
            try
            {
                var query = await _motivoAltaRepository
                    .GetAsync(id);

                var motivoAlta = query
                    .MapTo<CriarOuEditarUnidadeInternacaoTipo>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

    }
}
