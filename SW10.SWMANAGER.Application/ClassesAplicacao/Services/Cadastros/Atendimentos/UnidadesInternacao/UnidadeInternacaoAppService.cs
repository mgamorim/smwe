﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.UnidadesInternacao.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.UnidadesInternacao
{
    public class UnidadeInternacaoAppService : SWMANAGERAppServiceBase, IUnidadeInternacaoAppService
    {
        private readonly IRepository<UnidadeInternacao, long> _unidadeInternacaoRepository;
    //    private readonly IListarUnidadesInternacaoExcelExporter _listarUnidadesInternacaoExcelExporter;

        public UnidadeInternacaoAppService(
            IRepository<UnidadeInternacao, long> unidadeInternacaoRepository
            //   ,
       //     IListarUnidadesInternacaoExcelExporter listarUnidadesInternacaoExcelExporter
       )
        {
            _unidadeInternacaoRepository = unidadeInternacaoRepository;
        //    _listarUnidadesInternacaoExcelExporter = listarUnidadesInternacaoExcelExporter;
        }

        public async Task CriarOuEditar(CriarOuEditarUnidadeInternacao input)
        {
            try
            {
                var UnidadeInternacao = input.MapTo<UnidadeInternacao>();
                if (input.Id.Equals(0))
                {
                    await _unidadeInternacaoRepository.InsertAsync(UnidadeInternacao);
                }
                else
                {
                    await _unidadeInternacaoRepository.UpdateAsync(UnidadeInternacao);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarUnidadeInternacao input)
        {
            try
            {
                await _unidadeInternacaoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<UnidadeInternacaoDto>> Listar(ListarUnidadesInternacaoInput input)
        {
            var contarUnidadesInternacao = 0;
            List<UnidadeInternacao> unidadesInternacao;
            List<UnidadeInternacaoDto> unidadesInternacaoDtos = new List<UnidadeInternacaoDto>();
            try
            {
                var query = _unidadeInternacaoRepository
                    //.GetAll()
                    .GetAllIncluding(
                        m => m.UnidadeInternacaoTipo
                    )
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarUnidadesInternacao = await query
                    .CountAsync();

                unidadesInternacao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                unidadesInternacaoDtos = unidadesInternacao
                    .MapTo<List<UnidadeInternacaoDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<UnidadeInternacaoDto>(
                contarUnidadesInternacao,
                unidadesInternacaoDtos
                );
        }
        
        //public async Task<FileDto> ListarParaExcel(ListarUnidadesInternacaoInput input)
        //{
        //    try
        //    {
        //        var result = await Listar(input);
        //        var unidadesInternacao = result.Items;
        //        return _listarUnidadesInternacaoExcelExporter.ExportToFile(unidadesInternacao.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroExportar"));
        //    }
        //}
        
        public async Task<CriarOuEditarUnidadeInternacao> Obter(long id)
        {
            try
            {
                var query = await _unidadeInternacaoRepository
                    .GetAll()
                    .Include(m => m.UnidadeInternacaoTipo)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var unidadeInternacao = query
                    .MapTo<CriarOuEditarUnidadeInternacao>();

                return unidadeInternacao;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }
        
    }
}
