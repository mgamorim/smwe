﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao.Dto
{
    [AutoMap(typeof(UnidadeInternacaoTipo))]
    public class CriarOuEditarUnidadeInternacaoTipo : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }
    }
}
