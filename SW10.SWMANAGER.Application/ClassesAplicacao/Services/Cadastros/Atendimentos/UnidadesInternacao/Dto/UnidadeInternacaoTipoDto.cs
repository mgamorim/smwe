﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.UnidadesInternacao.Dto
{
    [AutoMap(typeof(UnidadeInternacaoTipo))]
    public class UnidadeInternacaoTipoDto : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }



        #region Mapeamento

        public static UnidadeInternacaoTipo Mapear(UnidadeInternacaoTipoDto unidadeInternacaoTipoDto)
        {
            UnidadeInternacaoTipo unidadeInternacaoTipo = new UnidadeInternacaoTipo();

            unidadeInternacaoTipo.Id = unidadeInternacaoTipoDto.Id;
            unidadeInternacaoTipo.Codigo = unidadeInternacaoTipoDto.Codigo;
            unidadeInternacaoTipo.Descricao = unidadeInternacaoTipoDto.Descricao;

            return unidadeInternacaoTipo;
        }

        public static UnidadeInternacaoTipoDto Mapear(UnidadeInternacaoTipo unidadeInternacaoTipo)
        {
            UnidadeInternacaoTipoDto unidadeInternacaoTipoDto = new UnidadeInternacaoTipoDto();

            unidadeInternacaoTipoDto.Id = unidadeInternacaoTipo.Id;
            unidadeInternacaoTipoDto.Codigo = unidadeInternacaoTipo.Codigo;
            unidadeInternacaoTipoDto.Descricao = unidadeInternacaoTipo.Descricao;

            return unidadeInternacaoTipoDto;
        }

        #endregion
    }

}
