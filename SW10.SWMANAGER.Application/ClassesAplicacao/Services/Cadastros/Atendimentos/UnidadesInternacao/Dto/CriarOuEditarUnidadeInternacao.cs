﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.UnidadesInternacao.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.UnidadesInternacao.Dto
{
    [AutoMap(typeof(UnidadeInternacao))]
    public class CriarOuEditarUnidadeInternacao : CamposPadraoCRUDDto
    {
        public string Codigo { get; set; }

        public string Descricao { get; set; }

        public string Localizacao { get; set; }

        public bool IsHospitalDia { get; set; }

        public bool IsAtivo { get; set; }

        public long? UnidadeInternacaoTipoId { get; set; }

        public virtual UnidadeInternacaoTipoDto UnidadeInternacaoTipo { get; set; }
    }
}
