using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.TiposAtendimento;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TabelasDominio.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Dto
{
    [AutoMap(typeof(TipoAtendimento))]
    public class TipoAtendimentoDto : CamposPadraoCRUDDto
    {
        public bool IsInternacao { get; set; }

        public bool IsAmbulatorioEmergencia { get; set; }

        public long? TabelaItemTissId { get; set; }
        public TabelaDominioDto TabelaDominio { get; set; }

        public static TipoAtendimentoDto Mapear(TipoAtendimento tipoAtendimento)
        {
            TipoAtendimentoDto tipoAtendimentoDto = new TipoAtendimentoDto();

            tipoAtendimentoDto.Id = tipoAtendimento.Id;
            tipoAtendimentoDto.Codigo = tipoAtendimento.Codigo;
            tipoAtendimentoDto.Descricao = tipoAtendimento.Descricao;
            tipoAtendimentoDto.TabelaItemTissId = tipoAtendimento.TabelaItemTissId;

            if(tipoAtendimento.TabelaDominio!=null)
            {
                tipoAtendimentoDto.TabelaDominio = TabelaDominioDto.Mapear(tipoAtendimento.TabelaDominio); 
            }

            return tipoAtendimentoDto;
        }
    }
}
