using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Exporting
{
    public interface IListarTipoAtendimentoExcelExporter
    {
        FileDto ExportToFile(List<TipoAtendimentoDto> tipoAtendimentoDtos);
    }
}
