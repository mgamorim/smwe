using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.TiposAtendimento;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposTabelaDominio;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.TiposAtendimento
{
    public class TipoAtendimentoAppService : SWMANAGERAppServiceBase, ITipoAtendimentoAppService
    {
        private readonly IRepository<TipoAtendimento, long> _tipoAtendimentoRepositorio;
        private readonly IListarTipoAtendimentoExcelExporter _listarTipoAtendimentoExcelExporter;


        public TipoAtendimentoAppService(IRepository<TipoAtendimento, long> tipoAtendimentoRepositorio,
            IListarTipoAtendimentoExcelExporter listarTipoAtendimentoExcelExporter)
        {
            _tipoAtendimentoRepositorio = tipoAtendimentoRepositorio;
            _listarTipoAtendimentoExcelExporter = listarTipoAtendimentoExcelExporter;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_Atendimento_TiposAtendimento_Create, AppPermissions.Pages_Tenant_Cadastros_Atendimento_TiposAtendimento_Edit)]
        public async Task CriarOuEditar(TipoAtendimentoDto input)
        {
            try
            {
                var tipoAtendimento = input.MapTo<TipoAtendimento>();
                if (input.Id.Equals(0))
                {
                    await _tipoAtendimentoRepositorio.InsertOrUpdateAsync(tipoAtendimento);
                }
                else
                {
                    await _tipoAtendimentoRepositorio.UpdateAsync(tipoAtendimento);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(TipoAtendimentoDto input)
        {
            try
            {
                await _tipoAtendimentoRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<TipoAtendimentoDto>> Listar(ListarTiposAtendimentoInput input)
        {
            var contarTiposAtendimento = 0;
            List<TipoAtendimento> tiposAtendimento;
            List<TipoAtendimentoDto> tiposAtendimentoDtos = new List<TipoAtendimentoDto>();
            try
            {
                var query = _tipoAtendimentoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarTiposAtendimento = await query
                    .CountAsync();

                tiposAtendimento = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                tiposAtendimentoDtos = tiposAtendimento
                    .MapTo<List<TipoAtendimentoDto>>();

            return new PagedResultDto<TipoAtendimentoDto>(
                contarTiposAtendimento,
                tiposAtendimentoDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarTiposAtendimentoInput input)
        {
            try
            {
                var query = await Listar(input);

                var tiposAtendimentoDtos = query.Items;

                return _listarTipoAtendimentoExcelExporter.ExportToFile(tiposAtendimentoDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<TipoAtendimentoDto> Obter(long id)
        {
            try
            {
                var result = await _tipoAtendimentoRepositorio.GetAsync(id);
                var tipoAtendimento = result.MapTo<TipoAtendimentoDto>();
                return tipoAtendimento;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            var isInternacao = false;
            var isAmbulatorioEmergencia = false;
            bool.TryParse(dropdownInput.filtros[0], out isAmbulatorioEmergencia);
            bool.TryParse(dropdownInput.filtros[1], out isInternacao);
            try
            {
                //get com filtro
                var query = from p in _tipoAtendimentoRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m => 
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower()
                        .Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        .Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        .Replace("ã", "a").Replace("õ", "o")
                        .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        .Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        .Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        .Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                        .WhereIf(isAmbulatorioEmergencia, m => m.TabelaDominio.TipoTabelaDominio.Id == (long)EnumTipoTabelaDominio.TipoAtendimento)
                        .WhereIf(isInternacao, m => m.TabelaDominio.TipoTabelaDominio.Id == (long)EnumTipoTabelaDominio.TipoInternação)

                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
