﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Collections.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.AtendimentosLeitosMov;
using SW10.SWMANAGER.ClassesAplicacao.Services.AtendimentosLeitosMov.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.UnidadesOrganizacionais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.AtendimentosLeitosMov;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos
{
    public class LeitoAppService : SWMANAGERAppServiceBase, ILeitoAppService
    {
        #region Dependencias
        private readonly IRepository<Leito, long> _leitoRepository;
        private readonly IAtendimentoAppService _atendimentoService;
        private readonly ILeitoStatusAppService _leitoStatusService;
        private readonly IAtendimentoLeitoMovAppService _atendimentoLeitoMovService;
        private readonly IRepository<LeitoStatus, long> _leitoStatusRepository;
        private readonly IRepository<SW10.SWMANAGER.ClassesAplicacao.Atendimentos.Atendimento, long> _atendimentoRepository;
        private readonly IRepository<UnidadeOrganizacional, long> _UnidadeOrganizacioalRepository;
        private readonly IRepository<Paciente, long> _pacienteRepository;
        private readonly IRepository<SisPessoa, long> _sisPessoaRepository;
        private readonly IRepository<AtendimentoLeitoMov, long> _atdLeitoMovRepository;

        public LeitoAppService(
            IRepository<Leito, long> leitoRepository,
            IRepository<LeitoStatus, long> leitoStatusRepository,
            IAtendimentoAppService atendimentoService,
            ILeitoStatusAppService leitoStatusService,
            IAtendimentoLeitoMovAppService atendimentoLeitoMovService,
             IRepository<SW10.SWMANAGER.ClassesAplicacao.Atendimentos.Atendimento, long> atendimentoRepository,
             IRepository<UnidadeOrganizacional, long> UnidadeOrganizacioalRepository,
             IRepository<Paciente, long> pacienteRepository,
             IRepository<SisPessoa, long> sisPessoaRepository,
             IRepository<AtendimentoLeitoMov, long> atdLeitoMovRepository
            )
        {
            _leitoRepository = leitoRepository;
            _leitoStatusRepository = leitoStatusRepository;
            _atendimentoService = atendimentoService;
            _leitoStatusService = leitoStatusService;
            _atendimentoLeitoMovService = atendimentoLeitoMovService;
            _atendimentoRepository = atendimentoRepository;
            _UnidadeOrganizacioalRepository = UnidadeOrganizacioalRepository;
            _pacienteRepository = pacienteRepository;
            _sisPessoaRepository = sisPessoaRepository;
            _atdLeitoMovRepository = atdLeitoMovRepository;
        }
        #endregion

        public async Task CriarOuEditar(CriarOuEditarLeito input)
        {
            try
            {
                var leito = input.MapTo<Leito>();
                leito.DataAtualizacao = DateTime.Now;

                if (input.Id.Equals(0))
                {
                    await _leitoRepository.InsertAsync(leito);
                }
                else
                {
                    var leitoEntity = _leitoRepository.GetAll()
                                                .Where(w => w.Id == input.Id)
                                                .FirstOrDefault();

                    if (leitoEntity != null)
                    {
                        leitoEntity.Ativo = input.Ativo;
                        leitoEntity.Codigo = input.Codigo;
                        leitoEntity.DataAtualizacao = input.DataAtualizacao;
                        leitoEntity.Descricao = input.Descricao;
                        leitoEntity.Extra = input.Extra;
                        leitoEntity.HospitalDia = input.HospitalDia;
                        leitoEntity.LeitoAih = input.LeitoAih;
                        leitoEntity.LeitoStatusId = input.LeitoStatusId;
                        leitoEntity.Ramal = input.Ramal;
                        leitoEntity.Sexo = input.Sexo;
                        leitoEntity.TabelaItemSusId = input.TabelaItemSusId;
                        leitoEntity.TabelaItemTissId = input.TabelaItemTissId;
                        leitoEntity.TipoAcomodacaoId = input.TipoAcomodacaoId;
                        leitoEntity.UnidadeOrganizacionalId = input.UnidadeOrganizacionalId;


                        await _leitoRepository.UpdateAsync(leitoEntity);
                    }





                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarLeito input)
        {
            try
            {
                await _leitoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<LeitoDto>> Listar(ListarLeitosInput input)
        {
            var contarLeitos = 0;
            List<Leito> leitos;
            List<LeitoDto> leitosDtos = new List<LeitoDto>();
            long TipoAcoId = Convert.ToInt64(input.TipoAcomodacao);
            try
            {

                var query = _leitoRepository
                    .GetAll()
                   .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.UnidadeOrganizacional.OrganizationUnit)
                    .Include(m => m.TabelaDominio)
                    .Where(m => m.LeitoStatusId == 1 && (input.UnidadeId == null || m.UnidadeOrganizacionalId == input.UnidadeId))
                    .WhereIf(input.TipoAcomodacao != null, m => m.TipoAcomodacaoId == TipoAcoId)
                    ;

                contarLeitos = await query
                    .CountAsync();

                leitos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                leitosDtos = leitos
                    .MapTo<List<LeitoDto>>();

                return new PagedResultDto<LeitoDto>(
                contarLeitos,
                leitosDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<Leito>> ListarParaRelatorioMapaLeitos(long empresaId)
        {
            List<Leito> leitos;
            // List<LeitoDto> leitosDtos = new List<LeitoDto>();

            try
            {
                var query = _leitoRepository
                    .GetAll()
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.UnidadeOrganizacional.OrganizationUnit)
                    ;

                leitos = await query
                    .AsNoTracking()
                    //.OrderBy(input.Sorting)
                    //.PageBy(input)
                    .ToListAsync();

                // leitosDtos = leitos.MapTo<List<LeitoDto>>();

                return leitos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<LeitoComAtendimentoDto>> ListarTodos()
        {
            try
            {
                var leitosComAtendimento = new List<LeitoComAtendimentoDto>();
                var leitos = await _leitoRepository
                    .GetAll()
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.TabelaDominio)
                    .AsNoTracking()
                    .ToListAsync();

                var leitosDtos = leitos
                    .MapTo<List<LeitoDto>>();

                foreach (var leito in leitos)
                {
                    var leitoComAtendimento = new LeitoComAtendimentoDto();
                    leitoComAtendimento.Leito = leito.MapTo<LeitoDto>();
                    leitoComAtendimento.Id = leito.Id;
                    var atendimento = _atendimentoService.ObterPorLeito(leito.Id);

                    leitoComAtendimento.AtendimentoId = atendimento != null ? atendimento.Id : 0;
                    leitoComAtendimento.AtendimentoAtual = atendimento;
                    leitosComAtendimento.Add(leitoComAtendimento);
                }

                return new ListResultDto<LeitoComAtendimentoDto> { Items = leitosComAtendimento };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<LeitoComAtendimentoDto>> ListarPorUnidade(ListarLeitosInput input)
        {
            try
            {
                long uoId = Convert.ToInt64(input.UO);
                var leitosComAtendimento = new List<LeitoComAtendimentoDto>();
                var leitos = await _leitoRepository
                    .GetAll()
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.UnidadeOrganizacional.OrganizationUnit)
                    .Include(m => m.TabelaDominio)
                    .WhereIf(input.TipoAtendimento == "inter", m => m.UnidadeOrganizacional.IsInternacao == true)
                    .WhereIf(input.TipoAtendimento == "ambu", m => m.UnidadeOrganizacional.IsAmbulatorioEmergencia == true)
                    .WhereIf(input.UO != null, m => m.UnidadeOrganizacional.OrganizationUnit.Id == uoId || m.UnidadeOrganizacional.Id == uoId)

                    .AsNoTracking()

                    .OrderBy(input.Sorting)
                    .ToListAsync();

                var leitosDtos = new List<LeitoDto>();

                foreach (var item in leitos)
                {
                    leitosDtos.Add(LeitoDto.MapearFromCore(item));
                }

                foreach (var leito in leitosDtos)
                {
                    var leitoComAtendimento = new LeitoComAtendimentoDto();
                    leitoComAtendimento.Id = leito.Id;
                    var atendimento = _atendimentoService.ObterPorLeito(leito.Id);

                    if (atendimento != null)
                    {
                        var atendimentoLeitoMov = _atendimentoLeitoMovService.Obter(leito.Id, atendimento.Id);
                        leitoComAtendimento.AtendimentoLeitoMov = atendimentoLeitoMov;
                        leitoComAtendimento.AtendimentoLeitoMovId = atendimento.Id;
                        leitoComAtendimento.AtendimentoId = atendimento.Id;
                        leitoComAtendimento.AtendimentoAtual = atendimento;
                    }

                    leitoComAtendimento.Leito = leito;
                    leitosComAtendimento.Add(leitoComAtendimento);
                }

                return new ListResultDto<LeitoComAtendimentoDto> { Items = leitosComAtendimento };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<LeitoComAtendimentoDto>> ListarPorUnidadePaginado(ListarLeitosInput input)
        {
            try
            {
                var leitosComAtendimento = new List<LeitoComAtendimentoDto>();

                //var query = from a in _atendimentoRepository.GetAll()
                //            join l in _leitoRepository.GetAll()
                var query = from l in _leitoRepository.GetAll()
                            join a in _atendimentoRepository
                                        .GetAll()
                                        .WhereIf(input.EmpresaId.HasValue, e => e.EmpresaId == input.EmpresaId.Value)
                                        .Where(w => !w.DataAlta.HasValue)

                            on l.Id equals a.LeitoId
                            into leitoAtendimento
                            from leitosAtendimento in leitoAtendimento.DefaultIfEmpty()

                            join status in _leitoStatusRepository.GetAll()
                            on l.LeitoStatusId equals status.Id

                            join UO in _UnidadeOrganizacioalRepository.GetAll()
                            on l.UnidadeOrganizacionalId equals UO.Id

                            join paciente in _pacienteRepository.GetAll()
                            on leitosAtendimento.PacienteId equals paciente.Id

                            into pacienteAtendimento
                            from pacientesAtendimentos in pacienteAtendimento.DefaultIfEmpty()

                            join sisPessoa in _sisPessoaRepository.GetAll()
                            on pacientesAtendimentos.SisPessoaId equals sisPessoa.Id

                            where l.UnidadeOrganizacional.IsInternacao
                               && (string.IsNullOrEmpty(input.UO) || l.UnidadeOrganizacional.Id.ToString() == input.UO)

                               && (string.IsNullOrEmpty(input.Filtro)
                                || l.Codigo.Contains(input.Filtro)
                                || l.Descricao.Contains(input.Filtro)
                                || l.UnidadeOrganizacional.Localizacao.Contains(input.Filtro)
                                || l.LeitoStatus.Descricao.Contains(input.Filtro)

                                || pacientesAtendimentos.NomeCompleto.Contains(input.Filtro)

                                )

                            select new
                            {
                                leito = l,
                                status = status,
                                unidade = UO,
                                atendimento = leitosAtendimento,
                                pacienteAtendimento = pacientesAtendimentos,
                                pessoa = sisPessoa,
                                id = l.Id
                            };



                //.Include(m => m.LeitoStatus)
                //.Include(m => m.TipoAcomodacao)
                //.Include(m => m.UnidadeOrganizacional)
                //.Include(m => m.UnidadeOrganizacional.OrganizationUnit)
                //.Include(m => m.TabelaDominio)
                //   .WhereIf(input.TipoAtendimento == "inter", m => m.UnidadeOrganizacional.IsInternacao == true)
                //   .WhereIf(input.TipoAtendimento == "ambu", m => m.UnidadeOrganizacional.IsAmbulatorioEmergencia == true)
                //   .WhereIf(!string.IsNullOrEmpty(input.UO), m => m.UnidadeOrganizacional.OrganizationUnit.Id.ToString() == input.UO || m.UnidadeOrganizacional.Id.ToString() == input.UO);


                //var q = from l in query
                //        join a in _atendimentoRepository.GetAll()
                //        on l.Id equals a.LeitoId
                //         into leitoAtendimento
                //        from leitosAtendimento in leitoAtendimento.DefaultIfEmpty()


                //        ;




                var atend = _atendimentoRepository
                    .GetAll()
                    .WhereIf(input.EmpresaId.HasValue, m => m.EmpresaId == input.EmpresaId.Value);

                var queryLeitos = from l in _leitoRepository.GetAll()
                                  join status in _leitoStatusRepository.GetAll()
                                  on l.LeitoStatusId equals status.Id

                                  join UO in _UnidadeOrganizacioalRepository.GetAll()
                                  on l.UnidadeOrganizacionalId equals UO.Id

                                  where (from a in atend
                                         where a.IsInternacao
                                         && a.DataAlta == null
                                         && a.LeitoId == l.Id
                                         select a.Id
                                            ).Count() == 0
                                && (string.IsNullOrEmpty(input.UO) || l.UnidadeOrganizacionalId.ToString() == input.UO)
                                 && (string.IsNullOrEmpty(input.Filtro)
                                || l.Codigo.Contains(input.Filtro)
                                || l.Descricao.Contains(input.Filtro)
                                || l.UnidadeOrganizacional.Localizacao.Contains(input.Filtro)
                                || l.LeitoStatus.Descricao.Contains(input.Filtro)
                                )

                                  select new
                                  {
                                      leito = l,
                                      status = status,
                                      unidade = UO,

                                  };

                var leitosNaoOcupados = queryLeitos
                 .AsNoTracking()
                  .OrderBy("Leito.Descricao");
                // .PageBy(input)
                //.ToList();




                var leitos = await query
                .AsNoTracking()
                 .OrderBy("Leito.Id")
                // .PageBy(input)
                 .ToListAsync();



                var leitosDtos = new List<LeitoDto>();


                foreach (var leitoMov in leitos)
                {
                    var leitoComAtendimento = new LeitoComAtendimentoDto();
                    leitoComAtendimento.AtendimentoLeitoMov = new AtendimentoLeitoMovDto();


                    var atdLeitosMovs = _atdLeitoMovRepository.GetAll()
                        .Where(x => x.LeitoId == leitoMov.id);

                    if (atdLeitosMovs != null && atdLeitosMovs.Count() > 0)
                    {
                        var atdLeitoMovMaisRecente = atdLeitosMovs.OrderBy(d => d.DataInicial).FirstOrDefault();
                    }



                    leitoComAtendimento.Leito = LeitoDto.MapearFromCore(leitoMov.leito);
                    leitoComAtendimento.Leito.LeitoStatus = new LeitoStatusDto { Id = leitoMov.status.Id, Descricao = leitoMov.status.Descricao, Cor = leitoMov.status.Cor };
                    leitoComAtendimento.Leito.UnidadeOrganizacional = UnidadeOrganizacionalDto.MapearFromCore(leitoMov.unidade);


                    leitoComAtendimento.AtendimentoLeitoMov.Leito = LeitoDto.MapearFromCore(leitoMov.leito);
                    leitoComAtendimento.AtendimentoLeitoMov.Leito.LeitoStatus = new LeitoStatusDto { Id = leitoMov.status.Id };
                    leitoComAtendimento.AtendimentoLeitoMov.LeitoId = leitoMov?.leito?.Id;


                    if (leitoMov.atendimento != null && leitoMov.atendimento.Id > 0)
                    {
                        leitoComAtendimento.AtendimentoId = leitoMov?.atendimento?.Id;
                        leitoComAtendimento.AtendimentoAtual = SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto.AtendimentoDto.MapearFromCore(leitoMov?.atendimento);
                        leitoComAtendimento.AtendimentoAtual.Paciente = new Pacientes.Dto.PacienteDto { SisPessoa = new Pessoas.Dto.SisPessoaDto { NomeCompleto = leitoMov?.pessoa.NomeCompleto } };

                        leitoComAtendimento.AtendimentoLeitoMov.Atendimento = SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto.AtendimentoDto.MapearFromCore(leitoMov?.atendimento);
                        leitoComAtendimento.AtendimentoLeitoMov.AtendimentoId = leitoMov?.atendimento?.Id;

                        if (leitoMov.atendimento != null && leitoMov.atendimento.DataRegistro != null)
                        {
                            leitoComAtendimento.AtendimentoAtual.DataRegistro = (DateTime)leitoMov.atendimento.DataRegistro;
                        }
                    }






                    // var atendimento = _atendimentoService.ObterPorLeito(leito.Id);

                    //  if (atendimento != null)
                    ///  {
                    //  var atendimentoLeitoMov = _atendimentoLeitoMovService.Obter(leito.Id, atendimento.Id);
                    //leitoComAtendimento.AtendimentoLeitoMov = atendimentoLeitoMov;
                    //leitoComAtendimento.AtendimentoLeitoMovId = atendimento.Id;
                    //leitoComAtendimento.AtendimentoId = atendimento.Id;
                    //leitoComAtendimento.AtendimentoAtual = atendimento;
                    //leitoComAtendimento.AtendimentoLeitoMov.Atendimento = atendimento;
                    //leitoComAtendimento.AtendimentoLeitoMov.AtendimentoId = atendimento.Id;
                    //leitoComAtendimento.AtendimentoAtual.DataRegistro = atendimento.DataRegistro;
                    //leitoComAtendimento.AtendimentoLeitoMov.Leito = leito;
                    //leitoComAtendimento.AtendimentoLeitoMov.LeitoId = leito.Id;
                    //  }

                    // leitoComAtendimento.Leito = LeitoDto.MapearFromCore(leito.leito);
                    leitosComAtendimento.Add(leitoComAtendimento);
                }
                if ((!input.EmpresaId.HasValue && !input.SomenteInternados))
                {
                    foreach (var leito in leitosNaoOcupados)
                    {
                        var leitoComAtendimento = new LeitoComAtendimentoDto();
                        leitoComAtendimento.AtendimentoLeitoMov = new AtendimentoLeitoMovDto();
                        leitoComAtendimento.Leito = LeitoDto.MapearFromCore(leito.leito);
                        leitoComAtendimento.Leito.LeitoStatus = new LeitoStatusDto { Id = leito.status.Id, Descricao = leito.status.Descricao, Cor = leito.status.Cor };
                        leitoComAtendimento.Leito.UnidadeOrganizacional = UnidadeOrganizacionalDto.MapearFromCore(leito.unidade);

                        leitoComAtendimento.AtendimentoLeitoMov.Leito = LeitoDto.MapearFromCore(leito.leito);
                        leitoComAtendimento.AtendimentoLeitoMov.Leito.LeitoStatus = new LeitoStatusDto { Id = leito.status.Id };
                        leitoComAtendimento.AtendimentoLeitoMov.LeitoId = leito?.leito?.Id;

                        leitosComAtendimento.Add(leitoComAtendimento);
                    }
                }

                return new PagedResultDto<LeitoComAtendimentoDto>(
                    query.Count() + (input.EmpresaId.HasValue || input.SomenteInternados ? 0 : queryLeitos.Count()),
                    leitosComAtendimento.OrderBy(o => o.Leito.Descricao).ToList()
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ICollection<LeitoDto>> ListarPorUnidadeParaDrop(long? id)
        {
            List<Leito> leitos;
            List<LeitoDto> leitosDtos = new List<LeitoDto>();
            try
            {
                IQueryable<Leito> query;

                if (id.HasValue)
                {
                    query = from m in _leitoRepository.GetAll()
                            where m.UnidadeOrganizacionalId == id && m.LeitoStatusId == 1
                            select m;
                }
                else
                {
                    query = from m in _leitoRepository.GetAll()
                            where m.LeitoStatusId == 1
                            select m;
                }

                leitos = await query
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.TabelaDominio)
                    .Where(m => m.LeitoStatusId == 1)
                    .AsNoTracking()
                    .ToListAsync();

                leitosDtos = leitos
                    .MapTo<List<LeitoDto>>();

                return leitosDtos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<LeitoDto>> ListarPorUnidadeParaDrop2(long id)
        {
            List<Leito> leitos;
            List<LeitoDto> leitosDtos = new List<LeitoDto>();
            try
            {
                var query = from m in _leitoRepository.GetAll()
                            where m.UnidadeOrganizacionalId == id
                            select m;

                leitos = await query
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.TabelaDominio)
                    .Where(m => m.LeitoStatusId == 1)
                    .AsNoTracking()
                    .ToListAsync();

                leitosDtos = leitos
                    .MapTo<List<LeitoDto>>();
                // return leitosDtos;
                return new ListResultDto<LeitoDto> { Items = leitosDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<Leito>> ListarAutoComplete(string term)
        {
            try
            {
                return await _leitoRepository
                    .GetAll()
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.TabelaDominio)
                    .WhereIf(!term.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(term.ToUpper())
                    )
                    //.AsNoTracking()
                    .ToListAsync();

                //var leitosDtos = leitos
                //    .MapTo<List<LeitoDto>>();

                //return new ListResultDto<LeitoDto> { Items = leitosDtos };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<LeitoDto> Obter(long id)
        {
            try
            {
                var query = await _leitoRepository
                    .GetAll()
                    .Include(m => m.LeitoStatus)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.UnidadeOrganizacional)
                    .Include(m => m.TabelaDominio)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var leito = query.MapTo<LeitoDto>();

                return leito;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public void OcuparLeito(long? leitoId)
        {
            try
            {
                var leito = _leitoRepository.Get(leitoId.Value);
                leito.DataAtualizacao = DateTime.Now;
                leito.LeitoStatus = _leitoStatusRepository.FirstOrDefault(l => l.Descricao == "Ocupado");
                _leitoRepository.UpdateAsync(leito);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public void DesocuparLeito(long? leitoId)
        {
            try
            {
                var leito = _leitoRepository.Get(leitoId.Value);
                leito.DataAtualizacao = DateTime.Now;
                leito.LeitoStatus = _leitoStatusRepository.FirstOrDefault(l => l.Descricao == "Vago");
                _leitoRepository.UpdateAsync(leito);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _leitoRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.DescricaoResumida.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarLeitoVagoDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _leitoRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.DescricaoResumida.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                        .Where(w=> w.LeitoStatusId == 1 && w.UnidadeOrganizacional.IsInternacao)//leito Vago
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
