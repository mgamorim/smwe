﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos
{
    public class LeitoStatusAppService : SWMANAGERAppServiceBase, ILeitoStatusAppService
    {
        private readonly IRepository<LeitoStatus, long> _motivoAltaRepository;
    //    private readonly IListarLeitosStatusExcelExporter _listarLeitosStatusExcelExporter;

        public LeitoStatusAppService(
            IRepository<LeitoStatus, long> motivoAltaRepository
            // ,
       //     IListarLeitosStatusExcelExporter listarLeitosStatusExcelExporter
       )
        {
            _motivoAltaRepository = motivoAltaRepository;
        //    _listarLeitosStatusExcelExporter = listarLeitosStatusExcelExporter;
        }

        public async Task CriarOuEditar(CriarOuEditarLeitoStatus input)
        {
            try
            {
                var LeitoStatus = input.MapTo<LeitoStatus>();
                if (input.Id.Equals(0))
                {
                    await _motivoAltaRepository.InsertAsync(LeitoStatus);
                }
                else
                {
                    var leitoStatusEntity = _motivoAltaRepository.GetAll()
                                                                 .Where(w => w.Id == input.Id)
                                                                 .FirstOrDefault();

                    if(leitoStatusEntity!=null)
                    {
                        leitoStatusEntity.Codigo = input.Codigo;
                        leitoStatusEntity.Descricao = input.Descricao;
                        leitoStatusEntity.Cor = input.Cor;
                        leitoStatusEntity.IsBloqueioAtendimento = input.IsBloqueioAtendimento;

                        await _motivoAltaRepository.UpdateAsync(leitoStatusEntity);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarLeitoStatus input)
        {
            try
            {
                await _motivoAltaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<LeitoStatusDto>> Listar(ListarLeitosStatusInput input)
        {
            var contarLeitosStatus = 0;
            List<LeitoStatus> motivosAlta;
            List<LeitoStatusDto> motivosAltaDtos = new List<LeitoStatusDto>();
            try
            {
                var query = _motivoAltaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarLeitosStatus = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<LeitoStatusDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<LeitoStatusDto>(
                contarLeitosStatus,
                motivosAltaDtos
                );
        }
        
        //public async Task<FileDto> ListarParaExcel(ListarLeitosStatusInput input)
        //{
        //    try
        //    {
        //        var result = await Listar(input);
        //        var motivosAlta = result.Items;
        //        return _listarLeitosStatusExcelExporter.ExportToFile(motivosAlta.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroExportar"));
        //    }
        //}
        
        public async Task<CriarOuEditarLeitoStatus> Obter(long id)
        {
            try
            {
                var query = await _motivoAltaRepository
                    .GetAsync(id);

                var motivoAlta = query
                    .MapTo<CriarOuEditarLeitoStatus>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }
        
    }
}
