﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos
{
    public class LeitoCaracteristicaAppService : SWMANAGERAppServiceBase, ILeitoCaracteristicaAppService
    {
        private readonly IRepository<LeitoCaracteristica, long> _motivoAltaRepository;
        //    private readonly IListarLeitoCaracteristicasExcelExporter _listarLeitoCaracteristicasExcelExporter;

        public LeitoCaracteristicaAppService(
            IRepository<LeitoCaracteristica, long> motivoAltaRepository
       // ,
       //     IListarLeitoCaracteristicasExcelExporter listarLeitoCaracteristicasExcelExporter
       )
        {
            _motivoAltaRepository = motivoAltaRepository;
            //    _listarLeitoCaracteristicasExcelExporter = listarLeitoCaracteristicasExcelExporter;
        }

        public async Task CriarOuEditar(CriarOuEditarLeitoCaracteristica input)
        {
            try
            {
                var LeitoCaracteristica = input.MapTo<LeitoCaracteristica>();
                if (input.Id.Equals(0))
                {
                    await _motivoAltaRepository.InsertAsync(LeitoCaracteristica);
                }
                else
                {
                    await _motivoAltaRepository.UpdateAsync(LeitoCaracteristica);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarLeitoCaracteristica input)
        {
            try
            {
                await _motivoAltaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<LeitoCaracteristicaDto>> Listar(ListarLeitoCaracteristicasInput input)
        {
            var contarLeitoCaracteristicas = 0;
            List<LeitoCaracteristica> motivosAlta;
            List<LeitoCaracteristicaDto> motivosAltaDtos = new List<LeitoCaracteristicaDto>();
            try
            {
                var query = _motivoAltaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarLeitoCaracteristicas = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<LeitoCaracteristicaDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<LeitoCaracteristicaDto>(
                contarLeitoCaracteristicas,
                motivosAltaDtos
                );
        }

        //public async Task<FileDto> ListarParaExcel(ListarLeitoCaracteristicasInput input)
        //{
        //    try
        //    {
        //        var result = await Listar(input);
        //        var motivosAlta = result.Items;
        //        return _listarLeitoCaracteristicasExcelExporter.ExportToFile(motivosAlta.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroExportar"));
        //    }
        //}

        public async Task<CriarOuEditarLeitoCaracteristica> Obter(long id)
        {
            try
            {
                var query = await _motivoAltaRepository
                    .GetAsync(id);

                var motivoAlta = query
                    .MapTo<CriarOuEditarLeitoCaracteristica>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

    }
}
