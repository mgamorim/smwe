﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos
{
    public class LeitoServicoAppService : SWMANAGERAppServiceBase, ILeitoServicoAppService
    {
        private readonly IRepository<LeitoServico, long> _motivoAltaRepository;
        //    private readonly IListarLeitoServicosExcelExporter _listarLeitoServicosExcelExporter;

        public LeitoServicoAppService(
            IRepository<LeitoServico, long> motivoAltaRepository
       // ,
       //     IListarLeitoServicosExcelExporter listarLeitoServicosExcelExporter
       )
        {
            _motivoAltaRepository = motivoAltaRepository;
            //    _listarLeitoServicosExcelExporter = listarLeitoServicosExcelExporter;
        }

        public async Task CriarOuEditar(CriarOuEditarLeitoServico input)
        {
            try
            {
                var LeitoServico = input.MapTo<LeitoServico>();
                if (input.Id.Equals(0))
                {
                    await _motivoAltaRepository.InsertAsync(LeitoServico);
                }
                else
                {
                    await _motivoAltaRepository.UpdateAsync(LeitoServico);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarLeitoServico input)
        {
            try
            {
                await _motivoAltaRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<LeitoServicoDto>> Listar(ListarLeitoServicosInput input)
        {
            var contarLeitoServicos = 0;
            List<LeitoServico> motivosAlta;
            List<LeitoServicoDto> motivosAltaDtos = new List<LeitoServicoDto>();
            try
            {
                var query = _motivoAltaRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarLeitoServicos = await query
                    .CountAsync();

                motivosAlta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                motivosAltaDtos = motivosAlta
                    .MapTo<List<LeitoServicoDto>>();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<LeitoServicoDto>(
                contarLeitoServicos,
                motivosAltaDtos
                );
        }

        //public async Task<FileDto> ListarParaExcel(ListarLeitoServicosInput input)
        //{
        //    try
        //    {
        //        var result = await Listar(input);
        //        var motivosAlta = result.Items;
        //        return _listarLeitoServicosExcelExporter.ExportToFile(motivosAlta.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroExportar"));
        //    }
        //}

        public async Task<CriarOuEditarLeitoServico> Obter(long id)
        {
            try
            {
                var query = await _motivoAltaRepository
                    .GetAsync(id);

                var motivoAlta = query
                    .MapTo<CriarOuEditarLeitoServico>();

                return motivoAlta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

    }
}
