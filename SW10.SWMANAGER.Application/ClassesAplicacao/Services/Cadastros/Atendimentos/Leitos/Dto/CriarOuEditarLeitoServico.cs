﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos.Dto
{
    [AutoMap(typeof(LeitoServico))]
    public class CriarOuEditarLeitoServico : CamposPadraoCRUDDto
    {
        [StringLength(10)]
        public string Codigo { get; set; }

        [StringLength(255)]
        public string Descricao { get; set; }

        [StringLength(10)]
        public string Ramal { get; set; }

        // FALTA A PROPRIEDADE 'setoresId'
    }
}