﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos.Dto
{
    [AutoMap(typeof(LeitoStatus))]
    public class LeitoStatusDto : CamposPadraoCRUDDto
	{
		[StringLength(10)]
		public string Codigo { get; set; }

		[StringLength(255)]
		public string Descricao { get; set; }

        [StringLength(7)]
        public string Cor { get; set; }

        public bool IsBloqueioAtendimento { get; set; }
    }
}
