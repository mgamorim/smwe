﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public interface ITipoFreteAppService : IApplicationService
    {
        Task<ListResultDto<TipoFreteDto>> ListarTodos();
        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
        Task<TipoFreteDto> Obter(long id);
    }
}
