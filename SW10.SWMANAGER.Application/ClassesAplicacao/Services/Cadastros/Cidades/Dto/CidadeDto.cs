﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Cidades;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto
{
    [AutoMap(typeof(Cidade))]
    public class CidadeDto : CamposPadraoCRUDDto
    {
        public string Nome { get; set; }

        public virtual EstadoDto Estado { get; set; }
        public long EstadoId { get; set; }

        public bool Capital { get; set; }
    }
}
