﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposUnidade.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposUnidade.Exporting
{
    public interface IListarTipoUnidadeExcelExporter
    {
        FileDto ExportToFile(List<TipoUnidadeDto> tipoUnidadeDtos);
    }
}
