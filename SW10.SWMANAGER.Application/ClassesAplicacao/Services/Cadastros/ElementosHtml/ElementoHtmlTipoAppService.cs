﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ElementosHtml;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ElementosHtml.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ElementosHtml
{
    public class ElementoHtmlTipoAppService : SWMANAGERAppServiceBase, IElementoHtmlTipoAppService
    {
        private readonly IRepository<ElementoHtmlTipo, long> _elementoHtmlRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ElementoHtmlTipoAppService(
            IRepository<ElementoHtmlTipo, long> elementoHtmlRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _elementoHtmlRepositorio = elementoHtmlRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }
        [UnitOfWork]
        public async Task<ElementoHtmlTipoDto> CriarOuEditar(ElementoHtmlTipoDto input)
        {
            try
            {
                var elementoHtml = input.MapTo<ElementoHtmlTipo>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _elementoHtmlRepositorio.InsertAndGetIdAsync(elementoHtml);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        elementoHtml = await _elementoHtmlRepositorio.UpdateAsync(elementoHtml);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }
        [UnitOfWork]
        public async Task Excluir(ElementoHtmlTipoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _elementoHtmlRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<ElementoHtmlTipoDto>> Listar(ListarInput input)
        {
            var contarElementoHtml = 0;
            List<ElementoHtmlTipo> elementoHtml;
            List<ElementoHtmlTipoDto> ElementoHtmlDtos = new List<ElementoHtmlTipoDto>();
            try
            {
                var query = _elementoHtmlRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarElementoHtml = await query
                    .CountAsync();

                elementoHtml = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                ElementoHtmlDtos = elementoHtml
                    .MapTo<List<ElementoHtmlTipoDto>>();

                return new PagedResultDto<ElementoHtmlTipoDto>(
                    contarElementoHtml,
                    ElementoHtmlDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ElementoHtmlTipoDto> Obter(long id)
        {
            try
            {
                var elementoHtmlPrincipalDto = new ElementoHtmlTipoDto();
                var query = _elementoHtmlRepositorio
                    .GetAll()
                    .Where(m => m.Id == id);
                    
                var dtos = await query.FirstOrDefaultAsync();

                var result = dtos.MapTo<ElementoHtmlTipoDto>();

                return result;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<ElementoHtmlTipoDto>> ListarTodos()
        {
            try
            {
                var query = _elementoHtmlRepositorio
                    .GetAll();

                var elementoHtml = await query
                    .AsNoTracking()
                    .ToListAsync();

                var elementosHtmlDto = elementoHtml
                    .MapTo<List<ElementoHtmlTipoDto>>();

                return new ListResultDto<ElementoHtmlTipoDto>
                {
                    Items = elementosHtmlDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<ElementoHtmlTipoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _elementoHtmlRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var elementoHtml = await query
                    .AsNoTracking()
                    .ToListAsync();

                var elementosHtmlDto = elementoHtml
                    .MapTo<List<ElementoHtmlTipoDto>>();

                return new ListResultDto<ElementoHtmlTipoDto>
                {
                    Items = elementosHtmlDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _elementoHtmlRepositorio);
            //return await ListarDropdownLambda(dropdownInput, _elementoHtmlRepositorio, m => m.IsElementoHtmlPrincipal, s => new DropdownItems { id = s.Id, text = string.Concat(s.Codigo, " - ", s.Descricao) }, m => m.Descricao);
        }

        public Task<FileDto> ListarParaExcel(ListarInput input)
        {
            throw new NotImplementedException();
        }
    }
}
