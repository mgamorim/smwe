﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ElementosHtml;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ElementosHtml.Dto
{
    [AutoMap(typeof(ElementoHtml))]
    public class ElementoHtmlDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public long? ElementoHtmlTipoId { get; set; }

        public int Tamanho { get; set; }

        public bool IsRequerido { get; set; }

        public bool IsDesativado { get; set; }

        public virtual ElementoHtmlTipoDto ElementoHtmlTipo { get; set; }

    }
}
