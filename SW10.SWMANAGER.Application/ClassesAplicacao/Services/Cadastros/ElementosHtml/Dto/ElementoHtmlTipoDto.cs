﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ElementosHtml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ElementosHtml.Dto
{
    [AutoMap(typeof(ElementoHtmlTipo))]
    public class ElementoHtmlTipoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public string HtmlHelper { get; set; }
    }
}
