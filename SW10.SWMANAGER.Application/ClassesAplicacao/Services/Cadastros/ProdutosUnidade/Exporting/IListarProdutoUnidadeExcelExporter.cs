﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade.Exporting
{
    public interface IListarProdutoUnidadeExcelExporter
    {
        FileDto ExportToFile(List<ProdutoUnidadeDto> produtoUnidadeDtos);
    }
}
