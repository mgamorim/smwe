﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade.Dto;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade
{
	public interface IProdutoUnidadeAppService : IApplicationService
    {
        //ListResultDto<TipoAtendimentoDto> GetTiposAtendimento(GetTiposAtendimentoInput input);
        Task<PagedResultDto<ProdutoUnidadeDto>> Listar(ListarProdutosUnidadeInput input);

        Task CriarOuEditar(CriarOuEditarProdutoUnidade input);

        Task Excluir(CriarOuEditarProdutoUnidade input);

        Task<ProdutoUnidadeDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarProdutosUnidadeInput input);

        Task<ResultDropdownList> ListarUnidadePorProdutoDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarUnidadeComprasPorProdutoDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarUnidadePorReferenciaProdutoDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarUnidadeConsumoProdutoDropdown(DropdownInput dropdownInput);
    }
}
