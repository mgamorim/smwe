﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposTipoTabelaDominio.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposTipoTabelaDominio.Exporting
{
	public interface IListarGrupoTipoTabelaDominioExcelExporter
	{
		FileDto ExportToFile(List<GrupoTipoTabelaDominioDto> tipoTabelaDominioDtos);
	}
}