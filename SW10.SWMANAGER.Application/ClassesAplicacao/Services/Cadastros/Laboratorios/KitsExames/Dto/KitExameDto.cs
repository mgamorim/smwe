﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Exames.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Kits.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.KitsExames.Dto
{
    [AutoMap(typeof(KitExame))]
    public class KitExameDto : CamposPadraoCRUDDto
    {
        public long? KitId { get; set; }
        public long? ExameId { get; set; }

        public bool IsLiberaKit { get; set; }

        public KitDto Kit { get; set; }

        //public ICollection<KitExameItem> KitExameItens { get; set; }

        public ExameDto Exame { get; set; }
    }
}
