﻿using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Cabecalho.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Cabecalho
{
    public interface ICabecalhoAppService : IApplicationService
    {
        CabecalhoDto Obter();

        DefaultReturn<CabecalhoDto> Editar(CabecalhoDto cabecalhoDto);
    }
}
