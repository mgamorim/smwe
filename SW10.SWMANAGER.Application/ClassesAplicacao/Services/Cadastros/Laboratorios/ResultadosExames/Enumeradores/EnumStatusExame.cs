﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Enumeradores
{
    public enum EnumStatusExame
    {
        Inicial=1,
        Pendente=2,
        Digitado=3,
        Conferido=4
    }
}
