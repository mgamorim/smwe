﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Formatas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Materiais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Tabelas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.KitsExames;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Exames.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboatorios.ResultadosExames
{
    public class ResultadoExameAppService : SWMANAGERAppServiceBase, IResultadoExameAppService
    {
        private readonly IListarResultadoExamesExcelExporter _listarResultadoExamesExcelExporter;
        private readonly IRepository<ResultadoExame, long> _resultadoExameRepositorio;
        private readonly IRepository<ExameStatus, long> _exameStatusRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        // private readonly IExameAppService _exameAppService;
        private readonly IFaturamentoContaItemAppService _fatContaItemAppService;
        private readonly IFormataAppService _formataAppService;
        private readonly IMaterialAppService _materialAppService;
        private readonly ITabelaAppService _tabelaAppService;
        private readonly IKitExameAppService _kitExameAppService;
        private readonly IRepository<FaturamentoItem, long> _faturamentoItemRepository;
        private readonly IRepository<RegistroArquivo, long> _registroArquivoRepository;

        public ResultadoExameAppService(
        IRepository<ResultadoExame, long> resultadoExameRepositorio,
        IRepository<ExameStatus, long> exameStatusRepositorio,
            IListarResultadoExamesExcelExporter listarResultadoExamesExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            //   IExameAppService exameAppService,
            IFaturamentoContaItemAppService fatContaItemAppService,
            IFormataAppService formataAppService,
            IMaterialAppService materialAppService,
            ITabelaAppService tabelaAppService,
            IKitExameAppService kitExameAppService,
            IRepository<FaturamentoItem, long> faturamentoItemRepository,
            IRepository<RegistroArquivo, long> registroArquivoRepository
            )
        {
            _resultadoExameRepositorio = resultadoExameRepositorio;
            _exameStatusRepositorio = exameStatusRepositorio;
            _listarResultadoExamesExcelExporter = listarResultadoExamesExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            // _exameAppService = exameAppService;
            _fatContaItemAppService = fatContaItemAppService;
            _formataAppService = formataAppService;
            _materialAppService = materialAppService;
            _tabelaAppService = tabelaAppService;
            _kitExameAppService = kitExameAppService;
            _faturamentoItemRepository = faturamentoItemRepository;
            _registroArquivoRepository = registroArquivoRepository;
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_DominioTiss_TiposTabelaDominio_Create, AppPermissions.Pages_Tenant_Cadastros_DominioTiss_TiposTabelaDominio_Edit)]
        public async Task CriarOuEditar(ResultadoExameDto input)
        {
            try
            {
                var resultadoExame = new ResultadoExame //input.MapTo<ResultadoExame>();
                {
                    Codigo = input.Codigo,
                    CreationTime = input.CreationTime,
                    CreatorUserId = input.CreatorUserId,
                    DataAlteracao = input.DataAlteracao,
                    DataAlteradoExame = input.DataAlteradoExame,
                    DataConferidoExame = input.DataConferidoExame,
                    DataDigitadoExame = input.DataDigitadoExame,
                    DataEnvioEmail = input.DataEnvioEmail,
                    DataExclusao = input.DataExclusao,
                    DataImporta = input.DataImporta,
                    DataImpressoExame = input.DataImpressoExame,
                    DataImpSolicita = input.DataImpSolicita,
                    DataInclusao = input.DataInclusao,
                    DataPendenteExame = input.DataPendenteExame,
                    DataUsuarioCienteExame = input.DataUsuarioCienteExame,
                    DeleterUserId = input.DeleterUserId,
                    DeletionTime = input.DeletionTime,
                    Descricao = input.Descricao,
                    FaturamentoItem = null,
                    FaturamentoItemId = input.FaturamentoItemId,
                    FaturamentoContaItem = null,
                    FaturamentoContaItemId = input.FaturamentoContaItemId,
                    FormataId = input.FormataId,
                    Id = input.Id,
                    ImpResultado = input.ImpResultado,
                    IsCienteExame = input.IsCienteExame,
                    IsDeleted = input.IsDeleted,
                    IsImprime = input.IsImprime,
                    IsSergioFranco = input.IsSergioFranco,
                    IsSigiloso = input.IsSigiloso,
                    IsSistema = input.IsSistema,
                    KitExameId = input.KitExameId,
                    LastModificationTime = input.LastModificationTime,
                    LastModifierUserId = input.LastModifierUserId,
                    MaqImpSolicita = input.MaqImpSolicita,
                    MaterialId = input.MaterialId,
                    Mneumonico = input.Mneumonico,
                    MotivoPendenteExame = input.MotivoPendenteExame,
                    Observacao = input.Observacao,
                    Quantidade = input.Quantidade,
                    ResultadoId = input.ResultadoId,
                    Seq = input.Seq,
                    TabelaId = input.TabelaId,
                    UsuarioAlteradoExameId = input.UsuarioAlteradoExameId,
                    UsuarioCienteExameId = input.UsuarioCienteExameId,
                    UsuarioConferidoExameId = input.UsuarioConferidoExameId,
                    UsuarioDigitadoExameId = input.UsuarioDigitadoExameId,
                    UsuarioImpressoExameId = input.UsuarioImpressoExameId,
                    UsuarioImpSolicitaId = input.UsuarioImpSolicitaId,
                    UsuarioIncluidoExameId = input.UsuarioIncluidoExameId,
                    UsuarioPendenteExameId = input.UsuarioPendenteExameId,
                    VolumeMaterial = input.VolumeMaterial,
                    ExameStatusId = input.ExameStatusId
                };

                if (input.Id.Equals(0))
                {
                    //  using (var unitOfWork = _unitOfWorkManager.Begin())
                    // {
                    resultadoExame.ExameStatusId = (long)EnumStatusExame.Inicial;
                    resultadoExame.Id = await _resultadoExameRepositorio.InsertAndGetIdAsync(resultadoExame);
                    //   unitOfWork.Complete();
                    //  unitOfWork.Dispose();
                    _unitOfWorkManager.Current.SaveChanges();

                    input.Id = resultadoExame.Id;
                    // }
                }
                else
                {
                    var ori = await _resultadoExameRepositorio.GetAsync(input.Id);
                    ori.Codigo = input.Codigo;
                    ori.CreationTime = input.CreationTime;
                    ori.CreatorUserId = input.CreatorUserId;
                    ori.DataAlteracao = input.DataAlteracao;
                    ori.DataAlteradoExame = input.DataAlteradoExame;
                    ori.DataConferidoExame = input.DataConferidoExame;
                    ori.DataDigitadoExame = input.DataDigitadoExame;
                    ori.DataEnvioEmail = input.DataEnvioEmail;
                    ori.DataExclusao = input.DataExclusao;
                    ori.DataImporta = input.DataImporta;
                    ori.DataImpressoExame = input.DataImpressoExame;
                    ori.DataImpSolicita = input.DataImpSolicita;
                    ori.DataInclusao = input.DataInclusao;
                    ori.DataPendenteExame = input.DataPendenteExame;
                    ori.DataUsuarioCienteExame = input.DataUsuarioCienteExame;
                    ori.DeleterUserId = input.DeleterUserId;
                    ori.DeletionTime = input.DeletionTime;
                    ori.Descricao = input.Descricao;
                    //ori.FaturamentoItem = null;
                    ori.FaturamentoItemId = input.FaturamentoItemId;
                    //ori.FaturamentoContaItem = null;
                    ori.FaturamentoContaItemId = input.FaturamentoContaItemId;
                    ori.FormataId = input.FormataId;
                    ori.ImpResultado = input.ImpResultado;
                    ori.IsCienteExame = input.IsCienteExame;
                    ori.IsDeleted = input.IsDeleted;
                    ori.IsImprime = input.IsImprime;
                    ori.IsSergioFranco = input.IsSergioFranco;
                    ori.IsSigiloso = input.IsSigiloso;
                    ori.IsSistema = input.IsSistema;
                    ori.KitExameId = input.KitExameId;
                    ori.LastModificationTime = input.LastModificationTime;
                    ori.LastModifierUserId = input.LastModifierUserId;
                    ori.MaqImpSolicita = input.MaqImpSolicita;
                    ori.MaterialId = input.MaterialId;
                    ori.Mneumonico = input.Mneumonico;
                    ori.MotivoPendenteExame = input.MotivoPendenteExame;
                    ori.Observacao = input.Observacao;
                    ori.Quantidade = input.Quantidade;
                    ori.ResultadoId = input.ResultadoId;
                    ori.Seq = input.Seq;
                    ori.TabelaId = input.TabelaId;
                    ori.UsuarioAlteradoExameId = input.UsuarioAlteradoExameId;
                    ori.UsuarioCienteExameId = input.UsuarioCienteExameId;
                    ori.UsuarioConferidoExameId = input.UsuarioConferidoExameId;
                    ori.UsuarioDigitadoExameId = input.UsuarioDigitadoExameId;
                    ori.UsuarioImpressoExameId = input.UsuarioImpressoExameId;
                    ori.UsuarioImpSolicitaId = input.UsuarioImpSolicitaId;
                    ori.UsuarioIncluidoExameId = input.UsuarioIncluidoExameId;
                    ori.UsuarioPendenteExameId = input.UsuarioPendenteExameId;
                    ori.VolumeMaterial = input.VolumeMaterial;

                    if (ori.ExameStatusId != 4)
                    {
                        await _resultadoExameRepositorio.UpdateAsync(ori);
                        _unitOfWorkManager.Current.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(ResultadoExameDto input)
        {
            try
            {
                await _resultadoExameRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<ListResultDto<ResultadoExame>> ListarTodos()
        {
            try
            {
                var query = await _resultadoExameRepositorio
                    .GetAllListAsync();

                var resultadoExamesDto = query.MapTo<List<ResultadoExame>>();

                return new ListResultDto<ResultadoExame> { Items = resultadoExamesDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ResultadoExameIndexDto>> ListarIndex(ListarInput input)
        {
            var contarTiposTabelaDominio = 0;
            List<ResultadoExame> resultadoExames = new List<ResultadoExame>();
            List<ResultadoExameIndexDto> resultadoExamesDtos = new List<ResultadoExameIndexDto>();
            try
            {
                long id = 0;
                long.TryParse(input.Id, out id);
                var query = _resultadoExameRepositorio
                    .GetAll()
                    .Include(m => m.UsuarioAlteradoExame)
                    .Include(m => m.UsuarioCienteExame)
                    .Include(m => m.UsuarioConferidoExame)
                    .Include(m => m.UsuarioDigitadoExame)
                    .Include(m => m.UsuarioImpressoExame)
                    .Include(m => m.UsuarioImpSolicita)
                    .Include(m => m.UsuarioIncluidoExame)
                    .Include(m => m.UsuarioPendenteExame)
                    .Include(m => m.Resultado)
                    .Include(m => m.Resultado.Atendimento)
                    .Include(m => m.Resultado.Atendimento.Empresa)
                    .Where(m => m.ResultadoId == id)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()));
                //.Select(m => new ResultadoExameIndexDto
                //{
                //    Id = m.Id,
                //    Mneumonico = m.Mneumonico,
                //    DataColeta = m.Resultado.CreationTime,
                //    NumeroExame = m.Exame.Codigo,
                //    NomeExame = m.Exame.Descricao,
                //    //UsuarioIncluidoId = m.UsuarioIncluidoExame.FullName,
                //    DataIncluido = m.DataInclusao,
                //    //UsuarioDigitadoId = m.UsuarioDigitadoExame.FullName,
                //    DataDigitado = m.DataDigitadoExame,
                //    //UsuarioConferidoId = m.UsuarioConferidoExame.FullName,
                //    DataConferido = m.DataConferidoExame,
                //    DataPendente = m.DataPendenteExame,
                //    //UsuarioImpressoId = m.UsuarioImpressoExame.FullName,
                //    DataImpresso = m.DataImpressoExame,
                //    DataEnvioEmail = m.DataEnvioEmail
                //});

                contarTiposTabelaDominio = await query
                    .CountAsync();

                resultadoExames = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in resultadoExames)
                {
                    var resultado = new ResultadoExameIndexDto();
                    resultado.Id = item.Id;
                    resultado.AtendimentoId = item.Resultado.AtendimentoId;
                    resultado.EmpresaId = item.Resultado.Atendimento?.EmpresaId;
                    resultado.Empresa = item.Resultado.Atendimento?.Empresa?.NomeFantasia;
                    resultado.DataColeta = item.Resultado.DataColeta;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.DataEnvioEmail = item.DataEnvioEmail;
                    resultado.DataImpresso = item.DataImpressoExame;
                    resultado.DataIncluido = item.DataInclusao;
                    resultado.DataPendente = item.DataPendenteExame;
                    resultado.Mneumonico = item.Mneumonico;
                    resultado.NomeExame = item.FaturamentoItem?.Descricao;
                    resultado.NumeroExame = item.FaturamentoItem?.Codigo;
                    resultado.UsuarioConferidoId = item.UsuarioConferidoExame?.FullName;
                    resultado.UsuarioDigitadoId = item.UsuarioDigitadoExame?.FullName;
                    resultado.UsuarioImpressoId = item.UsuarioImpressoExame?.FullName;
                    resultado.UsuarioIncluidoId = item.UsuarioIncluidoExame?.FullName;
                    resultado.ExameStatusId = item.ExameStatusId;
                    resultado.ExameStatus = item.ExameStatus?.Descricao;
                    resultado.ExameStatusCor = item.ExameStatus?.Cor;

                    resultadoExamesDtos.Add(resultado);
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ResultadoExameIndexDto>(
                contarTiposTabelaDominio,
                resultadoExamesDtos
                );
        }

        public async Task<PagedResultDto<ResultadoExameIndexCrudDto>> Listar(ListarInput input)
        {
            var contarTiposTabelaDominio = 0;
            List<ResultadoExame> resultadoExames = new List<ResultadoExame>();
            List<ResultadoExameIndexCrudDto> resultadoExamesDtos = new List<ResultadoExameIndexCrudDto>();
            try
            {
                long id = 0;
                long.TryParse(input.Id, out id);
                var query = _resultadoExameRepositorio
                    .GetAll()
                    .Include(m => m.UsuarioAlteradoExame)
                    .Include(m => m.UsuarioCienteExame)
                    .Include(m => m.UsuarioConferidoExame)
                    .Include(m => m.UsuarioDigitadoExame)
                    .Include(m => m.UsuarioImpressoExame)
                    .Include(m => m.UsuarioImpSolicita)
                    .Include(m => m.UsuarioIncluidoExame)
                    .Include(m => m.UsuarioPendenteExame)
                    .Include(m => m.Resultado)
                    .Include(m => m.Resultado.Atendimento)
                    .Include(m => m.Resultado.Atendimento.Empresa)
                    .Where(m => m.ResultadoId == id)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()));

                contarTiposTabelaDominio = await query
                    .CountAsync();

                resultadoExames = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in resultadoExames)
                {
                    var resultado = new ResultadoExameIndexCrudDto();
                    resultado.Id = item.Id;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.DataPendente = item.DataPendenteExame;
                    //  resultado.ExameId = item.FaturamentoItemId;
                    resultado.Exame = item.FaturamentoItem?.Descricao;
                    resultado.UsuarioConferidoId = item.UsuarioConferidoExameId;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.UsuarioDigitadoId = item.UsuarioDigitadoExameId;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.UsuarioPendenteId = item.UsuarioPendenteExameId;
                    resultado.DataPendente = item.DataPendenteExame;
                    resultado.FaturamentoContaItemId = item.FaturamentoContaItemId;
                    resultado.FaturamentoContaItem = item.FaturamentoContaItem?.Descricao;
                    resultado.IsSigiloso = item.IsSigiloso;
                    resultado.MaterialId = item.MaterialId;
                    resultado.Material = item.Material?.Descricao;
                    resultado.MotivoPendenteExame = item.MotivoPendenteExame;
                    resultado.ObservacaoExame = item.Observacao;
                    resultado.Quantidade = item.Quantidade;
                    resultado.AtendimentoId = item.Resultado.AtendimentoId;
                    resultado.EmpresaId = item.Resultado.Atendimento?.EmpresaId;
                    resultado.Empresa = item.Resultado.Atendimento?.Empresa?.NomeFantasia;
                    resultado.ExameStatusId = item.ExameStatusId;
                    resultado.ExameStatus = item.ExameStatus?.Descricao;
                    resultado.ExameStatusCor = item.ExameStatus?.Cor;

                    resultadoExamesDtos.Add(resultado);
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ResultadoExameIndexCrudDto>(
                contarTiposTabelaDominio,
                resultadoExamesDtos
                );
        }

        public async Task<ListResultDto<ResultadoExameIndexCrudDto>> ListarPorResultado(long id)
        {
            List<ResultadoExame> resultadoExames = new List<ResultadoExame>();
            List<ResultadoExameIndexCrudDto> resultadoExamesDtos = new List<ResultadoExameIndexCrudDto>();
            try
            {
                var query = _resultadoExameRepositorio
                    .GetAll()
                    .Include(m => m.UsuarioAlteradoExame)
                    .Include(m => m.UsuarioCienteExame)
                    .Include(m => m.UsuarioConferidoExame)
                    .Include(m => m.UsuarioDigitadoExame)
                    .Include(m => m.UsuarioImpressoExame)
                    .Include(m => m.UsuarioImpSolicita)
                    .Include(m => m.UsuarioIncluidoExame)
                    .Include(m => m.UsuarioPendenteExame)
                    .Include(m => m.Resultado)
                    .Include(m => m.Resultado.Atendimento)
                    .Include(m => m.Resultado.Atendimento.Empresa)
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Material)
                    .Include(m => m.ExameStatus)
                    .Where(m => m.ResultadoId == id);


                resultadoExames = await query
                    .ToListAsync();

                long idGrid = 0;
                foreach (var item in resultadoExames)
                {
                    var resultado = new ResultadoExameIndexCrudDto();
                    resultado.Id = item.Id;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.DataPendente = item.DataPendenteExame;
                    resultado.FaturamentoItemId = item.FaturamentoItemId;
                    resultado.Exame = item.FaturamentoItem?.Descricao;
                    resultado.UsuarioConferidoId = item.UsuarioConferidoExameId;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.UsuarioDigitadoId = item.UsuarioDigitadoExameId;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.UsuarioPendenteId = item.UsuarioPendenteExameId;
                    resultado.DataPendente = item.DataPendenteExame;
                    resultado.FaturamentoContaItemId = item.FaturamentoContaItemId;
                    resultado.FaturamentoContaItem = item.FaturamentoContaItem?.Descricao;
                    resultado.IsSigiloso = item.IsSigiloso;
                    resultado.MaterialId = item.MaterialId;
                    resultado.Material = item.Material?.Descricao;
                    resultado.MotivoPendenteExame = item.MotivoPendenteExame;
                    resultado.ObservacaoExame = item.Observacao;
                    resultado.Quantidade = item.Quantidade;
                    resultado.CreatorUserId = item.CreatorUserId;
                    resultado.CreationTime = item.CreationTime;
                    resultado.IsSistema = item.IsSistema;
                    resultado.LastModificationTime = item.LastModificationTime;
                    resultado.LastModifierUserId = item.LastModifierUserId;
                    resultado.IdGridResultadoExame = ++idGrid;
                    resultado.Cor = item.ExameStatus.Cor;
                    resultado.ExameStatusId = item.ExameStatusId;
                    resultado.AtendimentoId = item.Resultado.AtendimentoId;
                    resultado.EmpresaId = item.Resultado.Atendimento?.EmpresaId;
                    resultado.Empresa = item.Resultado.Atendimento?.Empresa?.NomeFantasia;
                    resultado.ExameStatusId = item.ExameStatusId;
                    resultado.ExameStatus = item.ExameStatus?.Descricao;
                    resultado.ExameStatusCor = item.ExameStatus?.Cor;

                    resultadoExamesDtos.Add(resultado);
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new ListResultDto<ResultadoExameIndexCrudDto>
            {
                Items = resultadoExamesDtos
            };
        }

        public async Task<ResultadoExameDto> Obter(long id)
        {
            try
            {
                var query = _resultadoExameRepositorio
                    .GetAll()
                    .Where(m => m.Id == id);

                var input = await query.FirstOrDefaultAsync();

                var resultadoExameDto = new ResultadoExameDto //resultadoExame.MapTo<ResultadoExameDto>();
                {
                    Codigo = input.Codigo,
                    CreationTime = input.CreationTime,
                    CreatorUserId = input.CreatorUserId,
                    DataAlteracao = input.DataAlteracao,
                    DataAlteradoExame = input.DataAlteradoExame,
                    DataConferidoExame = input.DataConferidoExame,
                    DataDigitadoExame = input.DataDigitadoExame,
                    DataEnvioEmail = input.DataEnvioEmail,
                    DataExclusao = input.DataExclusao,
                    DataImporta = input.DataImporta,
                    DataImpressoExame = input.DataImpressoExame,
                    DataImpSolicita = input.DataImpSolicita,
                    DataInclusao = input.DataInclusao,
                    DataPendenteExame = input.DataPendenteExame,
                    DataUsuarioCienteExame = input.DataUsuarioCienteExame,
                    DeleterUserId = input.DeleterUserId,
                    DeletionTime = input.DeletionTime,
                    Descricao = input.Descricao,
                    ExameStatusId = input.ExameStatusId,
                    FaturamentoItemId = input.FaturamentoItemId,
                    FaturamentoContaItemId = input.FaturamentoContaItemId,
                    FormataId = input.FormataId,
                    Id = input.Id,
                    ImpResultado = input.ImpResultado,
                    IsCienteExame = input.IsCienteExame,
                    IsDeleted = input.IsDeleted,
                    IsImprime = input.IsImprime,
                    IsSergioFranco = input.IsSergioFranco,
                    IsSigiloso = input.IsSigiloso,
                    IsSistema = input.IsSistema,
                    KitExameId = input.KitExameId,
                    LastModificationTime = input.LastModificationTime,
                    LastModifierUserId = input.LastModifierUserId,
                    MaqImpSolicita = input.MaqImpSolicita,
                    MaterialId = input.MaterialId,
                    Mneumonico = input.Mneumonico,
                    MotivoPendenteExame = input.MotivoPendenteExame,
                    Observacao = input.Observacao,
                    Quantidade = input.Quantidade,
                    ResultadoId = input.ResultadoId,
                    Seq = input.Seq,
                    TabelaId = input.TabelaId,
                    UsuarioAlteradoExameId = input.UsuarioAlteradoExameId,
                    UsuarioCienteExameId = input.UsuarioCienteExameId,
                    UsuarioConferidoExameId = input.UsuarioConferidoExameId,
                    UsuarioDigitadoExameId = input.UsuarioDigitadoExameId,
                    UsuarioImpressoExameId = input.UsuarioImpressoExameId,
                    UsuarioImpSolicitaId = input.UsuarioImpSolicitaId,
                    UsuarioIncluidoExameId = input.UsuarioIncluidoExameId,
                    UsuarioPendenteExameId = input.UsuarioPendenteExameId,
                    VolumeMaterial = input.VolumeMaterial
                };

                if (resultadoExameDto.FaturamentoItemId.HasValue)
                {
                    resultadoExameDto.FaturamentoItem = _faturamentoItemRepository.GetAll()
                                                                                   .Where(w => w.Id == resultadoExameDto.FaturamentoItemId)
                                                                                   .FirstOrDefault().MapTo<FaturamentoItemDto>();
                    //.Obter(resultadoExameDto.FaturamentoItemId.Value);
                }
                //if (resultadoExameDto.FaturamentoContaItemId.HasValue)
                //{
                //    resultadoExameDto.FaturamentoContaItem = await _fatContaItemAppService.Obter(resultadoExameDto.FaturamentoContaItemId.Value);
                //}
                if (resultadoExameDto.FormataId.HasValue)
                {
                    resultadoExameDto.Formata = await _formataAppService.Obter(resultadoExameDto.FormataId.Value);
                }
                if (resultadoExameDto.MaterialId.HasValue)
                {
                    resultadoExameDto.Material = await _materialAppService.Obter(resultadoExameDto.MaterialId.Value);
                }
                if (resultadoExameDto.TabelaId.HasValue)
                {
                    resultadoExameDto.Tabela = await _tabelaAppService.Obter(resultadoExameDto.TabelaId.Value);
                }
                if (resultadoExameDto.KitExameId.HasValue)
                {
                    resultadoExameDto.KitExame = await _kitExameAppService.Obter(resultadoExameDto.KitExameId.Value);
                }

                return resultadoExameDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<ExameStatusDto>> Legenda()
        {
            try
            {
                var query = await _exameStatusRepositorio
                    .GetAllListAsync();

                var exameStatusDto = new List<ExameStatusDto>();
                foreach (var item in query)
                {
                    exameStatusDto.Add(ExameStatusDto.Mapear(item));
                }

                return new ListResultDto<ExameStatusDto> { Items = exameStatusDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _resultadoExameRepositorio
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                var ResultadoExames = new ListResultDto<GenericoIdNome> { Items = query };

                List<ResultadoExameDto> ResultadoExamesList = new List<ResultadoExameDto>();

                return ResultadoExames;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarInput input)
        {
            try
            {
                var result = await ListarIndex(input);
                var ResultadoExames = result.Items;
                return _listarResultadoExamesExcelExporter.ExportToFile(ResultadoExames.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<ResultadoExameDto> pacientesDtos = new List<ResultadoExameDto>();
            try
            {
                //get com filtro
                var query = from p in _resultadoExameRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                        //||
                        //  m.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ResultadoExameIndexCrudDto>> ListarJson(List<ResultadoExameIndexCrudDto> list)
        {
            try
            {
                var result = await Task.Run(() => new PagedResultDto<ResultadoExameIndexCrudDto>(list.Count(), list));
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ResultadoExameIndexCrudDto>> ListarNaoConferidos(ListarResultadoExamesInput input)
        {
            var contarTiposTabelaDominio = 0;
            List<ResultadoExame> resultadoExames = new List<ResultadoExame>();
            List<ResultadoExameIndexCrudDto> resultadoExamesDtos = new List<ResultadoExameIndexCrudDto>();
            try
            {
                var query = _resultadoExameRepositorio
                    .GetAll()
                    .Include(m => m.Resultado)
                     .Include(m => m.FaturamentoItem)
                     .Include(m => m.ExameStatus)
                    .Where(m => m.ResultadoId == input.ColetaId
                             //                             && m.UsuarioConferidoExameId == null
                             )

                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()));

                contarTiposTabelaDominio = await query
                    .CountAsync();

                resultadoExames = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in resultadoExames)
                {
                    var resultado = new ResultadoExameIndexCrudDto();
                    resultado.Id = item.Id;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.DataPendente = item.DataPendenteExame;
                    resultado.FaturamentoItemId = item.FaturamentoItemId;
                    resultado.Exame = item.FaturamentoItem?.Descricao;
                    resultado.UsuarioConferidoId = item.UsuarioConferidoExameId;
                    resultado.DataConferido = item.DataConferidoExame;
                    resultado.UsuarioDigitadoId = item.UsuarioDigitadoExameId;
                    resultado.DataDigitado = item.DataDigitadoExame;
                    resultado.UsuarioPendenteId = item.UsuarioPendenteExameId;
                    resultado.DataPendente = item.DataPendenteExame;
                    resultado.FaturamentoContaItemId = item.FaturamentoContaItemId;
                    resultado.FaturamentoContaItem = item.FaturamentoContaItem?.Descricao;
                    resultado.IsSigiloso = item.IsSigiloso;
                    resultado.MaterialId = item.MaterialId;
                    resultado.Material = item.Material?.Descricao;
                    resultado.MotivoPendenteExame = item.MotivoPendenteExame;
                    resultado.ObservacaoExame = item.Observacao;
                    resultado.Quantidade = item.Quantidade;
                    resultado.Codigo = item.Resultado.Numero;
                    resultado.ExameStatusId = item.ExameStatusId;
                    resultado.Cor = item.ExameStatus.Cor;
                    resultadoExamesDtos.Add(resultado);
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ResultadoExameIndexCrudDto>(
                contarTiposTabelaDominio,
                resultadoExamesDtos
                );
        }

        public async Task<DefaultReturn<ResultadoExameIndexCrudDto>> RegistrarConferenciaExames(long[] examesIds)
        {
            var _retornoPadrao = new DefaultReturn<ResultadoExameIndexCrudDto>();
            _retornoPadrao.ReturnObject = new ResultadoExameIndexCrudDto();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                foreach (var item in examesIds)
                {
                    var resultadoExame = _resultadoExameRepositorio.GetAll()
                                                                   .Where(w => w.Id == item)
                                                                   .FirstOrDefault();

                    if (resultadoExame != null)
                    {
                        resultadoExame.ExameStatusId = (long)EnumStatusExame.Conferido;
                        resultadoExame.DataConferidoExame = DateTime.Now;
                        resultadoExame.UsuarioConferidoExameId = AbpSession.UserId;
                    }
                }

                unitOfWork.Complete();
                _unitOfWorkManager.Current.SaveChanges();
                unitOfWork.Dispose();
            }
            return _retornoPadrao;
        }




    }
}

