﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
//using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Informacoes.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Materiais.Dto
{
    [AutoMap(typeof(Material))]
    public class MaterialDto : CamposPadraoCRUDDto
    {
        public int Ordem { get; set; }

        //public int TipoLayout { get; set; }
        //public string DiretorioOrdem { get; set; }
        //public string DiretorioResultado { get; set; }
        //public InformacaoDto Informacao { get; set; }
    }
}
