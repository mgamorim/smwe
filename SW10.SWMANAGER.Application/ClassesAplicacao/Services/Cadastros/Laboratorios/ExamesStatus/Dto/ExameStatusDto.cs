﻿using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Exames.Dto
{
    public class ExameStatusDto : CamposPadraoCRUDDto
    {
        public string Cor { get; set; }

        public static ExameStatusDto Mapear(ExameStatus exameStatus)
        {
            ExameStatusDto exameStatusDto = new ExameStatusDto();

            exameStatusDto.Id = exameStatus.Id;
            exameStatusDto.Codigo = exameStatus.Codigo;
            exameStatusDto.Descricao = exameStatus.Descricao;
            exameStatusDto.Cor= exameStatus.Cor;

            return exameStatusDto;
        }

        public static ExameStatus Mapear(ExameStatusDto exameStatusDto)
        {
            ExameStatus exameStatus = new ExameStatus();

            exameStatus.Id = exameStatusDto.Id;
            exameStatus.Codigo = exameStatusDto.Codigo;
            exameStatus.Descricao = exameStatusDto.Descricao;
            exameStatus.Cor = exameStatusDto.Cor;

            return exameStatus;
        }
    }
}
