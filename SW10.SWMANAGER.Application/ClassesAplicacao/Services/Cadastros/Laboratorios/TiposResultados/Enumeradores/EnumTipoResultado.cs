﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.TiposResultados.Enumeradores
{
    public enum EnumTipoResultado
    {
        Numerico=1,
        Alfanumerico=2,
        Calculado=3,
        Tabela=4,
        Memo=5,
        Gráfico
    }
}
