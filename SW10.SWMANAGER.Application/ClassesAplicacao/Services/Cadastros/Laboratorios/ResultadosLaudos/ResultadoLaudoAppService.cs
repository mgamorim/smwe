﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Dto;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services;
using System.Web.Mvc;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Text.RegularExpressions;
using System.Data;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.TiposResultados.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Sistemas;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos
{
    public class ResultadoLaudoAppService : SWMANAGERAppServiceBase, IResultadoLaudoAppService
    {

        private readonly IListarResultadoLaudosExcelExporter _listarResultadoLaudosExcelExporter;
        private readonly IRepository<ResultadoLaudo, long> _resultadoLaudoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ResultadoExame, long> _resultadoExameRepository;
        private readonly IRepository<RegistroArquivo, long> _registroArquivoRepository;
        private readonly IRepository<TabelaResultado, long> _tabelaResultadoRepositoy;
        private readonly IRepository<ItemResultado, long> _itemResultadoRepository;
        private readonly IRepository<Resultado, long> _resultadoRepository;


        public ResultadoLaudoAppService(IRepository<ResultadoLaudo, long> resultadoLaudoRepositorio
                                     , IListarResultadoLaudosExcelExporter listarResultadoLaudosExcelExporter
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<ResultadoExame, long> resultadoExameRepository
            , IRepository<RegistroArquivo, long> registroArquivoRepository
            , IRepository<TabelaResultado, long> tabelaResultadoRepositoy
            , IRepository<ItemResultado, long> itemResultadoRepository
            , IRepository<Resultado, long> resultadoRepository

            )
        {
            _resultadoLaudoRepositorio = resultadoLaudoRepositorio;
            _listarResultadoLaudosExcelExporter = listarResultadoLaudosExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _resultadoExameRepository = resultadoExameRepository;
            _registroArquivoRepository = registroArquivoRepository;
            _tabelaResultadoRepositoy = tabelaResultadoRepositoy;
            _itemResultadoRepository = itemResultadoRepository;
            _resultadoRepository = resultadoRepository;

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_DominioTiss_TiposTabelaDominio_Create, AppPermissions.Pages_Tenant_Cadastros_DominioTiss_TiposTabelaDominio_Edit)]
        public async Task CriarOuEditar(ResultadoLaudoDto input)
        {
            try
            {
                var resultadoLaudo = ResultadoLaudoDto.Mapear(input); //.MapTo<ResultadoLaudo>();
                if (input.Id.Equals(0))
                {
                    await _resultadoLaudoRepositorio.InsertOrUpdateAsync(resultadoLaudo);
                }
                else
                {
                    await _resultadoLaudoRepositorio.UpdateAsync(resultadoLaudo);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task Excluir(ResultadoLaudoDto input)
        {
            try
            {
                await _resultadoLaudoRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<ResultadoLaudoDto>> Listar(ListarResultadoLaudosInput input)
        {
            var contarTiposTabelaDominio = 0;
            List<ResultadoLaudo> resultadoLaudos;
            List<ResultadoLaudoDto> resultadoLaudosDtos = new List<ResultadoLaudoDto>();
            try
            {
                var query = _resultadoLaudoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarTiposTabelaDominio = await query
                    .CountAsync();

                resultadoLaudos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                resultadoLaudosDtos = ResultadoLaudoDto.Mapear(resultadoLaudos).ToList();
                    //.MapTo<List<ResultadoLaudoDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ResultadoLaudoDto>(
                contarTiposTabelaDominio,
                resultadoLaudosDtos
                );
        }

        public async Task<ResultadoLaudoDto> Obter(long id)
        {
            try
            {
                var result = await _resultadoLaudoRepositorio.GetAsync(id);
                var resultadoLaudo = ResultadoLaudoDto.Mapear(result); //.MapTo<ResultadoLaudoDto>();
                return resultadoLaudo;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _resultadoLaudoRepositorio
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                var ResultadoLaudos = new ListResultDto<GenericoIdNome> { Items = query };

                List<ResultadoLaudoDto> ResultadoLaudosList = new List<ResultadoLaudoDto>();

                return ResultadoLaudos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public async Task<FileDto> ListarParaExcel(ListarResultadoLaudosInput input)
        //{
        //    try
        //    {
        //        var result = await Listar(input);
        //        var ResultadoLaudos = result.Items;
        //        return _listarResultadoLaudosExcelExporter.ExportToFile(ResultadoLaudos.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroExportar"));
        //    }
        //}

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<ResultadoLaudoDto> pacientesDtos = new List<ResultadoLaudoDto>();
            try
            {
                //get com filtro
                var query = from p in _resultadoLaudoRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                        || m.Descricao.ToUpper().Contains(dropdownInput.search.ToUpper())
                        //||
                        //  m.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [HttpPost]
        public async Task<DefaultReturn<ResultadoLaudoDto>> CriarOuEditarLista(string input, long coletaId)
        {
            var _retornoPadrao = new DefaultReturn<ResultadoLaudoDto>();
            _retornoPadrao.ReturnObject = new ResultadoLaudoDto();
            _retornoPadrao.Warnings = new List<ErroDto>();
            _retornoPadrao.Errors = new List<ErroDto>();

            try
            {



                List<FormatacaoItemIndexDto> resultadosDto = JsonConvert.DeserializeObject<List<FormatacaoItemIndexDto>>(input);

                ResultadoLaudoValidacaoService resultadoLaudoValidacaoService = new ResultadoLaudoValidacaoService(_itemResultadoRepository
                                                                                                                  , _resultadoRepository);
                _retornoPadrao.Errors = resultadoLaudoValidacaoService.Validar(resultadosDto, coletaId);

                if (_retornoPadrao.Errors.Count == 0)
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {

                        foreach (var item in resultadosDto)
                        {

                            var resultadoLaudo = _resultadoLaudoRepositorio.GetAll()
                                                                           .Where(w => w.Id == item.LaudoResultadoId)
                                                                           .FirstOrDefault();
                            resultadoLaudo.DataDigitadoLaudo = DateTime.Now;
                            // resultadoLaudo.ItemResultadoId = item.ItemId;
                            // resultadoLaudo.Referencia = item.Referencia;
                            resultadoLaudo.Resultado = item.Resultado;
                            resultadoLaudo.UsuarioLaudoId = AbpSession.UserId;

                            // resultadoLaudo.ResultadoExameId = item.ResultadoExameId;
                            // resultadoLaudo.Id = item.LaudoResultadoId ?? 0;
                            // resultadoLaudo.UnidadeId = item.UnidadeId;

                            //   if (item.LaudoResultadoId == null || item.LaudoResultadoId.Equals(0))
                            //  {
                            //     await _resultadoLaudoRepositorio.InsertOrUpdateAsync(resultadoLaudo);
                            // }
                            //else
                            //{
                            await _resultadoLaudoRepositorio.UpdateAsync(resultadoLaudo);
                            //}
                        }

                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();

                        unitOfWork.Dispose();
                        _retornoPadrao.ReturnObject.RegistroArquivoId = GerarArquivo(coletaId);
                    }

                }
                return _retornoPadrao;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        private long GerarArquivo(long coletaId)
        {
            long id = 0;
            try
            {




                var exames = _resultadoExameRepository.GetAll()
                                                       .Include(i => i.FaturamentoItem)
                                                       .Include(i => i.Formata)
                                                       //.Include(i=> i.Resultado)
                                                       //.Include(i => i.Resultado.Atendimento)
                                                       //.Include(i => i.Resultado.Atendimento.Origem)
                                                       //.Include(i => i.Resultado.Atendimento.Convenio)
                                                       //.Include(i => i.Resultado.Atendimento.Convenio.SisPessoa)
                                                       //.Include(i => i.Resultado.Atendimento.Paciente)
                                                       //.Include(i => i.Resultado.Atendimento.Paciente.SisPessoa)
                                                       //.Include(i => i.Resultado.Atendimento.Paciente.SisPessoa.Sexo)
                                                       .Where(w => w.ResultadoId == coletaId)
                                                       .ToList();

                var formatacao = "";


                foreach (var exame in exames)
                {
                    //Alterando o status dos exames para digitado
                    exame.ExameStatusId = (long)EnumStatusExame.Digitado;

                    formatacao += exame.Formata.Formatacao;

                    var laudos = _resultadoLaudoRepositorio.GetAll()
                                                          .Where(w => w.ResultadoExameId == exame.Id)
                                                          .Include(i => i.ItemResultado)
                                                          .Include(i => i.LaboratorioUnidade)
                                                          .ToList();
                    foreach (var item in laudos)
                    {
                        //numerico e alfanumeico
                        if (item.TipoResultadoId == (long)EnumTipoResultado.Numerico || item.TipoResultadoId == (long)EnumTipoResultado.Alfanumerico)
                        {
                            formatacao = formatacao.Replace(string.Concat("[", item.ItemResultado.Codigo, "]"), item.Resultado);
                        }
                        //Calculado
                        else if (item.TipoResultadoId == (long)EnumTipoResultado.Calculado)
                        {
                            var formula = item.Formula;


                            if (!string.IsNullOrEmpty(formula))
                            {
                                string pattern = Regex.Escape("[") + "(.*?)]";
                                MatchCollection matches = Regex.Matches(formula, pattern);

                                bool valido = true;

                                foreach (Match matche in matches)
                                {

                                    var resultado = laudos.Where(w => w.ItemResultado.Codigo == matche.Groups[1].Value)
                                                         .FirstOrDefault()
                                                         .Resultado;

                                    if (string.IsNullOrEmpty(resultado))
                                    {
                                        valido = false;
                                        break;
                                    }

                                    formula = formula.Replace(matche.Value, resultado.Replace(',', '.'));
                                }

                                if (valido)
                                {
                                    DataTable dt = new DataTable();
                                    var valorFormula = dt.Compute(formula, "");

                                    if (valorFormula.GetType() == typeof(decimal))
                                    {
                                        var resultado = (decimal)valorFormula;
                                        formatacao = formatacao.Replace(string.Concat("[", item.ItemResultado.Codigo, "]"), resultado.ToString(string.Format("N{0}", item.CasaDecimal)));
                                    }
                                    else if (valorFormula.GetType() == typeof(double))
                                    {
                                        var resultado = (double)valorFormula;
                                        formatacao = formatacao.Replace(string.Concat("[", item.ItemResultado.Codigo, "]"), resultado.ToString(string.Format("N{0}", item.CasaDecimal)));
                                    }
                                    else if (valorFormula.GetType() == typeof(float))
                                    {
                                        var resultado = (float)valorFormula;
                                        formatacao = formatacao.Replace(string.Concat("[", item.ItemResultado.Codigo, "]"), resultado.ToString(string.Format("N{0}", item.CasaDecimal)));
                                    }
                                    else if (valorFormula.GetType() == typeof(int))
                                    {
                                        var resultado = (int)valorFormula;
                                        formatacao = formatacao.Replace(string.Concat("[", item.ItemResultado.Codigo, "]"), resultado.ToString(string.Format("N{0}", item.CasaDecimal)));
                                    }

                                }
                            }
                        }

                        //Tabela
                        else if (item.TipoResultadoId == (long)EnumTipoResultado.Tabela)
                        {
                            long tabelaResultadoId;

                            long.TryParse(item.Resultado, out tabelaResultadoId);


                            var tabelaResuldado = _tabelaResultadoRepositoy.GetAll()
                                                      .Where(w => w.Id == tabelaResultadoId)
                                                      .FirstOrDefault();

                            if (tabelaResuldado != null)
                            {
                                formatacao = formatacao.Replace(string.Concat("[", item.ItemResultado.Codigo, "]"), tabelaResuldado.Descricao);
                            }
                        }

                    }
                    id = GravarArquivo(exame.Id, formatacao);
                    formatacao = string.Empty;
                }




                //Document document = new Document(PageSize.A4, 10, 10, 10, 10);
                //MemoryStream ms = new MemoryStream();
                //PdfWriter writer = PdfWriter.GetInstance(document, ms);
                //HTMLWorker obj = new HTMLWorker(document);

                //StringReader se = new StringReader(formatacao);
                //document.Open();
                //obj.Parse(se);

                //document.Close();

                //RegistroArquivo registroArquivo = new RegistroArquivo();

                //registroArquivo.Arquivo = ms.ToArray();
                //registroArquivo.RegistroTabelaId = (long)EnumArquivoTabela.Laboratorio;
                //registroArquivo.RegistroId = coletaId;

                //id = _registroArquivoRepository.InsertAndGetId(registroArquivo);
                //_unitOfWorkManager.Current.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

            return id;
        }

        private long GravarArquivo(long resultadoExameLaboratorioId, string formatacao)
        {

            Document document = new Document(PageSize.A4, 10, 10, 170, 10);
            MemoryStream ms = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            HTMLWorker obj = new HTMLWorker(document);

            StringReader se = new StringReader(formatacao);
            document.Open();
            obj.Parse(se);

            document.Close();

            RegistroArquivo registroArquivo = new RegistroArquivo();

            registroArquivo.Arquivo = ms.ToArray();
            registroArquivo.RegistroTabelaId = (long)EnumArquivoTabela.LaboratorioExame;
            registroArquivo.RegistroId = resultadoExameLaboratorioId;

            var id = _registroArquivoRepository.InsertAndGetId(registroArquivo);
            _unitOfWorkManager.Current.SaveChanges();

            return id;
        }

        public async Task<PagedResultDto<ExameResultadoDto>> ListarHistorioExamePorPaciente(ExameResultadoInput input)
        {

            var queryHistorio = _resultadoLaudoRepositorio.GetAll()
                                                          .Where(w => w.ResultadoExame.FaturamentoItemId == input.exameId
                                                                  && w.ResultadoExame.Resultado.Atendimento.PacienteId == input.pacienteId)

                                                          .Select(s => new ExameResultadoDto
                                                          {
                                                              DataColeta = s.ResultadoExame.Resultado.DataColeta,
                                                              Exame = s.ItemResultado.Descricao,
                                                              Resultado = s.Resultado,
                                                              TabelaId = s.ItemResultado.TabelaId,
                                                              TipoResultadoId = s.TipoResultadoId
                                                          });

            var contarResultados = await queryHistorio
                    .CountAsync();

            var resultadoLaudos = await queryHistorio
                .AsNoTracking()
                .OrderByDescending(o => o.DataColeta)
                .PageBy(input)
                .ToListAsync();

            foreach (var item in resultadoLaudos)
            {
                if (item.TipoResultadoId == (long)EnumTipoResultado.Tabela)
                {
                    long resultadoId;

                    if (long.TryParse(item.Resultado, out resultadoId))
                    {

                        var resTabela = _tabelaResultadoRepositoy.GetAll()
                                                 .Where(w => w.TabelaId == item.TabelaId
                                                        && w.Id == resultadoId)
                                                        .FirstOrDefault();

                        if(resTabela!=null)
                        {
                            item.Resultado = resTabela.Descricao;
                        }

                    }
                }
            }

            return new PagedResultDto<ExameResultadoDto>(
                contarResultados,
                resultadoLaudos
                );

        }

    }
}

