﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.TiposResultados.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Enumeradores;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos
{
    public class ResultadoLaudoValidacaoService
    {
        private readonly IRepository<ItemResultado, long> _itemResultadoRepository;
        private readonly IRepository<Resultado, long> _resultadoRepository;

        public ResultadoLaudoValidacaoService(IRepository<ItemResultado, long> itemResultadoRepository
                                            , IRepository<Resultado, long> resultadoRepository)
        {
            _itemResultadoRepository = itemResultadoRepository;
            _resultadoRepository = resultadoRepository;
        }


        List<ErroDto> Lista = new List<ErroDto>();
        public List<ErroDto> Validar(List<FormatacaoItemIndexDto> resultadosDto, long coletaId)
        {
            ValidarSoma100(resultadosDto.Where(m=>m.ExameStatusId!=4).ToList());


            var resultado = _resultadoRepository.GetAll()
                                                .Where(w => w.Id == coletaId)
                                                .Include(i => i.Atendimento.Paciente)
                                                .Include(i => i.Atendimento.Paciente.SisPessoa)
                                                .FirstOrDefault();




            foreach (var item in resultadosDto.Where(m => m.ExameStatusId != 4).ToList()) 
            {
                if (resultado.Atendimento.Paciente.SexoId != null)
                {
                    ValidarValoresConfigurados(item, (long)resultado.Atendimento.Paciente.SexoId);
                }
            }


            return Lista;
        }

        void ValidarSoma100(List<FormatacaoItemIndexDto> resultadosDto)
        {
            decimal soma = 0;
            decimal valor;

            bool existeIsSoma = false;
            foreach (var item in resultadosDto)
            {
                var itemResultado = _itemResultadoRepository.GetAll()
                                                           .Where(w => w.Id == item.ItemId
                                                                   && w.IsSoma100)
                                                           .FirstOrDefault();

                if (itemResultado != null)
                {
                    existeIsSoma = true;
                    if (decimal.TryParse(item.Resultado, out valor))
                    {
                        soma += valor;
                    }

                }
            }
            if (existeIsSoma && soma != 100)
            {
                Lista.Add(new ErroDto { CodigoErro = "EXAM003", Parametros= new List<object> { soma } });
            }




        }

        void ValidarValoresConfigurados(FormatacaoItemIndexDto resultado, long sexoId)
        {
            var itemResultado = _itemResultadoRepository.GetAll()
                                                        .Where(w => w.Id == resultado.ItemId)
                                                        .FirstOrDefault();

            if (itemResultado != null && itemResultado.TipoResultadoId == (long)EnumTipoResultado.Numerico)
            {
                decimal valorResultado;

                if (decimal.TryParse(resultado.Resultado, out valorResultado))
                {
                    if (sexoId == (long)EnumSexo.Masculino)
                    {
                        if (valorResultado < itemResultado.MinimoAceitavelMasculino)
                        {
                            Lista.Add(new ErroDto { CodigoErro = "EXAM001", Parametros = new List<object> { itemResultado.Descricao } });
                        }
                        else if (valorResultado > itemResultado.MaximoAceitavelMasculino)
                        {
                            Lista.Add(new ErroDto { CodigoErro = "EXAM002", Parametros = new List<object> { itemResultado.Descricao } });
                        }
                    }
                    else
                    {
                        if (valorResultado < itemResultado.MinimoAceitavelFeminino)
                        {
                            Lista.Add(new ErroDto { CodigoErro = "EXAM001", Parametros = new List<object> { itemResultado.Descricao } });
                        }
                        else if (valorResultado > itemResultado.MaximoAceitavelFeminino)
                        {
                            Lista.Add(new ErroDto { CodigoErro = "EXAM002", Parametros = new List<object> { itemResultado.Descricao } });
                        }
                    }
                }
            }
        }

    }
}
