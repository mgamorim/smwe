﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosLaudos.Dto
{
    public class ExameResultadoDto
    {
        public DateTime? DataColeta { get; set; }
        public string Exame { get; set; }
        public string Resultado { get; set; }
        public long? TabelaId { get; set; }
        public long? TipoResultadoId { get; set; }
    }
}
