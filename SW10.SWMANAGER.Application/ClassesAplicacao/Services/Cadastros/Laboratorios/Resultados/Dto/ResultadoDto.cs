﻿using Abp.AutoMapper;
using SW10.SWMANAGER.Authorization.Users;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Atendimentos.Leitos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Atendimentos.Leitos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.CentrosCustos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Tecnicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposAcomodacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Dto
{
    [AutoMap(typeof(Resultado))]
    public class ResultadoDto : CamposPadraoCRUDDto
    {
        public long? TecnicoId { get; set; }
        public long? FaturamentoContaId { get; set; }
        public long? ResponsavelId { get; set; }
        public long? UsuarioConferidoId { get; set; }//int] NULL,
        public long? UsuarioDigitadoId { get; set; }//int] NULL,
        public long? UsuarioEntregaId { get; set; }//int] NULL,
        public long? UsuarioCienteId { get; set; }//int] NULL,
        public long? TecnicoColetaId { get; set; }//int] NULL,
        public long? LeitoAtualId { get; set; }//int] NULL,
        public long? LocalAtualId { get; set; }//int] NULL,
        public long? RotinaId { get; set; }//int] NULL,
        public long? RequisicaoMovId { get; set; }//int] NULL,
        public long? MedicoSolicitanteId { get; set; }
        public long? AtendimentoId { get; set; }


        public TecnicoDto Tecnico { get; set; }

        public AtendimentoDto Atendimento { get; set; }

        public MedicoDto MedicoSolicitante { get; set; }

        public TecnicoDto Responsavel { get; set; }

        public User UsuarioConferido { get; set; }

        public User UsuarioDigitado { get; set; }

        public User UsuarioEntrega { get; set; }

        public User UsuarioCiente { get; set; }

        public TecnicoDto TecnicoColeta { get; set; }

        public LeitoDto LeitoAtual { get; set; }

        public UnidadeOrganizacionalDto LocalAtual { get; set; }

        public bool IsRn { get; set; }//dbo].[TBitControl] NULL,

        public bool IsEmail { get; set; }//dbo].[TBitControl] NULL,

        public bool IsEmergencia { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsUrgente { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsAvisoLab { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsAvisoMed { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsVisualiza { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsRotina { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsTransferencia { get; set; }//dbo].[TBitControl] NOT NULL,

        public bool IsCiente { get; set; }//dbo].[TBitControl] NOT NULL,

        public string Numero { get; set; }
        public long Nic { get; set; }
        public DateTime? DataColeta { get; set; }
        public long? SexoRnId { get; set; }//dbo].[TSexo] NULL,	    
        public DateTime? DataDigitado { get; set; }//dbo].[TDateTime] NULL,	    
        public DateTime? DataConferido { get; set; }//dbo].[TDateTime] NULL,	    
        public DateTime? DataEnvioEmail { get; set; }//datetime] NULL,
        public DateTime? DataEntregaExame { get; set; }//datetime] NULL,	    
        public string ObsEntrega { get; set; }//varchar](100) NULL,
        public string PessoaEntrega { get; set; }//varchar](50) NULL,
        public DateTime? DataPrevEntregaExame { get; set; }//dbo].[TDateTime] NULL,
        public string Gemelar { get; set; }//varchar](5) NULL,	    
        public DateTime? DataTecnico { get; set; }//dbo].[TDateTime] NULL,
        public DateTime? DataUsuarioCiente { get; set; }//dbo].[TDateTime] NULL,
        public string Peso { get; set; }//varchar](10) NULL,
        public string Altura { get; set; }//varchar](10) NULL,
        public string Remedio { get; set; }//varchar](500) NULL,

        public string ResultadosExamesList { get; set; }
        public string PacienteNome { get; set; }

        public long AmbulatorioInternacao { get; set; }


        public long? ConvenioId { get; set; }
        public ConvenioDto Convenio { get; set; }

        public long? CentroCustoId { get; set; }
        public CentroCustoDto CentroCusto { get; set; }

        public long? TipoAcomodacaoId { get; set; }
        public TipoAcomodacaoDto TipoAcomodacao { get; set; }

        public long? TurnoId { get; set; }
        public TurnoDto Turno { get; set; }

        public string NomeMedicoSolicitante { get; set; }
        public string CRMSolicitante { get; set; }


        #region Mapeamento

        public static Resultado Mapear(ResultadoDto resultadoDto)
        {
            Resultado resultado = new Resultado();

            resultado.Id = resultadoDto.Id;
            resultado.Codigo = resultadoDto.Codigo;
            resultado.Descricao = resultadoDto.Descricao;

            resultado.TecnicoId = resultadoDto.TecnicoId;
            resultado.FaturamentoContaId = resultadoDto.FaturamentoContaId;
            resultado.ResponsavelId = resultadoDto.ResponsavelId;
            resultado.UsuarioConferidoId = resultadoDto.UsuarioConferidoId;
            resultado.UsuarioDigitadoId = resultadoDto.UsuarioDigitadoId;
            resultado.UsuarioEntregaId = resultadoDto.UsuarioEntregaId;
            resultado.UsuarioCienteId = resultadoDto.UsuarioCienteId;
            resultado.TecnicoColetaId = resultadoDto.TecnicoColetaId;
            resultado.LeitoAtualId = resultadoDto.LeitoAtualId;
            resultado.LocalAtualId = resultadoDto.LocalAtualId;
            resultado.RotinaId = resultadoDto.RotinaId;
            resultado.RequisicaoMovId = resultadoDto.RequisicaoMovId;
            resultado.MedicoSolicitanteId = resultadoDto.MedicoSolicitanteId;
            resultado.AtendimentoId = resultadoDto.AtendimentoId;
            resultado.IsRn = resultadoDto.IsRn;
            resultado.IsEmail = resultadoDto.IsEmail;
            resultado.IsEmergencia = resultadoDto.IsEmergencia;
            resultado.IsUrgente = resultadoDto.IsUrgente;
            resultado.IsAvisoLab = resultadoDto.IsAvisoLab;
            resultado.IsAvisoMed = resultadoDto.IsAvisoMed;
            resultado.IsVisualiza = resultadoDto.IsVisualiza;
            resultado.IsRotina = resultadoDto.IsRotina;
            resultado.IsTransferencia = resultadoDto.IsTransferencia;
            resultado.IsCiente = resultadoDto.IsCiente;
            resultado.Numero = resultadoDto.Numero;
            resultado.Nic = resultadoDto.Nic;
            resultado.DataColeta = resultadoDto.DataColeta;
            resultado.SexoRnId = resultadoDto.SexoRnId;
            resultado.DataDigitado = resultadoDto.DataDigitado;
            resultado.DataConferido = resultadoDto.DataConferido;
            resultado.DataEnvioEmail = resultadoDto.DataEnvioEmail;
            resultado.DataEntregaExame = resultadoDto.DataEntregaExame;
            resultado.ObsEntrega = resultadoDto.ObsEntrega;
            resultado.PessoaEntrega = resultadoDto.PessoaEntrega;
            resultado.DataPrevEntregaExame = resultadoDto.DataPrevEntregaExame;
            resultado.Gemelar = resultadoDto.Gemelar;
            resultado.DataTecnico = resultadoDto.DataTecnico;
            resultado.DataUsuarioCiente = resultadoDto.DataUsuarioCiente;
            resultado.Peso = resultadoDto.Peso;
            resultado.Altura = resultadoDto.Altura;
            resultado.Remedio = resultadoDto.Remedio;
            resultado.NomeMedicoSolicitante = resultadoDto.NomeMedicoSolicitante;
            resultado.CRMSolicitante = resultadoDto.CRMSolicitante;

            if (resultadoDto.Tecnico != null)
            {
                resultado.Tecnico = TecnicoDto.Mapear(resultadoDto.Tecnico);
            }

            if (resultadoDto.MedicoSolicitante != null)
            {
                resultado.MedicoSolicitante = MedicoDto.Mapear(resultadoDto.MedicoSolicitante);
            }

            if (resultadoDto.Atendimento != null)
            {
                resultado.Atendimento = AtendimentoDto.Mapear(resultadoDto.Atendimento);
            }

            if (resultadoDto.Responsavel != null)
            {
                resultado.Responsavel = TecnicoDto.Mapear(resultadoDto.Responsavel);
            }

            //public User UsuarioConferido 

            //public User UsuarioDigitado 

            //public User UsuarioEntrega 

            //public User UsuarioCiente 

            if (resultadoDto.TecnicoColeta != null)
            {
                resultado.TecnicoColeta = TecnicoDto.Mapear(resultadoDto.TecnicoColeta);
            }

            if (resultadoDto.LeitoAtual != null)
            {
                resultado.LeitoAtual = LeitoDto.Mapear(resultadoDto.LeitoAtual);
            }

            if (resultadoDto.LocalAtual != null)
            {
                resultado.LocalAtual = UnidadeOrganizacionalDto.Mapear(resultadoDto.LocalAtual);
            }


            resultado.ConvenioId = resultadoDto.ConvenioId;

            if (resultadoDto.Convenio != null)
            {
                resultado.Convenio = ConvenioDto.Mapear(resultadoDto.Convenio);
            }

            resultado.CentroCustoId = resultadoDto.CentroCustoId;

            if (resultadoDto.CentroCusto != null)
            {
                resultado.CentroCusto = CentroCustoDto.Mapear(resultadoDto.CentroCusto);
            }

            resultado.TipoAcomodacaoId = resultadoDto.TipoAcomodacaoId;

            if (resultadoDto.TipoAcomodacao != null)
            {
                resultado.TipoAcomodacao = TipoAcomodacaoDto.Mapear(resultadoDto.TipoAcomodacao);
            }

            resultado.TurnoId = resultadoDto.TurnoId;

            if (resultadoDto.Turno != null)
            {
                resultado.Turno = TurnoDto.Mapear(resultadoDto.Turno);
            }

            return resultado;

        }


        public static ResultadoDto Mapear(Resultado resultado)
        {
            ResultadoDto resultadoDto = new ResultadoDto();

            resultadoDto.Id = resultado.Id;
            resultadoDto.Codigo = resultado.Codigo;
            resultadoDto.Descricao = resultado.Descricao;
            resultadoDto.TecnicoId = resultado.TecnicoId;
            resultadoDto.FaturamentoContaId = resultado.FaturamentoContaId;
            resultadoDto.ResponsavelId = resultado.ResponsavelId;
            resultadoDto.UsuarioConferidoId = resultado.UsuarioConferidoId;
            resultadoDto.UsuarioDigitadoId = resultado.UsuarioDigitadoId;
            resultadoDto.UsuarioEntregaId = resultado.UsuarioEntregaId;
            resultadoDto.UsuarioCienteId = resultado.UsuarioCienteId;
            resultadoDto.TecnicoColetaId = resultado.TecnicoColetaId;
            resultadoDto.LeitoAtualId = resultado.LeitoAtualId;
            resultadoDto.LocalAtualId = resultado.LocalAtualId;
            resultadoDto.RotinaId = resultado.RotinaId;
            resultadoDto.RequisicaoMovId = resultado.RequisicaoMovId;
            resultadoDto.MedicoSolicitanteId = resultado.MedicoSolicitanteId;
            resultadoDto.AtendimentoId = resultado.AtendimentoId;
            resultadoDto.IsRn = resultado.IsRn;
            resultadoDto.IsEmail = resultado.IsEmail;
            resultadoDto.IsEmergencia = resultado.IsEmergencia;
            resultadoDto.IsUrgente = resultado.IsUrgente;
            resultadoDto.IsAvisoLab = resultado.IsAvisoLab;
            resultadoDto.IsAvisoMed = resultado.IsAvisoMed;
            resultadoDto.IsVisualiza = resultado.IsVisualiza;
            resultadoDto.IsRotina = resultado.IsRotina;
            resultadoDto.IsTransferencia = resultado.IsTransferencia;
            resultadoDto.IsCiente = resultado.IsCiente;
            resultadoDto.Numero = resultado.Numero;
            resultadoDto.Nic = resultado.Nic;
            resultadoDto.DataColeta = resultado.DataColeta;
            resultadoDto.SexoRnId = resultado.SexoRnId;
            resultadoDto.DataDigitado = resultado.DataDigitado;
            resultadoDto.DataConferido = resultado.DataConferido;
            resultadoDto.DataEnvioEmail = resultado.DataEnvioEmail;
            resultadoDto.DataEntregaExame = resultado.DataEntregaExame;
            resultadoDto.ObsEntrega = resultado.ObsEntrega;
            resultadoDto.PessoaEntrega = resultado.PessoaEntrega;
            resultadoDto.DataPrevEntregaExame = resultado.DataPrevEntregaExame;
            resultadoDto.Gemelar = resultado.Gemelar;
            resultadoDto.DataTecnico = resultado.DataTecnico;
            resultadoDto.DataUsuarioCiente = resultado.DataUsuarioCiente;
            resultadoDto.Peso = resultado.Peso;
            resultadoDto.Altura = resultado.Altura;
            resultadoDto.Remedio = resultado.Remedio;
            resultadoDto.NomeMedicoSolicitante = resultado.NomeMedicoSolicitante;
            resultadoDto.CRMSolicitante = resultado.CRMSolicitante;


            if (resultado.Tecnico != null)
            {
                resultadoDto.Tecnico = TecnicoDto.Mapear(resultado.Tecnico);
            }

            if (resultado.MedicoSolicitante != null)
            {
                resultadoDto.MedicoSolicitante = MedicoDto.Mapear(resultado.MedicoSolicitante);
            }

            if (resultado.Atendimento != null)
            {
                resultadoDto.Atendimento = AtendimentoDto.Mapear(resultado.Atendimento);
            }

            if (resultado.Responsavel != null)
            {
                resultadoDto.Responsavel = TecnicoDto.Mapear(resultado.Responsavel);
            }

            //public User UsuarioConferido 

            //public User UsuarioDigitado 

            //public User UsuarioEntrega 

            //public User UsuarioCiente 

            if (resultado.TecnicoColeta != null)
            {
                resultadoDto.TecnicoColeta = TecnicoDto.Mapear(resultado.TecnicoColeta);
            }

            if (resultado.LeitoAtual != null)
            {
                resultadoDto.LeitoAtual = LeitoDto.Mapear(resultado.LeitoAtual);
            }

            if (resultado.LocalAtual != null)
            {
                resultadoDto.LocalAtual = UnidadeOrganizacionalDto.Mapear(resultado.LocalAtual);
            }



            resultadoDto.ConvenioId = resultado.ConvenioId;

            if (resultado.Convenio != null)
            {
                resultadoDto.Convenio = ConvenioDto.Mapear(resultado.Convenio);
            }

            resultadoDto.CentroCustoId = resultado.CentroCustoId;

            if (resultado.CentroCusto != null)
            {
                resultadoDto.CentroCusto = CentroCustoDto.Mapear(resultado.CentroCusto);
            }

            resultadoDto.TipoAcomodacaoId = resultado.TipoAcomodacaoId;

            if (resultado.TipoAcomodacao != null)
            {
                resultadoDto.TipoAcomodacao = TipoAcomodacaoDto.Mapear(resultado.TipoAcomodacao);
            }

            resultadoDto.TurnoId = resultado.TurnoId;

            if (resultado.Turno != null)
            {
                resultadoDto.Turno = TurnoDto.Mapear(resultado.Turno);
            }


            return resultadoDto;

        }


        public static List<ResultadoDto> Mapear(List<Resultado> list)
        {
            List<ResultadoDto> listDto = new List<ResultadoDto>();

            foreach (var item in list)
            {
                listDto.Add(Mapear(item));
            }

            return listDto;
        }

        public static List<Resultado> Mapear(List<ResultadoDto> listDto)
        {
            List<Resultado> list = new List<Resultado>();

            foreach (var item in listDto)
            {
                list.Add(Mapear(item));
            }

            return list;
        }

        #endregion
    }
}
