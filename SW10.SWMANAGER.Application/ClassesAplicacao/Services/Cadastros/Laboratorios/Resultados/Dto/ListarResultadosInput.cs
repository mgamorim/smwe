﻿namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Dto
{
    public class ListarResultadosInput : ListarInput
    {
        public long? AtendimentoId { get; set; }
        public string TipoAtendimento { get; set; }
    }
}
