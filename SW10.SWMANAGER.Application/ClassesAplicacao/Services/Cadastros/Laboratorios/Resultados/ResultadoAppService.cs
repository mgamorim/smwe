﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Newtonsoft.Json;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Dto;
using Newtonsoft.Json.Converters;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Contas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItens.Dto;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados.Input;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Dto;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using SW10.SWMANAGER.ClassesAplicacao.Services.RegistroArquivos.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.ResultadosExames.Enumeradores;
using SW10.SWMANAGER.ClassesAplicacao.Sistemas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.ContaItenss;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Resultados
{
    public class ResultadoAppService : SWMANAGERAppServiceBase, IResultadoAppService
    {

        private readonly IListarResultadosExcelExporter _listarResultadosExcelExporter;
        private readonly IRepository<Resultado, long> _resultadoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IResultadoExameAppService _resultadoExameAppService;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IRepository<SolicitacaoExameItem, long> _solicitacaoExameItemRepository;
        private readonly IRepository<FaturamentoConta, long> _faturamentoContaRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IContaAppService _contaAppService;
        private readonly IRepository<ResultadoExame, long> _resultadoExameRepositorio;
        private readonly IRepository<FormataItem, long> _formataItemRepositorio;
        private readonly IRepository<FaturamentoItem, long> _faturamentoItemRepositorio;
        private readonly IRepository<ResultadoLaudo, long> _resultadoLaudoRepository;
        private readonly IRepository<ItemResultado, long> _itemResultadoRepository;
        private readonly IRepository<TabelaResultado, long> _tabelaResultadoRepository;
        private readonly IRepository<RegistroArquivo, long> _registroArquivoRepository;
        private readonly IRepository<Parametro, long> _parametroRepository;

        private readonly IFaturamentoContaItemAppService _faturamentoContaItemAppService;


        public ResultadoAppService(
            IRepository<Resultado, long> resultadoRepositorio,
            IListarResultadosExcelExporter listarResultadosExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            IResultadoExameAppService resultadoExameAppService,
            IUltimoIdAppService ultimoIdAppService,
            IRepository<SolicitacaoExameItem, long> solicitacaoExameItemRepository,
            IRepository<FaturamentoConta, long> faturamentoContaRepository,
            IRepository<Atendimento, long> atendimentoRepository,
            IContaAppService contaAppService,
            IRepository<ResultadoExame, long> resultadoExameRepositorio,
            IRepository<FormataItem, long> formataItemRepositorio,
            IRepository<FaturamentoItem, long> faturamentoItemRepositorio,
            IRepository<ResultadoLaudo, long> resultadoLaudoRepository,
            IRepository<ItemResultado, long> itemResultadoRepository,
            IRepository<TabelaResultado, long> tabelaResultadoRepository,
            IRepository<Resultado, long> resultadoRepository,
            IRepository<RegistroArquivo, long> registroArquivoRepository,
            IRepository<Parametro, long> parametroRepository,
            IFaturamentoContaItemAppService faturamentoContaItemAppService
            )
        {
            _resultadoRepositorio = resultadoRepositorio;
            _listarResultadosExcelExporter = listarResultadosExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _resultadoExameAppService = resultadoExameAppService;
            _ultimoIdAppService = ultimoIdAppService;
            _solicitacaoExameItemRepository = solicitacaoExameItemRepository;
            _faturamentoContaRepository = faturamentoContaRepository;
            _atendimentoRepository = atendimentoRepository;
            _contaAppService = contaAppService;
            _resultadoExameRepositorio = resultadoExameRepositorio;
            _formataItemRepositorio = formataItemRepositorio;
            _faturamentoItemRepositorio = faturamentoItemRepositorio;
            _resultadoLaudoRepository = resultadoLaudoRepository;
            _itemResultadoRepository = itemResultadoRepository;
            _tabelaResultadoRepository = tabelaResultadoRepository;
            _registroArquivoRepository = registroArquivoRepository;
            _parametroRepository = parametroRepository;
            _faturamentoContaItemAppService = faturamentoContaItemAppService;
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_DominioTiss_TiposTabelaDominio_Create, AppPermissions.Pages_Tenant_Cadastros_DominioTiss_TiposTabelaDominio_Edit)]
        public async Task CriarOuEditar(ResultadoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {

                    var resultado = ResultadoDto.Mapear(input); //input.MapTo<Resultado>();

                    var exames = JsonConvert.DeserializeObject<List<ResultadoExameIndexCrudDto>>(input.ResultadosExamesList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });

                    resultado.FaturamentoConta = null;

                    if (input.Id.Equals(0))
                    {

                        resultado.Nic = Convert.ToInt64(await _ultimoIdAppService.ObterProximoCodigo("ColetaLaboratorio"));
                        input.Id = await _resultadoRepositorio.InsertAndGetIdAsync(resultado);

                        unitOfWork.Complete();
                        //     unitOfWork.Dispose();
                        _unitOfWorkManager.Current.SaveChanges();

                    }
                    else
                    {
                        //using (var unitOfWork = _unitOfWorkManager.Begin())
                        // {
                        await _resultadoRepositorio.UpdateAsync(resultado);
                        // unitOfWork.Complete();
                        //  unitOfWork.Dispose();
                        //  _unitOfWorkManager.Current.SaveChanges();
                        //}
                    }
                    List<FaturamentoContaItemDto> itensFaturamentoIds = new List<FaturamentoContaItemDto>();

                    foreach (var _resultadoExameItem in exames)
                    {
                        if (_resultadoExameItem.Id == 0 && !_resultadoExameItem.ExameStatusId.HasValue)
                        {
                            _resultadoExameItem.ExameStatusId = 1;
                        }
                        if (_resultadoExameItem.ExameStatusId.HasValue && _resultadoExameItem.ExameStatusId != 4)
                        {
                            var faturamentoItem = _faturamentoItemRepositorio.GetAll()
                                                                             .Where(w => w.Id == _resultadoExameItem.FaturamentoItemId)
                                                                             .FirstOrDefault();

                            var _resultado = Task.Run(() => Obter(input.Id)).Result;
                            var exame = new ResultadoExameDto
                            {
                                Codigo = _resultadoExameItem.Codigo,
                                CreationTime = _resultadoExameItem.CreationTime,
                                CreatorUserId = _resultadoExameItem.CreatorUserId,
                                DataConferidoExame = _resultadoExameItem.DataConferido,
                                DataDigitadoExame = _resultadoExameItem.DataDigitado,
                                DataPendenteExame = _resultadoExameItem.DataPendente,
                                FaturamentoItemId = _resultadoExameItem.FaturamentoItemId,
                                FaturamentoContaItemId = _resultadoExameItem.FaturamentoContaItemId,
                                Id = _resultadoExameItem.Id,
                                IsDeleted = _resultadoExameItem.IsDeleted,
                                IsSigiloso = _resultadoExameItem.IsSigiloso,
                                IsSistema = _resultadoExameItem.IsSistema,
                                LastModificationTime = _resultadoExameItem.LastModificationTime,
                                LastModifierUserId = _resultadoExameItem.LastModifierUserId,
                                MaterialId = _resultadoExameItem.MaterialId,
                                MotivoPendenteExame = _resultadoExameItem.MotivoPendenteExame,
                                Observacao = _resultadoExameItem.ObservacaoExame,
                                Quantidade = _resultadoExameItem.Quantidade,
                                UsuarioConferidoExameId = _resultadoExameItem.UsuarioConferidoId,
                                UsuarioDigitadoExameId = _resultadoExameItem.UsuarioDigitadoId,
                                UsuarioPendenteExameId = _resultadoExameItem.UsuarioPendenteId,
                                Resultado = _resultado,
                                ResultadoId = input.Id,
                                FormataId = faturamentoItem?.FormataId,
                                ExameStatusId = (long)EnumStatusExame.Inicial
                            };

                            //   using (var unitOfWork = _unitOfWorkManager.Begin())
                            //  {
                            if (exame.IsDeleted)
                            {
                                await _resultadoExameAppService.Excluir(exame);
                            }
                            else
                            {
                                await _resultadoExameAppService.CriarOuEditar(exame);
                                //CarregarFaturamentoContaItem(ResultadoExameDto.Mapear(exame));
                                var _exame = ResultadoExameDto.Mapear(exame);
                                _exame.FaturamentoItem = faturamentoItem;
                                CarregarFaturamentoContaItem(_exame);

                                // salvar os itens de fatconta

                                FaturamentoContaItemInsertDto faturamentoContaItemInsertDto = new FaturamentoContaItemInsertDto();

                                faturamentoContaItemInsertDto.AtendimentoId = _exame.Resultado.AtendimentoId ?? 0;
                                faturamentoContaItemInsertDto.Data = _exame.Resultado.DataColeta;
                                faturamentoContaItemInsertDto.ItensFaturamento = new List<FaturamentoContaItemDto>();
                                var item = FaturamentoContaItemDto.MapearFromCore(_exame.FaturamentoContaItem);
                                item.Id = exame.FaturamentoItemId.Value;
                                faturamentoContaItemInsertDto.ItensFaturamento.Add(item);

                                await _faturamentoContaItemAppService.InserirItensContaFaturamento(faturamentoContaItemInsertDto);

                            }

                            var solicitacaoItem = _solicitacaoExameItemRepository.GetAll()
                                                                                 .Where(w => w.Id == _resultadoExameItem.SolicitacaoItemId)
                                                                                 .FirstOrDefault();

                            if (solicitacaoItem != null)
                            {
                                solicitacaoItem.StatusSolicitacaoExameItemId = (long)EnumSolicitacaoExameItem.Registrado;
                                _solicitacaoExameItemRepository.Update(solicitacaoItem);
                            }


                            //    unitOfWork.Complete();
                            //    _unitOfWorkManager.Current.SaveChanges();
                            //    unitOfWork.Dispose();
                            //}

                            GerarResultadoExameLaudo(exame);

                        }
                    }

                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                    _unitOfWorkManager.Current.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        void GerarResultadoExameLaudo(ResultadoExameDto exame)
        {
            try
            {
                if (exame.FormataId != null && exame.FormataId != 0)
                {
                    var formataItens = _formataItemRepositorio.GetAll()
                                                                .Where(w => w.FormataId == exame.FormataId)
                                                                .Include(i => i.ItemResultado)
                                                                .ToList();

                    foreach (var item in formataItens)
                    {
                        ResultadoLaudo resultadoLaudo;

                        resultadoLaudo = _resultadoLaudoRepository.GetAll()
                                                 .Where(w => w.ResultadoExameId == exame.Id
                                                         && w.ItemResultadoId == item.Id)
                                                 .FirstOrDefault();


                        if (resultadoLaudo == null)
                        {
                            resultadoLaudo = new ResultadoLaudo();
                        }

                        if (item.ItemResultado == null)
                        {
                            var _itemResultado = _itemResultadoRepository.Get(item.ItemResultadoId.Value);
                            item.ItemResultado = _itemResultado;
                        }
                        var itemResultado = item.ItemResultado;

                        resultadoLaudo.IsInterface = itemResultado.IsInterface;
                        resultadoLaudo.ItemResultadoId = itemResultado.Id;
                        resultadoLaudo.UnidadeId = itemResultado.LaboratorioUnidadeId;
                        resultadoLaudo.Referencia = itemResultado.Referencia;
                        resultadoLaudo.TipoResultadoId = itemResultado.TipoResultadoId;
                        resultadoLaudo.CasaDecimal = itemResultado.CasaDecimal;
                        resultadoLaudo.ResultadoExameId = exame.Id;
                        resultadoLaudo.Formula = item.Formula;

                        resultadoLaudo.MaximoAceitavelFeminino = item.ItemResultado.MaximoAceitavelFeminino;
                        resultadoLaudo.MinimoAceitavelFeminino = item.ItemResultado.MinimoAceitavelFeminino;
                        resultadoLaudo.MaximoFeminino = item.ItemResultado.MaximoFeminino;
                        resultadoLaudo.MinimoFeminino = item.ItemResultado.MinimoFeminino;
                        resultadoLaudo.NormalFeminino = item.ItemResultado.NormalFeminino;

                        resultadoLaudo.MaximoAceitavelMasculino = item.ItemResultado.MaximoAceitavelMasculino;
                        resultadoLaudo.MinimoAceitavelMasculino = item.ItemResultado.MinimoAceitavelMasculino;
                        resultadoLaudo.MaximoMasculino = item.ItemResultado.MaximoMasculino;
                        resultadoLaudo.MinimoMasculino = item.ItemResultado.MinimoMasculino;
                        resultadoLaudo.NormalMasculino = item.ItemResultado.NormalMasculino;

                        if (resultadoLaudo.Id == 0)
                        {
                            _resultadoLaudoRepository.InsertAsync(resultadoLaudo);
                        }
                        else
                        {
                            _resultadoLaudoRepository.UpdateAsync(resultadoLaudo);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroInserir", ex);
            }
        }

        public async Task Excluir(ResultadoDto input)
        {
            try
            {
                await _resultadoRepositorio.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<ListResultDto<ResultadoDto>> ListarTodos()
        {
            try
            {
                var query = await _resultadoRepositorio
                    .GetAllListAsync();

                var resultadosDto = ResultadoDto.Mapear(query);//.MapTo<List<Resultado>>();

                return new ListResultDto<ResultadoDto> { Items = resultadosDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ResultadoIndexDto>> Listar(ListarResultadosInput input)
        {
            var contar = 0;
            List<Resultado> resultados;
            List<ResultadoIndexDto> resultadosDtos = new List<ResultadoIndexDto>();
            try
            {
                DateTime dataFinal = ((DateTime)input.EndDate).Date.AddDays(1).AddMilliseconds(-1);

                var query = _resultadoRepositorio
                    .GetAll()
                    .Include(i => i.Tecnico)
                    .Include(i => i.Atendimento.Paciente.SisPessoa)
                    .Include(i => i.Atendimento.Empresa)
                    .Where(m => (input.EmpresaId == null || m.Atendimento.EmpresaId == input.EmpresaId)
                               && (input.TipoAtendimento == ""
                               || input.TipoAtendimento == null
                               || input.TipoAtendimento == string.Empty
                               || (input.TipoAtendimento == "AMB" && m.Atendimento.IsAmbulatorioEmergencia)
                               || (input.TipoAtendimento == "INT" && m.Atendimento.IsInternacao)
                               || input.TipoAtendimento == "ALL")
                               && (input.StartDate == null || m.DataColeta >= ((DateTime)input.StartDate).Date)
                               && (input.EndDate == null || m.DataColeta <= dataFinal)

                    )
                    //.Where(m => m.FaturamentoConta.AtendimentoId.IsIn(input.AtendimentoId))
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    )
                    ;

                contar = await query
                    .CountAsync();

                resultados = await query
                    .AsNoTracking()
                    //.OrderBy(input.Sorting)
                    .OrderByDescending(o => o.DataColeta)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in resultados)
                {
                    resultadosDtos.Add(new ResultadoIndexDto
                    {
                        isRn = item.IsRn,
                        DataColeta = item.DataColeta,
                        DataEntrega = item.DataEntregaExame,
                        DataTecnico = item.DataTecnico,
                        EntreguePor = item.UsuarioEntrega != null ? item.UsuarioEntrega.FullName : string.Empty,
                        MedicoSolicitante = item.MedicoSolicitante != null ? item.MedicoSolicitante.NomeCompleto : string.Empty,
                        Numero = item.Numero,
                        Nic = item.Nic,
                        Tecnico = item.Tecnico != null ? item.Tecnico.Descricao : string.Empty,
                        Id = item.Id,
                        Paciente = item.Atendimento?.Paciente?.NomeCompleto,
                        NomeMedicoSolicitante = item.NomeMedicoSolicitante,
                        EmpresaId = item.Atendimento?.EmpresaId,
                        Empresa = item.Atendimento?.Empresa?.NomeFantasia

                    });
                }
                //resultadosDtos = resultadosDtos
                //    .AsQueryable()
                //    .AsNoTracking()
                //    .OrderBy(input.Sorting)
                //    .PageBy(input)
                //    .ToList();

                return new PagedResultDto<ResultadoIndexDto>(
                    contar,
                    resultadosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultadoDto> Obter(long id)
        {
            try
            {
                var query = _resultadoRepositorio.GetAll()
                    .Include(m => m.FaturamentoConta)
                    .Include(m => m.LeitoAtual)
                    .Include(m => m.MedicoSolicitante)
                    .Include(m => m.MedicoSolicitante.SisPessoa)
                    .Include(m => m.LocalAtual)
                    .Include(m => m.Responsavel)
                    .Include(m => m.Tecnico)
                    .Include(m => m.TecnicoColeta)
                    .Include(m => m.UsuarioCiente)
                    .Include(m => m.UsuarioConferido)
                    .Include(m => m.UsuarioDigitado)
                    .Include(m => m.UsuarioEntrega)
                    .Include(m => m.Atendimento)
                    .Include(m => m.Atendimento.Paciente)
                    .Include(m => m.Atendimento.Paciente.SisPessoa)
                    .Include(m => m.Convenio)
                    .Include(m => m.Convenio.SisPessoa)
                    .Include(m => m.TipoAcomodacao)
                    .Include(m => m.Turno)
                     .Include(m => m.CentroCusto)
                     .Include(m => m.LeitoAtual)
                    .Where(m => m.Id == id);

                var resultado = await query.FirstOrDefaultAsync();

                var resultadoDto = ResultadoDto.Mapear(resultado);//.MapTo<ResultadoDto>();

                return resultadoDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _resultadoRepositorio
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                var Resultados = new ListResultDto<GenericoIdNome> { Items = query };

                List<ResultadoDto> ResultadosList = new List<ResultadoDto>();

                return Resultados;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarResultadosInput input)
        {
            try
            {
                var result = await Listar(input);
                var Resultados = result.Items;
                return _listarResultadosExcelExporter.ExportToFile(Resultados.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<ResultadoDto> pacientesDtos = new List<ResultadoDto>();
            try
            {
                //get com filtro
                var query = from p in _resultadoRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                        //||
                        //  m.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        void CarregarFaturamentoContaItem(ResultadoExame resultadoExame)
        {
            try
            {
                var faturamentoContas = _faturamentoContaRepository.GetAll()
                                                                  .Where(w => w.AtendimentoId == resultadoExame.Resultado.AtendimentoId)
                                                                  .ToList();

                var atendimento = _atendimentoRepository.GetAll()
                                                        .Where(w => w.Id == resultadoExame.Resultado.AtendimentoId)
                                                        .FirstOrDefault();

                var faturamentoConta = atendimento.IsAmbulatorioEmergencia ? faturamentoContas.FirstOrDefault() : faturamentoContas.LastOrDefault();

                if (faturamentoConta != null)
                {
                    //foreach (var item in resultado.   .LaudoMovimentoItens)
                    // {
                    if (resultadoExame.FaturamentoContaItemId == null)
                    {
                        var faturamentoContaItem = new FaturamentoContaItem();

                        faturamentoContaItem.FaturamentoItemId = resultadoExame.FaturamentoItemId;
                        faturamentoContaItem.FaturamentoItem = resultadoExame.FaturamentoItem;
                        //faturamentoContaItem.CentroCustoId = resultadoExame. .CentroCustoId;
                        faturamentoContaItem.Data = DateTime.Now;
                        faturamentoContaItem.FaturamentoContaId = faturamentoConta.Id;
                        faturamentoContaItem.MedicoId = atendimento?.MedicoId;
                        faturamentoContaItem.Observacao = resultadoExame.Observacao;
                        faturamentoContaItem.Qtde = 1;
                        // faturamentoContaItem.TipoLeitoId = laudoMovimento.TipoAcomodacaoId;
                        // faturamentoContaItem.TurnoId = laudoMovimento.TurnoId;
                        // faturamentoContaItem.UnidadeOrganizacionalId = laudoMovimento.UnidadeOrganizacionalId;


                        var contaCalculoItem = new ContaCalculoItem();

                        contaCalculoItem.EmpresaId = (long)atendimento.EmpresaId;
                        contaCalculoItem.ConvenioId = (long)atendimento.ConvenioId;
                        contaCalculoItem.PlanoId = (long)atendimento.PlanoId;

                        CalculoContaItemInput calculoContaItemInput = new CalculoContaItemInput();

                        calculoContaItemInput.conta = contaCalculoItem;
                        calculoContaItemInput.FatContaItemDto = FaturamentoContaItemDto.MapearFromCore(faturamentoContaItem);

                        faturamentoContaItem.ValorItem = AsyncHelper.RunSync(() => _faturamentoContaItemAppService.CalcularValorUnitarioContaItem(calculoContaItemInput));

                        resultadoExame.FaturamentoContaItem = faturamentoContaItem;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<PagedResultDto<ResultadoColetaIndexDto>> ListarExamesPorColeta(ResultadoColetaInput input)
        {
            var contar = 0;
            List<ResultadoExame> resultados;
            List<ResultadoColetaIndexDto> resultadosDtos = new List<ResultadoColetaIndexDto>();
            try
            {

                var query = _resultadoExameRepositorio
                    .GetAll()
                    .Include(i => i.FaturamentoItem)
                    .Where(w => w.ResultadoId == input.ColetaId);

                contar = await query
                    .CountAsync();

                resultados = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in resultados)
                {
                    resultadosDtos.Add(new ResultadoColetaIndexDto
                    {
                        ResultadoExameId = item.Id,
                        Exame = item.FaturamentoItem.Descricao,
                    });
                }
                //resultadosDtos = resultadosDtos
                //    .AsQueryable()
                //    .AsNoTracking()
                //    .OrderBy(input.Sorting)
                //    .PageBy(input)
                //    .ToList();

                return new PagedResultDto<ResultadoColetaIndexDto>(
                    contar,
                    resultadosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<FormatacaoItemIndexDto>> ListarItensFormatacaoExame(LaudoResultadoInput input)
        {
            var contar = 0;
            List<FormataItem> resultados;
            List<FormatacaoItemIndexDto> resultadosDtos = new List<FormatacaoItemIndexDto>();
            try
            {
                var exame = _faturamentoItemRepositorio.GetAll()
                                                       .Where(w => w.Id == input.ExameId)
                                                       .FirstOrDefault();

                if (exame != null)
                {
                    var query = _formataItemRepositorio

                   .GetAll()
                   .Include(i => i.ItemResultado)
                   .Include(i => i.ItemResultado.LaboratorioUnidade)
                   .Where(w => w.FormataId == exame.FormataId);



                    contar = await query
                        .CountAsync();

                    resultados = await query
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    foreach (var item in resultados)
                    {
                        resultadosDtos.Add(new FormatacaoItemIndexDto
                        {
                            ItemId = item.Id,
                            CodigoItem = item.ItemResultado.Codigo,
                            DescricaoItem = item.ItemResultado.Descricao,
                            Referencia = item.ItemResultado.Referencia,
                            Unidade = item.ItemResultado.LaboratorioUnidade?.Descricao

                        });
                    }
                    //resultadosDtos = resultadosDtos
                    //    .AsQueryable()
                    //    .AsNoTracking()
                    //    .OrderBy(input.Sorting)
                    //    .PageBy(input)
                    //    .ToList();
                }
                return new PagedResultDto<FormatacaoItemIndexDto>(
                    contar,
                    resultadosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultadoExameDto> ObterResultadoExame(long id)
        {
            try
            {
                var query = _resultadoExameRepositorio.GetAll()
                    .Include(i => i.Resultado)
                    .Include(i => i.Resultado.Atendimento)
                    .Include(i => i.FaturamentoItem)
                    .Include(i => i.Resultado.MedicoSolicitante)
                    .Include(i => i.Resultado.Atendimento.Paciente)
                    .Include(i => i.Resultado.Atendimento.Paciente.SisPessoa)
                    .Where(m => m.Id == id);

                var resultado = await query.FirstOrDefaultAsync();

                var resultadoDto = ResultadoExameDto.Mapear(resultado);//.MapTo<ResultadoDto>();

                return resultadoDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<FormatacaoItemIndexDto>> ListarItensFormatacaoPorExame(LaudoResultadoInput input)
        {
            var contar = 0;
            List<FormataItem> resultados;
            List<FormatacaoItemIndexDto> resultadosDtos = new List<FormatacaoItemIndexDto>();
            try
            {
                var exame = _faturamentoItemRepositorio.GetAll()
                                                       .Where(w => w.Id == input.ExameId)
                                                       .FirstOrDefault();

                if (exame != null)
                {
                    var query = _formataItemRepositorio

                   .GetAll()
                   .Include(i => i.ItemResultado)
                   .Include(i => i.ItemResultado.LaboratorioUnidade)
                   .Where(w => w.FormataId == exame.FormataId);
                    resultados = await query
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    foreach (var item in resultados)
                    {

                        var resultadoLaudo = _resultadoLaudoRepository.GetAll()
                                                                       .Where(w => w.ItemResultadoId == item.ItemResultadoId
                                                                                 && w.ResultadoExameId == input.ResultadoExameId)
                                                                       .FirstOrDefault();


                        resultadosDtos.Add(new FormatacaoItemIndexDto
                        {
                            // Id = item.Id,
                            ItemId = item.ItemResultadoId ?? 0,
                            CodigoItem = item.ItemResultado.Codigo,
                            DescricaoItem = item.ItemResultado.Descricao,
                            Referencia = item.ItemResultado.Referencia,
                            Unidade = item.ItemResultado.LaboratorioUnidade?.Descricao,
                            UnidadeId = item.ItemResultado.LaboratorioUnidadeId,
                            Resultado = resultadoLaudo?.Resultado,
                            LaudoResultadoId = resultadoLaudo?.Id
                        });
                    }
                    //resultadosDtos = resultadosDtos
                    //    .AsQueryable()
                    //    .AsNoTracking()
                    //    .OrderBy(input.Sorting)
                    //    .PageBy(input)
                    //    .ToList();
                }
                return resultadosDtos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<FormatacaoItemIndexDto>> ListarItensFormatacaoPorColeta(LaudoResultadoInput input)
        {
            var contar = 0;
            List<FormataItem> resultados;
            List<FormatacaoItemIndexDto> resultadosDtos = new List<FormatacaoItemIndexDto>();
            try
            {
                var exames = _resultadoExameRepositorio.GetAll()
                                                       .Include(i => i.FaturamentoItem)
                                                       .Where(w => w.ResultadoId == input.ColetaId)
                                                       .ToList();

                long id = 0;

                foreach (var exame in exames)
                {
                    if (exame != null)
                    {
                        var laudos = _resultadoLaudoRepository.GetAll()
                            .Where(w => w.ResultadoExameId == exame.Id
                                   && (w.ItemResultado.TipoResultadoId == 1
                                   || w.ItemResultado.TipoResultadoId == 2
                                   || w.ItemResultado.TipoResultadoId == 4)
                                   )
                            // .OrderBy(o=> o.)
                            .Include(i => i.ItemResultado)
                            .Include(i => i.LaboratorioUnidade)
                            .Include(i => i.ResultadoExame)
                            .ToList();

                        foreach (var item in laudos)
                        {



                            var resultado = new FormatacaoItemIndexDto();

                            //Id = ++id,
                            resultado.ItemId = item.ItemResultadoId ?? 0;
                            resultado.CodigoItem = item.ItemResultado.Codigo;
                            resultado.DescricaoItem = item.ItemResultado.Descricao;
                            resultado.Referencia = item.Referencia;
                            resultado.Unidade = item.LaboratorioUnidade?.Descricao;
                            resultado.UnidadeId = item.UnidadeId;
                            resultado.Resultado = item.Resultado;
                            resultado.LaudoResultadoId = item.Id;
                            resultado.Exame = exame.FaturamentoItem.Descricao;
                            resultado.ResultadoExameId = exame.Id;
                            resultado.TipoResultadoId = item.TipoResultadoId ?? 0;
                            resultado.GridId = ++id;
                            resultado.CasaDecimal = item.CasaDecimal;
                            resultado.TabelaId = item.ItemResultado.TabelaId;
                            resultado.ExameStatusId = item.ResultadoExame.ExameStatusId;




                            AtualizarResultadoVisualizacao(resultado);

                            resultadosDtos.Add(resultado);


                        }
                    }
                }
                return resultadosDtos;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        void AtualizarResultadoVisualizacao(FormatacaoItemIndexDto formatacaoItemIndexDto)
        {

            if (formatacaoItemIndexDto.TipoResultadoId != 4)
            {
                formatacaoItemIndexDto.ResultadoVisualizacao = formatacaoItemIndexDto.Resultado;
            }
            else
            {

                long resultado;

                long.TryParse(formatacaoItemIndexDto.Resultado, out resultado);

                var tabelaResultado = _tabelaResultadoRepository.GetAll()
                                                                .Where(w => w.TabelaId == formatacaoItemIndexDto.TabelaId
                                                                        && w.Id == resultado)
                                                                .FirstOrDefault();
                if (tabelaResultado != null)
                {
                    formatacaoItemIndexDto.ResultadoVisualizacao = tabelaResultado.Descricao;
                }
            }
        }

        public async Task<PagedResultDto<ResultadoIndexDto>> ListarNaoConferido(ListarResultadosInput input)
        {
            var contar = 0;
            List<Resultado> resultados;
            List<ResultadoIndexDto> resultadosDtos = new List<ResultadoIndexDto>();
            try
            {
                //var queryResultadoExeme = _resultadoExameRepositorio.GetAll()
                //                                                    .Where(w => w.UsuarioConferidoExameId == null);

                var query = _resultadoRepositorio
                    .GetAll()
                    .Include(i => i.Tecnico)
                    .Include(i => i.Atendimento.Paciente.SisPessoa)
                    .Where(m => (input.AtendimentoId == null || m.AtendimentoId == input.AtendimentoId)
                               // && m.UsuarioConferidoId == null && queryResultadoExeme.Any(a => a.ResultadoId == m.Id)
                               )
                    //.Where(m => m.FaturamentoConta.AtendimentoId.IsIn(input.AtendimentoId))
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.Contains(input.Filtro) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contar = await query
                    .CountAsync();

                resultados = await query
                    .AsNoTracking()
                    //.OrderBy(input.Sorting)
                    .OrderByDescending(o => o.DataColeta)
                    .PageBy(input)
                    .ToListAsync();

                foreach (var item in resultados)
                {
                    resultadosDtos.Add(new ResultadoIndexDto
                    {
                        isRn = item.IsRn,
                        DataColeta = item.DataColeta,
                        DataEntrega = item.DataEntregaExame,
                        DataTecnico = item.DataTecnico,
                        EntreguePor = item.UsuarioEntrega != null ? item.UsuarioEntrega.FullName : string.Empty,
                        MedicoSolicitante = item.MedicoSolicitante != null ? item.MedicoSolicitante.NomeCompleto : string.Empty,
                        Numero = item.Numero,
                        Nic = item.Nic,
                        Tecnico = item.Tecnico != null ? item.Tecnico.Descricao : string.Empty,
                        Id = item.Id,
                        Paciente = item.Atendimento?.Paciente?.NomeCompleto,
                        NomeMedicoSolicitante = item.NomeMedicoSolicitante,
                        Codigo = item.Nic.ToString()

                    });
                }
                //resultadosDtos = resultadosDtos
                //    .AsQueryable()
                //    .AsNoTracking()
                //    .OrderBy(input.Sorting)
                //    .PageBy(input)
                //    .ToList();

                return new PagedResultDto<ResultadoIndexDto>(
                    contar,
                    resultadosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


        public RegistroArquivoDto ObterArquivoExameColeta(long coletaId)
        {
            var examesIds = _resultadoExameRepositorio.GetAll()
                                                     .Where(w => w.ResultadoId == coletaId)
                                                     .Select(s => s.Id)
                                                     .ToList();

            Document document = new Document(PageSize.A4, 10, 10, 20, 10);
            MemoryStream ms = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            writer.PageEvent = new ITextEvents();

            var resultado = _resultadoRepositorio.GetAll()
                                                 .Where(w => w.Id == coletaId)
                                                 .Include(i => i.Atendimento)
                                                 .Include(i => i.Atendimento.Empresa)
                                                 .Include(i => i.Atendimento.Origem)
                                                 .Include(i => i.Atendimento.Paciente)
                                                 .Include(i => i.Atendimento.Paciente.SisPessoa)
                                                 .Include(i => i.Atendimento.Paciente.SisPessoa.Sexo)
                                                 .Include(i => i.Atendimento.Convenio.SisPessoa)
                                                 .FirstOrDefault();

            var formatacao = "";

            if (resultado != null)
            {
                var parametro = _parametroRepository.GetAll()
                                                    .Where(w => w.Codigo == "LABCABECEX")
                                                    .FirstOrDefault();

                if (parametro != null)
                {
                    formatacao = parametro.Descricao;

                    formatacao = formatacao.Replace("[PACIENTE]", resultado.Atendimento.Paciente.NomeCompleto);
                    formatacao = formatacao.Replace("[CONVENIO]", resultado.Atendimento.Convenio.NomeFantasia);
                    formatacao = formatacao.Replace("[MEDICO]", resultado.NomeMedicoSolicitante);
                    formatacao = formatacao.Replace("[ORIGEM]", resultado.Atendimento.Origem?.Descricao);
                    formatacao = formatacao.Replace("[CODIGO]", resultado.Atendimento.Paciente.Codigo);
                    formatacao = formatacao.Replace("[SEXO]", resultado.Atendimento.Paciente.Sexo?.Descricao);
                    formatacao = formatacao.Replace("[DTCOLETA]", string.Format("{0:dd/MM/yyyy}", resultado.DataColeta));

                    if (resultado.Atendimento.Paciente.Nascimento != null)
                    {
                        var idade = DateDifference.GetExtendedDifference((DateTime)resultado.Atendimento.Paciente.Nascimento, (DateTime)resultado.DataColeta);
                        if (idade != null)
                        {
                            string textoIdade = string.Format("{0} anos, {1} meses e {2} dias", idade.Ano, idade.Mes, idade.Dia);
                            formatacao = formatacao.Replace("[IDADE]", textoIdade);
                        }
                    }
                    formatacao = formatacao.Replace("[DTIMPRESSAO]", string.Format("{0:dd/MM/yyyy}", DateTime.Now));


                    formatacao = formatacao.Replace("[EXAME]", "exame 1");
                }

                var parametroTextoCabecalho = _parametroRepository.GetAll()
                                                                  .Where(w => w.Codigo == "LABTEXTCAB")
                                                                  .FirstOrDefault();

                if (parametroTextoCabecalho != null)
                {
                    ((ITextEvents)writer.PageEvent).TextoCabecalho = parametroTextoCabecalho.Descricao;
                }
            }


            if (resultado.Atendimento.Empresa != null
        && resultado.Atendimento.Empresa.Logotipo != null
        && resultado.Atendimento.Empresa.Logotipo.Length > 0)
            {
                Image imagemCabecalho;

                imagemCabecalho = iTextSharp.text.Image.GetInstance(resultado.Atendimento.Empresa.Logotipo);

                imagemCabecalho.ScaleAbsolute(100f, 100f);

                ((ITextEvents)writer.PageEvent).ImagemCabecalho = imagemCabecalho;

            }
            document.Open();

            PdfContentByte cb = writer.DirectContent;
            PdfImportedPage page;

            int n = 0;
            bool hasPage = false;

            ((ITextEvents)writer.PageEvent).Header = formatacao;

            foreach (var exameId in examesIds)
            {
                var registroArquivo = _registroArquivoRepository.GetAll()
                                                                .Where(w => w.RegistroId == exameId
                                                                         && w.RegistroTabelaId == (long)EnumArquivoTabela.LaboratorioExame)
                                                                .OrderByDescending(o => o.Id)
                                                                .FirstOrDefault();

                if (registroArquivo != null)
                {
                    PdfReader reader = new PdfReader(registroArquivo.Arquivo);
                    n = reader.NumberOfPages;
                    document.SetPageSize(reader.GetPageSizeWithRotation(1));
                    document.NewPage();
                    page = writer.GetImportedPage(reader, 1);
                    cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                    hasPage = true;
                }
            }


            RegistroArquivoDto registroArquivoDto = new RegistroArquivoDto();
            if (hasPage)
            {
                document.Close();
                registroArquivoDto.Arquivo = ms.ToArray();
            }

            return registroArquivoDto;
        }


    }
}






public class ITextEvents : PdfPageEventHelper
{
    // This is the contentbyte object of the writer
    PdfContentByte cb;

    // we will put the final number of pages in a template
    PdfTemplate headerTemplate, footerTemplate;

    // this is the BaseFont we are going to use for the header / footer
    BaseFont bf = null;

    // This keeps track of the creation time
    DateTime PrintTime = DateTime.Now;

    #region Fields
    private string _header;
    public Image ImagemCabecalho { get; set; }
    public string TextoCabecalho { get; set; }
    #endregion

    #region Properties
    public string Header
    {
        get { return _header; }
        set { _header = value; }
    }
    #endregion    

    public override void OnOpenDocument(PdfWriter writer, Document document)
    {
        try
        {
            PrintTime = DateTime.Now;
            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb = writer.DirectContent;
            headerTemplate = cb.CreateTemplate(15, 15);
            footerTemplate = cb.CreateTemplate(10, 10);

            headerTemplate.Height = 10;
            //ImagemCabecalho.SetAbsolutePosition(20, 10);// (document.PageSize.Height - 1500));
            // ImagemCabecalho.Width = 50;

        }
        catch (DocumentException de)
        {
        }
        catch (System.IO.IOException ioe)
        {
        }
    }

    public override void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
    {
        try
        {
            base.OnEndPage(writer, document);
            iTextSharp.text.Font baseFontNormal = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font baseFontBig = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12f, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            HTMLWorker obj = new HTMLWorker(document);
            StringReader se = new StringReader(_header);
            obj.Parse(se);


            PdfPTable tbHeader = new PdfPTable(3);

            PdfPCell logocell = new PdfPCell(ImagemCabecalho, true);
            logocell.Border = 0;
            tbHeader.AddCell(logocell);

            PdfPCell cell2 = new PdfPCell(new Phrase("Laboratório de Análise Clínicas"));
            cell2.Border = 0;
            PdfPCell cell3 = new PdfPCell(new Phrase(TextoCabecalho, new Font(null, 9)));
            cell3.Border = 0;
            cell3.HorizontalAlignment = Element.ALIGN_CENTER;


            tbHeader.AddCell(cell2);
            tbHeader.AddCell(cell3);

            tbHeader.TotalWidth = document.PageSize.Width - 50;
            tbHeader.WriteSelectedRows(0, -1, 2, document.PageSize.Height - 5, writer.DirectContent);

            tbHeader.HorizontalAlignment = Element.ALIGN_CENTER;





            // Phrase p1Header = new Phrase(_header, baseFontNormal);


            ////Create PdfTable object
            //PdfPTable pdfTab = new PdfPTable(3);

            ////We will have to create separate cells to include image logo and 2 separate strings
            ////Row 1
            //PdfPCell pdfCell1 = new PdfPCell(ImagemCabecalho);
            ////PdfPCell pdfCell2 = new PdfPCell();
            ////PdfPCell pdfCell3 = new PdfPCell();
            //String text = "Page " + writer.PageNumber + " of ";


            ////Add paging to header
            //{
            //    cb.BeginText();
            //    cb.SetFontAndSize(bf, 12);
            //    cb.SetTextMatrix(document.PageSize.GetRight(200), document.PageSize.GetTop(45));
            //    cb.SetTextMatrix(document.LeftMargin, document.PageSize.Height - document.TopMargin);
            //    // cb.ShowText(text);
            //    cb.EndText();
            //    float len = bf.GetWidthPoint(text, 12);
            //    //Adds "12" in Page 1 of 12
            //    cb.AddTemplate(headerTemplate, document.PageSize.GetRight(200) + len, document.PageSize.GetTop(45));
            //}
            ////Add paging to footer
            //{
            //    cb.BeginText();
            //    cb.SetFontAndSize(bf, 12);
            //    cb.SetTextMatrix(document.PageSize.GetRight(180), document.PageSize.GetBottom(30));
            //    //cb.ShowText(text);
            //    cb.EndText();
            //    //  float len = bf.GetWidthPoint(text, 12);
            //    //cb.AddTemplate(footerTemplate, document.PageSize.GetRight(180) + len, document.PageSize.GetBottom(30));
            //}

            ////Row 2
            ////PdfPCell pdfCell4 = new PdfPCell(new Phrase("Sub Header Description", baseFontNormal));

            //////Row 3 
            ////PdfPCell pdfCell5 = new PdfPCell(new Phrase("Date:" + PrintTime.ToShortDateString(), baseFontBig));
            ////PdfPCell pdfCell6 = new PdfPCell();
            ////PdfPCell pdfCell7 = new PdfPCell(new Phrase("TIME:" + string.Format("{0:t}", DateTime.Now), baseFontBig));

            ////set the alignment of all three cells and set border to 0
            //pdfCell1.HorizontalAlignment = Element.ALIGN_CENTER;
            ////pdfCell2.HorizontalAlignment = Element.ALIGN_CENTER;
            ////pdfCell3.HorizontalAlignment = Element.ALIGN_CENTER;
            ////pdfCell4.HorizontalAlignment = Element.ALIGN_CENTER;
            ////pdfCell5.HorizontalAlignment = Element.ALIGN_CENTER;
            ////pdfCell6.HorizontalAlignment = Element.ALIGN_CENTER;
            ////pdfCell7.HorizontalAlignment = Element.ALIGN_CENTER;

            ////pdfCell2.VerticalAlignment = Element.ALIGN_BOTTOM;
            ////pdfCell3.VerticalAlignment = Element.ALIGN_MIDDLE;
            ////pdfCell4.VerticalAlignment = Element.ALIGN_TOP;
            ////pdfCell5.VerticalAlignment = Element.ALIGN_MIDDLE;
            ////pdfCell6.VerticalAlignment = Element.ALIGN_MIDDLE;
            ////pdfCell7.VerticalAlignment = Element.ALIGN_MIDDLE;

            ////pdfCell4.Colspan = 3;

            //pdfCell1.Border = 0;
            ////pdfCell2.Border = 0;
            ////pdfCell3.Border = 0;
            ////pdfCell4.Border = 0;
            ////pdfCell5.Border = 0;
            ////pdfCell6.Border = 0;
            ////pdfCell7.Border = 0;

            //////add all three cells into PdfTable
            //pdfTab.AddCell(pdfCell1);
            ////pdfTab.AddCell(pdfCell2);
            ////pdfTab.AddCell(pdfCell3);
            ////pdfTab.AddCell(pdfCell4);
            ////pdfTab.AddCell(pdfCell5);
            ////pdfTab.AddCell(pdfCell6);
            ////pdfTab.AddCell(pdfCell7);

            //pdfTab.TotalWidth = document.PageSize.Width - 80f;
            //pdfTab.WidthPercentage = 70;
            //pdfTab.HorizontalAlignment = Element.ALIGN_CENTER;

            ////call WriteSelectedRows of PdfTable. This writes rows from PdfWriter in PdfTable
            ////first param is start row. -1 indicates there is no end row and all the rows to be included to write
            ////Third and fourth param is x and y position to start writing
            //pdfTab.WriteSelectedRows(0, -1, 40, document.PageSize.Height - 30, writer.DirectContent);
            ////set pdfContent value

            ////Move the pointer and draw line to separate header section from rest of page
            cb.MoveTo(10, document.PageSize.Height - 160);
            cb.LineTo(document.PageSize.Width - 10, document.PageSize.Height - 160);
            cb.Stroke();

            ////Move the pointer and draw line to separate footer section from rest of page
            //cb.MoveTo(40, document.PageSize.GetBottom(50));
            //cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
            //cb.Stroke();
        }
        catch (Exception ex)
        {

        }
    }

    public override void OnCloseDocument(PdfWriter writer, Document document)
    {
        base.OnCloseDocument(writer, document);

        headerTemplate.BeginText();
        headerTemplate.SetFontAndSize(bf, 12);
        headerTemplate.SetTextMatrix(0, 0);
        headerTemplate.ShowText((writer.PageNumber - 1).ToString());
        headerTemplate.EndText();

        footerTemplate.BeginText();
        footerTemplate.SetFontAndSize(bf, 12);
        footerTemplate.SetTextMatrix(0, 0);
        footerTemplate.ShowText((writer.PageNumber - 1).ToString());
        footerTemplate.EndText();
    }
}
