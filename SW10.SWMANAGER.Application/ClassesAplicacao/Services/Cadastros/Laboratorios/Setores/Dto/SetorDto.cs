﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Laboratorios;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Laboratorios.Setores.Dto
{
    [AutoMap(typeof(Setor))]
    public class SetorDto : CamposPadraoCRUDDto
    {
        public int OrdemSetor { get; set; }
    }
}
