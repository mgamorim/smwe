﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosCaucao.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosCaucao.Exporting
{
    public interface IListarMotivosCaucaoExcelExporter
    {
        FileDto ExportToFile(List<MotivoCaucaoDto> MotivoCaucaoDtos);
    }
}
