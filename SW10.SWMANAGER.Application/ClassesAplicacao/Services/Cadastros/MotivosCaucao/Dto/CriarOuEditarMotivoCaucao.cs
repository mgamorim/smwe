﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.MotivosCaucao;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.MotivosCaucao.Dto
{
    [AutoMap(typeof(MotivoCaucao))]
    public class CriarOuEditarMotivoCaucao : CamposPadraoCRUDDto
    {

    }
}
