﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos.Dto;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Dto;
using System.Collections.Generic;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos
{
	public interface IGrupoAppService : IApplicationService
    {
        Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input);

        //ListResultDto<TipoAtendimentoDto> GetTiposAtendimento(GetTiposAtendimentoInput input);
        Task<PagedResultDto<GrupoDto>> Listar(ListarGruposInput input);

        Task<ListResultDto<GrupoDto>> ListarTodos();

        Task<GrupoDto> CriarOuEditar(GrupoDto input);

        Task Excluir(GrupoDto input);

        Task<GrupoDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarGruposInput input);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownGruposPorEstoque(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarDropdownGruposPorEstoqueIdObrigatorio(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarPorEstoqueDropdown(DropdownInput dropdownInput);
    }
}
