﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos.Exporting
{
    public interface IListarGrupoExcelExporter
    {
        FileDto ExportToFile(List<GrupoDto> produtoEspecieDtos);
    }
}