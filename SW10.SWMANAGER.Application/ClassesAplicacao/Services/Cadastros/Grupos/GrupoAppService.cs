﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Authorization;
using SW10.SWMANAGER.Authorization;
using System.Data.Entity;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using Abp.Domain.Uow;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.GruposClasse;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Grupos
{
    public class GrupoAppService : SWMANAGERAppServiceBase, IGrupoAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Grupo, long> _grupoRepositorio;
        private readonly IRepository<GrupoClasse, long> _classeRepository;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepository;
        private readonly IListarGrupoExcelExporter _listarGrupoExcelExporter;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IGrupoClasseAppService _classeAppService;

        public GrupoAppService(
            IRepository<Grupo, long> GrupoRepositorio,
            IRepository<EstoqueGrupo, long> estoqueGrupoRepository,
            IRepository<GrupoClasse, long> classeRepository,
            IUnitOfWorkManager UnitOfWorkManager,
            IListarGrupoExcelExporter listarGrupoExcelExporter,
            IUltimoIdAppService ultimoServicoAppService,
            IGrupoClasseAppService classeAppService
            )
        {
            _unitOfWorkManager = UnitOfWorkManager;
            _grupoRepositorio = GrupoRepositorio;
            _estoqueGrupoRepository = estoqueGrupoRepository;
            _classeRepository = classeRepository;
            _listarGrupoExcelExporter = listarGrupoExcelExporter;
            _ultimoIdAppService = ultimoServicoAppService;
            _classeAppService = classeAppService;
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _grupoRepositorio
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper())
                        )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Grupo_Create, AppPermissions.Pages_Tenant_Cadastros_CadastrosSuprimentos_Grupo_Edit)]
        [UnitOfWork]
        public async Task<GrupoDto> CriarOuEditar(GrupoDto input)
        {
            try
            {
                var grupo = input.MapTo<Grupo>();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var classes = new List<GrupoClasseDto>();
                    if (!input.ClasseList.IsNullOrWhiteSpace())
                    {
                        classes = JsonConvert.DeserializeObject<List<GrupoClasseDto>>(input.ClasseList);
                    }

                    if (input.Id.Equals(0))
                    {
                        grupo.Codigo = _ultimoIdAppService.ObterProximoCodigo("grupo").Result;
                        grupo.Id = await _grupoRepositorio.InsertAndGetIdAsync(grupo);
                    }
                    else
                    {
                        await _grupoRepositorio.UpdateAsync(grupo);
                    }

                    foreach (var classe in classes)
                    {
                        if (input.Id.Equals(0))
                        {
                            classe.GrupoId = grupo.Id;
                        }
                        else
                        {
                            classe.GrupoId = input.Id;
                        }

                        if (classe.IsDeleted)
                        {
                            await _classeAppService.Excluir(classe);
                        }
                        else
                        {
                            await _classeAppService.CriarOuEditar(classe);
                        }

                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                return grupo.MapTo<GrupoDto>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        [UnitOfWork]
        public async Task Excluir(GrupoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _grupoRepositorio.DeleteAsync(input.Id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<ListResultDto<GrupoDto>> ListarTodos()
        {
            List<Grupo> listCore;
            List<GrupoDto> listDtos = new List<GrupoDto>();
            try
            {
                listCore = await _grupoRepositorio
                    .GetAll()
                    .ToListAsync();

                listDtos = listCore
                    .MapTo<List<GrupoDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new ListResultDto<GrupoDto> { Items = listDtos };
        }

        public async Task<PagedResultDto<GrupoDto>> Listar(ListarGruposInput input)
        {
            var contarGrupo = 0;
            List<Grupo> Grupo;
            List<GrupoDto> GrupoDtos = new List<GrupoDto>();
            try
            {
                var query = _grupoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    //m.Palavra.Contains(input.Filtro) ||
                    //m.Observacao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarGrupo = await query
                    .CountAsync();

                Grupo = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                GrupoDtos = Grupo
                    .MapTo<List<GrupoDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<GrupoDto>(
                contarGrupo,
                GrupoDtos
                );
        }

        /// <summary>
        /// Cria um novo grupo e retorna um GrupoDto com seu Id
        /// </summary>
        [UnitOfWork]
        public GrupoDto CriarGetId(GrupoDto input)
        {
            try
            {
                //input.Codigo = ObterProximoNumero(input);
                GrupoDto grupoDto;
                var grupo = input.MapTo<Grupo>();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.Id.Equals(0))
                    {
                        //Inclui o grupo e retorna grupoDto com o Id
                        grupoDto = new GrupoDto { Id = AsyncHelper.RunSync(() => _grupoRepositorio.InsertAndGetIdAsync(grupo)) };
                    }
                    else
                    {
                        grupoDto = AsyncHelper.RunSync(() => _grupoRepositorio.UpdateAsync(grupo)).MapTo<GrupoDto>();
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                //grupoDto.Codigo = input.Codigo;
                return grupoDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task<FileDto> ListarParaExcel(ListarGruposInput input)
        {
            try
            {
                var query = await Listar(input);

                var GrupoDtos = query.Items;

                return _listarGrupoExcelExporter.ExportToFile(GrupoDtos.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<GrupoDto> Obter(long id)
        {
            try
            {
                var result = await _grupoRepositorio.GetAsync(id);
                var grupo = result.MapTo<GrupoDto>();
                return grupo;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            //return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoItemRepositorio);
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {

                var query = from g in _grupoRepositorio.GetAll()
                            //.Include(m => m.Divisao)
                            //.Include(m => m.Divisao.Subdivisoes)
                            //.Where(m => (m.Divisao.IsDivisaoPrincipal && m.Divisao.Subdivisoes.Count() == 0))
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby g.Descricao ascending
                            select new DropdownItems { id = g.Id, text = string.Concat(g.Codigo.ToString(), " - ", g.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ResultDropdownList> ListarPorEstoqueDropdown(DropdownInput dropdownInput)
        {
            //return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoItemRepositorio);
            long estoqueId = 0;
            var isEstoque = long.TryParse(dropdownInput.id, out estoqueId);
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from g in _estoqueGrupoRepository.GetAll()
                            .Include(m => m.Grupo)
                            //.Include(m => m.Divisao.Subdivisoes)
                            //.Where(m => (m.Divisao.IsDivisaoPrincipal && m.Divisao.Subdivisoes.Count() == 0))
                            .WhereIf(estoqueId > 0, m =>
                                m.EstoqueId.Equals(estoqueId)
                            )
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            orderby g.Grupo.Descricao ascending
                            select new DropdownItems { id = g.Grupo.Id, text = string.Concat(g.Grupo.Codigo.ToString(), " - ", g.Grupo.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ResultDropdownList> ListarDropdownGruposPorEstoque(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<GrupoDto> dtos = new List<GrupoDto>();
            try
            {
                //se um país estiver selecionado, filtra apenas pelos seus estados
                long estoqueId;
                long.TryParse(dropdownInput.filtro, out estoqueId);

                //get com filtro
                var query = (from p in _estoqueGrupoRepository.GetAll()
                            .WhereIf(!dropdownInput.filtro.IsNullOrEmpty(), m => m.EstoqueId == estoqueId)
                             join grupo in _grupoRepositorio.GetAll() on p.GrupoId equals grupo.Id
                             select grupo)
                            .Distinct();

                var query2 = query
                        .Where(m => m.Descricao != null && m.Descricao.Trim() != "")
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        );

                var lista = from ss in query2
                            orderby ss.Descricao ascending
                            select new DropdownItems { id = ss.Id, text = string.Concat(ss.Id, " - ", ss.Codigo, " - ", ss.Descricao) };

                //paginação 
                var queryResultPage = lista//query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdownGruposPorEstoqueIdObrigatorio(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<GrupoDto> dtos = new List<GrupoDto>();
            try
            {
                //se um país estiver selecionado, filtra apenas pelos seus estados
                long estoqueId;
                long.TryParse(dropdownInput.filtro, out estoqueId);

                if (estoqueId == 0)
                {
                    return new ResultDropdownList() { Items = new List<DropdownItems>(), TotalCount = 0 };
                }

                //get com filtro
                var query = (from p in _estoqueGrupoRepository.GetAll()
                            .WhereIf(!dropdownInput.filtro.IsNullOrEmpty(), m => m.EstoqueId == estoqueId)
                             join grupo in _grupoRepositorio.GetAll() on p.GrupoId equals grupo.Id
                             select grupo)
                            .Distinct();

                var query2 = query
                        .Where(m => m.Descricao != null && m.Descricao.Trim() != "")
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        );

                var lista = from ss in query2
                            orderby ss.Descricao ascending
                            select new DropdownItems { id = ss.Id, text = string.Concat(ss.Id, " - ", ss.Codigo, " - ", ss.Descricao) };

                //paginação 
                var queryResultPage = lista//query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


    }
}
