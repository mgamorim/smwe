﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto
{
	[AutoMap(typeof(Estado))]
    public class EstadoDto : CamposPadraoCRUDDto
    {
        public string Nome { get; set; }

        public string Uf { get; set; }

        [ForeignKey("PaisId")]
        public virtual PaisDto Pais { get; set; }
        public long PaisId { get; set; }

        //public virtual ICollection<CidadeDto> Cidades { get; set; }
    }
}
