﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Regioes.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Regioes.Exporting
{
    public interface IListarRegiaoExcelExporter
    {
        FileDto ExportToFile(List<RegiaoDto> tipoAtendimentoDtos);
    }
}
