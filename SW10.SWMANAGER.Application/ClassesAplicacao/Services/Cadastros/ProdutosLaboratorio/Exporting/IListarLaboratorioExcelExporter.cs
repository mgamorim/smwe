﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Exporting
{
    public interface IListarProdutoLaboratorioExcelExporter
    {
        FileDto ExportToFile(List<ProdutoLaboratorioDto> produtoLaboratorioDtos);
    }
}
