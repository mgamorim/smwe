﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosLaboratorio;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Fornecedores.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto
{
    [AutoMap(typeof(EstoqueLaboratorio))]
    public class ProdutoLaboratorioDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public long FornecedorId { get; set; }

        public long BrasLaboratorioId { get; set; }        

        //    public virtual FornecedorDto Fornecedor { get; set; }

    }
}
