﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio.Dto;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosLaboratorio
{
    public interface IProdutoLaboratorioAppService : IApplicationService
    {
        //ListResultDto<TipoAtendimentoDto> GetTiposAtendimento(GetTiposAtendimentoInput input);
        Task<PagedResultDto<ProdutoLaboratorioDto>> Listar(ListarProdutosLaboratorioInput input);

        Task<ListResultDto<ProdutoLaboratorioDto>> ListarTodos();

        Task CriarOuEditar(ProdutoLaboratorioDto input);

        Task Excluir(ProdutoLaboratorioDto input);

        Task<ProdutoLaboratorioDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarProdutosLaboratorioInput input);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
    }
}
