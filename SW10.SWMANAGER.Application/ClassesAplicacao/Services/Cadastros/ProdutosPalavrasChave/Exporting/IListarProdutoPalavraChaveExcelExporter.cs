﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosPalavrasChave.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosPalavrasChave.Exporting
{
    public interface IListarProdutoPalavraChaveExcelExporter
    {
        FileDto ExportToFile(List<ProdutoPalavraChaveDto> produtoPalavraChaveDtos);
    }
}