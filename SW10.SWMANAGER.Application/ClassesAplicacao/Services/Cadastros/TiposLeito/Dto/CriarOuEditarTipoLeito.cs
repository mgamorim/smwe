﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
//using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposLeito;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLeito.Dto
{

   // [AutoMap(typeof(TipoLeito))]
    public class CriarOuEditarTipoLeito : CamposPadraoCRUDDto
    {

        public string TipoLeitoId { get; set; }

        public string CodigoTiss { get; set; }

        public bool IsLeitoExtra { get; set; }

    }
}
