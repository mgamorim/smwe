﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLeito.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposTiposGrupoCentroCustoLeitos.Exporting
{
    public interface IListarTipoLeitoExcelExporter
    {
        FileDto ExportToFile(List<TipoLeitoDto> tipoLeitosDtos);
    }
}
