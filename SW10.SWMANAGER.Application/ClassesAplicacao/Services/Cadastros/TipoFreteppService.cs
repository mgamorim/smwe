﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.UI;
using Abp.Domain.Uow;



using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Movimentos
{
    public class TipoFreteAppService : SWMANAGERAppServiceBase, ITipoFreteAppService
    {
        private readonly IRepository<TipoFrete, long> _TipoFreteRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TipoFreteAppService(
         IRepository<TipoFrete, long> TipoFreteRepository,
         IUnitOfWorkManager unitOfWorkManager

         )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _TipoFreteRepository = TipoFreteRepository;
        }



        public async Task<ListResultDto<TipoFreteDto>> ListarTodos()
        {
            var contarTipoFrete = 0;
            List<TipoFrete> TipoFretes;
            List<TipoFreteDto> TipoFreteDtos = new List<TipoFreteDto>();
            try
            {
                var query = _TipoFreteRepository
                    .GetAll();

                contarTipoFrete = await query
                    .CountAsync();

                TipoFretes = await query
                    .AsNoTracking()
                    .ToListAsync();

                TipoFreteDtos = TipoFretes
                    .MapTo<List<TipoFreteDto>>();

                return new ListResultDto<TipoFreteDto> { Items = TipoFreteDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _TipoFreteRepository);
        }

        public async Task<TipoFreteDto> Obter(long id)
        {
            try
            {
                var tipoFrete = await _TipoFreteRepository.GetAsync(id);

                return tipoFrete.MapTo<TipoFreteDto>();
            }
            catch(Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }


    }
}
