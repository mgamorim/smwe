﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao.Dto;
using SW10.SWMANAGER.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao
{
    public interface IFormaAplicacaoAppService : IApplicationService
    {
        Task<PagedResultDto<FormaAplicacaoDto>> Listar(ListarInput input);

        Task<ListResultDto<FormaAplicacaoDto>> ListarTodos();

        Task<ListResultDto<FormaAplicacaoDto>> ListarFiltro(string filtro);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<FormaAplicacaoDto> CriarOuEditar(FormaAplicacaoDto input);

        Task Excluir(FormaAplicacaoDto input);

        Task<FormaAplicacaoDto> Obter(long id);

        //Task<FileDto> ListarParaExcel(ListarInput input);
    }
}
