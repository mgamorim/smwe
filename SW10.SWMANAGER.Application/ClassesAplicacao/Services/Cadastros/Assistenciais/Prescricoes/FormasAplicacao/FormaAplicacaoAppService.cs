﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.FormasAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao
{
    public class FormaAplicacaoAppService : SWMANAGERAppServiceBase, IFormaAplicacaoAppService
    {
        private readonly IRepository<FormaAplicacao, long> _formaAplicacaoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FormaAplicacaoAppService(
            IRepository<FormaAplicacao, long> formaAplicacaoRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _formaAplicacaoRepositorio = formaAplicacaoRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<FormaAplicacaoDto> CriarOuEditar(FormaAplicacaoDto input)
        {
            try
            {
                var formaAplicacao = input.MapTo<FormaAplicacao>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _formaAplicacaoRepositorio.InsertAndGetIdAsync(formaAplicacao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        formaAplicacao = await _formaAplicacaoRepositorio.UpdateAsync(formaAplicacao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(FormaAplicacaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _formaAplicacaoRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<FormaAplicacaoDto>> Listar(ListarInput input)
        {
            var contarFormaAplicacao = 0;
            List<FormaAplicacao> formaAplicacao;
            List<FormaAplicacaoDto> FormaAplicacaoDtos = new List<FormaAplicacaoDto>();
            try
            {
                var query = _formaAplicacaoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarFormaAplicacao = await query
                    .CountAsync();

                formaAplicacao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                FormaAplicacaoDtos = formaAplicacao
                    .MapTo<List<FormaAplicacaoDto>>();

                return new PagedResultDto<FormaAplicacaoDto>(
                    contarFormaAplicacao,
                    FormaAplicacaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<FormaAplicacaoDto> Obter(long id)
        {
            try
            {
                var result = await _formaAplicacaoRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var formaAplicacao = result.MapTo<FormaAplicacaoDto>();

                return formaAplicacao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<FormaAplicacaoDto> Obter(string formaAplicacao)
        {
            try
            {
                var query = _formaAplicacaoRepositorio
                    .GetAll()
                    .WhereIf(!formaAplicacao.IsNullOrWhiteSpace(), m =>
                     m.Codigo.ToUpper().Contains(formaAplicacao.ToUpper()) ||
                     m.Descricao.ToUpper().Contains(formaAplicacao.ToUpper())
                    );
                var result = await query.FirstOrDefaultAsync();
                var _formaAplicacao = result.MapTo<FormaAplicacaoDto>();

                return _formaAplicacao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<FormaAplicacaoDto>> ListarTodos()
        {
            try
            {
                var query = _formaAplicacaoRepositorio
                    .GetAll();

                var formaAplicacao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formasAplicacaoDto = formaAplicacao
                    .MapTo<List<FormaAplicacaoDto>>();

                return new ListResultDto<FormaAplicacaoDto>
                {
                    Items = formasAplicacaoDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<FormaAplicacaoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _formaAplicacaoRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var formaAplicacao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var formasAplicacaoDto = formaAplicacao
                    .MapTo<List<FormaAplicacaoDto>>();

                return new ListResultDto<FormaAplicacaoDto>
                {
                    Items = formasAplicacaoDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput,_formaAplicacaoRepositorio);
        }

        //public Task<FileDto> ListarParaExcel(ListarInput input)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
