﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Frequencias;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias.Dto
{
    [AutoMap(typeof(Frequencia))]
    public class FrequenciaDto : CamposPadraoCRUDDto
    {
        public long Intervalo { get; set; }
        public string Horarios { get; set; }
        public string HoraInicialMedicacao { get; set; }
        public bool IsSos { get; set; }


        public static FrequenciaDto Mapear(Frequencia input)
        {
            var result = new FrequenciaDto();
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.Descricao = input.Descricao;
            result.HoraInicialMedicacao = input.HoraInicialMedicacao;
            result.Horarios = input.Horarios;
            result.Id = input.Id;
            result.Intervalo = input.Intervalo;
            result.IsSistema = input.IsSistema;
            result.IsSos = input.IsSos;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;

            return result;
        }

        public static Frequencia Mapear(FrequenciaDto input)
        {
            var result = new Frequencia();
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.Descricao = input.Descricao;
            result.HoraInicialMedicacao = input.HoraInicialMedicacao;
            result.Horarios = input.Horarios;
            result.Id = input.Id;
            result.Intervalo = input.Intervalo;
            result.IsSistema = input.IsSistema;
            result.IsSos = input.IsSos;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;

            return result;
        }

        public static IEnumerable<FrequenciaDto> Mapear(List<Frequencia> input)
        {
            foreach (var item in input)
            {
                var result = new FrequenciaDto();
                result.Codigo = item.Codigo;
                result.CreationTime = item.CreationTime;
                result.CreatorUserId = item.CreatorUserId;
                result.Descricao = item.Descricao;
                result.HoraInicialMedicacao = item.HoraInicialMedicacao;
                result.Horarios = item.Horarios;
                result.Id = item.Id;
                result.Intervalo = item.Intervalo;
                result.IsSistema = item.IsSistema;
                result.IsSos = item.IsSos;
                result.LastModificationTime = item.LastModificationTime;
                result.LastModifierUserId = item.LastModifierUserId;

                yield return result;
            }
        }

        public static IEnumerable<Frequencia> Mapear(List<FrequenciaDto> input)
        {
            foreach (var item in input)
            {
                var result = new Frequencia();
                result.Codigo = item.Codigo;
                result.CreationTime = item.CreationTime;
                result.CreatorUserId = item.CreatorUserId;
                result.Descricao = item.Descricao;
                result.HoraInicialMedicacao = item.HoraInicialMedicacao;
                result.Horarios = item.Horarios;
                result.Id = item.Id;
                result.Intervalo = item.Intervalo;
                result.IsSistema = item.IsSistema;
                result.IsSos = item.IsSos;
                result.LastModificationTime = item.LastModificationTime;
                result.LastModifierUserId = item.LastModifierUserId;

                yield return result;
            }
        }

    }
}
