﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias.Dto;
using SW10.SWMANAGER.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias
{
    public interface IFrequenciaAppService : IApplicationService
    {
        Task<PagedResultDto<FrequenciaDto>> Listar(ListarInput input);

        Task<ListResultDto<FrequenciaDto>> ListarTodos();

        Task<ListResultDto<FrequenciaDto>> ListarFiltro(string filtro);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<FrequenciaDto> CriarOuEditar(FrequenciaDto input);

        Task Excluir(FrequenciaDto input);

        Task<FrequenciaDto> Obter(long id);

        Task<FileDto> ListarParaExcel(ListarInput input);
    }
}
