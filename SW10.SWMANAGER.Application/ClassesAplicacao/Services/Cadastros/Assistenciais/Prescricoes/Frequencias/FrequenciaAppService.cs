﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Frequencias;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias
{
    public class FrequenciaAppService : SWMANAGERAppServiceBase, IFrequenciaAppService
    {
        private readonly IRepository<Frequencia, long> _frequenciaRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FrequenciaAppService(
            IRepository<Frequencia, long> frequenciaRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _frequenciaRepositorio = frequenciaRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<FrequenciaDto> CriarOuEditar(FrequenciaDto input)
        {
            try
            {
                var frequencia = input.MapTo<Frequencia>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _frequenciaRepositorio.InsertAndGetIdAsync(frequencia);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        frequencia = await _frequenciaRepositorio.UpdateAsync(frequencia);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(FrequenciaDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _frequenciaRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<FrequenciaDto>> Listar(ListarInput input)
        {
            var contarFrequencia = 0;
            List<Frequencia> frequencia;
            List<FrequenciaDto> FrequenciaDtos = new List<FrequenciaDto>();
            try
            {
                var query = _frequenciaRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarFrequencia = await query
                    .CountAsync();

                frequencia = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                FrequenciaDtos = frequencia
                    .MapTo<List<FrequenciaDto>>();

                return new PagedResultDto<FrequenciaDto>(
                    contarFrequencia,
                    FrequenciaDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<FrequenciaDto> Obter(long id)
        {
            try
            {
                var result = await _frequenciaRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var frequencia = result.MapTo<FrequenciaDto>();

                return frequencia;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<FrequenciaDto> Obter(string frequencia)
        {
            try
            {
                var query = _frequenciaRepositorio
                    .GetAll()
                    .WhereIf(!frequencia.IsNullOrWhiteSpace(), m =>
                     m.Codigo.ToUpper().Contains(frequencia.ToUpper()) ||
                     m.Descricao.ToUpper().Contains(frequencia.ToUpper())
                    );
                var result = await query.FirstOrDefaultAsync();
                var _frequencia = result.MapTo<FrequenciaDto>();

                return _frequencia;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<FrequenciaDto>> ListarTodos()
        {
            try
            {
                var query = _frequenciaRepositorio
                    .GetAll();

                var frequencia = await query
                    .AsNoTracking()
                    .ToListAsync();

                var frequenciasDto = frequencia
                    .MapTo<List<FrequenciaDto>>();

                return new ListResultDto<FrequenciaDto>
                {
                    Items = frequenciasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<FrequenciaDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _frequenciaRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var frequencia = await query
                    .AsNoTracking()
                    .ToListAsync();

                var frequenciasDto = frequencia
                    .MapTo<List<FrequenciaDto>>();

                return new ListResultDto<FrequenciaDto>
                {
                    Items = frequenciasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from p in _frequenciaRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m => m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                                          || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                                                                      )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            //return await ListarCodigoDescricaoDropdown(dropdownInput,_frequenciaRepositorio);
        }

        [UnitOfWork(false)]
        public Task<FileDto> ListarParaExcel(ListarInput input)
        {
            throw new NotImplementedException();
        }
    }
}
