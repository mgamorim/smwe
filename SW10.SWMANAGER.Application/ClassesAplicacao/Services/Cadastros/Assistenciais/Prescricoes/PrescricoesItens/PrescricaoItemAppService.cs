﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens
{
    public class PrescricaoItemAppService : SWMANAGERAppServiceBase, IPrescricaoItemAppService
    {
        private readonly IRepository<PrescricaoItem, long> _prescricaoItemRepositorio;
        //private readonly IRepository<FormulaEstoque, long> _formulaEstoqueRepositorio;
        private readonly IRepository<Produto, long> _produtoRepositorio;
        private readonly IRepository<FaturamentoItem, long> _fatItemRepositorio;
        private readonly IFormulaEstoqueAppService _formulaEstoqueAppService;
        private readonly IFormulaFaturamentoAppService _formulaFaturamentoAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<FormulaEstoqueKit, long> _formulaEstoqueKitRepository;

        public PrescricaoItemAppService(
            IRepository<PrescricaoItem, long> prescricaoItemRepositorio,
            //IRepository<FormulaEstoque, long> formulaEstoqueRepositorio,
            IRepository<Produto, long> produtoRepositorio,
            IRepository<FaturamentoItem, long> fatItemRepositorio,
            IFormulaEstoqueAppService formulaEstqoueAppService,
            IFormulaFaturamentoAppService formulaFaturamentoAppService,
        IUnitOfWorkManager unitOfWorkManager,
        IRepository<FormulaEstoqueKit, long> formulaEstoqueKitRepository
            )
        {
            _prescricaoItemRepositorio = prescricaoItemRepositorio;
            //_formulaEstoqueRepositorio = formulaEstoqueRepositorio;
            _produtoRepositorio = produtoRepositorio;
            _formulaEstoqueAppService = formulaEstqoueAppService;
            _formulaFaturamentoAppService = formulaFaturamentoAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _fatItemRepositorio = fatItemRepositorio;
            _formulaEstoqueKitRepository = formulaEstoqueKitRepository;
        }

        [UnitOfWork]
        public async Task<PrescricaoItemDto> CriarOuEditar(PrescricaoItemDto input)
        {
            try
            {
                var prescricaoItem = input.MapTo<PrescricaoItem>();

                var formulasEstoque = new List<FormulaEstoqueDto>();
                if (!input.FormulaEstoqueList.IsNullOrWhiteSpace())
                {
                    formulasEstoque = JsonConvert.DeserializeObject<List<FormulaEstoqueDto>>(input.FormulaEstoqueList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                }
                var formulasFaturamento = new List<FormulaFaturamentoDto>();
                if (!input.FormulaFaturamentoList.IsNullOrWhiteSpace())
                {
                    formulasFaturamento = JsonConvert.DeserializeObject<List<FormulaFaturamentoDto>>(input.FormulaFaturamentoList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                }

                var formulasExameImagem = new List<FormulaFaturamentoDto>();
                if (!input.FormulaExameImagemList.IsNullOrWhiteSpace())
                {
                    formulasExameImagem = JsonConvert.DeserializeObject<List<FormulaFaturamentoDto>>(input.FormulaExameImagemList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                }

                var formulasExameLaboratorial = new List<FormulaFaturamentoDto>();
                if (!input.FormulaExameLaboratorialList.IsNullOrWhiteSpace())
                {
                    formulasExameLaboratorial = JsonConvert.DeserializeObject<List<FormulaFaturamentoDto>>(input.FormulaExameLaboratorialList, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                }

                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _prescricaoItemRepositorio.InsertAndGetIdAsync(prescricaoItem);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _prescricaoItemRepositorio.UpdateAsync(prescricaoItem);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }

                foreach (var formulaEstoque in formulasEstoque)
                {
                    formulaEstoque.PrescricaoItemId = input.Id;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (formulaEstoque.IsDeleted)
                        {
                            await _formulaEstoqueAppService.Excluir(formulaEstoque);
                        }
                        else
                        {
                            await _formulaEstoqueAppService.CriarOuEditar(formulaEstoque);
                        }
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                foreach (var formulaFaturamento in formulasFaturamento)
                {
                    formulaFaturamento.PrescricaoItemId = input.Id;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (formulaFaturamento.IsDeleted)
                        {
                            await _formulaFaturamentoAppService.Excluir(formulaFaturamento);
                        }
                        else
                        {
                            await _formulaFaturamentoAppService.CriarOuEditar(formulaFaturamento);
                        }
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                foreach (var formulaExameImagem in formulasExameImagem)
                {
                    formulaExameImagem.PrescricaoItemId = input.Id;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (formulaExameImagem.IsDeleted)
                        {
                            await _formulaFaturamentoAppService.Excluir(formulaExameImagem);
                        }
                        else
                        {
                            await _formulaFaturamentoAppService.CriarOuEditar(formulaExameImagem);
                        }
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                foreach (var formulaExameLaboratorial in formulasExameLaboratorial)
                {
                    formulaExameLaboratorial.PrescricaoItemId = input.Id;
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        if (formulaExameLaboratorial.IsDeleted)
                        {
                            await _formulaFaturamentoAppService.Excluir(formulaExameLaboratorial);
                        }
                        else
                        {
                            await _formulaFaturamentoAppService.CriarOuEditar(formulaExameLaboratorial);
                        }
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }



                if (!string.IsNullOrEmpty(input.FormulaEstoqueKitJson))
                {
                    var formulaEstoqueKitsInput = JsonConvert.DeserializeObject<List<FormulaEstoqueKitIndex>>(input.FormulaEstoqueKitJson);



                    var formulaEstoqueKits = _formulaEstoqueKitRepository.GetAll()
                                                                         .Where(w => w.PrescricaoItemId == input.Id)
                                                                         .ToList();

                    var kitsExcluidos = formulaEstoqueKits.Where(w => !formulaEstoqueKitsInput.Any(a => a.Id == w.Id));

                    foreach (var item in kitsExcluidos)
                    {
                        _formulaEstoqueKitRepository.Delete(item);
                    }


                    foreach (var item in formulaEstoqueKitsInput.Where(w => w.Id == 0))
                    {
                        var formulaEstoqueKit = new FormulaEstoqueKit();

                        formulaEstoqueKit.EstoqueKitId = item.KitId;
                        formulaEstoqueKit.PrescricaoItemId = input.Id;

                        _formulaEstoqueKitRepository.Insert(formulaEstoqueKit);
                    }
                }

                return input;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task InserirPorProduto(CadastroAgilizadoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    foreach (var produtoId in input.Ids)
                    {
                        var produto = await _produtoRepositorio.GetAsync(produtoId);
                        var prescricaoItem = new PrescricaoItem();
                        prescricaoItem.Codigo = produto.Codigo;
                        prescricaoItem.Descricao = produto.Descricao;
                        prescricaoItem.CreationTime = DateTime.Now;
                        prescricaoItem.CreatorUserId = AbpSession.UserId;
                        prescricaoItem.IsDeleted = false;
                        prescricaoItem.IsSistema = false;
                        prescricaoItem.ProdutoId = produtoId;
                        prescricaoItem.EstoqueId = input.EstoqueId;
                        prescricaoItem.FaturamentoItemId = produto.FaturamentoItemId;
                        prescricaoItem.DivisaoId = input.DivisaoId;
                        await _prescricaoItemRepositorio.InsertAsync(prescricaoItem);
                    }
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                    _unitOfWorkManager.Current.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task InserirPorFatItem(CadastroAgilizadoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    foreach (var id in input.Ids)
                    {
                        var item = await _fatItemRepositorio.GetAsync(id);
                        var prescricaoItem = new PrescricaoItem();
                        prescricaoItem.Codigo = item.Codigo;
                        prescricaoItem.Descricao = item.Descricao;
                        prescricaoItem.CreationTime = DateTime.Now;
                        prescricaoItem.CreatorUserId = AbpSession.UserId;
                        prescricaoItem.IsDeleted = false;
                        prescricaoItem.IsSistema = false;
                        prescricaoItem.FaturamentoItemId = id;
                        prescricaoItem.DivisaoId = input.DivisaoId;
                        await _prescricaoItemRepositorio.InsertAsync(prescricaoItem);
                    }
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                    _unitOfWorkManager.Current.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _prescricaoItemRepositorio.DeleteAsync(id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task ExcluirPorProduto(List<string> ids)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    foreach (var item in ids)
                    {
                        long id;
                        var isId = long.TryParse(item, out id);
                        if (isId)
                        {
                            var prescricao = await _prescricaoItemRepositorio.GetAll()
                                .Where(m => m.ProdutoId == id)
                                .FirstOrDefaultAsync();
                            if (prescricao != null)
                            {
                                await _prescricaoItemRepositorio.DeleteAsync(prescricao.Id);
                            }
                        }
                    }
                    unitOfWork.Complete();
                    unitOfWork.Dispose();

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task ExcluirPorFatItem(List<string> ids)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    foreach (var item in ids)
                    {
                        long id;
                        var isId = long.TryParse(item, out id);
                        if (isId)
                        {
                            var prescricao = await _prescricaoItemRepositorio.GetAll()
                                .Where(m => m.FaturamentoItemId == id)
                                .FirstOrDefaultAsync();
                            if (prescricao != null)
                            {
                                await _prescricaoItemRepositorio.DeleteAsync(prescricao.Id);
                            }
                        }
                    }
                    unitOfWork.Complete();
                    unitOfWork.Dispose();

                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoItemDto>> Listar(ListarInput input)
        {
            var contarPrescricaoItem = 0;
            List<PrescricaoItem> prescricaoItem;
            List<PrescricaoItemDto> PrescricaoItemDtos = new List<PrescricaoItemDto>();
            try
            {
                var query = _prescricaoItemRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarPrescricaoItem = await query
                    .CountAsync();

                prescricaoItem = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                PrescricaoItemDtos = prescricaoItem
                    .MapTo<List<PrescricaoItemDto>>();

                return new PagedResultDto<PrescricaoItemDto>(
                    contarPrescricaoItem,
                    PrescricaoItemDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoItemDto> Obter(long id)
        {
            try
            {
                var result = await _prescricaoItemRepositorio
                    .GetAll()
                    .Include(m => m.TipoPrescricao)
                    .Include(m => m.Divisao)
                    .Include(m => m.FormaAplicacao)
                    .Include(m => m.Frequencia)
                    .Include(m => m.TipoControle)
                    .Include(m => m.Unidade)
                    .Include(m => m.UnidadeRequisicao)
                    .Include(m => m.VelocidadeInfusao)
                    .Include(m => m.Produto)
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Estoque)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var prescricaoItem = PrescricaoItemDto.Mapear(result);

                return prescricaoItem;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoItemDto> ObterPorProduto(long produtoId)
        {
            try
            {
                var result = await _prescricaoItemRepositorio
                    .GetAll()
                    .Include(m => m.TipoPrescricao)
                    .Include(m => m.Divisao)
                    .Include(m => m.FormaAplicacao)
                    .Include(m => m.Frequencia)
                    .Include(m => m.TipoControle)
                    .Include(m => m.Unidade)
                    .Include(m => m.UnidadeRequisicao)
                    .Include(m => m.VelocidadeInfusao)
                    .Include(m => m.Produto)
                    .Include(m => m.FaturamentoItem)
                    .Include(m => m.Estoque)
                    .Where(m => m.ProdutoId == produtoId)
                    .FirstOrDefaultAsync();
                var prescricaoItem = PrescricaoItemDto.Mapear(result);

                return prescricaoItem;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemDto>> ListarTodos()
        {
            try
            {
                var query = _prescricaoItemRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var prescricaoItem = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposControlesDto = PrescricaoItemDto.Mapear(prescricaoItem);

                return new ListResultDto<PrescricaoItemDto>
                {
                    Items = tiposControlesDto.ToList()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _prescricaoItemRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var prescricaoItem = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposControlesDto = PrescricaoItemDto.Mapear(prescricaoItem);

                return new ListResultDto<PrescricaoItemDto>
                {
                    Items = tiposControlesDto.ToList()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            //return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoItemRepositorio);
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            long id = 0;
            var result = long.TryParse(dropdownInput.id, out id);
            try
            {

                var query = from p in _prescricaoItemRepositorio.GetAll()
                            //.Include(m => m.Divisao)
                            //.Include(m => m.Divisao.Subdivisoes)
                            //.Where(m => (m.Divisao.IsDivisaoPrincipal && m.Divisao.Subdivisoes.Count() == 0))
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                            m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                            m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                            )
                            .WhereIf(id > 0, m =>
                               m.DivisaoId == id)
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<PagedResultDto<GenericoIdNome>> ListarProdutosDisponiveis(ListarInput input)
        {
            try
            {
                long grupoId = 0;
                var isGrupo = long.TryParse(input.Id, out grupoId);

                var query = _produtoRepositorio.GetAll()
                    .WhereIf(isGrupo, m => m.GrupoId.Equals(grupoId))
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DescricaoResumida.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Except(_prescricaoItemRepositorio.GetAll()
                    .Include(m => m.Produto)
                    .Where(m => m.ProdutoId.HasValue)
                    .Select(s => s.Produto));

                var result = await query.ToListAsync();

                var produtos = new List<GenericoIdNome>();

                foreach (var item in result)
                {
                    if (item != null)
                    {
                        produtos.Add(new GenericoIdNome
                        {
                            Id = item.Id,
                            Nome = item.Descricao ?? string.Empty
                        });
                    }
                }

                if (input.Sorting.ToUpper().Contains("CREATIONTIME"))
                {
                    input.Sorting = "Nome";
                }

                produtos = produtos
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<GenericoIdNome>()
                {
                    Items = produtos,
                    TotalCount = result.Count()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<GenericoIdNome>> ListarProdutosIncluidos(ListarInput input)
        {
            try
            {
                long grupoId = 0;
                var isGrupo = long.TryParse(input.Id, out grupoId);

                var query = _prescricaoItemRepositorio.GetAll()
                    .Include(m => m.Produto)
                    .Where(m => m.ProdutoId.HasValue)
                    .WhereIf(isGrupo, m => m.Produto.GrupoId.Equals(grupoId))
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                        m.Produto.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Produto.DescricaoResumida.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Produto.Codigo.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Select(s => s.Produto)
                    .Distinct();

                var result = await query.ToListAsync();

                var produtos = new List<GenericoIdNome>();

                foreach (var item in result)
                {
                    if (item != null)
                    {
                        produtos.Add(new GenericoIdNome
                        {
                            Id = item.Id,
                            Nome = item.Descricao ?? string.Empty
                        });
                    }

                }

                if (input.Sorting.ToUpper().Contains("CREATIONTIME"))
                {
                    input.Sorting = "Nome";
                }

                produtos = produtos
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<GenericoIdNome>()
                {
                    Items = produtos,
                    TotalCount = result.Count()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<GenericoIdNome>> ListarExamesLaboratoriaisDisponiveis(ListarInput input)
        {
            try
            {
                long grupoId = 0;
                var isGrupo = long.TryParse(input.Id, out grupoId);

                var query = _fatItemRepositorio.GetAll()
                    .Include(m => m.Grupo)
                    .Include(m => m.SubGrupo)
                    .Where(f => f.Grupo.IsLaboratorio || f.IsLaboratorio || f.SubGrupo.IsLaboratorio)
                    .WhereIf(isGrupo, m => m.GrupoId == grupoId)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Except(_prescricaoItemRepositorio.GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Where(m => m.FaturamentoItemId.HasValue)
                    .Select(s => s.FaturamentoItem));
                //.Distinct();

                var result = await query.ToListAsync();

                var itens = new List<GenericoIdNome>();

                foreach (var item in result)
                {
                    if (item != null)
                    {
                        itens.Add(new GenericoIdNome
                        {
                            Id = item.Id,
                            Nome = item.Descricao ?? string.Empty
                        });
                    }
                }

                if (input.Sorting.ToUpper().Contains("CREATIONTIME"))
                {
                    input.Sorting = "Nome";
                }

                itens = itens
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<GenericoIdNome>()
                {
                    Items = itens,
                    TotalCount = result.Count()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<GenericoIdNome>> ListarExamesLaboratoriaisIncluidos(ListarInput input)
        {
            try
            {
                long grupoId = 0;
                var isGrupo = long.TryParse(input.Id, out grupoId);

                var query = _prescricaoItemRepositorio.GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Where(m => m.FaturamentoItemId.HasValue)
                    .Where(f => f.FaturamentoItem.Grupo.IsLaboratorio || f.FaturamentoItem.IsLaboratorio || f.FaturamentoItem.SubGrupo.IsLaboratorio)
                    .WhereIf(isGrupo, m => m.FaturamentoItem.GrupoId == grupoId)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                        m.FaturamentoItem.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.FaturamentoItem.Codigo.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Select(s => s.FaturamentoItem)
                    .Distinct();

                var result = await query.ToListAsync();

                var itens = new List<GenericoIdNome>();

                foreach (var item in result)
                {
                    if (item != null)
                    {
                        itens.Add(new GenericoIdNome
                        {
                            Id = item.Id,
                            Nome = item.Descricao ?? string.Empty
                        });
                    }

                }

                if (input.Sorting.ToUpper().Contains("CREATIONTIME"))
                {
                    input.Sorting = "Nome";
                }

                itens = itens
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<GenericoIdNome>()
                {
                    Items = itens,
                    TotalCount = result.Count()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<GenericoIdNome>> ListarExamesImagemDisponiveis(ListarInput input)
        {
            try
            {
                long grupoId = 0;
                var isGrupo = long.TryParse(input.Id, out grupoId);

                var query = _fatItemRepositorio.GetAll()
                    .Where(f => (f.Grupo.IsLaudo && !f.Grupo.IsLaboratorio) || (f.IsLaudo && !f.IsLaboratorio) || (f.SubGrupo.IsLaudo && !f.SubGrupo.IsLaboratorio))
                    .WhereIf(isGrupo, m => m.GrupoId == grupoId)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Except(_prescricaoItemRepositorio.GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Where(m => m.FaturamentoItemId.HasValue)
                    .Select(s => s.FaturamentoItem))
                    .Distinct();

                var result = await query.ToListAsync();

                var itens = new List<GenericoIdNome>();

                foreach (var item in result)
                {
                    if (item != null)
                    {
                        itens.Add(new GenericoIdNome
                        {
                            Id = item.Id,
                            Nome = item.Descricao ?? string.Empty
                        });
                    }
                }

                if (input.Sorting.ToUpper().Contains("CREATIONTIME"))
                {
                    input.Sorting = "Nome";
                }

                itens = itens
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<GenericoIdNome>()
                {
                    Items = itens,
                    TotalCount = result.Count()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<GenericoIdNome>> ListarExamesImagemIncluidos(ListarInput input)
        {
            try
            {
                long grupoId = 0;
                var isGrupo = long.TryParse(input.Id, out grupoId);

                var query = _prescricaoItemRepositorio.GetAll()
                    .Include(m => m.FaturamentoItem)
                    .Where(m => m.FaturamentoItemId.HasValue)
                    .Where(f => (f.FaturamentoItem.Grupo.IsLaudo && !f.FaturamentoItem.Grupo.IsLaboratorio) || (f.FaturamentoItem.IsLaudo && !f.FaturamentoItem.IsLaboratorio) || (f.FaturamentoItem.SubGrupo.IsLaudo && !f.FaturamentoItem.SubGrupo.IsLaboratorio))
                    .WhereIf(isGrupo, m => m.FaturamentoItem.GrupoId == grupoId)
                    .WhereIf(!input.Filtro.IsNullOrWhiteSpace(), m =>
                        m.FaturamentoItem.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.FaturamentoItem.Codigo.ToUpper().Contains(input.Filtro.ToUpper())
                        )
                    .Select(s => s.FaturamentoItem)
                    .Distinct();

                var result = await query.ToListAsync();

                var itens = new List<GenericoIdNome>();

                foreach (var item in result)
                {
                    if (item != null)
                    {
                        itens.Add(new GenericoIdNome
                        {
                            Id = item.Id,
                            Nome = item.Descricao ?? string.Empty
                        });
                    }

                }

                if (input.Sorting.ToUpper().Contains("CREATIONTIME"))
                {
                    input.Sorting = "Nome";
                }

                itens = itens
                    .AsQueryable()
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                return new PagedResultDto<GenericoIdNome>()
                {
                    Items = itens,
                    TotalCount = result.Count()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
