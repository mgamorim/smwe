﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens.Dto
{
    public class FormulaEstoqueKitIndex
    {
        public long IdGrid { get; set; }
        public long Id { get; set; }
        public long KitId { get; set; }
        public string KitDescricao { get; set; }
        public long PrescricaoItemId { get; set; }


    }
}
