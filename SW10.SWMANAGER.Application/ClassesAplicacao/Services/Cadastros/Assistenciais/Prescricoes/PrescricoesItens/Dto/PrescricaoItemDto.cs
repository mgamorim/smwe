﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.FormasAplicacao;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Frequencias;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposControles;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao;
using SW10.SWMANAGER.ClassesAplicacao.Faturamentos.Itens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.FormasAplicacao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Frequencias.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposControles.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Faturamentos.Itens.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens.Dto
{
    [AutoMap(typeof(PrescricaoItem))]
    public class PrescricaoItemDto : CamposPadraoCRUDDto, IDescricao
    {
        public long? DivisaoId { get; set; }

        public bool IsAtivo { get; set; }

        public bool IsAlertaDuplicidade { get; set; }

        public bool IsExigeJustificativa { get; set; }

        public string Justificativa { get; set; }

        public long? TipoPrescricaoId { get; set; }

        public long? TipoControleId { get; set; }

        public bool IsAlteraQuantidade { get; set; }

        public long TotalDias { get; set; }

        public decimal? Quantidade { get; set; }

        public long? UnidadeId { get; set; }

        public long? FormaAplicacaoId { get; set; }

        public long? FrequenciaId { get; set; }

        public long? VelocidadeInfusaoId { get; set; }

        public long? UnidadeRequisicaoId { get; set; }

        public long? ProdutoId { get; set; }
        public ProdutoDto Produto { get; set; }

        public long? FaturamentoItemId { get; set; }
        public FaturamentoItemDto FaturamentoItem { get; set; }

        public long? EstoqueId { get; set; }
        public EstoqueDto Estoque { get; set; }

        public DivisaoDto Divisao { get; set; }

        public TipoPrescricaoDto TipoPrescricao { get; set; }

        public TipoControleDto TipoControle { get; set; }

        public UnidadeDto Unidade { get; set; }

        public FormaAplicacaoDto FormaAplicacao { get; set; }

        public FrequenciaDto Frequencia { get; set; }

        public VelocidadeInfusaoDto VelocidadeInfusao { get; set; }

        public UnidadeDto UnidadeRequisicao { get; set; }

        public string FormulaEstoqueList { get; set; }

        public string FormulaFaturamentoList { get; set; }

        public string FormulaExameImagemList { get; set; }

        public string FormulaExameLaboratorialList { get; set; }

        public string FormulaEstoqueKitJson { get; set; }

        public static PrescricaoItemDto Mapear(PrescricaoItem input)
        {
            var prescricaoItem = new PrescricaoItemDto(); //input.MapTo<PrescricaoItemDto>();
            if (input != null)
            {
                prescricaoItem.Codigo = input.Codigo;
                prescricaoItem.CreationTime = input.CreationTime;
                prescricaoItem.CreatorUserId = input.CreatorUserId;
                prescricaoItem.Descricao = input.Descricao;
                prescricaoItem.Id = input.Id;
                prescricaoItem.IsAlertaDuplicidade = input.IsAlertaDuplicidade;
                prescricaoItem.IsAlteraQuantidade = input.IsAlteraQuantidade;
                prescricaoItem.IsAtivo = input.IsAtivo;
                prescricaoItem.IsExigeJustificativa = input.IsExigeJustificativa;
                prescricaoItem.Justificativa = input.Justificativa;
                prescricaoItem.LastModificationTime = input.LastModificationTime;
                prescricaoItem.LastModifierUserId = input.LastModifierUserId;
                prescricaoItem.Quantidade = input.Quantidade;
                prescricaoItem.TotalDias = input.TotalDias;

                prescricaoItem.UnidadeId = input.UnidadeId;
                prescricaoItem.UnidadeRequisicaoId = input.UnidadeRequisicaoId;
                prescricaoItem.VelocidadeInfusaoId = input.VelocidadeInfusaoId;
                prescricaoItem.DivisaoId = input.DivisaoId;
                prescricaoItem.EstoqueId = input.EstoqueId;
                prescricaoItem.FaturamentoItemId = input.FaturamentoItemId;
                prescricaoItem.FormaAplicacaoId = input.FormaAplicacaoId;
                prescricaoItem.FrequenciaId = input.FrequenciaId;
                prescricaoItem.ProdutoId = input.ProdutoId;
                prescricaoItem.TipoControleId = input.TipoControleId;
                prescricaoItem.TipoPrescricaoId = input.TipoPrescricaoId;

                prescricaoItem.Unidade = input.Unidade.MapTo<UnidadeDto>();
                prescricaoItem.UnidadeRequisicao = input.UnidadeRequisicao.MapTo<UnidadeDto>();
                prescricaoItem.VelocidadeInfusao = input.VelocidadeInfusao.MapTo<VelocidadeInfusaoDto>();
                prescricaoItem.Divisao = input.Divisao.MapTo<DivisaoDto>();
                prescricaoItem.Estoque = input.Estoque.MapTo<EstoqueDto>();
                prescricaoItem.FaturamentoItem = input.FaturamentoItem.MapTo<FaturamentoItemDto>();
                prescricaoItem.FormaAplicacao = input.FormaAplicacao.MapTo<FormaAplicacaoDto>();
                prescricaoItem.Frequencia = input.Frequencia.MapTo<FrequenciaDto>();
                prescricaoItem.Produto = input.Produto.MapTo<ProdutoDto>();
                prescricaoItem.TipoControle = input.TipoControle.MapTo<TipoControleDto>();
                prescricaoItem.TipoPrescricao = input.TipoPrescricao.MapTo<TipoPrescricaoDto>();
            }
            return prescricaoItem;

        }

        public static PrescricaoItem Mapear(PrescricaoItemDto input)
        {
            var prescricaoItem = new PrescricaoItem(); //input.MapTo<PrescricaoItemDto>();
            prescricaoItem.Codigo = input.Codigo;
            prescricaoItem.CreationTime = input.CreationTime;
            prescricaoItem.CreatorUserId = input.CreatorUserId;
            prescricaoItem.Descricao = input.Descricao;
            prescricaoItem.DivisaoId = input.DivisaoId;
            prescricaoItem.EstoqueId = input.EstoqueId;
            prescricaoItem.FaturamentoItemId = input.FaturamentoItemId;
            prescricaoItem.FormaAplicacaoId = input.FormaAplicacaoId;
            prescricaoItem.FrequenciaId = input.FrequenciaId;
            prescricaoItem.Id = input.Id;
            prescricaoItem.IsAlertaDuplicidade = input.IsAlertaDuplicidade;
            prescricaoItem.IsAlteraQuantidade = input.IsAlteraQuantidade;
            prescricaoItem.IsAtivo = input.IsAtivo;
            prescricaoItem.IsExigeJustificativa = input.IsExigeJustificativa;
            prescricaoItem.Justificativa = input.Justificativa;
            prescricaoItem.LastModificationTime = input.LastModificationTime;
            prescricaoItem.LastModifierUserId = input.LastModifierUserId;
            prescricaoItem.ProdutoId = input.ProdutoId;
            prescricaoItem.Quantidade = input.Quantidade;
            prescricaoItem.TipoControleId = input.TipoControleId;
            prescricaoItem.TipoPrescricaoId = input.TipoPrescricaoId;
            prescricaoItem.TotalDias = input.TotalDias;
            prescricaoItem.UnidadeId = input.UnidadeId;
            prescricaoItem.UnidadeRequisicaoId = input.UnidadeRequisicaoId;
            prescricaoItem.VelocidadeInfusaoId = input.VelocidadeInfusaoId;

            prescricaoItem.Unidade = input.Unidade.MapTo<Unidade>();
            prescricaoItem.UnidadeRequisicao = input.UnidadeRequisicao.MapTo<Unidade>();
            prescricaoItem.VelocidadeInfusao = input.VelocidadeInfusao.MapTo<VelocidadeInfusao>();
            prescricaoItem.Divisao = input.Divisao.MapTo<Divisao>();
            prescricaoItem.Estoque = input.Estoque.MapTo<Estoque>();
            prescricaoItem.FaturamentoItem = input.FaturamentoItem.MapTo<FaturamentoItem>();
            prescricaoItem.FormaAplicacao = input.FormaAplicacao.MapTo<FormaAplicacao>();
            prescricaoItem.Frequencia = input.Frequencia.MapTo<Frequencia>();
            prescricaoItem.Produto = input.Produto.MapTo<Produto>();
            prescricaoItem.TipoControle = input.TipoControle.MapTo<TipoControle>();
            prescricaoItem.TipoPrescricao = input.TipoPrescricao.MapTo<TipoPrescricao>();

            return prescricaoItem;

        }

        public static IEnumerable<PrescricaoItemDto> Mapear(List<PrescricaoItem> input)
        {
            foreach (var item in input)
            {
                var prescricaoItem = new PrescricaoItemDto(); //input.MapTo<PrescricaoItemDto>();
                prescricaoItem.Codigo = item.Codigo;
                prescricaoItem.CreationTime = item.CreationTime;
                prescricaoItem.CreatorUserId = item.CreatorUserId;
                prescricaoItem.Descricao = item.Descricao;
                prescricaoItem.Id = item.Id;
                prescricaoItem.IsAlertaDuplicidade = item.IsAlertaDuplicidade;
                prescricaoItem.IsAlteraQuantidade = item.IsAlteraQuantidade;
                prescricaoItem.IsAtivo = item.IsAtivo;
                prescricaoItem.IsExigeJustificativa = item.IsExigeJustificativa;
                prescricaoItem.Justificativa = item.Justificativa;
                prescricaoItem.LastModificationTime = item.LastModificationTime;
                prescricaoItem.LastModifierUserId = item.LastModifierUserId;
                prescricaoItem.Quantidade = item.Quantidade;
                prescricaoItem.TotalDias = item.TotalDias;

                prescricaoItem.UnidadeId = item.UnidadeId;
                prescricaoItem.UnidadeRequisicaoId = item.UnidadeRequisicaoId;
                prescricaoItem.VelocidadeInfusaoId = item.VelocidadeInfusaoId;
                prescricaoItem.DivisaoId = item.DivisaoId;
                prescricaoItem.EstoqueId = item.EstoqueId;
                prescricaoItem.FaturamentoItemId = item.FaturamentoItemId;
                prescricaoItem.FormaAplicacaoId = item.FormaAplicacaoId;
                prescricaoItem.FrequenciaId = item.FrequenciaId;
                prescricaoItem.ProdutoId = item.ProdutoId;
                prescricaoItem.TipoControleId = item.TipoControleId;
                prescricaoItem.TipoPrescricaoId = item.TipoPrescricaoId;

                prescricaoItem.Unidade = item.Unidade.MapTo<UnidadeDto>();
                prescricaoItem.UnidadeRequisicao = item.UnidadeRequisicao.MapTo<UnidadeDto>();
                prescricaoItem.VelocidadeInfusao = item.VelocidadeInfusao.MapTo<VelocidadeInfusaoDto>();
                prescricaoItem.Divisao = item.Divisao.MapTo<DivisaoDto>();
                prescricaoItem.Estoque = item.Estoque.MapTo<EstoqueDto>();
                prescricaoItem.FaturamentoItem = item.FaturamentoItem.MapTo<FaturamentoItemDto>();
                prescricaoItem.FormaAplicacao = item.FormaAplicacao.MapTo<FormaAplicacaoDto>();
                prescricaoItem.Frequencia = item.Frequencia.MapTo<FrequenciaDto>();
                prescricaoItem.Produto = item.Produto.MapTo<ProdutoDto>();
                prescricaoItem.TipoControle = item.TipoControle.MapTo<TipoControleDto>();
                prescricaoItem.TipoPrescricao = item.TipoPrescricao.MapTo<TipoPrescricaoDto>();

                yield return prescricaoItem;
            }

        }

        public static IEnumerable<PrescricaoItem> Mapear(List<PrescricaoItemDto> input)
        {
            foreach (var item in input)
            {
                var prescricaoItem = new PrescricaoItem(); //input.MapTo<PrescricaoItemDto>();
                prescricaoItem.Codigo = item.Codigo;
                prescricaoItem.CreationTime = item.CreationTime;
                prescricaoItem.CreatorUserId = item.CreatorUserId;
                prescricaoItem.Descricao = item.Descricao;
                prescricaoItem.DivisaoId = item.DivisaoId;
                prescricaoItem.EstoqueId = item.EstoqueId;
                prescricaoItem.FaturamentoItemId = item.FaturamentoItemId;
                prescricaoItem.FormaAplicacaoId = item.FormaAplicacaoId;
                prescricaoItem.FrequenciaId = item.FrequenciaId;
                prescricaoItem.Id = item.Id;
                prescricaoItem.IsAlertaDuplicidade = item.IsAlertaDuplicidade;
                prescricaoItem.IsAlteraQuantidade = item.IsAlteraQuantidade;
                prescricaoItem.IsAtivo = item.IsAtivo;
                prescricaoItem.IsExigeJustificativa = item.IsExigeJustificativa;
                prescricaoItem.Justificativa = item.Justificativa;
                prescricaoItem.LastModificationTime = item.LastModificationTime;
                prescricaoItem.LastModifierUserId = item.LastModifierUserId;
                prescricaoItem.ProdutoId = item.ProdutoId;
                prescricaoItem.Quantidade = item.Quantidade;
                prescricaoItem.TipoControleId = item.TipoControleId;
                prescricaoItem.TipoPrescricaoId = item.TipoPrescricaoId;
                prescricaoItem.TotalDias = item.TotalDias;
                prescricaoItem.UnidadeId = item.UnidadeId;
                prescricaoItem.UnidadeRequisicaoId = item.UnidadeRequisicaoId;
                prescricaoItem.VelocidadeInfusaoId = item.VelocidadeInfusaoId;

                prescricaoItem.Unidade = item.Unidade.MapTo<Unidade>();
                prescricaoItem.UnidadeRequisicao = item.UnidadeRequisicao.MapTo<Unidade>();
                prescricaoItem.VelocidadeInfusao = item.VelocidadeInfusao.MapTo<VelocidadeInfusao>();
                prescricaoItem.Divisao = item.Divisao.MapTo<Divisao>();
                prescricaoItem.Estoque = item.Estoque.MapTo<Estoque>();
                prescricaoItem.FaturamentoItem = item.FaturamentoItem.MapTo<FaturamentoItem>();
                prescricaoItem.FormaAplicacao = item.FormaAplicacao.MapTo<FormaAplicacao>();
                prescricaoItem.Frequencia = item.Frequencia.MapTo<Frequencia>();
                prescricaoItem.Produto = item.Produto.MapTo<Produto>();
                prescricaoItem.TipoControle = item.TipoControle.MapTo<TipoControle>();
                prescricaoItem.TipoPrescricao = item.TipoPrescricao.MapTo<TipoPrescricao>();

                yield return prescricaoItem;
            }
        }
    }
}
