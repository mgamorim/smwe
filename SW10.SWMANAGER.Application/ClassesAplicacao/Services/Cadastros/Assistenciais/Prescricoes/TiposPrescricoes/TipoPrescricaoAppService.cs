﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes
{
    public class TipoPrescricaoAppService : SWMANAGERAppServiceBase, ITipoPrescricaoAppService
    {
        private readonly IRepository<TipoPrescricao, long> _tipoPrescricaoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TipoPrescricaoAppService(
            IRepository<TipoPrescricao, long> tipoPrescricaoRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _tipoPrescricaoRepositorio = tipoPrescricaoRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<TipoPrescricaoDto> CriarOuEditar(TipoPrescricaoDto input)
        {
            try
            {
                var tipoPrescricao = input.MapTo<TipoPrescricao>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _tipoPrescricaoRepositorio.InsertAndGetIdAsync(tipoPrescricao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        tipoPrescricao = await _tipoPrescricaoRepositorio.UpdateAsync(tipoPrescricao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(TipoPrescricaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tipoPrescricaoRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<TipoPrescricaoDto>> Listar(ListarInput input)
        {
            var contarTipoPrescricao = 0;
            List<TipoPrescricao> tipoPrescricao;
            List<TipoPrescricaoDto> TipoPrescricaoDtos = new List<TipoPrescricaoDto>();
            try
            {
                var query = _tipoPrescricaoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoPrescricao = await query
                    .CountAsync();

                tipoPrescricao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                TipoPrescricaoDtos = tipoPrescricao
                    .MapTo<List<TipoPrescricaoDto>>();

                return new PagedResultDto<TipoPrescricaoDto>(
                    contarTipoPrescricao,
                    TipoPrescricaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TipoPrescricaoDto> Obter(long id)
        {
            try
            {
                var result = await _tipoPrescricaoRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var tipoPrescricao = result.MapTo<TipoPrescricaoDto>();

                return tipoPrescricao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoPrescricaoDto>> ListarTodos()
        {
            try
            {
                var query = _tipoPrescricaoRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var tipoPrescricao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposPrescricoesDto = tipoPrescricao
                    .MapTo<List<TipoPrescricaoDto>>();

                return new ListResultDto<TipoPrescricaoDto>
                {
                    Items = tiposPrescricoesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoPrescricaoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _tipoPrescricaoRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var tipoPrescricao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposPrescricoesDto = tipoPrescricao
                    .MapTo<List<TipoPrescricaoDto>>();

                return new ListResultDto<TipoPrescricaoDto>
                {
                    Items = tiposPrescricoesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tipoPrescricaoRepositorio);
        }

    }
}
