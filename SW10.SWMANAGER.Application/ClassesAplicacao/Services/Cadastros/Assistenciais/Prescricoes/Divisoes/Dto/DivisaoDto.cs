﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes.Dto;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes.Dto
{
    [AutoMap(typeof(Divisao))]
    public class DivisaoDto : CamposPadraoCRUDDto
    {
        public long? TipoPrescricaoId { get; set; }

        public int Ordem { get; set; }

        public bool IsDivisaoPrincipal { get; set; }

        public long? DivisaoPrincipalId { get; set; }

        public DivisaoDto DivisaoPrincipal { get; set; }

        public bool IsResposta { get; set; }

        public string TiposRespostasSelecionadas { get; set; }

        public TipoPrescricaoDto TipoPrescricao { get; set; }

        public bool IsQuantidade { get; set; }
        public bool IsUnidadeMedida { get; set; }
        public bool IsVelocidadeInfusao { get; set; }
        public bool IsDuracao { get; set; }
        public bool IsFormaAplicacao { get; set; }
        public bool IsFrequencia { get; set; }
        public bool IsUniddeOrganizacional { get; set; }
        public bool IsMedico { get; set; }
        public bool IsDataInicio { get; set; }
        public bool IsDiasAplicacao { get; set; }
        public bool IsObservacao { get; set; }
        public bool IsCopiarPrescricao { get; set; }
        public bool IsTipoMedicacao { get; set; }
        public bool IsExameImagem { get; set; }
        public bool IsExameLaboratorial { get; set; }
        public bool IsSetorExame { get; set; }
        public bool IsProdutoEstoque { get; set; }
        public bool IsControlaVolume { get; set; }
        public bool IsSangueDerivado { get; set; }
        public bool IsSeNecessario { get; set; }
        public bool IsUrgente { get; set; }
        public bool IsAgora { get; set; }
        public bool IsAcm { get; set; }
        public bool IsMedicamento { get; set; }

        public static DivisaoDto Mapear(Divisao input)
        {
            var result = new DivisaoDto();
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.Descricao = input.Descricao;
            result.DivisaoPrincipalId = input.DivisaoPrincipalId;
            result.Id = input.Id;
            result.IsAcm = input.IsAcm;
            result.IsAgora = input.IsAgora;
            result.IsControlaVolume = input.IsControlaVolume;
            result.IsCopiarPrescricao = input.IsCopiarPrescricao;
            result.IsDataInicio = input.IsDataInicio;
            result.IsDiasAplicacao = input.IsDiasAplicacao;
            result.IsDivisaoPrincipal = input.IsDivisaoPrincipal;
            result.IsDuracao = input.IsDuracao;
            result.IsExameImagem = input.IsExameImagem;
            result.IsExameLaboratorial = input.IsExameLaboratorial;
            result.IsFormaAplicacao = input.IsFormaAplicacao;
            result.IsFrequencia = input.IsFrequencia;
            result.IsMedicamento = input.IsMedicamento;
            result.IsMedico = input.IsMedico;
            result.IsObservacao = input.IsObservacao;
            result.IsProdutoEstoque = input.IsProdutoEstoque;
            result.IsQuantidade = input.IsQuantidade;
            result.IsSangueDerivado = input.IsSangueDerivado;
            result.IsSeNecessario = input.IsSeNecessario;
            result.IsSetorExame = input.IsSetorExame;
            result.IsSistema = input.IsSistema;
            result.IsTipoMedicacao = input.IsTipoMedicacao;
            result.IsUnidadeMedida = input.IsUnidadeMedida;
            result.IsUniddeOrganizacional = input.IsUniddeOrganizacional;
            result.IsUrgente = input.IsUrgente;
            result.IsVelocidadeInfusao = input.IsVelocidadeInfusao;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;
            result.Ordem = input.Ordem;
            result.TipoPrescricaoId = input.TipoPrescricaoId;

            if (input.DivisaoPrincipal != null)
            {
                result.DivisaoPrincipal = Mapear(input.DivisaoPrincipal);
            }

            if (input.TipoPrescricao != null)
            {
                result.TipoPrescricao = TipoPrescricaoDto.Mapear(input.TipoPrescricao);
            }

            return result;
        }

        public static Divisao Mapear(DivisaoDto input)
        {
            var result = new Divisao();
            result.Codigo = input.Codigo;
            result.CreationTime = input.CreationTime;
            result.CreatorUserId = input.CreatorUserId;
            result.Descricao = input.Descricao;
            result.DivisaoPrincipalId = input.DivisaoPrincipalId;
            result.Id = input.Id;
            result.IsAcm = input.IsAcm;
            result.IsAgora = input.IsAgora;
            result.IsControlaVolume = input.IsControlaVolume;
            result.IsCopiarPrescricao = input.IsCopiarPrescricao;
            result.IsDataInicio = input.IsDataInicio;
            result.IsDiasAplicacao = input.IsDiasAplicacao;
            result.IsDivisaoPrincipal = input.IsDivisaoPrincipal;
            result.IsDuracao = input.IsDuracao;
            result.IsExameImagem = input.IsExameImagem;
            result.IsExameLaboratorial = input.IsExameLaboratorial;
            result.IsFormaAplicacao = input.IsFormaAplicacao;
            result.IsFrequencia = input.IsFrequencia;
            result.IsMedicamento = input.IsMedicamento;
            result.IsMedico = input.IsMedico;
            result.IsObservacao = input.IsObservacao;
            result.IsProdutoEstoque = input.IsProdutoEstoque;
            result.IsQuantidade = input.IsQuantidade;
            result.IsSangueDerivado = input.IsSangueDerivado;
            result.IsSeNecessario = input.IsSeNecessario;
            result.IsSetorExame = input.IsSetorExame;
            result.IsSistema = input.IsSistema;
            result.IsTipoMedicacao = input.IsTipoMedicacao;
            result.IsUnidadeMedida = input.IsUnidadeMedida;
            result.IsUniddeOrganizacional = input.IsUniddeOrganizacional;
            result.IsUrgente = input.IsUrgente;
            result.IsVelocidadeInfusao = input.IsVelocidadeInfusao;
            result.LastModificationTime = input.LastModificationTime;
            result.LastModifierUserId = input.LastModifierUserId;
            result.Ordem = input.Ordem;
            result.TipoPrescricaoId = input.TipoPrescricaoId;

            if (input.DivisaoPrincipal != null)
            {
                result.DivisaoPrincipal = Mapear(input.DivisaoPrincipal);
            }

            if (input.TipoPrescricao != null)
            {
                result.TipoPrescricao = TipoPrescricaoDto.Mapear(input.TipoPrescricao);
            }

            return result;
        }

        public static IEnumerable<DivisaoDto> Mapear(List<Divisao> list)
        {
            foreach (var input in list)
            {
                var result = new DivisaoDto();
                result.Codigo = input.Codigo;
                result.CreationTime = input.CreationTime;
                result.CreatorUserId = input.CreatorUserId;
                result.Descricao = input.Descricao;
                result.DivisaoPrincipalId = input.DivisaoPrincipalId;
                result.Id = input.Id;
                result.IsAcm = input.IsAcm;
                result.IsAgora = input.IsAgora;
                result.IsControlaVolume = input.IsControlaVolume;
                result.IsCopiarPrescricao = input.IsCopiarPrescricao;
                result.IsDataInicio = input.IsDataInicio;
                result.IsDiasAplicacao = input.IsDiasAplicacao;
                result.IsDivisaoPrincipal = input.IsDivisaoPrincipal;
                result.IsDuracao = input.IsDuracao;
                result.IsExameImagem = input.IsExameImagem;
                result.IsExameLaboratorial = input.IsExameLaboratorial;
                result.IsFormaAplicacao = input.IsFormaAplicacao;
                result.IsFrequencia = input.IsFrequencia;
                result.IsMedicamento = input.IsMedicamento;
                result.IsMedico = input.IsMedico;
                result.IsObservacao = input.IsObservacao;
                result.IsProdutoEstoque = input.IsProdutoEstoque;
                result.IsQuantidade = input.IsQuantidade;
                result.IsSangueDerivado = input.IsSangueDerivado;
                result.IsSeNecessario = input.IsSeNecessario;
                result.IsSetorExame = input.IsSetorExame;
                result.IsSistema = input.IsSistema;
                result.IsTipoMedicacao = input.IsTipoMedicacao;
                result.IsUnidadeMedida = input.IsUnidadeMedida;
                result.IsUniddeOrganizacional = input.IsUniddeOrganizacional;
                result.IsUrgente = input.IsUrgente;
                result.IsVelocidadeInfusao = input.IsVelocidadeInfusao;
                result.LastModificationTime = input.LastModificationTime;
                result.LastModifierUserId = input.LastModifierUserId;
                result.Ordem = input.Ordem;
                result.TipoPrescricaoId = input.TipoPrescricaoId;

                if (input.DivisaoPrincipal != null)
                {
                    result.DivisaoPrincipal = Mapear(input.DivisaoPrincipal);
                }

                if (input.TipoPrescricao != null)
                {
                    result.TipoPrescricao = TipoPrescricaoDto.Mapear(input.TipoPrescricao);
                }

                yield return result;
            }
        }

        public static IEnumerable<Divisao> Mapear(List<DivisaoDto> list)
        {
            foreach (var input in list)
            {
                var result = new Divisao();
                result.Codigo = input.Codigo;
                result.CreationTime = input.CreationTime;
                result.CreatorUserId = input.CreatorUserId;
                result.Descricao = input.Descricao;
                result.DivisaoPrincipalId = input.DivisaoPrincipalId;
                result.Id = input.Id;
                result.IsAcm = input.IsAcm;
                result.IsAgora = input.IsAgora;
                result.IsControlaVolume = input.IsControlaVolume;
                result.IsCopiarPrescricao = input.IsCopiarPrescricao;
                result.IsDataInicio = input.IsDataInicio;
                result.IsDiasAplicacao = input.IsDiasAplicacao;
                result.IsDivisaoPrincipal = input.IsDivisaoPrincipal;
                result.IsDuracao = input.IsDuracao;
                result.IsExameImagem = input.IsExameImagem;
                result.IsExameLaboratorial = input.IsExameLaboratorial;
                result.IsFormaAplicacao = input.IsFormaAplicacao;
                result.IsFrequencia = input.IsFrequencia;
                result.IsMedicamento = input.IsMedicamento;
                result.IsMedico = input.IsMedico;
                result.IsObservacao = input.IsObservacao;
                result.IsProdutoEstoque = input.IsProdutoEstoque;
                result.IsQuantidade = input.IsQuantidade;
                result.IsSangueDerivado = input.IsSangueDerivado;
                result.IsSeNecessario = input.IsSeNecessario;
                result.IsSetorExame = input.IsSetorExame;
                result.IsSistema = input.IsSistema;
                result.IsTipoMedicacao = input.IsTipoMedicacao;
                result.IsUnidadeMedida = input.IsUnidadeMedida;
                result.IsUniddeOrganizacional = input.IsUniddeOrganizacional;
                result.IsUrgente = input.IsUrgente;
                result.IsVelocidadeInfusao = input.IsVelocidadeInfusao;
                result.LastModificationTime = input.LastModificationTime;
                result.LastModifierUserId = input.LastModifierUserId;
                result.Ordem = input.Ordem;
                result.TipoPrescricaoId = input.TipoPrescricaoId;

                if (input.DivisaoPrincipal != null)
                {
                    result.DivisaoPrincipal = Mapear(input.DivisaoPrincipal);
                }

                if (input.TipoPrescricao != null)
                {
                    result.TipoPrescricao = TipoPrescricaoDto.Mapear(input.TipoPrescricao);
                }

                yield return result;
            }
        }

    }
}