﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposPrescricoes;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes
{
    public class DivisaoAppService : SWMANAGERAppServiceBase, IDivisaoAppService
    {
        private readonly IRepository<Divisao, long> _divisaoRepositorio;
        private readonly IRepository<DivisaoTipoResposta, long> _divisaoTipoRespostaRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITipoPrescricaoAppService _tipoPrescricaoService;

        public DivisaoAppService(
            IRepository<Divisao, long> divisaoRepositorio,
            IRepository<DivisaoTipoResposta, long> divisaoTipoRespostaRepositorio,
            IUnitOfWorkManager unitOfWorkManager,
            ITipoPrescricaoAppService tipoPrescricaoService
            )
        {
            _divisaoRepositorio = divisaoRepositorio;
            _divisaoTipoRespostaRepositorio = divisaoTipoRespostaRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
            _tipoPrescricaoService = tipoPrescricaoService;
        }

        [UnitOfWork]
        public async Task<DivisaoDto> CriarOuEditar(DivisaoDto input)
        {
            try
            {
                var divisao = DivisaoDto.Mapear(input);
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _divisaoRepositorio.InsertAndGetIdAsync(divisao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        _unitOfWorkManager.Current.SaveChanges();
                    }
                    return input;
                }
                else
                {
                    //Eliminando filhos caso o registro tenha sido rebaixado de divisão principal para comum/subdivisao
                    if (!input.IsDivisaoPrincipal)
                    {
                        var subs = await _divisaoRepositorio
                            .GetAll()
                            .Where(m => m.DivisaoPrincipalId == input.Id)
                            .ToListAsync();

                        using (var unitOfWork = _unitOfWorkManager.Begin())
                        {
                            foreach (var sub in subs)
                            {
                                sub.DivisaoPrincipalId = null;
                                await _divisaoRepositorio.UpdateAsync(sub);
                            }
                            unitOfWork.Complete();
                            _unitOfWorkManager.Current.SaveChanges();
                            unitOfWork.Dispose();
                        }
                    }
                    else
                    {
                        if (input.DivisaoPrincipalId.HasValue)
                        {
                            divisao.DivisaoPrincipalId = null;
                        }
                    }

                    //salvando registro editado
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _divisaoRepositorio.UpdateAsync(divisao);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }

                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task<DivisaoDto> SalvarSubDivisao(DivisaoDto input)
        {
            try
            {
                var divisao = DivisaoDto.Mapear(input); //.MapTo<Divisao>();

                //    //registro original (sem os filhos)
                var divisaoSalvar = await _divisaoRepositorio
                    .GetAll()
                    //.Include(m => m.TiposRespostas)
                    .Where(m => m.Id == input.Id)
                    .FirstOrDefaultAsync();

                //alterando o registro original com os dados enviados pelo usuário
                divisaoSalvar.Codigo = input.Codigo;
                divisaoSalvar.CreationTime = divisao.CreationTime;
                divisaoSalvar.CreatorUserId = divisao.CreatorUserId;
                divisaoSalvar.DeleterUserId = divisao.DeleterUserId;
                divisaoSalvar.DeletionTime = divisao.DeletionTime;
                divisaoSalvar.Descricao = input.Descricao;
                divisaoSalvar.DivisaoPrincipalId = input.IsDivisaoPrincipal ? null : input.DivisaoPrincipalId;
                divisaoSalvar.IsDeleted = divisao.IsDeleted;
                divisaoSalvar.IsDivisaoPrincipal = input.IsDivisaoPrincipal;
                divisaoSalvar.TipoPrescricaoId = input.TipoPrescricaoId;
                divisaoSalvar.IsSistema = divisao.IsSistema;
                divisaoSalvar.LastModificationTime = divisao.LastModificationTime;
                divisaoSalvar.LastModifierUserId = divisao.LastModifierUserId;
                divisaoSalvar.Ordem = input.Ordem;

                //salvando registro editado
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    divisao = await _divisaoRepositorio.UpdateAsync(divisaoSalvar);
                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(DivisaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _divisaoRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<DivisaoDto>> Listar(ListarDivisoesInput input)
        {
            var contarDivisao = 0;
            List<Divisao> divisao;
            List<DivisaoDto> DivisaoDtos = new List<DivisaoDto>();
            try
            {
                var query = _divisaoRepositorio
                    .GetAll()
                    .Where(m => m.IsDivisaoPrincipal)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DivisaoPrincipal.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DivisaoPrincipal.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarDivisao = await query
                    .CountAsync();

                divisao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                DivisaoDtos = DivisaoDto.Mapear(divisao).ToList();

                return new PagedResultDto<DivisaoDto>(
                    contarDivisao,
                    DivisaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<DivisaoDto>> ListarSubDivisoes(ListarDivisoesInput input)
        {
            var contarDivisao = 0;
            List<Divisao> divisao;
            List<DivisaoDto> DivisaoDtos = new List<DivisaoDto>();
            try
            {
                var query = _divisaoRepositorio
                    .GetAll()
                    .Where(m => m.DivisaoPrincipalId == input.DivisaoPrincipalId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarDivisao = await query
                    .CountAsync();

                divisao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                DivisaoDtos = DivisaoDto.Mapear(divisao).ToList();

                return new PagedResultDto<DivisaoDto>(
                    contarDivisao,
                    DivisaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<DivisaoDto>> ListarDivisoesSemRelacao(ListarDivisoesInput input)
        {
            var contarDivisao = 0;
            List<Divisao> divisao;
            List<DivisaoDto> DivisaoDtos = new List<DivisaoDto>();
            try
            {
                var query = _divisaoRepositorio
                    .GetAll()
                    .Where(m => m.DivisaoPrincipalId == null && !m.IsDivisaoPrincipal)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarDivisao = await query
                    .CountAsync();

                divisao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                DivisaoDtos = DivisaoDto.Mapear(divisao).ToList();
                    

                return new PagedResultDto<DivisaoDto>(
                    contarDivisao,
                    DivisaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<DivisaoDto> Obter(long id)
        {
            try
            {
                var divisaoPrincipalDto = new DivisaoDto();
                var m = await _divisaoRepositorio
                    .GetAll()
                    .Include(d => d.DivisaoPrincipal)
                    .Include(d => d.TipoPrescricao)
                    .Where(d => d.Id == id)
                    .FirstOrDefaultAsync();

                var dto = DivisaoDto.Mapear(m);

                if (dto.TipoPrescricaoId.HasValue)
                {
                    var tipoPrescricao = await _tipoPrescricaoService.Obter(dto.TipoPrescricaoId.Value);
                    dto.TipoPrescricao = tipoPrescricao;
                }


                return dto;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<DivisaoDto> Obter(string divisao)
        {
            try
            {
                var query = _divisaoRepositorio
                    .GetAll()
                    .WhereIf(!divisao.IsNullOrWhiteSpace(), d =>
                     d.Codigo.ToUpper().Contains(divisao.ToUpper()) ||
                     d.Descricao.ToUpper().Contains(divisao.ToUpper())
                    );
                var m = await query.FirstOrDefaultAsync();
                var _divisao = DivisaoDto.Mapear(m);

                return _divisao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<DivisaoDto>> ListarTodos()
        {
            try
            {
                var query = _divisaoRepositorio
                    .GetAll()
                    .Include(m => m.DivisaoPrincipal);

                var divisoes = await query
                   // .AsNoTracking()
                    .ToListAsync();

                var divisoesDto = DivisaoDto.Mapear(divisoes).ToList();

                return new ListResultDto<DivisaoDto>
                {
                    Items = divisoesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<DivisaoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _divisaoRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var divisao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var divisoesDto = DivisaoDto.Mapear(divisao).ToList();

                return new ListResultDto<DivisaoDto>
                {
                    Items = divisoesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = (from p in _divisaoRepositorio.GetAll()
                             .Where(m => m.IsDivisaoPrincipal && m.Subdivisoes.Count() == 0)
                             .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                             orderby p.Ordem ascending //, p.Codigo ascending
                             select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) })
                             .Union(from p in _divisaoRepositorio.GetAll()
                                    .Where(m => !m.IsDivisaoPrincipal && m.DivisaoPrincipalId.HasValue)
                                    .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                    m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                    orderby p.Ordem ascending //, p.Codigo ascending
                                    select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                    );

                var queryResultPage = query
                    .OrderBy(m => m.text)
                    .Skip(numberOfObjectsPerPage * pageInt)
                    .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        [UnitOfWork(false)]
        public Task<FileDto> ListarParaExcel(ListarDivisoesInput input)
        {
            throw new NotImplementedException();
        }
    }
}
