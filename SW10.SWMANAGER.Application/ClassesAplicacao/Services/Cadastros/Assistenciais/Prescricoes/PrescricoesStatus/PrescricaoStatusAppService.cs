﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus
{
    public class PrescricaoStatusAppService : SWMANAGERAppServiceBase, IPrescricaoStatusAppService
    {
        private readonly IRepository<PrescricaoStatus, long> _prescricaoStatusRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PrescricaoStatusAppService(
            IRepository<PrescricaoStatus, long> prescricaoStatusRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _prescricaoStatusRepositorio = prescricaoStatusRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<PrescricaoStatusDto> CriarOuEditar(PrescricaoStatusDto input)
        {
            try
            {
                var prescricaoStatus = input.MapTo<PrescricaoStatus>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _prescricaoStatusRepositorio.InsertAndGetIdAsync(prescricaoStatus);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        prescricaoStatus = await _prescricaoStatusRepositorio.UpdateAsync(prescricaoStatus);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(PrescricaoStatusDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _prescricaoStatusRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoStatusDto>> Listar(ListarInput input)
        {
            var contarPrescricaoStatus = 0;
            List<PrescricaoStatus> prescricaoStatus;
            List<PrescricaoStatusDto> PrescricaoStatusDtos = new List<PrescricaoStatusDto>();
            try
            {
                var query = _prescricaoStatusRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarPrescricaoStatus = await query
                    .CountAsync();

                prescricaoStatus = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                PrescricaoStatusDtos = prescricaoStatus
                    .MapTo<List<PrescricaoStatusDto>>();

                return new PagedResultDto<PrescricaoStatusDto>(
                    contarPrescricaoStatus,
                    PrescricaoStatusDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoStatusDto> Obter(long id)
        {
            try
            {
                var result = await _prescricaoStatusRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var prescricaoStatus = result.MapTo<PrescricaoStatusDto>();

                return prescricaoStatus;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoStatusDto>> ListarTodos()
        {
            try
            {
                var query = _prescricaoStatusRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var prescricaoStatus = await query
                    .AsNoTracking()
                    .ToListAsync();

                var prescricoesStatusDto = prescricaoStatus
                    .MapTo<List<PrescricaoStatusDto>>();

                return new ListResultDto<PrescricaoStatusDto>
                {
                    Items = prescricoesStatusDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoStatusDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _prescricaoStatusRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var prescricaoStatus = await query
                    .AsNoTracking()
                    .ToListAsync();

                var prescricoesStatusDto = prescricaoStatus
                    .MapTo<List<PrescricaoStatusDto>>();

                return new ListResultDto<PrescricaoStatusDto>
                {
                    Items = prescricoesStatusDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoStatusRepositorio);
        }

    }
}
