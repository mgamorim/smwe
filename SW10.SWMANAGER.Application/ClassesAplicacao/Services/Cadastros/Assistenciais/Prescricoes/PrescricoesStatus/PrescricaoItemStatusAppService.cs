﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesStatus
{
    public class PrescricaoItemStatusAppService : SWMANAGERAppServiceBase, IPrescricaoItemStatusAppService
    {
        private readonly IRepository<PrescricaoItemStatus, long> _prescricaoItemStatusRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PrescricaoItemStatusAppService(
            IRepository<PrescricaoItemStatus, long> prescricaoItemStatusRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _prescricaoItemStatusRepositorio = prescricaoItemStatusRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<PrescricaoItemStatusDto> CriarOuEditar(PrescricaoItemStatusDto input)
        {
            try
            {
                var prescricaoItemStatus = input.MapTo<PrescricaoItemStatus>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _prescricaoItemStatusRepositorio.InsertAndGetIdAsync(prescricaoItemStatus);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        prescricaoItemStatus = await _prescricaoItemStatusRepositorio.UpdateAsync(prescricaoItemStatus);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(PrescricaoItemStatusDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _prescricaoItemStatusRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<PrescricaoItemStatusDto>> Listar(ListarInput input)
        {
            var contarPrescricaoItemStatus = 0;
            List<PrescricaoItemStatus> prescricaoItemStatus;
            List<PrescricaoItemStatusDto> PrescricaoItemStatusDtos = new List<PrescricaoItemStatusDto>();
            try
            {
                var query = _prescricaoItemStatusRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarPrescricaoItemStatus = await query
                    .CountAsync();

                prescricaoItemStatus = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                PrescricaoItemStatusDtos = prescricaoItemStatus
                    .MapTo<List<PrescricaoItemStatusDto>>();

                return new PagedResultDto<PrescricaoItemStatusDto>(
                    contarPrescricaoItemStatus,
                    PrescricaoItemStatusDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PrescricaoItemStatusDto> Obter(long id)
        {
            try
            {
                var result = await _prescricaoItemStatusRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var prescricaoItemStatus = result.MapTo<PrescricaoItemStatusDto>();

                return prescricaoItemStatus;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemStatusDto>> ListarTodos()
        {
            try
            {
                var query = _prescricaoItemStatusRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var prescricaoItemStatus = await query
                    .AsNoTracking()
                    .ToListAsync();

                var prescricoesItensStatusDto = prescricaoItemStatus
                    .MapTo<List<PrescricaoItemStatusDto>>();

                return new ListResultDto<PrescricaoItemStatusDto>
                {
                    Items = prescricoesItensStatusDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<PrescricaoItemStatusDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _prescricaoItemStatusRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var prescricaoItemStatus = await query
                    .AsNoTracking()
                    .ToListAsync();

                var prescricoesItensStatusDto = prescricaoItemStatus
                    .MapTo<List<PrescricaoItemStatusDto>>();

                return new ListResultDto<PrescricaoItemStatusDto>
                {
                    Items = prescricoesItensStatusDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _prescricaoItemStatusRepositorio);
        }

    }
}
