﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.VelocidadesInfusao
{
    public class VelocidadeInfusaoAppService : SWMANAGERAppServiceBase, IVelocidadeInfusaoAppService
    {
        private readonly IRepository<VelocidadeInfusao, long> _velocidadeInfusaoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public VelocidadeInfusaoAppService(
            IRepository<VelocidadeInfusao, long> velocidadeInfusaoRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _velocidadeInfusaoRepositorio = velocidadeInfusaoRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<VelocidadeInfusaoDto> CriarOuEditar(VelocidadeInfusaoDto input)
        {
            try
            {
                var velocidadeInfusao = input.MapTo<VelocidadeInfusao>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _velocidadeInfusaoRepositorio.InsertAndGetIdAsync(velocidadeInfusao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        velocidadeInfusao = await _velocidadeInfusaoRepositorio.UpdateAsync(velocidadeInfusao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(VelocidadeInfusaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _velocidadeInfusaoRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<VelocidadeInfusaoDto>> Listar(ListarInput input)
        {
            var contarVelocidadeInfusao = 0;
            List<VelocidadeInfusao> velocidadeInfusao;
            List<VelocidadeInfusaoDto> VelocidadeInfusaoDtos = new List<VelocidadeInfusaoDto>();
            try
            {
                var query = _velocidadeInfusaoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarVelocidadeInfusao = await query
                    .CountAsync();

                velocidadeInfusao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                VelocidadeInfusaoDtos = velocidadeInfusao
                    .MapTo<List<VelocidadeInfusaoDto>>();

                return new PagedResultDto<VelocidadeInfusaoDto>(
                    contarVelocidadeInfusao,
                    VelocidadeInfusaoDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<VelocidadeInfusaoDto> Obter(long id)
        {
            try
            {
                var result = await _velocidadeInfusaoRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var velocidadeInfusao = result.MapTo<VelocidadeInfusaoDto>();

                return velocidadeInfusao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<VelocidadeInfusaoDto>> ListarTodos()
        {
            try
            {
                var query = _velocidadeInfusaoRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var velocidadeInfusao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var velocidadesInfusaoDto = velocidadeInfusao
                    .MapTo<List<VelocidadeInfusaoDto>>();

                return new ListResultDto<VelocidadeInfusaoDto>
                {
                    Items = velocidadesInfusaoDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<VelocidadeInfusaoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _velocidadeInfusaoRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var velocidadeInfusao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var velocidadesInfusaoDto = velocidadeInfusao
                    .MapTo<List<VelocidadeInfusaoDto>>();

                return new ListResultDto<VelocidadeInfusaoDto>
                {
                    Items = velocidadesInfusaoDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _velocidadeInfusaoRepositorio);
        }

    }
}
