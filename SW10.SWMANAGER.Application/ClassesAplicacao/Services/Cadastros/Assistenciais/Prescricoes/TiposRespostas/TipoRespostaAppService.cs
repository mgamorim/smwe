﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Assistenciais.Medicos;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Assistenciais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.Divisoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.PrescricoesItens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas
{
    public class TipoRespostaAppService : SWMANAGERAppServiceBase, ITipoRespostaAppService
    {
        private readonly IRepository<TipoResposta, long> _tipoRespostaRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDivisaoAppService _divisaoAppService;
        private readonly IPrescricaoItemAppService _prescricaoItemAppService;

        public TipoRespostaAppService(
            IRepository<TipoResposta, long> tipoRespostaRepositorio,
            IUnitOfWorkManager unitOfWorkManager,
            IDivisaoAppService divisaoAppService,
            IPrescricaoItemAppService prescricaoItemAppService
            )
        {
            _tipoRespostaRepositorio = tipoRespostaRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
            _divisaoAppService = divisaoAppService;
            _prescricaoItemAppService = prescricaoItemAppService;
        }

        [UnitOfWork]
        public async Task<TipoRespostaDto> CriarOuEditar(TipoRespostaDto input)
        {
            try
            {
                var tipoResposta = input.MapTo<TipoResposta>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _tipoRespostaRepositorio.InsertAndGetIdAsync(tipoResposta);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        tipoResposta = await _tipoRespostaRepositorio.UpdateAsync(tipoResposta);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(TipoRespostaDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tipoRespostaRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<TipoRespostaDto>> Listar(ListarTipoRespostaInput input)
        {
            var contarTipoResposta = 0;
            List<TipoResposta> tipoResposta;
            List<TipoRespostaDto> TipoRespostaDtos = new List<TipoRespostaDto>();
            try
            {
                var query = _tipoRespostaRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoResposta = await query
                    .CountAsync();

                tipoResposta = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                TipoRespostaDtos = tipoResposta
                    .MapTo<List<TipoRespostaDto>>();

                return new PagedResultDto<TipoRespostaDto>(
                    contarTipoResposta,
                    TipoRespostaDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TipoRespostaDto> Obter(long id)
        {
            try
            {
                var result = await _tipoRespostaRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var tipoResposta = result.MapTo<TipoRespostaDto>();

                return tipoResposta;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaDto>> ListarTodos()
        {
            try
            {
                var query = _tipoRespostaRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var tipoResposta = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = tipoResposta
                    .MapTo<List<TipoRespostaDto>>();

                return new ListResultDto<TipoRespostaDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _tipoRespostaRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var tipoResposta = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = tipoResposta
                    .MapTo<List<TipoRespostaDto>>();

                return new ListResultDto<TipoRespostaDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tipoRespostaRepositorio);
        }

        [UnitOfWork(false)]
        public Task<FileDto> ListarParaExcel(ListarTipoRespostaInput input)
        {
            throw new NotImplementedException();
        }

    }
}
