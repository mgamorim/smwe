﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas
{
    public class TipoRespostaTipoRespostaConfiguracaoAppService : SWMANAGERAppServiceBase, ITipoRespostaTipoRespostaConfiguracaoAppService
    {
        private readonly IRepository<TipoRespostaTipoRespostaConfiguracao, long> _tipoRespostaTipoRespostaConfiguracaoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TipoRespostaTipoRespostaConfiguracaoAppService(
            IRepository<TipoRespostaTipoRespostaConfiguracao, long> tipoRespostaTipoRespostaConfiguracaoRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _tipoRespostaTipoRespostaConfiguracaoRepositorio = tipoRespostaTipoRespostaConfiguracaoRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<TipoRespostaTipoRespostaConfiguracaoDto> CriarOuEditar(TipoRespostaTipoRespostaConfiguracaoDto input)
        {
            try
            {
                var tipoRespostaTipoRespostaConfiguracao = input.MapTo<TipoRespostaTipoRespostaConfiguracao>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _tipoRespostaTipoRespostaConfiguracaoRepositorio.InsertAndGetIdAsync(tipoRespostaTipoRespostaConfiguracao);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        tipoRespostaTipoRespostaConfiguracao = await _tipoRespostaTipoRespostaConfiguracaoRepositorio.UpdateAsync(tipoRespostaTipoRespostaConfiguracao);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(TipoRespostaTipoRespostaConfiguracaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tipoRespostaTipoRespostaConfiguracaoRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<TipoRespostaTipoRespostaConfiguracaoDto>> Listar(ListarInput input)
        {
            var contarTipoRespostaTipoRespostaConfiguracao = 0;
            List<TipoRespostaTipoRespostaConfiguracao> tipoRespostaTipoRespostaConfiguracao;
            List<TipoRespostaTipoRespostaConfiguracaoDto> TipoRespostaTipoRespostaConfiguracaoDtos = new List<TipoRespostaTipoRespostaConfiguracaoDto>();
            try
            {
                var query = _tipoRespostaTipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    .Include(m => m.TipoResposta)
                    .Include(m => m.TipoRespostaConfiguracao)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.TipoResposta.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.TipoResposta.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoRespostaTipoRespostaConfiguracao = await query
                    .CountAsync();

                tipoRespostaTipoRespostaConfiguracao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                TipoRespostaTipoRespostaConfiguracaoDtos = tipoRespostaTipoRespostaConfiguracao
                    .MapTo<List<TipoRespostaTipoRespostaConfiguracaoDto>>();

                return new PagedResultDto<TipoRespostaTipoRespostaConfiguracaoDto>(
                    contarTipoRespostaTipoRespostaConfiguracao,
                    TipoRespostaTipoRespostaConfiguracaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaTipoRespostaConfiguracaoDto>> ListarPorTipoResposta(ListarTipoRespostaTipoRespostaConfiguracaoInput input)
        {
            var contarTipoRespostaConfiguracaoElementoHtml = 0;
            List<TipoRespostaTipoRespostaConfiguracao> tipoRespostaConfiguracaoElementoHtml;
            List<TipoRespostaTipoRespostaConfiguracaoDto> TipoRespostaConfiguracaoElementoHtmlDtos = new List<TipoRespostaTipoRespostaConfiguracaoDto>();
            try
            {
                var query = _tipoRespostaTipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    //.Include(m => m.TipoRespostaConfiguracao)
                    .Include(m => m.TipoRespostaConfiguracao)
                    .Where(m => m.TipoRespostaId == input.TipoRespostaId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.TipoRespostaConfiguracao.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.TipoRespostaConfiguracao.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoRespostaConfiguracaoElementoHtml = await query
                    .CountAsync();

                tipoRespostaConfiguracaoElementoHtml = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                TipoRespostaConfiguracaoElementoHtmlDtos = tipoRespostaConfiguracaoElementoHtml
                    .MapTo<List<TipoRespostaTipoRespostaConfiguracaoDto>>();

                return new PagedResultDto<TipoRespostaTipoRespostaConfiguracaoDto>(
                    contarTipoRespostaConfiguracaoElementoHtml,
                    TipoRespostaConfiguracaoElementoHtmlDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TipoRespostaTipoRespostaConfiguracaoDto> Obter(long id)
        {
            try
            {
                var result = await _tipoRespostaTipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Include(m => m.TipoRespostaConfiguracao)
                    .FirstOrDefaultAsync();
                var tipoRespostaTipoRespostaConfiguracao = result.MapTo<TipoRespostaTipoRespostaConfiguracaoDto>();

                return tipoRespostaTipoRespostaConfiguracao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaTipoRespostaConfiguracaoDto>> ListarTodos()
        {
            try
            {
                var query = _tipoRespostaTipoRespostaConfiguracaoRepositorio
                    .GetAll();

                var tipoRespostaTipoRespostaConfiguracao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = tipoRespostaTipoRespostaConfiguracao
                    .MapTo<List<TipoRespostaTipoRespostaConfiguracaoDto>>();

                return new ListResultDto<TipoRespostaTipoRespostaConfiguracaoDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaTipoRespostaConfiguracaoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _tipoRespostaTipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    .Include(m => m.TipoResposta)
                    .Include(m => m.TipoRespostaConfiguracao)
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.TipoResposta.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.TipoResposta.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var tipoRespostaTipoRespostaConfiguracao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = tipoRespostaTipoRespostaConfiguracao
                    .MapTo<List<TipoRespostaTipoRespostaConfiguracaoDto>>();

                return new ListResultDto<TipoRespostaTipoRespostaConfiguracaoDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public Task<FileDto> ListarParaExcel(ListarInput input)
        {
            throw new NotImplementedException();
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                var query = from p in _tipoRespostaTipoRespostaConfiguracaoRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m => m.TipoRespostaConfiguracao.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                                          || m.TipoRespostaConfiguracao.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                                                                      )
                            orderby p.TipoRespostaConfiguracao.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.TipoRespostaConfiguracao.Codigo.ToString(), " - ", p.TipoRespostaConfiguracao.Descricao) };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
