﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto
{
    [AutoMap(typeof(TipoRespostaConfiguracao))]
    public class TipoRespostaConfiguracaoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public ICollection<TipoRespostaConfiguracaoElementoHtmlDto> ElementosHtml { get; set; }
    }
}
