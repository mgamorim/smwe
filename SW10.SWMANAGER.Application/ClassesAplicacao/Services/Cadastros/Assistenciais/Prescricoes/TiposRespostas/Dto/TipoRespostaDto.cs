﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto
{
    [AutoMap(typeof(TipoResposta))]
    public class TipoRespostaDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }

        public virtual ICollection<TipoRespostaTipoRespostaConfiguracaoDto> TipoRespostaConfiguracoes { get; set; }
    }
}
