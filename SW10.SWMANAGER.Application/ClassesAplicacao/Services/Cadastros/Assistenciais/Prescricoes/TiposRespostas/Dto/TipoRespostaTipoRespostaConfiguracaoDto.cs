﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto
{
    [AutoMap(typeof(TipoRespostaTipoRespostaConfiguracao))]
    public class TipoRespostaTipoRespostaConfiguracaoDto : CamposPadraoCRUDDto
    {
        public long TipoRespostaId { get; set; }

        public virtual TipoRespostaDto TipoResposta { get; set; }


        public long TipoRespostaConfiguracaoId { get; set; }

        public virtual TipoRespostaConfiguracaoDto TipoRespostaConfiguracao { get; set; }
    }
}
