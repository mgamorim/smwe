﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ElementosHtml.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto
{
    [AutoMap(typeof(TipoRespostaConfiguracaoElementoHtml))]
    public class TipoRespostaConfiguracaoElementoHtmlDto : CamposPadraoCRUDDto
    {
        public long ElementoHtmlId { get; set; }

        public ElementoHtmlDto ElementoHtml { get; set; }

        public string Rotulo { get; set; }

        public string RotuloPosElemento { get; set; }

        public long TipoRespostaConfiguracaoId { get; set; }

        public TipoRespostaConfiguracaoDto TipoRespostaConfiguracao { get; set; }
    }
}
