﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposRespostas;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposRespostas
{
    public class TipoRespostaConfiguracaoAppService : SWMANAGERAppServiceBase, ITipoRespostaConfiguracaoAppService
    {
        private readonly IRepository<TipoRespostaConfiguracao, long> _tipoRespostaConfiguracaoRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TipoRespostaConfiguracaoAppService(
            IRepository<TipoRespostaConfiguracao, long> tipoRespostaConfiguracaoRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _tipoRespostaConfiguracaoRepositorio = tipoRespostaConfiguracaoRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<TipoRespostaConfiguracaoDto> CriarOuEditar(TipoRespostaConfiguracaoDto input)
        {
            try
            {
                var tipoRespostaConfiguracao = input.MapTo<TipoRespostaConfiguracao>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _tipoRespostaConfiguracaoRepositorio.InsertAndGetIdAsync(tipoRespostaConfiguracao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        tipoRespostaConfiguracao = await _tipoRespostaConfiguracaoRepositorio.UpdateAsync(tipoRespostaConfiguracao);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(TipoRespostaConfiguracaoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tipoRespostaConfiguracaoRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<TipoRespostaConfiguracaoDto>> Listar(ListarInput input)
        {
            var contarTipoRespostaConfiguracao = 0;
            List<TipoRespostaConfiguracao> tipoRespostaConfiguracao;
            List<TipoRespostaConfiguracaoDto> TipoRespostaConfiguracaoDtos = new List<TipoRespostaConfiguracaoDto>();
            try
            {
                var query = _tipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoRespostaConfiguracao = await query
                    .CountAsync();

                tipoRespostaConfiguracao = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                TipoRespostaConfiguracaoDtos = tipoRespostaConfiguracao
                    .MapTo<List<TipoRespostaConfiguracaoDto>>();

                return new PagedResultDto<TipoRespostaConfiguracaoDto>(
                    contarTipoRespostaConfiguracao,
                    TipoRespostaConfiguracaoDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TipoRespostaConfiguracaoDto> Obter(long id)
        {
            try
            {
                var result = await _tipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var tipoRespostaConfiguracao = result.MapTo<TipoRespostaConfiguracaoDto>();

                return tipoRespostaConfiguracao;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaConfiguracaoDto>> ListarTodos()
        {
            try
            {
                var query = _tipoRespostaConfiguracaoRepositorio
                    .GetAll();

                var tipoRespostaConfiguracao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = tipoRespostaConfiguracao
                    .MapTo<List<TipoRespostaConfiguracaoDto>>();

                return new ListResultDto<TipoRespostaConfiguracaoDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoRespostaConfiguracaoDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _tipoRespostaConfiguracaoRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var tipoRespostaConfiguracao = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposRespostasDto = tipoRespostaConfiguracao
                    .MapTo<List<TipoRespostaConfiguracaoDto>>();

                return new ListResultDto<TipoRespostaConfiguracaoDto>
                {
                    Items = tiposRespostasDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tipoRespostaConfiguracaoRepositorio);
        }

        [UnitOfWork(false)]
        public Task<FileDto> ListarParaExcel(ListarInput input)
        {
            throw new NotImplementedException();
        }
    }
}
