﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposControles;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposControles.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposControles
{
    public class TipoControleAppService : SWMANAGERAppServiceBase, ITipoControleAppService
    {
        private readonly IRepository<TipoControle, long> _tipoControleRepositorio;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TipoControleAppService(
            IRepository<TipoControle, long> tipoControleRepositorio,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _tipoControleRepositorio = tipoControleRepositorio;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task<TipoControleDto> CriarOuEditar(TipoControleDto input)
        {
            try
            {
                var tipoControle = input.MapTo<TipoControle>();
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _tipoControleRepositorio.InsertAndGetIdAsync(tipoControle);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        tipoControle = await _tipoControleRepositorio.UpdateAsync(tipoControle);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return input;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(TipoControleDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _tipoControleRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<TipoControleDto>> Listar(ListarInput input)
        {
            var contarTipoControle = 0;
            List<TipoControle> tipoControle;
            List<TipoControleDto> TipoControleDtos = new List<TipoControleDto>();
            try
            {
                var query = _tipoControleRepositorio
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                        );

                contarTipoControle = await query
                    .CountAsync();

                tipoControle = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                TipoControleDtos = tipoControle
                    .MapTo<List<TipoControleDto>>();

                return new PagedResultDto<TipoControleDto>(
                    contarTipoControle,
                    TipoControleDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<TipoControleDto> Obter(long id)
        {
            try
            {
                var result = await _tipoControleRepositorio
                    .GetAll()
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var tipoControle = result.MapTo<TipoControleDto>();

                return tipoControle;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoControleDto>> ListarTodos()
        {
            try
            {
                var query = _tipoControleRepositorio
                    .GetAll()
                    .OrderBy(m => m.Codigo);

                var tipoControle = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposControlesDto = tipoControle
                    .MapTo<List<TipoControleDto>>();

                return new ListResultDto<TipoControleDto>
                {
                    Items = tiposControlesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<TipoControleDto>> ListarFiltro(string filtro)
        {
            try
            {
                var query = _tipoControleRepositorio
                    .GetAll()
                    .WhereIf(!filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(filtro.ToUpper())
                        );

                var tipoControle = await query
                    .AsNoTracking()
                    .ToListAsync();

                var tiposControlesDto = tipoControle
                    .MapTo<List<TipoControleDto>>();

                return new ListResultDto<TipoControleDto>
                {
                    Items = tiposControlesDto
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarCodigoDescricaoDropdown(dropdownInput, _tipoControleRepositorio);
        }

    }
}
