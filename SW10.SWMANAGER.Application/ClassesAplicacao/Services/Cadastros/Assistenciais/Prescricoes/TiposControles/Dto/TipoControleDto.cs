﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Prescricoes.TiposControles;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Prescricoes.TiposControles.Dto
{
    [AutoMap(typeof(TipoControle))]
    public class TipoControleDto : CamposPadraoCRUDDto, IDescricao
    {
        public string Descricao { get; set; }
    }
}
