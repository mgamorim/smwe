﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Atestados;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;


namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados
{
    public class ModeloAtestadoAppService : SWMANAGERAppServiceBase, IModeloAtestadoAppService
    {
        private readonly IRepository<ModeloAtestado, long> _modeloAtestadoRepository;
        private readonly IListarModelosAtestadosExcelExporter _listarModelosAtestadosExcelExporter;

        public ModeloAtestadoAppService(IRepository<ModeloAtestado, long> modeloAtestadoRepository, IListarModelosAtestadosExcelExporter listarModelosAtestadosExcelExporter)
        {
            _modeloAtestadoRepository = modeloAtestadoRepository;
            _listarModelosAtestadosExcelExporter = listarModelosAtestadosExcelExporter;
        }

        public async Task CriarOuEditar(ModeloAtestadoDto input)
        {
            try
            {
                var modeloAtestado = input.MapTo<ModeloAtestado>();
                if (input.Id.Equals(0))
                {
                    await _modeloAtestadoRepository.InsertAsync(modeloAtestado);
                }
                else
                {
                    await _modeloAtestadoRepository.UpdateAsync(modeloAtestado);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(ModeloAtestadoDto input)
        {
            try
            {
                await _modeloAtestadoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<ModeloAtestadoDto>> Listar(ListarInput input)
        {
            try
            {
                var contarAtestados = 0;
                List<ModeloAtestado> modelosAtestados = new List<ModeloAtestado>();
                List<ModeloAtestadoDto> modelosatestadosDto = new List<ModeloAtestadoDto>();

                var query = _modeloAtestadoRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Conteudo.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarAtestados = await query
                    .CountAsync();

                modelosAtestados = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                modelosatestadosDto = modelosAtestados.MapTo<List<ModeloAtestadoDto>>();

                return new PagedResultDto<ModeloAtestadoDto>(
                    contarAtestados,
                    modelosatestadosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _modeloAtestadoRepository
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                       m.Conteudo.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Conteudo.Substring(0, 40) + "..." })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarInput input)
        {
            try
            {
                var result = await Listar(input);
                var modelosAtestados = result.Items;
                return _listarModelosAtestadosExcelExporter.ExportToFile(modelosAtestados.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<ModeloAtestadoDto> Obter(long id)
        {
            var query = await _modeloAtestadoRepository
                .GetAsync(id);

            var modeloAtestado = query.MapTo<ModeloAtestadoDto>();

            return modeloAtestado;
        }

        public async Task<ListResultDto<ModeloAtestadoDto>> ListarTodos()
        {
            try
            {
                var query = await _modeloAtestadoRepository
                    .GetAllListAsync();

                var modeloAtestadosDto = query.MapTo<List<ModeloAtestadoDto>>();

                return new ListResultDto<ModeloAtestadoDto> { Items = modeloAtestadosDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
