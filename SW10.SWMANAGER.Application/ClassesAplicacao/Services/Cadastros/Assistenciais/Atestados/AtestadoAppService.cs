﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Atestados;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;


namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados
{
    public class AtestadoAppService : SWMANAGERAppServiceBase, IAtestadoAppService
    {
        private readonly IRepository<Atestado, long> _atestadoRepository;
        private readonly IListarAtestadosExcelExporter _listarAtestadosExcelExporter;

        public AtestadoAppService(IRepository<Atestado, long> atestadoRepository, IListarAtestadosExcelExporter listarAtestadosExcelExporter)
        {
            _atestadoRepository = atestadoRepository;
            _listarAtestadosExcelExporter = listarAtestadosExcelExporter;
        }

        public async Task CriarOuEditar(AtestadoDto input)
        {
            try
            {
                var estado = input.MapTo<Atestado>();
                if (input.Id.Equals(0))
                {
                    await _atestadoRepository.InsertAsync(estado);
                }
                else
                {
                    await _atestadoRepository.UpdateAsync(estado);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(AtestadoDto input)
        {
            try
            {
                await _atestadoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<AtestadoDto>> Listar(ListarAtestadosInput input)
        {
            try
            {
                var contarAtestados = 0;
                List<Atestado> atestados = new List<Atestado>();
                List<AtestadoDto> atestadosDto = new List<AtestadoDto>();

                var query = _atestadoRepository
                    .GetAll()
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.ModeloAtestado)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.TipoAtestado)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Conteudo.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarAtestados = await query
                    .CountAsync();

                atestados = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                atestadosDto = atestados.MapTo<List<AtestadoDto>>();

                return new PagedResultDto<AtestadoDto>(
                    contarAtestados,
                    atestadosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _atestadoRepository
                    .GetAll()
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.ModeloAtestado)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.TipoAtestado)
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                       m.Conteudo.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Conteudo.Substring(0, 40) + "..." })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarAtestadosInput input)
        {
            try
            {
                var result = await Listar(input);
                var atestados = result.Items;
                return _listarAtestadosExcelExporter.ExportToFile(atestados.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<AtestadoDto> Obter(long id)
        {
            var query = await _atestadoRepository
                .GetAll()
                    .Include(m => m.Medico)
                    .Include(m => m.Medico.SisPessoa)
                    .Include(m => m.ModeloAtestado)
                    .Include(m => m.Paciente)
                    .Include(m => m.Paciente.SisPessoa)
                    .Include(m => m.TipoAtestado)
                .Where(m => m.Id == id)
                .FirstOrDefaultAsync();

            var estado = query.MapTo<AtestadoDto>();

            return estado;
        }

    }
}
