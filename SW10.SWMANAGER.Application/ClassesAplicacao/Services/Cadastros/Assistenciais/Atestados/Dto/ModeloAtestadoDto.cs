﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Atestados;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Dto
{
    [AutoMap(typeof(ModeloAtestado))]
    public class ModeloAtestadoDto: CamposPadraoCRUDDto
    {
        public string Titulo { get; set; }
        public string Conteudo { get; set; }
    }
}
