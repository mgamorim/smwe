﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Atestados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Medicos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Dto
{
    [AutoMap(typeof(Atestado))]
    public class AtestadoDto : CamposPadraoCRUDDto
    {
        public DateTime DataAtendimento { get; set; }
        public string Conteudo { get; set; }
        public long? MedicoId { get; set; }
        public long? PacienteId { get; set; }
        public long? TipoAtestadoId { get; set; }
        public long? ModeloAtestadoId { get; set; }

        public virtual MedicoDto Medico { get; set; }

        public virtual PacienteDto Paciente { get; set; }

        public virtual TipoAtestadoDto TipoAtestado { get; set; }

        public virtual TipoAtestadoDto ModeloAtestado { get; set; }

    }
}
