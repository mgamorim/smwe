﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Atestados;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Dto
{
    [AutoMap(typeof(TipoAtestado))]
    public class TipoAtestadoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
