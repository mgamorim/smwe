﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais.Atestados;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;


namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.Atestados
{
    public class TipoAtestadoAppService : SWMANAGERAppServiceBase, ITipoAtestadoAppService
    {
        private readonly IRepository<TipoAtestado, long> _tipoAtestadoRepository;
        private readonly IListarTiposAtestadosExcelExporter _listarAtestadosExcelExporter;

        public TipoAtestadoAppService(IRepository<TipoAtestado, long> tipoAtestadoRepository, IListarTiposAtestadosExcelExporter listarAtestadosExcelExporter)
        {
            _tipoAtestadoRepository = tipoAtestadoRepository;
            _listarAtestadosExcelExporter = listarAtestadosExcelExporter;
        }

        public async Task CriarOuEditar(TipoAtestadoDto input)
        {
            try
            {
                var tipoAtestado = input.MapTo<TipoAtestado>();
                if (input.Id.Equals(0))
                {
                    await _tipoAtestadoRepository.InsertAsync(tipoAtestado);
                }
                else
                {
                    await _tipoAtestadoRepository.UpdateAsync(tipoAtestado);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(TipoAtestadoDto input)
        {
            try
            {
                await _tipoAtestadoRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public async Task<PagedResultDto<TipoAtestadoDto>> Listar(ListarInput input)
        {
            try
            {
                var contarAtestados = 0;
                List<TipoAtestado> tipoAtestados = new List<TipoAtestado>();
                List<TipoAtestadoDto> tipoAtestadosDto = new List<TipoAtestadoDto>();

                var query = _tipoAtestadoRepository
                    .GetAll()
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarAtestados = await query
                    .CountAsync();

                tipoAtestados = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                tipoAtestadosDto = tipoAtestados.MapTo<List<TipoAtestadoDto>>();

                return new PagedResultDto<TipoAtestadoDto>(
                    contarAtestados,
                    tipoAtestadosDto
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _tipoAtestadoRepository
                    .GetAll()
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                       m.Descricao.ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarInput input)
        {
            try
            {
                var result = await Listar(input);
                var tiposAtestados = result.Items;
                return _listarAtestadosExcelExporter.ExportToFile(tiposAtestados.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }

        }

        public async Task<ListResultDto<TipoAtestadoDto>> ListarTodos()
        {
            try
            {
                var query = await _tipoAtestadoRepository
                    .GetAllListAsync();

                var tipoAtestadosDto = query.MapTo<List<TipoAtestadoDto>>();

                return new ListResultDto<TipoAtestadoDto> { Items = tipoAtestadosDto };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<TipoAtestadoDto> Obter(long id)
        {
            var query = await _tipoAtestadoRepository
                .GetAsync(id);

            var tipoAtestado = query.MapTo<TipoAtestadoDto>();

            return tipoAtestado;
        }

    }
}
