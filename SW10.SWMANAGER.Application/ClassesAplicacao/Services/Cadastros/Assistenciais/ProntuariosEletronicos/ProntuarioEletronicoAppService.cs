﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using System.Data.Entity.SqlServer;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos
{
    public class ProntuarioEletronicoAppService : SWMANAGERAppServiceBase, IProntuarioEletronicoAppService
    {
        private readonly IRepository<Prontuario, long> _prontuarioRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IFormRespostaAppService _formRespostaAppService;
        private readonly IOperacaoAppService _operacaoAppService;
        private readonly IUnidadeOrganizacionalAppService _unidadeOrganizacionalAppService;
        //private readonly IListarProntuariosExcelExporter _listarProntuariosExcelExporter;

        public ProntuarioEletronicoAppService(
            IRepository<Prontuario, long> prontuarioRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IUltimoIdAppService ultimoIdAppService,
            IFormRespostaAppService formRespostaAppService,
            IOperacaoAppService operacaoAppService,
            IUnidadeOrganizacionalAppService unidadeOrganizacionalAppService
            )
        {
            _prontuarioRepository = prontuarioRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoIdAppService;
            _formRespostaAppService = formRespostaAppService;
            _operacaoAppService = operacaoAppService;
            _unidadeOrganizacionalAppService = unidadeOrganizacionalAppService;
        }

        [UnitOfWork]
        public async Task<ProntuarioEletronicoDto> CriarOuEditar(ProntuarioEletronicoDto input)
        {
            try
            {
                var prontuario = new Prontuario
                {
                    AtendimentoId = input.AtendimentoId,
                    Codigo = input.Codigo,
                    CreationTime = input.CreationTime,
                    CreatorUserId = input.CreatorUserId,
                    DataAdmissao = input.DataAdmissao,
                    DeleterUserId = input.DeleterUserId,
                    DeletionTime = input.DeletionTime,
                    Descricao = input.Descricao,
                    FormRespostaId = input.FormRespostaId,
                    Id = input.Id,
                    IsDeleted = input.IsDeleted,
                    IsSistema = input.IsSistema,
                    LastModificationTime = input.LastModificationTime,
                    LastModifierUserId = input.LastModifierUserId,
                    Observacao = input.Observacao,
                    OperacaoId = input.OperacaoId,
                    ProfissionalSaudeId = input.ProfissionalSaudeId,
                    ProntuarioPrincipalId = input.ProntuarioPrincipalId,
                    UnidadeOrganizacionalId = input.UnidadeOrganizacionalId
                }; // input.MapTo<Prontuario>();

                if (input.Id.Equals(0))
                {
                    var ultimosId = await _ultimoIdAppService.ListarTodos();
                    var ultimoId = ultimosId.Items.Where(m => m.NomeTabela == "AdmissaoMedica").FirstOrDefault();
                    prontuario.Codigo = ultimoId.Codigo;
                    input.Codigo = prontuario.Codigo;
                    var codigo = Convert.ToInt64(ultimoId.Codigo);
                    codigo++;
                    ultimoId.Codigo = codigo.ToString();
                    await _ultimoIdAppService.CriarOuEditar(ultimoId);
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _prontuarioRepository.InsertAndGetIdAsync(prontuario);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _prontuarioRepository.UpdateAsync(prontuario);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        [UnitOfWork]
        public async Task AtualizarFormId(long id, long respostaId)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var prontuario = await _prontuarioRepository.FirstOrDefaultAsync(id);
                    prontuario.FormRespostaId = respostaId;
                    await _prontuarioRepository.UpdateAsync(prontuario);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                    _unitOfWorkManager.Current.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroSalvar", ex);
            }
        }

        public async Task Excluir(ProntuarioEletronicoDto input)
        {
            try
            {
                await _prontuarioRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        [UnitOfWork(false)]
        public async Task<PagedResultDto<ProntuarioEletronicoIndexDto>> Listar(ListarInput input)
        {
            var contarProntuarios = 0;
            //List<Prontuario> prontuarios;
            List<ProntuarioEletronicoIndexDto> prontuariosDtos = new List<ProntuarioEletronicoIndexDto>();
            long atendimentoId = 0;
            long.TryParse(input.PrincipalId, out atendimentoId);
            try
            {
                var query = _prontuarioRepository
                    .GetAll()
                    //.Include(m => m.UnidadeOrganizacional)
                    //.Include(m => m.Operacao)
                    .Where(m => m.OperacaoId == input.OperacaoId)
                    .WhereIf(atendimentoId > 0, m => m.AtendimentoId == atendimentoId)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Codigo.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarProntuarios = await query
                    .CountAsync();

                var prontuarios = query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input);
                //.ToListAsync();

                prontuariosDtos = await prontuarios
                    .Select(s => new ProntuarioEletronicoIndexDto
                    {
                        CodigoAtendimento = s.Atendimento.Codigo,
                        DataAdmissao = s.DataAdmissao,
                        Empresa = s.Atendimento.Empresa.NomeFantasia,
                        Formulario = s.FormResposta.FormConfig.Nome,
                        FormRespostaId = s.FormRespostaId,
                        Id = s.Id,
                        Medico = s.Atendimento.Medico.NomeCompleto,
                        Paciente = s.Atendimento.Paciente.NomeCompleto,
                        UnidadeOrganizacional = s.UnidadeOrganizacional.Descricao
                    }).ToListAsync();

                //.MapTo<List<ProntuarioEletronicoDto>>();

                return new PagedResultDto<ProntuarioEletronicoIndexDto>(
                        contarProntuarios,
                        prontuariosDtos
                        );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ListResultDto<ProntuarioEletronicoDto>> ListarTodos()
        {
            try
            {
                var query = await _prontuarioRepository
                    .GetAll()
                    .Select(m => new ProntuarioEletronicoDto
                    {
                        AtendimentoId = m.AtendimentoId,
                        Codigo = m.Codigo,
                        CreationTime = m.CreationTime,
                        CreatorUserId = m.CreatorUserId,
                        DataAdmissao = m.DataAdmissao,
                        DeleterUserId = m.DeleterUserId,
                        DeletionTime = m.DeletionTime,
                        Descricao = m.Descricao,
                        FormRespostaId = m.FormRespostaId,
                        Id = m.Id,
                        IsDeleted = m.IsDeleted,
                        IsSistema = m.IsSistema,
                        LastModificationTime = m.LastModificationTime,
                        LastModifierUserId = m.LastModifierUserId,
                        Observacao = m.Observacao,
                        OperacaoId = m.OperacaoId,
                        ProfissionalSaudeId = m.ProfissionalSaudeId,
                        ProntuarioPrincipalId = m.ProntuarioPrincipalId,
                        UnidadeOrganizacionalId = m.UnidadeOrganizacionalId
                    })
                    .ToListAsync();

                var prontuariosDto = query.ToList();

                //.MapTo<List<ProntuarioEletronicoDto>>();
                return new ListResultDto<ProntuarioEletronicoDto> { Items = prontuariosDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public async Task<FileDto> ListarParaExcel(ListarInput input)
        //{
        //    try
        //    {
        //        var result = await Listar(input);
        //        var prontuarios = result.Items;
        //        return _listarProntuariosExcelExporter.ExportToFile(prontuarios.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroExportar"));
        //    }
        //}

        [UnitOfWork(false)]
        public async Task<ProntuarioEletronicoDto> Obter(long id)
        {
            try
            {
                var query = _prontuarioRepository
                    .GetAll()
                    .Where(m => m.Id == id)
                    .Select(m => new ProntuarioEletronicoDto
                    {
                        AtendimentoId = m.AtendimentoId,
                        Codigo = m.Codigo,
                        CreationTime = m.CreationTime,
                        CreatorUserId = m.CreatorUserId,
                        DataAdmissao = m.DataAdmissao,
                        DeleterUserId = m.DeleterUserId,
                        DeletionTime = m.DeletionTime,
                        Descricao = m.Descricao,
                        FormRespostaId = m.FormRespostaId,
                        Id = m.Id,
                        IsDeleted = m.IsDeleted,
                        IsSistema = m.IsSistema,
                        LastModificationTime = m.LastModificationTime,
                        LastModifierUserId = m.LastModifierUserId,
                        Observacao = m.Observacao,
                        OperacaoId = m.OperacaoId,
                        ProfissionalSaudeId = m.ProfissionalSaudeId,
                        ProntuarioPrincipalId = m.ProntuarioPrincipalId,
                        UnidadeOrganizacionalId = m.UnidadeOrganizacionalId
                    });

                var prontuarioDto = await query.FirstOrDefaultAsync();
                //if (prontuarioDto.FormRespostaId.HasValue)
                //{
                //    prontuarioDto.FormResposta = await _formRespostaAppService.Obter(prontuarioDto.FormRespostaId.Value);
                //}
                if (prontuarioDto.OperacaoId.HasValue)
                {
                    prontuarioDto.Operacao = await _operacaoAppService.Obter(prontuarioDto.OperacaoId.Value);
                }
                if (prontuarioDto.ProfissionalSaudeId.HasValue)
                {

                }
                if (prontuarioDto.UnidadeOrganizacionalId > 0)
                {
                    prontuarioDto.UnidadeOrganizacional = await _unidadeOrganizacionalAppService.ObterPorId(prontuarioDto.UnidadeOrganizacionalId);
                }
                return prontuarioDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork(false)]
        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _prontuarioRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.Descricao) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
