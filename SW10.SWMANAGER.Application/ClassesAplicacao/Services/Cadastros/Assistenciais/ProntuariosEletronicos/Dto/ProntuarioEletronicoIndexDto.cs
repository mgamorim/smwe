﻿using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos.Dto
{
    public class ProntuarioEletronicoIndexDto : CamposPadraoCRUDDto
    {
        public string CodigoAtendimento { get; set; }
        public string Paciente { get; set; }
        public string Medico { get; set; }
        public DateTime? DataAdmissao { get; set; }
        public string UnidadeOrganizacional { get; set; }
        public string Empresa { get; set; }
        public string Formulario { get; set; }
        public long? FormRespostaId { get; set; }
    }
}
