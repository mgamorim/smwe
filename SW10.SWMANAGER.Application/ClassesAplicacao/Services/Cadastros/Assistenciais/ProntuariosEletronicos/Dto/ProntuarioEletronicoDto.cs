﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Assistenciais;
using SW10.SWMANAGER.ClassesAplicacao.Configuracoes.GeradorFormularios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Atendimentos.Atendimentos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProfissionaisSaude;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.UnidadesOrganizacionais.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.GeradorFormularios.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Configuracoes.Operacoes.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Assistenciais.ProntuariosEletronicos.Dto
{
    [AutoMap(typeof(Prontuario))]
    public class ProntuarioEletronicoDto : CamposPadraoCRUDDto
    {
        public DateTime DataAdmissao { get; set; }

        public long UnidadeOrganizacionalId { get; set; }

        public UnidadeOrganizacionalDto UnidadeOrganizacional { get; set; }

        public long AtendimentoId { get; set; }

        public AtendimentoDto Atendimento { get; set; }

        public long? ProfissionalSaudeId { get; set; }

        public ProfissionalSaudeDto ProfissionalSaude { get; set; }

        public string Observacao { get; set; }

        public long? FormRespostaId { get; set; }

        public FormResposta FormResposta { get; set; }

        public long? OperacaoId { get; set; }

        public OperacaoDto Operacao { get; set; }

        public long? ProntuarioPrincipalId { get; set; }

        public ProntuarioEletronicoDto ProntuarioPrincipal { get; set; }

        public ICollection<ProntuarioEletronicoDto> SubProntuarios { get; set; }

        public ICollection<ProntuarioEletronicoLogDto> ProntuarioLogs { get; set; }
    }
}
