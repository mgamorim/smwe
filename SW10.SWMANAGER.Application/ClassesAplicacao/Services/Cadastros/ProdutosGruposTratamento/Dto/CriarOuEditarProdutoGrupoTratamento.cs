﻿
using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.ProdutosGruposTratamento;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosGruposTratamento.Dto
{
    [AutoMap(typeof(ProdutoGrupoTratamento))]
    public class CriarOuEditarProdutoGrupoTratamento : CamposPadraoCRUDDto
    {

        public string Descricao { get; set; }

    }
}
