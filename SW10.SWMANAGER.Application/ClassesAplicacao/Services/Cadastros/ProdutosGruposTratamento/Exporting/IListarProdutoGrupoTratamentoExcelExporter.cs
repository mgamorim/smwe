﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosGruposTratamento.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosGruposTratamento.Exporting
{
    public interface IListarProdutoGrupoTratamentoExcelExporter
    {
        FileDto ExportToFile(List<ProdutoGrupoTratamentoDto> produtoGrupoTratamentoDtos);
    }
}
