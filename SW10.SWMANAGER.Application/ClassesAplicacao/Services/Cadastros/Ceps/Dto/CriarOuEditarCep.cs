﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.CEP;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Ceps.Dto
{
    [AutoMap(typeof(Cep))]
    public class CriarOuEditarCep : CamposPadraoCRUDDto
    {
        public string CEP { get; set; }

        public string Logradouro { get; set; }

        public string Bairro { get; set; }

        public string Complemento { get; set; }

        public string Complemento2 { get; set; }

        public string UnidadePostagem { get; set; }

        public long TipoLogradouroId { get; set; }

        public virtual TipoLogradouroDto TipoLogradouro { get; set; }


        public long CidadeId { get; set; }

        public virtual CidadeDto Cidade { get; set; }

        public long EstadoId { get; set; }

        public virtual EstadoDto Estado { get; set; }

        public long PaisId { get; set; }

       public virtual PaisDto Pais { get; set; }
    }
}