﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.CEP;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Ceps.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Ceps.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Dto;
using SW10.SWMANAGER.CorreiosService;
using SW10.SWMANAGER.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Ceps
{
    public class CepAppService : SWMANAGERAppServiceBase, ICepAppService
    {
        private readonly IRepository<Cep, long> _cepRepositorio;
        private readonly ICidadeAppService _cidadeAppService;
        private readonly IEstadoAppService _estadoAppService;
        private readonly IPaisAppService _paisAppService;
        private readonly IListarCepExcelExporter _listarCepExcelExporter;
        private readonly ITipoLogradouroAppService _tipoLogradouroAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CepAppService(
            IRepository<Cep, long> cepRepositorio,
            ICidadeAppService cidadeAppService,
            IEstadoAppService estadoAppService,
            IPaisAppService paisAppService,
            IListarCepExcelExporter listarCepExcelExporter,
            ITipoLogradouroAppService tipoLogradouroAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _cepRepositorio = cepRepositorio;
            _cidadeAppService = cidadeAppService;
            _estadoAppService = estadoAppService;
            _paisAppService = paisAppService;
            _listarCepExcelExporter = listarCepExcelExporter;
            _tipoLogradouroAppService = tipoLogradouroAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }
        [UnitOfWork]
        public async Task<Cep> CriarOuEditar(CriarOuEditarCep input)
        {
            try
            {
                var cep = input.MapTo<Cep>();
                var novoCep = cep;
                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        novoCep.Id = await _cepRepositorio.InsertAndGetIdAsync(cep);
                        unitOfWork.Complete();
                        unitOfWork.Dispose();
                        return novoCep;
                    }
                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        var cepEntity = _cepRepositorio.GetAll()
                                                       .Where(w => w.Id == input.Id)
                                                       .FirstOrDefault();

                        if (cepEntity != null)
                        {
                            cepEntity.Bairro = input.Bairro;
                            cepEntity.CEP = input.CEP;
                            cepEntity.CidadeId = input.CidadeId;
                            cepEntity.EstadoId = input.EstadoId;
                            cepEntity.Logradouro = input.Logradouro;
                            cepEntity.PaisId = input.PaisId;
                            cepEntity.TipoLogradouroId = input.TipoLogradouroId;

                            novoCep = await _cepRepositorio.UpdateAsync(cepEntity);
                            unitOfWork.Complete();
                            unitOfWork.Dispose();
                        }
                        return novoCep;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }
        [UnitOfWork]
        public async Task Excluir(CriarOuEditarCep input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _cepRepositorio.DeleteAsync(input.Id);
                    unitOfWork.Complete();
                    unitOfWork.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        public async Task<PagedResultDto<CepDto>> Listar(ListarCepsInput input)
        {
            var contarCep = 0;
            List<Cep> cep;
            List<CepDto> CepDtos = new List<CepDto>();
            try
            {
                var query = _cepRepositorio
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.Logradouro.Contains(input.Filtro));

                contarCep = await query
                    .CountAsync();

                cep = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                CepDtos = cep
                    .MapTo<List<CepDto>>();

                return new PagedResultDto<CepDto>(
                    contarCep,
                    CepDtos
                    );
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public async Task<ListResultDto<CepDto>> ListarAutoComplete(string input, long? estadoId)
        //{
        //    try
        //    {
        //        var query = await _cepRepositorio
        //            .GetAll()
        //            .WhereIf(!input.IsNullOrEmpty(), m =>
        //                m.CEP.ToUpper().Contains(input.ToUpper())
        //            )
        //            .WhereIf(estadoId.HasValue, m =>
        //             m.EstadoId == estadoId)
        //            .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.CEP })
        //            .ToListAsync();

        //        var ceps = new ListResultDto<GenericoIdNome> { Items = query };

        //        List<CepDto> cepsList = new List<CepDto>();

        //        foreach (var cep in ceps.Items)
        //        {
        //            cepsList.Add(cep.MapTo<CepDto>());
        //        }

        //        ListResultDto<CepDto> cidadesDto = new ListResultDto<CepDto> { Items = cepsList };

        //        return CepDto;

        //        //	return new ListResultDto<CidadeDto> { Items = query };
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}

        public async Task<FileDto> ListarParaExcel(ListarCepsInput input)
        {
            try
            {
                var result = await Listar(input);
                var cep = result.Items;
                return _listarCepExcelExporter.ExportToFile(cep.ToList());
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<CriarOuEditarCep> Obter(long id)
        {
            try
            {
                var result = await _cepRepositorio
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();
                var cep = result.MapTo<CriarOuEditarCep>();

                return cep;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<CepDto> Obter(string cep)
        {
            try
            {
                var query = _cepRepositorio
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Where(m => m.CEP == cep);
                var result = await query.FirstOrDefaultAsync();
                var _cep = result.MapTo<CepDto>();

                return _cep;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<CepCorreios> ConsultaCep(string cep)
        {
            //caso o cep já exista no banco, o retorno precisa popular o Id para que o serviço entenda que se trata de uma edição
            var cepCorreios = new CepCorreios();
            try
            {
                var _cep = await Obter(cep);
                if (_cep != null)
                {
                    cepCorreios = new CepCorreios
                    {
                        Bairro = _cep.Bairro,
                        Cep = _cep.CEP,
                        Cidade = _cep.Cidade.Nome,
                        CidadeId = _cep.CidadeId,
                        Complemento = _cep.Complemento,
                        Complemento2 = _cep.Complemento,
                        End = _cep.Logradouro,
                        EstadoId = _cep.EstadoId,
                        PaisId = _cep.PaisId,
                        Uf = _cep.Estado.Uf,
                        UnidadesPostagem = _cep.UnidadePostagem,
                        Id = _cep.Id,
                        IsDeleted = _cep.IsDeleted,
                        IsSistema = _cep.IsSistema
                    };
                }
                else
                {
                    cepCorreios = new CepCorreios();
                    var service = new AtendeClienteClient();
                    var endereco = await service.consultaCEPAsync(cep);
                    service.Close();
                    if (endereco.@return.end.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException(L("CepInvalido"));
                    }
                    cepCorreios.Bairro = endereco.@return.bairro;
                    cepCorreios.Cidade = endereco.@return.cidade;
                    cepCorreios.Uf = endereco.@return.uf;
                    cepCorreios.Cep = endereco.@return.cep;
                    cepCorreios.Complemento = endereco.@return.complemento;
                    cepCorreios.Complemento2 = endereco.@return.complemento2;
                    cepCorreios.End = endereco.@return.end;
                    cepCorreios.Bairro = endereco.@return.bairro;

                    //procurando o estado
                    var estado = await _estadoAppService.Obter(cepCorreios.Uf);

                    if (estado == null)
                    {
                        //estado não existe, cadastrar
                        var inputEstado = new EstadoDto();
                        inputEstado.PaisId = 1; //Brasil
                        inputEstado.Nome = cepCorreios.Uf;
                        inputEstado.Uf = cepCorreios.Uf;
                        await _estadoAppService.CriarOuEditar(inputEstado);
                        estado = await _estadoAppService.Obter(cepCorreios.Uf);
                    }
                    cepCorreios.EstadoId = estado.Id;
                    cepCorreios.PaisId = estado.PaisId;

                    //procurando a cidade
                    var cidade = await _cidadeAppService.ObterComEstado(cepCorreios.Cidade, estado.Id);
                    if (cidade == null)
                    {
                        //se não existir, incluir
                        var inputCidade = new CidadeDto();
                        inputCidade.Capital = false;
                        inputCidade.EstadoId = estado.Id;
                        inputCidade.Nome = cepCorreios.Cidade;
                        await _cidadeAppService.CriarOuEditar(inputCidade);
                        cidade = await _cidadeAppService.ObterComEstado(cepCorreios.Cidade, estado.Id);
                    }
                    cepCorreios.CidadeId = cidade.Id;

                    var end = cepCorreios.End.Split(' ');
                    var tipoLogradouro = await _tipoLogradouroAppService.Obter(end[0].ToString());
                    if (tipoLogradouro == null)
                    {
                        var inputTipoLogradouro = new CriarOuEditarTipoLogradouroDto();
                        inputTipoLogradouro.Descricao = end[0]; //tipoLogradouro.Descricao;
                        inputTipoLogradouro.Abreviacao = end[0]; //tipoLogradouro.Abreviacao;
                        await _tipoLogradouroAppService.CriarOuEditar(inputTipoLogradouro);
                    }
                }
                return cepCorreios;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(L("CepNaoEncontrado"));
            }
        }
    }
}
