﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros
{
    [AutoMap(typeof(TipoFrete))]
    public class TipoFreteDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
