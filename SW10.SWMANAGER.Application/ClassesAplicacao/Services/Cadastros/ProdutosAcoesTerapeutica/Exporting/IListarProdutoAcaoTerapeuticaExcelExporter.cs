﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosAcoesTerapeutica.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosAcoesTerapeutica.Exporting
{
    public interface IListarProdutoAcaoTerapeuticaExcelExporter
    {
        FileDto ExportToFile(List<ProdutoAcaoTerapeuticaDto> produtoAcaoTerapeuticaDtos);
    }
}