﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Profissoes;
using System.ComponentModel.DataAnnotations;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Profissoes.Dto
{

    [AutoMap(typeof(Profissao))]
    public class CriarOuEditarProfissao : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
