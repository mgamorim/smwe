﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.CEP;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Cidades;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Estados;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposLogradouro;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Planos.Dto;
using System;
using System.Collections.Generic;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto
{
    [AutoMap(typeof(Convenio))]
    public class ConvenioDto : PessoaJuridicaDto
    {
        public SisPessoa SisPessoa { get; set; }
        public string Nome { get; set; }

        public bool IsAtivo { get; set; }

        public byte[] Logotipo { get; set; }

        public string LogotipoMimeType { get; set; }

        public bool IsFilotranpica { get; set; }

        public string LogradouroCobranca { get; set; }

        public long? CepCobrancaId { get; set; }
        public virtual Cep CepCobranca { get; set; }

        public long? TipoLogradouroCobrancaId { get; set; }

        public virtual TipoLogradouro TipoLogradouroCobranca { get; set; }

        public string NumeroCobranca { get; set; }

        public string ComplementoCobranca { get; set; }

        public string BairroCobranca { get; set; }

        public long? CidadeCobrancaId { get; set; }

        public virtual Cidade CidadeCobranca { get; set; }

        public long? EstadoCobrancaId { get; set; }

        public virtual Estado EstadoCobranca { get; set; }

         public string Cargo { get; set; }

        public DateTime DataInicioContrato { get; set; }

        public int Vigencia { get; set; }

        public DateTime DataProximaRenovacaoContratual { get; set; }

        public DateTime DataInicialContrato { get; set; }

        public DateTime DataUltimaRenovacaoContrato { get; set; }

        public string RegistroANS { get; set; }
        //public virtual ICollection<PlanoDto> Planos { get; set; }

        public List<IdentificacaoPrestadorNaOperadoraDto> IdentificacoesPrestadoresNaOperadoraDto { get; set; }


        public static ConvenioDto Mapear(Convenio convenio)
        {
            ConvenioDto convenioDto = new ConvenioDto();

            convenioDto.Id = convenio.Id;
            convenioDto.Codigo = convenio.Codigo;
            convenioDto.Descricao = convenio.Descricao;
            convenioDto.Nome = convenio.Nome;
            convenioDto.NomeFantasia = convenio.NomeFantasia;
            convenioDto.RegistroANS = convenio.RegistroANS;

            if(convenio.IdentificacoesPrestadoresNaOperadora!=null)
            {
                foreach (var item in convenio.IdentificacoesPrestadoresNaOperadora)
                {
                    item.Convenio = null;
                    convenioDto.IdentificacoesPrestadoresNaOperadoraDto.Add(IdentificacaoPrestadorNaOperadoraDto.Mapear(item));
                }
            }

            return convenioDto;

        }


        public static Convenio Mapear(ConvenioDto convenioDto)
        {
            Convenio convenio = new Convenio();

            convenio.Id = convenioDto.Id;
            convenio.Codigo = convenioDto.Codigo;
            convenio.Descricao = convenioDto.Descricao;
            convenio.Nome = convenioDto.Nome;
            convenio.NomeFantasia = convenioDto.NomeFantasia;

            return convenio;

        }



    }
}