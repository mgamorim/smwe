﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pessoas.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.TiposLogradouros.Dto;
using System;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto
{

    [AutoMap(typeof(Convenio))]
    public class CriarOuEditarConvenio : PessoaJuridicaDto
    {
        public string Nome { get; set; }

        public bool IsAtivo { get; set; }

        public byte[] Logotipo { get; set; }

        public string LogotipoMimeType { get; set; }

        public bool IsFilotranpica { get; set; }

        public string LogradouroCobranca { get; set; }

        public long? CepCobrancaId { get; set; }

        public long? TipoLogradouroCobrancaId { get; set; }

        public string NumeroCobranca { get; set; }

        public string ComplementoCobranca { get; set; }

        public string BairroCobranca { get; set; }

        public long? CidadeCobrancaId { get; set; }

        public long? EstadoCobrancaId { get; set; }

        public string Cargo { get; set; }

        public DateTime DataInicioContrato { get; set; }

        public int Vigencia { get; set; }

        public DateTime DataProximaRenovacaoContratual { get; set; }

        public DateTime DataInicialContrato { get; set; }

        public DateTime DataUltimaRenovacaoContrato { get; set; }

        public long? SisPessoaId { get; set; }
        public SisPessoaDto SisPessoa { get; set; }

        public string RegistroANS { get; set; }

        public void MapearFromConvenio (Convenio core)
        {
            this.Id = core.Id;
            this.Nome                               = core.Nome;
            this.IsAtivo                            = core.IsAtivo;
            this.Logotipo                           = core.Logotipo;
            this.LogotipoMimeType                   = core.LogotipoMimeType;
            this.IsFilotranpica                     = core.IsFilotranpica;
            this.LogradouroCobranca                 = core.LogradouroCobranca;
            this.CepCobrancaId                      = core.CepCobrancaId;
            this.TipoLogradouroCobrancaId           = core.TipoLogradouroCobrancaId;
            this.NumeroCobranca                     = core.NumeroCobranca;
            this.ComplementoCobranca                = core.ComplementoCobranca;
            this.BairroCobranca                     = core.BairroCobranca;
            this.CidadeCobrancaId                   = core.CidadeCobrancaId;
            this.EstadoCobrancaId                   = core.EstadoCobrancaId;
            this.Cargo                              = core.Cargo;
            this.DataInicioContrato                 = core.DataInicioContrato;
            this.Vigencia                           = core.Vigencia;
            this.DataProximaRenovacaoContratual     = core.DataProximaRenovacaoContratual;
            this.DataInicialContrato                = core.DataInicialContrato;
            this.DataUltimaRenovacaoContrato        = core.DataUltimaRenovacaoContrato;
            this.SisPessoaId                        = core.SisPessoaId;
            this.SisPessoa                          = core.SisPessoa.MapTo<SisPessoaDto>();
            this.RegistroANS = core.RegistroANS;

            this.RazaoSocial = core.RazaoSocial;
            this.NomeFantasia = core.NomeFantasia;
            this.Cnpj = core.Cnpj;
            this.InscricaoEstadual = core.InscricaoEstadual;
            this.InscricaoMunicipal = core.InscricaoMunicipal;
            this.Cep = core.Cep;
            this.TipoLogradouroId = core.TipoLogradouroId;
            this.Logradouro = core.Logradouro;
            this.Complemento = core.Complemento;
            this.Numero = core.Numero;
            this.Bairro = core.Bairro;
            this.CidadeId = core.CidadeId;
            this.EstadoId = core.EstadoId;
            this.PaisId = core.PaisId;
            this.Telefone1 = core.Telefone1;
          //  this.TipoTelefone1 = core.TipoTelefone2;
            this.Telefone2 = core.Telefone3;
          //  this.TipoTelefone2 = core.TipoTelefone2;
            this.Telefone3 = core.Telefone3;
         //   this.TipoTelefone3 = core.TipoTelefone3;
            this.Telefone4 = core.Telefone4;
         //   this.TipoTelefone4 = core.TipoTelefone4;
            this.Email = core.Email;
            this.Cidade               = core.Cidade.MapTo<CidadeDto>();
            this.Estado               = core.Estado.MapTo<EstadoDto>();
            this.Pais                 = core.Pais.MapTo<PaisDto>();
            this.TipoLogradouro       = core.TipoLogradouro.MapTo<TipoLogradouroDto>();
        }
    }
}
