﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Convenios;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Exporting;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Domain.Uow;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Enderecos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto;
using Microsoft.AspNet.Identity;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios
{
    public class ConvenioAppService : SWMANAGERAppServiceBase, IConvenioAppService
    {
        #region Cabecalho
        private readonly IRepository<Convenio, long> _convenioRepository;
        private readonly IListarConveniosExcelExporter _listarConveniosExcelExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<SisPessoa, long> _sisPessoa;

        public ConvenioAppService(IRepository<Convenio, long> convenioRepository
                                , IListarConveniosExcelExporter listarConveniosExcelExporter
                                , IUnitOfWorkManager unitOfWorkManager
                                , IRepository<SisPessoa, long> sisPessoa)
        {
            _convenioRepository = convenioRepository;
            _listarConveniosExcelExporter = listarConveniosExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _sisPessoa = sisPessoa;
        }
        #endregion cabecalho.

        [UnitOfWork]//Atualizado (Pablo 08/08/2017)
        public async Task CriarOuEditar(CriarOuEditarConvenio input)
        {
            //precisa acertar no CORE, excluir propriedade repetida 07/08/2017 pablo
            input.DataInicioContrato = input.DataInicialContrato;
            try
            {

                //input.MapTo<Convenio>();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.Id.Equals(0))
                    {
                        await _convenioRepository.InsertAsync(CarregarDadosConvenio(input));
                    }
                    else
                    {
                        //var novoConvenio = _convenioRepository.GetAll()
                        //                                      .Where(w => w.Id == input.Id)
                        //                                      .Include(i => i.SisPessoa)
                        //                                      .FirstOrDefault();

                        //if(novoConvenio !=null )
                        //{
                        //    novoConvenio.SisPessoa.NomeFantasia = input.NomeFantasia;

                        await _convenioRepository.UpdateAsync(CarregarDadosConvenio(input));
                        //}
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(CriarOuEditarConvenio input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var convenio = _convenioRepository.GetAll()
                                                      .Include(i=> i.SisPessoa)
                                                      .Where(w=> w.Id == input.Id)
                                                      .FirstOrDefault();

                    if (convenio != null)
                    {
                       
                        _convenioRepository.Delete(convenio);
                        unitOfWork.Complete();
                       _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }


       






        [UnitOfWork]
        public async Task ExcluirPorId(long id)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _convenioRepository.DeleteAsync(id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }


        public async Task<PagedResultDto<ConvenioDto>> Listar(ListarConveniosInput input)
        {
            var contarConvenios = 0;
            List<Convenio> convenios;
            List<ConvenioDto> conveniosDtos = new List<ConvenioDto>();
            try
            {
                var query = _convenioRepository
                    .GetAll()
                    .Include(m => m.CepCobranca)
                    .Include(m => m.CidadeCobranca)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.EstadoCobranca)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoLogradouroCobranca)
                    .Include(m => m.SisPessoa)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.RazaoSocial.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cnpj.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.InscricaoEstadual.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.InscricaoMunicipal.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DataInicialContrato.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone1.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone2.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone3.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone4.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DataUltimaRenovacaoContrato.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.DataProximaRenovacaoContratual.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Email.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Logradouro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Bairro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Estado.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Estado.Uf.ToUpper().Contains(input.Filtro.ToUpper())
                    //m.NumeroRegistroAns.ToString().ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarConvenios = await query
                    .CountAsync();

                convenios = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                conveniosDtos = convenios
                    .MapTo<List<ConvenioDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ConvenioDto>(
                contarConvenios,
                conveniosDtos
                );
        }

        public async Task<ListResultDto<ConvenioDto>> ListarTodos()
        {
            try
            {
                var query = await _convenioRepository
                    .GetAll()
                    .Include(m => m.CepCobranca)
                    .Include(m => m.CidadeCobranca)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.EstadoCobranca)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoLogradouroCobranca)
                      .Include(m => m.SisPessoa)
                    .ToListAsync();

                // var conveniosDto = query.MapTo<List<ConvenioDto>>();

                var conveniosDto = new List<ConvenioDto>();
                foreach (var item in query)
                {
                    ConvenioDto convenioDto = new ConvenioDto();

                    convenioDto.Id = item.Id;
                    convenioDto.Codigo = item.Codigo;
                    convenioDto.NomeFantasia = item.NomeFantasia;

                    conveniosDto.Add(convenioDto);
                }


                return new ListResultDto<ConvenioDto> { Items = conveniosDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _convenioRepository
                    .GetAll()
                    .Include(m => m.CepCobranca)
                    .Include(m => m.CidadeCobranca)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.EstadoCobranca)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoLogradouroCobranca)
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.NomeFantasia.ToUpper().Contains(input.ToUpper()) ||
                        m.RazaoSocial.ToUpper().Contains(input.ToUpper()) ||
                        m.Cnpj.ToUpper().Contains(input.ToUpper()) ||
                        m.InscricaoEstadual.ToUpper().Contains(input.ToUpper()) ||
                        m.InscricaoMunicipal.ToUpper().Contains(input.ToUpper()) ||
                        m.DataInicialContrato.ToString().ToUpper().Contains(input.ToUpper()) ||
                        m.Telefone1.ToUpper().Contains(input.ToUpper()) ||
                        m.Telefone2.ToUpper().Contains(input.ToUpper()) ||
                        m.Telefone3.ToUpper().Contains(input.ToUpper()) ||
                        m.Telefone4.ToUpper().Contains(input.ToUpper()) ||
                        m.DataUltimaRenovacaoContrato.ToString().ToUpper().Contains(input.ToUpper()) ||
                        m.DataProximaRenovacaoContratual.ToString().ToUpper().Contains(input.ToUpper()) ||
                        m.Email.ToUpper().Contains(input.ToUpper()) ||
                        m.Logradouro.ToUpper().Contains(input.ToUpper()) ||
                        m.Bairro.ToUpper().Contains(input.ToUpper()) ||
                        m.Cidade.Nome.ToUpper().Contains(input.ToUpper()) ||
                        m.Estado.Nome.ToUpper().Contains(input.ToUpper()) ||
                        m.Estado.Uf.ToUpper().Contains(input.ToUpper())
                    //m.NumeroRegistroAns.ToString().ToUpper().Contains(input.ToUpper())
                    )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.NomeFantasia })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<FileDto> ListarParaExcel(ListarConveniosInput input)
        {
            try
            {
                var result = await Listar(input);
                var convenios = result.Items;
                return _listarConveniosExcelExporter.ExportToFile(convenios.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExportar"));
            }
        }

        public async Task<CriarOuEditarConvenio> Obter(long id)
        {
            if (id == 0)
            {
                return null;
            }

            try
            {
                var result = await _convenioRepository
                    .GetAll()
                    .Include(m => m.CepCobranca)
                    .Include(m => m.CidadeCobranca)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.EstadoCobranca)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoLogradouroCobranca)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Pais))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Estado))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Cidade))
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                //var convenio = result
                //    //.FirstOrDefault()
                //    .MapTo<CriarOuEditarConvenio>();

                // Mapeamento manual emergencial (o auto map acima esta derrubando o servidor)
                CriarOuEditarConvenio convenio = new CriarOuEditarConvenio();
                convenio.MapearFromConvenio(result);

                if (result.SisPessoa != null && result.SisPessoa.Enderecos != null && result.SisPessoa.Enderecos.Count > 0)
                {
                    var endereco = result.SisPessoa.Enderecos[0];
                    convenio.Cep = endereco.Cep;
                    convenio.PaisId = endereco.PaisId;
                    convenio.Logradouro = endereco.Logradouro;
                    convenio.Numero = endereco.Numero;
                    convenio.Complemento = endereco.Complemento;
                    convenio.Bairro = endereco.Bairro;
                    convenio.EstadoId = endereco.EstadoId;
                    convenio.CidadeId = endereco.CidadeId;
                    convenio.Pais = endereco.Pais.MapTo<PaisDto>();
                    convenio.Estado = endereco.Estado.MapTo<EstadoDto>();
                    convenio.Cidade = endereco.Cidade.MapTo<CidadeDto>();
                }

                return convenio;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ConvenioDto> ObterDto(long id)
        {
            try
            {
                var result = await _convenioRepository
                    .GetAll()
                    .Include(m => m.CepCobranca)
                    .Include(m => m.CidadeCobranca)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.EstadoCobranca)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoLogradouroCobranca)
                    .Include(m => m.SisPessoa)
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var convenio = result
                    //.FirstOrDefault()
                    .MapTo<ConvenioDto>();

                return convenio;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            try
            {
                //get com filtro
                var query = from p in _convenioRepository.GetAll().Include(i => i.SisPessoa)
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.NomeFantasia.ToLower()
                        .Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        .Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        .Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        .Replace("ã", "a").Replace("õ", "o")
                        .Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        .Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        .Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        .Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.NomeFantasia ascending
                            select new DropdownItems { id = p.Id, text = string.Concat(p.Codigo, " - ", p.NomeFantasia) };
                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        Convenio CarregarDadosConvenio(CriarOuEditarConvenio input)
        {
            Convenio convenio = null;

            if (input.Id == 0)
            {
                convenio = new Convenio();
            }
            else
            {
                var convenioAtual = _convenioRepository.GetAll()
                                                .Where(w => w.Id == input.Id)
                                                .Include(i => i.SisPessoa)
                                                .Include(i => i.SisPessoa.Enderecos)
                                                .FirstOrDefault();

                convenio = convenioAtual;// ?? new Convenio();
            }

            //convenio.Id = input.Id;
            convenio.DataInicialContrato = input.DataInicialContrato;
            convenio.DataUltimaRenovacaoContrato = input.DataUltimaRenovacaoContrato;
            convenio.DataProximaRenovacaoContratual = input.DataProximaRenovacaoContratual;
            convenio.IsAtivo = input.IsAtivo;
            convenio.RegistroANS = input.RegistroANS;
            convenio.Logotipo = input.Logotipo;
            convenio.LogotipoMimeType = input.LogotipoMimeType;

            convenio.DataInicioContrato = DateTime.Now;

            SisPessoa sisPessoa = convenio.SisPessoa;

            if (convenio.SisPessoa == null)
            {
                sisPessoa = _sisPessoa.GetAll()
                                               .Where(w => w.Cnpj == input.Cnpj)
                                               .FirstOrDefault();

                convenio.SisPessoa = sisPessoa;
            }



            if (sisPessoa != null)
            {
                convenio.SisPessoaId = sisPessoa.Id;

                if (sisPessoa.Enderecos != null && sisPessoa.Enderecos.Count() > 0)
                {
                    CarregarEndereco(input, sisPessoa.Enderecos[0]);
                }
                else
                {
                    var endereco = new Endereco();
                    CarregarEndereco(input, endereco);

                    sisPessoa.Enderecos = new List<Endereco> { endereco };
                }


                //  convenio.SisPessoa = sisPessoa;



            }
            else
            {
                sisPessoa = new SisPessoa();
                convenio.SisPessoa = sisPessoa;



                var endereco = new Endereco();
                sisPessoa.Enderecos = new List<Endereco>();

                CarregarEndereco(input, endereco);
                sisPessoa.Enderecos.Add(endereco);
            }


            sisPessoa.NomeFantasia = input.NomeFantasia;
            sisPessoa.RazaoSocial = input.RazaoSocial;
            sisPessoa.Cnpj = input.Cnpj;
            sisPessoa.InscricaoEstadual = input.InscricaoEstadual;
            sisPessoa.InscricaoMunicipal = input.InscricaoMunicipal;
            sisPessoa.Telefone1 = input.Telefone1;
            sisPessoa.Telefone2 = input.Telefone2;
            sisPessoa.Telefone3 = input.Telefone3;
            sisPessoa.Telefone4 = input.Telefone4;
            sisPessoa.Email = input.Email;
            // sisPessoa.Nascimento = DateTime.Now;
            sisPessoa.TipoPessoaId = 2;
            sisPessoa.FisicaJuridica = "J";

            return convenio;
        }

        void CarregarEndereco(CriarOuEditarConvenio input, Endereco endereco)
        {
            endereco.Bairro = input.Bairro;
            endereco.Cep = input.Cep;
            endereco.CidadeId = input.CidadeId;
            endereco.Complemento = input.Complemento;
            endereco.EstadoId = input.EstadoId;
            endereco.Logradouro = input.Logradouro;
            endereco.Numero = input.Numero;
            endereco.PaisId = input.PaisId;
        }

        public async Task<CriarOuEditarConvenio> ObterCNPJ(string cnpj)
        {
            try
            {
                var result = await _convenioRepository
                    .GetAll()
                    .Include(m => m.CepCobranca)
                    .Include(m => m.CidadeCobranca)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.EstadoCobranca)
                    .Include(m => m.Pais)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoLogradouroCobranca)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Where(m => m.SisPessoa.Cnpj == cnpj)
                    .FirstOrDefaultAsync();

                var convenio = result
                    //.FirstOrDefault()
                    .MapTo<CriarOuEditarConvenio>();

                if (result != null && result.SisPessoa != null && result.SisPessoa.Enderecos != null && result.SisPessoa.Enderecos.Count > 0)
                {
                    var endereco = result.SisPessoa.Enderecos[0];
                    convenio.Cep = endereco.Cep;
                    convenio.PaisId = endereco.PaisId;
                    convenio.Logradouro = endereco.Logradouro;
                    convenio.Numero = endereco.Numero;
                    convenio.Complemento = endereco.Complemento;
                    convenio.Bairro = endereco.Bairro;
                }

                return convenio;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
    }
}
