﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios.Dto;
using SW10.SWMANAGER.Dto;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Convenios
{
    public interface IConvenioAppService : IApplicationService
    {
        Task<PagedResultDto<ConvenioDto>> Listar(ListarConveniosInput input);

		Task<ListResultDto<GenericoIdNome>> ListarAutoComplete (string input);

		Task<ListResultDto<ConvenioDto>> ListarTodos ();
        
		Task CriarOuEditar (CriarOuEditarConvenio input);

        Task Excluir(CriarOuEditarConvenio input);

        Task<CriarOuEditarConvenio> Obter(long id);

        Task<ConvenioDto> ObterDto (long id);

        Task<FileDto> ListarParaExcel(ListarConveniosInput input);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);
        Task<CriarOuEditarConvenio> ObterCNPJ(string cnpj);
        Task ExcluirPorId(long id);
    }
}
