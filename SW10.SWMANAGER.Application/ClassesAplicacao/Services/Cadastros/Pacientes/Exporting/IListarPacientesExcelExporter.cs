﻿using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Exporting
{
    public interface IListarPacientesExcelExporter
    {
        FileDto ExportToFile(List<PacienteDto> pacientesDtos);
    }
}
