﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.ViewModels;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes
{
    public interface IPacienteAppService : IApplicationService
    {
        Task<PagedResultDto<PacienteDto>> Listar(ListarPacientesInput input);

        Task<ListResultDto<PacienteDto>> ListarTodos();

        Task<List<Paciente>> ListarAutoComplete(string term);

        Task<PagedResultDto<ListarPacientesIndex>> ListarParaIndex(ListarPacientesInput input);

        // temp
       // Task<PagedResultDto<ListarPacientesIndex>> ListarParaIndex();

        Task<long> CriarOuEditarOriginal(PacienteDto input);

        Task<long> CriarOuEditar(PacienteDto input);

        Task Excluir(PacienteDto input);

        Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput);

        Task<ResultDropdownList> ListarPacientesEmAtendimentoDropdown(DropdownInput dropdownInput);

        Task<PacienteDto> Obter(long id);

        Task<PacienteDto> Obter2(long id);

        Task<PacienteDto> ObterNoMap(long id);

        Task<FileDto> ListarParaExcel(ListarPacientesInput input);

        Task<ListResultDto<VWTesteDto>> ListarResumo();

        Task<PacienteDto> ObterPorCpf(string cpf);
    }
}
