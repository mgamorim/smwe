﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes
{
	public interface IPacientePesoAppService : IApplicationService
	{
		Task CriarOuEditar(PacientePesoDto input);

		Task Excluir(PacientePesoDto input);

		Task<PacientePesoDto> Obter(long id);
	}
}
