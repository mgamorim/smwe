﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Enumeradores
{
    public enum EnumSexo
    {
        Masculino =1,
        Feminino =2,
        Ambos =3
    }
}
