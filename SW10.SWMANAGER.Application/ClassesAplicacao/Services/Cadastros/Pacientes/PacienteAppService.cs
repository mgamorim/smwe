﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Dto;
using SW10.SWMANAGER.Dto;
using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Pacientes;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes.Exporting;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Collections.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.ViewModels;
using SW10.SWMANAGER.ClassesAplicacao.ViewModels;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using Abp.Threading;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.Enderecos;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Paises.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Estados.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Cidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Nacionalidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Cadastros.TiposLogradouro;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.VisualASA;
using SW10.SWMANAGER.ClassesAplicacao.Atendimentos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Pacientes
{
    public class PacienteAppService : SWMANAGERAppServiceBase, IPacienteAppService
    {
        #region
        private readonly IRepository<Paciente, long> _pacienteRepository;
        private readonly IRepository<Atendimento, long> _atendimentoRepository;
        private readonly IRepository<VWTeste, long> _vwTesteRepository;
        private readonly IListarPacientesExcelExporter _listarPacientesExcelExporter;
        private readonly IRepository<SisPessoa, long> _sisPessoa;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IVisualAppService _visualAppService;

        public PacienteAppService(
            IRepository<Paciente, long> pacienteRepository,
            IRepository<Atendimento, long> atendimentoRepository,
            IRepository<VWTeste, long> vwTesteRepository,
            IListarPacientesExcelExporter listarPacientesExcelExporter,
            IRepository<SisPessoa, long> sisPessoa,
            IUltimoIdAppService ultimoIdAppService,
            IVisualAppService visualAppService
            )
        {
            _pacienteRepository = pacienteRepository;
            _atendimentoRepository = atendimentoRepository;
            _listarPacientesExcelExporter = listarPacientesExcelExporter;
            _vwTesteRepository = vwTesteRepository;
            _sisPessoa = sisPessoa;
            _ultimoIdAppService = ultimoIdAppService;
            _visualAppService = visualAppService;
        }
        #endregion

        //public async Task<long> CriarOuEditar(PacienteDto input)
        //{
        //    try
        //    {
        //        //     var paciente = input.MapTo<Paciente>();

        //        var paciente = new Paciente();
        //        paciente.Bairro = input.Bairro;
        //        paciente.Cep = input.Cep;
        //        paciente.CidadeId = input.CidadeId;
        //        paciente.Cns = input.Cns;
        //        paciente.Codigo = input.Codigo;
        //        paciente.CodigoPaciente = input.CodigoPaciente;
        //        paciente.Complemento = input.Complemento;
        //        paciente.Cpf = input.Cpf;
        //        paciente.CreationTime = input.CreationTime;
        //        paciente.CreatorUserId = input.CreatorUserId;
        //        paciente.DeleterUserId = input.DeleterUserId;
        //        paciente.DeletionTime = input.DeletionTime;
        //        paciente.Descricao = input.Descricao;
        //        paciente.Email = input.Email;
        //        paciente.Emissao = input.Emissao;
        //        paciente.Emissor = input.Emissor;
        //        paciente.EstadoId = input.EstadoId;
        //        paciente.Id = input.Id;
        //        paciente.IsDeleted = input.IsDeleted;
        //        paciente.IsDoador = input.IsDoador;
        //        paciente.IsSistema = input.IsSistema;
        //        paciente.NacionalidadeId = input.NacionalidadeId;
        //        paciente.NomeCompleto = input.NomeCompleto;
        //        paciente.PaisId = input.PaisId;
        //        paciente.SexoId = input.SexoId;
        //        //     paciente.SisPessoaId      = input.SisPessoaId      ;
        //        paciente.TipoLogradouroId = input.TipoLogradouroId;
        //        paciente.TipoSanguineoId = input.TipoSanguineoId;



        //        // temp
        //        //   paciente.
        //        // fim - temp

        //        long IdPaciente = new long();
        //        if (input.Id.Equals(0))
        //        {
        //            //await _pacienteRepository.InsertAsync(paciente);
        //            //var IdEstoque = AsyncHelper.RunSync(() => EstoqueRepositorio.InsertAndGetIdAsync(paciente));
        //            paciente = CarregarDadosPaciente(input);
        //            IdPaciente = AsyncHelper.RunSync(() => _pacienteRepository.InsertAndGetIdAsync(paciente));
        //            //await _pacienteRepository.InsertAsync(paciente);
        //        }
        //        else
        //        {
        //            await _pacienteRepository.UpdateAsync(CarregarDadosPaciente(input));
        //            IdPaciente = paciente.Id;
        //        }

        //        return IdPaciente;
        //    }

        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroSalvar"), ex);
        //    }

        //}

        public async Task<long> CriarOuEditarOriginal(PacienteDto input)
        {
            try
            {
                var paciente = input.MapTo<Paciente>();
                long IdPaciente = new long();
                if (input.Id.Equals(0))
                {
                    //await _pacienteRepository.InsertAsync(paciente);
                    //var IdEstoque = AsyncHelper.RunSync(() => EstoqueRepositorio.InsertAndGetIdAsync(paciente));
                    paciente = CarregarDadosPaciente(input);
                    IdPaciente = AsyncHelper.RunSync(() => _pacienteRepository.InsertAndGetIdAsync(paciente));
                    //await _pacienteRepository.InsertAsync(paciente);
                }
                else
                {
                    await _pacienteRepository.UpdateAsync(CarregarDadosPaciente(input));
                    IdPaciente = paciente.Id;
                }

                return IdPaciente;
            }

            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task<long> CriarOuEditar(PacienteDto input)
        {
            try
            {
                var paciente = input.MapTo<Paciente>();
                long IdPaciente = new long();

                var pacienteNovo = new PacienteDto();

                pacienteNovo.Bairro = input.Bairro;
                pacienteNovo.Cep = input.Cep;
                pacienteNovo.CidadeId = input.CidadeId;
                pacienteNovo.Cns = input.Cns;
                pacienteNovo.Codigo = input.Codigo;
                pacienteNovo.CodigoPaciente = input.CodigoPaciente;
                pacienteNovo.Complemento = input.Complemento;
                pacienteNovo.Cpf = input.Cpf;
                pacienteNovo.CreationTime = input.CreationTime;
                pacienteNovo.CreatorUserId = input.CreatorUserId;
                pacienteNovo.DeleterUserId = input.DeleterUserId;
                pacienteNovo.DeletionTime = input.DeletionTime;
                pacienteNovo.Descricao = input.Descricao;
                pacienteNovo.Email = input.Email;
                pacienteNovo.Emissao = input.Emissao;
                pacienteNovo.Emissor = input.Emissor;
                pacienteNovo.EstadoId = input.EstadoId;
                pacienteNovo.Id = input.Id;
                pacienteNovo.IsDeleted = input.IsDeleted;
                pacienteNovo.IsDoador = input.IsDoador;
                pacienteNovo.IsSistema = input.IsSistema;
                pacienteNovo.NacionalidadeId = input.NacionalidadeId > 0 ? input.NacionalidadeId : null;
                pacienteNovo.NomeCompleto = input.NomeCompleto;
                pacienteNovo.Numero = input.Numero;
                pacienteNovo.PaisId = input.PaisId > 0 ? input.PaisId : null;
                pacienteNovo.SexoId = input.SexoId > 0 ? input.SexoId : null;
                pacienteNovo.TipoLogradouroId = input.TipoLogradouroId > 0 ? input.TipoLogradouroId : null;
                pacienteNovo.TipoSanguineoId = input.TipoSanguineoId > 0 ? input.TipoSanguineoId : null;

                if (input.Id.Equals(0))
                {
                    var pacienteComSisPessoa = CarregarDadosPaciente(pacienteNovo);

                    pacienteComSisPessoa.Prontuario = input.Prontuario;
                    pacienteComSisPessoa.Cpf = input.Cpf;
                    pacienteComSisPessoa.NomeCompleto = input.NomeCompleto;
                    pacienteComSisPessoa.Bairro = input.Bairro;
                    pacienteComSisPessoa.Cep = input.Cep;
                    pacienteComSisPessoa.CidadeId = input.CidadeId == 0 ? null : input.CidadeId;
                    pacienteComSisPessoa.Cns = input.Cns;
                    pacienteComSisPessoa.Codigo = input.Codigo;
                    pacienteComSisPessoa.CodigoPaciente = Convert.ToInt32(await _ultimoIdAppService.ObterProximoCodigo("Paciente")); // input.CodigoPaciente;
                    pacienteComSisPessoa.Complemento = input.Complemento;
                    pacienteComSisPessoa.Cpf = input.Cpf;
                    pacienteComSisPessoa.CreationTime = input.CreationTime;
                    pacienteComSisPessoa.CreatorUserId = input.CreatorUserId;
                    pacienteComSisPessoa.DeleterUserId = input.DeleterUserId;
                    pacienteComSisPessoa.DeletionTime = input.DeletionTime;
                    pacienteComSisPessoa.Descricao = input.Descricao;
                    pacienteComSisPessoa.Email = input.Email;
                    pacienteComSisPessoa.Emissao = input.Emissao;
                    pacienteComSisPessoa.Emissor = input.Emissor;
                    pacienteComSisPessoa.EstadoId = input.EstadoId == 0 ? null : input.EstadoId;
                    pacienteComSisPessoa.Id = input.Id;
                    pacienteComSisPessoa.IsDeleted = input.IsDeleted;
                    pacienteComSisPessoa.IsDoador = input.IsDoador;
                    pacienteComSisPessoa.IsSistema = input.IsSistema;
                    pacienteComSisPessoa.NacionalidadeId = input.NacionalidadeId == 0 ? null : input.NacionalidadeId;
                    pacienteComSisPessoa.NomeCompleto = input.NomeCompleto;
                    pacienteComSisPessoa.PaisId = input.PaisId == 0 ? null : input.PaisId;
                    pacienteComSisPessoa.SexoId = input.SexoId == 0 ? null : input.SexoId;
                    pacienteComSisPessoa.TipoLogradouroId = input.TipoLogradouroId == 0 ? null : input.TipoLogradouroId;
                    pacienteComSisPessoa.SisPessoa.TipoLogradouroId = input.TipoLogradouroId == 0 ? null : input.TipoLogradouroId;
                    pacienteComSisPessoa.TipoSanguineoId = input.TipoSanguineoId == 0 ? null : input.TipoSanguineoId;
                    pacienteComSisPessoa.Nascimento = input.Nascimento;
                    pacienteComSisPessoa.SisPessoa.Nascimento = input.Nascimento;
                    pacienteComSisPessoa.Numero = input.Numero;
                    pacienteComSisPessoa.Logradouro = input.Logradouro;
                    pacienteComSisPessoa.TipoLogradouro = input.TipoLogradouro.MapTo<TipoLogradouro>();
                    pacienteComSisPessoa.TipoLogradouroId = input.TipoLogradouroId;
                    pacienteComSisPessoa.Telefone1 = input.Telefone1;
                    pacienteComSisPessoa.Telefone2 = input.Telefone2;
                    pacienteComSisPessoa.Telefone3 = input.Telefone3;
                    pacienteComSisPessoa.Telefone4 = input.Telefone4;
                    pacienteComSisPessoa.TipoTelefone1Id = input.TipoTelefone1Id == 0 ? null : input.TipoTelefone1Id;
                    pacienteComSisPessoa.TipoTelefone2Id = input.TipoTelefone2Id == 0 ? null : input.TipoTelefone2Id;
                    pacienteComSisPessoa.TipoTelefone3Id = input.TipoTelefone3Id == 0 ? null : input.TipoTelefone3Id;
                    pacienteComSisPessoa.TipoTelefone4Id = input.TipoTelefone4Id == 0 ? null : input.TipoTelefone4Id;
                    pacienteComSisPessoa.Prontuario = input.Prontuario;
                    pacienteComSisPessoa.Cns = input.Cns;
                    pacienteComSisPessoa.ReligiaoId = input.ReligiaoId == 0 ? null : input.ReligiaoId;
                    pacienteComSisPessoa.CorPeleId = input.CorPeleId == 0 ? null : input.CorPeleId;
                    pacienteComSisPessoa.EstadoCivilId = input.EstadoCivilId == 0 ? null : input.EstadoCivilId;
                    pacienteComSisPessoa.NomeMae = input.NomeMae;
                    pacienteComSisPessoa.NomePai = input.NomePai;
                    pacienteComSisPessoa.Rg = input.Rg;
                    pacienteComSisPessoa.ProfissaoId = input.ProfissaoId == 0 ? null : input.ProfissaoId;
                    pacienteComSisPessoa.EscolaridadeId = input.EscolaridadeId == 0 ? null : input.EscolaridadeId;
                    pacienteComSisPessoa.NaturalidadeId = input.NaturalidadeId == 0 ? null : input.NaturalidadeId;
                    pacienteComSisPessoa.Observacao = input.Observacao;
                    paciente = pacienteComSisPessoa;

                    IdPaciente = AsyncHelper.RunSync(() => _pacienteRepository.InsertAndGetIdAsync(paciente));
                }
                else
                {
                    var pacienteComSisPessoa = CarregarDadosPaciente(pacienteNovo);

                    pacienteComSisPessoa.Prontuario = input.Prontuario;
                    pacienteComSisPessoa.Cpf = input.Cpf;
                    pacienteComSisPessoa.NomeCompleto = input.NomeCompleto;
                    pacienteComSisPessoa.Bairro = input.Bairro;
                    pacienteComSisPessoa.Cep = input.Cep;
                    pacienteComSisPessoa.CidadeId = input.CidadeId == 0 ? null : input.CidadeId;
                    pacienteComSisPessoa.Cns = input.Cns;
                    pacienteComSisPessoa.Codigo = input.Codigo;
                    if (pacienteComSisPessoa.CodigoPaciente.Equals(0))
                    {
                        pacienteComSisPessoa.CodigoPaciente = Convert.ToInt32(await _ultimoIdAppService.ObterProximoCodigo("Paciente")); // input.CodigoPaciente;
                    }
                    pacienteComSisPessoa.Complemento = input.Complemento;
                    pacienteComSisPessoa.Cpf = input.Cpf;
                    pacienteComSisPessoa.CreationTime = input.CreationTime;
                    pacienteComSisPessoa.CreatorUserId = input.CreatorUserId;
                    pacienteComSisPessoa.DeleterUserId = input.DeleterUserId;
                    pacienteComSisPessoa.DeletionTime = input.DeletionTime;
                    pacienteComSisPessoa.Descricao = input.Descricao;
                    pacienteComSisPessoa.Email = input.Email;
                    pacienteComSisPessoa.Emissao = input.Emissao;
                    pacienteComSisPessoa.Emissor = input.Emissor;
                    pacienteComSisPessoa.EstadoId = input.EstadoId == 0 ? null : input.EstadoId;
                    pacienteComSisPessoa.Id = input.Id;
                    pacienteComSisPessoa.IsDeleted = input.IsDeleted;
                    pacienteComSisPessoa.IsDoador = input.IsDoador;
                    pacienteComSisPessoa.IsSistema = input.IsSistema;
                    pacienteComSisPessoa.NacionalidadeId = input.NacionalidadeId == 0 ? null : input.NacionalidadeId;
                    pacienteComSisPessoa.NomeCompleto = input.NomeCompleto;
                    pacienteComSisPessoa.PaisId = input.PaisId == 0 ? null : input.PaisId;
                    pacienteComSisPessoa.SexoId = input.SexoId == 0 ? null : input.SexoId;
                    pacienteComSisPessoa.TipoLogradouroId = input.TipoLogradouroId == 0 ? null : input.TipoLogradouroId;
                    pacienteComSisPessoa.TipoSanguineoId = input.TipoSanguineoId == 0 ? null : input.TipoSanguineoId;
                    pacienteComSisPessoa.Nascimento = input.Nascimento;
                    pacienteComSisPessoa.Numero = input.Numero;
                    pacienteComSisPessoa.Logradouro = input.Logradouro;
                    pacienteComSisPessoa.Telefone1 = input.Telefone1;
                    pacienteComSisPessoa.Telefone2 = input.Telefone2;
                    pacienteComSisPessoa.Telefone3 = input.Telefone3;
                    pacienteComSisPessoa.Telefone4 = input.Telefone4;
                    pacienteComSisPessoa.TipoTelefone1Id = input.TipoTelefone1Id == 0 ? null : input.TipoTelefone1Id;
                    pacienteComSisPessoa.TipoTelefone2Id = input.TipoTelefone2Id == 0 ? null : input.TipoTelefone2Id;
                    pacienteComSisPessoa.TipoTelefone3Id = input.TipoTelefone3Id == 0 ? null : input.TipoTelefone3Id;
                    pacienteComSisPessoa.TipoTelefone4Id = input.TipoTelefone4Id == 0 ? null : input.TipoTelefone4Id;
                    pacienteComSisPessoa.Prontuario = input.Prontuario;
                    pacienteComSisPessoa.Cns = input.Cns;
                    pacienteComSisPessoa.ReligiaoId = input.ReligiaoId == 0 ? null : input.ReligiaoId;
                    pacienteComSisPessoa.CorPeleId = input.CorPeleId == 0 ? null : input.CorPeleId;
                    pacienteComSisPessoa.EstadoCivilId = input.EstadoCivilId == 0 ? null : input.EstadoCivilId;
                    pacienteComSisPessoa.NomeMae = input.NomeMae;
                    pacienteComSisPessoa.NomePai = input.NomePai;
                    pacienteComSisPessoa.Rg = input.Rg;
                    pacienteComSisPessoa.ProfissaoId = input.ProfissaoId == 0 ? null : input.ProfissaoId;
                    pacienteComSisPessoa.EscolaridadeId = input.EscolaridadeId == 0 ? null : input.EscolaridadeId;
                    pacienteComSisPessoa.NaturalidadeId = input.NaturalidadeId == 0 ? null : input.NaturalidadeId;
                    pacienteComSisPessoa.Observacao = input.Observacao;
                    paciente = pacienteComSisPessoa;

                    await _pacienteRepository.UpdateAsync(paciente);


                    IdPaciente = paciente.Id;
                }

                _visualAppService.MigrarSisPessoa(paciente.SisPessoaId.Value);
                return IdPaciente;
            }

            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task Excluir(PacienteDto input)
        {
            try
            {
                await _pacienteRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public class ListarDropdownInput
        {
            public string search { get; set; }
            public string page { get; set; }
        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<PacienteDto> pacientesDtos = new List<PacienteDto>();
            // DateTime dt = new DateTime(p.Nascimento);
            try
            {
                //get com filtro
                var query = from p in _pacienteRepository.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                        m.CodigoPaciente.ToString().ToLower().Contains(dropdownInput.search.ToLower()) ||
                        m.SisPessoa.NomeCompleto.ToLower()

                        //Retirado, pois estava gerando muita lentidão

                        //.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u")
                        //.Replace("à", "a").Replace("è", "e").Replace("ì", "i").Replace("ò", "o").Replace("ù", "u")
                        //.Replace("â", "a").Replace("ê", "e").Replace("î", "i").Replace("ô", "o").Replace("û", "u")
                        //.Replace("ã", "a").Replace("õ", "o")
                        //.Replace("Á", "A").Replace("É", "E").Replace("Í", "I").Replace("Ó", "O").Replace("Ú", "U")
                        //.Replace("À", "A").Replace("È", "E").Replace("Ì", "I").Replace("Ô", "O").Replace("Ù", "U")
                        //.Replace("Â", "A").Replace("Ê", "E").Replace("Î", "I").Replace("Õ", "O").Replace("Û", "U")
                        //.Replace("Ã", "A").Replace("Õ", "O")
                        .Contains(dropdownInput.search.ToLower())
                        )
                            orderby p.NomeCompleto ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.CodigoPaciente.ToString(), " - ", p.NomeCompleto, " - ", p.Nascimento.ToString().Replace(p.Nascimento.ToString(), string.Concat(((DateTime)p.Nascimento).Day.ToString() + "/" + ((DateTime)p.Nascimento).Month.ToString() + "/" + ((DateTime)p.Nascimento).Year.ToString())))
                            };

                //paginação 
                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarPacientesEmAtendimentoDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);

            var tipoAmb = dropdownInput.filtros[0];
            var tipoInt = dropdownInput.filtros[1];
            if (tipoInt.IsNullOrWhiteSpace() && tipoAmb.IsNullOrWhiteSpace())
            {
                throw new Exception("Informar o tipo de atendimento");
            }
            var isInternacao = tipoInt.ToLower() == "true" ? true : false;
            var isAmbulatorio = tipoAmb.ToLower() == "true" ? true : false;
            List<PacienteDto> pacientesDtos = new List<PacienteDto>();
            // DateTime dt = new DateTime(p.Nascimento);
            try
            {
                var query = from ate in _atendimentoRepository
                                .GetAll()
                                //.Include(m => m.Paciente)
                                //.Include(m => m.Paciente.SisPessoa)
                                .WhereIf(isInternacao, m => m.IsInternacao)
                                .WhereIf(isAmbulatorio, m => m.IsAmbulatorioEmergencia)
                                .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m =>
                                    m.Paciente.CodigoPaciente.ToString().ToLower().Contains(dropdownInput.search.ToLower()) ||
                                    m.Paciente.SisPessoa.NomeCompleto.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                    m.Codigo.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                    m.DataRegistro.ToString().Contains(dropdownInput.search.ToLower())
                                )
                                .Where(m => !m.DataAlta.HasValue && m.AtendimentoMotivoCancelamentoId == null)
                            orderby ate.Paciente.NomeCompleto ascending
                            select new DropdownItems
                            {
                                id = ate.Id,
                                text = string.Concat(
                                   ate.Codigo, " - ",
                                   ate.DataRegistro.ToString().Replace(ate.DataRegistro.ToString(), string.Concat(((DateTime)ate.DataRegistro).Day.ToString() + "/" + ((DateTime)ate.DataRegistro).Month.ToString() + "/" + ((DateTime)ate.DataRegistro).Year.ToString())), " Pac.: ",
                                   ate.Paciente.CodigoPaciente.ToString(), " - ",
                                   ate.Paciente.NomeCompleto, " - ",
                                   ate.Paciente.Nascimento.ToString().Replace(ate.Paciente.Nascimento.ToString(), string.Concat(((DateTime)ate.Paciente.Nascimento).Day.ToString() + "/" + ((DateTime)ate.Paciente.Nascimento).Month.ToString() + "/" + ((DateTime)ate.Paciente.Nascimento).Year.ToString()))
                                   )
                            };

                int total = await query.CountAsync();
                //paginação 
                var queryResultPage = query
                    .OrderBy(o => o.text)
                    .Skip(numberOfObjectsPerPage * pageInt)
                    .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<PacienteDto>> Listar(ListarPacientesInput input)
        {
            var contarPacientes = 0;
            List<Paciente> pacientes;
            List<PacienteDto> pacientesDtos = new List<PacienteDto>();
            try
            {
                var query = _pacienteRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Rg.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Nascimento.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.NomeMae.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.NomePai.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Logradouro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Numero.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Complemento.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Bairro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Estado.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Estado.Uf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Pais.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Pais.Sigla.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cep.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Observacao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Prontuario.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Cns.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Email.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Indicacao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone1.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone2.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone3.ToUpper().Contains(input.Filtro.ToUpper()) ||
                        m.Telefone4.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarPacientes = await query
                    .CountAsync();

                pacientes = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                pacientesDtos = pacientes
                    .MapTo<List<PacienteDto>>();

                return new PagedResultDto<PacienteDto>(
                contarPacientes,
                pacientesDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<PacienteDto>> ListarTodos()
        {
            try
            {
                var pacientes = await _pacienteRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .AsNoTracking()
                    .ToListAsync();

                var pacientesDtos = pacientes
                    .MapTo<List<PacienteDto>>();

                return new ListResultDto<PacienteDto> { Items = pacientesDtos };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<List<Paciente>> ListarAutoComplete(string term)
        {
            try
            {
                return await _pacienteRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .WhereIf(!term.IsNullOrEmpty(), m =>
                    m.NomeCompleto.ToUpper().Contains(term.ToUpper()) ||
                    m.Rg.ToUpper().Contains(term.ToUpper()) ||
                    m.Cpf.ToUpper().Contains(term.ToUpper()) ||
                    m.Nascimento.ToString().ToUpper().Contains(term.ToUpper()) ||
                    m.NomeMae.ToUpper().Contains(term.ToUpper()) ||
                    m.NomePai.ToUpper().Contains(term.ToUpper()) ||
                    m.Logradouro.ToUpper().Contains(term.ToUpper()) ||
                    m.Numero.ToUpper().Contains(term.ToUpper()) ||
                    m.Complemento.ToUpper().Contains(term.ToUpper()) ||
                    m.Bairro.ToUpper().Contains(term.ToUpper()) ||
                    m.Cidade.Nome.ToUpper().Contains(term.ToUpper()) ||
                    m.Estado.Nome.ToUpper().Contains(term.ToUpper()) ||
                    m.Estado.Uf.ToUpper().Contains(term.ToUpper()) ||
                    m.Pais.Nome.ToUpper().Contains(term.ToUpper()) ||
                    m.Pais.Sigla.ToUpper().Contains(term.ToUpper()) ||
                    //m.PacientePlanos.FirstOrDefault().Plano.Descricao.ToUpper().Contains(term.ToUpper()) ||
                    //m.PacienteConvenios.FirstOrDefault().Convenio.NomeFantasia.ToUpper().Contains(term.ToUpper()) ||
                    m.Cep.ToUpper().Contains(term.ToUpper()) ||
                    m.Observacao.ToUpper().Contains(term.ToUpper()) ||
                    // m.Pendencia.ToUpper().Contains(term.ToUpper()) ||
                    m.Prontuario.ToString().ToUpper().Contains(term.ToUpper()) ||
                    m.Cns.ToString().ToUpper().Contains(term.ToUpper()) ||
                    m.Email.ToUpper().Contains(term.ToUpper()) ||
                    m.Indicacao.ToUpper().Contains(term.ToUpper()) ||
                    m.Telefone1.ToUpper().Contains(term.ToUpper()) ||
                    m.Telefone2.ToUpper().Contains(term.ToUpper()) ||
                    m.Telefone3.ToUpper().Contains(term.ToUpper()) ||
                    m.Telefone4.ToUpper().Contains(term.ToUpper())
                    )
                    //.AsNoTracking()
                    .ToListAsync();

                //var pacientesDtos = pacientes
                //    .MapTo<List<PacienteDto>>();

                //return new ListResultDto<PacienteDto> { Items = pacientesDtos };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ListarPacientesIndex>> ListarParaIndex(ListarPacientesInput input)
        {
            var contarPacientes = 0;
            List<Paciente> pacientes;
            List<ListarPacientesIndex> pacientesDtos = new List<ListarPacientesIndex>();
            try
            {
                var query = _pacienteRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                    m.NomeCompleto.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Rg.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cpf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Nascimento.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.NomeMae.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.NomePai.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Logradouro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Numero.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Complemento.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Bairro.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cidade.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Estado.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Estado.Uf.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Pais.Nome.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Pais.Sigla.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cep.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    //m.PacientePlanos.FirstOrDefault().Plano.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    //m.PacienteConvenios.FirstOrDefault().Convenio.NomeFantasia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Observacao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    // m.Pendencia.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Prontuario.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Cns.ToString().ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Email.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Indicacao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone1.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone2.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone3.ToUpper().Contains(input.Filtro.ToUpper()) ||
                    m.Telefone4.ToUpper().Contains(input.Filtro.ToUpper())
                    );

                contarPacientes = await query
                    .CountAsync();

                pacientes = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                // pacientesDtos = pacientes
                //     .MapTo<List<ListarPacientesIndex>>();


                foreach (var item in pacientes)
                {
                    ListarPacientesIndex listarPacientesIndex = new ListarPacientesIndex();
                    listarPacientesIndex.Id = Convert.ToInt64(item.Id);
                    listarPacientesIndex.NomeCompleto = item.NomeCompleto;
                    listarPacientesIndex.Identidade = item.Rg;
                    listarPacientesIndex.Cpf = item.Cpf;
                    listarPacientesIndex.Nascimento = item.Nascimento; //item.SisPessoa.Nascimento.HasValue ? item.SisPessoa.Nascimento : null;
                    listarPacientesIndex.Telefone = item.Telefone1;
                    listarPacientesIndex.NomeMae = item.NomeMae;
                    listarPacientesIndex.NomePai = item.NomePai;

                    pacientesDtos.Add(listarPacientesIndex);
                }

                return new PagedResultDto<ListarPacientesIndex>(
                contarPacientes,
                pacientesDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        // temp
        //public async Task<PagedResultDto<ListarPacientesIndex>> ListarParaIndex()
        //{
        //    var contarPacientes = 0;
        //    List<Paciente> pacientes;
        //    List<ListarPacientesIndex> pacientesDtos = new List<ListarPacientesIndex>();
        //    try
        //    {
        //        var query = _pacienteRepository
        //            .GetAll()
        //            ;

        //        contarPacientes = await query
        //            .CountAsync();

        //        pacientes = await query
        //            .AsNoTracking()
        //            //.OrderBy(input.Sorting)
        //            //.PageBy(input)
        //            .ToListAsync();

        //        pacientesDtos = pacientes
        //            .MapTo<List<ListarPacientesIndex>>();

        //        return new PagedResultDto<ListarPacientesIndex>(
        //        contarPacientes,
        //        pacientesDtos
        //        );
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("ErroPesquisar"), ex);
        //    }
        //}

        public async Task<FileDto> ListarParaExcel(ListarPacientesInput input)
        {
            try
            {
                var result = await Listar(input);
                var pacientes = result.Items;
                return _listarPacientesExcelExporter.ExportToFile(pacientes.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [Obsolete]
        public async Task<PacienteDto> Obter(long id)
        {
            try
            {
                var query = await _pacienteRepository
                    .GetAll()
                    .Include(m => m.SisPessoa)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.Sexo)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Pais))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Estado))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Cidade))
                     .Include(m => m.SisPessoa.Enderecos.Select(s => s.Pais))
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                PacienteDto pacienteDto = new PacienteDto();

                pacienteDto.SisPessoa = new Pessoas.Dto.SisPessoaDto();
                pacienteDto.Id = query.Id;
                pacienteDto.SisPessoa.NomeCompleto = query.NomeCompleto;
                pacienteDto.SisPessoa.Email = query.Email;
                pacienteDto.Sexo = query.Sexo.MapTo<SexoDto>();
                pacienteDto.SisPessoa.SexoId = query.SexoId;

                if (query.Nascimento.HasValue)
                {
                    pacienteDto.SisPessoa.Nascimento = (DateTime)query.Nascimento;
                }

                pacienteDto.Cep = query.Cep;
                pacienteDto.TipoLogradouroId = query.TipoLogradouroId;
                pacienteDto.Logradouro = query.Logradouro;
                pacienteDto.Numero = query.Numero;
                pacienteDto.Complemento = query.Complemento;
                pacienteDto.Pais = query.Pais.MapTo<PaisDto>();
                pacienteDto.PaisId = query.PaisId;
                pacienteDto.Estado = query.Estado.MapTo<EstadoDto>();
                pacienteDto.EstadoId = query.EstadoId;
                pacienteDto.Cidade = query.Cidade.MapTo<CidadeDto>();
                pacienteDto.CidadeId = query.CidadeId;
                pacienteDto.Bairro = query.Bairro;
                pacienteDto.SisPessoa.Email = query.Email;
                pacienteDto.SisPessoa.Codigo = query.Codigo;
                pacienteDto.Codigo = query.Codigo;
                pacienteDto.CodigoPaciente = query.CodigoPaciente;
                if (query.TipoTelefone1 != null)
                {
                    pacienteDto.TipoTelefone1Id = Convert.ToInt32(query.TipoTelefone1.Id);
                }

                if (query.TipoTelefone2 != null)
                {
                    pacienteDto.TipoTelefone2Id = Convert.ToInt32(query.TipoTelefone2.Id);
                }

                if (query.TipoTelefone3 != null)
                {
                    pacienteDto.TipoTelefone3Id = Convert.ToInt32(query.TipoTelefone3.Id);
                }

                if (query.TipoTelefone4 != null)
                {
                    pacienteDto.TipoTelefone4Id = Convert.ToInt32(query.TipoTelefone4.Id);
                }

                pacienteDto.Telefone1 = query.Telefone1;
                pacienteDto.Telefone2 = query.Telefone2;
                pacienteDto.Telefone3 = query.Telefone3;
                pacienteDto.Telefone4 = query.Telefone4;
                pacienteDto.Prontuario = query.Prontuario;
                pacienteDto.Cns = query.Cns;
                pacienteDto.TipoSanguineoId = query.TipoSanguineoId;

                if (query.Religiao != null)
                {
                    pacienteDto.ReligiaoId = query.Religiao.Id;
                }

                if (query.Pais != null)
                {
                    pacienteDto.PaisId = query.Pais.Id;
                }


                if (query.CorPele != null)
                {
                    pacienteDto.CorPeleId = query.CorPele.Id;
                }

                if (query.EstadoCivil != null)
                {
                    pacienteDto.EstadoCivilId = query.EstadoCivil.Id;
                }

                pacienteDto.Bairro = query.Bairro;
                pacienteDto.NomeMae = query.NomeMae;
                pacienteDto.NomePai = query.NomePai;
                pacienteDto.Rg = query.Rg;
                pacienteDto.Emissor = query.Emissor;
                pacienteDto.Emissao = query.Emissao;
                pacienteDto.Cpf = query.Cpf;
                pacienteDto.ProfissaoId = query.ProfissaoId;
                //criarOuEditarPaciente.EscolaridadeId = query.Escolaridade;
                pacienteDto.NaturalidadeId = query.NaturalidadeId;
                pacienteDto.Nacionalidade = query.Nacionalidade.MapTo<NacionalidadeDto>();
                pacienteDto.NacionalidadeId = query.NacionalidadeId;
                pacienteDto.Observacao = query.Observacao;

                // var paciente = query.MapTo<CriarOuEditarPaciente>();
                return pacienteDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PacienteDto> Obter2(long id)
        {
            try
            {
                var query = await _pacienteRepository
                    .GetAll()
                    .Include(m => m.SisPessoa)
                    .Include(m => m.CorPele)
                    .Include(m => m.EstadoCivil)
                    .Include(m => m.Escolaridade)
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.Religiao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.TipoTelefone1)
                    .Include(m => m.TipoTelefone2)
                    .Include(m => m.TipoTelefone3)
                    .Include(m => m.TipoTelefone4)
                    .Include(m => m.Sexo)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Pais))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Estado))
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.Cidade))
                     .Include(m => m.SisPessoa.Enderecos.Select(s => s.Pais))
                    .Where(m => m.Id == id)
                    .FirstOrDefaultAsync();

                var paciente = query.MapTo<PacienteDto>();

                return paciente;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PacienteDto> ObterNoMap(long id)
        {
            try
            {
                var result = await _pacienteRepository
                    .GetAsync(id);
                //.Where(m => m.Id.Equals(id));

                //var result = await query
                //    .FirstOrDefaultAsync();

                //var paciente = result
                //    .MapTo<CriarOuEditarPaciente>();
                var pacienteDto = new PacienteDto
                {
                    Bairro = result.Bairro,
                    Cep = result.Cep,
                    CidadeId = result.CidadeId,
                    Cns = result.Cns,
                    Complemento = result.Complemento,
                    //  ConvenioId = result.ConvenioId,
                    // CorPele = result.CorPele,
                    Cpf = result.Cpf,
                    CreationTime = result.CreationTime,
                    CreatorUserId = result.CreatorUserId,
                    DeleterUserId = result.DeleterUserId,
                    DeletionTime = result.DeletionTime,
                    Email = result.Email,
                    Emissao = result.Emissao,
                    Emissor = result.Emissor,
                    Logradouro = result.Logradouro,
                    //Escolaridade = result.Escolaridade,
                    //EstadoCivil = result.EstadoCivil,
                    EstadoId = result.EstadoId,
                    Foto = result.Foto,
                    FotoMimeType = result.FotoMimeType,
                    Id = result.Id,
                    Indicacao = result.Indicacao,
                    IsDeleted = result.IsDeleted,
                    IsSistema = result.IsSistema,
                    LastModificationTime = result.LastModificationTime,
                    LastModifierUserId = result.LastModifierUserId,
                    TipoSanguineoId = result.TipoSanguineoId,
                    Nascimento = (DateTime)result.Nascimento,
                    NaturalidadeId = result.NaturalidadeId,
                    NacionalidadeId = result.NacionalidadeId,
                    NomeCompleto = result.NomeCompleto,
                    NomeMae = result.NomeMae,
                    NomePai = result.NomePai,
                    Numero = result.Numero,
                    Observacao = result.Observacao,
                    IsDoador = result.IsDoador,
                    PaisId = result.PaisId,
                    //  Pendencia = result.Pendencia,
                    //  PlanoId = result.PlanoId,
                    ProfissaoId = result.ProfissaoId,
                    Prontuario = result.Prontuario,
                    // Religiao = result.Religiao,
                    Rg = result.Rg,
                    SexoId = result.SexoId,
                    Telefone1 = result.Telefone1,
                    Telefone2 = result.Telefone2,
                    Telefone3 = result.Telefone3,
                    Telefone4 = result.Telefone4,
                    //TipoTelefone1 = result.TipoTelefone1,
                    //TipoTelefone2 = result.TipoTelefone2,
                    //TipoTelefone3 = result.TipoTelefone3,
                    //TipoTelefone4 = result.TipoTelefone4
                };
                return pacienteDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ListResultDto<VWTesteDto>> ListarResumo()
        {
            try
            {
                var query = await _vwTesteRepository.GetAllListAsync();
                var result = query.MapTo<List<VWTesteDto>>();
                return new ListResultDto<VWTesteDto> { Items = result };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("ErroConsultar");
            }
        }

        public async Task<PacienteDto> ObterPorCpf(string cpf)
        {
            try
            {
                var query = await _pacienteRepository
                    .GetAll()
                    .Include(m => m.Cidade)
                    .Include(m => m.Estado)
                    .Include(m => m.Nacionalidade)
                    .Include(m => m.Naturalidade)
                    .Include(m => m.Pais)
                    .Include(m => m.Profissao)
                    .Include(m => m.TipoLogradouro)
                    .Include(m => m.TipoSanguineo)
                    .Include(m => m.SisPessoa)
                    .Include(m => m.SisPessoa.Enderecos)
                    .Include(m => m.SisPessoa.Enderecos.Select(s => s.TipoLogradouro))
                    .Where(m => m.Cpf == cpf)
                    .FirstOrDefaultAsync();

                var paciente = query.MapTo<PacienteDto>();

                return paciente;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        Paciente CarregarDadosPaciente(PacienteDto input)
        {
            Paciente paciente = null;
            var endereco = new Endereco();

            if (input.Id == 0)
            {
                paciente = new Paciente();
            }
            else
            {
                var pacienteAtual = _pacienteRepository.GetAll()
                                                .Where(w => w.Id == input.Id)
                                                .Include(i => i.SisPessoa)
                                                .Include(i => i.SisPessoa.Enderecos)
                                                .FirstOrDefault();

                paciente = pacienteAtual ?? new Paciente();
            }

            paciente.Id = input.Id;
            paciente.Codigo = input.Codigo;
            paciente.CodigoPaciente = input.CodigoPaciente;
            paciente.CorPeleId = input.CorPeleId;
            paciente.Foto = input.Foto;
            paciente.FotoMimeType = input.FotoMimeType;
            paciente.Indicacao = input.Indicacao;
            paciente.IsDoador = input.IsDoador;
            paciente.ProfissaoId = input.ProfissaoId;
            paciente.Prontuario = input.Prontuario;
            paciente.ReligiaoId = input.ReligiaoId;
            paciente.SexoId = input.SexoId;
            paciente.TipoSanguineoId = input.TipoSanguineoId;
            paciente.Nascimento = input.Nascimento;
            paciente.Cpf = input.Cpf;
            paciente.Cns = input.Cns;
            paciente.Observacao = input.Observacao;



            SisPessoa sisPessoa = paciente.SisPessoa;

            if (!string.IsNullOrEmpty(input.Cpf) && paciente.SisPessoa == null)
            {
                sisPessoa = _sisPessoa.GetAll()
                                               .Where(w => w.Cpf == input.Cpf)
                                               .FirstOrDefault();

                paciente.SisPessoa = sisPessoa;
            }



            if (sisPessoa != null)
            {
                paciente.SisPessoaId = sisPessoa.Id;

                if (sisPessoa.Enderecos != null && sisPessoa.Enderecos.Count() > 0)
                {
                    CarregarEndereco(input, sisPessoa.Enderecos[0]);
                }
                else
                {
                    endereco = new Endereco();
                    CarregarEndereco(input, endereco);

                    sisPessoa.Enderecos = new List<Endereco> { endereco };
                }
                //  convenio.SisPessoa = sisPessoa;
            }
            else
            {
                sisPessoa = new SisPessoa();
                paciente.SisPessoa = sisPessoa;

                endereco = new Endereco();
                sisPessoa.Enderecos = new List<Endereco>();

                CarregarEndereco(input, endereco);
                sisPessoa.Enderecos.Add(endereco);

            }


            sisPessoa.NomeCompleto = input.NomeCompleto;
            sisPessoa.Cpf = input.Cpf;
            sisPessoa.EscolaridadeId = input.EscolaridadeId;
            sisPessoa.NacionalidadeId = input.NacionalidadeId;
            sisPessoa.Nascimento = input.Nascimento;
            sisPessoa.NomeMae = input.NomeMae;
            sisPessoa.NomePai = input.NomePai;
            sisPessoa.ProfissaoId = input.ProfissaoId;
            sisPessoa.Rg = input.Rg;
            sisPessoa.EmissaoRg = input.Emissao;
            sisPessoa.Email = input.Email;
            sisPessoa.TipoPessoaId = 6;
            sisPessoa.FisicaJuridica = "F";
            sisPessoa.IsCredito = true;
            sisPessoa.Telefone1 = input.Telefone1;
            sisPessoa.Telefone2 = input.Telefone2;
            sisPessoa.Telefone3 = input.Telefone3;
            sisPessoa.Telefone4 = input.Telefone4;
            sisPessoa.TipoLogradouroId = input.TipoLogradouroId;
            sisPessoa.TipoTelefone1Id = input.TipoTelefone1Id;
            sisPessoa.TipoTelefone2Id = input.TipoTelefone2Id;
            sisPessoa.TipoTelefone3Id = input.TipoTelefone3Id;
            sisPessoa.TipoTelefone4Id = input.TipoTelefone4Id;
            sisPessoa.SexoId = input.SexoId;
            sisPessoa.ReligiaoId = input.ReligiaoId;
            sisPessoa.EstadoCivilId = input.EstadoCivilId;
            sisPessoa.NaturalidadeId = input.NaturalidadeId;

            sisPessoa.Enderecos = new List<Endereco>();
            CarregarEndereco(input, endereco);
            sisPessoa.Enderecos.Add(endereco);

            return paciente;
        }

        //Paciente CarregarDadosPaciente(CriarOuEditarPaciente input)
        //{
        //    Paciente paciente = null;
        //    var endereco = new Endereco();

        //    if (input.Id == 0)
        //    {
        //        paciente = new Paciente();
        //    }
        //    else
        //    {
        //        var pacienteAtual = _pacienteRepository.GetAll()
        //                                        .Where(w => w.Id == input.Id)
        //                                        .Include(i => i.SisPessoa)
        //                                        .Include(i => i.SisPessoa.Enderecos)
        //                                        .FirstOrDefault();

        //        paciente = pacienteAtual ?? new Paciente();
        //    }

        //    paciente.Id = input.Id;
        //    paciente.Codigo = input.Codigo;
        //    paciente.CodigoPaciente = input.CodigoPaciente;
        //    paciente.CorPeleId = input.CorPeleId;
        //    paciente.Foto = input.Foto;
        //    paciente.FotoMimeType = input.FotoMimeType;
        //    paciente.Indicacao = input.Indicacao;
        //    paciente.IsDoador = input.IsDoador;
        //    paciente.ProfissaoId = input.ProfissaoId;
        //    paciente.Prontuario = input.Prontuario;
        //    if (input.ReligiaoId != null)
        //    {
        //        paciente.ReligiaoId = Convert.ToInt32(input.ReligiaoId);
        //    }

        //    paciente.SexoId = input.SexoId;
        //    paciente.TipoSanguineoId = input.TipoSanguineoId;
        //    paciente.Nascimento = input.Nascimento;
        //    paciente.Cpf = input.Cpf;
        //    paciente.Cns = input.Cns;
        //    paciente.Observacao = input.Observacao;



        //    SisPessoa sisPessoa = paciente.SisPessoa;

        //    if (paciente.SisPessoa == null)
        //    {
        //        sisPessoa = _sisPessoa.GetAll()
        //                                       .Where(w => w.Cpf == input.Cpf)
        //                                       .FirstOrDefault();

        //        paciente.SisPessoa = sisPessoa;
        //    }



        //    if (sisPessoa != null)
        //    {
        //        paciente.SisPessoaId = sisPessoa.Id;

        //        if (sisPessoa.Enderecos != null && sisPessoa.Enderecos.Count() > 0)
        //        {
        //            CarregarEndereco(input, sisPessoa.Enderecos[0]);
        //        }
        //        else
        //        {
        //             endereco = new Endereco();
        //            CarregarEndereco(input, endereco);

        //            sisPessoa.Enderecos = new List<Endereco> { endereco };
        //        }
        //        //  convenio.SisPessoa = sisPessoa;
        //    }
        //    else
        //    {
        //        sisPessoa = new SisPessoa();
        //        paciente.SisPessoa = sisPessoa;
        //        //sisPessoa.NomeCompleto = input.NomeCompleto; 
        //        //sisPessoa.Cpf = input.Cpf;
        //        //sisPessoa.Escolaridade = input.Escolaridade;
        //        //sisPessoa.NacionalidadeId = input.NacionalidadeId;
        //        //sisPessoa.Nascimento = input.Nascimento;
        //        //sisPessoa.NomeMae = input.NomeMae;
        //        //sisPessoa.NomePai = input.NomePai;
        //        //sisPessoa.ProfissaoId = input.ProfissaoId;
        //        //sisPessoa.Rg = input.Rg;
        //        //// sisPessoa.Nascimento = DateTime.Now;
        //        //sisPessoa.TipoPessoaId = 2;
        //        //sisPessoa.FisicaJuridica = "F";

        //        endereco = new Endereco();
        //        sisPessoa.Enderecos = new List<Endereco>();

        //        CarregarEndereco(input, endereco);
        //        sisPessoa.Enderecos.Add(endereco);

        //    }




        //    sisPessoa.NomeCompleto = input.NomeCompleto;
        //    sisPessoa.Cpf = input.Cpf;
        //    if (input.Escolaridade != null)
        //    {
        //        sisPessoa.Escolaridade.Id = input.Escolaridade.Id;
        //    }

        //    sisPessoa.NacionalidadeId = input.NacionalidadeId;
        //    sisPessoa.Nascimento = input.Nascimento;
        //    sisPessoa.NomeMae = input.NomeMae;
        //    sisPessoa.NomePai = input.NomePai;
        //    sisPessoa.ProfissaoId = input.ProfissaoId;
        //    sisPessoa.Rg = input.Rg;
        //    sisPessoa.EmissaoRg = input.Emissao;
        //    sisPessoa.Email= input.Email;
        //    sisPessoa.TipoPessoaId = 2;
        //    sisPessoa.FisicaJuridica = "F";
        //    sisPessoa.IsCredito = true;
        //    sisPessoa.Telefone1 = input.Telefone1;
        //    sisPessoa.Telefone2 = input.Telefone2;
        //    sisPessoa.Telefone3 = input.Telefone3;
        //    sisPessoa.Telefone4 = input.Telefone4;
        //    sisPessoa.TipoTelefone1Id = input.TipoTelefone1Id;
        //    sisPessoa.TipoTelefone2Id = input.TipoTelefone2Id;
        //    sisPessoa.TipoTelefone3Id = input.TipoTelefone3Id;
        //    sisPessoa.TipoTelefone4Id = input.TipoTelefone4Id;
        //    sisPessoa.SexoId = input.SexoId;
        //    sisPessoa.ReligiaoId = input.ReligiaoId;
        //    sisPessoa.EstadoCivilId = input.EstadoCivilId;
        //    sisPessoa.NaturalidadeId = input.NaturalidadeId;

        //    sisPessoa.Enderecos = new List<Endereco>();
        //    CarregarEndereco(input, endereco);
        //    sisPessoa.Enderecos.Add(endereco);
        //    //sisPessoa.Enderecos[0].TipoLogradouroId = input.TipoLogradouroId;
        //    return paciente;
        //}

        void CarregarEndereco(PacienteDto input, Endereco endereco)
        {
            endereco.Bairro = input.Bairro;
            endereco.Cep = input.Cep;
            endereco.CidadeId = input.CidadeId;
            endereco.Complemento = input.Complemento;
            endereco.EstadoId = input.EstadoId;
            endereco.Logradouro = input.Logradouro;
            endereco.Numero = input.Numero;
            endereco.PaisId = input.PaisId;
        }
    }
}
