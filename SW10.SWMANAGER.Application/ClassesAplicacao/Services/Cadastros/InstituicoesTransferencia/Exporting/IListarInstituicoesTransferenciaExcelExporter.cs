﻿using System.Collections.Generic;
using SW10.SWMANAGER.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.InstituicoesTransferencia.Dto;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.InstituicoesTransferencia.Exporting
{
    public interface IListarInstituicaoTransferenciaExcelExporter
    {
        FileDto ExportToFile(List<InstituicaoTransferenciaDto> InstituicaoTransferenciaDtos);
    }
}
