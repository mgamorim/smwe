﻿#region Usings
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.UI;
using MoreLinq;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.UltimosIds;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ComprasRequisicao;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos.Exporting;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEmpresa.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosUnidade.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Unidades.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Compras;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
#endregion usings.

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos
{
    /// <summary>
    /// Serviço de Produtos
    /// Classe que implementa os Serviços para operação com Produtos
    /// </summary>
    public class ProdutoAppService : SWMANAGERAppServiceBase, IProdutoAppService
    {
        #region ↓ Atributos

        #region → Services
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEstoqueAppService _estoqueAppService;
        private readonly IUltimoIdAppService _ultimoIdAppService;
        private readonly IListarProdutosExcelExporter _listarProdutosExcelExporter;
        private readonly IProdutoUnidadeAppService _produtoUnidadeAppService;
        //private readonly ICompraRequisicaoAppService _compraRequisicaoAppService;
        #endregion

        #region → Repositorios
        private readonly IRepository<DCB, long> _dcbRepositorio;
        private readonly IRepository<ProdutoSaldo, long> _produtoSaldoRepositorio;
        private readonly IRepository<Produto, long> _produtoRepositorio;
        private readonly IRepository<Estoque, long> _estoqueRepositorio;
        private readonly IRepository<ProdutoUnidade, long> _produtoUnidadeRepositorio;
        private readonly IRepository<ProdutoEstoque, long> _produtoEstoqueRepositorio;
        private readonly IRepository<ProdutoEmpresa, long> _produtoEmpresaRepositorio;
        private readonly IRepository<EstoquePreMovimento, long> _estoquePreMovimentoRepositorio;
        private readonly IRepository<EstoquePreMovimentoItem, long> _estoquePreMovimentoItemRepositorio;
        private readonly IRepository<EstoqueMovimentoLoteValidade, long> _estoqueMovimentoLoteValidadeRepositorio;
        private readonly IRepository<EstoqueGrupo, long> _estoqueGrupoRepositorio;
        private readonly IRepository<EstoqueMovimento, long> _estoqueMovimentoRepositorio;
        private readonly IRepository<EstoqueMovimentoItem, long> _estoqueMovimentoItemRepositorio;
        private readonly IRepository<CompraRequisicao, long> _compraRequisicaoRepositorio;
        private readonly IRepository<CompraRequisicaoItem, long> _compraRequisicaoItemRepositorio;
        #endregion

        #endregion Atributos

        #region ↓ Construtores
        public ProdutoAppService(

        #region → Services
            IUnitOfWorkManager unitOfWorkManager,
            IEstoqueAppService estoqueAppService,
            IUltimoIdAppService ultimoIdAppService,
            IListarProdutosExcelExporter listarProdutosExcelExporter,
            IProdutoUnidadeAppService produtoUnidadeAppService,
            //ICompraRequisicaoAppService compraRequisicaoAppService,
        #endregion

        #region → Repositorios
            IRepository<DCB, long> dcbRepositorio,
            IRepository<ProdutoSaldo, long> produtoSaldoRepositorio,
            IRepository<Produto, long> produtoRepositorio,
            IRepository<Estoque, long> estoqueRepositorio,
            IRepository<ProdutoUnidade, long> produtoUnidadeRepositorio,
            IRepository<ProdutoEstoque, long> produtoEstoqueRepositorio,
            IRepository<ProdutoEmpresa, long> produtoEmpresaRepositorio,
            IRepository<EstoquePreMovimento, long> estoquePreMovimentoRepositorio,
            IRepository<EstoquePreMovimentoItem, long> estoquePreMovimentoItemRepositorio,
            IRepository<EstoqueMovimentoLoteValidade, long> estoqueMovimentoLoteValidadeRepositorio,
            IRepository<EstoqueGrupo, long> estoqueGrupoRepositorio,
            IRepository<EstoqueMovimento, long> estoqueMovimentoRepositorio,
            IRepository<EstoqueMovimentoItem, long> estoqueMovimentoItemRepositorio,
            IRepository<CompraRequisicao, long> compraRequisicaoRepositorio,
            IRepository<CompraRequisicaoItem, long> compraRequisicaoItemRepositorio
        #endregion

        )

        {
            #region → Services
            _unitOfWorkManager = unitOfWorkManager;
            _ultimoIdAppService = ultimoIdAppService;
            _listarProdutosExcelExporter = listarProdutosExcelExporter;
            _produtoUnidadeAppService = produtoUnidadeAppService;
            _estoqueAppService = estoqueAppService;
            //_compraRequisicaoAppService = compraRequisicaoAppService;
            #endregion Services

            #region → Repositorios
            _produtoRepositorio = produtoRepositorio;
            _estoqueRepositorio = estoqueRepositorio;
            _produtoUnidadeRepositorio = produtoUnidadeRepositorio;
            _produtoEstoqueRepositorio = produtoEstoqueRepositorio;
            _produtoEmpresaRepositorio = produtoEmpresaRepositorio;
            _dcbRepositorio = dcbRepositorio;
            _produtoSaldoRepositorio = produtoSaldoRepositorio;
            _estoquePreMovimentoRepositorio = estoquePreMovimentoRepositorio;
            _estoquePreMovimentoItemRepositorio = estoquePreMovimentoItemRepositorio;
            _estoqueMovimentoLoteValidadeRepositorio = estoqueMovimentoLoteValidadeRepositorio;
            _estoqueGrupoRepositorio = estoqueGrupoRepositorio;
            _estoqueMovimentoRepositorio = estoqueMovimentoRepositorio;
            _estoqueMovimentoItemRepositorio = estoqueMovimentoItemRepositorio;
            _compraRequisicaoRepositorio = compraRequisicaoRepositorio;
            _compraRequisicaoItemRepositorio = compraRequisicaoItemRepositorio;
            #endregion
        }

        #endregion Construtores

        #region ↓ Propriedades
        //-- Nada implementado
        #endregion Propriedades

        #region ↓ Metodos

        #region → Basico - CRUD
        /// <summary>
        /// Cria ou Edita um produto, considerando se o valor do atributo ID possui valor ou nao
        /// </summary>
        /// <param name="input">Dto de Produto</param>
        /// <returns>Sem retorno</returns>
        [UnitOfWork]
        public async Task CriarOuEditar(ProdutoDto input)
        {
            try
            {
                input.Codigo = ObterProximoNumero(input).ToString();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    var produto = input.MapTo<Produto>();
                    if (input.Id.Equals(0))
                    {
                        await _produtoRepositorio.InsertAsync(produto);
                    }
                    else
                    {
                        var produtoEntity = _produtoRepositorio.GetAll()
                                                               .Where(w => w.Id == input.Id)
                                                               .FirstOrDefault();

                        if(produtoEntity !=null)
                        {
                            produtoEntity.Codigo = input.Codigo;
                            produtoEntity.CodigoBarra = input.CodigoBarra;
                            produtoEntity.CodigoTISS = input.CodigoTISS;
                            produtoEntity.ContaAdministrativaId = input.ContaAdministrativaId;
                            produtoEntity.DCBId = input.DCBId;
                            produtoEntity.Descricao = input.Descricao;
                            produtoEntity.DescricaoResumida = input.DescricaoResumida;
                            produtoEntity.EstoqueLocalizacaoId = input.EstoqueLocalizacaoId;
                            produtoEntity.EtiquetaId = input.EtiquetaId;
                            produtoEntity.FaturamentoItemId = input.FaturamentoItemId;
                            produtoEntity.GeneroId = input.GeneroId;
                            produtoEntity.GrupoClasseId = input.GrupoClasseId;
                            produtoEntity.GrupoId = input.GrupoId;
                            produtoEntity.GrupoSubClasseId = input.GrupoSubClasseId;
                            produtoEntity.IsAtivo = input.IsAtivo;
                            produtoEntity.IsBloqueioCompra = input.IsBloqueioCompra;
                            produtoEntity.IsConsignado = input.IsConsignado;
                            produtoEntity.IsCurvaABC = input.IsCurvaABC;
                            produtoEntity.IsFaturamentoItem = input.IsFaturamentoItem;
                            produtoEntity.IsKit = input.IsKit;
                            produtoEntity.IsLiberadoMovimentacao = input.IsLiberadoMovimentacao;
                            produtoEntity.IsLote = input.IsLote;
                            produtoEntity.IsMedicamento = input.IsMedicamento;
                            produtoEntity.IsMedicamentoControlado = input.IsMedicamentoControlado;
                            produtoEntity.IsOPME = input.IsOPME;
                            produtoEntity.IsPadronizado = input.IsPadronizado;
                            produtoEntity.IsPrescricaoItem = input.IsPrescricaoItem;
                            produtoEntity.IsPrincipal = input.IsPrincipal;
                            produtoEntity.IsSerie = input.IsSerie;
                            produtoEntity.IsValidade = input.IsValidade;
                            produtoEntity.ProdutoPrincipalId = input.ProdutoPrincipalId;

                            await _produtoRepositorio.UpdateAsync(produtoEntity);
                        }


                       
                    }

                    if (input.UnidadeReferencialId != null)
                    {
                        ProdutoUnidade produtoUnidade = _produtoUnidadeRepositorio.GetAll().Where(w => w.ProdutoId == produto.Id
                                                                  && w.UnidadeTipoId == 1).FirstOrDefault();

                        if (produtoUnidade != null)
                        {
                            produtoUnidade.ProdutoId = produto.Id;
                            produtoUnidade.UnidadeId = (long)input.UnidadeReferencialId;
                            produtoUnidade.UnidadeTipoId = 1;
                            produtoUnidade.IsAtivo = true;
                            produtoUnidade.IsPrescricao = false;

                            await _produtoUnidadeRepositorio.UpdateAsync(produtoUnidade);
                        }
                        else
                        {
                            produtoUnidade = new ProdutoUnidade();

                            produtoUnidade.ProdutoId = produto.Id;
                            produtoUnidade.UnidadeId = (long)input.UnidadeReferencialId;
                            produtoUnidade.UnidadeTipoId = 1;
                            produtoUnidade.IsAtivo = true;
                            produtoUnidade.IsPrescricao = false;

                            await _produtoUnidadeRepositorio.InsertAsync(produtoUnidade);

                        }



                        if (input.UnidadeGerencialId != null)
                        {
                            ProdutoUnidade produtoUnidadeGerencial = _produtoUnidadeRepositorio.GetAll().Where(w => w.ProdutoId == produto.Id
                                                                 && w.UnidadeTipoId == 2).FirstOrDefault();

                            if (produtoUnidadeGerencial != null)
                            {
                                produtoUnidadeGerencial.ProdutoId = produto.Id;
                                produtoUnidadeGerencial.UnidadeId = (long)input.UnidadeGerencialId;
                                produtoUnidadeGerencial.UnidadeTipoId = 2;
                                produtoUnidadeGerencial.IsAtivo = true;
                                produtoUnidadeGerencial.IsPrescricao = false;

                                await _produtoUnidadeRepositorio.UpdateAsync(produtoUnidadeGerencial);
                            }
                            else
                            {
                                produtoUnidadeGerencial = new ProdutoUnidade();

                                produtoUnidadeGerencial.ProdutoId = produto.Id;
                                produtoUnidadeGerencial.UnidadeId = (long)input.UnidadeGerencialId;
                                produtoUnidadeGerencial.UnidadeTipoId = 2;
                                produtoUnidadeGerencial.IsAtivo = true;
                                produtoUnidadeGerencial.IsPrescricao = false;

                                await _produtoUnidadeRepositorio.InsertAsync(produtoUnidadeGerencial);
                            }
                        }
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        /// <summary>
        /// Exclui um produto
        /// </summary>
        /// <param name="input">Dto de Produto</param>
        /// <returns>Sem retorno</returns>
        [UnitOfWork]
        public async Task Excluir(ProdutoDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _produtoRepositorio.DeleteAsync(input.Id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }
        }

        /// <summary>
        /// Retorna um Dto de Produto
        /// </summary>
        /// <param name="id">Id do produto desejado</param>
        /// <returns>Dto de Produto</returns>
        public async Task<ProdutoDto> Obter(long id)
        {
            try
            {
                var query = _produtoRepositorio
                    .GetAll()
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse)
                    .Where(m => m.Id == id);

                var result = await query.FirstOrDefaultAsync();
                var produto = result.MapTo<ProdutoDto>();

                produto.possuiMovimentacaoDeEstoque = await ExisteMovimentacaoDeEstoque(id);

                produto.possuiRequisicaoDeCompraPendente = await ExisteRequisicaoDeCompraPendenteParaOProduto(id);

                return produto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        #endregion

        #region → Basico - Listar

        /// <summary>
        /// Retorna uma lista paginada de Dtos Produtos preparada para popular um JTable
        /// </summary>
        /// <param name="input">ListarProdutosInput</param>
        public async Task<PagedResultDto<ProdutoDto>> Listar(ListarProdutosInput input)
        {
            var contarProdutos = 0;
            List<Produto> Produtos;
            List<ProdutoDto> ProdutosDtos = new List<ProdutoDto>();

            long grupoId;
            long grupoClasseId;
            long grupoSubClasseId;
            long DCBId;
            long FiltroPrincipal;
            long FiltroStatus;

            long.TryParse(input.GrupoId, out grupoId);
            long.TryParse(input.GrupoClasseId, out grupoClasseId);
            long.TryParse(input.GrupoSubClasseId, out grupoSubClasseId);
            long.TryParse(input.DCBId, out DCBId);
            long.TryParse(input.FiltroPrincipal, out FiltroPrincipal);
            long.TryParse(input.FiltroStatus, out FiltroStatus);

            try
            {
                var query = _produtoRepositorio
                    .GetAll()
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse)
                    .Include(m => m.DCB)
                    .Include(m => m.Classe)
                    .Where(m =>
                       (m.Id.ToString().Contains(input.Filtro) ||
                       m.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                       m.DescricaoResumida.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.DCB.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||

                         m.Grupo.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.Classe.Descricao.ToUpper().Contains(input.Filtro.ToUpper()) ||
                         m.SubClasse.Descricao.ToUpper().Contains(input.Filtro.ToUpper())
                       )

                       && (grupoId == 0 || m.GrupoId == grupoId)
                       && (grupoClasseId == 0 || m.GrupoClasseId == grupoClasseId)
                       && (grupoSubClasseId == 0 || m.GrupoSubClasseId == grupoSubClasseId)
                        && (DCBId == 0 || m.DCBId == DCBId)
                    )

                    //Opcao para Listar todos os Produtos que são Principais e Produtos que nao sao produtos principais e também nao possuem um produto principal
                    .WhereIf(FiltroPrincipal == 1, m => m.IsPrincipal == true || m.ProdutoPrincipalId == null)

                    //Mostrar apenas os que possuem produto principal(que sao filhos)
                    .WhereIf(FiltroPrincipal == 2, m => m.ProdutoPrincipalId > 0)

                    .WhereIf(FiltroStatus == 1, m => m.IsAtivo)

                    .WhereIf(FiltroStatus == 2, m => m.IsAtivo);

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoDto>>();

                return new PagedResultDto<ProdutoDto>(
                    contarProdutos,
                    ProdutosDtos
                );

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna uma lista de Dtos de produto
        /// </summary>
        public async Task<ListResultDto<ProdutoDto>> ListarTodos()
        {
            var contarProdutos = 0;
            List<Produto> Produtos;
            List<ProdutoDto> ProdutosDtos = new List<ProdutoDto>();
            try
            {
                var query = _produtoRepositorio
                    .GetAll()
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse);

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoDto>>();

                return new ListResultDto<ProdutoDto> { Items = ProdutosDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        #endregion Basico - Listar

        #region → Obter
        /// <summary>
        /// Retorna o próximo codigo disponivel. Ps: Não é o campo Id
        /// </summary>
        public string ObterProximoNumero(ProdutoDto input)
        {

            if (Convert.ToInt64(input.Codigo.IsNullOrEmpty() ? "0" : input.Codigo) == 0)
            {
                ///TODO: NOVO PRODUTO
                //return _ultimoIdAppService.ObterProximoCodigo("SaidaProduto").Result;
                return _ultimoIdAppService.ObterProximoCodigo("Produto").Result;

                //var query = _produtoRepositorio.GetAll().Count() > 0 ? _produtoRepositorio.GetAll().Max(m => Convert.ToInt64(m.Codigo)) : 0; //   _produtoRepositorio.GetAll().Max(m => m.Codigo);

            }
            else
            {
                return input.Codigo;
            }
        }

        /// <summary>
        /// Cria um novo produto e retorna um ProdutoDto com seu Id
        /// </summary>
        [UnitOfWork]
        public ProdutoDto CriarGetId(ProdutoDto input)
        {
            try
            {
                input.Codigo = ObterProximoNumero(input).ToString();
                ProdutoDto produtoDto;
                var produto = input.MapTo<Produto>();

                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    if (input.Id.Equals(0))
                    {
                        //Inclui o produto e retorna produtoDto com o Id
                        produtoDto = new ProdutoDto { Id = AsyncHelper.RunSync(() => _produtoRepositorio.InsertAndGetIdAsync(produto)) };

                        //Inclui as unidades Referencial e Gerencial

                    }
                    else
                    {
                        produtoDto = AsyncHelper.RunSync(() => _produtoRepositorio.UpdateAsync(produto)).MapTo<ProdutoDto>();
                    }


                    if (input.UnidadeReferencialId != null)
                    {
                        CriarOuEditarProdutoUnidade produtoUnidade = new CriarOuEditarProdutoUnidade();
                        produtoUnidade.ProdutoId = produtoDto.Id;
                        produtoUnidade.UnidadeId = (long)input.UnidadeReferencialId;
                        produtoUnidade.UnidadeTipoId = 1;
                        produtoUnidade.IsAtivo = true;
                        produtoUnidade.IsPrescricao = false;

                        _produtoUnidadeAppService.CriarOuEditar(produtoUnidade);

                        if (input.UnidadeGerencialId != null)
                        {
                            CriarOuEditarProdutoUnidade produtoUnidadeGerencial = new CriarOuEditarProdutoUnidade();
                            produtoUnidadeGerencial.ProdutoId = produtoDto.Id;
                            produtoUnidadeGerencial.UnidadeId = (long)input.UnidadeGerencialId;
                            produtoUnidadeGerencial.UnidadeTipoId = 2;
                            produtoUnidadeGerencial.IsAtivo = true;
                            produtoUnidadeGerencial.IsPrescricao = false;

                            _produtoUnidadeAppService.CriarOuEditar(produtoUnidadeGerencial);
                        }
                    }

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }

                produtoDto.Codigo = input.Codigo;
                return produtoDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }

        }

        public async Task<ListResultDto<UnidadeDto>> ObterUnidadePorProduto(long id)
        {
            List<ProdutoUnidadeDto> listDtos = new List<ProdutoUnidadeDto>();
            List<UnidadeDto> unidades = new List<UnidadeDto>();
            try
            {
                //var result = await _produtoUnidadeRepositorio
                //    .GetAll()
                //    .Where(w => w.ProdutoId == id)
                //    .FirstOrDefaultAsync();

                var result = await _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Include(m => m.Unidade)
                    .Where(a => a.ProdutoId == id)
                    .ToListAsync();

                listDtos = result.MapTo<List<ProdutoUnidadeDto>>();
                unidades = listDtos.Select(s => s.Unidade).DistinctBy(m => m.Id).ToList();
                return new ListResultDto<UnidadeDto> { Items = unidades };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        public async Task<ListResultDto<UnidadeDto>> ObterUnidadePorProduto(long id, bool listarRefGer = true)
        {
            List<ProdutoUnidadeDto> listDtos = new List<ProdutoUnidadeDto>();
            List<UnidadeDto> unidades = new List<UnidadeDto>();
            try
            {
                //var result = await _produtoUnidadeRepositorio
                //    .GetAll()
                //    .Where(w => w.ProdutoId == id)
                //    .FirstOrDefaultAsync();

                var result = await _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Include(m => m.Unidade)
                    .Where(a => a.ProdutoId == id)
                    .WhereIf(listarRefGer == false, m => ((m.UnidadeTipoId != 1) && (m.UnidadeTipoId != 2)))
                    .ToListAsync();

                listDtos = result.MapTo<List<ProdutoUnidadeDto>>();
                unidades = listDtos.Select(s => s.Unidade).DistinctBy(m => m.Id).ToList();
                return new ListResultDto<UnidadeDto> { Items = unidades };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }

        }

        /// <summary>
        /// Retorna a Unidade Referencia ou Gerencial do produto
        /// Referencia: idTipoUnidade = 1 | Gerencial: idTipoUnidade = 2
        /// </summary>
        public async Task<UnidadeDto> ObterUnidadePorTipo(long idProduto, long idTipoUnidade)
        {
            List<ProdutoUnidadeDto> listDtos = new List<ProdutoUnidadeDto>();
            List<UnidadeDto> unidades = new List<UnidadeDto>();
            try
            {
                var query = _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Include(m => m.Unidade)
                    .Where(a => a.ProdutoId == idProduto && a.UnidadeTipoId == idTipoUnidade);

                var result = await query.FirstOrDefaultAsync();

                var produtoUnidade = result.MapTo<ProdutoUnidadeDto>();
                if (produtoUnidade != null)
                {
                    var unidade = produtoUnidade.Unidade;
                    return unidade;
                }
                else
                {
                    return new UnidadeDto();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna a Unidade Refekrencia do produto - UnidadeTipoId = 1
        /// </summary>
        public async Task<UnidadeDto> ObterUnidadeReferencial(long idProduto)
        {
            List<ProdutoUnidadeDto> listDtos = new List<ProdutoUnidadeDto>();
            List<UnidadeDto> unidades = new List<UnidadeDto>();
            try
            {
                var query = _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(u => u.Unidade)
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Where(a => a.ProdutoId == idProduto && a.UnidadeTipoId == 1);

                var result = await query.FirstOrDefaultAsync();

                var produtoUnidade = result.MapTo<ProdutoUnidadeDto>();
                if (produtoUnidade != null)
                {
                    var unidade = produtoUnidade.Unidade;
                    return unidade;
                }
                else
                {
                    return new UnidadeDto();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna a Unidade Gerencial do produto (UnidadeTipoId = 2)
        /// </summary>
        public async Task<UnidadeDto> ObterUnidadeGerencial(long idProduto)
        {
            List<ProdutoUnidadeDto> listDtos = new List<ProdutoUnidadeDto>();
            List<UnidadeDto> unidades = new List<UnidadeDto>();
            try
            {
                var query = _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Include(m => m.Unidade)
                    .Where(a => a.ProdutoId == idProduto && a.UnidadeTipoId == 2);

                var result = await query.FirstOrDefaultAsync();

                var produtoUnidade = result.MapTo<ProdutoUnidadeDto>();

                if (produtoUnidade != null)
                {
                    var unidade = produtoUnidade.Unidade;
                    return unidade;
                }
                else
                {
                    return new UnidadeDto();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        #endregion

        #region → Gets
        /// <summary>
        /// Indica se há movimentação de estoque para o produto
        /// </summary>
        /// <param name="id">Id do produto desejado</param>
        /// <returns>bool</returns>
        public async Task<bool> ExisteMovimentacaoDeEstoque(long id)
        {
            var contarProdutos = 0;

            try
            {
                var query = _estoquePreMovimentoItemRepositorio
                    .GetAll()
                    .Where(m => m.ProdutoId == id);

                contarProdutos = await query
                    .CountAsync();

                bool tem;

                tem = contarProdutos > 0;

                return tem;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Indica se um produto possui Requisição de Compra
        /// </summary>
        /// <param name="id">Id do produto desejado</param>
        /// <returns>bool</returns>
        public async Task<bool> ExisteRequisicaoDeCompraPendenteParaOProduto(long id)
        {
            var contarProdutos = 0;

            try
            {
                var query = from cmp in _compraRequisicaoRepositorio.GetAll()
                            join cmpItem in _compraRequisicaoItemRepositorio.GetAll() on cmp.Id equals cmpItem.RequisicaoId into joined
                            from cmpItem in joined.DefaultIfEmpty()
                            where (cmpItem.ProdutoId == id) && (cmp.Id == cmpItem.RequisicaoId) && (!cmp.IsEncerrada)
                            select cmp.Id;

                //.GetAll()
                //.Where(m => m.ProdutoId == id);

                contarProdutos = await query
                    .CountAsync();

                bool existe;

                existe = contarProdutos > 0;

                return existe;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }
        #endregion

        #region → Listar

        /// <summary>
        /// Retorna uma lista de produto ativos e não bloqueados para compra
        /// </summary>
        /// <returns>Lista de Dtos de produto</returns>
        public async Task<ListResultDto<ProdutoDto>> ListarTodosParaMovimento()
        {
            var contarProdutos = 0;
            List<Produto> Produtos;
            List<ProdutoDto> ProdutosDtos = new List<ProdutoDto>();
            try
            {
                var query = _produtoRepositorio
                    .GetAll()
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse)
                    .Where(w => !w.IsPrincipal
                                      && !w.IsBloqueioCompra
                                      && w.IsAtivo);

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoDto>>();

                return new ListResultDto<ProdutoDto> { Items = ProdutosDtos };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<ListResultDto<ProdutoDto>> ListarProdutosExcetoId(long ProdutoExcetoId)
        {
            List<Produto> Produtos;
            List<ProdutoDto> ProdutosDtos = new List<ProdutoDto>();
            try
            {
                var query = _produtoRepositorio
                           .GetAll()
                    .Include(m => m.Classe)
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse)
                            .Where(m => m.Id != ProdutoExcetoId);

                Produtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoDto>>();

                return new ListResultDto<ProdutoDto> { Items = ProdutosDtos };
            }

            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// Retorna Lista com todos os produtos definidos como Principal(Mestre)
        /// </summary>
        /// <returns>ListResultDto<GenericoIdNome> de Produto</returns>
        public async Task<ListResultDto<GenericoIdNome>> ListarProdutosMestre()
        {
            //List<ProdutoDto> produtosDtos = new List<ProdutoDto>();
            try
            {
                var query = await _produtoRepositorio
                    .GetAll()
                    //.Include(m => m.DCB)
                    //.Include(m => m.EstoqueLocalizacao)
                    //.Include(m => m.Genero)
                    //.Include(m => m.Grupo)
                    //.Include(m => m.ProdutoPrincipal)
                    //.Include(m => m.SubClasse)
                    .Where(a => a.IsPrincipal == true)
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                //var produtosDto = query.MapTo<List<ProdutoDto>>();

                return new ListResultDto<GenericoIdNome> { Items = query };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }

        }

        /// <summary>
        /// Retorna uma lista com Nome(descricao) e Id de produtos.
        /// </summary>
        public async Task<ListResultDto<GenericoIdNome>> ListarAutoComplete(string input)
        {
            try
            {
                var query = await _produtoRepositorio
                    .GetAll()
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse)
                    .WhereIf(!input.IsNullOrEmpty(), m =>
                        m.Descricao.ToUpper().Contains(input.ToUpper()) ||
                        m.DescricaoResumida.ToUpper().Contains(input.ToUpper())
                        )
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                    .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// ListResultDto de DCB
        /// </summary>
        public async Task<ListResultDto<GenericoIdNome>> ListarDCBs()
        {
            try
            {
                var query = await _dcbRepositorio
                    .GetAll()
                    // .Where(w=>w.Id==1)
                    .Select(m => new GenericoIdNome { Id = m.Id, Nome = m.Descricao })
                   .ToListAsync();

                return new ListResultDto<GenericoIdNome> { Items = query };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<ListResultDto<ProdutoDto>> ListarProdutosMestresExcetoId(long ProdutoExcetoId)
        {
            List<Produto> Produtos;
            List<ProdutoDto> ProdutosDtos = new List<ProdutoDto>();
            try
            {
                var query = _produtoRepositorio
                           .GetAll()
                    .Include(m => m.Classe)
                    .Include(m => m.DCB)
                    .Include(m => m.EstoqueLocalizacao)
                    .Include(m => m.Genero)
                    .Include(m => m.Grupo)
                    .Include(m => m.ProdutoPrincipal)
                    .Include(m => m.SubClasse)
                           .Where(m => m.Id != ProdutoExcetoId && m.IsPrincipal);

                Produtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoDto>>();

                return new ListResultDto<ProdutoDto> { Items = ProdutosDtos };
            }

            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<PagedResultDto<ProdutoUnidadeDto>> ListarProdutoUnidade(long produtoId)
        {
            var contarProdutosUnidades = 0;
            List<ProdutoUnidade> ProdutosUnidades;
            List<ProdutoUnidadeDto> ProdutosUnidadesDtos = new List<ProdutoUnidadeDto>();
            try
            {
                var query = _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Include(m => m.Unidade)
                    .Where(m => m.ProdutoId == produtoId);

                contarProdutosUnidades = await query
                    .CountAsync();

                ProdutosUnidades = await query
                    .AsNoTracking()
                    .ToListAsync();

                ProdutosUnidadesDtos = ProdutosUnidades
                    .MapTo<List<ProdutoUnidadeDto>>();

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
            return new PagedResultDto<ProdutoUnidadeDto>(
                contarProdutosUnidades,
                ProdutosUnidadesDtos
                );
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<PagedResultDto<ProdutoUnidadeDto>> ListarProdutosUnidadesPorProduto(ListarInput input)
        {
            var contarProdutos = 0;
            List<ProdutoUnidade> Produtos;
            List<ProdutoUnidadeDto> ProdutosDtos = new List<ProdutoUnidadeDto>();
            try
            {
                var id = Convert.ToInt64(input.Filtro);
                var query = _produtoUnidadeRepositorio
                    .GetAll()
                    .Include(m => m.Produto)
                    .Include(m => m.Tipo)
                    .Include(m => m.Unidade)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.ProdutoId == id
                    );

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoUnidadeDto>>();

                return new PagedResultDto<ProdutoUnidadeDto>(
                    contarProdutos,
                    ProdutosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<PagedResultDto<ProdutoEmpresaDto>> ListarProdutosEmpresasPorProduto(ListarInput input)
        {
            var contarProdutos = 0;
            List<ProdutoEmpresa> Produtos;
            List<ProdutoEmpresaDto> ProdutosDtos = new List<ProdutoEmpresaDto>();
            try
            {
                var id = Convert.ToInt64(input.Filtro);
                var query = _produtoEmpresaRepositorio
                    .GetAll()
                    .Include(m => m.Empresa)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.ProdutoId == id
                    );

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoEmpresaDto>>();

                return new PagedResultDto<ProdutoEmpresaDto>(
                    contarProdutos,
                    ProdutosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<PagedResultDto<ProdutoEstoqueDto>> ListarProdutosEstoquesPorProduto(ListarInput input)
        {
            var contarProdutos = 0;
            List<ProdutoEstoque> Produtos;
            List<ProdutoEstoqueDto> ProdutosDtos = new List<ProdutoEstoqueDto>();
            try
            {
                var id = Convert.ToInt64(input.Filtro);
                var query = _produtoEstoqueRepositorio
                    .GetAll()
                    .Include(m => m.Estoque)
                    .WhereIf(!input.Filtro.IsNullOrEmpty(), m =>
                        m.ProdutoId == id
                    );

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoEstoqueDto>>();

                return new PagedResultDto<ProdutoEstoqueDto>(
                    contarProdutos,
                    ProdutosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<PagedResultDto<ProdutoSaldoMinDto>> ListarProdutoSaldo(ListarInput input)
        {
            var contarProdutos = 0;
            // List<ProdutoSaldo> Produtos;
            List<ProdutoSaldoDto> ProdutosDtos = new List<ProdutoSaldoDto>();
            try
            {
                long id = 0;
                var result = long.TryParse(input.Filtro, out id);
                var query = _produtoSaldoRepositorio
                    .GetAll()
                    .Include(m => m.Estoque)
                    .WhereIf(id > 0, m =>
                          m.ProdutoId == id
                    );

                var queryList = query
                                .GroupBy(g => g.EstoqueId)
                                .Select(s => new ProdutoSaldoDto
                                {
                                    EstoqueId = s.Key,
                                    QuantidadeAtual = s.Sum(soma => soma.QuantidadeAtual),
                                    QuantidadeEntradaPendente = s.Sum(soma => soma.QuantidadeEntradaPendente),
                                    QuantidadeSaidaPendente = s.Sum(soma => soma.QuantidadeSaidaPendente),
                                    SaldoFuturo = s.Sum(soma => soma.QuantidadeAtual + soma.QuantidadeEntradaPendente - soma.QuantidadeSaidaPendente)
                                })
                                .ToList();

                var listaProdutoSaldoDto = new List<ProdutoSaldoMinDto>();

                UnidadeDto unidadeGerencial = await ObterUnidadeGerencial(id);

                foreach (var item in queryList)
                {
                    var estoqueDto = new EstoqueDto();
                    estoqueDto = await _estoqueAppService.Obter(item.EstoqueId);
                    listaProdutoSaldoDto.Add(new ProdutoSaldoMinDto
                    {
                        ProdutoId = id,
                        EstoqueId = item.EstoqueId,
                        Descricao = estoqueDto.Descricao,
                        QuantidadeAtual = item.QuantidadeAtual,
                        QuantidadeEntradaPendente = item.QuantidadeEntradaPendente,
                        QuantidadeSaidaPendente = item.QuantidadeSaidaPendente,
                        SaldoFuturo = item.SaldoFuturo,
                        QuantidadeGerencialAtual = item.QuantidadeAtual > 0 ? item.QuantidadeAtual / unidadeGerencial.Fator>0 ? unidadeGerencial.Fator : 1 : 0,
                        QuantidadeGerencialEntradaPendente = item.QuantidadeEntradaPendente > 0 ? item.QuantidadeEntradaPendente / unidadeGerencial.Fator > 0 ? unidadeGerencial.Fator : 1 : 0,
                        QuantidadeGerencialSaidaPendente = item.QuantidadeGerencialSaidaPendente > 0 ? item.QuantidadeSaidaPendente / unidadeGerencial.Fator > 0 ? unidadeGerencial.Fator : 1 : 0,
                        SaldoGerencialFuturo = item.SaldoFuturo > 0 ? item.SaldoFuturo / unidadeGerencial.Fator > 0 ? unidadeGerencial.Fator : 1 : 0
                    });
                }

                contarProdutos = await query
                    .CountAsync();

                //Produtos = await query
                //var  Produtos = await queryList
                var Produtos = listaProdutoSaldoDto
                      //.AsNoTracking()
                      //.OrderBy(input.Sorting)
                      //.PageBy(input)
                      //.ToListAsync();
                      .ToList();

                //ProdutosDtos = Produtos
                //    .MapTo<List<ProdutoSaldoDto>>();

                return new PagedResultDto<ProdutoSaldoMinDto>(
                    contarProdutos,
                    Produtos
                    //ProdutosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ProdutoSaldoDto>> ListarProdutoSaldoDetalhes(ListarSaldoInput input)
        {
            var contarProdutos = 0;
            List<ProdutoSaldo> Produtos;
            List<ProdutoSaldoDto> ProdutosDtos = new List<ProdutoSaldoDto>();
            try
            {
                UnidadeDto unidadeGerencial = await ObterUnidadeGerencial((long)input.Id);
                UnidadeDto unidadeReferencial = await ObterUnidadeReferencial((long)input.Id);

                var query = _produtoSaldoRepositorio
                    .GetAll()
                    .Include(m => m.LoteValidade)
                    .Include(m => m.LoteValidade.EstoqueLaboratorio)
                    .Where(m => m.ProdutoId == input.Id
                        && m.EstoqueId == input.EstoqueId
                    )
                    .Select(s => new ProdutoSaldoDto
                    {
                        EstoqueId = s.EstoqueId,
                        LoteValidadeId = s.LoteValidadeId,
                        QuantidadeAtual = s.QuantidadeAtual,

                        NomeLaboratorio = s.LoteValidade.EstoqueLaboratorio.Descricao,
                        Lote = s.LoteValidade.Lote,
                        Validade = s.LoteValidade.Validade,

                        QuantidadeEntradaPendente = s.QuantidadeEntradaPendente,
                        QuantidadeSaidaPendente = s.QuantidadeSaidaPendente,
                        SaldoFuturo = (s.QuantidadeAtual + s.QuantidadeEntradaPendente - s.QuantidadeSaidaPendente),

                        QuantidadeGerencialAtual = s.QuantidadeAtual / unidadeGerencial.Fator,
                        QuantidadeGerencialEntradaPendente = s.QuantidadeEntradaPendente / unidadeGerencial.Fator,
                        QuantidadeGerencialSaidaPendente = s.QuantidadeSaidaPendente / unidadeGerencial.Fator,
                        SaldoGerencialFuturo = ((s.QuantidadeAtual / unidadeGerencial.Fator) + (s.QuantidadeEntradaPendente / unidadeGerencial.Fator) - (s.QuantidadeSaidaPendente / unidadeGerencial.Fator)),

                        UnidadeGerencial = unidadeGerencial.Sigla,
                        UnidadeReferencia = unidadeReferencial.Sigla

                    });


                contarProdutos = await query
                    .CountAsync();

                //Produtos = query.ToList();

                ProdutosDtos = query.ToList();

                //ProdutosDtos = Produtos.MapTo<List<ProdutoSaldoDto>>();

                return new PagedResultDto<ProdutoSaldoDto>(
                    contarProdutos,
                    ProdutosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ProdutoSaldoMinDto>> ListarProdutoSaldoFilhos(ListarInput input)
        {
            var contarProdutos = 0;
            // List<ProdutoSaldo> Produtos;
            List<ProdutoSaldoDto> ProdutosDtos = new List<ProdutoSaldoDto>();
            try
            {
                var id = Convert.ToInt64(input.Filtro);
                var query = from produto in _produtoRepositorio.GetAll()
                            join saldoProduto in _produtoSaldoRepositorio.GetAll()
                            on produto.Id equals saldoProduto.ProdutoId
                            where produto.ProdutoPrincipalId == id

                            select saldoProduto;


                var queryList = query
                                .GroupBy(g => new { g.EstoqueId, g.ProdutoId })
                                .Select(s => new ProdutoSaldoDto
                                {
                                    EstoqueId = s.Key.EstoqueId,
                                    ProdutoId = s.Key.ProdutoId,
                                    QuantidadeAtual = s.Sum(soma => soma.QuantidadeAtual),
                                    QuantidadeEntradaPendente = s.Sum(soma => soma.QuantidadeEntradaPendente),
                                    QuantidadeSaidaPendente = s.Sum(soma => soma.QuantidadeSaidaPendente),
                                    SaldoFuturo = s.Sum(soma => soma.QuantidadeAtual + soma.QuantidadeEntradaPendente - soma.QuantidadeSaidaPendente)
                                })
                                .ToList();

                var listaProdutoSaldoDto = new List<ProdutoSaldoMinDto>();



                foreach (var item in queryList)
                {
                    var estoqueDto = new EstoqueDto();
                    estoqueDto = await _estoqueAppService.Obter(item.EstoqueId);
                    var produto = await Obter(item.ProdutoId);

                    UnidadeDto unidadeGerencial = await ObterUnidadeGerencial(item.ProdutoId);
                    UnidadeDto unidadeReferencial = await ObterUnidadeReferencial(item.ProdutoId);

                    listaProdutoSaldoDto.Add(new ProdutoSaldoMinDto
                    {
                        ProdutoId = item.ProdutoId,
                        EstoqueId = item.EstoqueId,
                        Descricao = estoqueDto.Descricao,
                        DescricaoProduto = produto.Descricao,

                        QuantidadeAtual = item.QuantidadeAtual,
                        QuantidadeEntradaPendente = item.QuantidadeEntradaPendente,
                        QuantidadeSaidaPendente = item.QuantidadeSaidaPendente,
                        SaldoFuturo = item.SaldoFuturo,

                        QuantidadeGerencialAtual = item.QuantidadeAtual / unidadeGerencial.Fator,
                        QuantidadeGerencialEntradaPendente = item.QuantidadeEntradaPendente / unidadeGerencial.Fator,
                        QuantidadeGerencialSaidaPendente = item.QuantidadeSaidaPendente / unidadeGerencial.Fator,
                        SaldoGerencialFuturo = item.SaldoFuturo / unidadeGerencial.Fator,
                        UnidadeGerencial = unidadeGerencial.Sigla,
                        UnidadeReferencia = unidadeReferencial.Sigla


                    });
                }

                contarProdutos = await query
                    .CountAsync();

                //Produtos = await query
                //var  Produtos = await queryList
                var Produtos = listaProdutoSaldoDto
                      //.AsNoTracking()
                      //.OrderBy(input.Sorting)
                      //.PageBy(input)
                      //.ToListAsync();
                      .ToList();

                //ProdutosDtos = Produtos
                //    .MapTo<List<ProdutoSaldoDto>>();

                return new PagedResultDto<ProdutoSaldoMinDto>(
                    contarProdutos,
                    Produtos
                    //ProdutosDtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        //public async Task<PagedResultDto<ProdutoDto>> ListarProdutoMesmoPrincipal(long id)
        //public async Task<PagedResultDto<ProdutoDto>> ListarProdutoMesmoPrincipal(long id, long principalId)
        public async Task<PagedResultDto<ProdutoDto>> ListarProdutoMesmoPrincipal(ListarInput input)
        {
            var contarProdutos = 0;
            List<Produto> Produtos;
            List<ProdutoDto> ProdutosDtos = new List<ProdutoDto>();
            try
            {
                //var id = Convert.ToInt64(input.Filtro);
                var id = Convert.ToInt64(input.Id);
                var principalId = Convert.ToInt64(input.PrincipalId);

                var query = _produtoRepositorio
                    .GetAll()
                    .Where(m => m.Id != id)
                    .Where(m => m.ProdutoPrincipalId == principalId);

                contarProdutos = await query
                    .CountAsync();

                Produtos = await query
                    .AsNoTracking()
                    .ToListAsync();

                ProdutosDtos = Produtos
                    .MapTo<List<ProdutoDto>>();

                //return new ListResultDto<ProdutoDto> { Items = ProdutosDtos };

                return new PagedResultDto<ProdutoDto>(
                    contarProdutos,
                    ProdutosDtos
                );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        public async Task<PagedResultDto<ProdutoSaldoMinDto>> ListarProdutoMesmoPrincipalComSaldo(ListarInput input)
        {
            var contarProdutos = 0;
            // List<ProdutoSaldo> Produtos;
            List<ProdutoSaldoDto> ProdutosDtos = new List<ProdutoSaldoDto>();
            try
            {
                //var query = from mov in _estoqueMovimentoReposi0tory.GetAll()
                //            join item in _estoqueMovimentoItemRepositorio.GetAll()
                //            on mov.Id equals item.MovimentoId
                //            into movjoin
                //            from joinedmov in movjoin.DefaultIfEmpty()
                //            where mov.EstoqueId == estoqueId
                //               && joinedmov.ProdutoId == produtoId
                //               && !String.IsNullOrEmpty(joinedmov.NumeroSerie)


                var id = Convert.ToInt64(input.Id);
                var principalId = Convert.ToInt64(input.PrincipalId);
                var query = from produto in _produtoRepositorio.GetAll()
                            join saldoProduto in _produtoSaldoRepositorio.GetAll()
                            on produto.Id equals saldoProduto.ProdutoId
                            into produtojoin
                            from joined in produtojoin.DefaultIfEmpty()
                            where ((produto.ProdutoPrincipalId == principalId) && (produto.IsAtivo)) && (produto.Id != id)
                            select new //Rapidinho
                            {
                                //ProdutoId = joined.ProdutoId == null ? 0 : joined.ProdutoId,
                                ProdutoId = produto.Id,
                                //EstoqueId = joined.EstoqueId,
                                QuantidadeAtual = joined == null ? 0 : joined.QuantidadeAtual,
                                QuantidadeEntradaPendente = joined == null ? 0 : joined.QuantidadeEntradaPendente,
                                QuantidadeSaidaPendente = joined == null ? 0 : joined.QuantidadeSaidaPendente
                            };

                var queryList = query
                                //.GroupBy(g => new { g.EstoqueId, g.ProdutoId })
                                .GroupBy(g => new { g.ProdutoId })
                                .Select(s => new ProdutoSaldoDto
                                {
                                    //EstoqueId = s.Key.EstoqueId,
                                    ProdutoId = s.Key.ProdutoId,
                                    QuantidadeAtual = s.Sum(soma => soma.QuantidadeAtual),
                                    QuantidadeEntradaPendente = s.Sum(soma => soma.QuantidadeEntradaPendente),
                                    QuantidadeSaidaPendente = s.Sum(soma => soma.QuantidadeSaidaPendente),
                                    SaldoFuturo = s.Sum(soma => soma.QuantidadeAtual + soma.QuantidadeEntradaPendente - soma.QuantidadeSaidaPendente)
                                })
                                .ToList();

                var listaProdutoSaldoDto = new List<ProdutoSaldoMinDto>();

                foreach (var item in queryList)
                {
                    //var estoqueDto = new EstoqueDto();
                    //estoqueDto = await _estoqueAppService.Obter(item.EstoqueId);
                    var produto = await Obter(item.ProdutoId);

                    UnidadeDto unidadeGerencial = await ObterUnidadeGerencial(item.ProdutoId);
                    UnidadeDto unidadeReferencial = await ObterUnidadeReferencial(item.ProdutoId);

                    listaProdutoSaldoDto.Add(new ProdutoSaldoMinDto
                    {
                        Codigo = produto.Codigo,
                        ProdutoId = item.ProdutoId,
                        //EstoqueId = item.EstoqueId,
                        //Descricao = estoqueDto.Descricao,
                        EstoqueId = 0,
                        Descricao = null,
                        DescricaoProduto = produto.Descricao,

                        QuantidadeAtual = item.QuantidadeAtual,
                        QuantidadeEntradaPendente = item.QuantidadeEntradaPendente,
                        QuantidadeSaidaPendente = item.QuantidadeSaidaPendente,
                        SaldoFuturo = item.SaldoFuturo,

                        QuantidadeGerencialAtual = item.QuantidadeAtual / unidadeGerencial.Fator,
                        QuantidadeGerencialEntradaPendente = item.QuantidadeEntradaPendente / unidadeGerencial.Fator,
                        QuantidadeGerencialSaidaPendente = item.QuantidadeSaidaPendente / unidadeGerencial.Fator,
                        SaldoGerencialFuturo = item.SaldoFuturo / unidadeGerencial.Fator,
                        UnidadeReferencia = unidadeReferencial.Sigla,
                        UnidadeGerencial = unidadeGerencial.Sigla
                    });
                }

                contarProdutos = await query
                    .CountAsync();

                //var  Produtos = await queryList
                var Produtos = listaProdutoSaldoDto
                //var Produtos = query
                //.AsNoTracking()
                //.OrderBy(input.Sorting)
                //.PageBy(input)
                //.ToListAsync();
                .ToList();

                //return new PagedResultDto<Rapidinho>(
                return new PagedResultDto<ProdutoSaldoMinDto>(
                    contarProdutos,
                    Produtos
                    );
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"));
            }
        }

        #endregion Listar

        #region → Dropdowns

        public async Task<ResultDropdownList> ListarProdutoDropdown(DropdownInput dropdownInput)
        {

            return await ListarDropdownLambda(dropdownInput
                                                     , _produtoRepositorio
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                     && (!m.IsPrincipal
                                                     && !m.IsBloqueioCompra
                                                     && m.IsAtivo)


                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );

        }

        /// <summary>
        /// Listar produtos filtrados por Grupo, paginados para uso com select2
        /// </summary>
        /// <param name="dropdownInput"></param>
        /// <returns></returns>
        public async Task<ResultDropdownList> ListarProdutoPorGrupoDropdown(DropdownInput dropdownInput)
        {
            long grupoId;

            long.TryParse(dropdownInput.filtro, out grupoId);

            return await ListarDropdownLambda(dropdownInput
                                                     , _produtoRepositorio
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                     && (!m.IsPrincipal
                                                     && !m.IsBloqueioCompra
                                                     && m.IsAtivo)

                                                     && m.GrupoId == grupoId

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );
        }

        /// <summary>
        ///Listar para BrasPreco, paginados para uso com select2
        /// </summary>
        /// <param name="dropdownInput"></param>
        /// <returns></returns>
        public async Task<ResultDropdownList> ListarDropdownParaBrasPreco(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            int numberOfObjectsPerPage = 1;

            List<ProdutoDto> faturamentoItensDto = new List<ProdutoDto>();
            try
            {
                if (!int.TryParse(dropdownInput.totalPorPagina, out numberOfObjectsPerPage))
                {
                    throw new Exception("NotANumber");
                }

                var query = from p in _produtoRepositorio.GetAll()
                            .WhereIf(!dropdownInput.filtro.IsNullOrEmpty(),
                            m => m.Id.ToString() == dropdownInput.filtro)
                            .WhereIf(!dropdownInput.search.IsNullOrEmpty()
                            ,
                            m => m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()) ||
                                m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower())
                                )
                            orderby p.Descricao ascending
                            select new DropdownItems
                            {
                                id = p.Id,
                                text = string.Concat(p.Codigo, " - ", p.Descricao)
                            };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                var result = await queryResultPage.ToListAsync();

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = result, TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<ResultDropdownList> ListarProdutoPorEstoqueDropdown(DropdownInput dropdownInput)
        {
            long estoqueId;

            long.TryParse(dropdownInput.filtros[0], out estoqueId);

            var estoquesGrupos = _estoqueGrupoRepositorio.GetAll();

            var result = await ListarDropdownLambda(dropdownInput
                                         , _produtoRepositorio
                                         , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                        || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                         && (!m.IsPrincipal
                                         && !m.IsBloqueioCompra
                                         && m.IsAtivo
                                         )

                                         && estoquesGrupos.Any(a => a.GrupoId == m.GrupoId
                                                                                   && a.EstoqueId == estoqueId)




                                        , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                        , o => o.Descricao
                                        );

            return result;
        }

        public async Task<ResultDropdownList> ListarMedicamentoPorEstoqueDropdown(DropdownInput dropdownInput)
        {
            long estoqueId;
            bool isMedicamento;

            long.TryParse(dropdownInput.filtros[0], out estoqueId);
            bool.TryParse(dropdownInput.filtros[1], out isMedicamento);

            var estoquesGrupos = _estoqueGrupoRepositorio.GetAll();

            var result = await ListarDropdownLambda(dropdownInput
                                         , _produtoRepositorio
                                         , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                        || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                         && (!m.IsPrincipal
                                         //&& m.ProdutoPrincipalId != null
                                         && !m.IsBloqueioCompra
                                         && m.IsAtivo
                                         && m.IsMedicamento == isMedicamento
                                         )
                                         && estoquesGrupos.Any(a => a.GrupoId == m.GrupoId &&
                                                                    a.EstoqueId == estoqueId // &&
                                                                                             // m.GrupoId == a.GrupoId
                                                                    )
                                        , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                        , o => o.Descricao
                                        );

            return result;
        }

        public async Task<ResultDropdownList> ListarProdutoPorEstoque2Dropdown(DropdownInput dropdownInput)
        {
            long estoqueId;

            //long.TryParse(dropdownInput.filtros[0], out estoqueId);

            long.TryParse(dropdownInput.filtro, out estoqueId);

            var produtosEstoque = _produtoEstoqueRepositorio.GetAll();

            return await ListarDropdownLambda(dropdownInput
                                              , _produtoRepositorio
                                              , m => (!m.IsPrincipal && !m.IsBloqueioCompra && m.IsAtivo)
                                                      &&
                                                      produtosEstoque.Any(a => a.ProdutoId == m.Id && a.EstoqueId == estoqueId)
                                              , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                              , o => o.Descricao
                                              );
        }

        public async Task<ResultDropdownList> ListarProdutoPorSaidaAtendimentoDropdown(DropdownInput dropdownInput)
        {
            long estoqueId;
            long atendimentoId;

            long.TryParse(dropdownInput.filtros[0], out estoqueId);
            long.TryParse(dropdownInput.filtros[1], out atendimentoId);


            var query = from movimento in _estoqueMovimentoRepositorio.GetAll()
                        join movimentoItem in _estoqueMovimentoItemRepositorio.GetAll()
                        on movimento.Id equals movimentoItem.MovimentoId
                        where movimento.EstoqueId == estoqueId
                           && movimento.AtendimentoId == atendimentoId
                        select movimentoItem;


            return await ListarDropdownLambda(dropdownInput
                                                     , _produtoRepositorio
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                     && (!m.IsPrincipal
                                                     && !m.IsBloqueioCompra
                                                     && m.IsAtivo

                                                     && query.Any(a => a.ProdutoId == m.Id)

                                                     )

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );


        }

        public async Task<ResultDropdownList> ListarProdutoPorSaidaSetorDropdown(DropdownInput dropdownInput)
        {
            long estoqueId;
            long unidadeOrganizacionalId;

            long.TryParse(dropdownInput.filtros[0], out estoqueId);
            long.TryParse(dropdownInput.filtros[1], out unidadeOrganizacionalId);


            var query = from movimento in _estoqueMovimentoRepositorio.GetAll()
                        join movimentoItem in _estoqueMovimentoItemRepositorio.GetAll()
                        on movimento.Id equals movimentoItem.MovimentoId
                        where movimento.EstoqueId == estoqueId
                           && movimento.UnidadeOrganizacionalId == unidadeOrganizacionalId
                        select movimentoItem;


            return await ListarDropdownLambda(dropdownInput
                                                     , _produtoRepositorio
                                                     , m => (string.IsNullOrEmpty(dropdownInput.search) || m.Descricao.ToLower().Contains(dropdownInput.search.ToLower())
                                                    || m.Codigo.ToString().ToLower().Contains(dropdownInput.search.ToLower()))
                                                     && (!m.IsPrincipal
                                                     && !m.IsBloqueioCompra
                                                     && m.IsAtivo

                                                     && query.Any(a => a.ProdutoId == m.Id)

                                                     )

                                                    , p => new DropdownItems { id = p.Id, text = string.Concat(p.Codigo.ToString(), " - ", p.Descricao) }
                                                    , o => o.Descricao
                                                    );


        }

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await ListarProdutoPorEstoque2Dropdown(dropdownInput);
        }

        public async Task<ResultDropdownList> ListarDcbDropdown(DropdownInput dropdownInput)
        {
            int pageInt = int.Parse(dropdownInput.page) - 1;
            var numberOfObjectsPerPage = int.Parse(dropdownInput.totalPorPagina);
            List<DcbDto> dcbsDtos = new List<DcbDto>();
            try
            {
                var query = from p in _dcbRepositorio.GetAll()
                        .WhereIf(!dropdownInput.search.IsNullOrEmpty(), m => m.Descricao.ToLower().Contains(dropdownInput.search.ToLower()))
                            orderby p.Descricao ascending
                            select new DropdownItems { id = p.Id, text = p.Codigo + " - " + p.Descricao };

                var queryResultPage = query
                  .Skip(numberOfObjectsPerPage * pageInt)
                  .Take(numberOfObjectsPerPage);

                int total = await query.CountAsync();

                return new ResultDropdownList() { Items = queryResultPage.ToList(), TotalCount = total };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        #endregion Dropdowns

        #endregion Metodos

    }
}