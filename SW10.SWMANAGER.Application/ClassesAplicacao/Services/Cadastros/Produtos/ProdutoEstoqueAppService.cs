﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.ProdutosEstoque.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Abp.Linq.Extensions;
using SW10.SWMANAGER.ClassesAplicacao.Services.Suprimentos.Estoques.Dto;
using SW10.SWMANAGER.ClassesAplicacao.Suprimentos.Estoques.Movimentos;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Produtos
{
    public class ProdutoEstoqueAppService : SWMANAGERAppServiceBase, IProdutoEstoqueAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ProdutoEstoque, long> _produtoEstoqueRepositorio;
        private readonly IRepository<Estoque, long> _estoqueRepositorio;

        public ProdutoEstoqueAppService(IUnitOfWorkManager unitOfWorkManager
            , IRepository<ProdutoEstoque, long> produtoEstoqueRepositorio
            , IRepository<Estoque, long> estoqueRepositorio
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _produtoEstoqueRepositorio = produtoEstoqueRepositorio;
            _estoqueRepositorio = estoqueRepositorio;
        }

        public async Task<ProdutoEstoqueDto> CriarOuEditar(ProdutoEstoqueDto input)
        {
            try
            {
                var produtoEstoque = input.MapTo<ProdutoEstoque>();//new ProdutoEstoque();


                if (input.Id.Equals(0))
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        input.Id = await _produtoEstoqueRepositorio.InsertAndGetIdAsync(produtoEstoque);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }

                }
                else
                {
                    using (var unitOfWork = _unitOfWorkManager.Begin())
                    {
                        await _produtoEstoqueRepositorio.UpdateAsync(produtoEstoque);
                        unitOfWork.Complete();
                        _unitOfWorkManager.Current.SaveChanges();
                        unitOfWork.Dispose();
                    }
                }
                var query = await Obter(input.Id);
                var newObj = query.MapTo<ProdutoEstoque>();

               var  produtoRelacaoPortariaDto = newObj.MapTo<ProdutoEstoqueDto>();
                //produtoRelacaoPortariaDto.TipoUnidade = newObj.TipoUnidade;

                return produtoRelacaoPortariaDto;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroSalvar"), ex);
            }
        }

        public async Task<ProdutoEstoqueDto> Obter(long id)
        {
            try
            {
                var query = await _produtoEstoqueRepositorio
                    .GetAll()
                    .Include(m => m.Estoque)
                    .FirstOrDefaultAsync(m => m.Id == id);

                var produtoEstoque = query.MapTo<ProdutoEstoqueDto>();

                return produtoEstoque;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        public async Task<PagedResultDto<ProdutoEstoqueDto>> ListarPorProduto(ListarInput input)
        {
            try
            {
                var produtoId = Convert.ToInt64(input.Filtro);
                var query =  _produtoEstoqueRepositorio
                    .GetAll()
                    .Include(m => m.Estoque)
                    .Where(w => w.ProdutoId == produtoId);

               var contarProdutosEstoque = await  query.CountAsync();

               var produtosEstoque = await query
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

               var  produtosEstoqueDtos = produtosEstoque
                    .MapTo<List<ProdutoEstoqueDto>>();

                return new PagedResultDto<ProdutoEstoqueDto>(
                    contarProdutosEstoque,
                    produtosEstoqueDtos
                    );

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

        [UnitOfWork]
        public async Task Excluir(ProdutoEstoqueDto input)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin())
                {
                    await _produtoEstoqueRepositorio.DeleteAsync(input.Id);

                    unitOfWork.Complete();
                    _unitOfWorkManager.Current.SaveChanges();
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroExcluir"), ex);
            }

        }

        public ListResultDto<EstoqueDto> ListarTodosNaoRelacionadosProdutos(long produtoId, long id)
        {
            try
            {
                List<EstoqueDto> objListaDto = new List<EstoqueDto>();

                var produtosEstoques = _produtoEstoqueRepositorio.GetAll().Where(w => w.ProdutoId == produtoId);

                var query = _estoqueRepositorio
                    .GetAll()
                    .Where(w => !produtosEstoques.Any(a=>a.EstoqueId == w.Id) || produtosEstoques.Any(a => a.Id ==id && a.EstoqueId == w.Id));

                //.Where(w =>  _produtoEstoqueRepositorio.GetAll()
                //       .Any(a => a.ProdutoId == w.Id ));

                objListaDto = query.MapTo<List<EstoqueDto>>();

                return new ListResultDto<EstoqueDto> { Items = objListaDto };

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("ErroPesquisar"), ex);
            }
        }

    }
}
