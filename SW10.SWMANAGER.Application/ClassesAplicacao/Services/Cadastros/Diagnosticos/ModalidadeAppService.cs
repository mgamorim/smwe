﻿using Abp.Domain.Repositories;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using SW10.SWMANAGER.ClassesAplicacao.Services.Base.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Diagnosticos
{
    public class ModalidadeAppService : SWMANAGERAppServiceBase, IModalidadeAppService
    {
        private readonly IRepository<Modalidade, long> _modalidadeRepository;

        public ModalidadeAppService(IRepository<Modalidade, long> modalidadeRepository)
        {
            _modalidadeRepository = modalidadeRepository;
        }
             

        public async Task<ResultDropdownList> ListarDropdown(DropdownInput dropdownInput)
        {
            return await base.ListarCodigoDescricaoDropdown(dropdownInput, _modalidadeRepository);
        }
    }
}
