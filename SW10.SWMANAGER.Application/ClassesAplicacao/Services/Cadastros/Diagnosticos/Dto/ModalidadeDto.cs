﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Diagnosticos.Dto
{
    [AutoMap(typeof(Modalidade))]
    public class ModalidadeDto : CamposPadraoCRUDDto
    {
        public override string Codigo { get; set; }

        public bool IsParecer { get; set; }

    }
}
