﻿using Abp.AutoMapper;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Diagnosticos.Modalidades.Dto
{
    [AutoMap(typeof(Modalidade))]
   public class ModalidadeDto : CamposPadraoCRUD
    {
        public bool IsParecer { get; set; }
        public string Filtro { get; set; }
    }
}

