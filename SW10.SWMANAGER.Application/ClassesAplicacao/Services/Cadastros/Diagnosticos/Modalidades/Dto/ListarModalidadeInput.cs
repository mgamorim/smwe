﻿using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Runtime.Validation;
using SW10.SWMANAGER.ClassesAplicacao.Diagnosticos.Imagens;
using SW10.SWMANAGER.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW10.SWMANAGER.ClassesAplicacao.Services.Cadastros.Diagnosticos.Modalidades.Dto
{
    public class ListarModalidadeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filtro { get; set; }
        public string IsParecer { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Descricao";
            }
        }
    }

}
