﻿using Abp.AutoMapper;

namespace SW10.SWMANAGER.ClassesAplicacao.Services
{
    [AutoMap(typeof(Sexo))]
    public class SexoDto : CamposPadraoCRUDDto
    {
        public string Descricao { get; set; }
    }
}
