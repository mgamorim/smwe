using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Organizations;

namespace SW10.SWMANAGER.Organizations.Dto
{
    [AutoMapFrom(typeof(OrganizationUnit))]
    [AutoMapTo(typeof(OrganizationUnit))]
    public class OrganizationUnitDto : AuditedEntityDto<long>
    {
        public long? ParentId { get; set; }

        public string Code { get; set; }

        public string DisplayName { get; set; }

        public int MemberCount { get; set; }


        #region Mapeamento

        public static OrganizationUnit Mapear(OrganizationUnitDto organizationUnitDto)
        {
            OrganizationUnit organizationUnit = new OrganizationUnit();

            organizationUnit.Code = organizationUnitDto.Code;
            organizationUnit.DisplayName = organizationUnitDto.DisplayName;
            organizationUnit.Id = organizationUnitDto.Id;
            organizationUnit.ParentId = organizationUnitDto.ParentId;

            return organizationUnit;
        }


        public static OrganizationUnitDto Mapear(OrganizationUnit organizationUnit)
        {
            OrganizationUnitDto organizationUnitDto = new OrganizationUnitDto();

            organizationUnitDto.Code = organizationUnit.Code;
            organizationUnitDto.DisplayName = organizationUnit.DisplayName;
            organizationUnitDto.Id = organizationUnit.Id;
            organizationUnitDto.ParentId = organizationUnit.ParentId;

            return organizationUnitDto;
        }

        #endregion

    }
}