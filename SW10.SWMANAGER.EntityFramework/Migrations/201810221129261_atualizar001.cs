namespace SW10.SWMANAGER.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class atualizar001 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AteAtendimento", "CaraterAtendimentoId", "dbo.SisTabelaDominio");
            DropForeignKey("dbo.AteAtendimento", "IndicacaoAcidenteId", "dbo.SisTabelaDominio");
            DropForeignKey("dbo.AteAtendimentoGrupoCID", "AtendimentoId", "dbo.AteAtendimento");
            DropForeignKey("dbo.AteAtendimentoGrupoCID", "GrupoCIDId", "dbo.GrupoCID");
            DropIndex("dbo.AteAtendimento", new[] { "CaraterAtendimentoId" });
            DropIndex("dbo.AteAtendimento", new[] { "IndicacaoAcidenteId" });
            DropIndex("dbo.AteAtendimentoGrupoCID", new[] { "AtendimentoId" });
            DropIndex("dbo.AteAtendimentoGrupoCID", new[] { "GrupoCIDId" });
            AlterColumn("dbo.SisMedico", "Nascimento", c => c.DateTime(nullable: false));
            DropColumn("dbo.AteAtendimento", "CNS");
            DropColumn("dbo.AteAtendimento", "CaraterAtendimentoId");
            DropColumn("dbo.AteAtendimento", "IndicacaoAcidenteId");
            DropTable("dbo.AteAtendimentoGrupoCID",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AtendimentoGrupoCID_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AteAtendimentoGrupoCID",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AtendimentoId = c.Long(nullable: false),
                        GrupoCIDId = c.Long(nullable: false),
                        IsSistema = c.Boolean(nullable: false),
                        Codigo = c.String(maxLength: 10),
                        Descricao = c.String(),
                        ImportaId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AtendimentoGrupoCID_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AteAtendimento", "IndicacaoAcidenteId", c => c.Long());
            AddColumn("dbo.AteAtendimento", "CaraterAtendimentoId", c => c.Long());
            AddColumn("dbo.AteAtendimento", "CNS", c => c.String());
            AlterColumn("dbo.SisMedico", "Nascimento", c => c.DateTime());
            CreateIndex("dbo.AteAtendimentoGrupoCID", "GrupoCIDId");
            CreateIndex("dbo.AteAtendimentoGrupoCID", "AtendimentoId");
            CreateIndex("dbo.AteAtendimento", "IndicacaoAcidenteId");
            CreateIndex("dbo.AteAtendimento", "CaraterAtendimentoId");
            AddForeignKey("dbo.AteAtendimentoGrupoCID", "GrupoCIDId", "dbo.GrupoCID", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AteAtendimentoGrupoCID", "AtendimentoId", "dbo.AteAtendimento", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AteAtendimento", "IndicacaoAcidenteId", "dbo.SisTabelaDominio", "Id");
            AddForeignKey("dbo.AteAtendimento", "CaraterAtendimentoId", "dbo.SisTabelaDominio", "Id");
        }
    }
}
